# coding: utf-8
# Copyright (c) Foxit Software Inc..

Pod::Spec.new do |s|
  s.name           = 'FoxitPDF'
  s.version        = '7.5.1'
  s.summary        = 'Foxit PDF SDK provides high-performance libraries to help any software developer add robust PDF functionality to their enterprise, mobile and cloud applications across all platforms (includes Windows, Mac, Linux, Web, Android, iOS, and UWP), using the most popular development languages and environments. Application developers who use Foxit PDF SDK can leverage Foxit’s powerful, standard-compliant PDF technology to securely display, create, edit, annotate, format, organize, print, share, secure, search documents as well as to fill PDF forms. Additionally, Foxit PDF SDK includes a built-in, embeddable PDF Viewer, making the development process easier and faster. For more detailed information, please visit the website https://developers.foxitsoftware.com/pdf-sdk/'
  s.author         = 'Foxit Software Incorporated'
  s.homepage       = 'https://developers.foxitsoftware.com/pdf-sdk/ios/'
  s.platform       = :ios, '11.0'
  s.license        = 'MIT'
  s.source         = { :git => '' }
  s.subspec 'FoxitRDK' do |ss|
    ss.source_files  = 'FoxitRDK.framework/Headers/**.h'
    ss.public_header_files =  'FoxitRDK.framework/Headers/**.h'
    ss.vendored_frameworks = 'FoxitRDK.framework'
  end
  s.subspec 'uiextensionsDynamic' do |ss|
    ss.source_files  =  'uiextensionsDynamic.framework/Headers/**.h',
    ss.public_header_files =   'uiextensionsDynamic.framework/Headers/**.h'
    ss.vendored_frameworks =  'uiextensionsDynamic.framework'
    ss.dependency 'FoxitPDF/FoxitRDK'
  end
  s.subspec 'FoxitPDFScanUI' do |ss|
    ss.source_files  =  'FoxitPDFScanUI.framework/Headers/**.h',
    ss.public_header_files =   'FoxitPDFScanUI.framework/Headers/**.h'
    ss.vendored_frameworks =  'FoxitPDFScanUI.framework'
    ss.dependency 'FoxitPDF/FoxitRDK'
    ss.dependency 'FoxitPDF/uiextensionsDynamic'
  end
end
