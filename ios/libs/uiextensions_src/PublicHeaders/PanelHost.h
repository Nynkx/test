/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

/**
 * @file    PanelHost.h
 * @details    Panel host manage all the panels including annoation, attachment, outline, bookmark.. The caller may add or remove the specified panel on the panel host. The panels will be displayed when the panel button on the bottom bar is pressed.
 */

#import "SegmentView.h"

/** @brief Panel types. */
typedef NS_ENUM(NSUInteger, FSPanelType) {
    /** @brief Annotation panel type. */
    FSPanelTypeAnnotation = 1,
    /** @brief Attachment panel type. */
    FSPanelTypeAttachment,
    /** @brief Outline panel type. */
    FSPanelTypeOutline,
    /** @brief Reading bookmark panel type. */
    FSPanelTypeReadingBookmark,
    /** @brief Reading digital signature panel type. */
    FSPanelTypeDigitalSignature
};

/** For adding a new panel type, caller could implement this protocal. */
@protocol IPanelSpec <NSObject>
/** @brief Return one of the predefined enumeration value FSPanelType. If the caller will implement a new panel type, then the return value should not conflict with the predefined values, or there will be undefined behavious. */
- (int)getType;
- (UIView *)getTopToolbar;
- (UIView *)getContentView;
- (SegmentItem*)getSegmentItem;
- (void)onActivated;
- (void)onDeactivated;
@end

/** @brief Panel UI implementation. */
@interface PanelHost : NSObject <SegmentDelegate>
@property (nonatomic, strong) SegmentView *segmentView;
@property (nonatomic, strong) NSMutableArray *specs;
@property (nonatomic, strong) id<IPanelSpec> currentSpec;
@property (nonatomic, strong) UIView *contentView;
- (instancetype)initWithFrame:(CGSize)size;
- (void)addSpec:(id<IPanelSpec>)spec;
- (void)insertSpec:(id<IPanelSpec>)spec atIndex:(int)index;
- (void)removeSpec:(id<IPanelSpec>)spec;
/** @brief Reload the panel segment view interface. If a panel is added or removed, then this function should be called.*/
- (void)reloadSegmentView;
@end
