/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

/**
 * @file	SettingBar.h
 * @details	The setting bar consists controls to set page layout, screen brightness, reflow, crop and so on.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol IRotationEventListener;
@class UIExtensionsManager;
@class FSPDFViewCtrl;
@class SettingBar;
/**
 * @brief	Enumeration for item types in setting bar.
 *
 * @details	Values of this enumeration should be used alone.
 */
typedef NS_ENUM(NSUInteger, SettingItemType) {
    /** @brief	Single page layout button. */
    SINGLE,
    /** @brief	Continuous page layout button. */
    CONTINUOUS,
    /** @brief	Double page layout button. */
    DOUBLEPAGE,
    /** @brief	Cover page layout button. */
    COVERPAGE,
    /** @brief	Thumbnail button. */
    THUMBNAIL,
    /** @brief	Reflow button. */
    REFLOW,
    /** @brief	Crop page button. */
    CROPPAGE,
    /** @brief	Lock screen button. */
    LOCKSCREEN,
    /** @brief	Brightness slider view. */
    BRIGHTNESS,
    /** @brief	Night mode button. */
    NIGHTMODE,
    /** @brief  Pan&zoom button. */
    PANZOOM,
    /** @brief  Fitpage button. */
    FITPAGE,
    /** @brief  Fitwidth button. */
    FITWIDTH,
    /** @brief  rotate button. */
    ROTATE,
    /** @brief  Speech button. */
    SPEECH,
};


/** @brief    application life cycle listener. */
@protocol IAppLifecycleListener <NSObject>
@optional
/** @brief    triggered when application will resign active. */
- (void)applicationWillResignActive:(UIApplication *)application;
/** @brief    triggered when application did enter the background. */
- (void)applicationDidEnterBackground:(UIApplication *)application;
/** @brief    triggered when application did enter the foreground. */
- (void)applicationWillEnterForeground:(UIApplication *)application;
/** @brief    triggered when application did become active. */
- (void)applicationDidBecomeActive:(UIApplication *)application;
@end


/** @brief	SettingBar delegate. */
@protocol SettingBarDelegate <NSObject>
@optional
// Methods for notification of selection/deselection events.
/**
 * @brief	Triggered when select single page layout.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarSinglePageLayout:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select continuous page layout.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarContinuousLayout:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select double page layout.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarDoublePageLayout:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select double page layout with cover.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarCoverPageLayout:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select thumbnail item.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarThumbnail:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select reflow item.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarReflow:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select crop page mode.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarCrop:(SettingBar *)settingBar;
/**
 * @brief    Triggered when speech page text mode.
 *
 * @param[in]    settingBar      The setting bar.
 */
- (void)settingBarSpeech:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select pan&zoom mode.
 *
 * @param[in]	settingBar      The setting bar.
 */
- (void)settingBarPanAndZoom:(SettingBar *)settingBar;
/**
 * @brief	Triggered when select lock screen item.
 *
 * @param[in]	settingBar      The setting bar.
 * @param[in]	isLockScreen    Whether to lock screen rotation.
 */
- (void)settingBar:(SettingBar *)settingBar setLockScreen:(BOOL)isLockScreen;
/**
 * @brief	Triggered when select night mode item.
 *
 * @param[in]	settingBar      The setting bar.
 * @param[in]	isNightMode     Night mode or not.
 */
- (void)settingBar:(SettingBar *)settingBar setNightMode:(BOOL)isNightMode;
/**
 * @brief    Triggered when select fitpage item.
 *
 * @param[in]    settingBar      The setting bar.
 */
- (void)settingBarFitPage:(SettingBar *)settingBar;
/**
 * @brief    Triggered when select fitwidth item.
 *
 * @param[in]    settingBar      The setting bar.
 */
- (void)settingBarFitWidth:(SettingBar *)settingBar;
/**
 * @brief    Triggered when select rotate item.
 *
 * @param[in]    settingBar      The setting bar.
 */
- (void)settingBarRotate:(SettingBar *)settingBar;
/**
 * @brief    Triggered when setting bar size is changed.
 *
 * @param[in]    settingBar      The setting bar.
 */
- (void)settingBarDidChangeSize:(SettingBar *)settingBar;

@end

/** @brief Setting bar is actived by tapping "View" item on the bottom bar. */
@interface SettingBar : NSObject <IAppLifecycleListener>
/** @brief Content view for setting bar. */
@property (nonatomic, strong) UIView *contentView;
/** @brief Get/Set delegate for setting bar. */
@property (nonatomic, weak) id<SettingBarDelegate> delegate;

/**
 * @brief  Deprecated, use isItemHidden instead.
 * Get setting bar items show/hide status.
 */
- (NSMutableDictionary *)getItemHiddenStatus;

/** @brief  Check if item is visible or not. */
- (BOOL)isItemHidden:(SettingItemType) type;

/**
 * @brief	Hide or show item in setting bar.
 *
 * @param[in]	itemType     Item type. Please refer to {@link SettingItemType::SINGLE SettingItemType::XXX} values and this should be one of these values.
 */
- (void)setItem:(SettingItemType)itemType hidden:(BOOL)hidden;
/**
 * @brief	Update layout of items in setting bar.
 */
- (void)updateBtnLayout;
@end
