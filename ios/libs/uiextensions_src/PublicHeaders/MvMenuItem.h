/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

/**
 * @file    MvMenuItem.h
 * @details    The menu item definition.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol IMvCallback;
/** brief Definition for menu item.*/
@interface MvMenuItem : NSObject
/** brief The menu tag.*/
@property (nonatomic, assign) NSUInteger tag;
/** brief The menu title text.*/
@property (nonatomic, strong) NSString *text;
/** brief The menu icon id. Unused.*/
@property (nonatomic, assign) NSInteger iconId;
/** brief The menu item is enabled or not.*/
@property (nonatomic, assign) BOOL enable;
/** brief The callback action associated with this menu item.*/
@property (nonatomic, weak) id<IMvCallback> callBack;
/** brief The custom view for item. Unused.*/
@property (nonatomic, strong) UIView *customView;

@end
