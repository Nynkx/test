/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CaretAnnotHandler.h"

#define FSPDF_ANNOT_INTENTNAME_CARET_REPLACE "Replace"
#define FSPDF_ANNOT_INTENTNAME_CARET_INSERTTEXT "Insert Text"

@interface CaretAnnotHandler ()

@property (nonatomic, strong) NSArray<FSAnnotAttributes *> *attributesArrayBeforeModify; //for undo

@end

@implementation CaretAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-30, -30, -30, -30);
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
        self.attributesArrayBeforeModify = nil;
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotCaret;
}

- (CGRect)getAnnotRect:(FSAnnot *)annot {
    FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
    if (![markup isGrouped]) {
        return [super getAnnotRect:annot];
    } else {
        int pageIndex = annot.pageIndex;
        CGRect unionRect = CGRectZero;
        FSMarkupArray *markups = [markup getGroupElements];
        for (int i = 0; i < [markups getSize]; i++) {
            FSAnnot *groupAnnot = [markups getAt:i];
            CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:groupAnnot.fsrect pageIndex:pageIndex];
            if (CGRectIsEmpty(unionRect))
                unionRect = rect;
            else
                unionRect = CGRectUnion(unionRect, rect);
        }
        return unionRect;
    }
}

- (BOOL)canMoveAnnot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)canResizeAnnot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)shouldDrawAnnot:(FSAnnot *)annot inPDFViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    return YES;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    [super onAnnotSelected:annot];

    self.attributesArrayBeforeModify = [self createAnnotAttributes:[[FSCaret alloc] initWithAnnot:annot]];
}

- (void)showStyle {
    NSArray *colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.extensionsManager.propertyBar setColors:colors];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY frame:CGRectZero];
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];
    int pageIndex = annot.pageIndex;
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
}

- (BOOL)addAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (!addUndo || ![[[FSMarkup alloc] initWithAnnot:annot] isGrouped]) {
        return [super addAnnot:annot addUndo:addUndo];
    }
    // save all attributes of annot in same group
    FSPDFPage *page = [annot getPage];
    NSArray<FSAnnotAttributes *> *attributesArray = [self createAnnotAttributes:[[FSCaret alloc] initWithAnnot:annot]];
    int pageIndex = annot.pageIndex;
    UndoItem *undoItem = [UndoItem itemWithUndo:^(UndoItem *item) {
        FSAnnot *annot = [Utility getAnnotByNM:attributesArray[0].NM inPage:page];
        if (annot && ![annot isEmpty]) {
            [self removeAnnot:annot addUndo:NO];
        }
    }
        redo:^(UndoItem *item) {
            NSMutableArray<FSMarkup *> *annotArray = [NSMutableArray<FSMarkup *> arrayWithCapacity:[attributesArray count]];
            for (FSAnnotAttributes *attributes in attributesArray) {
                FSAnnot *annot = [page addAnnot:attributes.type rect:attributes.rect];
                if (annot && ![annot isEmpty]) {
                    [attributes resetAnnot:annot];
                    [annotArray addObject:[[FSMarkup alloc] initWithAnnot:annot]];
                }
            }
            FSMarkupArray *fsAnnotArray = [[FSMarkupArray alloc] init];
            for (FSMarkup *annot in annotArray) {
                [fsAnnotArray add:annot];
            }
            [page setAnnotGroup:fsAnnotArray header_index:0];
            for (FSAnnot *annot in annotArray) {
                id<IAnnotHandler> annotHandler = [self.extensionsManager getAnnotHandlerByAnnot:annot];
                [annotHandler addAnnot:annot addUndo:NO];
            }
        }
        pageIndex:pageIndex
                          attributes:attributesArray];
    [self.extensionsManager addUndoItem:undoItem];

    [self.extensionsManager onAnnotAdded:page annot:annot];

    [self refreshPageForAnnot:annot reRenderPage:YES];
    return YES;
}

- (BOOL)modifyAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (!addUndo || ![annot canModify] || ![[[FSMarkup alloc] initWithAnnot:annot] isGrouped]) {
        return [super modifyAnnot:annot addUndo:addUndo];;
    }
    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return NO;
    }
    annot.modifiedDate = [NSDate date];

    NSMutableArray<UndoItem *> *undoItemArray = [NSMutableArray<UndoItem *> array];
    NSUInteger count = self.attributesArrayBeforeModify.count;
    NSArray<FSAnnotAttributes *> *attributesArray = [self createAnnotAttributes:[[FSCaret alloc] initWithAnnot:annot]];
    assert(count == attributesArray.count);
    for (int i = 0; i < count; i++) {
        UndoItem *item = [UndoItem itemForUndoModifyAnnotWithOldAttributes:self.attributesArrayBeforeModify[i] newAttributes:attributesArray[i] pdfViewCtrl:self.pdfViewCtrl page:page annotHandler:[self.extensionsManager getAnnotHandlerByType:attributesArray[i].type]];
        if (item) {
            [undoItemArray addObject:item];
        }
    }
    [self.extensionsManager addUndoItem:[UndoItem itemByMergingItems:undoItemArray]];

    [self.extensionsManager onAnnotModified:page annot:annot];
    [self refreshPageForAnnot:annot reRenderPage:YES];
    return YES;
}

- (BOOL)removeAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (![annot canModify] || ![[[FSMarkup alloc] initWithAnnot:annot] isGrouped]) {
        return [super removeAnnot:annot addUndo:addUndo];;
    }
    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return NO;
    }
    int pageIndex = annot.pageIndex;
    CGRect rect = [self getAnnotRect:annot];
    rect = UIEdgeInsetsInsetRect(rect, self.annotRefreshRectEdgeInsets);
    rect = CGRectIntersection(rect, [self.pdfViewCtrl getPageView:pageIndex].bounds);

    [self.extensionsManager onAnnotWillDelete:page annot:annot];

    if (addUndo) {
        NSArray<FSAnnotAttributes *> *attributesArray = self.attributesArrayBeforeModify ?: [self createAnnotAttributes:[[FSCaret alloc] initWithAnnot:annot]];
        UndoItem *undoItem = [UndoItem itemWithUndo:^(UndoItem *item) {
            NSMutableArray<FSMarkup *> *annotArray = [NSMutableArray<FSMarkup *> arrayWithCapacity:[attributesArray count]];
            for (FSAnnotAttributes *attributes in attributesArray) {
                FSAnnot *annot = [page addAnnot:attributes.type rect:attributes.rect];
                if (annot && ![annot isEmpty]) {
                    [attributes resetAnnot:annot];
                    [annotArray addObject:[[FSMarkup alloc] initWithAnnot:annot]];
                }
            }
            FSMarkupArray *fsAnnotArray = [[FSMarkupArray alloc] init];
            for (FSMarkup *annot in annotArray) {
                [fsAnnotArray add:annot];
            }
            [page setAnnotGroup:fsAnnotArray header_index:0];
            for (FSAnnot *annot in annotArray) {
                id<IAnnotHandler> annotHandler = [self.extensionsManager getAnnotHandlerByAnnot:annot];
                [annotHandler addAnnot:annot addUndo:NO];
            }
        }
            redo:^(UndoItem *item) {
                FSAnnot *annot = [Utility getAnnotByNM:attributesArray[0].NM inPage:page];
                if (annot && ![annot isEmpty]) {
                    [self removeAnnot:annot addUndo:NO];
                }
            }
            pageIndex:pageIndex
                              attributes:attributesArray];
        [self.extensionsManager addUndoItem:undoItem];
    }

    FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
    FSMarkupArray *annots = [markup getGroupElements];
    for (int i = 0; i < [annots getSize]; i++) {
        FSAnnot *groupAnnot = [annots getAt:i];
        if (![groupAnnot isEqualToAnnot:annot]) {
            [[self.extensionsManager getAnnotHandlerByAnnot:groupAnnot] removeAnnot:groupAnnot addUndo:NO];
        }
    }
    BOOL ret = [page removeAnnot:annot];
    if (ret) {
        [self.extensionsManager onAnnotDeleted:page annot:annot];
        [self.pdfViewCtrl refresh:rect pageIndex:pageIndex];
    }
    return ret;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = [super menuOptionsForAnnot:annot];
    options = options & (~FSMenuOptionCopyText);
    return options;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    if (self.extensionsManager.currentAnnot == annot) {
        if (pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint]) {
            return YES;
        } else {
            [self.extensionsManager setCurrentAnnot:nil];
            return YES;
        }
    } else {
        [self.extensionsManager setCurrentAnnot:annot];
//        [self comment];
        return YES;
    }
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
    FSAnnot *mkFsAnnot = nil;
    CGRect mkRect = CGRectZero;

    FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
    if ([markup isGrouped]) {
        FSMarkupArray *groupAnnots = [markup getGroupElements];
        for (int i = 0; i < [groupAnnots getSize]; i++) {
            FSAnnot *groupAnnot = [groupAnnots getAt:i];
            if (groupAnnot.type == FSAnnotStrikeOut) {
                mkFsAnnot = groupAnnot;
                break;
            }
        }
        if (mkFsAnnot && ![mkFsAnnot isEmpty]) {
            mkRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:mkFsAnnot.fsrect pageIndex:pageIndex];
        }
    } else {
        mkRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
    }
    if (pageIndex == annot.pageIndex && self.extensionsManager.currentAnnot == annot) {
        CGRect annotRect = [Utility getAnnotRect:annot pdfViewCtrl:self.pdfViewCtrl];
        annotRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:annotRect pageIndex:pageIndex];
        CGRect drawRect = CGRectMake(ceilf(rect.origin.x), ceilf(rect.origin.y), ceilf(annotRect.size.width), ceilf(annotRect.size.height));
        if (self.annotImage) {
            CGContextSaveGState(context);

            CGContextTranslateCTM(context, drawRect.origin.x, drawRect.origin.y);
            CGContextTranslateCTM(context, 0, drawRect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextTranslateCTM(context, -drawRect.origin.x, -drawRect.origin.y);
            CGContextDrawImage(context, drawRect, [self.annotImage CGImage]);

            CGContextRestoreGState(context);
        }
        drawRect = CGRectInset(drawRect, -2, -2);
        CGContextSetLineWidth(context, 2.0);
        CGFloat dashArray[] = {3, 3, 3, 3};
        CGContextSetLineDash(context, 3, dashArray, 4);
        CGColorRef borderColor = [[self borderColorForSelectedAnnot:annot] CGColor];
        CGContextSetStrokeColorWithColor(context, borderColor);
        CGContextStrokeRect(context, drawRect);

        if (mkFsAnnot && ![mkFsAnnot isEmpty]) {
            mkRect = CGRectInset(mkRect, -5, -5);
            CGContextSetLineWidth(context, 2.0);
            CGContextSetLineDash(context, 3, dashArray, 4);
            CGContextSetStrokeColorWithColor(context, borderColor);
            CGContextStrokeRect(context, mkRect);
        }
    }
}

#pragma mark private

- (NSArray<FSAnnotAttributes *> *)createAnnotAttributes:(FSCaret *)annot {
    NSMutableArray<FSAnnotAttributes *> *attributesArray = [NSMutableArray<FSAnnotAttributes *> array];
    [attributesArray addObject:[FSAnnotAttributes attributesWithAnnot:annot]];
    NSString *NM = annot.NM;
    FSMarkupArray *groupAnnots = [annot getGroupElements];
    for (int i = 0; i < [groupAnnots getSize]; i++) {
        FSAnnot *groupAnnot = [groupAnnots getAt:i];
        if (![groupAnnot.NM isEqualToString:NM]) {
            [attributesArray addObject:[FSAnnotAttributes attributesWithAnnot:groupAnnot]];
        }
    }
    return attributesArray;
}

@end
