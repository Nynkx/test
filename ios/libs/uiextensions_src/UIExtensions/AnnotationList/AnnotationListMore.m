/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AnnotationListMore.h"

#import "AnnotationListCell.h"
#import "AnnotationListViewController.h"
#import "AttachmentViewController.h"

#import "ReadingBookmarkListCell.h"
#import "ReadingBookmarkViewController.h"
#import "ReplyTableViewController.h"

@interface AnnotationListMore () <UIGestureRecognizerDelegate>
@property (nonatomic, assign) CGRect annotDetailBtRect;
@property (nonatomic, assign) CGRect replyBtRect;
@property (nonatomic, assign) CGRect bookmarkDetailBtRect;
@property (nonatomic, assign) CGRect bookmarkDetaileBtRect;

@end

@implementation AnnotationListMore {
}

- (id)initWithOrigin:(CGPoint)origin height:(CGFloat)height canRename:(BOOL)canRename canEditContent:(BOOL)canEditContent canDescript:(BOOL)canDescript canDelete:(BOOL)canDelete canReply:(BOOL)canReply canSave:(BOOL)canSave canFlatten:(BOOL)canFlatten canApplyRedaction:(BOOL)canApplyRedaction{
    // buttons
    self.renameButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kRename")
                                                              imageNormal:[UIImage imageNamed:@"document_edit_small_rename" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                            imageSelected:[UIImage imageNamed:@"document_edit_small_rename" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                             imageDisable:[UIImage imageNamed:@"document_edit_small_rename" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    self.replyButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kReply")
                                                             imageNormal:[UIImage imageNamed:@"panel_more_reply" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                           imageSelected:[UIImage imageNamed:@"panel_more_reply" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                            imageDisable:[UIImage imageNamed:@"panel_more_reply" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    self.noteButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kIconNote")
                                                            imageNormal:[UIImage imageNamed:@"panel_more_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                          imageSelected:[UIImage imageNamed:@"panel_more_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                           imageDisable:[UIImage imageNamed:@"panel_more_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    self.descriptionButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kDescription")
                                                                   imageNormal:[UIImage imageNamed:@"panel_more_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                                 imageSelected:[UIImage imageNamed:@"panel_more_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                                  imageDisable:[UIImage imageNamed:@"panel_more_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    self.saveButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kSave")
                                                            imageNormal:[UIImage imageNamed:@"panel_more_save" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                          imageSelected:[UIImage imageNamed:@"panel_more_save" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                           imageDisable:[UIImage imageNamed:@"panel_more_save" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    self.deleteButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kDelete")
                                                              imageNormal:[UIImage imageNamed:@"panel_more_delete" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                            imageSelected:[UIImage imageNamed:@"panel_more_delete" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                             imageDisable:[UIImage imageNamed:@"panel_more_delete" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    self.flattenButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kFlatten")
                                                              imageNormal:[UIImage imageNamed:@"panel_more_flatten" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                            imageSelected:[UIImage imageNamed:@"panel_more_flatten" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                             imageDisable:[UIImage imageNamed:@"panel_more_flatten" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    self.applyRedactButton = [AnnotationListMore createButtonWithImageAndTitle:FSLocalizedForKey(@"kApply")
                                                               imageNormal:[UIImage imageNamed:@"apply_redaction" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                             imageSelected:[UIImage imageNamed:@"apply_redaction" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                              imageDisable:[UIImage imageNamed:@"apply_redaction" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    NSMutableArray<UIButton *> *buttons = [NSMutableArray<UIButton *> array];
    if (canSave) {
        [buttons addObject:self.saveButton];
    }
    if (canRename) {
        [buttons addObject:self.renameButton];
    }
    if (canReply) {
        [buttons addObject:self.replyButton];
    }
    if (canEditContent) {
        [buttons addObject:self.noteButton];
    }
    if (canDescript) {
        [buttons addObject:self.descriptionButton];
    }
    if (canFlatten) {
         [buttons addObject:self.flattenButton];
    }
    if (canApplyRedaction) {
        [buttons addObject:self.applyRedactButton];
    }
    if (canDelete) {
        [buttons addObject:self.deleteButton];
    }
    const CGFloat minButtonWidth = 50.0f;
    __block CGFloat currrentX = 0;
    [buttons enumerateObjectsUsingBlock:^(UIButton *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        CGFloat buttonWidth = MAX(minButtonWidth, obj.frame.size.width);
        CGRect buttonFrame = {currrentX, 0, buttonWidth, height};
        obj.frame = buttonFrame;
        obj.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [obj addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        currrentX += buttonWidth;
    }];

    CGRect frame = {origin, currrentX, height};
    if ([super initWithFrame:frame]) {
        self.backgroundColor =  OptionMoreColor;
        [buttons enumerateObjectsUsingBlock:^(UIButton *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            [self addSubview:obj];
        }];
    }
    return self;
}

- (void)handleClick:(UIButton *)button {
    if (button == self.renameButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreRename:)]) {
            [self.delegate annotationListMoreRename:self];
        }
    } else if (button == self.deleteButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreDelete:)]) {
            [self.delegate annotationListMoreDelete:self];
        }
    } else if (button == self.replyButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreReply:)]) {
            [self.delegate annotationListMoreReply:self];
        }
    } else if (button == self.saveButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreSave:)]) {
            [self.delegate annotationListMoreSave:self];
        }
    } else if (button == self.noteButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreEdit:)]) {
            [self.delegate annotationListMoreEdit:self];
        }
    } else if (button == self.descriptionButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreDescript:)]) {
            [self.delegate annotationListMoreDescript:self];
        }
    }
    else if (button == self.flattenButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreFlatten:)]) {
            [self.delegate annotationListMoreFlatten:self];
        }
    }
    else if (button == self.applyRedactButton) {
        if ([self.delegate respondsToSelector:@selector(annotationListMoreApplyRedaction:)]) {
            [self.delegate annotationListMoreApplyRedaction:self];
        }
    }
}

+ (UIButton *)createButtonWithImageAndTitle:(NSString *)title
                                imageNormal:(UIImage *)imageNormal
                              imageSelected:(UIImage *)imageSelected
                               imageDisable:(UIImage *)imageDisabled {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

    CGSize titleSize = [Utility getTextSize:title fontSize:9.0f maxSize:CGSizeMake(200, 100)];

    float width = imageNormal.size.width;
    float height = imageNormal.size.height;
    button.contentMode = UIViewContentModeScaleToFill;
    [button setImage:imageNormal forState:UIControlStateNormal];
    [button setImage:[Utility imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateHighlighted];
    [button setImage:[Utility imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateSelected];

    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRGB:0x5c5c5c] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithRGB:0x5c5c5c] forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont systemFontOfSize:9];

    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height * 1.5, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    button.frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width + 2 : width, titleSize.height + height);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    return button;
}

@end
