/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CommentModule.h"

@interface CommentModule ()<MoreMenuItemAction>
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@end

@implementation CommentModule

- (instancetype)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _extensionsManager = extensionsManager;
        [self loadModule];
    }
    return self;
}

- (void)loadModule{
    
    MoreMenuGroup *group = [_extensionsManager.more getGroup:TAG_GROUP_COMMENT];
    if (!group) {
        group = [[MoreMenuGroup alloc] init];
        group.tag = TAG_GROUP_COMMENT;
        
        group.title = FSLocalizedForKey(@"kReadComment");
        [_extensionsManager.more addGroup:group];
    }
    MoreMenuItem *importItem = [[MoreMenuItem alloc] init];
    importItem.tag = TAG_ITEM_IMPORTCOMMENT;
    importItem.text = FSLocalizedForKey(@"kImportComments");
    importItem.callBack = self;
    [_extensionsManager.more addMenuItem:TAG_GROUP_COMMENT withItem:importItem];
    
    MoreMenuItem *exportItem = [[MoreMenuItem alloc] init];
    exportItem.tag = TAG_ITEM_EXPORTCOMMENT;
    exportItem.text = FSLocalizedForKey(@"kExportComments");
    exportItem.callBack = self;
    [_extensionsManager.more addMenuItem:TAG_GROUP_COMMENT withItem:exportItem];
}

#pragma MoreMenuItemAction
- (void)onClick:(MoreMenuItem *)item{
    
    BOOL canAddAnnot = [Utility canAddAnnot:_pdfViewCtrl];
    BOOL isDynamicXFA = [_pdfViewCtrl isDynamicXFA];
    if ((!canAddAnnot && item.tag == TAG_ITEM_IMPORTCOMMENT) || isDynamicXFA) {
        AlertViewCtrlShow(FSLocalizedForKey(@"kWarning"), FSLocalizedForKey(@"kRMSNoAccess"));
        return;
    }
    _extensionsManager.hiddenMoreMenu = YES;
    
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    
    if (item.tag == TAG_ITEM_IMPORTCOMMENT) {
        selectDestination.fileOperatingMode = FileListMode_Import;
        selectDestination.expectFileType = @[ @"fdf", @"xfdf"];

    }else if (item.tag == TAG_ITEM_EXPORTCOMMENT){
        selectDestination.fileOperatingMode = FileListMode_SaveTo;
    }
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        NSString *filePath = [destinationFolder lastObject];
        if (item.tag == TAG_ITEM_IMPORTCOMMENT) {
            FSFDFDoc *importFdoc = [[FSFDFDoc alloc] initWithPath:filePath];
            if ([importFdoc isEmpty]) {
                [controller dismissViewControllerAnimated:YES completion:^{
                    AlertViewCtrlShow(FSLocalizedForKey(@"kWarning"), FSLocalizedForKey(@"kNoCommentsImport"));
                }];
                return;
            }
            FSPDFDoc *doc = self.pdfViewCtrl.currentDoc;
            int type = 0x0002;
            FSRange *range = [[FSRange alloc] initWithStart_index:0 end_index:[self.pdfViewCtrl.currentDoc getPageCount] filter:FSRangeAll];
            
            void (^importFailed)(void) = ^{
                [controller dismissViewControllerAnimated:YES completion:^{
                    AlertViewCtrlShow(FSLocalizedForKey(@"kError"), FSLocalizedForKey(@"kFailedImportData"));
                }];
            };
            @try {
                int pageCount = [doc getPageCount];
                int beforeAnnotCount = 0;
                for (int i = 0; i < pageCount; i++) {
                    FSPDFPage *page = [doc getPage:i];
                    beforeAnnotCount += [page getAnnotCount];
                }
                BOOL flag = [doc importFromFDF:importFdoc types:type page_range:range];
                if (flag) {
                    pageCount = [doc getPageCount];
                    int afterAnnotCount = 0;
                    for (int i = 0; i < pageCount; i++) {
                        FSPDFPage *page = [doc getPage:i];
                        afterAnnotCount += [page getAnnotCount];
                        [self.extensionsManager.thumbnailCache removeThumbnailCacheOfPageAtIndex:i];
                    }
                    if (beforeAnnotCount == afterAnnotCount) {
                        [controller dismissViewControllerAnimated:YES completion:^{
                            AlertViewCtrlShow(FSLocalizedForKey(@"kWarning"), FSLocalizedForKey(@"kNoCommentsImport"));
                        }];
                        return;
                    }
                    for (int i = 0; i < [self.pdfViewCtrl.currentDoc getPageCount]; i++) {
                        [self.pdfViewCtrl refresh:i];
                    }
                    
                    self.extensionsManager.isDocModified = YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAnnotLsitUpdate object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAttachmentLsitUpdate object:nil];
                    [controller dismissViewControllerAnimated:YES completion:^{
                        AlertViewCtrlShow(FSLocalizedForKey(@"kSuccess"), FSLocalizedForKey(@"kSuccessImportData"));
                    }];
                }else{
                    importFailed();
                }
            } @catch (NSException *exception) {
                importFailed();
            }
  
        }else{
            [controller dismissViewControllerAnimated:YES completion:^{
                [self inputFileNameWithPath:filePath controller:controller];
            }];
        }
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    
    [selectDestinationNavController fs_setCustomTransitionAnimator:self.extensionsManager.presentedVCAnimator];
    [self.pdfViewCtrl.fs_viewController presentViewController:selectDestinationNavController
                                     animated:YES
                                   completion:nil];
}

- (void)inputFileNameWithPath:(NSString *)path controller:(FileSelectDestinationViewController *)controller{
    InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kInputNewFileName")
                                                                   message:nil
                                                        buttonClickHandler:^(InputAlertView *alertView, NSInteger buttonIndex) {
                                                            if (buttonIndex == 1) {
                                                                FSFDFDoc *exportFdoc = [[FSFDFDoc alloc] initWithType:FSFDFDocFDF];
                                                                FSRange *range = [[FSRange alloc] initWithStart_index:0 end_index:[self.pdfViewCtrl.currentDoc getPageCount] filter:FSRangeAll];
                                                                FSPDFDoc *doc = self.pdfViewCtrl.currentDoc;
                                                                int type = 0x0002;
                                                                void (^saveFdocFailedTip)(void) = ^{
                                                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                                        [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kFailedExportData") actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                                                                            [self inputFileNameWithPath:path controller:controller];
                                                                        }];
                                                                    });
                                                                };
                                                                
                                                                @try {
                                                                    BOOL flag = [doc exportToFDF:exportFdoc types:type page_range:range];
                                                                    if (flag) {
                                                                        NSString *saveFilePath = [NSString stringWithFormat:@"%@/%@.fdf",path, alertView.inputTextField.text];
                                                                        void (^saveFdoc)(void) = ^{
                                                                            BOOL isSuccess = [exportFdoc saveAs:saveFilePath];
                                                                            if (isSuccess) {
                                                                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                                                    [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kSuccessExportData") actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                                                                                        [controller dismissViewControllerAnimated:YES completion:nil];
                                                                                    }];
                                                                                });
                                                                            }else{
                                                                                saveFdocFailedTip();
                                                                            }

                                                                        };
                                                                        NSFileManager *filemanager = [NSFileManager defaultManager];
                                                                        if ([filemanager fileExistsAtPath:saveFilePath]) {
                                                                            UIAlertController *alertController = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kReplace") ] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                                                                                if (index == 1) {
                                                                                    saveFdoc();
                                                                                }
                                                                            }];
                                                                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                                                 [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                            });
                                                                            
                                                                        }else{
                                                                            saveFdoc();
                                                                        }
                                                                    }else{
                                                                        saveFdocFailedTip();
                                                                    }
                                                                } @catch (NSException *exception) {
                                                                    saveFdocFailedTip();
                                                                }
                                                                return ;
                                                            }
                                                            [controller dismissViewControllerAnimated:YES completion:nil];
                                                        } cancelButtonTitle:FSLocalizedForKey(@"kCancel")
                                                         otherButtonTitles:FSLocalizedForKey(@"kOK"), nil];
    inputAlertView.style = TSAlertViewStyleInputText;
    inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
    inputAlertView.usesMessageTextView = NO;
    [inputAlertView show];
}
@end
