/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "TbBaseBar.h"
#import "UIFSDocProviderCallback.h"
#import "FSAnnotHandler.h"

@protocol IGestureEventListener;

@interface FormAnnotHandler : FSAnnotHandler <FSFillerAssistCallback, IAnnotHandler, IDocEventListener, IRotationEventListener> {
    BOOL _isOver;
    BOOL _keyboardShown;
    CGRect _originalRect;
    float _keyboardHeight;
    NSUInteger _inputCharacterLocation;
    __weak TaskServer *_taskServer;
    int _focusedWidgetIndex;
    BOOL _lockKeyBoardPosition;
    BOOL _needRotate;
    dispatch_semaphore_t _lock;
    NSString* _changeText;
    NSString* _lastInputText;
    BOOL _isDel;
}

@property (nonatomic, strong) NSTimer *formTimer;
@property (nonatomic, weak) FSTimer *formTimerCallback;
@property (nonatomic, assign) BOOL hasFormChanged;
@property (nonatomic, assign) BOOL editFormControlNeedTextInput;
@property (nonatomic, assign) BOOL editFormControlNeedSetCursor;
@property (nonatomic, strong) UITextView *hiddenTextField;
@property (nonatomic, assign) CGRect currentEditRect;
@property (nonatomic, copy) NSString *lastText;
@property (nonatomic, strong) TbBaseBar *formNaviBar;
@property (nonatomic, strong) TbBaseBar *textFormNaviBar;
@property (nonatomic, strong) FSFiller *formFiller;
@property (nonatomic, strong) UIView *formnaviBackView;

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager;
- (TbBaseBar *)buildFormNaviBar;
- (void)endTextInputWithExit:(BOOL)exit;
- (void)setEnableHighlightForm:(BOOL)enableHighlightForm;
- (void)setHighlightFormColor:(UIColor *)highlightFormColor;
@end
