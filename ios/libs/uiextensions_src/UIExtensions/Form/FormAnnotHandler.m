/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FormAnnotHandler.h"

#import "UIExtensionsConfig+private.h"

static const CGFloat DEFAULT_INPUTVIEW_HEIGHT = 49;

@interface CustomInputAccessoryView : UIView

@property CGFloat bottomConstraintValue;

@end

@implementation CustomInputAccessoryView

- (instancetype)init{
    self = [super init];
    if (self) {
        float screenWidth = [UIScreen mainScreen].bounds.size.width;
        self.frame = CGRectMake(0, 0, screenWidth, DEFAULT_INPUTVIEW_HEIGHT);
        
        _bottomConstraintValue = 0;
    }
    return self;
}

-(CGSize)intrinsicContentSize{
    CGSize size = [super intrinsicContentSize];
    float newHeight = _bottomConstraintValue ? DEFAULT_INPUTVIEW_HEIGHT + _bottomConstraintValue : DEFAULT_INPUTVIEW_HEIGHT;
    return CGSizeMake(size.width, newHeight);
}

-(void)layoutMarginsDidChange {
    [super layoutMarginsDidChange];
    _bottomConstraintValue = self.layoutMargins.bottom;
    [self invalidateIntrinsicContentSize];
}
@end
@interface FormAnnotHandler () <UITextViewDelegate, IGestureEventListener>

@property (nonatomic, assign) NSInteger annotCountTag;
@end

@implementation FormAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super initWithUIExtensionsManager:extensionsManager];
    if (self) {
        [self.extensionsManager registerRotateChangedListener:self];
        _taskServer = self.extensionsManager.taskServer;
        _lock = dispatch_semaphore_create(1);
        _keyboardHeight = 0;
        _formFiller = nil;
        _focusedWidgetIndex = -1;
        _inputCharacterLocation = 0;
        _needRotate = NO;
        _annotCountTag = -1;
        
        if (!self.extensionsManager.config.defaultSettings.disableFormNavigationBar) {
            self.formNaviBar = [self buildFormNaviBar];
            [self.pdfViewCtrl addSubview:self.formNaviBar.contentView];
            [self.formNaviBar.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(self.pdfViewCtrl);
                make.height.mas_equalTo(49);
                if (@available(iOS 11.0, *)) {
                    make.bottom.mas_equalTo(self.pdfViewCtrl.mas_safeAreaLayoutGuideBottom);
                } else {
                    make.bottom.mas_equalTo(self.pdfViewCtrl.mas_bottom);
                }
            }];
            [self.formNaviBar.contentView setHidden:YES];
            
            if (IS_IPHONE_OVER_TEN) {
                _formnaviBackView = [[UIView alloc] init];
                _formnaviBackView.backgroundColor = self.formNaviBar.contentView.backgroundColor;
                [self.formNaviBar.contentView insertSubview:_formnaviBackView atIndex:0 ];
                [_formnaviBackView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.left.right.mas_equalTo(self.pdfViewCtrl);
                    make.height.mas_equalTo(70);
                }];
            }
        }

        self.hiddenTextField = [[UITextView alloc] init];
        self.hiddenTextField.hidden = YES;
        self.hiddenTextField.delegate = self;
        self.hiddenTextField.text = @"";
        self.lastText = @"";
        
        self.hiddenTextField.inputAccessoryView = [self inputAccessoryView];
        
        [self.pdfViewCtrl addSubview:self.hiddenTextField];
        _lockKeyBoardPosition = NO;
        
        [self.extensionsManager registerGestureEventListener:self];
    }
    return self;
}

- (void)dealloc{
    [self.extensionsManager unregisterRotateChangedListener:self];
}

-(FSFieldType)getFormAnnotFieldType:(FSAnnot *)annot{
    if (annot.getType == FSAnnotWidget) {
        FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
        FSField *field = [widget getField];
        FSFieldType fieldType = [field getType];
        return  fieldType;
    }
    return FSFieldTypeUnknown;
}

- (void)setFocus:(FSControl *)control isHidden:(BOOL)hidden {
    if (control && ![control isEmpty]) {
        @try {
            [_formFiller setFocus:control];
        } @catch (NSException *exception) {
        }
    } else {
        @try {
            [self killFocus];
        } @catch (NSException *exception) {
        }
    }
    [self.formNaviBar.contentView setHidden:hidden];
}

- (void)killFocus{
    #if _MAC_CATALYST_
        self.extensionsManager.form_wait_catalyst = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self.formFiller killFocus];
            dispatch_semaphore_signal(self.extensionsManager.form_wait_catalyst);
        });
        dispatch_semaphore_wait(self.extensionsManager.form_wait_catalyst, DISPATCH_TIME_FOREVER);
    #else
        [_formFiller killFocus];
    #endif
}

- (BOOL)canNavigateTo:(FSControl *)control {
    if (![control isEmpty]) {
        FSField *field = [control getField];
        unsigned int fflags = [field getFlags];
        FSFieldType fieldType = [[control getField] getType];
        BOOL bRet = fieldType == FSFieldTypePushButton || fieldType == FSFieldTypeSignature || fflags & FSFieldFlagReadOnly;
        return !bRet;
    }
    else{
        return NO;
    }
}

- (BOOL)canShowKeybord:(FSControl *)control {
    if (_MAC_CATALYST_) return NO;
    FSField *field = [control getField];
    FSFieldType fieldType = [field getType];
    return FSFieldTypeTextField == fieldType || (FSFieldTypeComboBox == fieldType && ([field getFlags] & FSFieldFlagComboEdit));
}

- (UIView *)inputAccessoryView{
    if (self.extensionsManager.config.defaultSettings.disableFormNavigationBar) return nil;
    self.textFormNaviBar = [self buildFormNaviBar];
    if (IS_IPHONE_OVER_TEN) {
        CustomInputAccessoryView *backview = [[CustomInputAccessoryView alloc] init];
        backview.translatesAutoresizingMaskIntoConstraints = NO;
        self.textFormNaviBar.contentView.frame = CGRectMake(0, 0, self.pdfViewCtrl.frame.size.width, 49);
        [backview addSubview:self.textFormNaviBar.contentView];
        return backview;
    }else{
        return self.textFormNaviBar.contentView;
    }
}

- (TbBaseBar *)buildFormNaviBar {
    @weakify(self);
    CGRect screenFrame = self.pdfViewCtrl.bounds;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        screenFrame = CGRectMake(0, 0, screenFrame.size.height, screenFrame.size.width);
    }

    TbBaseBar *formNaviBar = [[TbBaseBar alloc] init];
    formNaviBar.top = NO;
    formNaviBar.contentView.frame = CGRectMake(0, screenFrame.size.height - 49, screenFrame.size.width, 49);
    formNaviBar.contentView.backgroundColor = NormalBarBackgroundColor;

    UIImage *prevImg = [UIImage imageNamed:@"formfill_pre_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    TbBaseItem *prevItem = [TbBaseItem createItemWithImage:prevImg background:nil];
    prevItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        FSPDFDoc *doc = [self.pdfViewCtrl getDoc];
        int curPageIndex = -1;
        if (self.extensionsManager.currentAnnot)
            curPageIndex = [self.extensionsManager.currentAnnot pageIndex];
        else
            curPageIndex = [self.pdfViewCtrl getCurrentPage];
        FSPDFPage *page = [doc getPage:curPageIndex];
        int pageCount = [doc getPageCount];
        int annotCount = [page getAnnotCount];
        BOOL canShowKeybord = NO;
        int savedPos = self->_focusedWidgetIndex;
        int savedPageIndex = curPageIndex;
        int prePos = self->_focusedWidgetIndex < 0 ? 0 : self->_focusedWidgetIndex - 1;
        while (true) {
            if (prePos == -1) {
                if (curPageIndex == 0)
                    curPageIndex = pageCount - 1;
                else
                    curPageIndex--;

                page = [doc getPage:curPageIndex];
                annotCount = [page getAnnotCount];
                prePos = annotCount - 1;
                if (annotCount == 0)
                    continue;
            }
            if (savedPageIndex == curPageIndex && savedPos == prePos)
                break;

            FSAnnot *annot = [page getAnnot:prePos];
            if ([annot getType] == FSAnnotWidget) {
                FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
                if ([self canNavigateTo:[widget getControl]]) {
                    canShowKeybord = [self canShowKeybord:[widget getControl]];
                    FSRectF *rect = [annot getRect];


                    [self.extensionsManager setCurrentAnnot:widget];
                    [self setFocus:[widget getControl] isHidden:NO];
                    if (canShowKeybord) {
                        [self gotoFormField];
                    } else {
                        if (![self.pdfViewCtrl isContinuous]) {
                            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:curPageIndex];
                            CGPoint pvPt = CGPointZero;
                            pvPt.x = pvRect.origin.x - (self.pdfViewCtrl.frame.size.width - pvRect.size.width) / 2;
                            pvPt.y = pvRect.origin.y - (self.pdfViewCtrl.frame.size.height - pvRect.size.height) / 2;
                            FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvPt pageIndex:curPageIndex];
                            [self.pdfViewCtrl gotoPage:curPageIndex withDocPoint:pdfPt animated:YES];
                        }else{
                            PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
                            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:curPageIndex];
                            
                            float orignY = pvRect.origin.y;
                            BOOL isTwoPagesMode = (pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE);
                            if (!isTwoPagesMode) {
                                orignY = pvRect.origin.y - CGRectGetHeight(self.pdfViewCtrl.frame)/2 ;
                            }
                            CGPoint newOrign = CGPointMake(pvRect.origin.x - CGRectGetWidth(self.pdfViewCtrl.frame)/2, orignY);
                            FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:newOrign pageIndex:curPageIndex];
                            [self.pdfViewCtrl gotoPage:curPageIndex withDocPoint:pdfPt animated:YES];
                        }
                        
                        
                    }
                    self->_focusedWidgetIndex = prePos;
                    break;
                }
            }
            prePos--;
        }
        
        if (canShowKeybord || (_MAC_CATALYST_ && self.editFormControlNeedTextInput)) {
            
            [self beginInput];
        } else
            [self endTextInputWithExit:NO];
    };
    [formNaviBar addItem:prevItem displayPosition:Position_CENTER];

    UIImage *nextImg = [UIImage imageNamed:@"formfill_next_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    TbBaseItem *nextItem = [TbBaseItem createItemWithImage:nextImg background:nil];
    nextItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        FSPDFDoc *doc = [self.pdfViewCtrl getDoc];
        int curPageIndex = -1;
        if (self.extensionsManager.currentAnnot)
            curPageIndex = [self.extensionsManager.currentAnnot pageIndex];
        else
            curPageIndex = [self.pdfViewCtrl getCurrentPage];
        FSPDFPage *page = [doc getPage:curPageIndex];
        int pageCount = [doc getPageCount];
        int annotCount = [page getAnnotCount];
        BOOL canShowKeybord = NO;
        int i = self->_focusedWidgetIndex;
        int savedPos = self->_focusedWidgetIndex;
        int savedPageIndex = curPageIndex;
        int next = i + 1;
        while (true) {
            if (next == annotCount) {
                if (curPageIndex == pageCount - 1)
                    curPageIndex = 0;
                else
                    curPageIndex++;

                page = [doc getPage:curPageIndex];
                annotCount = [page getAnnotCount];
                next = 0;
                if (annotCount == 0)
                    continue;
            }
            if (savedPageIndex == curPageIndex && next == savedPos)
                break;
            FSAnnot *annot = [page getAnnot:next];
            if ([annot getType] == FSAnnotWidget) {
                FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
                if ([self canNavigateTo:[widget getControl]]) {
                    canShowKeybord = [self canShowKeybord:[widget getControl]];
                    FSRectF *rect = [annot getRect];

                    [self.extensionsManager setCurrentAnnot:widget];
                    
                    [self setFocus:[widget getControl] isHidden:NO];
                    if (canShowKeybord) {
                        [self gotoFormField];
                    } else {
                        if (![self.pdfViewCtrl isContinuous]) {
                            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:curPageIndex];
                            CGPoint pvPt = CGPointZero;
                            pvPt.x = pvRect.origin.x - (self.pdfViewCtrl.frame.size.width - pvRect.size.width) / 2;
                            pvPt.y = pvRect.origin.y - (self.pdfViewCtrl.frame.size.height - pvRect.size.height) / 2;
                            FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvPt pageIndex:curPageIndex];
                            [self.pdfViewCtrl gotoPage:curPageIndex withDocPoint:pdfPt animated:YES];
                        }else{
                            PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
                            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:curPageIndex];
                            
                            float orignY = pvRect.origin.y;
                            BOOL isTwoPagesMode = (pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE);
                            if (!isTwoPagesMode) {
                                orignY = pvRect.origin.y - CGRectGetHeight(self.pdfViewCtrl.frame)/2 ;
                            }
                            CGPoint newOrign = CGPointMake(pvRect.origin.x - CGRectGetWidth(self.pdfViewCtrl.frame)/2, orignY);
                            FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:newOrign pageIndex:curPageIndex];
                            [self.pdfViewCtrl gotoPage:curPageIndex withDocPoint:pdfPt animated:YES];
                        }
                        
                    }
                    self->_focusedWidgetIndex = next;
                    break;
                }
            }
            next++;
        }

        if (canShowKeybord || (_MAC_CATALYST_ && self.editFormControlNeedTextInput)) {
            [self beginInput];
        } else
            [self endTextInputWithExit:NO];
    };
    [formNaviBar addItem:nextItem displayPosition:Position_CENTER];

    TbBaseItem *resetItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kReset") imageNormal:nil imageSelected:nil imageDisable:nil background:nil imageTextRelation:0];
    resetItem.textColor = BlackThemeTextColor;
    resetItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        FSPDFDoc *doc = [self.pdfViewCtrl getDoc];
        
        int curPageIndex = [self.pdfViewCtrl getCurrentPage];
        if (self.extensionsManager.currentPageIndex >= 0 ) {
            curPageIndex = self.extensionsManager.currentPageIndex;
        }
        
        FSPDFPage *page = [doc getPage:curPageIndex];
        FSAnnot *annot = [page getAnnot:self->_focusedWidgetIndex];
        if ([annot getType] == FSAnnotWidget) {
            FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
            FSField *field = [widget getField];

            @try {
                [self killFocus];
            } @catch (NSException *exception) {
            }
            
            [field reset];
            [self setFocus:[widget getControl] isHidden:NO];
            self.hiddenTextField.text = @"";
            CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
            newRect = CGRectInset(newRect, -30, -30);
            [self.pdfViewCtrl refresh:newRect pageIndex:annot.pageIndex needRender:NO];
            self.extensionsManager.isDocModified = YES;
        }
    };
    [formNaviBar addItem:resetItem displayPosition:Position_CENTER];

    TbBaseItem *doneItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kDone") imageNormal:nil imageSelected:nil imageDisable:nil background:nil imageTextRelation:0];
    doneItem.textColor = BlackThemeTextColor;
    doneItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
//        [self setFocus:nil isHidden:YES];
//        self->_focusedWidgetIndex = -1;
        [self.extensionsManager setCurrentAnnot:nil];
    };
    [formNaviBar addItem:doneItem displayPosition:Position_CENTER];
    
    NSArray<UIView *> *tmpItemViews = @[ prevItem.contentView, nextItem.contentView, resetItem.contentView ,doneItem.contentView];
    UIView *superview = formNaviBar.contentView;
    UIView *lastIntervalView = nil;
    int count = (int)tmpItemViews.count;
    
    for (int i = 0; i < count + 1; i++) {
        UIView *intervalView = [[UIView alloc] init];
        [superview addSubview:intervalView];
        
        if (i == 0) {
            [intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.bottom.equalTo(superview);
            }];
        }
        else{
            UIView *itemView = tmpItemViews[i-1];
            [itemView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastIntervalView.mas_right);
                make.centerY.equalTo(superview);
                make.size.sizeOffset(itemView.frame.size);
            }];
            
            [intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(itemView.mas_right);
                make.bottom.equalTo(lastIntervalView);
                make.width.height.equalTo(lastIntervalView);
                if (i == count) {
                    make.right.equalTo(superview);
                }
            }];
        }
        lastIntervalView = intervalView;
    }

    return formNaviBar;
}

- (UIColor *)borderColorForSelectedAnnot:(FSAnnot *)annot {
    return  self.extensionsManager.config.defaultSettings.commonlyUsed.color;
}

#pragma IAnnotHandler

- (FSAnnotType)getType {
    return FSAnnotWidget;
}

- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    if ([annot isEmpty]) return NO;
    FSAnnot *hitAnnot = nil;
    @try {
        hitAnnot = [[annot getPage] getAnnotAtPoint:point tolerance:5];
    } @catch (NSException *exception) {
        return NO;
    }
    if (hitAnnot && ![hitAnnot isEmpty] && [hitAnnot isEqualToAnnot:annot]) {
        FSWidget *widget = [[FSWidget alloc] initWithAnnot:hitAnnot];
        if (widget && ![widget isEmpty]) {
            FSField *field = [widget getField];
            if (field && ![field isEmpty] && ([field getFlags] & FSFieldFlagReadOnly))
                return NO;
            return YES;
        }
    }
    return NO;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    [super onAnnotSelected:annot];
    _isOver = NO;
}

- (void)onAnnotDeselected:(FSAnnot *)annot {
    if ([annot isEqualToAnnot:self.annot]) {
        [self endTextInputWithExit:NO];
        self->_focusedWidgetIndex = -1;
        [self setFocus:nil isHidden:YES];
    }
    [super onAnnotDeselected:annot];
    if (annot == nil) {
        [self setFocus:nil isHidden:YES];
    }
    [self.extensionsManager removeThumbnailCacheOfPageAtIndex:annot.pageIndex];
}

- (BOOL)addAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (annot.getType != FSAnnotWidget) {
        return [super addAnnot:annot addUndo:addUndo];;
    }
    [self.extensionsManager removeThumbnailCacheOfPageAtIndex:annot.pageIndex];
    FSPDFPage *page = [annot getPage];
    if (addUndo) {
        FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:annot];
        [self.extensionsManager addUndoItem:[UndoItem itemForUndoAddAnnotWithAttributes:attributes page:page annotHandler:self]];
    }
    if (!_formFiller) {
        [self onDocOpened:self.pdfViewCtrl.currentDoc error:FSErrSuccess];
    }
//    self.extensionsManager.isDocModified = YES;
    [self.extensionsManager onAnnotAdded:page annot:annot];
    [self refreshPageForAnnot:annot reRenderPage:YES];
    return YES;
}

- (BOOL)modifyAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (annot.getType != FSAnnotWidget) {
        return [super modifyAnnot:annot addUndo:addUndo];;
    }
    if (![annot canModify]) {
        return NO;
    }
    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return NO;
    }
    if (addUndo) {
        annot.modifiedDate = [NSDate date];
        [self.extensionsManager addUndoItem:[UndoItem itemForUndoModifyAnnotWithOldAttributes:self.attributesBeforeModify newAttributes:[FSAnnotAttributes attributesWithAnnot:annot] pdfViewCtrl:self.pdfViewCtrl page:page annotHandler:self]];
    }
    self.extensionsManager.isDocModified = YES;
    [self refreshPageForAnnot:annot reRenderPage:YES];
    return YES;
}

- (BOOL)removeAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (annot.getType != FSAnnotWidget) {
        return [super removeAnnot:annot addUndo:addUndo];;
    }
    [self hideMenu];
    if (self.extensionsManager.propertyBar.isShowing) {
        [self.extensionsManager.propertyBar dismissPropertyBar];
    }
    
    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return NO;
    }
    
    int pageIndex = annot.pageIndex;
    CGRect rect = [self getAnnotRect:annot];
    rect = UIEdgeInsetsInsetRect(rect, self.annotRefreshRectEdgeInsets);
    rect = CGRectIntersection(rect, [self.pdfViewCtrl getPageView:pageIndex].bounds);
    
    [self.extensionsManager onAnnotWillDelete:page annot:annot];
    if (addUndo) {
        FSAnnotAttributes *attributes = self.attributesBeforeModify ?: [FSAnnotAttributes attributesWithAnnot:annot];
        [self.extensionsManager addUndoItem:[UndoItem itemForUndoDeleteAnnotWithAttributes:attributes page:page annotHandler:self]];
    }
    
    [self.pdfViewCtrl lockRefresh];
    FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
    FSControl *control = [widget getControl];
    if (![control isEmpty]) {
        FSField *field = [control getField];
        if (![field isEmpty]) {
            if ([annot isEqualToAnnot:self.annot]) {
                [self setFocus:nil isHidden:YES];
            }
            
            if ([field getType] == FSFieldTypeSignature) {
                FSSignature *sig = [[FSSignature alloc] initWithField:field];
                [self.pdfViewCtrl.currentDoc removeSignature:sig];
            }else{
                FSForm *form = [[FSForm alloc] initWithDocument:self.pdfViewCtrl.currentDoc];
                FSControl *control = [widget getControl];
                if (![control isEmpty] && [field getControlCount] > 1) {
                    [form removeControl:control];
                }else{
                    [form removeField:field];
                }
            }
        }
    }

    [self.pdfViewCtrl unlockRefresh];
    [self.pdfViewCtrl refresh:rect pageIndex:pageIndex];
    
    [self.extensionsManager onAnnotDeleted:page annot:annot];
    
    self.attributesBeforeModify = nil;
    self.annotImage = nil;
    [self.extensionsManager setCurrentAnnot:nil];
    return YES;
}

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(nonnull UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *_Nullable)annot {
    
    if ([[self.pdfViewCtrl currentDoc] isXFA]) {
        return NO;
    }
    
    BOOL docHasSignature = [Utility isDocumentSigned:[self.pdfViewCtrl currentDoc]];
    BOOL allowEdit = docHasSignature ? NO : [Utility canModifyContents:self.pdfViewCtrl];
    
    if (![Utility canFillFormInDocument:[self.pdfViewCtrl currentDoc]] || [annot isEmpty] || ![Utility canAssemble:self.extensionsManager.pdfViewCtrl] || !allowEdit) {
        return NO;
    }
    if ([annot getType] != FSAnnotWidget) {
        return NO;
    }
    FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
    FSField *field = [widget getField];
    if (![field isEmpty]) {
        self.shouldShowMenuOnSelection = YES;
        return [super onPageViewLongPress:pageIndex recognizer:recognizer annot:annot];
    }
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot{
    if (self.shouldShowMenuOnSelection) {
        return [super onPageViewPan:pageIndex recognizer:recognizer annot:annot];
    }
    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSPDFPage *page = [self.pdfViewCtrl.currentDoc getPage:pageIndex];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        @try {
            [_formFiller onLButtonDown:page point:pdfPoint flags:0];
        } @catch (NSException *exception) {
        }
    }else if (recognizer.state == UIGestureRecognizerStateChanged){
        @try {
             [_formFiller onMouseMove:page point:pdfPoint flags:0];
        } @catch (NSException *exception) {
        }
        
    }else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled ){
        @try {
            [_formFiller onLButtonUp:page point:pdfPoint flags:0];
            if (!self.editFormControlNeedSetCursor) {
                [self.extensionsManager setCurrentAnnot:nil];
            }
        } @catch (NSException *exception) {
        }
    }
    return YES;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if (self.shouldShowMenuOnSelection) {
        [self.extensionsManager setCurrentAnnot:nil];
        self.shouldShowMenuOnSelection = NO;
        CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
        FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        if (pageIndex != annot.pageIndex || ![self isHitAnnot:annot point:pdfPoint]) {
            return YES;
        }
    }
    BOOL hasForm = NO;
    @try {
        hasForm = [self.pdfViewCtrl.currentDoc hasForm];
    } @catch (NSException *e) {
    }
    if (!hasForm) {
        return NO;
    }
    BOOL canFillForm = [Utility canFillForm:self.pdfViewCtrl];
    if (!canFillForm) {
        return NO;
    }
    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    if ([self.extensionsManager.currentAnnot isEqualToAnnot:annot]) {
        FSAnnot *hitAnnot = nil;
        FSPDFPage *page = [[self.pdfViewCtrl getDoc] getPage:pageIndex];
        @try {
            hitAnnot = [page getAnnotAtPoint:pdfPoint tolerance:5];
            if (![hitAnnot isEmpty] && hitAnnot.pageIndex != pageIndex) {
                //TODO:
                [self killFocus];
                hitAnnot = [page getAnnotAtPoint:pdfPoint tolerance:5];
            }
        } @catch (NSException *exception) {
        }
        BOOL isSignature = [Utility isSignatureWithAnnot:hitAnnot];
        if (self.annot && ![self.annot isEmpty]) {
            if (![Utility isAnnot:hitAnnot uniqueToAnnot:annot]) {
                [self.extensionsManager setCurrentAnnot:nil];
            }
            BOOL isHitAnnot = [self isHitAnnot:hitAnnot point:pdfPoint];
            if (!isHitAnnot) {
                return YES;
            }
        }
        if ( hitAnnot && ![hitAnnot isEmpty] && hitAnnot.type == FSAnnotWidget && !isSignature) {
            [self onMouseLButtonClick:[self.pdfViewCtrl.currentDoc getPage:pageIndex] point:pdfPoint hitAnnot:hitAnnot isHidden:NO];
            if (self.editFormControlNeedTextInput) {
                {
                    _lockKeyBoardPosition = YES;
                    [self.extensionsManager setCurrentAnnot:hitAnnot];
                    _lockKeyBoardPosition = NO;
                    
                    [self beginInput];
                }
            } else {
                [self.extensionsManager setCurrentAnnot:hitAnnot];
            }
            _focusedWidgetIndex = [hitAnnot getIndex];
            _hasFormChanged = NO;
            _editFormControlNeedSetCursor = NO;

        } else {
            [self onMouseLButtonClick:[self.pdfViewCtrl.currentDoc getPage:pageIndex] point:pdfPoint hitAnnot:hitAnnot isHidden:YES];
            if (isSignature) {
                [self.extensionsManager setCurrentAnnot:hitAnnot];
            }
        }
        return YES;
    } else {
        [self.extensionsManager setCurrentAnnot:annot];
        
        _hasFormChanged = NO;
        _editFormControlNeedSetCursor = NO;
        
        [self onMouseLButtonClick:[self.pdfViewCtrl.currentDoc getPage:pageIndex] point:pdfPoint hitAnnot:annot isHidden:NO];
        
        _focusedWidgetIndex = [annot getIndex];
        
        BOOL needReturn = _isOver;
        if (needReturn) {
            return YES;
        }
        
        if (self.editFormControlNeedTextInput) {
            {
                [self beginInput];
            }
        } else {
            [self endTextInputWithExit:NO];
        }
        
        return _hasFormChanged;
    }
}

- (void)onMouseLButtonClick:(FSPDFPage *)page point:(FSPointF *)point hitAnnot:(FSAnnot *)hitAnnot isHidden:(BOOL)hidden {
    if (!hitAnnot || [hitAnnot isEmpty] ||  [hitAnnot getType] != FSAnnotWidget) {
        return;
    }
    @try {
    #if _MAC_CATALYST_
        self.extensionsManager.form_wait_catalyst = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self.formFiller onLButtonDown:page point:point flags:0];
            [self.formFiller onLButtonUp:page point:point flags:0];
            dispatch_semaphore_signal(self.extensionsManager.form_wait_catalyst);
        });
        dispatch_semaphore_wait(self.extensionsManager.form_wait_catalyst, DISPATCH_TIME_FOREVER);
    #else
        [_formFiller onLButtonDown:page point:point flags:0];
        [_formFiller onLButtonUp:page point:point flags:0];
    #endif

    } @catch (NSException *exception) {
    }
    
    if (!hidden) {
        FSWidget *widget = [[FSWidget alloc] initWithAnnot:hitAnnot];
        FSField *field = [widget getField];
        FSFieldType fieldType = [field isEmpty] ? FSFieldTypeUnknown : [field getType];
        if (FSFieldTypePushButton == fieldType)
            hidden = YES;
    }
    [self.formNaviBar.contentView setHidden:hidden];
    [self.pdfViewCtrl bringSubviewToFront:self.formNaviBar.contentView];
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer annot:(FSAnnot *)annot {
    BOOL canFillForm = [Utility canFillForm:self.pdfViewCtrl];
    if (!canFillForm) {
        return NO;
    }

    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [gestureRecognizer locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    if (pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint]) {
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot{
    if (self.shouldShowMenuOnSelection) {
        [super onDraw:pageIndex inContext:context annot:annot];
    }
}

- (void)showStyle {

    FSWidget *annot = [[FSWidget alloc] initWithAnnot:self.extensionsManager.currentAnnot];
    FSFieldType fieldType = [self getFormAnnotFieldType:annot];
    FSField *field = [annot getField];
    FSControl *control = [annot getControl];
    FSDefaultAppearance *appearance = [control getDefaultAppearance];
    float fontSize = [appearance getText_size];
    FSFont *font = [appearance getFont];
    NSString *fontName = @"";
    if (font && ![font isEmpty]) {
        fontName = [font getName];
    }
    NSArray *colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.extensionsManager.propertyBar setColors:colors];
    if (fieldType == FSFieldTypeTextField) {
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_FONTNAME | PROPERTY_FONTSIZE frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_FONTNAME | PROPERTY_FONTSIZE lock:!annot.canModifyAppearance];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];

        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME stringValue:fontName];

        self.extensionsManager.propertyBar.fontLayout.isAutoFontSize = YES;
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTSIZE floatValue:fontSize];

    }else if (fieldType == FSFieldTypeCheckBox){
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR lock:!annot.canModifyAppearance];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    }else if (fieldType == FSFieldTypeRadioButton){
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_TEXTNAME | PROPERTY_COLOR frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_TEXTNAME | PROPERTY_TEXTINPUT lock:!annot.canModifyAppearance];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_TEXTNAME stringValue:field.getName];
    }else if (fieldType == FSFieldTypeComboBox){
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_FONTNAME | PROPERTY_FONTSIZE | PROPERTY_FORMOPTION | PROPERTY_COLOR frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME | PROPERTY_FONTSIZE | PROPERTY_FORMOPTION | PROPERTY_COLOR lock:!annot.canModifyAppearance];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FORMOPTION value:annot];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME stringValue:fontName];
        self.extensionsManager.propertyBar.fontLayout.isAutoFontSize = YES;
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTSIZE floatValue:fontSize];
    }else if (fieldType == FSFieldTypeListBox){
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_FONTNAME | PROPERTY_FONTSIZE | PROPERTY_FORMOPTION | PROPERTY_COLOR frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME | PROPERTY_FONTSIZE | PROPERTY_FORMOPTION | PROPERTY_COLOR lock:!annot.canModifyAppearance];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FORMOPTION value:annot];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME stringValue:fontName];
        self.extensionsManager.propertyBar.fontLayout.isAutoFontSize = YES;
        [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTSIZE floatValue:fontSize];
    }
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionNone;
    if (![annot.NM hasPrefix:@"FoxitPDFSDK"]) {
        return options;
    }
    if ([Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
        if (annot && ![annot isEmpty]) {
            FSFieldType fieldType = [self getFormAnnotFieldType:annot];
            if (fieldType == FSFieldTypeTextField || fieldType == FSFieldTypeCheckBox || fieldType == FSFieldTypeRadioButton || fieldType == FSFieldTypeListBox || fieldType == FSFieldTypeComboBox){
                options |= FSMenuOptionStyle;
            }
        }
    }
    return options;
}

- (void)endOp:(BOOL)needDelay {
    if (!_isOver) {
        _isOver = YES;
    }
    [self.pdfViewCtrl refresh:CGRectZero pageIndex:[self.pdfViewCtrl getCurrentPage]];
}

- (void)beginInput{
    #if !_MAC_CATALYST_
        [self.formNaviBar.contentView setHidden:YES];
    #endif
    
    [self.hiddenTextField becomeFirstResponder];
    self.hiddenTextField.text = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardWillHideNotification
                                            object:nil];
    #if _MAC_CATALYST_
        _keyboardHeight = self.textFormNaviBar.contentView.fs_height;
        [self gotoFormField];
    #endif
    
}

- (void)endTextInputWithExit:(BOOL)exit {
    if (self.hiddenTextField) {
        _isOver = YES;
        [self.hiddenTextField resignFirstResponder];
        _isOver = NO;
        self.lastText = @"";
        self.hiddenTextField.text = @"";
        if (!exit) {
            [self.pdfViewCtrl refresh:CGRectZero pageIndex:[self.pdfViewCtrl getCurrentPage]];
        }
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        
        #if _MAC_CATALYST_
            [self.pdfViewCtrl setBottomOffset:0];
        #endif
    }
}

- (void)setEnableHighlightForm:(BOOL)enableHighlightForm{
    [_formFiller highlightFormFields:self.extensionsManager.enableHighlightForm];
    [self setHighlightFormColor:self.extensionsManager.highlightFormColor];
}

- (void)setHighlightFormColor:(UIColor *)highlightFormColor{
    if (self.extensionsManager.enableHighlightForm) {
        if (highlightFormColor) [_formFiller setHighlightColor:highlightFormColor.fs_argbValue];
    }
}


#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if(range.length > 0 )
    {
        _isDel = YES;
        for(int i=0; i<range.length; i++)
            [_formFiller onChar:FSFillerVkeyBack flags:0];
    }
    else
    {
        if([text isEqualToString:@""]) {
            _isDel = YES;
            [_formFiller onChar:FSFillerVkeyBack flags:0];
        }
    }
    
    _changeText = text;
    _lastText = textView.text;
    _inputCharacterLocation = range.location;
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    if(_inputCharacterLocation == 0xFFFF || _changeText == nil) return;
    _inputCharacterLocation = 0xFFFF;
    
    self.extensionsManager.isDocModified = YES;
    
    if (!_isDel && textView.text.length > 0 && textView.text.length == _lastText.length) {
        for(int i = 0; i < _changeText.length; i ++) {
            [_formFiller onChar:FSFillerVkeyBack flags:0];
        }
        
        _changeText = [textView.text substringFromIndex:(textView.text.length - _changeText.length)];
    } else if (!_isDel && textView.text.length > _lastText.length && ![[textView.text substringToIndex:_lastText.length] isEqual:_lastText]) {
        for(int i = 0; i < textView.text.length; i ++) {
            [_formFiller onChar:FSFillerVkeyBack flags:0];
        }
        _changeText = textView.text;
    } else if (!_isDel && textView.text.length > _lastText.length && [[textView.text substringToIndex:_lastText.length] isEqual:_lastText]
               && ![[textView.text substringFromIndex:_lastText.length] isEqual:_changeText]) {
        _changeText = [textView.text substringFromIndex:_lastText.length];
    }
    
    NSString *other = @"➋➌➍➎➏➐➑➒";
    if([other rangeOfString:_changeText].location != NSNotFound) {
        _changeText = [textView.text substringFromIndex:(textView.text.length - _changeText.length)];
    }

    for(NSUInteger i=0; i< _changeText.length; i++)
    {
        unichar cha = [_changeText characterAtIndex:i];
        //Control character as space.
        if(cha == 0x2006) cha = FSFillerVkeySpace;
        [_formFiller onChar:cha flags:0];
    }
    
    _changeText = @"";
    _lastText = @"";
    _isDel = NO;
    if (!self.editFormControlNeedTextInput) {
        [self endOp:YES];
    }
}

#pragma mark -
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"


- (void)gotoFormField {
    
    dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER);
    FSAnnot *dmAnnot = self.extensionsManager.currentAnnot;
    if (dmAnnot && ![dmAnnot isEmpty]) {
        CGPoint oldPvPoint = [self.pdfViewCtrl convertDisplayViewPtToPageViewPt:CGPointMake(0, 0) pageIndex:dmAnnot.pageIndex];
        FSPointF *oldPdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:oldPvPoint pageIndex:dmAnnot.pageIndex];

        CGRect pvAnnotRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:dmAnnot.fsrect pageIndex:dmAnnot.pageIndex];
        CGRect dvAnnotRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:pvAnnotRect pageIndex:dmAnnot.pageIndex];
        
        if (DEVICE_iPHONE) {
            CGFloat maxHeight = 300.f;
            if (CGRectGetHeight(dvAnnotRect) > maxHeight) {
                dvAnnotRect.size.height = maxHeight;
            }
        }
        
        PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
        if (_needRotate) {
            CGPoint annotPVOrigin = [self.pdfViewCtrl convertDisplayViewPtToPageViewPt:pvAnnotRect.origin pageIndex:dmAnnot.pageIndex];
            FSPointF *annotPVOriginf = [self.pdfViewCtrl convertPageViewPtToPdfPt:annotPVOrigin pageIndex:dmAnnot.pageIndex];
            [self.pdfViewCtrl gotoPage:dmAnnot.pageIndex withDocPoint:annotPVOriginf animated:YES];
            _needRotate = NO;
            pvAnnotRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:dmAnnot.fsrect pageIndex:dmAnnot.pageIndex];
            dvAnnotRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:pvAnnotRect pageIndex:dmAnnot.pageIndex];
            if ([self.pdfViewCtrl isContinuous]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self gotoFormField];
                });
                dispatch_semaphore_signal(_lock);
                return;
            }
        }
        
        CGFloat dvAnnotMaxYExtra = CGRectGetHeight(self.pdfViewCtrl.bounds) - CGRectGetMaxY(dvAnnotRect);
        if (dvAnnotMaxYExtra < _keyboardHeight) {
            float dvOffsetY = _keyboardHeight - fabs(dvAnnotMaxYExtra) + 20;
            
            if (dvOffsetY + CGRectGetHeight(pvAnnotRect) > CGRectGetHeight(self.pdfViewCtrl.bounds)) {
                dvOffsetY =  CGRectGetHeight(self.pdfViewCtrl.bounds) - _keyboardHeight - 88;
            }
            CGRect offsetRect = CGRectMake(0, 0, 100, dvOffsetY);

            CGRect pvRect = [self.pdfViewCtrl convertDisplayViewRectToPageViewRect:offsetRect pageIndex:dmAnnot.pageIndex];
            FSRectF *pdfRect = [self.pdfViewCtrl convertPageViewRectToPdfRect:pvRect pageIndex:dmAnnot.pageIndex];
            float pdfOffsetY = pdfRect.top - pdfRect.bottom;
           
            FSPointF *jumpPdfPoint = [[FSPointF alloc] init];
            [jumpPdfPoint set:oldPdfPoint.x y:oldPdfPoint.y - pdfOffsetY];
            if (pageLayoutMode == PDF_LAYOUT_MODE_SINGLE || pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE) {
                [self.pdfViewCtrl setBottomOffset:dvOffsetY];
            } else if ([self.pdfViewCtrl isContinuous]) {
                int currentPage = [self.pdfViewCtrl getCurrentPage];
                if (currentPage == currentPage - 1) {
                    FSRectF *fsRect = [[FSRectF alloc] init];
                    [fsRect setLeft:0];
                    [fsRect setBottom:pdfOffsetY];
                    [fsRect setRight:pdfOffsetY];
                    [fsRect setTop:0];
                    float tmpPvOffset = [self.pdfViewCtrl convertPdfRectToPageViewRect:fsRect pageIndex:dmAnnot.pageIndex].size.width;
                    CGRect tmpPvRect = CGRectMake(0, 0, 10, tmpPvOffset);
                    CGRect tmpDvRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:tmpPvRect pageIndex:dmAnnot.pageIndex];
                    [self.pdfViewCtrl setBottomOffset:tmpDvRect.size.height];
                } else {
                    [self.pdfViewCtrl setBottomOffset:dvOffsetY];
//                    [self.pdfViewCtrl gotoPage:dmAnnot.pageIndex withDocPoint:jumpPdfPoint animated:YES];
                }
            }
        } else {
            CGPoint dvPt = CGPointZero;
            dvPt.x = dvAnnotRect.origin.x - (self.pdfViewCtrl.frame.size.width - dvAnnotRect.size.width) / 2;
            dvPt.y = dvAnnotRect.origin.y - (self.pdfViewCtrl.frame.size.height - dvAnnotRect.size.height) / 2;
            CGPoint pvPt = [self.pdfViewCtrl convertDisplayViewPtToPageViewPt:dvPt pageIndex:dmAnnot.pageIndex];
            FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvPt pageIndex:dmAnnot.pageIndex];
            if (![self.pdfViewCtrl isContinuous]) {
                [self.pdfViewCtrl gotoPage:dmAnnot.pageIndex withDocPoint:pdfPt animated:YES];
            }else {
                FSRectF *rect = dmAnnot.getRect;
                int curPageIndex = dmAnnot.pageIndex;
                CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:curPageIndex];
                
                float orignY = pvRect.origin.y;
                BOOL isTwoPagesMode = (pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE);
                if (!isTwoPagesMode) {
                    orignY = pvRect.origin.y - CGRectGetHeight(self.pdfViewCtrl.frame)/2 ;
                }
                CGPoint newOrign = CGPointMake(pvRect.origin.x - CGRectGetWidth(self.pdfViewCtrl.frame)/2, orignY);
                FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:newOrign pageIndex:curPageIndex];
                [self.pdfViewCtrl gotoPage:curPageIndex withDocPoint:pdfPt animated:YES];
            }
        }
    }
    dispatch_semaphore_signal(_lock);
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    if (_keyboardShown)
        return;
    _keyboardShown = YES;
    NSDictionary *info = [aNotification userInfo];
    // Get the frame of the keyboard.
    NSValue *frame = nil;
    frame = [info objectForKey:UIKeyboardBoundsUserInfoKey];
    CGRect keyboardFrame = [frame CGRectValue];
    _keyboardHeight = keyboardFrame.size.height;
    [self gotoFormField];
}

- (void)keyboardWasHidden:(NSNotification *)aNotification {
    _keyboardShown = NO;
    [self endOp:YES];
    FSAnnot *dmAnnot = self.extensionsManager.currentAnnot;
    if (dmAnnot && ![dmAnnot isEmpty] && !_lockKeyBoardPosition) {
        PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
        if (pageLayoutMode == PDF_LAYOUT_MODE_SINGLE || dmAnnot.pageIndex == [self.pdfViewCtrl.currentDoc getPageCount] - 1 || pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE) {
            [self.pdfViewCtrl setBottomOffset:0];
        }
        else if ([self.pdfViewCtrl isContinuous]) {
            [self.pdfViewCtrl setBottomOffset:0];
        }
    }
}

- (void)setFormTimer:(int)uElapse lpTimerFunc:(FSTimer *)lpTimerFunc {
    [self killFormTimer];
    self.formTimer = [NSTimer scheduledTimerWithTimeInterval:(float) uElapse / (float) 1000
                                                      target:self
                                                    selector:@selector(handleFormTimer:)
                                                    userInfo:nil
                                                     repeats:YES];
    self.formTimerCallback = lpTimerFunc;
}

- (void)killFormTimer {
    self.formTimerCallback = nil;
    [self.formTimer invalidate];
    self.formTimer = nil;
}

- (void)handleFormTimer:(NSTimer *)timer {
    if (_formTimerCallback) {
        [_formTimerCallback onTimer:10];
    }
}

#pragma mark <FSFillerAssistCallback>

- (void)refresh:(FSPDFPage *)page rect:(FSRectF *)pdfRect {
    void (^block) (void) = ^{
        if (self.annotCountTag > 0) {
             int annotCount = [page getAnnotCount];
            if (self.annotCountTag != annotCount) {
               [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAnnotLsitUpdate object:nil];
                self.annotCountTag = -1;
            }
        }
        int pageIndex = [page getIndex];
        CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:pdfRect pageIndex:pageIndex];
        [self.pdfViewCtrl refresh:rect pageIndex:pageIndex];
    };

    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            block();
        });
    }
    _hasFormChanged = YES;
}

- (BOOL)setTimer:(int)elapse timer:(FSTimer *)timerFunc out_timer_id:(int *)out_timer_id {
    [self setFormTimer:elapse lpTimerFunc:timerFunc];
    *out_timer_id = 10;
    return YES;
}

- (BOOL)killTimer:(int)timer_id {
    [self killFormTimer];
    return YES;
}

- (void)focusGotOnControl:(FSControl *)control value:(NSString *)fieldValue {
    FSField *field = [control getField];
    FSFieldType type = [field getType];
    self.editFormControlNeedSetCursor = NO;
    self.editFormControlNeedTextInput = NO;
    _annotCountTag = [[[control getWidget] getPage] getAnnotCount];
    if (type != FSFieldTypeTextField)
        self.editFormControlNeedSetCursor = YES;
    if (type == FSFieldTypeTextField || (type == FSFieldTypeComboBox && ([field getFlags] & FSFieldFlagComboEdit)))
        self.editFormControlNeedTextInput = YES;
}

- (void)focusLostFromControl:(FSControl *)control value:(NSString *)fieldValue {
    _annotCountTag = -1;
    self.editFormControlNeedSetCursor = NO;
    self.editFormControlNeedTextInput = NO;
}

#pragma mark IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    if (nil == _formFiller) {
        BOOL hasForm = NO;
        @try {
            hasForm = [document hasForm];
        } @catch (NSException *e) {
        }
        if (hasForm && [Utility canFillForm:self.pdfViewCtrl]) {
            // creating form filler may block current thread if -[ExActionHandler alert:title:type:icon:] called
            FSForm *form = [[FSForm alloc] initWithDocument:document];
            _formFiller = [[FSFiller alloc] initWithForm:form assist:self];
            [self setEnableHighlightForm:self.extensionsManager.enableHighlightForm];
        }
    }
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    if (self.annot) {
        [self setFocus:nil isHidden:YES];
        [self endTextInputWithExit:YES];
    }
    self->_focusedWidgetIndex = -1;
    if (document)
        _formFiller = nil;
    _formTimerCallback = nil;
}

#pragma mark IRotationEventListener

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (_keyboardShown) {
        [self endTextInputWithExit:NO];
        _needRotate = YES;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    self.hiddenTextField.inputAccessoryView = [self inputAccessoryView];
    if (_needRotate) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self beginInput];
        });
    }
}

@end
