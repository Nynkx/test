/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FormModule.h"
#import "FormToolHandler.h"
#import "FormAnnotHandler.h"

@interface FormModule () {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
    UIButton *currentItemBtn;
}
@property (nonatomic, weak) TbBaseItem *checkBoxItem;
@property (nonatomic, strong) MoreMenuGroup *group;
@property (nonatomic, weak) MoreMenuView *moreMenu;
@property (nonatomic, strong) MoreMenuItem *createFormItem;
@property (nonatomic, strong) MoreMenuItem *exportFormItem;
@property (nonatomic, strong) MoreMenuItem *importFormItem;
@property (nonatomic, strong) MoreMenuItem *resetFormItem;
@end

@implementation FormModule

- (NSString *)getName {
    return @"Form";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];

        FormAnnotHandler *annotHandler = [[FormAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];
        FormToolHandler *toolHandler = [[FormToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        [_extensionsManager registerRotateChangedListener:annotHandler];
        [_pdfViewCtrl registerDocEventListener:annotHandler];
    }
    return self;
}

- (void)loadModule {
    self.moreMenu = _extensionsManager.more;
    self.group = [self.moreMenu getGroup:TAG_GROUP_FORM];
    if (!self.group) {
        self.group = [[MoreMenuGroup alloc] init];
        self.group.title = FSLocalizedForKey(@"kForm");
        self.group.tag = TAG_GROUP_FORM;
        [self.moreMenu addGroup:self.group];
    }

    self.createFormItem = [[MoreMenuItem alloc] init];
    self.createFormItem.tag = TAG_ITEM_CREATEFORM;
    self.createFormItem.callBack = self;
    self.createFormItem.text = FSLocalizedForKey(@"kCreateFormFields");
    [self.moreMenu addMenuItem:self.group.tag withItem:self.createFormItem];
    
    self.exportFormItem = [[MoreMenuItem alloc] init];
    self.exportFormItem.tag = TAG_ITEM_EXPORTFORM;
    self.exportFormItem.callBack = self;
    self.exportFormItem.text = FSLocalizedForKey(@"kExportForm");
    [self.moreMenu addMenuItem:self.group.tag withItem:self.exportFormItem];

    self.importFormItem = [[MoreMenuItem alloc] init];
    self.importFormItem.tag = TAG_ITEM_IMPORTFORM;
    self.importFormItem.callBack = self;
    self.importFormItem.text = FSLocalizedForKey(@"kImportForm");
    [self.moreMenu addMenuItem:self.group.tag withItem:self.importFormItem];

    self.resetFormItem = [[MoreMenuItem alloc] init];
    self.resetFormItem.tag = TAG_ITEM_RESETFORM;
    self.resetFormItem.callBack = self;
    self.resetFormItem.text = FSLocalizedForKey(@"kResetFormFields");
    [self.moreMenu addMenuItem:self.group.tag withItem:self.resetFormItem];
}

- (void)onClick:(MoreMenuItem *)item {
    BOOL isDynamicXFA = [_pdfViewCtrl isDynamicXFA];
    
    if (!isDynamicXFA && ![_pdfViewCtrl.currentDoc hasForm] && item.tag != TAG_ITEM_CREATEFORM) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kNoFormAvailable") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    BOOL docHasSignature = [Utility isDocumentSigned:[_pdfViewCtrl currentDoc]];
    BOOL allowEdit = docHasSignature ? NO : [Utility canModifyContents:_pdfViewCtrl] && ![[_pdfViewCtrl currentDoc] isXFA];
    if (((![Utility canAssemble:_pdfViewCtrl] || !allowEdit) && item.tag == TAG_ITEM_CREATEFORM)) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    NSFileManager *filemanager = [NSFileManager defaultManager];
    _extensionsManager.hiddenMoreMenu = YES;

    BOOL canFillForm = [Utility canFillForm:_pdfViewCtrl];
    
    if (item.tag == TAG_ITEM_CREATEFORM) {
        [_extensionsManager changeState:STATE_CREATEFORM];
    }
    else if (item.tag == TAG_ITEM_EXPORTFORM) {
        if (!canFillForm) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            return;
        }

        FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
        selectDestination.isRootFileDirectory = YES;
        selectDestination.fileOperatingMode = FileListMode_Select;
        [selectDestination loadFilesWithPath:DOCUMENT_PATH];
        selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
            __block void (^inputFileName)(void) = ^() {
                InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kInputNewFileName")
                                                                               message:nil
                                                                    buttonClickHandler:^(UIView *alertView, NSInteger buttonIndex) {
                                                                        if (buttonIndex == 0) {
                                                                            return;
                                                                        }
                                                                        InputAlertView *inputAlert = (InputAlertView *) alertView;
                                                                        NSString *fileName = inputAlert.inputTextField.text;

                                                                        if ([fileName rangeOfString:@"/"].location != NSNotFound) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kIllegalNameWarning") preferredStyle:UIAlertControllerStyleAlert];
                                                                                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                                                                                       style:UIAlertActionStyleCancel
                                                                                                                                     handler:^(UIAlertAction *action) {
                                                                                                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                                                                                                             inputFileName();
                                                                                                                                         });
                                                                                                                                         return;
                                                                                                                                     }];
                                                                                [alertController addAction:cancelAction];
                                                                                [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                            });
                                                                            return;
                                                                        } else if (fileName.length == 0) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                inputFileName();
                                                                            });
                                                                            return;
                                                                        }

                                                                        void (^createXML)(NSString *xmlFilePath) = ^(NSString *xmlFilePath) {
                                                                            NSString *tmpFilePath = [TEMP_PATH stringByAppendingPathComponent:[xmlFilePath lastPathComponent]];
                                                                            BOOL isSuccess = NO;
                                                                            if (isDynamicXFA) {
                                                                                FSXFADoc *xfadoc = [self->_pdfViewCtrl getXFADoc];
                                                                                @try {
                                                                                    isSuccess = [xfadoc exportData:tmpFilePath export_type:FSXFADocExportDataTypeXML];
                                                                                } @catch (NSException *exception) {
                                                                                    NSLog(@"ExportToXML EXCEPTION NAME:%@", exception.name);
                                                                                }
                                                                            }else{
                                                                                FSForm *form = [[FSForm alloc] initWithDocument:self->_pdfViewCtrl.currentDoc];
                                                                                if (nil == [form getCptr])
                                                                                    return;

                                                                                @try {
                                                                                    isSuccess = [form exportToXML:tmpFilePath];
                                                                                } @catch (NSException *exception) {
                                                                                    NSLog(@"ExportToXML EXCEPTION NAME:%@", exception.name);
                                                                                }
                                                                            }

                                                                            if (isSuccess) {
                                                                                double delayInSeconds = 0.4;
                                                                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                                                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                                                                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kExportFormSuccess") preferredStyle:UIAlertControllerStyleAlert];
                                                                                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
                                                                                    [alertController addAction:action];
                                                                                    [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                                    [filemanager moveItemAtPath:tmpFilePath toPath:xmlFilePath error:nil];
                                                                                });
                                                                            } else {
                                                                                double delayInSeconds = 0.4;
                                                                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                                                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                                                                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kExportFormFailed") preferredStyle:UIAlertControllerStyleAlert];
                                                                                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
                                                                                    [alertController addAction:action];
                                                                                    [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                                });
                                                                            }

                                                                        };

                                                                        NSString *xmlFilePath = [destinationFolder[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.xml", fileName]];
                                                                        NSFileManager *fileManager = [[NSFileManager alloc] init];
                                                                        if ([fileManager fileExistsAtPath:xmlFilePath]) {
                                                                            double delayInSeconds = 0.3;
                                                                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                                                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                                                                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                                                                                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                                                                                       style:UIAlertActionStyleCancel
                                                                                                                                     handler:^(UIAlertAction *action) {
                                                                                                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                                                                                                             inputFileName();
                                                                                                                                         });
                                                                                                                                     }];
                                                                                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kReplace")
                                                                                                                                 style:UIAlertActionStyleDefault
                                                                                                                               handler:^(UIAlertAction *action) {
                                                                                                                                   inputFileName = nil;
                                                                                                                                   [fileManager removeItemAtPath:xmlFilePath error:nil];
                                                                                                                                   createXML(xmlFilePath);
                                                                                                                               }];
                                                                                [alertController addAction:cancelAction];
                                                                                [alertController addAction:action];
                                                                                [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                            });
                                                                        } else {
                                                                            inputFileName = nil;
                                                                            createXML(xmlFilePath);
                                                                        }
                                                                    }
                                                                     cancelButtonTitle:FSLocalizedForKey(@"kCancel")
                                                                     otherButtonTitles:FSLocalizedForKey(@"kOK"), nil];
                inputAlertView.style = TSAlertViewStyleInputText;
                inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
                inputAlertView.usesMessageTextView = NO;
                [inputAlertView show];
            };

            [controller dismissViewControllerAnimated:YES completion:^{
                inputFileName();
            }];
        };
        selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
            [controller dismissViewControllerAnimated:YES completion:nil];
        };
        FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
        [selectDestinationNavController fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];
        UIViewController *rootViewController = _pdfViewCtrl.fs_viewController;
        [rootViewController presentViewController:selectDestinationNavController
                                         animated:YES
                                       completion:nil];
    } else if (item.tag == TAG_ITEM_IMPORTFORM) {
        if (!canFillForm) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            return;
        }

        FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
        selectDestination.isRootFileDirectory = YES;
        selectDestination.fileOperatingMode = FileListMode_Import;
        if (isDynamicXFA) {
            selectDestination.expectFileType = @[ @"xml", @"xdp"];
        }else{
            selectDestination.expectFileType = [NSArray arrayWithObject:@"xml"];
        }
        [selectDestination loadFilesWithPath:DOCUMENT_PATH];

        selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
            [controller dismissViewControllerAnimated:YES completion:^{
                if (destinationFolder.count > 0) {
                    BOOL isSuccess = NO;
                    if (isDynamicXFA) {
                        FSXFADoc *xfaDoc = [self->_pdfViewCtrl getXFADoc];
                        @try {
                            isSuccess = [xfaDoc importData:destinationFolder[0]];
                        } @catch (NSException *exception) {
                            NSLog(@"xfaDoc Importdata EXCEPTION NAME:%@", exception.name);
                        }
                    }else{
                        FSForm *form = [[FSForm alloc] initWithDocument:self->_pdfViewCtrl.currentDoc];
                        if (nil == [form getCptr] || [form isEmpty])
                            return;
                        
                        @try {
                            isSuccess = [form importFromXML:destinationFolder[0]];
                        } @catch (NSException *exception) {
                            NSLog(@"ImportFromXML EXCEPTION NAME:%@", exception.name);
                        }
                    }
                    
                    if (isSuccess) {
                        self->_extensionsManager.isDocModified = YES;
                        double delayInSeconds = 0.3;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kImportFormSuccess") preferredStyle:UIAlertControllerStyleAlert];
                            __weak UIExtensionsManager *weakExtMgr = self->_extensionsManager;
                            
                            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                if (isDynamicXFA) {
                                    int page_index = -1;
                                    [weakExtMgr.pdfViewCtrl reloadXFADoc:FSDocProviderCallbackPageViewEventTypeAdded page_index:@[@(page_index)]];
                                }
                            }];
                            [alertController addAction:action];
                            [self->_pdfViewCtrl.fs_viewController  presentViewController:alertController animated:YES completion:nil];
                        });
                    } else {
                        double delayInSeconds = 0.3;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kImportFormFailed") preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:action];
                            [self->_pdfViewCtrl.fs_viewController  presentViewController:alertController animated:YES completion:nil];
                        });
                    }
                }
            }];
            
        };
        selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
            [controller dismissViewControllerAnimated:YES completion:nil];
        };
        FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
        [selectDestinationNavController fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];
        [_pdfViewCtrl.fs_viewController presentViewController:selectDestinationNavController animated:YES completion:nil];
    } else if (item.tag == TAG_ITEM_RESETFORM) {
        if (!canFillForm) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            return;
        }

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kSureToResetFormFields") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo") style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           if (isDynamicXFA) {
                                                               FSXFADoc *xfadoc = [self->_pdfViewCtrl getXFADoc];
                                                               [xfadoc resetForm];
                                                           }else{
                                                               FSForm *form = [[FSForm alloc] initWithDocument:self->_pdfViewCtrl.currentDoc];
                                                               if (nil == [form getCptr])
                                                                   return;
                                                               @try {
                                                                   #if _MAC_CATALYST_
                                                                       self->_extensionsManager.form_wait_catalyst = dispatch_semaphore_create(0);
                                                                       dispatch_async(dispatch_get_global_queue(0, 0), ^{
                                                                           [form reset];
                                                                           dispatch_semaphore_signal(self->_extensionsManager.form_wait_catalyst);
                                                                       });
                                                                       dispatch_semaphore_wait(self->_extensionsManager.form_wait_catalyst, DISPATCH_TIME_FOREVER);
                                                                   #else
                                                                       [form reset];
                                                                   #endif
                                                                   
                                                               } @catch (NSException *exception) {
                                                               }
                                                           }
                                                           self->_extensionsManager.isDocModified = YES;
                                                           [self->_extensionsManager clearThumbnailCachesForPDFAtPath:self->_pdfViewCtrl.filePath];
                                                       }];
        [alertController addAction:cancelAction];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    }
}

- (BOOL)enterCreateFormState {
    BOOL isDynamicXFA = [_pdfViewCtrl isDynamicXFA];
    
    BOOL docHasSignature = [Utility isDocumentSigned:[_pdfViewCtrl currentDoc]];
    BOOL allowEdit = docHasSignature ? NO : [Utility canModifyContents:_pdfViewCtrl] && ![[_pdfViewCtrl currentDoc] isXFA];
    if ((![Utility canAssemble:_pdfViewCtrl] || !allowEdit)) {
        [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") actionBack:nil onCtrl:self->_pdfViewCtrl.fs_viewController];
        return NO;
    }
    if (isDynamicXFA) {
        [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kUnsupportedDocFormat") actionBack:nil onCtrl:self->_pdfViewCtrl.fs_viewController];
        return NO;
    }
    if (![Utility canFillFormInDocument:[_pdfViewCtrl currentDoc]] || ![Utility canAddAnnot:_pdfViewCtrl] || ![Utility canAssemble:_pdfViewCtrl]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") actionBack:nil onCtrl:self->_pdfViewCtrl.fs_viewController];
        });
        return NO;
    }
    if ([_pdfViewCtrl currentDoc].isSigned) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kHadProtect") actionBack:nil onCtrl:self->_pdfViewCtrl.fs_viewController];
        });
        return NO;
    }

    [_extensionsManager.toolSetBar removeAllItems];
    
    TbBaseItem *textFileItem = [TbBaseItem createItemWithImage:ImageNamed(@"create_form_textfield")];
    textFileItem.tag = FSFieldTypeTextField;
    textFileItem.imageSelected = ImageNamed(@"create_form_textfield_selected");
    [_extensionsManager.toolSetBar addItem:textFileItem displayPosition:Position_CENTER];
    @weakify(self);
    textFileItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self onTapClickItem:item];
    };
    
    TbBaseItem *checkBoxItem = [TbBaseItem createItemWithImage:ImageNamed(@"create_form_checkbox")];
    self.checkBoxItem = checkBoxItem;
    self.checkBoxItem.tag = FSFieldTypeCheckBox;
    checkBoxItem.imageSelected = ImageNamed(@"create_form_checkbox_selected");
    [_extensionsManager.toolSetBar addItem:self.checkBoxItem displayPosition:Position_CENTER];
    
    self.checkBoxItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self onTapClickItem:item];
    };
        
    TbBaseItem *radioItem = [TbBaseItem createItemWithImage:ImageNamed(@"create_form_radio")];
    radioItem.tag = FSFieldTypeRadioButton;
    radioItem.imageSelected = ImageNamed(@"create_form_radio_selected");
    [_extensionsManager.toolSetBar addItem:radioItem displayPosition:Position_CENTER];
    radioItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self onTapClickItem:item];
    };
    
    TbBaseItem *comboBoxItem = [TbBaseItem createItemWithImage:ImageNamed(@"create_form_combobox")];
    comboBoxItem.tag = FSFieldTypeComboBox;
    comboBoxItem.imageSelected = ImageNamed(@"create_form_combobox_selected");
    [_extensionsManager.toolSetBar addItem:comboBoxItem displayPosition:Position_CENTER];
    comboBoxItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self onTapClickItem:item];
    };
    
    TbBaseItem *listBoxItem = [TbBaseItem createItemWithImage:ImageNamed(@"create_form_listbox")];
    listBoxItem.tag = FSFieldTypeListBox;
    listBoxItem.imageSelected = ImageNamed(@"create_form_listbox_selected");
    [_extensionsManager.toolSetBar addItem:listBoxItem displayPosition:Position_CENTER];
    listBoxItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self onTapClickItem:item];
    };
    
    TbBaseItem *signatureItem = nil;
    if (_extensionsManager.config.loadSignature) {
        signatureItem = [TbBaseItem createItemWithImage:ImageNamed(@"create_form_signature")];
        signatureItem.tag = FSFieldTypeSignature;
        signatureItem.imageSelected = ImageNamed(@"create_form_signature_selected");
        [_extensionsManager.toolSetBar addItem:signatureItem displayPosition:Position_CENTER];
        signatureItem.enable = _extensionsManager.config.loadSignature;
        signatureItem.onTapClick = ^(TbBaseItem *item) {
            @strongify(self);
            [self onTapClickItem:item];
        };
    }
    
    UIView *layoutView = [_extensionsManager.toolSetBar.contentView viewWithTag:45678];
    if (!layoutView) {
        layoutView = [[UIView alloc] init];
        layoutView.backgroundColor = [UIColor fs_clearColor];
        layoutView.userInteractionEnabled = NO;
        [_extensionsManager.toolSetBar.contentView addSubview:layoutView];
        [layoutView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.top.bottom.mas_equalTo(layoutView.superview);
            make.width.mas_equalTo(0.1).priorityLow();
        }];
    }
    
    [textFileItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.checkBoxItem.contentView);
        make.left.mas_equalTo(layoutView);
        make.size.mas_equalTo(textFileItem.contentView.fs_size);
    }];
    
    [self.checkBoxItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.checkBoxItem.contentView.superview);
        make.left.mas_equalTo(textFileItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(self.checkBoxItem.contentView.fs_size);
    }];
    
    [radioItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(radioItem.contentView.superview);
        make.left.mas_equalTo(self.checkBoxItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(radioItem.contentView.fs_size);
    }];
    
    [comboBoxItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(comboBoxItem.contentView.superview);
        make.left.mas_equalTo(radioItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(comboBoxItem.contentView.fs_size);
    }];
    
    [listBoxItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(listBoxItem.contentView.superview);
        make.left.mas_equalTo(comboBoxItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        if (!signatureItem) {make.right.mas_equalTo(layoutView);}
        make.size.mas_equalTo(listBoxItem.contentView.fs_size);
    }];
        
    if (signatureItem) {
        [signatureItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.checkBoxItem.contentView);
            make.left.mas_equalTo(listBoxItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
            make.right.mas_equalTo(layoutView);
            make.size.mas_equalTo(signatureItem.contentView.fs_size);
        }];
    }
    return YES;
}

- (void)onTapClickItem:(TbBaseItem *)item {
    FormToolHandler *toolHandler = [_extensionsManager getToolHandlerByName:Tool_Form];
    if (item.button != currentItemBtn) {
        currentItemBtn.selected = NO;
        item.button.selected = YES;
        currentItemBtn = item.button;
        toolHandler.fieldType = item.tag;
    }else{
        currentItemBtn.selected = NO;
        currentItemBtn = nil;
        toolHandler.fieldType = FSFieldTypeUnknown;
    }
    toolHandler.continueAdd = item.button.selected;
}

@end
