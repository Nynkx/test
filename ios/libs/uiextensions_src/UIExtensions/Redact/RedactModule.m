/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "RedactModule.h"
#import "RedactToolHandler.h"
#import "RedactAnnotHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>
#import "UIExtensionsConfig.h"

@interface RedactModule ()<IDocEventListener> {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
    FSAnnotType _annotType;
}

@end

@implementation RedactModule

- (NSString *)getName {
    return @"Redact";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];


    }
    return self;
}

- (void)loadModule {
    RedactToolHandler* toolHandler = [[RedactToolHandler alloc] initWithUIExtensionsManager:_extensionsManager];
    [_extensionsManager registerToolHandler:toolHandler];
    RedactAnnotHandler* annotHandler = [[RedactAnnotHandler alloc] initWithUIExtensionsManager:_extensionsManager];
    [_extensionsManager registerAnnotHandler:annotHandler];
    [_pdfViewCtrl registerDocEventListener:self];
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error{
    _extensionsManager.isSupportRedaction = [FSLibrary hasModuleLicenseRight:FSModuleNameRedaction];
}


@end
