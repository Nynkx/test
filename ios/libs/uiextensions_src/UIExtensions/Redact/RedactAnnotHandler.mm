/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "RedactAnnotHandler.h"

@implementation RedactAnnotHandler {
    __weak TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super initWithUIExtensionsManager:extensionsManager];
    if (self) {
        _taskServer = extensionsManager.taskServer;
        self.annotBorderEdgeInsets = UIEdgeInsetsMake(-5, -5, -5, -5);
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotRedact;
}

- (BOOL)canMoveAnnot:(FSAnnot *)annot {
    FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
    return !redact.quads.count && redact.canMove;
}

- (BOOL)canResizeAnnot:(FSAnnot *)annot {
    FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
    return !redact.quads.count && redact.canResize;
}

- (BOOL)shouldDrawAnnot:(FSAnnot *)annot inPDFViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    return YES;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen | FSMenuOptionFlatten;
    
    if (self.extensionsManager.isSupportRedaction && ([Utility canApplyRedaction:[self.extensionsManager.pdfViewCtrl currentDoc]])) {
        options |= FSMenuOptionApplyRedaction;
    }
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
    return options;
}

- (void)applyRedaction{
    UIAlertController *alert = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kRedaction") message:FSLocalizedForKey(@"kRedactTip") actions:@[FSLocalizedForKey(@"kCancel"),FSLocalizedForKey(@"kOK")] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
        if (index == 1) {
            FSRedact *annot = [[FSRedact alloc] initWithAnnot:self.extensionsManager.currentAnnot];
            [self.extensionsManager setCurrentAnnot:nil];
            int pageIndex = annot.pageIndex;
            [Utility applyRedactAnnot:annot completed:^(NSArray<FSAnnotAttributes *> * _Nonnull replyAttributes) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAnnotLsitUpdate object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAttachmentLsitUpdate object:nil];
                [self.pdfViewCtrl refresh:pageIndex needRender:YES];
                self.extensionsManager.isDocModified = YES;
                [self.extensionsManager.undoItems removeAllObjects];
                [self.extensionsManager.redoItems removeAllObjects];
                [self.extensionsManager removeUndoItems];
            }];
        }else{
            [self showMenu];
        }
    }];
    [self.pdfViewCtrl.fs_viewController presentViewController:alert animated:YES completion:nil];
}

- (BOOL)removeAnnot:(FSAnnot *)annot {
    return [super removeAnnot:annot addUndo:self.extensionsManager.isSupportRedaction];
}

- (void)fixInvalidFontForFreeText:(FSAnnot *)annot {
    FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
    FSDefaultAppearance *ap = [redact getDefaultAppearance];
    if (!ap.font || [ap.font isEmpty] || ap.text_size == 0) {
        if (!ap.font || [ap.font isEmpty]) {
            FSFont *font = [[FSFont alloc] initWithFont_id:(FSFontStandardID)[Utility toStandardFontID:self.extensionsManager.config.defaultSettings.annotations.redaction.textFace]];
            ap.font = font;
        }
        if (ap.text_size == 0) {
            ap.text_size = self.extensionsManager.config.defaultSettings.annotations.redaction.textSize;
        }
        ap.text_color = self.extensionsManager.config.defaultSettings.annotations.redaction.textColor.rgbValue;
        ap.flags = FSDefaultAppearanceFlagFont | FSDefaultAppearanceFlagTextColor | FSDefaultAppearanceFlagFontSize;
        [redact setDefaultAppearance:ap];
    }
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    [self fixInvalidFontForFreeText:annot];
    [super onAnnotSelected:annot];
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *_Nullable)annot{
    [super onDraw:pageIndex inContext:context annot:annot];
    if (pageIndex == annot.pageIndex && [Utility isAnnot:annot uniqueToAnnot:self.annot]) {
        FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
        NSMutableArray *quadPoints = [[NSMutableArray alloc] initWithCapacity:[redact.quadPoints getSize]];
        for (int i = 0; i < [redact.quadPoints getSize]; i++) {
            [quadPoints addObject:[redact.quadPoints getAt:i]];
        }
        
        if (!quadPoints.count) {
            CGRect fillRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:redact.fsrect pageIndex:annot.pageIndex];
            CGContextAddRect(context, fillRect);
            [[UIColor colorWithRGB:redact.applyFillColor] set];
            CGContextFillPath(context);
            return;
        }
        
        [quadPoints enumerateObjectsUsingBlock:^(FSQuadPoints *quad, NSUInteger idx, BOOL * _Nonnull stop) {
            FSRectF *dibRect = [Utility convertToFSRect:quad.first p2:quad.fourth];
            CGRect fillRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:annot.pageIndex];
            CGContextAddRect(context, fillRect);
            [[UIColor colorWithRGB:redact.applyFillColor] set];
            CGContextFillPath(context);
        }];
    }
}

- (void)showStyle {
    FSRedact *annot = [[FSRedact alloc] initWithAnnot:self.extensionsManager.currentAnnot];
    NSArray *colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.extensionsManager.propertyBar setColors:colors];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_TEXTINPUT | PROPERTY_FONTNAME | PROPERTY_FONTSIZE | PROPERTY_COLOR frame:CGRectZero];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME | PROPERTY_FONTSIZE |PROPERTY_TEXTINPUT | PROPERTY_COLOR lock:!annot.canModifyAppearance];
    
    FSDefaultAppearance *appearance = [annot getDefaultAppearance];
    int fontColor = 0;
    float fontSize = 12.f;
    FSFont *font = [appearance getFont];
    NSString *fontName = @"Courier";
    if (font && ![font isEmpty]) {
        fontName = [font getName];
        fontSize = [appearance getText_size];
        fontColor = [appearance getText_color];
    }
    [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME stringValue:fontName];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTSIZE floatValue:fontSize];
    
    [self.extensionsManager.propertyBar setProperty:PROPERTY_TEXTINPUT intValue:fontColor];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_TEXTINPUT stringValue:annot.overlayText];
    
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.applyFillColor];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];

    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
}

- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    if (annot.type == FSAnnotStrikeOut && [Utility isReplaceText:[[FSStrikeOut alloc] initWithAnnot:annot]]) {
        return NO;
    }
    return [super isHitAnnot:annot point:point];
}

@end
