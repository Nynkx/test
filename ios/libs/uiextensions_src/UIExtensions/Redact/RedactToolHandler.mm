/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "RedactToolHandler.h"
#import "PaintUtility.h"

@interface RedactToolHandler ()
@property (nonatomic, strong) FSAnnot *annot;
@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;

@property (nonatomic, assign) BOOL isEndDrawing;
@property (nonatomic, assign) int currentPageIndex;

@property (nonatomic, strong) PaintUtility *paintUtility;
@property (nonatomic, strong) NSMutableDictionary *tempPointsDict;
@end

@implementation RedactToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotRedact;
        self.tempPointsDict = @{}.mutableCopy;
    }
    return self;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
    _isEndDrawing = NO;
    self.annot = nil;
    
    self.tempPointsDict = @{}.mutableCopy;
}

- (void)onDeactivate {
    _isEndDrawing = NO;
    if (self.annot) {
        FSAnnot *annot = self.annot;
        [_pdfViewCtrl lockRefresh];
        [self updateAnnotRect];
        [annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        [_pdfViewCtrl refresh:self.annot.pageIndex];
    }
    self.annot = nil;
    [self.tempPointsDict removeAllObjects];
    self.tempPointsDict = nil;
}

-(void) updateAnnotRect {
    if(self.startPoint && self.endPoint)
        self.annot.fsrect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}


- (NSString *)getName {
    return Tool_Redaction;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    self.startPoint = nil;
    self.endPoint = nil;
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    
    [self.tempPointsDict setObject:@(point) forKey:@(pageIndex)];
    
    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSRectF *dibRect = nil;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.currentPageIndex = pageIndex;
        _isEndDrawing = NO;
        
        dibRect = [[FSRectF alloc] init];
        [dibRect setLeft:dibPoint.x];
        [dibRect setBottom:dibPoint.y];
        [dibRect setRight:dibPoint.x + 0.1];
        [dibRect setTop:dibPoint.y + 0.1];
        if (self.annot) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
            @try {
                [self updateAnnotRect];
                [annotHandler addAnnot:self.annot addUndo:YES];
            } @catch (NSException *exception) {
            } @finally {
                self.annot = nil;
            }
        }
        self.annot = [self addAnnotToPage:pageIndex withRect:dibRect];
        if (!self.annot) {
            return YES;
        }
        
        self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.endPoint   = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        
        FSRectF *lineFSRect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:self.annot.lineWidth top1:self.annot.lineWidth];
        CGRect lineRect = [_pdfViewCtrl convertPdfRectToPageViewRect:lineFSRect pageIndex:pageIndex];
        float lineWidth = lineRect.size.width;
        
        self.paintUtility = [[PaintUtility alloc] init];
        self.paintUtility.annotLineWidth = lineWidth;
        self.paintUtility.annotColor = _extensionsManager.config.defaultSettings.annotations.redaction.color.rgbValue;
        self.paintUtility.annotOpacity = 0;
        
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (self.currentPageIndex == pageIndex) {
            FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
            float marginX = [Utility getAnnotMinXMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
            float marginY = [Utility getAnnotMinYMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
            FSRotation rotation = [page getRotation];
            CGFloat pdfPageWidth = (rotation == FSRotation0 || rotation == FSRotation180 || rotation == FSRotationUnknown) ? [page getWidth] : [page getHeight];
            CGFloat pdfPageHeight = (rotation == FSRotation0 || rotation == FSRotation180 || rotation == FSRotationUnknown) ? [page getHeight] : [page getWidth];
            if (!(dibPoint.x < marginX || dibPoint.y > pdfPageHeight - marginY || dibPoint.y < marginY || dibPoint.x > pdfPageWidth - marginX)) {
                
                self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
                
                CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:pageIndex];
                rect = CGRectInset(rect, -10, -10);
                [_pdfViewCtrl refresh:pageIndex];
            }
        }
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        if (pageIndex != self.annot.pageIndex) {
            point = [self.tempPointsDict[@(self.annot.pageIndex)] CGPointValue];
            pageIndex = self.annot.pageIndex;
        }
        
        float marginX = [Utility getAnnotMinXMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
        float marginY = [Utility getAnnotMinYMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
        
        UIView *pageview = [_pdfViewCtrl getPageView:pageIndex];
        
        if (point.y < marginY) {
            point.y = marginY;
        }
        
        if (point.y > pageview.bounds.size.height - marginY) {
            point.y = pageview.bounds.size.height - marginY;
        }
        
        if (point.x < marginX) {
            point.x = marginX;
        }
        
        if (point.x > pageview.bounds.size.width - marginX) {
            point.x = pageview.bounds.size.width - marginX;
        }
        
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        
        [_pdfViewCtrl lockRefresh];
        self.annot.fsrect = dibRect;
        [_pdfViewCtrl unlockRefresh];
        
        if (self.type == FSAnnotRedact) {
            if (self.annot.fsrect.top - self.annot.fsrect.bottom < 5.0 || self.annot.fsrect.right - self.annot.fsrect.left < 5.0) {
                FSAnnot* annot = self.annot;
                BOOL result = [self removeAnnot:annot];
                if (result) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self->_pdfViewCtrl refresh:pageIndex];
                    });
                }
                
                return NO;
            }
        }
        
        if (self.annot) {
            [_pdfViewCtrl lockRefresh];
            [self.annot resetAppearanceStream];
            [_pdfViewCtrl unlockRefresh];
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
            FSAnnot *annot = self.annot;
            @try {
                [annotHandler addAnnot:self.annot addUndo:YES];
            } @catch (NSException *exception) {
            } @finally {
                self.annot = nil;
                self.paintUtility = nil;
                [_extensionsManager setCurrentToolHandler:nil];
                if (!annot.quads.count) {
                    [_extensionsManager setCurrentAnnot:annot];
                }
            }
        }
        _isEndDrawing = YES;
        return YES;
    }
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)removeAnnot:(FSAnnot *)annot {
    FSPDFPage *page = [annot getPage];
    BOOL result = [page removeAnnot:annot];
    
    self.annot = nil;
    return result;
}

- (FSAnnot *)addAnnotToPage:(int)pageIndex withRect:(FSRectF *)rect {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;
    
    FSRedact *annot = nil;
    @try {
        FSRedaction *redaction = [[FSRedaction alloc] initWithDocument:page.getDocument];
        FSRectFArray *rects = [[FSRectFArray alloc] init];
        [rects add:rect];
        annot = [redaction markRedactAnnot:page rects:rects];
    } @catch (NSException *exception) {
        return nil;
    }
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.applyFillColor = [_extensionsManager getPropertyBarSettingColor:self.type];
    annot.opacity = [_extensionsManager getAnnotOpacity:self.type] / 100.0;
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    annot.flags = FSAnnotFlagPrint;
    annot.subject = @"Redact";
    annot.contents = @"Redaction";
    
    FSDefaultAppearance *appearance = [annot getDefaultAppearance];
    appearance.flags = FSDefaultAppearanceFlagFont | FSDefaultAppearanceFlagTextColor | FSDefaultAppearanceFlagFontSize;
    NSString *fontName = [_extensionsManager getAnnotFontName:FSAnnotRedact];
    int fontID = [Utility toStandardFontID:fontName];
    if (fontID == -1) {
        [appearance setFont:[[FSFont alloc] initWithName:fontName styles:0 charset:FSFontCharsetDefault weight:0]];
    } else {
        [appearance setFont:[[FSFont alloc] initWithFont_id:(FSFontStandardID)fontID]];
    }
    [appearance setText_size:[_extensionsManager getAnnotFontSize:FSAnnotRedact]];
    unsigned int color = [_extensionsManager getAnnotTextColor:FSAnnotRedact];
    [appearance setText_color:color];
    [annot setDefaultAppearance:appearance];
    
    return annot;
}


- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_extensionsManager.currentToolHandler != self) {
        return;
    }
    if (!self.startPoint || !self.endPoint || self.currentPageIndex != pageIndex) {
        return;
    }
    if (_isEndDrawing) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self->_pdfViewCtrl refresh:pageIndex needRender:YES];
        });
        return;
    }
    
    CGPoint startPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint pageIndex:pageIndex];
    CGPoint endPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.endPoint pageIndex:pageIndex];
    
    CGRect rect = [self getRectFromStartPoint:startPoint endPoint:endPoint];
    [self.paintUtility drawMultipleSelectWithRect:rect inContext:context];

}

- (CGRect)getRectFromStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CGFloat xminValue = MIN(startPoint.x, endPoint.x);
    CGFloat yminValue = MIN(startPoint.y, endPoint.y);
    
    CGFloat xmaxValue = MAX(startPoint.x, endPoint.x);
    CGFloat ymaxValue = MAX(startPoint.y, endPoint.y);
    
    CGRect result = CGRectMake(xminValue, yminValue, fabs(xmaxValue-xminValue), fabs(ymaxValue-yminValue));
    return result;
}

@end
