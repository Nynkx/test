/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ImageAnnotHandler.h"

@implementation ImageAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        _minImageWidthInPage = 0.1;
        _maxImageWidthInPage = 0.5;
        _minImageHeightInPage = 0.1;
        _maxImageHeightInPage = 0.5;
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotScreen;
}

- (BOOL)keepAspectRatioForResizingAnnot:(FSAnnot *)annot {
    return YES;
}

- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot {
    UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
    CGFloat pageWidth = CGRectGetWidth(pageView.bounds);
    CGFloat pageHeight = CGRectGetHeight(pageView.bounds);
    CGFloat minW = self.minImageWidthInPage * pageWidth;
    CGFloat minH = self.minImageHeightInPage * pageHeight;
    return CGSizeMake(minW, minH);
}

- (CGSize)maxSizeForResizingAnnot:(FSAnnot *)annot {
    UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
    CGFloat pageWidth = CGRectGetWidth(pageView.bounds);
    CGFloat pageHeight = CGRectGetHeight(pageView.bounds);
    CGFloat maxW = self.maxImageWidthInPage * pageWidth;
    CGFloat maxH = self.maxImageHeightInPage * pageHeight;
    return CGSizeMake(maxW, maxH);
}

- (void)showStyle {
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_OPACITY | PROPERTY_ROTATION frame:CGRectZero];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY | PROPERTY_ROTATION lock:!self.annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:self.annot.opacity * 100.0];
    FSRotation rotation = [[[FSScreen alloc] initWithAnnot:self.annot] getRotation];
    if (rotation != 0) {
        rotation = 4 - rotation;
    }
    [self.extensionsManager.propertyBar setProperty:PROPERTY_ROTATION intValue:[Utility valueForRotation:rotation]];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];
    int pageIndex = self.annot.pageIndex;
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:pageIndex];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:@[ [self.pdfViewCtrl getDisplayView] ]];
}

- (UIColor *)borderColorForSelectedAnnot:(FSAnnot *)annot {
    return [UIColor colorWithRGB:0x179cd8];
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionNone;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionStyle;
        options |= FSMenuOptionFlatten;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    return options;
}

@end
