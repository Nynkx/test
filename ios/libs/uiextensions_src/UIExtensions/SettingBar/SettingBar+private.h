/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#ifndef SettingBar_private_h
#define SettingBar_private_h

#import <FoxitRDK/FSPDFViewControl.h>
#import <UIKit/UIKit.h>
#import "SettingBar.h"

@interface SettingBar ()

@property (nonatomic, strong, nonnull) IBOutlet UIButton *singleViewBtn;
@property (nonatomic, strong, nonnull) IBOutlet UISwitch *continueSwith;
@property (nonatomic, strong, nonnull) UIButton *thumbnailViewBtn;
@property (nonatomic, strong, nonnull) UIButton *reflowBtn;
@property (nonatomic, strong, nonnull) UIButton *cropBtn;
@property (nonatomic, strong, nonnull) UIButton *speechBtn;
@property (nonatomic, strong, nonnull) UIButton *screenLockBtn;
@property (nonatomic, strong, nonnull) UIButton *panAndZoomBtn;
@property (nonatomic, strong, nonnull) IBOutlet UISwitch *nightModeSwith;
@property (nonatomic, strong, nonnull) IBOutlet UIButton *doubleViewBtn;
@property (nonatomic, strong, nonnull) IBOutlet UIButton *coverBtn;
@property (nonatomic, strong, nonnull) IBOutlet UIButton *fitpageBtn;
@property (nonatomic, strong, nonnull) IBOutlet UIButton *fitwidthBtn;
@property (nonatomic, strong, nonnull) UIButton *rotateBtn;

/**
 * @brief    Initialize the setting bar.
 *
 * @param[in]    extensionsManager   The extensions manager.
 *
 * @return    The setting bar instance.
 */
- (instancetype _Nullable )initWithUIExtensionsManager:(UIExtensionsManager *_Nullable)extensionsManager;

- (UIView *_Nullable)getItemView:(SettingItemType)itemType;
- (void)updateLayoutButtonsWithLayout:(PDF_LAYOUT_MODE)layout;

@end

#endif /* SettingBar_private_h */
