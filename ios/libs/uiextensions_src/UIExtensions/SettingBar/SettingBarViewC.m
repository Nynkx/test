/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SettingBarViewC.h"

@interface SettingBarViewC ()
@property (weak, nonatomic) IBOutlet UIScrollView *containerView;

@end

@implementation SettingBarViewC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)setView:(UIView *)view{
    [super setView:_containerView];
}


@end
