/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SettingBar.h"
#import "SettingBar+private.h"
#import "SettingBarViewC.h"


typedef NS_ENUM(NSUInteger, KDividLineType) {
    KDividLineTypeHorizontal,
    KDividLineTypeVertical
};

@interface SettingBar () <IDocEventListener>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thirdViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fourViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fifthViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sixthViewHeight;
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (weak, nonatomic) IBOutlet UILabel *pageMode;
@property (weak, nonatomic) IBOutlet UILabel *pageShow;
@property (nonatomic, strong) IBOutlet  UISlider *brightnessControl;

@property (nonatomic, strong) IBOutlet UIImageView *brightnessBigger;
@property (nonatomic, strong) IBOutlet UIImageView *brightnessSmaller;

@property (nonatomic, assign) float tempSysBrightness;
@property (nonatomic, assign) BOOL isEnterBg;
@property (nonatomic, assign) BOOL isActive;

@property (nonatomic, assign) BOOL isBrightnessManual;
@property (nonatomic, assign) BOOL isNightMode;
@property (nonatomic, assign) BOOL isScreenLocked;

@property (nonatomic, strong) NSMutableArray *bottomButtonArr;
@property (nonatomic, strong) IBOutlet UIScrollView *levelBottomView;
@property (nonatomic, strong) UIView *levelBottomContainerView;

@property (nonatomic, strong) IBOutlet UISwitch *brightnessSwitch;
@property (nonatomic, assign) PDF_LAYOUT_MODE lastLayoutMode;
@end

@implementation SettingBar

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommonView" bundle:[NSBundle bundleForClass:[self class]]];
    SettingBarViewC *settingBarViewC = [storyboard instantiateViewControllerWithIdentifier:@"SettingBarViewC"];
    self = settingBarViewC.settingBar;
    if (self) {
        self.pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self.pdfViewCtrl registerDocEventListener:self];
        self.extensionsManager = extensionsManager;
        _isBrightnessManual = NO;
        self.isEnterBg = NO;
        self.isActive = NO;

        self.tempSysBrightness = [UIScreen mainScreen].brightness;

        UIDevice *device = [UIDevice currentDevice];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:device];

        self.contentView = [[UIView alloc] init];
        self.contentView.backgroundColor = ThemeCellBackgroundColor;

        UIView *view = settingBarViewC.view;
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.equalTo(self.contentView);
        }];
        
        self.thumbnailViewBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kViewModeThumbnail") imageNormal:ImageNamed(@"thumbnail") imageSelected: ImageNamed(@"thumbnail_selected") imageDisable:ImageNamed(@"thumbnail_selected")];
        
        self.reflowBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kReflow") imageNormal:ImageNamed(@"readview_reflow_normal") imageSelected: ImageNamed(@"readview_reflow_selected") imageDisable:ImageNamed(@"readview_reflow_selected")];

        self.cropBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kCropMode") imageNormal:ImageNamed(@"readview_crop_normal") imageSelected:ImageNamed(@"readview_crop_selected") imageDisable:ImageNamed(@"readview_crop_selected")];
        
        self.speechBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kSpeak") imageNormal:ImageNamed(@"speech") imageSelected:ImageNamed(@"speech_select") imageDisable:ImageNamed(@"speech_select")];
        

        self.screenLockBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kScreenLock") imageNormal:ImageNamed(@"readview_screen_lock_normal") imageSelected:ImageNamed(@"readview_screen_lock_selected") imageDisable:ImageNamed(@"readview_screen_lock_selected")];
        
        self.screenLockBtn.hidden = !self.extensionsManager.needScreenLock;
        
        NSDictionary *infoPlistDic = [NSBundle mainBundle].infoDictionary;
        if ([[infoPlistDic objectForKey:@"UIRequiresFullScreen"] boolValue] != YES && [[infoPlistDic objectForKey:@"UISupportedInterfaceOrientations"] count] == 4) {
            _screenLockBtn.hidden = YES;
        }

        self.panAndZoomBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kPanAndZoom") imageNormal:ImageNamed(@"zoom_mode") imageSelected:ImageNamed(@"zoom_mode_selected") imageDisable:ImageNamed(@"zoom_mode_selected")];
        
        
        self.rotateBtn = [SettingBar createItemWithImageAndTitle:FSLocalizedForKey(@"kRotateView") imageNormal:ImageNamed(@"readview_roateview_normal") imageSelected:ImageNamed(@"readview_roateview_selected") imageDisable:ImageNamed(@"readview_roateview_selected")];
        
        [self.brightnessControl setThumbImage:ImageNamed(@"property_linewidth_slider.png") forState:UIControlStateNormal];
        [self.brightnessControl setThumbImage:ImageNamed(@"property_linewidth_slider.png") forState:UIControlStateDisabled];
        self.brightnessControl.minimumValue = 0.2f;
        self.brightnessControl.enabled = !self.brightnessSwitch.on;
        
        _levelBottomContainerView = [[UIView alloc] init];
#if _MAC_CATALYST_
        _levelBottomView.showsHorizontalScrollIndicator = YES;
#endif
        [_levelBottomView addSubview:_levelBottomContainerView];
        
        [_levelBottomContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.left.right.top.bottom.equalTo(self.levelBottomView);
        }];
        
        if (self.extensionsManager.config.loadThumbnail) {
            [_levelBottomContainerView addSubview:self.thumbnailViewBtn];
        }
        [_levelBottomContainerView addSubview:self.reflowBtn];
        [_levelBottomContainerView addSubview:self.cropBtn];
        [_levelBottomContainerView addSubview:self.speechBtn];
        [_levelBottomContainerView addSubview:self.rotateBtn];
        [_levelBottomContainerView addSubview:self.screenLockBtn];
        [_levelBottomContainerView addSubview:self.panAndZoomBtn];
        
        self.continueSwith.on = NO;
        
        [self updateBtnLayout];
        [self addBtnTarget];
    }
    return self;
}

- (NSMutableArray *)getShowBtnFromArray:(NSMutableArray *)sourceArray {
    NSMutableArray *_tempArr = [NSMutableArray array];
    for (int i = 0; i < sourceArray.count; i++) {
        UIButton *button = [sourceArray objectAtIndex:i];
        [button mas_remakeConstraints:^(MASConstraintMaker *make) {[make removeExisting];}];
        if (!button.hidden) {
            [_tempArr addObject:button];
        }
    }

    return _tempArr;
}

- (void)updateBtnLayout {
    
    if (self.singleViewBtn.hidden && self.doubleViewBtn.hidden && self.coverBtn.hidden) {
        ((UIView *)self.firstViewHeight.firstItem).hidden = YES;
        self.firstViewHeight.constant = 0.f;
    }else{
        self.firstViewHeight.constant = 50.f;
        ((UIView *)self.firstViewHeight.firstItem).hidden = NO;
    }
    if (self.continueSwith.hidden) {
        self.secondViewHeight.constant = 0.f;
        ((UIView *)self.secondViewHeight.firstItem).hidden = YES;
    }else{
        self.secondViewHeight.constant = 50.f;
        ((UIView *)self.secondViewHeight.firstItem).hidden = NO;
    }
    if (self.fitpageBtn.hidden && self.fitwidthBtn.hidden) {
        ((UIView *)self.thirdViewHeight.firstItem).hidden = YES;
        self.thirdViewHeight.constant = 0.f;
    }else{
        self.thirdViewHeight.constant = 50.f;
        ((UIView *)self.thirdViewHeight.firstItem).hidden = NO;
    }

    if (self.brightnessControl.hidden) {
        self.fourViewHeight.constant = 0.f;
        ((UIView *)self.fourViewHeight.firstItem).hidden = YES;
    }else{
        self.fourViewHeight.constant = 100.f;
        ((UIView *)self.fourViewHeight.firstItem).hidden = NO;
    }
    if (self.nightModeSwith.hidden) {
        self.fifthViewHeight.constant = 0.f;
        ((UIView *)self.fifthViewHeight.firstItem).hidden = YES;
    }else{
        self.fifthViewHeight.constant = 50.f;
        ((UIView *)self.fifthViewHeight.firstItem).hidden = NO;
    }
    
    NSMutableArray *bottomBtnArr = [self getShowBtnFromArray:@[ self.coverBtn, self.doubleViewBtn, self.singleViewBtn].mutableCopy];
    UIView  *lastButton = nil;
    for ( int i = 0; i < bottomBtnArr.count; i++) {
        UIButton *button = bottomBtnArr[i];
        [button mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(lastButton ? lastButton.mas_left : button.superview.mas_right).with.offset(lastButton ? -25 : -15);
            make.centerY.equalTo(button.superview);
        }];
        lastButton = button;
    }
    
    bottomBtnArr = [self getShowBtnFromArray:@[ self.fitwidthBtn, self.fitpageBtn].mutableCopy];
    lastButton = nil;
    for ( int i = 0; i < bottomBtnArr.count; i++) {
        UIButton *button = bottomBtnArr[i];
        [button mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(lastButton ? lastButton.mas_left : button.superview.mas_right).with.offset(lastButton ? -25 : -15);
            make.centerY.equalTo(button.superview);
        }];
        lastButton = button;
    }


    CGRect rect = self.contentView.frame;
    rect.size.height = self.firstViewHeight.constant + self.secondViewHeight.constant + self.thirdViewHeight.constant + self.fourViewHeight.constant + self.fifthViewHeight.constant;
    
    self.bottomButtonArr = [_levelBottomContainerView.subviews mutableCopy];
    bottomBtnArr = [self getShowBtnFromArray:self.bottomButtonArr];
    lastButton = nil;
    for ( int i = 0; i < bottomBtnArr.count; i++) {
        UIButton *button = bottomBtnArr[i];
        [button mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(button.frame.size);
            make.left.mas_equalTo(lastButton ? lastButton.mas_right : button.superview.mas_left).with.offset(lastButton ? 15 : 5);
            make.centerY.equalTo(button.superview);
        }];
        lastButton = button;
    }

    if (rect.size.height && DEVICE_iPAD && bottomBtnArr.count < 5) {
        rect.size.width = 320;
    }
    
    [lastButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastButton.superview).with.offset(-5);
    }];

    if (!lastButton) {
        self.sixthViewHeight.constant = 0.f;
        ((UIView *)self.sixthViewHeight.firstItem).hidden = YES;
    }else{
        self.sixthViewHeight.constant = 100.f;
        ((UIView *)self.sixthViewHeight.firstItem).hidden = NO;
    }
    rect.size.height += self.sixthViewHeight.constant;
    self.contentView.frame = rect;
    
    if ([self.delegate respondsToSelector:@selector(settingBarDidChangeSize:)]) {
        [self.delegate settingBarDidChangeSize:self];
    }
}

- (void)addBtnTarget {
    [self.singleViewBtn addTarget:self action:@selector(singleClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.continueSwith addTarget:self action:@selector(continueClicked) forControlEvents:UIControlEventTouchUpInside];

    if (self.extensionsManager.config.loadThumbnail) {
        [self.thumbnailViewBtn addTarget:self action:@selector(thumbnailClicked) forControlEvents:UIControlEventTouchUpInside];
    }

    [self.fitpageBtn addTarget:self action:@selector(fitpageClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.fitwidthBtn addTarget:self action:@selector(fitwidthClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.rotateBtn addTarget:self action:@selector(rotateClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.reflowBtn addTarget:self action:@selector(reflowClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.cropBtn addTarget:self action:@selector(cropClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.speechBtn addTarget:self action:@selector(speechClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.screenLockBtn addTarget:self action:@selector(screenClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.panAndZoomBtn addTarget:self action:@selector(panAndZoomClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.brightnessSwitch addTarget:self action:@selector(onSwitchClicked) forControlEvents:UIControlEventValueChanged];

    [self.brightnessControl addTarget:self action:@selector(sliderChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.brightnessControl addTarget:self action:@selector(sliderChangedEndValue) forControlEvents:UIControlEventTouchUpInside];

    [self.nightModeSwith addTarget:self action:@selector(nightModeClicked) forControlEvents:UIControlEventTouchUpInside];

    [self.doubleViewBtn addTarget:self action:@selector(doubleClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.doubleViewBtn addTarget:self action:@selector(doubleClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.coverBtn addTarget:self action:@selector(coverClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (UIButton *)createButtonWith:(NSDictionary *)buttoninfo {
    UIButton *button = [UIButton buttonWithType:[buttoninfo[@"type"] intValue]];

    UIImage *viewbgNormal = [ImageNamed(@"normalImage") resizableImageWithCapInsets:UIEdgeInsetsMake(14, 19, 14, 19)];
    UIImage *viewSelected = [ImageNamed(@"selectedImage") resizableImageWithCapInsets:UIEdgeInsetsMake(14, 19, 14, 19)];

    [button setBackgroundImage:viewbgNormal forState:UIControlStateNormal];
    [button setBackgroundImage:viewSelected forState:UIControlStateHighlighted];
    [button setBackgroundImage:viewSelected forState:UIControlStateSelected];

    [button setTitle:FSLocalizedForKey(buttoninfo[@"title"]) forState:UIControlStateNormal];

    [button setTitleColor:[UIColor colorWithRGB:[buttoninfo[@"normalTitleColor"] unsignedIntValue]] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRGB:[buttoninfo[@"higinlightTitleColor"] unsignedIntValue]] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithRGB:[buttoninfo[@"selectTitleColor"] unsignedIntValue]] forState:UIControlStateSelected];

    button.titleLabel.font = [UIFont systemFontOfSize:[buttoninfo[@"titleFont"] floatValue]];
    return button;
}

- (void)setTempSysBrightness:(float)tempSysBrightness {
    _tempSysBrightness = tempSysBrightness;
}

- (void)onSwitchClicked {
    self.brightnessControl.enabled = !self.brightnessSwitch.on;

    if (self.brightnessSwitch.on) {
        [self.brightnessSmaller setImage:[SettingBar imageByApplyingAlpha:ImageNamed(@"readview_brightness_smaller") alpha:0.5]];
        [self.brightnessBigger setImage:[SettingBar imageByApplyingAlpha:ImageNamed(@"readview_brightness_bigger") alpha:0.5]];
        [UIScreen mainScreen].brightness = self.tempSysBrightness;

    } else {
        [self.brightnessSmaller setImage:ImageNamed(@"readview_brightness_smaller")];
        [self.brightnessBigger setImage:ImageNamed(@"readview_brightness_bigger")];
        {
            self.brightnessControl.value = [UIScreen mainScreen].brightness;
        }
    }
    _isBrightnessManual = !self.brightnessSwitch.on;
}

- (void)sliderChangedValue {
    [UIScreen mainScreen].brightness = self.brightnessControl.value;
}

- (void)sliderChangedEndValue {
}

- (void)singleClicked {
    [self updateLayoutButtonsWithLayout:PDF_LAYOUT_MODE_SINGLE];
    if ([self.delegate respondsToSelector:@selector(settingBarSinglePageLayout:)]) {
        [self.delegate settingBarSinglePageLayout:self];
    }
}

- (void)continueClicked {
    [self.pdfViewCtrl setContinuous:self.continueSwith.on];
    [self updateLayoutButtonsWithLayout:PDF_LAYOUT_MODE_UNKNOWN];
    if ([self.delegate respondsToSelector:@selector(settingBarContinuousLayout:)]) {
        [self.delegate settingBarContinuousLayout:self];
    }
}

- (void)doubleClicked {
    [self updateLayoutButtonsWithLayout:PDF_LAYOUT_MODE_TWO];
    if ([self.delegate respondsToSelector:@selector(settingBarDoublePageLayout:)]) {
        [self.delegate settingBarDoublePageLayout:self];
    }
}

- (void)coverClicked {
    [self updateLayoutButtonsWithLayout:PDF_LAYOUT_MODE_TWO_RIGHT];
    if ([self.delegate respondsToSelector:@selector(settingBarCoverPageLayout:)]) {
        [self.delegate settingBarCoverPageLayout:self];
    }
}

- (void)thumbnailClicked {
    self.singleViewBtn.selected = NO;
//    self.continueSwith.on = NO;
    self.doubleViewBtn.selected = NO;
    self.coverBtn.selected = NO;
    self.thumbnailViewBtn.selected = YES;
    if ([self.delegate respondsToSelector:@selector(settingBarThumbnail:)]) {
        [self.delegate settingBarThumbnail:self];
    }
}

- (void)fitpageClicked {
    PDF_LAYOUT_MODE currentLayoutMode = [_pdfViewCtrl getPageLayoutMode];
    if (currentLayoutMode == PDF_LAYOUT_MODE_UNKNOWN ||  currentLayoutMode == PDF_LAYOUT_MODE_REFLOW) {
        return;
    }
//    [self updateLayoutButtonsWithLayout:currentLayoutMode];
    if ([self.delegate respondsToSelector:@selector(settingBarFitPage:)]) {
        [self.delegate settingBarFitPage:self];
    }
}

- (void)fitwidthClicked {
    PDF_LAYOUT_MODE currentLayoutMode = [_pdfViewCtrl getPageLayoutMode];
    if (currentLayoutMode == PDF_LAYOUT_MODE_UNKNOWN ||  currentLayoutMode == PDF_LAYOUT_MODE_REFLOW) {
        return;
    }
//    [self updateLayoutButtonsWithLayout:currentLayoutMode];
    if ([self.delegate respondsToSelector:@selector(settingBarFitWidth:)]) {
        [self.delegate settingBarFitWidth:self];
    }
}

- (void)rotateClicked {
    PDF_LAYOUT_MODE currentLayoutMode = [_pdfViewCtrl getPageLayoutMode];
    if (currentLayoutMode == PDF_LAYOUT_MODE_UNKNOWN ||  currentLayoutMode == PDF_LAYOUT_MODE_REFLOW) {
        return;
    }
    //    [self updateLayoutButtonsWithLayout:currentLayoutMode];
    if ([self.delegate respondsToSelector:@selector(settingBarRotate:)]) {
        [self.delegate settingBarRotate:self];
    }
}

- (void)reflowClicked {
    [self updateLayoutButtonsWithLayout:PDF_LAYOUT_MODE_REFLOW];
    if ([self.delegate respondsToSelector:@selector(settingBarReflow:)]) {
        [self.delegate settingBarReflow:self];
    }
}

- (void)cropClicked {
    ((UIButton *) [self getItemView:CROPPAGE]).selected = YES;
    if ([self.delegate respondsToSelector:@selector(settingBarCrop:)]) {
        [self.delegate settingBarCrop:self];
    }
}

- (void)speechClicked {
    ((UIButton *) [self getItemView:SPEECH]).selected = YES;
    if ([self.delegate respondsToSelector:@selector(settingBarSpeech:)]) {
        [self.delegate settingBarSpeech:self];
    }
}

- (void)screenClicked {
    self.screenLockBtn.selected = !self.screenLockBtn.selected;
    if ([self.delegate respondsToSelector:@selector(settingBar:setLockScreen:)]) {
        [self.delegate settingBar:self setLockScreen:self.screenLockBtn.selected];
    }
}

- (void)panAndZoomClicked {
    //self.panAndZoomBtn.selected = !self.panAndZoomBtn.selected;
    if ([self.delegate respondsToSelector:@selector(settingBarPanAndZoom:)]) {
        [self.delegate settingBarPanAndZoom:self];
    }
}

- (void)nightModeClicked {
    if ([self.delegate respondsToSelector:@selector(settingBar:setNightMode:)]) {
        [self.delegate settingBar:self setNightMode:self.nightModeSwith.on];
    }
}

# pragma mark - get setting bar items show/hide status.
-(NSMutableDictionary *)getItemHiddenStatus{
    NSMutableDictionary *settingbarInfo = [@{} mutableCopy];
    
    [settingbarInfo setObject:self.singleViewBtn.isHidden ? @"YES":@"NO" forKey:@"SingleViewBtn"];
    [settingbarInfo setObject:self.continueSwith.isHidden ? @"YES":@"NO" forKey:@"ContinueSwith"];
    [settingbarInfo setObject:self.thumbnailViewBtn.isHidden ? @"YES":@"NO" forKey:@"ThumbnailViewBtn"];
    [settingbarInfo setObject:self.reflowBtn.isHidden ? @"YES":@"NO" forKey:@"ReflowBtn"];
    [settingbarInfo setObject:self.cropBtn.isHidden ? @"YES":@"NO" forKey:@"CropBtn"];
    [settingbarInfo setObject:self.panAndZoomBtn.isHidden ? @"YES":@"NO" forKey:@"PanAndZoomBtn"];
    [settingbarInfo setObject:self.screenLockBtn.isHidden ? @"YES":@"NO" forKey:@"ScreenLockBtn"];
    [settingbarInfo setObject:self.nightModeSwith.isHidden ? @"YES":@"NO" forKey:@"NightModeSwith"];
    [settingbarInfo setObject:self.brightnessControl.isHidden ? @"YES":@"NO" forKey:@"BrightnessControl"];
    [settingbarInfo setObject:self.coverBtn.isHidden ? @"YES":@"NO" forKey:@"CoverPageBtn"];
    [settingbarInfo setObject:self.doubleViewBtn.isHidden ? @"YES":@"NO" forKey:@"DoubleViewBtn"];
    [settingbarInfo setObject:self.fitpageBtn.isHidden ? @"YES":@"NO" forKey:@"FitpageBtn"];
    [settingbarInfo setObject:self.fitwidthBtn.isHidden ? @"YES":@"NO" forKey:@"FitwidthBtn"];
    [settingbarInfo setObject:self.rotateBtn.isHidden ? @"YES":@"NO" forKey:@"RotateBtn"];
    [settingbarInfo setObject:self.speechBtn.isHidden ? @"YES":@"NO" forKey:@"SpeechBtn"];
    return settingbarInfo;
}

- (BOOL)isItemHidden:(SettingItemType) type {
    UIView* view = [self getItemView:type];
    if(view)
        return view.isHidden;
    return YES;
}

- (void)setItem:(SettingItemType)itemType hidden:(BOOL)hidden {
    UIView *itemView = [self getItemView:itemType];
    itemView.hidden = hidden;
}

- (UIView *_Nullable)getItemView:(SettingItemType)itemType {
    switch (itemType) {
        case SINGLE:
            return self.singleViewBtn;
        case CONTINUOUS:
            return self.continueSwith;
        case DOUBLEPAGE:
            return self.doubleViewBtn;
        case COVERPAGE:
            return self.coverBtn;
        case THUMBNAIL:
            return self.thumbnailViewBtn;
        case REFLOW:
            return self.reflowBtn;
        case CROPPAGE:
            return self.cropBtn;
        case LOCKSCREEN:
            return self.screenLockBtn;
        case NIGHTMODE:
            return self.nightModeSwith;
        case BRIGHTNESS:
            return self.brightnessControl;
        case PANZOOM:
            return self.panAndZoomBtn;
        case FITPAGE:
            return self.fitpageBtn;
        case FITWIDTH:
            return self.fitwidthBtn;
        case ROTATE:
            return self.rotateBtn;
        case SPEECH:
            return self.speechBtn;
        default:
            return nil;
    }
}

- (void)updateLayoutButtonsWithLayout:(PDF_LAYOUT_MODE)layout {
    self.singleViewBtn.selected = layout == PDF_LAYOUT_MODE_SINGLE;
//    self.continueSwith.on = layout == PDF_LAYOUT_MODE_CONTINUOUS;
    self.continueSwith.on = [self.extensionsManager.pdfViewCtrl isContinuous];
    self.doubleViewBtn.selected = layout == PDF_LAYOUT_MODE_TWO;
    self.coverBtn.selected = layout == PDF_LAYOUT_MODE_TWO_RIGHT;
    self.reflowBtn.selected = layout == PDF_LAYOUT_MODE_REFLOW;
    self.thumbnailViewBtn.selected = NO;
    
    BOOL canSpeak = [Utility canCopyText:self.extensionsManager.pdfViewCtrl] && self.extensionsManager.config.copyText && ![_pdfViewCtrl isDynamicXFA];
    self.speechBtn.enabled = canSpeak;
    self.speechBtn.selected = NO;
    
    float currentPageScale = [self.extensionsManager.pdfViewCtrl getScale];
    float defaultPageScale = [self.extensionsManager.pdfViewCtrl getDefaultPageScale];

    currentPageScale = [[NSString stringWithFormat:@"%.6f",currentPageScale] floatValue];
    defaultPageScale = [[NSString stringWithFormat:@"%.6f",defaultPageScale] floatValue];
    
    self.fitpageBtn.selected = [_pdfViewCtrl getZoomMode] == PDF_DISPLAY_ZOOMMODE_FITPAGE && currentPageScale == defaultPageScale;
    self.fitwidthBtn.selected = [_pdfViewCtrl getZoomMode] == PDF_DISPLAY_ZOOMMODE_FITWIDTH && currentPageScale == defaultPageScale;

    self.cropBtn.selected = [_pdfViewCtrl getCropMode] != PDF_CROP_MODE_NONE ? YES : NO;
    self.panAndZoomBtn.enabled = self.cropBtn.selected?NO:YES;
    
    if (self.singleViewBtn.selected) {
        self.pageMode.text = FSLocalizedForKey(@"kViewModeSingle");
    }else if (self.doubleViewBtn.selected){
        self.pageMode.text = FSLocalizedForKey(@"kViewModeNoCover");
    }else if (self.coverBtn.selected){
        self.pageMode.text = FSLocalizedForKey(@"kViewModeCover");
    }else{
        self.pageMode.text = @"";
    }
    if ([_pdfViewCtrl getZoomMode] == PDF_DISPLAY_ZOOMMODE_FITPAGE) {
        self.pageShow.text = FSLocalizedForKey(@"kFitPage");
    }else if ([_pdfViewCtrl getZoomMode] == PDF_DISPLAY_ZOOMMODE_FITWIDTH){
        self.pageShow.text = FSLocalizedForKey(@"kFitWidth");
    }else{
        self.pageShow.text = @"";
    }
}

+ (UIButton *)createItemWithImageAndTitle:(NSString *)title
                              imageNormal:(UIImage *)imageNormal
                            imageSelected:(UIImage *)imageSelected
                             imageDisable:(UIImage *)imageDisabled {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize titleSize = [Utility getTextSize:title fontSize:12.0f maxSize:CGSizeMake(300, 200)];

    float width = imageNormal.size.width;
    float height = imageNormal.size.height;
    button.contentMode = UIViewContentModeScaleToFill;
    [button setImage:imageNormal forState:UIControlStateNormal];
    [button setImage:imageSelected forState:UIControlStateHighlighted];
    [button setImage:imageSelected forState:UIControlStateSelected];

    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont systemFontOfSize:12.0f];

    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    button.frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width + 2 : width, titleSize.height + height);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    return button;
}

+ (UIImage *)imageByApplyingAlpha:(UIImage *)image alpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);

    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);

    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);

    CGContextSetAlpha(ctx, alpha);

    CGContextDrawImage(ctx, area, image.CGImage);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}

#pragma mark IAppLifecycleListener

- (void)applicationWillResignActive:(UIApplication *)application {
    if (!self.isEnterBg) {
        if (_pdfViewCtrl.currentDoc) {
            [UIScreen mainScreen].brightness = self.tempSysBrightness;
            self.isActive = YES;
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if (!self.isActive) {
        if (_pdfViewCtrl.currentDoc) {
            [UIScreen mainScreen].brightness = self.tempSysBrightness;
            self.isEnterBg = YES;
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if (self.isEnterBg) {
        self.tempSysBrightness = [UIScreen mainScreen].brightness;
        if (_pdfViewCtrl.currentDoc) {
            self.brightnessSwitch.on = !_isBrightnessManual;
            [self onSwitchClicked];
            self.isEnterBg = NO;
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if (self.isActive) {
        self.tempSysBrightness = [UIScreen mainScreen].brightness;
        if (_pdfViewCtrl.currentDoc) {
            self.brightnessSwitch.on = !_isBrightnessManual;
            [self onSwitchClicked];
            self.isActive = NO;
        }
    }
}

#pragma mark IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    [self updateLayoutButtonsWithLayout:[_pdfViewCtrl getPageLayoutMode]];
}

// todo
- (void)readStarted {
    self.brightnessSwitch.on = !_isBrightnessManual;
    self.tempSysBrightness = [UIScreen mainScreen].brightness;
    [self onSwitchClicked];
}

- (void)readDestroy {
    [UIScreen mainScreen].brightness = self.tempSysBrightness;
}

- (void)orientationChanged:(NSNotification *)note {
}
@end
