/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SearchViewController.h"
#import "FSBaseViews.h"


@implementation SearchbyViewController

@synthesize searchbyClickedHandler = _searchbyClickedHandler;

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        _titles = [[NSArray alloc] initWithObjects:
                                       FSLocalizedForKey(@"kCaseSensitive"),
                                       FSLocalizedForKey(@"kWholeWordsOnly"),
                                       FSLocalizedForKey(@"kSearchInInternet"),
                                       nil];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.scrollEnabled = NO;
    self.navigationItem.title = FSLocalizedForKey(@"kSearch");
    self.tableView.backgroundColor = UIColor_DarkMode(self.tableView.backgroundColor, ThemePopViewDividingLineColor);
    self.tableView.separatorColor = ThemePopViewDividingLineColor;
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)searchSwitch:(UISwitch *)sender{
    if (self.searchbyClickedHandler) {
        self.searchbyClickedHandler((int) sender.tag - 100, sender.on);
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"searchbyCellIdentifier";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.backgroundColor = self.tableView.backgroundColor;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        if (indexPath.row == 0 || indexPath.row == 1) {
            FSSwitch *searchSwitch = [[FSSwitch alloc] initWithFrame:CGRectZero];
            [searchSwitch addTarget:self action:@selector(searchSwitch:) forControlEvents:UIControlEventTouchUpInside];
            searchSwitch.tag = indexPath.row + 100;
            [cell.contentView addSubview:searchSwitch];
            [searchSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(cell.contentView.mas_right).offset(-15);
                make.centerY.equalTo(cell.contentView);
            }];
        }
    }
    cell.textLabel.text = [_titles objectAtIndex:indexPath.row];
    cell.textLabel.textColor = BlackThemeTextColor;
    if (DEVICE_iPHONE) {
        cell.textLabel.font = [UIFont fontWithName:cell.textLabel.font.fontName size:15.f];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.searchbyClickedHandler && indexPath.row == 2) {
        self.searchbyClickedHandler(2, YES);
    }
}

@end
