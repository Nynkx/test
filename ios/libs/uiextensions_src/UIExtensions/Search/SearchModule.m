/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SearchModule.h"
#import "SearchResult.h"
#import "SearchViewController.h"
#import "StringDrawUtil.h"
#import "TaskServer.h"

#define STYLE_CELL_WIDTH 300
#define STYLE_PAGE_SUMMARY_HEIGHT 25
#define STYLE_PAGE_SUMMARY_LEFT 15
#define STYLE_PAGE_SUMMARY_TOP 6
#define STYLE_PAGE_SUMMARY_WIDTH 200
#define STYLE_PAGE_SUMMARY_WIDTH_IPHONE 260
#define STYLE_PAGE_SUMMARY_LABEL_HEIGHT (STYLE_PAGE_SUMMARY_HEIGHT - 2 * STYLE_PAGE_SUMMARY_TOP)
#define STYLE_PAGE_SUMMARY_RIGHT_LEFT 107
#define STYLE_PAGE_SUMMARY_RIGHT_LEFT_IPHONE 47
#define STYLE_PAGE_SUMMARY_FONT [UIFont boldSystemFontOfSize:18.0]
#define STYLE_INFO_LEFT 15
#define STYLE_INFO_TOP 5
#define STYLE_INFO_FONT [UIFont systemFontOfSize:13.0]

#define SEARCH_BAR_TEXT [self.searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] //remove prefixed and suffixed whitespace

@interface UIFSSearchCancelCallback : NSObject<FSSearchCancelCallback>
@property (nonatomic, assign) BOOL cancelNow;
-(BOOL)needToCancelNow;
@end

@implementation UIFSSearchCancelCallback
- (instancetype)init {
    self = [super init];
    if (self) {
        self.cancelNow = NO;
    }
    return self;
}

-(BOOL)needToCancelNow
{
    return self.cancelNow;
}
@end

@interface SearchModule () <IRotationEventListener, UIPopoverPresentationControllerDelegate>

@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;

@property (nonatomic, strong) FSToolbar *topBar;
@property (nonatomic, strong) TbBaseBar *topBaseBar;
@property (nonatomic, strong) TbBaseItem *filterItem;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) TbBaseItem *cancelItem;

@property (nonatomic, strong) FSToolbar *bottomBar;
@property (nonatomic, strong) TbBaseBar *bottomBaseBar;
@property (nonatomic, strong) TbBaseItem *previousItem;
@property (nonatomic, strong) TbBaseItem *nextItem;
@property (nonatomic, strong) TbBaseItem *showListItem;

@property (nonatomic, strong) UIControl *maskView;
@property (nonatomic, strong) UILabel *foundLable;
@property (nonatomic, strong) UIView *totalView;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) BOOL topBaseBarHidden;
@property (nonatomic, assign) BOOL bottomBaseBarHidden;
@property (nonatomic, assign) BOOL tableviewHidden;

@property (nonatomic, strong) NSMutableArray *arraySearch;

@property (nonatomic, strong) UIViewController *searchbyController;

@property (nonatomic, strong) NSOperationQueue *searchOPQueue;

@property (nonatomic, strong) NSArray *needDrawRects;
@property (nonatomic, assign) int needPageIndex;
@property (nonatomic, assign) CGRect cleanDrawRect;
@property (nonatomic, assign) int cleanPageIndex;

@property (nonatomic, strong) SearchInfo *selectedSearchInfo;
@property (nonatomic, strong) SearchResult *selectedSearchResult;
@property (nonatomic, assign) BOOL isKeyboardShowing;

@property (nonatomic, assign) BOOL onSearchState;

@property (nonatomic, assign) unsigned int textSearchSearchFlags;
@property (nonatomic, strong) NSString *textSearchKeyWords;
@property (nonatomic, strong) UIFSSearchCancelCallback *searchCancelCallBack;

@end

@implementation SearchModule {
}

- (NSString *)getName {
    return @"Search";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [extensionsManager registerRotateChangedListener:self];
        _extensionsManager = extensionsManager;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRotate) name:UIApplicationWillChangeStatusBarOrientationNotification object:nil];
        _textSearchSearchFlags = FSTextSearchSearchNormal;
        self.textSearchKeyWords = nil;
        self.searchCancelCallBack = nil;
    }

    if ([UIBarButtonItem respondsToSelector:@selector(appearanceWhenContainedIn:)]) {
        [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[ [UISearchBar class] ]] setTintColor:WhiteThemeTextColor];
    }
    [self loadModule];
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.extensionsManager unregisterRotateChangedListener:self];
}

- (void)loadModule {
    [_pdfViewCtrl registerDrawEventListener:self];
    [_pdfViewCtrl registerScrollViewEventListener:self];
    [_pdfViewCtrl registerGestureEventListener:self];
    [_pdfViewCtrl registerDocEventListener:self];

    self.arraySearch = [NSMutableArray array];
    self.searchOPQueue = [[NSOperationQueue alloc] init];
    _selectedSearchInfo = nil;
    _selectedSearchResult = nil;

    self.maskView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_pdfViewCtrl.bounds), CGRectGetHeight(_pdfViewCtrl.bounds))];
    self.maskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
    
    self.topBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
    [self.pdfViewCtrl addSubview:self.topBar];
    [self.topBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.pdfViewCtrl);
    }];
    
    self.topBaseBar = [[TbBaseBar alloc] init];
    self.topBaseBar.contentView.frame = CGRectMake(0, 0, CGRectGetWidth(_pdfViewCtrl.bounds), 44);
    [self.topBar addSubview:self.topBaseBar.contentView];
    [self.topBaseBar.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.topBaseBar.contentView.superview);
    }];

    __weak typeof(self) weakSelf = self;
    self.filterItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"common_setting_gray" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    self.filterItem.onTapClick = ^(TbBaseItem *item) {
        if (weakSelf.searchbyController.presentingViewController) {
            [weakSelf.searchbyController.presentingViewController dismissViewControllerAnimated:true completion:nil];
        } else {
            CGRect rect = [weakSelf.filterItem.contentView convertRect:weakSelf.filterItem.contentView.bounds toView:weakSelf.pdfViewCtrl];
            
            FSNavigationController *searchByNavCtrl = [[FSNavigationController alloc] initWithRootViewController:weakSelf.searchbyController];
            searchByNavCtrl.navigationBarHidden = YES;
            searchByNavCtrl.preferredContentSize = DEVICE_iPHONE ? CGSizeMake(220, 44 * 3) : CGSizeMake(250, 44 * 3);
            searchByNavCtrl.modalPresentationStyle = UIModalPresentationPopover;
            searchByNavCtrl.popoverPresentationController.delegate = weakSelf;
            searchByNavCtrl.popoverPresentationController.sourceView = weakSelf.pdfViewCtrl;
            searchByNavCtrl.popoverPresentationController.sourceRect = rect;
            searchByNavCtrl.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
            [weakSelf.pdfViewCtrl.fs_viewController presentViewController:searchByNavCtrl animated:true completion:nil];
        }
    };
    [self.topBaseBar addItem:self.filterItem displayPosition:Position_LT];
    [self.filterItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(ITEM_MARGIN_LITTLE_NORMAL);
        make.centerY.mas_equalTo(self.filterItem.contentView.superview);
        make.size.mas_equalTo(self.filterItem.contentView.fs_size);
    }];

    self.cancelItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kCancel")];
    self.cancelItem.textColor = ThemeLikeBlueColor;
    self.cancelItem.textFont = [UIFont systemFontOfSize:15.0f];
    [self.topBaseBar addItem:self.cancelItem displayPosition:Position_RB];
    [self.cancelItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-ITEM_MARGIN_LITTLE_NORMAL);
        make.centerY.mas_equalTo(self.filterItem.contentView);
        make.size.mas_equalTo(self.cancelItem.contentView.fs_size);
    }];
    self.cancelItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf cancelSearchWithExit:NO];
    };

    CGSize cancelSize = [Utility getTextSize:FSLocalizedForKey(@"kCancel") fontSize:15.0f maxSize:CGSizeMake(200, 100)];

    CGRect searchFrame;
    if (DEVICE_iPHONE) {
        searchFrame = CGRectMake(46, 25, CGRectGetWidth(_pdfViewCtrl.bounds) - cancelSize.width - 5 - 65, 30);
    } else {
        searchFrame = CGRectMake(46, 25, CGRectGetWidth(_pdfViewCtrl.bounds) - cancelSize.width - 5 - 65, 30);
    }

    self.searchBar = [[UISearchBar alloc] initWithFrame:searchFrame];
    self.searchBar.placeholder = FSLocalizedForKey(@"kTrimWhitespace");
    
    [self.topBaseBar.contentView addSubview:self.searchBar];
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.filterItem.contentView);
        make.left.equalTo(self.topBaseBar.contentView.mas_left).offset(40);
        make.right.equalTo(self.topBaseBar.contentView.mas_right).offset(-(cancelSize.width + (DEVICE_iPHONE ? 15 : 19)));
        make.height.mas_equalTo(30);
    }];

    self.searchBar.delegate = self;
    if ([self.searchBar respondsToSelector:@selector(barTintColor)]) {
        self.searchBar.backgroundImage = [UIImage new];
    } else {
        [[self.searchBar.subviews objectAtIndex:0] removeFromSuperview];
        [self.searchBar setBackgroundColor:[UIColor whiteColor]];
    }
    
    self.bottomBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];
    [self.pdfViewCtrl addSubview:self.bottomBar];
    [self.bottomBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.pdfViewCtrl);
    }];

    self.bottomBaseBar = [[TbBaseBar alloc] init];
    self.bottomBaseBar.contentView.frame = CGRectMake(0, CGRectGetHeight(_pdfViewCtrl.bounds), CGRectGetWidth(_pdfViewCtrl.bounds), 49);
    
    if (DEVICE_iPHONE) {
        self.bottomBaseBar.intervalWidth = 50;
    } else {
        self.bottomBaseBar.intervalWidth = 100;
    }
    [self.bottomBar addSubview:self.bottomBaseBar.contentView];
    [self.bottomBaseBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.bottomBaseBar.contentView.superview);
    }];

    self.previousItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"search_previous" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"search_previous" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"search_previous_disable" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    self.previousItem.tag = 0;
    [self.bottomBaseBar addItem:self.previousItem displayPosition:Position_CENTER];
    self.previousItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf previousKeyLocation];
    };

    self.showListItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"search_showlist" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"search_showlist_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"search_showlist_disable" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    self.showListItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf setTableViewHidden:NO];
        [weakSelf setBottomBaseBarHidden:YES];
    };
    [self.bottomBaseBar addItem:self.showListItem displayPosition:Position_CENTER];

    self.nextItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"search_next" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"search_next" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"search_next_disable" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    [self.bottomBaseBar addItem:self.nextItem displayPosition:Position_CENTER];
    self.nextItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf nextKeyLocation];
    };
    
    [self.previousItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.showListItem.contentView.mas_left).offset(-self.bottomBaseBar.intervalWidth);
        make.centerY.mas_equalTo(self.previousItem.contentView.superview);
        make.size.mas_equalTo(self.previousItem.contentView.fs_size);
    }];
    
    [self.showListItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.showListItem.contentView.superview);
        make.size.mas_equalTo(self.showListItem.contentView.fs_size);
    }];
    
    [self.nextItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.showListItem.contentView.mas_right).offset(self.bottomBaseBar.intervalWidth);
        make.centerY.mas_equalTo(self.nextItem.contentView.superview);
        make.size.mas_equalTo(self.nextItem.contentView.fs_size);
    }];

    self.foundLable = [[UILabel alloc] initWithFrame:CGRectMake(STYLE_PAGE_SUMMARY_LEFT, 0, 300, 30)];
    self.foundLable.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
    self.foundLable.backgroundColor = [UIColor whiteColor];
    self.foundLable.textColor = BlackThemeTextColor;
    self.foundLable.font = [UIFont systemFontOfSize:12.0f];
    self.foundLable.textAlignment = NSTextAlignmentLeft;

    self.totalView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, 300, 30)];
    self.totalView.backgroundColor = UIColor_DarkMode([UIColor fs_whiteColor], ThemeCellBackgroundColor);
    
    self.foundLable.backgroundColor = self.totalView.backgroundColor;

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetWidth(_pdfViewCtrl.bounds) - 300, 94, 300, CGRectGetHeight(_pdfViewCtrl.bounds) - 94) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = ThemeCellBackgroundColor;
    self.tableView.separatorColor = DividingLineColor ;
    
    [_pdfViewCtrl addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.tableView.superview.mas_bottom).offset(0);
        make.left.mas_equalTo(self.tableView.superview.mas_right);
        make.width.mas_equalTo(300);
    }];

    [_pdfViewCtrl addSubview:self.totalView];
    [self.totalView addSubview:self.foundLable];
    [self.totalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topBar.mas_bottom);
        make.bottom.equalTo(self.tableView.mas_top);
        make.height.mas_equalTo(@30);
        make.width.left.mas_equalTo(self.tableView);
    }];

    self.topBaseBarHidden = YES;
    self.bottomBaseBarHidden = YES;
    self.tableviewHidden = YES;
    
    //search button.
    UIButton *searchButton = [Utility createButtonWithImage:[UIImage imageNamed:@"search.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    searchButton.tag = FS_TOPBAR_ITEM_SEARCH_TAG;
    [searchButton addTarget:self action:@selector(onClickSearchButton:) forControlEvents:UIControlEventTouchUpInside];
    _extensionsManager.topToolbar.items = ({
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
        item.tag = FS_TOPBAR_ITEM_SEARCH_TAG;
        NSMutableArray *items = (_extensionsManager.topToolbar.items ?: @[]).mutableCopy;
        [items addObject:item];
        items;
    });
}

- (void)unload {
}

- (void)onClickSearchButton:(UIButton *)button {
    [self.extensionsManager changeState:STATE_SEARCH];
}

- (void)willRotate {
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
}

- (UIViewController *)searchbyController {
    if (!_searchbyController) {
        SearchbyViewController *searchByCtr = [[SearchbyViewController alloc] initWithStyle:UITableViewStylePlain];
        searchByCtr.searchbyClickedHandler = ^(int type, BOOL flag) {
            if (type == 0) {
                if (flag) {
                    self.textSearchSearchFlags |= FSTextSearchSearchMatchCase;
                }else{
                    self.textSearchSearchFlags &= ~FSTextSearchSearchMatchCase;
                }
                [self searchBarSearchButtonClicked:self.searchBar];
            } else if (type == 1) {
                if (flag) {
                    self.textSearchSearchFlags |= FSTextSearchSearchMatchWholeWord;
                }else{
                    self.textSearchSearchFlags &= ~FSTextSearchSearchMatchWholeWord;
                }
                [self searchBarSearchButtonClicked:self.searchBar];
            }else{
                [self buttonSearchClick:nil];
            }
        };
        _searchbyController = searchByCtr;
    }
    return _searchbyController;
}

#pragma mark <UIPopoverPresentationControllerDelegate>

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection {
    return UIModalPresentationNone;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView  * __nonnull * __nonnull)view{
    CGRect showRect = [self.filterItem.contentView convertRect:self.filterItem.contentView.bounds toView:self.pdfViewCtrl];
    *rect = showRect;
}

- (void)showSearchBar:(BOOL)show {
    if (show) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardWillHideNotification object:nil];

        [self setTopBaseBarHidden:NO];
        [self.searchBar becomeFirstResponder];
        self.onSearchState = YES;

        [_extensionsManager onSearchStarted];
    } else {
        [self setTopBaseBarHidden:YES];
        [self.searchBar resignFirstResponder];
        [self setBottomBaseBarHidden:YES];
        [self setTableViewHidden:YES];
        self.onSearchState = NO;

        [_extensionsManager onSearchCanceled];
        self.textSearchKeyWords = nil;
    }
}

- (void)setTopBaseBarHidden:(BOOL)hidden {
    if (_topBaseBarHidden == hidden) {
        return;
    }
    _topBaseBarHidden = hidden;
    
    if (hidden) {
        [self.topBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self->_pdfViewCtrl);
            make.bottom.equalTo(self->_pdfViewCtrl.mas_top);
        }];
        
    } else {
        [self.topBar mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.left.right.equalTo(self->_pdfViewCtrl);
        }];
        
    }
    
    if (self->_pdfViewCtrl.window) {
        [UIView animateWithDuration:0.3f animations:^{
            [self->_pdfViewCtrl layoutIfNeeded];
        }];
    }
}

- (void)setBottomBaseBarHidden:(BOOL)hidden{
    if (_bottomBaseBarHidden == hidden) {
        return;
    }
    _bottomBaseBarHidden = hidden;
    if (hidden) {
        [self.bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self->_pdfViewCtrl);
            make.top.equalTo(self->_pdfViewCtrl.mas_bottom);
        }];
    } else {
        
        [self.bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self->_pdfViewCtrl);
        }];
    }
    
    if (self->_pdfViewCtrl.window) {
        [UIView animateWithDuration:0.3f animations:^{
            [self->_pdfViewCtrl layoutIfNeeded];
        }];
    }
}

- (void)setTableViewHidden:(BOOL)hidden {
    if (_tableviewHidden == hidden) {
        return;
    }
    _tableviewHidden = hidden;
    if (hidden) {
        [UIView animateWithDuration:0.3
                         animations:^{
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tableView.superview.mas_right).offset(0);
        }];
                             [self.tableView layoutIfNeeded];
                         }];

        [UIView animateWithDuration:0.4
            animations:^{
                self.maskView.alpha = 0.1f;
            }
            completion:^(BOOL finished) {

                [self.maskView removeFromSuperview];
            }];
    } else {
        self.maskView.backgroundColor = [UIColor blackColor];
        self.maskView.alpha = 0.3f;
        self.maskView.tag = 300;
        [self.maskView addTarget:self action:@selector(dissmiss:) forControlEvents:UIControlEventTouchUpInside];
        [_pdfViewCtrl insertSubview:self.maskView belowSubview:self.topBar];
        [self.maskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(self.pdfViewCtrl);
        }];
        
        [UIView animateWithDuration:0.3
                         animations:^{
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
              make.left.equalTo(self.tableView.superview.mas_right).offset(-300);
          }];
            [self.tableView layoutIfNeeded];
                         }];
        }
}

- (void)dissmiss:(id)sender {
    UIControl *control = (UIControl *) sender;
    if (control.tag == 300) {
        [self setTableViewHidden:YES];
        [self setBottomBaseBarHidden:NO];
    }
}

- (void)buttonSearchClick:(id)sender {
    [self.searchbyController.presentingViewController dismissViewControllerAnimated:true completion:nil];
    NSString *keyword = SEARCH_BAR_TEXT;
    NSString *url;
    if (keyword.length > 0) {
        url = [NSString stringWithFormat:@"http://www.google.com/search?q=%@", [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    } else {
        url = @"http://www.google.com";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)previousKeyLocation {
    if (_selectedSearchResult == nil) {
        if (self.arraySearch.count == 0) {
            return;
        } else {
            self.selectedSearchResult = [self.arraySearch objectAtIndex:0];
        }
    }
    if (_selectedSearchInfo == nil) {
        self.selectedSearchInfo = [self.selectedSearchResult.infos objectAtIndex:self.selectedSearchResult.infos.count - 1];
        [self gotoPage:self.selectedSearchResult.index rects:self.selectedSearchInfo.rects];
    } else {
        NSUInteger infoIndex = [self.selectedSearchResult.infos indexOfObject:self.selectedSearchInfo];
        if (infoIndex == 0) {
            if (self.arraySearch.count < 1)
                return;
            NSUInteger resultIndex = [self.arraySearch indexOfObject:self.selectedSearchResult];
            if (resultIndex == 0) {
                self.selectedSearchResult = [self.arraySearch lastObject];
            } else {
                self.selectedSearchResult = [self.arraySearch objectAtIndex:resultIndex - 1];
            }
            self.selectedSearchInfo = [self.selectedSearchResult.infos lastObject];
        } else {
            self.selectedSearchInfo = [self.selectedSearchResult.infos objectAtIndex:infoIndex - 1];
        }
        [self gotoPage:self.selectedSearchResult.index rects:self.selectedSearchInfo.rects];
    }
}

- (void)nextKeyLocation {
    if (_selectedSearchResult == nil) {
        if (self.arraySearch.count != 0) {
            self.selectedSearchResult = [self.arraySearch objectAtIndex:0];
        } else {
            return;
        }
    }

    if (_selectedSearchInfo == nil) {
        self.selectedSearchInfo = [self.selectedSearchResult.infos objectAtIndex:0];
        [self gotoPage:_selectedSearchResult.index rects:self.selectedSearchInfo.rects];
    } else {
        NSUInteger infoIndex = [self.selectedSearchResult.infos indexOfObject:self.selectedSearchInfo];
        if (infoIndex == self.selectedSearchResult.infos.count - 1) {
            if (self.arraySearch.count < 1)
                return;
            NSUInteger resultIndex = [self.arraySearch indexOfObject:self.selectedSearchResult];
            if (resultIndex == self.arraySearch.count - 1) {
                self.selectedSearchResult = [self.arraySearch objectAtIndex:0];
            } else {
                self.selectedSearchResult = [self.arraySearch objectAtIndex:resultIndex + 1];
            }
            self.selectedSearchInfo = [self.selectedSearchResult.infos objectAtIndex:0];
        } else {
            self.selectedSearchInfo = [self.selectedSearchResult.infos objectAtIndex:infoIndex + 1];
        }
        [self gotoPage:self.selectedSearchResult.index rects:self.selectedSearchInfo.rects];
    }
}

- (void)gotoPage:(int)index rects:(NSArray *)rects {
    if ([self gotoPage:index animated:YES]) {
        if (rects && rects.count > 0) {
            self.needDrawRects = rects;
            CGRect firstNeedDrawRect = [[rects objectAtIndex:0] CGRectValue];
            CGRect unionNeedDrawRect = CGRectZero;
            for (id obj in rects) {
                CGRect rect = [obj CGRectValue];
                unionNeedDrawRect = CGRectUnion(unionNeedDrawRect, rect);
            }
            
            self.needPageIndex = index;
            if ([self.pdfViewCtrl isContinuous]) {
                FSPointF *point = [[FSPointF alloc] init];
                [point set:CGRectGetMidX(firstNeedDrawRect) - 80 y:CGRectGetMidY(firstNeedDrawRect) + 150];
                [_pdfViewCtrl gotoPage:index withDocPoint:point animated:YES];
            } else {
                FSPointF *point = [[FSPointF alloc] init];
                [point set:CGRectGetMidX(firstNeedDrawRect) y:CGRectGetMidY(firstNeedDrawRect)];
                
                point.x -= 20;
                point.y += 20;

                UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
                if (DEVICE_iPHONE && (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)) {
                    //Avoid the search highlight to be sheltered from top bar. To do, need to check page rotation.
                    [point setY:[point getY] + 64];
                }

                [_pdfViewCtrl gotoPage:index withDocPoint:point animated:YES];
            }

            FSRectF *docRectIva = [Utility CGRect2FSRectF:unionNeedDrawRect];
            FSRectF *oldRectIva = [Utility CGRect2FSRectF:self.cleanDrawRect];
            CGRect pvRectIna = [_pdfViewCtrl convertPdfRectToPageViewRect:docRectIva pageIndex:self.needPageIndex];
            CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:oldRectIva pageIndex:self.cleanPageIndex];
            [_pdfViewCtrl refresh:pvRectIna pageIndex:self.needPageIndex];
            [_pdfViewCtrl refresh:oldRect pageIndex:self.cleanPageIndex];
            self.cleanDrawRect = unionNeedDrawRect;
            self.cleanPageIndex = index;
            [self setTableViewHidden:YES];
            [self setBottomBaseBarHidden:NO];
        }
    }
}

- (BOOL)gotoPage:(int)index animated:(BOOL)animated {
    if (index >= 0 && index < [_pdfViewCtrl.currentDoc getPageCount]) {
        [_pdfViewCtrl gotoPage:index animated:animated];
        return YES;
    }
    return NO;
}

- (void)clearSearch {
    self.searchCancelCallBack.cancelNow = YES;
    [self.searchBar setText:nil];
    [self.arraySearch removeAllObjects];
    [self.searchOPQueue cancelAllOperations];
    self.needDrawRects = nil;
    self.textSearchKeyWords = nil;
    [self.tableView reloadData];
}

- (void)cancelSearchWithExit:(BOOL)exit {
    [self showSearchBar:NO];
    [self clearSearch];

    if (!exit) {
        FSRectF *oldRectIva = [Utility CGRect2FSRectF:self.cleanDrawRect];
        CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:oldRectIva pageIndex:self.cleanPageIndex];
        [_pdfViewCtrl refresh:oldRect pageIndex:self.cleanPageIndex];
    }

    [self.searchBar resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

    //If there is still in searching, wait for finishing.
    Task *task = [[Task alloc] init];
    task.run = ^() {

    };
    [self.extensionsManager.taskServer executeSync:task];
}

-(void)getSearchResult:(NSString *)searchWords withSearchRange:(NSRange)range completed:(void (^)(SearchResult *ret,int pageIndex))completed{
    NSMutableDictionary *pageResultDict = @{}.mutableCopy;
    self.searchCancelCallBack = [[UIFSSearchCancelCallback alloc] init];
    int searchPageIndex = (int)range.location;
    
    FSPDFDoc *document = self->_pdfViewCtrl.currentDoc;
    int oldpageIndex = 0;
    @try {
        FSTextSearch *textSearch = [[FSTextSearch alloc] initWithDocument:document cancel:self.searchCancelCallBack flags: FSTextPageParseTextUseStreamOrder];
        [textSearch setStartPage:searchPageIndex];
        if (range.length != 0) {
            [textSearch setEndPage:(int)range.location + (int)range.length];
        }
        [textSearch setPattern:searchWords];
        [textSearch setSearchFlags:self.textSearchSearchFlags];
        
        BOOL bRet = [textSearch findNext];
        while (bRet) {
            int pageIndex = [textSearch getMatchPageIndex];
            SearchResult *result = [pageResultDict objectForKey:@(pageIndex)];
            if (result == nil) {
                result = [[SearchResult alloc] init];
                result.index = pageIndex;
                result.infos = [NSMutableArray array];
                
                [pageResultDict setObject:result forKey:@(pageIndex)];
            }
            
            NSString *sentence = [textSearch getMatchSentence];
            int sentencePos = [textSearch getMatchSentenceStartIndex];
            FSRectFArray *rects = [textSearch getMatchRects];
            
            SearchInfo *info = [[SearchInfo alloc] init];
            info.snippet = sentence;
            info.keywordLocation = sentencePos;
            if (sentence.length < searchWords.length) {
                info.snippet = [searchWords stringByReplacingCharactersInRange:NSMakeRange(0, sentence.length-sentencePos) withString:sentence];
            }
            
            info.rects = [NSMutableArray array];
            for (int i = 0; i < [rects getSize]; i++) {
                FSRectF *fsrect = [rects getAt:i];
                [info.rects addObject:[NSValue valueWithCGRect:[Utility FSRectF2CGRect:fsrect]]];
            }
            
            [result.infos addObject:info];
            
            if (pageIndex != oldpageIndex) {
                SearchResult *lastResult = [pageResultDict objectForKey:@(oldpageIndex)];
                completed(lastResult,oldpageIndex);
                
                oldpageIndex = pageIndex;
            }

            bRet = [textSearch findNext];
        }

        SearchResult *lastResult = [pageResultDict objectForKey:@(oldpageIndex)];
        completed(lastResult,oldpageIndex);
        
    } @catch (NSException *e) {
    }
}

- (void)searchTextInPDF:(NSString *)str currentPage:(NSArray *)pages {
    self.textSearchKeyWords = str;
    [self.searchOPQueue cancelAllOperations];
    if (str.length == 0 || str == nil) {
        return;
    }
    if (pages == nil || pages.count == 0) {
        return;
    }

    [self setTableViewHidden:NO];
    __block int totalCount = 0;
    __block NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
        
        [self getSearchResult:str withSearchRange:NSMakeRange(0, 0) completed:^(SearchResult *ret, int pageIndex) {
            totalCount += ret.infos.count;

            dispatch_async(dispatch_get_main_queue(), ^{
                if (ret.infos.count > 0) {
                    [self searchFound:ret];
                }

                if (self.arraySearch.count == 0) {
                    self.foundLable.text = [NSString stringWithFormat:@"%@  %d", FSLocalizedForKey(@"kTotalFound"), 0];
                    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
                    [self.tableView reloadData];
                    self.previousItem.enable = NO;
                    self.nextItem.enable = NO;
                }
            });
            
        }];
        
    }];
    [self.searchOPQueue addOperation:op];
}

- (void)searchOnePageProcessed {
}

- (void)searchStart {
}

- (void)searchStoped {
}

- (void)searchFound:(SearchResult *)result {
    [self.arraySearch addObject:result];
    int totalCount = 0;
    for (SearchResult *result in self.arraySearch) {
        totalCount += result.infos.count;
    }
    self.foundLable.text = [NSString stringWithFormat:@"%@  %d", FSLocalizedForKey(@"kTotalFound"), totalCount];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView reloadData];
    self.previousItem.enable = YES;
    self.showListItem.enable = YES;
    self.nextItem.enable = YES;
}

- (NSString *)generateRText:(NSString *)content searchKeyword:(NSString *)keyword realLocation:(int)realIndex {
    //Replace multi whitespace with one.
    for (int index = 0; index < keyword.length; index++) {
        if ([keyword characterAtIndex:index] == 32) {
            int j = index;
            for (; [keyword characterAtIndex:j] == 32; j++)
                ;
            NSRange replaceWhitespace = {index, j - index};
            keyword = [keyword stringByReplacingCharactersInRange:replaceWhitespace withString:@" "];
        }
    }

    NSArray *contentArray = [StringDrawUtil seperateString:content bySeparator:keyword];
    NSString *rtText = @"";
    int highlightIndex = 0;
    NSStringCompareOptions options = NSCaseInsensitiveSearch;
    if ((self.textSearchSearchFlags & FSTextSearchSearchMatchCase) == FSTextSearchSearchMatchCase) {
        options = NSLiteralSearch;
    }

    for (NSString *str in contentArray) {
        if ([str compare:keyword options:options] == NSOrderedSame) {
            if (highlightIndex == realIndex) {
                rtText = [rtText stringByAppendingFormat:@"<font face='%@' size=%d color='#dd1100'><b>%@</b></font>", STYLE_INFO_FONT.fontName, (int) STYLE_INFO_FONT.pointSize, str];
            } else {
                rtText = [rtText stringByAppendingFormat:@"<font face='%@' size=%d color='#dd1100'>%@</font>", STYLE_INFO_FONT.fontName, (int) STYLE_INFO_FONT.pointSize, str];
            }
            highlightIndex++;
        } else {
            rtText = [rtText stringByAppendingFormat:@"<font face='%@' size=%d color='#000000'>%@</font>", STYLE_INFO_FONT.fontName, (int) STYLE_INFO_FONT.pointSize, str];
        }
    }
    return rtText;
}

#pragma mark - UISearchBar delegate handler

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self.searchbyController.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
   [self.searchbyController.presentingViewController dismissViewControllerAnimated:true completion:nil];
    if (SEARCH_BAR_TEXT.length > 0) {
        [self.searchBar resignFirstResponder];
        NSString *searchKey = SEARCH_BAR_TEXT;

        NSMutableArray *pages = [NSMutableArray array];
        for (int i = 0; i < [_pdfViewCtrl.currentDoc getPageCount]; i++) {
            [pages addObject:@(i)];
        }
        self.selectedSearchResult = nil;
        self.selectedSearchInfo = nil;
        [self.arraySearch removeAllObjects];
        [self.tableView reloadData];
        self.foundLable.text = FSLocalizedForKey(@"kSearching");

        @try {
            [self searchTextInPDF:searchKey currentPage:pages];
        }
        @catch (NSException *exception) {
            if ([[exception name] isEqualToString:@"OutOfMemory"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [FSPDFViewCtrl recoverForOOM];
                });
                return;
            }
        }

        [self setBottomBaseBarHidden:YES];
    } else {
        [self setTableViewHidden:YES];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    //if (SEARCH_BAR_TEXT == nil || SEARCH_BAR_TEXT == 0) {
    //    [self clearSearch];
    //}
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    //if (SEARCH_BAR_TEXT == nil || SEARCH_BAR_TEXT.length == 0) {
    //    [self clearSearch];
    //}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.arraySearch.count > 0) {
        return self.arraySearch.count; //: arraySearch.count+1/*show total match count*/;
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    SearchResult *oneResult = [self.arraySearch objectAtIndex:section];
    return oneResult.infos.count + 1 /*first line is page summary*/;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    SearchResult *oneResult = [self.arraySearch objectAtIndex:indexPath.section];
    if (indexPath.row == 0) //page summary
    {
        height = STYLE_PAGE_SUMMARY_HEIGHT;
    } else {
        SearchInfo *oneInfo = [oneResult.infos objectAtIndex:indexPath.row - 1];
        
        NSString *text = oneInfo.snippet;
        CGSize textSize = [Utility getTextSize:text fontSize:14.0f maxSize:CGSizeMake(300 - STYLE_INFO_LEFT, 1000)];
        height = MAX(textSize.height + 2 * STYLE_INFO_TOP ,STYLE_PAGE_SUMMARY_HEIGHT);

        if (height < 0 ) {
            height = 0;
        }
    }

    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    if (indexPath.row == 0) //page summary or show total match count
    {
        static NSString *CellIdentifier = @"PageSummaryCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor whiteColor];

            UILabel *labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(STYLE_PAGE_SUMMARY_LEFT, STYLE_PAGE_SUMMARY_TOP, STYLE_PAGE_SUMMARY_WIDTH, STYLE_PAGE_SUMMARY_LABEL_HEIGHT)];
            labelLeft.textAlignment = NSTextAlignmentLeft;
            labelLeft.font = [UIFont systemFontOfSize:12]; //STYLE_PAGE_SUMMARY_FONT;
            labelLeft.textColor = [UIColor colorWithRGB:0x5c5c5c];
            labelLeft.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:labelLeft];
            UILabel *labelRight = [[UILabel alloc] initWithFrame:CGRectMake((DEVICE_iPHONE ? STYLE_PAGE_SUMMARY_RIGHT_LEFT_IPHONE : STYLE_PAGE_SUMMARY_RIGHT_LEFT), STYLE_PAGE_SUMMARY_TOP,
                                                                            (DEVICE_iPHONE ? STYLE_PAGE_SUMMARY_WIDTH_IPHONE : STYLE_PAGE_SUMMARY_WIDTH), STYLE_PAGE_SUMMARY_LABEL_HEIGHT)];
            labelRight.textAlignment = NSTextAlignmentRight;
            labelRight.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            labelRight.font = [UIFont systemFontOfSize:12];
            labelRight.textColor = [UIColor colorWithRGB:0x5c5c5c];
            labelRight.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:labelRight];
        }
        UILabel *labelLeft = [cell.contentView.subviews objectAtIndex:0];
        UILabel *labelRight = [cell.contentView.subviews objectAtIndex:1];
        SearchResult *oneResult = [self.arraySearch objectAtIndex:indexPath.section];
        labelLeft.text = [NSString stringWithFormat:@"%@ %d", FSLocalizedForKey(@"kPage"), oneResult.index + 1 /*page index start from 0*/];
        labelRight.text = [NSString stringWithFormat:@"%lu", (unsigned long) oneResult.infos.count];

        
    } else {
        if ([self.arraySearch count] == 0)
            return nil;
        static NSString *CellIdentifier = @"searchCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *labelContent = nil;
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8];
            UILabel *labelContent = [[UILabel alloc] initWithFrame:CGRectZero];
            labelContent.backgroundColor = [UIColor clearColor];
            labelContent.tag = 1000001;
            labelContent.font = [UIFont systemFontOfSize:14.0];
            labelContent.numberOfLines = 0;
            labelContent.textColor = BlackThemeTextColor;
            [cell.contentView addSubview:labelContent];
        }
        
        if (!labelContent) {
            labelContent = (UILabel *)[cell viewWithTag:1000001];
        }
        SearchResult *oneResult = [self.arraySearch objectAtIndex:indexPath.section];
        
        SearchInfo *oneInfo = [oneResult.infos objectAtIndex:indexPath.row - 1];

        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc]initWithString:oneInfo.snippet];
        NSString *pattern = self.textSearchKeyWords;
        
        if (pattern.length > 0 && contentStr.length > 0) {
            NSRange match = NSMakeRange(oneInfo.keywordLocation, pattern.length);
            [contentStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16.0] range:match];
            [contentStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRGB:0x179cd8 alpha:1] range:match];
        }
        
        [labelContent setAttributedText:contentStr];
        
        NSString *text = oneInfo.snippet;
        CGSize textSize = [Utility getTextSize:text fontSize:14.0f maxSize:CGSizeMake(300 - STYLE_INFO_LEFT, 1000)];
        CGFloat height = MAX(textSize.height + 2 * STYLE_INFO_TOP ,STYLE_PAGE_SUMMARY_HEIGHT);
        if (height < 0 ) {
            height = 0;
        }
        
        labelContent.frame = CGRectMake(STYLE_INFO_LEFT, 0, 300 - STYLE_INFO_LEFT, height);
        
    }
    cell.backgroundColor = ThemeCellBackgroundColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= 0 && indexPath.section < [self.arraySearch count]) {
        SearchResult *oneResult = [self.arraySearch objectAtIndex:indexPath.section];
        if (indexPath.row != 0) //page summary
        {
            SearchInfo *oneInfo = [oneResult.infos objectAtIndex:indexPath.row - 1];
            CGRect firstNeedDrawRect = [oneInfo.rects count] ? [[oneInfo.rects objectAtIndex:0] CGRectValue] : CGRectZero;
            self.needDrawRects = oneInfo.rects;
            CGRect unionNeedDrawRect = CGRectZero;
            for (id obj in oneInfo.rects) {
                CGRect rect = [obj CGRectValue];
                unionNeedDrawRect = CGRectUnion(unionNeedDrawRect, rect);
            }

            self.needPageIndex = oneResult.index;

            if ([self.pdfViewCtrl isContinuous]) {
                FSPointF *point = [[FSPointF alloc] init];
                [point set:CGRectGetMidX(firstNeedDrawRect) - 80 y:CGRectGetMidY(firstNeedDrawRect) + 150];
                [_pdfViewCtrl gotoPage:oneResult.index withDocPoint:point animated:YES];
            } else {
                FSPointF *point = [[FSPointF alloc] init];
                [point set:CGRectGetMidX(firstNeedDrawRect) y:CGRectGetMidY(firstNeedDrawRect)];
                point.x -= 20;
                point.y += 20;
                [_pdfViewCtrl gotoPage:oneResult.index withDocPoint:point animated:YES];
            }

            FSRectF *docRectIva = [Utility CGRect2FSRectF:unionNeedDrawRect];
            FSRectF *oldRectIva = [Utility CGRect2FSRectF:self.cleanDrawRect];
            CGRect pvRectIna = [_pdfViewCtrl convertPdfRectToPageViewRect:docRectIva pageIndex:self.needPageIndex];
            CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:oldRectIva pageIndex:self.cleanPageIndex];
            [_pdfViewCtrl refresh:pvRectIna pageIndex:self.needPageIndex];
            [_pdfViewCtrl refresh:oldRect pageIndex:self.cleanPageIndex];

            self.cleanDrawRect = unionNeedDrawRect;
            self.cleanPageIndex = oneResult.index;
            [self setTableViewHidden:YES];
            [self setBottomBaseBarHidden:NO];
            self.selectedSearchResult = oneResult;
            self.selectedSearchInfo = oneInfo;
            [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}

#pragma mark IDrawEventListener

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (self.onSearchState && pageIndex == self.needPageIndex) {
        for (id obj in self.needDrawRects) {
            CGRect rect = [obj CGRectValue];
            FSRectF *docRectIva = [Utility CGRect2FSRectF:rect];
            CGRect pvRectIna = [_pdfViewCtrl convertPdfRectToPageViewRect:docRectIva pageIndex:pageIndex];
            UIColor *color = [UIColor colorWithRGB:0x179cd8 alpha:0.8];
            CGContextSetFillColorWithColor(context, [color CGColor]);
            CGContextFillRect(context, pvRectIna);
        }
    }
}

#pragma mark IGestureEventListener

- (BOOL)onTap:(UITapGestureRecognizer *)recognizer {
    if (self.onSearchState) {
        if (_isKeyboardShowing) {
            [self.searchBar resignFirstResponder];
        }
    }
    return NO;
}

- (BOOL)onLongPress:(UILongPressGestureRecognizer *)recognizer {
    if (self.onSearchState) {
        if (_isKeyboardShowing) {
            [self.searchBar resignFirstResponder];
        }
    }
    return NO;
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    if (self.onSearchState) {
        _isKeyboardShowing = YES;
    }
}

- (void)keyboardWasHidden:(NSNotification *)aNotification {
    if (self.onSearchState) {
        _isKeyboardShowing = NO;
    }
}

#pragma mark-- IDocEventListener
- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    if (!DEVICE_iPHONE && self.searchbyController.presentingViewController) {
        //        [self.searchbyPopoverCtrl dismissPopoverAnimated:YES];
        [self.searchbyController.presentingViewController dismissViewControllerAnimated:true completion:nil];
    }
    if (![_pdfViewCtrl isDynamicXFA] && self.onSearchState) {
        [self cancelSearchWithExit:YES];
    }
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

#pragma mark - IRotationEventListener

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    self.maskView.frame = CGRectMake(0, 0, CGRectGetWidth(_pdfViewCtrl.bounds), CGRectGetHeight(_pdfViewCtrl.bounds));
}
@end
