/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
#import "UIExtensionsManager.h"
#import "UIExtensionsConfig+private.h"
#import "AnnotationPanel.h"
#import "OutlinePanel.h"
#import "CropViewController.h"
#import "PanAndZoomViewController.h"
#import "AlertView.h"
#import "DXPopover.h"
#import "FSAnnotExtent.h"
#import "MenuControl.h"
#import "MenuItem.h"
#import "NoteDialog.h"
#import "PropertyBar.h"
#import "SettingBar+private.h"
#import "UIButton+Extensions.h"
#import "FSUndo.h"
#import "FSThumbnailCache.h"
#import "FSThumbnailViewController.h"
#import "PanelController.h"
#import "SettingBar.h"
#import "DynamicXFAHandler.h"
#import "AudioPlayerControl.h"

#import "AttachmentModule.h"
#import "EraseModule.h"
#import "FormModule.h"
#import "FreetextModule.h"
#import "ImageModule.h"
#import "InsertModule.h"
#import "LineModule.h"
#import "LinkModule.h"
#import "MarkupModule.h"
#import "MoreModule.h"
#import "NoteModule.h"
#import "PageNavigationModule.h"
#import "PasswordModule.h"
#import "PencilModule.h"
#import "PolygonModule.h"
#import "PolyLineModule.h"
#import "ReadingBookmarkModule.h"
#import "ReflowModule.h"
#import "ReplaceModule.h"
#import "AudioVideoModule.h"
#import "Search/SearchModule.h"
#import "SelectionModule.h"
#import "ShapeModule.h"
#import "SignToolHandler.h"
#import "FillSignModule.h"
#import "StampModule.h"
#import "TextboxModule.h"
#import "CalloutModule.h"
#import "UndoModule.h"
#import "DistanceModule.h"
#import "MultipleSelectionModule.h"
#import "RedactModule.h"
#import "CommentModule.h"
#import "VoiceModule.h"
#import "SignatureModule.h"

#import "AttachmentAnnotHandler.h"
#import "AttachmentToolHandler.h"
#import "CaretAnnotHandler.h"
#import "DigitalSignatureAnnotHandler.h"
#import "EraseToolHandler.h"
#import "FormAnnotHandler.h"
#import "FtAnnotHandler.h"
#import "FtToolHandler.h"
#import "InsertToolHandler.h"
#import "LineAnnotHandler.h"
#import "LineToolHandler.h"
#import "LinkAnnotHandler.h"
#import "NoteAnnotHandler.h"
#import "NoteToolHandler.h"
#import "PencilAnnotHandler.h"
#import "PencilToolHandler.h"
#import "PolygonAnnotHandler.h"
#import "PolygonToolHandler.h"
#import "PolyLineAnnotHandler.h"
#import "PolyLineToolHandler.h"
#import "ReplaceToolHandler.h"
#import "SelectToolHandler.h"
#import "ShapeAnnotHandler.h"
#import "ShapeToolHandler.h"
#import "SignToolHandler.h"
#import "StampAnnotHandler.h"
#import "StampIconController.h"
#import "StampToolHandler.h"
#import "ImageAnnotHandler.h"
#import "AudioVideoAnnotHandler.h"
#import "TextMKAnnotHandler.h"
#import "TextMKToolHandler.h"
#import "RedactToolHandler.h"
#import "MultipleSelectionToolHandler.h"

@implementation UIExtensionsManager {
    UIControl *maskView;
}

- (id)initWithPDFViewControl:(FSPDFViewCtrl *)viewctrl {
    return [self initWithPDFViewControl:viewctrl configuration:nil];
}

- (id)initWithPDFViewControl:(FSPDFViewCtrl *)viewctrl configuration:(NSData *_Nullable)jsonConfigData {
    UIExtensionsConfig *config;
    if (jsonConfigData) {
        config = [[UIExtensionsConfig alloc] initWithJSONData:jsonConfigData];
        if (!config) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kInvalidJSON") message:FSLocalizedForKey(@"kFailedLoadJSON") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
            return nil;
        }
    } else {
        config = [[UIExtensionsConfig alloc] init];
    }
    return [self initWithPDFViewControl:viewctrl configurationObject:config];
}

- (id)initWithPDFViewControl:(FSPDFViewCtrl *)viewctrl configurationObject:(UIExtensionsConfig *_Nonnull)configuration {
    self = [super init];
    _pdfViewCtrl = viewctrl;
    self.config = configuration;
    self.currentPageIndex = -1;
    
    self.fullScreenListeners = [[NSMutableArray alloc] init];
    self.panelListeners = [[NSMutableArray alloc] init];
    self.stateChangeListeners = [NSMutableArray array];
    self.currentState = STATE_NORMAL;

    self.toolHandlers = [NSMutableArray array];
    self.annotHandlers = [NSMutableArray array];
    self.annotListeners = [NSMutableArray array];
    self.docModifiedListeners = [NSMutableArray array];
    self.toolListeners = [NSMutableArray array];
    self.searchListeners = [NSMutableArray array];
    self.currentToolHandler = nil;
    self.propertyBarListeners = [NSMutableDictionary dictionary];
    self.rotateListeners = [[NSMutableArray alloc] init];
    self.annotColors = [NSMutableDictionary dictionary];
    self.annotTextColors = [NSMutableDictionary dictionary];
    self.annotOpacities = [NSMutableDictionary dictionary];
    self.annotLineWidths = [NSMutableDictionary dictionary];
    self.annotFontSizes = [NSMutableDictionary dictionary];
    self.annotFontNames = [NSMutableDictionary dictionary];
    self.annotPropertyListeners = [NSMutableArray<IAnnotPropertyListener> array];
    self.pageNumberListeners = [[NSMutableArray alloc] init];
    
    self.highlightFormColor = self.config.defaultSettings.highlightFormColor;
    self.enableLinks =  !self.config.disableLink;
    self.enableHighlightLinks = self.config.defaultSettings.highlightLink;
    self.noteIcon = [Utility getIconTypeWithIconName:self.config.defaultSettings.annotations.note.icon annotType:FSAnnotNote];
    self.attachmentIcon = [Utility getIconTypeWithIconName:self.config.defaultSettings.annotations.attachment.icon annotType:FSAnnotFileAttachment];
    self.eraserLineWidth = 4;
    self.distanceUnit = ({
        SettingObj *distance = self.config.defaultSettings.annotations.distance;
        [NSString stringWithFormat:@"%d %@ = %d %@",distance.scaleFromValue,distance.scaleFromUnit,distance.scaleToValue,distance.scaleToUnit];
    });
    self.screenAnnotRotation = [Utility rotationForValue:@(self.config.defaultSettings.annotations.image.rotation)] ;
    self.linksHighlightColor = self.config.defaultSettings.highlightLinkColor;
    self.selectionHighlightColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.3];
    self.securityHandlers = [NSMutableArray array];
    self.isDocModified = NO;
    self.docSaveFlag = FSPDFDocSaveFlagIncremental;
    self.taskServer = [[TaskServer alloc] init];

    self.undoItems = [NSMutableArray<UndoItem *> array];
    self.redoItems = [NSMutableArray<UndoItem *> array];
    self.undoListeners = [NSMutableArray<IFSUndoEventListener> array];

    self.shouldShowMenu = NO;
    self.shouldShowPropertyBar = NO;
    self.isRotating = NO;
    self.isScrollViewDragging = NO;
    self.isScrollViewDecelerating = NO;
    self.isScrollViewZooming = NO;
    _isFullScreen = self.config.defaultSettings.fullscreen;
    _enableTopToolbar = YES;
    _enableBottomToolbar = YES;
    _needScreenLock = DEVICE_iPHONE;
    _isSuspendAutoFullScreen = NO;
    self.continueAddAnnot = self.config.defaultSettings.annotations.continuouslyAdd;
    _annotAuthor = [[UIDevice currentDevice].name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _enableHighlightForm = self.config.defaultSettings.highlightForm;
    _canUpdateAnnotDefaultProperties = YES;
    
    [self buildToolbars];
    [self buildItems];
    [self setViewsConstraint];
    [_pdfViewCtrl registerLayoutEventListener:self];
    [_pdfViewCtrl registerDrawEventListener:self];
    [_pdfViewCtrl registerGestureEventListener:self];
    [_pdfViewCtrl registerRecoveryEventListener:self];
    [_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_pdfViewCtrl registerScrollViewEventListener:self];
    
    self.panelController = [[FSPanelController alloc] initWithExtensionsManager:self];
    self.panelController.isHidden = YES;

    self.propertyBar = [[PropertyBar alloc] initWithPDFViewController:viewctrl extensionsManager:self];
    [self.propertyBar registerPropertyBarListener:self];

    NSMutableArray *modules = [NSMutableArray array];
    if (self.config.loadPageNavigation)
        [modules addObject:[[PageNavigationModule alloc] initWithUIExtensionsManager:self]];
    if (self.config.loadReadingBookmark)
        [modules addObject:[[ReadingBookmarkModule alloc] initWithUIExtensionsManager:self]];
    if (self.config.loadSearch)
        [modules addObject:[[SearchModule alloc] initWithUIExtensionsManager:self]];
    [modules addObject:[[MoreModule alloc] initWithUIExtensionsManager:self]];
    if (self.config.loadForm)
        [modules addObject:[[FormModule alloc] initWithUIExtensionsManager:self]];
    
    NSSet *tools = self.config.tools;
    if ([tools fs_containsAnyObjectNotInArray:@[ Tool_Multiple_Selection ]])
        [modules addObject:[[MultipleSelectionModule alloc] initWithUIExtensionsManager:self]];
    
    if ([tools fs_containsAnyObjectNotInArray:@[ Tool_Select ]]) {
        [modules addObject:[[UndoModule alloc] initWithUIExtensionsManager:self]];
    }
    // annotations
    if ([tools fs_containsAnyObjectInArray:@[ Tool_Highlight, Tool_Underline, Tool_Squiggly, Tool_Strikeout ]]) {
        [modules addObject:[[MarkupModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Note]) {
        [modules addObject:[[NoteModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools fs_containsAnyObjectInArray:@[ Tool_Oval, Tool_Rectangle ]]) {
        [modules addObject:[[ShapeModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Freetext]) {
        [modules addObject:[[FreetextModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Textbox]) {
        [modules addObject:[[TextboxModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Callout]) {
        [modules addObject:[[CalloutModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Pencil]) {
        [modules addObject:[[PencilModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Eraser]) {
        [modules addObject:[[EraseModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools fs_containsAnyObjectInArray:@[ Tool_Line, Tool_Arrow ]]) {
        [modules addObject:[[LineModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools fs_containsAnyObjectInArray:@[ Tool_Distance ]]) {
        [modules addObject:[[DistanceModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Stamp]) {
        [modules addObject:[[StampModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Insert]) {
        [modules addObject:[[InsertModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Replace]) {
        [modules addObject:[[ReplaceModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_Image]) {
        [modules addObject:[[ImageModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools fs_containsAnyObjectInArray:@[ Tool_Polygon, Tool_Cloud ]]) {
        [modules addObject:[[PolygonModule alloc] initWithUIExtensionsManager:self]];
    }
    if ([tools containsObject:Tool_PolyLine]) {
        [modules addObject:[[PolyLineModule alloc] initWithUIExtensionsManager:self]];
    }
    /// rdk 6.2 add
    if ([tools fs_containsAnyObjectInArray:@[ Tool_Audio, Tool_Video ]]) {
        [modules addObject:[[AudioVideoModule alloc] initWithUIExtensionsManager:self]];
        /// rdk 7.1 add
        if ([tools containsObject:Tool_Audio]) {
            [modules addObject:[[VoiceModule alloc] initWithUIExtensionsManager:self]];
        }
    }
    /// rdk 7.0 add
    if ([tools containsObject:Tool_Redaction]) {
        [modules addObject:[[RedactModule alloc] initWithUIExtensionsManager:self]];
    }
    if (self.config.loadAttachment) {
        [modules addObject:[[AttachmentModule alloc] initWithUIExtensionsManager:self]];
    }

    [modules addObject:[[ReflowModule alloc] initWithUIExtensionsManager:self]];
    
    if (self.config.loadSignature)
         [modules addObject:[[SignatureModule alloc] initWithUIExtensionsManager:self]];
    if (self.config.fillSign)
        [modules addObject:[[FillSignModule alloc] initWithUIExtensionsManager:self]];
    if ([self.config.tools containsObject:Tool_Select])
        [modules addObject:[[SelectionModule alloc] initWithUIExtensionsManager:self]];
    
    [modules addObject:[[LinkModule alloc] initWithUIExtensionsManager:self]];
    
    self.passwordModule = [[PasswordModule alloc] initWithExtensionsManager:self];
    [modules addObject:self.passwordModule];
    
    [modules addObject:[[CommentModule alloc] initWithExtensionsManager:self]];
    
    self.speechModule =  [[SpeechModule alloc] initWithExtensionsManager:self];
    [modules addObject:self.passwordModule];
    
    self.modules = modules;
    self.menuControl = [[MenuControl alloc] initWithUIExtensionsManager:self];
    self.menuControl.delegate = self;

    DynamicXFAHandler* xfaAnnotHandler = [[DynamicXFAHandler alloc] initWithUIExtensionsManager:self];
    [self registerAnnotHandler:xfaAnnotHandler];
    [_pdfViewCtrl registerDocEventListener:xfaAnnotHandler];

    _iconProvider = [[ExAnnotIconProviderCallback alloc] init];
    [FSLibrary setAnnotIconProviderCallback:_iconProvider];
    _actionHandler = [[ExActionHandler alloc] initWithExtensionsManager:self];
    [FSLibrary setActionCallback:_actionHandler];

    self.thumbnailCache = [[FSThumbnailCache alloc] initWithUIExtenionsManager:self];
    [_pdfViewCtrl registerPageEventListener:self.thumbnailCache];
    [self registerAnnotEventListener:self.thumbnailCache];
    
    self.presentedVCAnimator = [[FSPresentedVCAnimator alloc] initWithPdfViewCtrl:_pdfViewCtrl];

    _topToolBarItemsArr = [self.topToolbar.items mutableCopy];
    _bottomToolBarItemsArr = [self.bottomToolbar.items mutableCopy];

    [FSLibrary enableJavaScript:configuration.runJavaScript];

    UISettingsModel *seting = self.config.defaultSettings;
    
    self.pdfViewCtrl.reflowBackgroundColor = seting.reflowBackgroundColor;
    
    if ([seting.zoomMode isEqualToString:@"FitWidth"]) {
        [self.pdfViewCtrl setZoomMode:PDF_DISPLAY_ZOOMMODE_FITWIDTH];
    }else if ([seting.zoomMode isEqualToString:@"FitPage"]){
        [self.pdfViewCtrl setZoomMode:PDF_DISPLAY_ZOOMMODE_FITPAGE];
    }

    if ([seting.colorMode isEqualToString:@"Normal"]) {
        [self.pdfViewCtrl setColorMode:FSRendererColorModeNormal];
    }else if ([seting.colorMode isEqualToString:@"Night"]){
        [self.settingBar.nightModeSwith sendActionsForControlEvents:UIControlEventTouchUpInside];
    }else if ([seting.colorMode isEqualToString:@"Map"]){
        self.pdfViewCtrl.mappingModeBackgroundColor = seting.mapBackgroundColor;
        self.pdfViewCtrl.mappingModeForegroundColor = seting.mapForegroundColor;
        [self.pdfViewCtrl setColorMode:FSRendererColorModeMapping];
    }
    
    if ([seting.pageMode isEqualToString:@"Single"]) {
        [self.settingBar.singleViewBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }else if ([seting.pageMode isEqualToString:@"Reflow"]){
        [self.settingBar.reflowBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([seting.pageMode isEqualToString:@"Facing"]) {
        [self.settingBar.doubleViewBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }else if ([seting.pageMode isEqualToString:@"CoverLeft"]){
        [self.pdfViewCtrl setPageLayoutMode:PDF_LAYOUT_MODE_TWO_LEFT];
    }else if ([seting.pageMode isEqualToString:@"CoverMiddle"]){
        [self.pdfViewCtrl setPageLayoutMode:PDF_LAYOUT_MODE_TWO_MIDDLE];
    }else if ([seting.pageMode isEqualToString:@"CoverRight"]){
        [self.settingBar.coverBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    if (seting.continuous == YES && ![seting.pageMode isEqualToString:@"Reflow"]) {
        self.settingBar.continueSwith.on = YES;
        [self.settingBar.continueSwith sendActionsForControlEvents:UIControlEventTouchUpInside];
    }else if(seting.continuous == NO && ![seting.pageMode isEqualToString:@"Reflow"]) {
        self.settingBar.continueSwith.on = NO;
        [self.settingBar.continueSwith sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

#pragma mark - get bottomToolbar item hide/show status
-(NSMutableDictionary *)getBottomToolbarItemHiddenStatus {
    NSMutableDictionary *bottombarbuttonInfo = [@{
                                               @"LIST":@"YES",
                                               @"VIEW":@"YES",
                                               @"COMMENT":@"YES",
                                               @"SIGNATURE":@"YES",
                                               @"FILL":@"YES",
                                               } mutableCopy];
    
    NSMutableDictionary *bottomItemTagDic = [@{
                                               [NSNumber numberWithInt:FS_BOTTOMBAR_ITEM_PANEL_TAG]:@"LIST",
                                               [NSNumber numberWithInt:FS_BOTTOMBAR_ITEM_READMODE_TAG]:@"VIEW",
                                               [NSNumber numberWithInt:FS_BOTTOMBAR_ITEM_ANNOT_TAG]:@"COMMENT",
                                               [NSNumber numberWithInt:FS_BOTTOMBAR_ITEM_SIGNATURE_TAG]:@"SIGNATURE",
                                               [NSNumber numberWithInt:FS_BOTTOMBAR_ITEM_FILLSIGN_TAG]:@"FILL",
                                              } mutableCopy];
    
    for (UIBarButtonItem *tempbarItem in self.bottomToolbar.items) {
        NSArray *tempArr = [bottomItemTagDic allKeys];
        NSNumber *tag = [NSNumber numberWithLong:tempbarItem.tag];
        if ([tempArr containsObject: tag]) {
            [bottombarbuttonInfo setObject:@"NO" forKey:[bottomItemTagDic objectForKey:tag]];
        }
    }
    
    return bottombarbuttonInfo;
}

#pragma mark - get topToolbar item hide/show status
-(NSMutableDictionary *)getTopToolbarItemHiddenStatus {
    NSMutableDictionary *topbarbuttonInfo = [@{
                                               @"BACK":self.backButton.isHidden ? @"YES":@"NO",
                                               } mutableCopy];
    
    NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:3];
    
    for (UIBarButtonItem *tempbarItem in self.topToolbar.items) {
        [tempArr addObject:[NSNumber numberWithLong:tempbarItem.tag ]];
    }
    
    NSString *bookmarkResult = @"NO";
    NSString *searchResult = @"NO";
    NSString *morekResult = @"NO";
    NSString *backResult = @"NO";
    bookmarkResult = [tempArr containsObject:[NSNumber numberWithInt:FS_TOPBAR_ITEM_BOOKMARK_TAG]] ? @"NO" : @"YES";
    searchResult = [tempArr containsObject:[NSNumber numberWithInt:FS_TOPBAR_ITEM_SEARCH_TAG]] ?  @"NO" : @"YES";
    morekResult = [tempArr containsObject:[NSNumber numberWithInt:FS_TOPBAR_ITEM_MORE_TAG]] ?  @"NO" : @"YES";
    backResult = [tempArr containsObject:[NSNumber numberWithInt:FS_TOPBAR_ITEM_BACK_TAG]] ?  @"NO" : @"YES";
    
    [topbarbuttonInfo setObject:bookmarkResult forKey:@"BOOKMARK"];
    [topbarbuttonInfo setObject:searchResult forKey:@"SEARCH"];
    [topbarbuttonInfo setObject:morekResult forKey:@"MORE"];
    [topbarbuttonInfo setObject:backResult forKey:@"BACK"];
    
    return topbarbuttonInfo;
}

#pragma mark - set toolbar items hide/show
-(void)setToolbarItemHiddenWithTag:(NSUInteger)itemTag hidden:(BOOL)isHidden{
    NSMutableArray *toolBarItems = nil;
    if(itemTag < 200)
        toolBarItems = [self.topToolbar.items mutableCopy];
    else
        toolBarItems = [self.bottomToolbar.items mutableCopy];
    if (isHidden){
        [toolBarItems enumerateObjectsUsingBlock:^(UIBarButtonItem  *_Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if (itemTag < 200) {
                if (item.tag == itemTag) {
                    [toolBarItems removeObject:item];
                    *stop = YES;
                }
            }else{
                if (item.tag == itemTag) {
                    if (idx + 1 > toolBarItems.count) {
                        [toolBarItems removeObject:item];
                    }
                    else{
                        [toolBarItems removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx, 2)]];
                    }
                    *stop = YES;
                }
            }
        }];
    }else{
        
        NSMutableArray *readdToolBarItems = nil;
        if(itemTag < 200)
            readdToolBarItems = [[NSMutableArray alloc] initWithArray:_topToolBarItemsArr];
        else
            readdToolBarItems = [[NSMutableArray alloc] initWithArray:_bottomToolBarItemsArr];
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [readdToolBarItems enumerateObjectsUsingBlock:^(UIBarButtonItem  *_Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if (itemTag < 200) {
                if (item.tag == FS_TOPBAR_ITEM_BACK_TAG && item.tag == itemTag){
                    if (![toolBarItems containsObject:item]){
                        [toolBarItems insertObject:item atIndex:1];
                    }
                    *stop = YES;
                }else{
                    if (item.tag == itemTag){
                        if (![toolBarItems containsObject:item]){
                            [toolBarItems addObject:item];
                        }
                        *stop = YES;
                    }
                }
            }
            else{
                if (item.tag == itemTag) {
                    if (![toolBarItems containsObject:item]){
                        [toolBarItems addObject:item];
                        [toolBarItems addObject:flexibleSpace];
                    }
                    *stop = YES;
                }
            }
        }];
    }
    
    if(itemTag < 200)
        [self.topToolbar setItems:toolBarItems animated:NO];
    else
        [self.bottomToolbar setItems:toolBarItems animated:NO];
}

#pragma mark <SettingBarDelegate>

- (void)settingBarSinglePageLayout:(SettingBar *)settingBar {
    [self.pdfViewCtrl setPageLayoutMode:PDF_LAYOUT_MODE_SINGLE];
    self.hiddenSettingBar = YES;
    
    for (id<IPageNumberListener> listener in self.pageNumberListeners) {
        if ([listener respondsToSelector:@selector(updatePageNumber)]) {
            [listener updatePageNumber];
        }
    }
}

- (void)settingBarContinuousLayout:(SettingBar *)settingBar {
    self.hiddenSettingBar = YES;
    
    for (id<IPageNumberListener> listener in self.pageNumberListeners) {
        if ([listener respondsToSelector:@selector(updatePageNumber)]) {
            [listener updatePageNumber];
        }
    }
}

- (void)settingBarDoublePageLayout:(SettingBar *)settingBar {
    [self.pdfViewCtrl setPageLayoutMode:PDF_LAYOUT_MODE_TWO];
    self.hiddenSettingBar = YES;
    
    for (id<IPageNumberListener> listener in self.pageNumberListeners) {
        if ([listener respondsToSelector:@selector(updatePageNumber)]) {
            [listener updatePageNumber];
        }
    }
}

- (void)settingBarCoverPageLayout:(SettingBar *)settingBar {
    [self.pdfViewCtrl setPageLayoutMode:PDF_LAYOUT_MODE_TWO_RIGHT];
    self.hiddenSettingBar = YES;
    
    for (id<IPageNumberListener> listener in self.pageNumberListeners) {
        if ([listener respondsToSelector:@selector(updatePageNumber)]) {
            [listener updatePageNumber];
        }
    }
}

- (void)settingBarThumbnail:(SettingBar *)settingBar {
    [self showThumbnailView];
    self.hiddenSettingBar = YES;
}

- (void)settingBarReflow:(SettingBar *)settingBar {
    [self changeState:STATE_REFLOW];
}

- (void)enterReflowMode:(BOOL)flag{
    BOOL isReflow = [self.pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW;
    if (isReflow == flag) {
        return;
    }

    ReflowModule *reflowModule = (ReflowModule *)[self getModuleByName:@"Reflow"];
    [reflowModule enterReflowMode:flag];
    
    self.hiddenSettingBar = YES;
}

- (void)quitGotoMode{
    [(PageNavigationModule *)[self getModuleByName:@"PageNavigation"] quitGotoMode];
}


- (BOOL)enterCreateFormState{
    FormModule *formModule = (FormModule *)[self getModuleByName:@"Form"];
    return [formModule enterCreateFormState];
}

- (void)settingBarCrop:(SettingBar *)settingBar {
    self.hiddenSettingBar = YES;
    [self setCropMode];
}

- (void)settingBarSpeech:(SettingBar *)settingBar {
    self.hiddenSettingBar = YES;
    [self changeState:STATE_SPEECH];
}

- (void)settingBarPanAndZoom:(SettingBar *)settingBar {
    [self changeState:STATE_PANZOOM];
}

- (void)settingBar:(SettingBar *)settingBar setLockScreen:(BOOL)isLockScreen {
    self.isScreenLocked = isLockScreen;
    self.hiddenSettingBar = YES;
}

- (void)settingBar:(SettingBar *)settingBar setNightMode:(BOOL)isNightMode {
    self.pdfViewCtrl.isNightMode = isNightMode;
    self.hiddenSettingBar = YES;
}

- (void)settingBarFitPage:(SettingBar *)settingBar {
    [self.pdfViewCtrl setZoomMode:PDF_DISPLAY_ZOOMMODE_FITPAGE];
    self.hiddenSettingBar = YES;
}

- (void)settingBarFitWidth:(SettingBar *)settingBar {
    [self.pdfViewCtrl setZoomMode:PDF_DISPLAY_ZOOMMODE_FITWIDTH];
    self.hiddenSettingBar = YES;
}

- (void)settingBarRotate:(SettingBar *)settingBar {
    int viewRotation = [self.pdfViewCtrl getViewRotation];
    viewRotation = (viewRotation + 1 ) % 4 ;
    [self.pdfViewCtrl rotateView:viewRotation];
    [self.pdfViewCtrl setPageLayoutMode:[self.pdfViewCtrl getPageLayoutMode]];
    self.hiddenSettingBar = YES;
}

- (void)settingBarDidChangeSize:(SettingBar *)settingBar {
    [self updateSettingBarContainer];
}

#pragma mark - IPageEventListener

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    self.isDocModified = YES;
    self.docSaveFlag = FSPDFDocSaveFlagXRefStream;
    // remove all undo/redo items in removed pages
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(UndoItem *undoItem, NSDictionary *bindings) {
        return ![indexes containsObject:[NSNumber numberWithInt:undoItem.pageIndex]];
    }];
    [self.undoItems filterUsingPredicate:predicate];
    [self.redoItems filterUsingPredicate:predicate];

    void (^updatePageIndex)(UndoItem *item) = ^(UndoItem *item) {
        int subcount = 0;
        for (NSNumber *x in indexes) {
            if (item.pageIndex > [x intValue])
                subcount++;
        }
        item.pageIndex -= subcount;
    };

    [self.undoItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        updatePageIndex((UndoItem *) obj);
    }];

    [self.redoItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        updatePageIndex((UndoItem *) obj);
    }];

    for (id<IFSUndoEventListener> listener in self.undoListeners) {
        if ([listener respondsToSelector:@selector(onUndoChanged)]) {
            [listener onUndoChanged];
        }
    }
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    //todo wei update undo/redo
    self.isDocModified = YES;
}

- (void)onPagesRotated:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
    self.isDocModified = YES;
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    self.isDocModified = YES;
    //Load Form if there it is.
    FormAnnotHandler *handler = (FormAnnotHandler *) [self getAnnotHandlerByType:FSAnnotWidget];
    if (!handler)
        return;
    [handler onDocOpened:self.pdfViewCtrl.currentDoc error:0];

    //Reset the cropMode.
    [_pdfViewCtrl setCropMode:PDF_CROP_MODE_NONE];
    ((UIButton *) [self.settingBar getItemView:CROPPAGE]).selected = NO;
}

- (void)onPageChanged:(int)oldIndex currentIndex:(int)currentIndex {
    if ([self canAutoHideTopBottomToolBar]) {
        [self delayHideTopBottomToolBar];
    }
    [self hideMenu];
    if (self.zoomSlider && [self getState] == STATE_PANZOOM) {
        if (!self.zoomSlider.value) self.zoomSlider.value = [_pdfViewCtrl getZoom];
        
        if (!self.zoomSlider.hidden) {
            [_pdfViewCtrl setZoom:[_pdfViewCtrl getZoom] withSlider:NO];
        };
        
        if ([_pdfViewCtrl getPageCount] - 1 == _pdfViewCtrl.getCurrentPage) {
            self.nextPageButtonItem.enabled = NO;
        } else {
            self.nextPageButtonItem.enabled = YES;
        }
        if (0 == _pdfViewCtrl.getCurrentPage) {
            self.prevPageButtonItem.enabled = NO;
        } else {
            self.prevPageButtonItem.enabled = YES;
        }
    }
}

- (void)setPanAndZoomMode {
    if (self.panZoomView) {
        return;
    }
    
    self.panZoomView = [[PanZoomView alloc] initWithUIExtensionsManager:self];
    
    [self.pdfViewCtrl addSubview:self.panZoomView];
    
    _topToolBarItemsArr = self.topToolbar.items;
    _bottomToolBarItemsArr = self.bottomToolbar.items;
    
    [self buildPanZoomBottomBar];
}

- (void)buildPanZoomBottomBar {
    UIButton *exitButton = [Utility createButtonWithImage:[UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    [exitButton addTarget:self action:@selector(onClickExitPanZoomButton) forControlEvents:UIControlEventTouchUpInside];
    exitButton.enabled = YES;
    UIBarButtonItem *exitButtonItem = [[UIBarButtonItem alloc] initWithCustomView:exitButton];
    
    //zoom tool
    FSView *contentView = [[FSView alloc] init];
    contentView.backgroundColor = [UIColor fs_clearColor];
    [self.bottomToolbar addSubview:contentView];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.mas_equalTo(self.bottomToolbar);
    }];
    
    self.panZoomBottomContentView = contentView;
    
    UIButton *zoomOutButton = [Utility createButtonWithImage:[UIImage imageNamed:@"zoom_out" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    zoomOutButton.enabled = NO;
    [contentView addSubview:zoomOutButton];
    
    UIButton *zoomInButton = [Utility createButtonWithImage:[UIImage imageNamed:@"zoom_in" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    zoomInButton.enabled = NO;
    [contentView addSubview:zoomInButton];
    
    //page change tool
    UIButton *prevPageButton = [Utility createButtonWithImage:[UIImage imageNamed:@"zoom_prevPage" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    [contentView addSubview:prevPageButton];
    [prevPageButton addTarget:self action:@selector(onClickPrevPageButton) forControlEvents:UIControlEventTouchUpInside];
    self.prevPageButtonItem = [[UIBarButtonItem alloc] initWithCustomView:prevPageButton];

    UIButton *nextPageButton = [Utility createButtonWithImage:[UIImage imageNamed:@"zoom_nextPage" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    [contentView addSubview:nextPageButton];
    [nextPageButton addTarget:self action:@selector(onClickNextPageButton) forControlEvents:UIControlEventTouchUpInside];
    self.nextPageButtonItem = [[UIBarButtonItem alloc] initWithCustomView:nextPageButton];
    
    int width = zoomInButton.frame.size.width + zoomOutButton.frame.size.width + prevPageButton.frame.size.width + nextPageButton.frame.size.width;
    self.zoomSlider = [[UISlider alloc] initWithFrame:CGRectMake( 0, 0, self.pdfViewCtrl.fs_width - width - 100, 20)];
    self.zoomSlider.maximumValue = 10.0;
    self.zoomSlider.minimumValue = 1.0;
    self.zoomSlider.value = [_pdfViewCtrl getZoom];
    self.zoomSlider.continuous = YES;
    [self.zoomSlider addTarget:self action:@selector(onZoomSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.zoomSlider addTarget:self action:@selector(onZoomSliderTouch) forControlEvents:UIControlEventTouchDown];
    [self.zoomSlider addTarget:self action:@selector(onZoomSliderLeave) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside | UIControlEventTouchCancel];
    [contentView addSubview:self.zoomSlider];
    
    [zoomOutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(ITEM_MARGIN_NORMAL);
        make.centerY.mas_equalTo(self.bottomToolbar);
    }];
    
    [self.zoomSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(zoomOutButton.mas_right).offset(ITEM_MARGIN_LITTLE_NORMAL);
        make.centerY.mas_equalTo(zoomOutButton);
    }];
    
    [zoomInButton mas_makeConstraints:^(MASConstraintMaker *make) {
          make.left.mas_equalTo(self.zoomSlider.mas_right).offset(ITEM_MARGIN_LITTLE_NORMAL);
          make.size.mas_equalTo(zoomOutButton);
          make.centerY.mas_equalTo(zoomOutButton);
      }];
    
    [prevPageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(zoomInButton.mas_right).offset(ITEM_MARGIN_LITTLE_NORMAL);
        make.size.mas_equalTo(zoomOutButton);
        make.centerY.mas_equalTo(zoomOutButton);
    }];
    
    [nextPageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(prevPageButton.mas_right).offset(ITEM_MARGIN_LITTLE_NORMAL);
        make.size.mas_equalTo(zoomOutButton);
        make.right.mas_offset(-ITEM_MARGIN_NORMAL);
        make.centerY.mas_equalTo(zoomOutButton);
    }];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *leftPadding = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftPadding.width = 10 - [Utility getUIToolbarPaddingX];
    self.topToolbar.items = @[ leftPadding, exitButtonItem, flexibleSpace ];
    self.bottomToolbar.items = @[];
    
    if ([_pdfViewCtrl getPageCount] - 1 == _pdfViewCtrl.getCurrentPage) {
        self.nextPageButtonItem.enabled = NO;
    }
    if (0 == _pdfViewCtrl.getCurrentPage) {
        self.prevPageButtonItem.enabled = NO;
    }
}

- (void)onClickExitPanZoomButton {
    [self changeState:STATE_NORMAL];
}

- (void)cancelPanZoom{
    
    if (!self.enableTopToolbar) {
        [self enableTopToolbar:NO];
    }
    [self.panZoomBottomContentView removeFromSuperview];
    self.panZoomBottomContentView = nil;
    [self.panZoomView removeFromSuperview];

    [self unregisterAnnotEventListener:self.panZoomView];
    [self unregisterRotateChangedListener:self.panZoomView];
    [_pdfViewCtrl unregisterPageEventListener:self.panZoomView];
    [_pdfViewCtrl unregisterScrollViewEventListener:self.panZoomView];
    [_pdfViewCtrl unregisterLayoutEventListener:self.panZoomView];
    [_pdfViewCtrl unregisterDocEventListener:self.panZoomView];
    
    self.panZoomView = nil;
    
    self.topToolbar.items = _topToolBarItemsArr;
    self.bottomToolbar.items = _bottomToolBarItemsArr;
   
    [_pdfViewCtrl setZoom:[_pdfViewCtrl getDefaultPageScale]/[_pdfViewCtrl getScale]];
}

- (void)onZoomSliderValueChanged:(UISlider*)slider {
    CGFloat scaleFactor = slider.value;
    [_pdfViewCtrl setZoom:scaleFactor withSlider: YES];
}

- (void)onZoomSliderTouch{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoFullScreen) object:nil];
}

- (void)onZoomSliderLeave{
    if ([self canAutoHideTopBottomToolBar]) {
        [self delayHideTopBottomToolBar];
    }
}

- (void)onClickPrevPageButton {
    [_pdfViewCtrl gotoPrevPage:NO];
}

- (void)onClickNextPageButton {
    [_pdfViewCtrl gotoNextPage:NO];
}

- (void)setCropMode {
    self.settingBar.panAndZoomBtn.enabled = NO;

    CropViewController *cropViewController = [[CropViewController alloc] initWithNibName:@"CropViewController" bundle:[NSBundle bundleForClass:[self class]]];
    [cropViewController setExtension:self];
    cropViewController.cropViewClosedHandler = ^() {
        [self setFullScreen:NO];
    };
    FSNavigationController *navController = [[FSNavigationController alloc] initWithRootViewController:cropViewController];
    navController.navigationBarHidden = YES;
    [navController fs_setCustomTransitionAnimator:self.presentedVCAnimator];
    
    [self.pdfViewCtrl.fs_viewController presentViewController:navController animated:YES completion:nil];
}

- (void)setCurrentToolHandler:(id<IToolHandler>)toolHandler {
    id<IToolHandler> lastToolHandler = _currentToolHandler;
    if (lastToolHandler != nil) {
        [lastToolHandler onDeactivate];
    }
    if (toolHandler != nil) {
        if ([self currentAnnot] != nil)
            [self setCurrentAnnot:nil];
    }

    _currentToolHandler = toolHandler;

    if (_currentToolHandler != nil) {
        [_currentToolHandler onActivate];
    }

    for (id<IToolEventListener> listener in self.toolListeners) {
        if ([listener respondsToSelector:@selector(onToolChanged:CurrentToolName:)]) {
            [listener onToolChanged:[lastToolHandler getName] CurrentToolName:[_currentToolHandler getName]];
        }
    }
}

- (id<IToolHandler>)getToolHandlerByName:(NSString *)name {
    // tools share one tool handler
    if ([@[ Tool_Highlight, Tool_Squiggly, Tool_Strikeout, Tool_Underline ] containsObject:name]) {
        name = Tool_Markup;
    } else if ([@[ Tool_Rectangle, Tool_Oval ] containsObject:name]) {
        name = Tool_Shape;
    } else if ([name isEqualToString:Tool_Arrow]) {
        name = Tool_Line;
    }else if ([name isEqualToString:Tool_Audio]) {
        name = Tool_Video;
    }
    for (id<IToolHandler> toolHandler in self.toolHandlers) {
        if ([toolHandler respondsToSelector:@selector(getName)]) {
            if ([[toolHandler getName] isEqualToString:name]) {
                return toolHandler;
            }
        }
    }
    return nil;
}

- (id<IModule>)getModuleByName:(NSString *)name{
    for (id module in self.modules) {
        if ([module respondsToSelector:@selector(getName)] && [[module getName] isEqualToString:name]) {
            return module;
        }
    }
    return nil;
}


- (void)registerToolHandler:(id<IToolHandler>)toolHandler {
    if (self.toolHandlers) {
        [self.toolHandlers addObject:toolHandler];
    }
}

- (void)unregisterToolHandler:(id<IToolHandler>)toolHandler {
    if ([self.toolHandlers containsObject:toolHandler]) {
        [self.toolHandlers removeObject:toolHandler];
    }
}
- (void)registerAnnotHandler:(id<IAnnotHandler>)annotHandler {
    if (self.annotHandlers && ![self.annotHandlers containsObject:annotHandler]) {
        [self.annotHandlers addObject:annotHandler];
    }
}
- (void)unregisterAnnotHandler:(id<IAnnotHandler>)annotHandler {
    if ([self.annotHandlers containsObject:annotHandler]) {
        [self.annotHandlers removeObject:annotHandler];
    }
}

- (id<IAnnotHandler>)getAnnotHandlerByType:(FSAnnotType)type {
    if ([_pdfViewCtrl isDynamicXFA]) {
        return nil;
    }
    if (type == FSAnnotSquiggly || type == FSAnnotStrikeOut || type == FSAnnotUnderline) {
        type = FSAnnotHighlight;
    }
    if (type == FSAnnotSquare) {
        type = FSAnnotCircle;
    }
    for (id<IAnnotHandler> annotHandler in self.annotHandlers) {
        if ([annotHandler respondsToSelector:@selector(getType)]) {
            if ([annotHandler getType] == type) {
                return annotHandler;
            }
        }
    }
    return nil;
}

- (id<IAnnotHandler>)getAnnotHandlerByAnnot:(FSAnnot *)annot {
    FSAnnotType type = [annot getType];
    if (type == FSAnnotSquiggly || type == FSAnnotStrikeOut || type == FSAnnotUnderline) {
        type = FSAnnotHighlight;
    }
    if (type == FSAnnotSquare) {
        type = FSAnnotCircle;
    }
    for (id<IAnnotHandler> annotHandler in self.annotHandlers) {
        if ([annotHandler respondsToSelector:@selector(getType)]) {
            if ([annotHandler getType] == type) {
                if (FSAnnotWidget == type) {
                    FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
                    FSField *field = [widget getField];
                    FSFieldType fieldType = [field isEmpty] ? FSFieldTypeUnknown : [field getType];
                    if (FSFieldTypeSignature == fieldType && [annotHandler isKindOfClass:[DigitalSignatureAnnotHandler class]]) {
                        return annotHandler;
                    }
                    if (FSFieldTypeSignature != fieldType && [annotHandler isKindOfClass:[FormAnnotHandler class]]) {
                        return annotHandler;
                    }
                }
                else {
                    if (type == FSAnnotScreen) {
                        FSScreen *screen = [[FSScreen alloc] initWithAnnot:annot];
                        FSScreenActionType screenActiontype = screen.screenActiontype;
                        if ((screenActiontype == FSScreenActionTypeUnknow || screenActiontype == FSScreenActionTypeImage) && [annotHandler isKindOfClass:[ImageAnnotHandler class]]) return annotHandler;
                        if (screenActiontype == FSScreenActionTypeAudio && [annotHandler isKindOfClass:[AudioVideoAnnotHandler class]]) return annotHandler;
                        if (screenActiontype == FSScreenActionTypeVideo && [annotHandler isKindOfClass:[AudioVideoAnnotHandler class]]) return annotHandler;
                        continue;
                    }
                     return annotHandler;
                }
            }
        }
    }
    return nil;
}

- (void)registerDocModifiedEventListener:(id<IDocModifiedEventListener>)listener {
    if (self.docModifiedListeners) {
        [self.docModifiedListeners addObject:listener];
    }
}
- (void)unregisterDocModifiedEventListener:(id<IDocModifiedEventListener>)listener {
    if ([self.docModifiedListeners containsObject:listener]) {
        [self.docModifiedListeners removeObject:listener];
    }
}

- (void)registerAnnotEventListener:(id<IAnnotEventListener>)listener {
    if (self.annotListeners) {
        [self.annotListeners addObject:listener];
    }
}
- (void)unregisterAnnotEventListener:(id<IAnnotEventListener>)listener {
    if ([self.annotListeners containsObject:listener]) {
        [self.annotListeners removeObject:listener];
    }
}

- (void)registerToolEventListener:(id<IToolEventListener>)listener {
    if (self.toolListeners) {
        [self.toolListeners addObject:listener];
    }
}
- (void)unregisterToolEventListener:(id<IToolEventListener>)listener {
    if ([self.toolListeners containsObject:listener]) {
        [self.toolListeners removeObject:listener];
    }
}

- (void)registerUndoEventListener:(id<IFSUndoEventListener>)listener {
    if (self.undoListeners) {
        [self.undoListeners addObject:listener];
    }
}

- (void)unregisterUndoEventListener:(id<IFSUndoEventListener>)listener {
    if ([self.undoListeners containsObject:listener]) {
        [self.undoListeners removeObject:listener];
    }
}

- (void)registerSearchEventListener:(id<ISearchEventListener>)listener {
    if (self.searchListeners) {
        [self.searchListeners addObject:listener];
    }
}

- (void)unregisterSearchEventListener:(id<ISearchEventListener>)listener {
    if ([self.searchListeners containsObject:listener]) {
        [self.searchListeners removeObject:listener];
    }
}

#pragma mark <MenuControlDelegate>

- (void)menuControlDidHide:(MenuControl *)menuControl {
    if (self.isRotating || self.isScrollViewDragging || self.isScrollViewDecelerating || self.isScrollViewZooming) {
        return;
    }
    self.shouldShowMenu = NO;
}

- (void)menuControlDidShow:(MenuControl *)menuControl {
    if (self.isRotating || self.isScrollViewDragging || self.isScrollViewDecelerating || self.isScrollViewZooming) {
        return;
    }
    self.shouldShowMenu = YES;
}

- (void)hideMenu {
    if (self.menuControl.isMenuVisible) {
        [self.menuControl hideMenu];
    }
}

- (void)showMenu {
    if ([self.currentToolHandler isKindOfClass:[MultipleSelectionToolHandler class]]) {
        [(MultipleSelectionToolHandler *)self.currentToolHandler showMenu];
        return;
    }
    if (self.currentAnnot == nil || [self.currentAnnot isEmpty]) {
        return;
    }
    id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:self.currentAnnot];
    if (annotHandler == nil) {
        return;
    }
    
    CGRect dvRect = CGRectNull;
    if (self.currentAnnot) {
        int pageIndex = self.currentAnnot.pageIndex;
        CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.currentAnnot.fsrect pageIndex:pageIndex];
        dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
    } else {
        int pageIndex = self.currentPageIndex;
        CGPoint pvPoint = self.currentPoint;
        CGRect pvRect = CGRectMake(pvPoint.x, pvPoint.y, 2, 2);
        dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
    }
    if (CGRectIsEmpty(dvRect) || CGRectIsNull(CGRectIntersection(dvRect, self.pdfViewCtrl.bounds))) {
        return;
    }
    [self.menuControl setRect:dvRect];
    [self.menuControl showMenu];
}

#pragma mark <IPropertyBarListener>

- (void)onPropertyBarShow {
    if (self.isRotating || self.isScrollViewDragging || self.isScrollViewDecelerating || self.isScrollViewZooming) {
        return;
    }
    self.shouldShowPropertyBar = YES;
}

- (void)onPropertyBarDismiss {
    if (self.isRotating || self.isScrollViewDragging || self.isScrollViewDecelerating || self.isScrollViewZooming) {
        return;
    }
    self.shouldShowPropertyBar = NO;
    if (self.shouldShowMenu) {
        [self showMenu];
    }
}

- (void)hidePropertyBar {
    [self.propertyBar dismissPropertyBar];
}

- (void)showPropertyBar {
    if (self.currentAnnot && self.currentAnnot.pageIndex == [self.pdfViewCtrl getCurrentPage]) {
        int pageIndex = self.currentAnnot.pageIndex;
        CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.currentAnnot.fsrect pageIndex:pageIndex];
        CGRect dvRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
        [self.propertyBar showPropertyBar:dvRect inView:self.pdfViewCtrl viewsCanMove:@[ [self.pdfViewCtrl getDisplayView] ]];
    }
}

#pragma mark - IGestureEventListener

- (void)registerGestureEventListener:(id<IGestureEventListener>)listener {
    if (!self.guestureEventListeners) {
        self.guestureEventListeners = [NSMutableArray<IGestureEventListener> array];
    }
    if (![self.guestureEventListeners containsObject:listener]) {
        [self.guestureEventListeners addObject:listener];
    }
}

- (void)unregisterGestureEventListener:(id<IGestureEventListener>)listener {
    [self.guestureEventListeners removeObject:listener];
}

- (void)comment {
    unsigned int color = [self getPropertyBarSettingColor:FSAnnotNote];

    float pageWidth = [_pdfViewCtrl getPageViewWidth:self.currentPageIndex];
    float pageHeight = [_pdfViewCtrl getPageViewHeight:self.currentPageIndex];

//    float scale = pageWidth / 1000.0;
    
    CGPoint pvPoint = self.currentPoint;

    if (pvPoint.x > pageWidth - NOTE_ANNOTATION_WIDTH )
        pvPoint.x = pageWidth - NOTE_ANNOTATION_WIDTH ;
    if (pvPoint.y > pageHeight - NOTE_ANNOTATION_WIDTH )
        pvPoint.y = pageHeight - NOTE_ANNOTATION_WIDTH ;

//    CGRect rect = CGRectMake(pvPoint.x , pvPoint.y , NOTE_ANNOTATION_WIDTH , NOTE_ANNOTATION_WIDTH );
    float scale = [_pdfViewCtrl getPageViewWidth:self.currentPageIndex] / 1000.0;
    
    FSRectF *iconfsrect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:NOTE_ANNOTATION_WIDTH top1:NOTE_ANNOTATION_WIDTH];
    CGRect iconrect = [_pdfViewCtrl convertPdfRectToPageViewRect:iconfsrect pageIndex:self.currentPageIndex];
    float iconwidth = iconrect.size.width;
    CGRect rect = CGRectMake(pvPoint.x - NOTE_ANNOTATION_WIDTH * scale / 2, pvPoint.y - NOTE_ANNOTATION_WIDTH * scale / 2, iconwidth, iconwidth);
    
    FSRectF *dibRect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:self.currentPageIndex];

    FSPointF *tempPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:pvPoint pageIndex:self.currentPageIndex];
    FSRectF *annotTempRect = [[FSRectF alloc] init];
    annotTempRect.left = tempPoint.x;
    annotTempRect.bottom = tempPoint.y - NOTE_ANNOTATION_WIDTH;
    annotTempRect.right = annotTempRect.left + NOTE_ANNOTATION_WIDTH;
    annotTempRect.top = annotTempRect.bottom + NOTE_ANNOTATION_WIDTH;
    dibRect = annotTempRect;
    
    NoteDialog *noteDialog = [[NoteDialog alloc] init];
    [noteDialog show:nil replyAnnots:nil title:nil];
    noteDialog.noteEditDone = ^(NoteDialog *dialog) {
        FSPDFPage *page = [self->_pdfViewCtrl.currentDoc getPage:self.currentPageIndex];
        if (!page || [page isEmpty])
            return;

        FSNote *note = [[FSNote alloc] initWithAnnot:[page addAnnot:FSAnnotNote rect:dibRect]];
        note.color = color;
        int opacity = [self getPropertyBarSettingOpacity:FSAnnotNote];
        note.opacity = opacity / 100.0f;
        note.icon = self.noteIcon;
        note.author = self.annotAuthor;
        note.contents = [Utility encodeEmojiString:[dialog getContent]];
        note.NM = [Utility getUUID];
        note.lineWidth = 2;
        note.modifiedDate = [NSDate date];
        note.createDate = [NSDate date];
        note.subject = @"Note";
        note.flags = FSAnnotFlagPrint | FSAnnotFlagNoZoom | FSAnnotFlagNoRotate;
        [self.pdfViewCtrl lockRefresh];
        [note resetAppearanceStream];
        [self.pdfViewCtrl unlockRefresh];
        id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:note];
        [annotHandler addAnnot:note];
    };
    [self setCurrentToolHandler:nil];
}

- (void)typeWriter {
    FtToolHandler *toolHandler = (FtToolHandler *) [self getToolHandlerByName:Tool_Freetext];
    [self setCurrentToolHandler:toolHandler];
    toolHandler.freeTextStartPoint = self.currentPoint;
    [toolHandler onPageViewTap:self.currentPageIndex recognizer:nil];
    toolHandler.isTypewriterToolbarActive = NO;
    if (self.currentToolHandler == (id<IToolHandler>)self) { // todo
        [self setCurrentToolHandler:nil];
    }
}

- (void)signature {
    if (self.currentState == STATE_NORMAL || self.currentState == STATE_CREATEFORM || self.currentState == STATE_SPEECH) [self changeState:STATE_SIGNATURE];
    SignToolHandler *toolHandler = (SignToolHandler *) [self getToolHandlerByName:Tool_Signature];
    toolHandler.isAdded = NO;
    toolHandler.signatureStartPoint = self.currentPoint;
    [toolHandler onPageViewTap:self.currentPageIndex recognizer:nil];
}

- (void)redact{
    if (!self.isSupportRedaction) {
        [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailedLoadModuleForLicenseTip") actionBack:nil];
        return;
    }
    RedactToolHandler *toolHandler = (RedactToolHandler *) [self getToolHandlerByName:Tool_Redaction];
    [self setCurrentToolHandler:toolHandler];
}

- (void)showBlankMenu:(int)pageIndex point:(CGPoint)point {
    if (![self shouldEditAnnotPoint:point pageIndex:pageIndex]) return;
    
    self.currentPoint = point;
    self.currentPageIndex = pageIndex;
    NSMutableArray *array = [NSMutableArray array];
    MenuItem *commentItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kNote") object:self action:@selector(comment)];
    MenuItem *typeWriterItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kTypewriter") object:self action:@selector(typeWriter)];
    MenuItem *signatureItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSignAction") object:self action:@selector(signature)];
    MenuItem *redactItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kRedaction") object:self action:@selector(redact)];

    if ([Utility canAddAnnot:_pdfViewCtrl]) {
        if ([self.config.tools containsObject:Tool_Note]) {
            [array addObject:commentItem];
        }
        if ([self.config.tools containsObject:Tool_Freetext]) {
            [array addObject:typeWriterItem];
        }
        if ([self.config.tools containsObject:Tool_Redaction]) {
            [array addObject:redactItem];
        }
    }
    if ([Utility canAddSign:_pdfViewCtrl]) {
        if (self.config.loadSignature &&
            [Utility canAddSignToDocument:[_pdfViewCtrl currentDoc]] &&
            [Utility canAddSign:_pdfViewCtrl] &&
            ![_pdfViewCtrl isDynamicXFA] &&
            [Utility canFillForm:_pdfViewCtrl])
            [array addObject:signatureItem];
    }

    if (array.count > 0) {
        CGRect dvRect = CGRectMake(point.x, point.y, 2, 2);
        dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:dvRect pageIndex:pageIndex];
        MenuControl *annotMenu = self.menuControl;
        annotMenu.menuItems = array;
        [annotMenu setRect:dvRect];
        [annotMenu showMenu];
    }
}

- (BOOL)shouldEditAnnotPoint:(CGPoint)point pageIndex:(int)pageIndex{
    if ([_pdfViewCtrl isDynamicXFA]) {
        return YES;
    }
    FSPDFPage *currentPage = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    CGFloat pageH = currentPage.getHeight;
    CGFloat pageW = currentPage.getWidth;
    
    FSRotation rotation = currentPage.rotation;
//    FSRotation viewRoation = (FSRotation)[_pdfViewCtrl getViewRotation];
//    rotation = (rotation + viewRoation) % 4;
    if (rotation == FSRotation90 || rotation == FSRotation270) {
        CGFloat tmpPageH = pageH;
        pageH = pageW;
        pageW = tmpPageH;
    }
    
    CGRect pointRect = CGRectMake(point.x, point.y, 5, 5);
    FSRectF *rect = [_pdfViewCtrl convertPageViewRectToPdfRect:pointRect pageIndex:pageIndex];
    FSRectF *realRect = [Utility getPageRealRect:currentPage];
    
    if (rect.right > realRect.right ||
        rect.top > realRect.top ||
        rect.left < realRect.left ||
        rect.bottom < realRect.bottom) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kEditOutsideDocument") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:true completion:nil];
        return NO;
    }
    return YES;
}

- (BOOL)onLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if ([self canAutoHideTopBottomToolBar]) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoFullScreen) object:nil];
    }
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if ([self canAutoHideTopBottomToolBar]) {
            [self delayHideTopBottomToolBar];
        }
    }
    CGPoint point = [gestureRecognizer locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    //    if (pageIndex < 0)
    //        return NO;
    //    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    //    CGPoint cgPoint = [gestureRecognizer locationInView:pageView];
    //    CGRect rect1 = [pageView frame];
    //    CGSize size = rect1.size;
    //    if (cgPoint.x > size.width || cgPoint.y > size.height || cgPoint.x < 0 || cgPoint.y < 0)
    //        return NO;

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    id<IToolHandler> originToolHandler = self.currentToolHandler;
    if (self.currentToolHandler != nil) {
        if ([self.currentToolHandler onPageViewLongPress:pageIndex recognizer:gestureRecognizer]) {
            return YES;
        }
    }
    id<IToolHandler> selectTool = [self getToolHandlerByName:Tool_Select];
    if (self.currentToolHandler == selectTool)
        [self setCurrentToolHandler:nil];

    if (self.currentToolHandler == nil) {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewLongPress:pageIndex recognizer:gestureRecognizer annot:annot];
        }

        point = [gestureRecognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
        if (annot != nil && ![annot isEmpty]) {
            if ([annot getType] != FSAnnotLink && ![annot isKindOfClass:[FSTextMarkup class]]) //for adding text markups one more at same text
            {
                annotHandler = [self getAnnotHandlerByAnnot:annot];
                if (annotHandler != nil) {
                    return [annotHandler onPageViewLongPress:pageIndex recognizer:gestureRecognizer annot:annot];
                }
            }
        }

        id<IAnnotHandler> linkAnnotHandler = [self getAnnotHandlerByType:FSAnnotLink];
        if (linkAnnotHandler && [linkAnnotHandler onPageViewLongPress:pageIndex recognizer:gestureRecognizer annot:nil]) {
            return YES;
        }
        
        if (![_pdfViewCtrl isDynamicXFA]) {
            if (originToolHandler != selectTool && [selectTool onPageViewLongPress:pageIndex recognizer:gestureRecognizer]) {
                if (self.currentToolHandler == nil) {
                    [self setCurrentToolHandler:selectTool];
                }
                return YES;
            }
        }
        
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            [self showBlankMenu:pageIndex point:point];
            #if _MAC_CATALYST_
                gestureRecognizer.enabled = NO;
                gestureRecognizer.enabled = YES;
            #endif
            return YES;
        }
    }

    for (id<IGestureEventListener> listener in self.guestureEventListeners) {
        if ([listener respondsToSelector:@selector(onLongPress:)]) {
            if ([listener onLongPress:gestureRecognizer])
                return YES;
        }
    }

    return NO;
}

#pragma mark ILinkEventListener

- (void)registerLinkEventListener:(id<ILinkEventListener>)listener {
    if (!self.linkEventListeners) {
        self.linkEventListeners = [NSMutableArray<ILinkEventListener> array];
    }
    [self.linkEventListeners addObject:listener];
}

- (void)unregisterLinkEventListener:(id<ILinkEventListener>)listener {
    [self.linkEventListeners removeObject:listener];
}

- (BOOL)onLinkOpen:(id)link LocationInfo:(CGPoint)pointParam {
    for (id<ILinkEventListener> listener in self.linkEventListeners) {
        if ([listener respondsToSelector:@selector(onLinkOpen:LocationInfo:)]) {
            if ([listener onLinkOpen:link LocationInfo:pointParam])
                return YES;
        }
    }
    return NO;
}

- (BOOL)onTap:(UITapGestureRecognizer *)gestureRecognizer {
    if ([self canAutoHideTopBottomToolBar]) {
        [self delayHideTopBottomToolBar];
    }
    
    CGPoint point = [gestureRecognizer locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0)
        return NO;

    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint cgPoint = [gestureRecognizer locationInView:pageView];
    CGRect rect1 = [pageView frame];
    CGSize size = rect1.size;
    PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
    BOOL (^fullScreenBlock)(void) = ^(){
        if (self.currentState == STATE_PAGENAVIGATE)
            return NO;
        CGFloat width = pageView.bounds.size.width;
        BOOL flag = self.hiddenTopToolbar && self.hiddenBottomToolbar;
        if (!self.enableTopToolbar || !self.enableBottomToolbar) {
            flag = (
                    (!self.enableTopToolbar && self.hiddenBottomToolbar) || (self.hiddenTopToolbar && !self.enableBottomToolbar)
                    ) &&
            !(!self.enableTopToolbar && !self.enableBottomToolbar);
        }
        if([self.pdfViewCtrl isContinuous]) {
            [self setFullScreen:!flag];
            return YES;
        }else {
            if (width * 0.2 < cgPoint.x && cgPoint.x < width * 0.8) {
                [self setFullScreen:!flag];
                return YES;
            }
        }
        return NO;
    };
    
    if (cgPoint.x > size.width || cgPoint.y > size.height || cgPoint.x < 0 || cgPoint.y < 0)
        return NO;

    if (pageLayoutMode == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    id<IToolHandler> originToolHandler = self.currentToolHandler;
    if (self.currentToolHandler != nil) {
        CGPoint newpoint = [_pdfViewCtrl convertDisplayViewPtToPageViewPt:point pageIndex:pageIndex];
        if (![self shouldEditAnnotPoint:newpoint pageIndex:pageIndex]) return YES;
        if ([self.currentToolHandler onPageViewTap:pageIndex recognizer:gestureRecognizer]) {
            return YES;
        }
    }
    id<IToolHandler> selectTool = [self getToolHandlerByName:Tool_Select];
    if (self.currentToolHandler == selectTool)
        [self setCurrentToolHandler:nil];

    if (self.currentToolHandler == nil) {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewTap:pageIndex recognizer:gestureRecognizer annot:annot];
        }
        point = [gestureRecognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];

        if (originToolHandler != selectTool && [selectTool onPageViewTap:pageIndex recognizer:gestureRecognizer]) {
            return YES;
        }
        if ([_pdfViewCtrl isDynamicXFA]) {
            FSXFAWidget *widget = nil;
            widget = self.currentWidget;
            if (widget != nil && ![widget isEmpty]) {
                id<IAnnotHandler> dynamicXFAHandler = [self getDynamicAnnotHandler];
                if (dynamicXFAHandler != nil) {
                    return [dynamicXFAHandler onPageViewTap:pageIndex recognizer:gestureRecognizer widget:widget];
                }
            }
            
            FSXFAWidget *xfaWidget = [self getXFAWidgetAtPoint:point pageIndex:pageIndex];
            if (xfaWidget != nil && ![xfaWidget isEmpty]) {
                id<IAnnotHandler> dynamicXFAHandler = [self getDynamicAnnotHandler];
                if (dynamicXFAHandler != nil) {
                    return [dynamicXFAHandler onPageViewTap:pageIndex recognizer:gestureRecognizer widget:xfaWidget];
                }
            }
        }
        
        if (annot != nil && ![annot isEmpty]) {
            if (annot.isMarkup && annot.getType != FSAnnotCaret) {
                FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
                if (markup.isGrouped && [self.config.tools containsObject:Tool_Multiple_Selection]) {
                    id<IToolHandler> toolHandler = [self getToolHandlerByName:Tool_Multiple_Selection];
                    if ([toolHandler isKindOfClass:[MultipleSelectionToolHandler class]]) {
                        [self setCurrentToolHandler:toolHandler];
                        self.shouldShowMenu = YES;
                        if ([(MultipleSelectionToolHandler *)toolHandler selectedGroupAnnot:annot])  return YES;
                    }
                }
            }
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler != nil) {
                return [annotHandler onPageViewTap:pageIndex recognizer:gestureRecognizer annot:annot];
            }
        }
        
        id<IAnnotHandler> linkAnnotHandler = [self getAnnotHandlerByType:FSAnnotLink];
        if (linkAnnotHandler && [linkAnnotHandler onPageViewTap:pageIndex recognizer:gestureRecognizer annot:nil]) {
            return YES;
        }
    }

    for (id<IGestureEventListener> listener in self.guestureEventListeners) {
        if ([listener respondsToSelector:@selector(onTap:)]) {
            if ([listener onTap:gestureRecognizer])
                return YES;
        }
    }
    
    return fullScreenBlock();
}

- (BOOL)onDoubleTap:(UITapGestureRecognizer *)gestureRecognizer {
    if ([self canAutoHideTopBottomToolBar]) {
        [self delayHideTopBottomToolBar];
    }
    return NO;
}

- (BOOL)onPan:(UIPanGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0){
        id<IToolHandler> originToolHandler = self.currentToolHandler;
        if (originToolHandler != nil) {
            [recognizer setValue:@(UIGestureRecognizerStateEnded) forKey:@"state"];
            if ([self.currentToolHandler onPageViewPan:pageIndex recognizer:recognizer]) {
                return YES;
            }
        }
        return NO;
    }
    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    
    id<IToolHandler> originToolHandler = self.currentToolHandler;
    if (self.currentToolHandler != nil) {
        if ([self.currentToolHandler onPageViewPan:pageIndex recognizer:recognizer]) {
            return YES;
        }
    }

    if (recognizer.state == UIGestureRecognizerStateChanged && [self getToolHandlerByName:Tool_Shape] == originToolHandler) {
        ShapeToolHandler *shapeToolHandler = (ShapeToolHandler *)originToolHandler;
        if (shapeToolHandler.annot != nil && shapeToolHandler.annot.pageIndex != pageIndex) {
            [recognizer setValue:@(UIGestureRecognizerStateEnded) forKey:@"state"];
            return YES;
        }
        UIView *pageview = [_pdfViewCtrl getPageView:pageIndex];
        CGPoint p = [recognizer locationInView:pageview];
        BOOL isOut = ![pageview pointInside:p withEvent:nil];

        if (isOut) {
            [recognizer setValue:@(UIGestureRecognizerStateEnded) forKey:@"state"];
            return YES;
        }
    }
    
    id<IToolHandler> selectTool = [self getToolHandlerByName:Tool_Select];
    if (self.currentToolHandler == selectTool)
        [self setCurrentToolHandler:nil];

    if (self.currentToolHandler == nil) {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            if ([[annot getPage] getIndex] == pageIndex ||
                recognizer.state == UIGestureRecognizerStateEnded ||
                recognizer.state == UIGestureRecognizerStateCancelled) {
                annotHandler = [self getAnnotHandlerByAnnot:annot];
                return [annotHandler onPageViewPan:pageIndex recognizer:recognizer annot:annot];
            }
        }
        

        point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler != nil) {
                return [annotHandler onPageViewPan:pageIndex recognizer:recognizer annot:annot];
            }
        }
        id<IToolHandler> selectTool = [self getToolHandlerByName:Tool_Select];
        if (originToolHandler != selectTool && [selectTool onPageViewPan:pageIndex recognizer:recognizer]) {
            return YES;
        }
    }
    for (id<IGestureEventListener> listener in self.guestureEventListeners) {
        if ([listener respondsToSelector:@selector(onPan:)]) {
            if ([listener onPan:recognizer])
                return YES;
        }
    }
    return NO;
}

- (id<IAnnotHandler>)getDynamicAnnotHandler {
    for (id<IAnnotHandler> annotHandler in self.annotHandlers) {
        if ([annotHandler respondsToSelector:@selector(getName)]) {
            if ([[annotHandler getName] isEqualToString: @"FSXFAWidget"]) {
                return annotHandler;
            }
        }
    }
    return nil;
}

-(FSXFAWidget *)getXFAWidgetAtPoint:(CGPoint)pvPoint pageIndex:(int)pageIndex {
    FSXFAPage *page = [[_pdfViewCtrl getXFADoc] getPage:pageIndex];
    
    FSMatrix2D *matrix = [_pdfViewCtrl getDisplayMatrix:pageIndex];
    FSPointF *devicePoint = [[FSPointF alloc] init];
    [devicePoint set:pvPoint.x y:pvPoint.y];
    FSXFAWidget *widget = [page getWidgetAtDevicePoint:matrix device_point:devicePoint tolerance:5];
    
    if (!widget || [widget isEmpty]) {
        return nil;
    }
    return widget;
}
- (FSAnnot *)getAnnotAtPoint:(CGPoint)pvPoint pageIndex:(int)pageIndex {
    if ([_pdfViewCtrl isDynamicXFA]) {
        return nil;
    }
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    FSMatrix2D *matrix = [_pdfViewCtrl getDisplayMatrix:pageIndex];
    FSPointF *devicePoint = [[FSPointF alloc] init];
    [devicePoint set:pvPoint.x y:pvPoint.y];
    FSAnnot *annot = [page getAnnotAtDevicePoint:devicePoint tolerance:5 matrix:matrix];
    
    if (!annot || [annot isEmpty]) {
        return nil;
    }
    if (![self.config canInteractWithAnnot:annot]) {
        return nil;
    }
    
    id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:annot];
    if ([annotHandler isHitAnnot:annot point:[_pdfViewCtrl convertPageViewPtToPdfPt:pvPoint pageIndex:pageIndex]]) {
        return annot;
    }
    
    if (annot.type == FSAnnotStrikeOut) {
        FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
        FSMarkupArray *groupAnnots = [markup getGroupElements];
        for (int i = 0; i < [groupAnnots getSize]; i++) {
            FSAnnot *groupAnnot = [groupAnnots getAt:i];
            if (groupAnnot.type == FSAnnotCaret) {
                //                return groupAnnot;
                // can't just return groupAnnot since it will be destroyed in MarkupArray dtor
                return [[FSAnnot alloc] initWithAnnot:groupAnnot];
            }
        }
        return annot;
    }
    return nil;
}

- (BOOL)onShouldBegin:(UIGestureRecognizer *)recognizer {
    
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0)
        return NO;

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    if (_currentToolHandler != nil) {
        if ([_currentToolHandler respondsToSelector:@selector(onPageViewShouldBegin:recognizer:)] && [_currentToolHandler onPageViewShouldBegin:pageIndex recognizer:recognizer]) {
            return YES;
        }
    }

    if (self.currentToolHandler == nil || [[self.currentToolHandler getName] isEqualToString:Tool_Select]) {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewShouldBegin:pageIndex recognizer:recognizer annot:annot];
        }

        if ([recognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            if (((UITapGestureRecognizer *) recognizer).numberOfTapsRequired == 2) {
                return NO;
            }
        }

        if ([recognizer isKindOfClass:[UITapGestureRecognizer class]] ||
            [recognizer isKindOfClass:[UILongPressGestureRecognizer class]] ||
            [recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];

            if ([_pdfViewCtrl isDynamicXFA]) {
                FSXFAWidget *widget = self.currentWidget;
                if (widget != nil && ![widget isEmpty]) {
                    annotHandler = [self getDynamicAnnotHandler];
                    return [annotHandler onPageViewShouldBegin:pageIndex recognizer:recognizer widget:widget];
                }
                
                FSXFAWidget *xfaWidget = [self getXFAWidgetAtPoint:point pageIndex:pageIndex];
                if (xfaWidget != nil && ![xfaWidget isEmpty]) {
                    id<IAnnotHandler> dynamicXFAHandler = [self getDynamicAnnotHandler];
                    if (dynamicXFAHandler != nil && ![recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
                        return [dynamicXFAHandler onPageViewShouldBegin:pageIndex recognizer:recognizer widget:xfaWidget];
                    }
                }
                return NO;
            }else{
                annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
                if (annot != nil && ![annot isEmpty]) {
                    annotHandler = [self getAnnotHandlerByAnnot:annot];
                    if (annotHandler != nil && ![recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
                        return [annotHandler onPageViewShouldBegin:pageIndex recognizer:recognizer annot:annot];
                    }
                }
                
                id<IAnnotHandler> linkAnnotHandler = [self getAnnotHandlerByType:FSAnnotLink];
                if (linkAnnotHandler && [linkAnnotHandler onPageViewShouldBegin:pageIndex recognizer:recognizer annot:nil] && ![recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
                    return YES;
                }
                
                id<IToolHandler> selectTool = [self getToolHandlerByName:Tool_Select];
                if (self.currentToolHandler != selectTool && [selectTool onPageViewShouldBegin:pageIndex recognizer:recognizer]) {
                    return YES;
                }
            }
        }
        return NO;
    }
    // return no here,
    // make the pan gesture recognized by the page container
    if (self.currentToolHandler != nil) {
        NSString *name = [self.currentToolHandler getName];
        if (name != nil) {
            if ([name isEqualToString:Tool_Fill_Sign]) {
                return [self.currentToolHandler onPageViewShouldBegin:pageIndex recognizer:recognizer];
            }
            return YES;
        }
    }

    for (id<IGestureEventListener> listener in self.guestureEventListeners) {
        if ([listener respondsToSelector:@selector(onShouldBegin:)]) {
            if ([listener onShouldBegin:recognizer])
                return YES;
        }
    }

    return NO;
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0)
        return NO;
    
    self.currentPoint = point;
    self.currentPageIndex = pageIndex;
    
    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    if (_currentToolHandler != nil) {
        if ([_currentToolHandler onPageViewTouchesBegan:pageIndex touches:touches withEvent:event]) {
            return YES;
        }
    } else {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewTouchesBegan:pageIndex touches:touches withEvent:event annot:annot];
        }
        point = [[touches anyObject] locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        
        if ([_pdfViewCtrl isDynamicXFA]) {
            FSXFAWidget *widget = nil;
            widget = self.currentWidget;
            if (widget != nil && ![widget isEmpty]) {
                annotHandler = [self getDynamicAnnotHandler];
                return [annotHandler onPageViewTouchesBegan:pageIndex touches:touches withEvent:event widget:widget];
            }
            
            FSXFAWidget *xfaWidget = [self getXFAWidgetAtPoint:point pageIndex:pageIndex];
            if (xfaWidget != nil && ![xfaWidget isEmpty]) {
                id<IAnnotHandler> dynamicXFAHandler = [self getDynamicAnnotHandler];
                if (dynamicXFAHandler != nil) {
                    return [dynamicXFAHandler onPageViewTouchesBegan:pageIndex touches:touches withEvent:event widget:xfaWidget];
                }
            }
        }
        
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler != nil) {
                return [annotHandler onPageViewTouchesBegan:pageIndex touches:touches withEvent:event annot:annot];
            }
        }
        return NO;
    }
    return NO;
}
- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0)
        return NO;

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    if (_currentToolHandler != nil) {
        if ([_currentToolHandler onPageViewTouchesMoved:pageIndex touches:touches withEvent:event]) {
            return YES;
        }
    } else {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewTouchesMoved:pageIndex touches:touches withEvent:event annot:annot];
        }
        point = [[touches anyObject] locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler != nil) {
                return [annotHandler onPageViewTouchesMoved:pageIndex touches:touches withEvent:event annot:annot];
            }
        }
        return NO;
    }
    return NO;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0){
        if (_currentToolHandler != nil &&[[_currentToolHandler getName] isEqualToString:Tool_Pencil]) {
            if ([_currentToolHandler onPageViewTouchesEnded:pageIndex touches:touches withEvent:event]) {
                return YES;
            }
        }
        return NO;
    }

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }
    /** The current tool will handle the tourches first if it's actived. */
    if (_currentToolHandler != nil) {
        if ([_currentToolHandler onPageViewTouchesEnded:pageIndex touches:touches withEvent:event]) {
            return YES;
        }
    } else {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewTouchesEnded:pageIndex touches:touches withEvent:event annot:annot];
        }
        
        if ([_pdfViewCtrl isDynamicXFA]) {
            FSXFAWidget *widget = nil;
            widget = self.currentWidget;
            if (widget != nil && ![widget isEmpty]) {
                annotHandler = [self getDynamicAnnotHandler];
                return [annotHandler onPageViewTouchesEnded:pageIndex touches:touches withEvent:event widget:widget];
            }
            
            FSXFAWidget *xfaWidget = [self getXFAWidgetAtPoint:point pageIndex:pageIndex];
            if (xfaWidget != nil && ![xfaWidget isEmpty]) {
                id<IAnnotHandler> dynamicXFAHandler = [self getDynamicAnnotHandler];
                if (dynamicXFAHandler != nil) {
                    return [dynamicXFAHandler onPageViewTouchesEnded:pageIndex touches:touches withEvent:event widget:xfaWidget];
                }
            }
        }
        
        point = [[touches anyObject] locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler != nil) {
                return [annotHandler onPageViewTouchesEnded:pageIndex touches:touches withEvent:event annot:annot];
            }
        }
        return NO;
    }
    return NO;
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:[_pdfViewCtrl getDisplayView]];
    int pageIndex = [_pdfViewCtrl getPageIndex:point];
    if (pageIndex < 0)
        return NO;

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_REFLOW) {
        return NO;
    }

    if (_currentToolHandler != nil) {
        if ([_currentToolHandler onPageViewTouchesCancelled:pageIndex touches:touches withEvent:event]) {
            return YES;
        }
    } else {
        //annot handler
        FSAnnot *annot = nil;
        id<IAnnotHandler> annotHandler = nil;
        annot = self.currentAnnot;
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            return [annotHandler onPageViewTouchesCancelled:pageIndex touches:touches withEvent:event annot:annot];
        }
        point = [[touches anyObject] locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
        if (annot != nil && ![annot isEmpty]) {
            annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler != nil) {
                return [annotHandler onPageViewTouchesCancelled:pageIndex touches:touches withEvent:event annot:annot];
            }
        }
        return NO;
    }
    return NO;
}

#pragma mark IDrawEventListener
- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    for (id<IToolHandler> handler in self.toolHandlers) {
        if ([handler respondsToSelector:@selector(onDraw:inContext:)]) {
            [handler onDraw:pageIndex inContext:context];
        }
    }

    if (self.currentAnnot) {
        id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:self.currentAnnot];
        if ([annotHandler respondsToSelector:@selector(onDraw:inContext:annot:)]) {
            [annotHandler onDraw:pageIndex inContext:context annot:self.currentAnnot];
        }
    }
    
    if (self.currentWidget) {
        id<IAnnotHandler> annotHandler = [self getDynamicAnnotHandler];
        if (annotHandler) {
            [annotHandler onDraw:pageIndex inContext:context widget:self.currentWidget];
        }
    }

    id<IAnnotHandler> annotHandler = [self getAnnotHandlerByType:FSAnnotLink];
    if (annotHandler) {
        if ([annotHandler respondsToSelector:@selector(onDraw:inContext:annot:)]) {
            [annotHandler onDraw:pageIndex inContext:context annot:nil];
        }
    }
}

- (void)setDocModified:(BOOL)isDocModified{
    [self willChangeValueForKey:@"isDocModified"];
    _isDocModified = isDocModified;
    if (isDocModified) {
        for (id<IDocModifiedEventListener> listener in self.docModifiedListeners) {
            if ([listener respondsToSelector:@selector(onDocModified:)]) {
                [listener onDocModified:self.pdfViewCtrl.currentDoc];
            }
        }
    }
    [self didChangeValueForKey:@"isDocModified"];
}

- (void)onAnnotAdded:(FSPDFPage *)page annot:(FSAnnot *)annot {
    for (id<IAnnotEventListener> listener in self.annotListeners) {
        if ([listener respondsToSelector:@selector(onAnnotAdded:annot:)]) {
            [listener onAnnotAdded:page annot:annot];
        }
    }
    [self addPopupWithAnnot:annot];
    self.isDocModified = YES;
    if ([self getState] != STATE_ANNOTTOOL)
        return;
    if (!self.continueAddAnnot && ![@[ Tool_Pencil, Tool_Callout, Tool_Polygon, Tool_PolyLine ] containsObject:[self.currentToolHandler getName]]) {
        [self setCurrentToolHandler:nil];
        [self changeState:STATE_EDIT];
    }
}

- (void)onAnnotWillDelete:(FSPDFPage *)page annot:(FSAnnot *)annot {
    if (annot == self.currentAnnot) {
        self.currentAnnot = nil;
    }

    for (id<IAnnotEventListener> listener in self.annotListeners) {
        if ([listener respondsToSelector:@selector(onAnnotWillDelete:annot:)]) {
            [listener onAnnotWillDelete:page annot:annot];
        }
    }
}

- (void)onAnnotDeleted:(FSPDFPage *)page annot:(FSAnnot *)annot {
    for (id<IAnnotEventListener> listener in self.annotListeners) {
        if ([listener respondsToSelector:@selector(onAnnotDeleted:annot:)]) {
            [listener onAnnotDeleted:page annot:annot];
        }
    }
    self.isDocModified = YES;
}

- (void)onAnnotModified:(FSPDFPage *)page annot:(FSAnnot *)annot {
    for (id<IAnnotEventListener> listener in self.annotListeners) {
        if ([listener respondsToSelector:@selector(onAnnotModified:annot:)]) {
            [listener onAnnotModified:page annot:annot];
        }
    }

    self.isDocModified = YES;
}
- (void)onAnnotSelected:(FSPDFPage *)page annot:(FSAnnot *)annot {
    for (id<IAnnotEventListener> listener in self.annotListeners) {
        if ([listener respondsToSelector:@selector(onAnnotSelected:annot:)]) {
            [listener onAnnotSelected:page annot:annot];
        }
    }
}

- (void)onAnnotDeselected:(FSPDFPage *)page annot:(FSAnnot *)annot {
    for (id<IAnnotEventListener> listener in self.annotListeners) {
        if ([listener respondsToSelector:@selector(onAnnotDeselected:annot:)]) {
            [listener onAnnotDeselected:page annot:annot];
        }
    }
}

#pragma mark <IScrollViewEventListener>

- (void)onScrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.isScrollViewDragging = YES;
    if (self.shouldShowPropertyBar) {
        [self hidePropertyBar];
    } else if (self.shouldShowMenu) {
        [self hideMenu];
    }
}

- (void)onScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    self.isScrollViewDragging = NO;
    if (!decelerate) { // if will decelerate, show menu or propert bar when end decelerating
        if (self.shouldShowPropertyBar) {
            [self hidePropertyBar];
        }
        if (self.shouldShowMenu) {
            [self showMenu];
        }
    }
}

- (void)onScrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    self.isScrollViewDecelerating = YES;
}

- (void)onScrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.isScrollViewDecelerating = NO;
    if (self.shouldShowPropertyBar) {
        [self hidePropertyBar];
    }
    if (self.shouldShowMenu) {
        [self showMenu];
    }
}

- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView {
    self.isScrollViewZooming = YES;
    if (self.shouldShowPropertyBar) {
        [self hidePropertyBar];
    } else if (self.shouldShowMenu) {
        [self hideMenu];
    }
}

- (void)onScrollViewDidEndZooming:(UIScrollView *)scrollView {
    self.isScrollViewZooming = NO;
    if (self.shouldShowPropertyBar) {
        [self hidePropertyBar];
    }
    if (self.shouldShowMenu) {
        [self showMenu];
    }
    if (self.zoomSlider) {
        self.zoomSlider.value = [_pdfViewCtrl getZoom];
    }
}

- (void)setCurrentAnnot:(FSAnnot *)annot {
    if ([Utility isAnnot:annot uniqueToAnnot:self.currentAnnot]) {
        return;
    }

    if (self.currentAnnot != nil && ![self.currentAnnot isEmpty]) {
        id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:self.currentAnnot];
        if (annotHandler) {
            [annotHandler onAnnotDeselected:self.currentAnnot];
            [self onAnnotDeselected:[annot getPage] annot:self.currentAnnot];
        }
    }

    _currentAnnot = annot;

    if (annot == nil) {
        self.shouldShowMenu = NO;
        self.shouldShowPropertyBar = NO;
        self.menuControl.menuItems = nil;
        [self hideMenu];
        [self hidePropertyBar];
    } else {
        int pageIndex = annot.pageIndex;
        CGRect pvRect = [Utility getAnnotRect:annot pdfViewCtrl:_pdfViewCtrl];
        pvRect = CGRectIntersection([_pdfViewCtrl getPageView:pageIndex].bounds, pvRect);
        CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
        BOOL isAnnotVisible = CGRectContainsRect(_pdfViewCtrl.bounds, dvRect);

        void (^block)(void) = ^{
            id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:annot];
            if (annotHandler) {
                [annotHandler onAnnotSelected:annot];
                [self onAnnotSelected:[annot getPage] annot:annot];
            }
        };

        if (isAnnotVisible) {
            block();
        } else {
            FSPointF *fspt = [[FSPointF alloc] init];
            FSRectF *fsrect = annot.type == FSAnnotCaret ? [Utility getCaretAnnotRect:[[FSCaret alloc] initWithAnnot:annot]] : annot.fsrect;
            [fspt set:fsrect.left y:fsrect.top];
            if (DEVICE_iPHONE) {
                //Avoid being sheltered from top bar. To do, need to check page rotation.
                [fspt setY:[fspt getY] + 64];
            }
            [_pdfViewCtrl gotoPage:pageIndex withDocPoint:fspt animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                block();
            });
        }
    }
}

- (void)setCurrentWidget:(FSXFAWidget *)currentWidget {
//    if ([Utility isAnnot:annot uniqueToAnnot:self.currentAnnot]) {
//        return;
//    }
    
    if (self.currentWidget != nil && ![self.currentWidget isEmpty]) {
        id<IAnnotHandler> annotHandler = [self getDynamicAnnotHandler];
        if (annotHandler) {
            [annotHandler onXFAWidgetDeselected:currentWidget];
        }
    }
    
    _currentWidget = currentWidget;
    
    if (currentWidget == nil) {
        self.shouldShowMenu = NO;
        self.shouldShowPropertyBar = NO;
        [self hideMenu];
        [self hidePropertyBar];
    } else {
        int pageIndex = currentWidget.getXFAPage.getIndex;
        CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:[currentWidget getRect] pageIndex:pageIndex];
        pvRect = CGRectIntersection([_pdfViewCtrl getPageView:pageIndex].bounds, pvRect);
        CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
        BOOL isAnnotVisible = CGRectContainsRect(_pdfViewCtrl.bounds, dvRect);

        void (^block)(void) = ^{
            id<IAnnotHandler> annotHandler = [self getDynamicAnnotHandler];
            if (annotHandler) {
                [annotHandler onXFAWidgetSelected:currentWidget];
            }
        };
        
        if (isAnnotVisible) {
            block();
        } else {
            FSPointF *fspt = [[FSPointF alloc] init];
            FSRectF *fsrect = [currentWidget getRect];
            [fspt set:fsrect.left y:fsrect.top];
            if (DEVICE_iPHONE) {
                //Avoid being sheltered from top bar. To do, need to check page rotation.
                [fspt setY:[fspt getY] + 64];
            }
            [_pdfViewCtrl gotoPage:pageIndex withDocPoint:fspt animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                block();
            });
        }
    }
}

#pragma mark - annot property

- (void)showProperty:(FSAnnotType)annotType rect:(CGRect)rect inView:(UIView *)view {
    // stamp
    if (annotType == FSAnnotStamp) {
        {
            __weak UIExtensionsManager *weakSelf = self;
            self.stampIconController = [[StampIconController alloc] initWithUIExtensionsManager:self];
            self.stampIconController.modalPresentationStyle = UIModalPresentationFullScreen;
            self.stampIconController.selectHandler = ^(int icon) {
                ((StampToolHandler *) [weakSelf getToolHandlerByName:Tool_Stamp]).stampIcon = icon;
                if (DEVICE_iPHONE) {
                    [weakSelf.stampIconController dismissViewControllerAnimated:YES completion:nil];
                }
            };
        }

        if (!DEVICE_iPHONE) {
            self.stampIconController.modalPresentationStyle = UIModalPresentationPopover;
            self.stampIconController.preferredContentSize = CGSizeMake(320, 420);
            self.stampIconController.popoverPresentationController.delegate = self;
            self.stampIconController.popoverPresentationController.sourceRect = rect;
            self.stampIconController.popoverPresentationController.sourceView = view;
            self.stampIconController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
        }
        UIViewController *rootViewController = self.pdfViewCtrl.fs_viewController;
        [rootViewController presentViewController:self.stampIconController animated:YES completion:nil];

        return;
    }
    // eraser
    if (annotType == FSAnnotInk && [[self.currentToolHandler getName] isEqualToString:Tool_Eraser]) {
        [self.propertyBar resetBySupportedItems:PROPERTY_LINEWIDTH frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_COLOR intValue:[UIColor grayColor].rgbValue];
        [self.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:self.eraserLineWidth];
        [self.propertyBar addListener:self];
        [self.propertyBar showPropertyBar:rect inView:view viewsCanMove:nil];
        return;
    }

    // image
    if (annotType == FSAnnotScreen) {
        [self.propertyBar resetBySupportedItems:PROPERTY_OPACITY | PROPERTY_ROTATION frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_COLOR intValue:[UIColor grayColor].rgbValue];
        [self.propertyBar setProperty:PROPERTY_OPACITY intValue:[self getAnnotOpacity:[self filterAnnotType:FSAnnotScreen]]];
        [self.propertyBar setProperty:PROPERTY_ROTATION intValue:[Utility valueForRotation:self.screenAnnotRotation]];
        [self.propertyBar addListener:self];
        [self.propertyBar showPropertyBar:rect inView:view viewsCanMove:nil];
        return;
    }

    NSArray *colors = nil;
    colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.propertyBar setColors:colors];

    if (annotType == FSAnnotSquare || annotType == FSAnnotCircle || (annotType == FSAnnotLine && ![[self.currentToolHandler getName] isEqualToString:Tool_Distance]) || annotType == FSAnnotPolygon || annotType == FSAnnotPolyLine) {
        [self.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:[self getAnnotLineWidth:annotType]];
    } else if (annotType == FSAnnotFreeText) {
        [self.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_FONTNAME | PROPERTY_FONTSIZE frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_FONTSIZE floatValue:[self getAnnotFontSize:annotType]];
        [self.propertyBar setProperty:PROPERTY_FONTNAME stringValue:[self getAnnotFontName:annotType]];
    } else if (annotType == FSAnnotInk) {
        [self.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:[self getAnnotLineWidth:annotType]];
    } else if (annotType == FSAnnotFileAttachment) {
        [self.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_ATTACHMENT_ICONTYPE frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_ATTACHMENT_ICONTYPE intValue:self.attachmentIcon];
    }
    // distance
    else if (annotType == FSAnnotLine && [[self.currentToolHandler getName] isEqualToString:Tool_Distance]) {
        [self.propertyBar resetBySupportedItems:PROPERTY_DISTANCE_UNIT | PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
        
        [self.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:[self getAnnotLineWidth:annotType]];
        [self.propertyBar setProperty:PROPERTY_COLOR intValue:[self getPropertyBarSettingColor:annotType]];
        [self.propertyBar setProperty:PROPERTY_OPACITY intValue:[self getAnnotOpacity:annotType]];
        
        [self.propertyBar setProperty:PROPERTY_DISTANCE_UNIT stringValue:self.distanceUnit];
    
        [self.propertyBar addListener:self];
        [self.propertyBar showPropertyBar:rect inView:view viewsCanMove:nil];
        return;
    }
    else {
        [self.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY frame:CGRectZero];
    }
    [self.propertyBar setProperty:PROPERTY_COLOR intValue:[self getPropertyBarSettingColor:annotType]];
    [self.propertyBar setProperty:PROPERTY_OPACITY intValue:[self getAnnotOpacity:annotType]];

    if (annotType == FSAnnotNote) {
        [self.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_ICONTYPE frame:CGRectZero];
        [self.propertyBar setProperty:PROPERTY_ICONTYPE intValue:self.noteIcon];
    }
    [self.propertyBar addListener:self];
    [self.propertyBar showPropertyBar:rect inView:view viewsCanMove:nil];
}

/** @brief Customized annotation type on application level. */
#define FSAnnotImage 111
#define FSAnnotAudio 113
#define FSAnnotVideo 115

- (int)filterAnnotType:(FSAnnotType)annotType {
    NSString *toolHandlerName = [self.currentToolHandler getName];
    if (FSAnnotLine == annotType) {
        LineToolHandler *toolHandler = [self getToolHandlerByName:Tool_Line];
        if (toolHandler.isArrowLine)
            return FSAnnotArrowLine;

        if (toolHandler.isDistanceTool) {
            return FSAnnotDistance;
        }
    } else if (FSAnnotCaret == annotType) {
        if ([toolHandlerName isEqualToString:Tool_Insert]) {
            return FSAnnotInsert;
        } else if ([toolHandlerName isEqualToString:Tool_Replace]) {
            return FSAnnotReplace;
        } else if (self.currentAnnot && ![self.currentAnnot.intent isEqualToString:@"Replace"]) {
            return FSAnnotInsert;
        }
    } else if (FSAnnotFreeText == annotType) {
        if ([toolHandlerName isEqualToString:Tool_Textbox]) {
            return FSAnnotTextbox;
        } else if ([toolHandlerName isEqualToString:Tool_Callout]) {
            return FSAnnotCallout;
        } else if (self.currentAnnot && self.currentAnnot.intent.length == 0) {
            return FSAnnotTextbox;
        } else if (self.currentAnnot && [self.currentAnnot.intent isEqualToString:@"FreeTextCallout"]) {
            return FSAnnotCallout;
        }
    } else if (FSAnnotPolygon == annotType) {
        BOOL isPolygon = YES;
        if ([[self.currentToolHandler getName] isEqualToString:Tool_Polygon]) {
            isPolygon = ((PolygonToolHandler *) self.currentToolHandler).isPolygon;
        } else if (self.currentAnnot && self.currentAnnot.type == FSAnnotPolygon) {
            isPolygon = !([[([[FSPolygon alloc] initWithAnnot:self.currentAnnot]) getBorderInfo] getStyle] == FSBorderInfoCloudy);
        }
        if (!isPolygon) {
            return FSAnnotCloud;
        }
    }else if (annotType == FSAnnotScreen) {
        if ([[self.currentToolHandler getName] isEqualToString:Tool_Image] ||
            (self.currentAnnot && self.currentAnnot.screenActiontype == FSScreenActionTypeImage)) {
            return FSAnnotImage;
        }
    }

    return (int)annotType;
}

- (unsigned int)getPropertyBarSettingColor:(FSAnnotType)annotType {
    return [self getAnnotColor:[self filterAnnotType:annotType]];
}

- (unsigned int)getPropertyBarSettingOpacity:(FSAnnotType)annotType {
    return [self getAnnotOpacity:annotType];
}

-(BOOL)checkValidAnnotType:(FSAnnotType)annotType{
    NSArray *unsupportAnnotTypes = @[@(FSAnnotPrinterMark), @(FSAnnotTrapNet), @(FSAnnotWatermark), @(FSAnnot3D), @(FSAnnotPopup), @(FSAnnotUnknownType), @(FSAnnotLink), @(FSAnnotPSInk), @(FSAnnotSound), @(FSAnnotMovie), @(FSAnnotWidget)];
    if ([unsupportAnnotTypes containsObject:@(annotType)]) {
        return NO;
    }
    
    NSMutableArray *annotTypesArr = @[].mutableCopy;
    for (int i =0 ; i < 28 ; i++) {
        [annotTypesArr addObject:@(i)];
    }
    
    NSArray *customUIFSAnnotType = @[@(FSAnnotArrowLine), @(FSAnnotInsert), @(FSAnnotReplace), @(FSAnnotTextbox), @(FSAnnotCloud), @(FSAnnotCallout), @(FSAnnotDistance)];
    if (![annotTypesArr containsObject:@(annotType)] && ![customUIFSAnnotType containsObject:@(annotType)] ) {
        return NO;
    }
    return  YES;
}

- (unsigned int)getAnnotTextColor:(FSAnnotType)annotType{
    if (![self checkValidAnnotType:annotType]) {
        return 0;
    }
    NSNumber *colorNum = self.annotTextColors[[NSNumber numberWithInt:[self filterAnnotType:annotType]]];
    if (colorNum != nil) {
        return colorNum.intValue;
    } else {
        // markup
        Annotations *annotations = self.config.defaultSettings.annotations;
    
        // free text
        if (annotType == FSAnnotFreeText) {
             return annotations.typewriter.textColor.rgbValue;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotCallout) {
            return annotations.callout.textColor.rgbValue;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotTextbox) {
            return annotations.textbox.textColor.rgbValue;
        }
        else if (annotType == FSAnnotRedact) {
            return annotations.redaction.textColor.rgbValue;
        }
        else {
            return 0;
        }
    }
}

- (unsigned int)getAnnotColor:(FSAnnotType)annotType {
    if (![self checkValidAnnotType:annotType]) {
        return 0;
    }
    
    NSNumber *colorNum = self.annotColors[[NSNumber numberWithInt:[self filterAnnotType:annotType]]];
    if (colorNum != nil) {
        return colorNum.intValue;
    } else {
        // markup
        Annotations *annotations = self.config.defaultSettings.annotations;
    
        if (annotType == FSAnnotHighlight) {
            return annotations.highlight.color.rgbValue;
        }else if (annotType == FSAnnotSquiggly) {
            return annotations.squiggly.color.rgbValue;
        }
        else if (annotType == FSAnnotUnderline) {
            return annotations.underline.color.rgbValue;
        } else if (annotType == FSAnnotStrikeOut) {
            return annotations.strikeout.color.rgbValue;
        }
        // note
        else if (annotType == FSAnnotNote) {
            return annotations.note.color.rgbValue;
        }
        // shape
        else if (annotType == FSAnnotSquare) {
            return annotations.rectangle.color.rgbValue;
        }
        else if (annotType == FSAnnotCircle) {
            return annotations.oval.color.rgbValue;
        }
        else if (annotType == FSAnnotInk) {
            return annotations.pencil.color.rgbValue;
        }
        else if (annotType == FSAnnotPolygon) {
            return annotations.polygon.color.rgbValue;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotCloud) {
            return annotations.cloud.color.rgbValue;
        }
        else if (annotType == FSAnnotPolyLine) {
            return annotations.polyline.color.rgbValue;
        }
        else if (annotType == FSAnnotLine) {
            return annotations.line.color.rgbValue;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotDistance) {
            return annotations.distance.color.rgbValue;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotArrowLine) {
            return annotations.arrow.color.rgbValue;
        }
        // free text
        else if (annotType == FSAnnotFreeText) {
             return [self getAnnotTextColor:annotType];
        }
        else if ([self filterAnnotType:annotType] == FSAnnotCallout) {
            return [self getAnnotTextColor:annotType];
        }
        else if ([self filterAnnotType:annotType] == FSAnnotTextbox) {
            return [self getAnnotTextColor:annotType];
        }
        else if (annotType == FSAnnotCaret) {
            return 0;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotInsert) {
            return annotations.insert.color.rgbValue;
        }
        else if ([self filterAnnotType:annotType] == FSAnnotReplace){
            return annotations.replace.color.rgbValue;
        }
        else if (annotType == FSAnnotFileAttachment) {
            return annotations.attachment.color.rgbValue;
        }
        else if (annotType == FSAnnotRedact) {
            return annotations.redaction.fillColor.rgbValue;
        }
        else {
            return 0;
        }
    }
}

- (void)setAnnotTextColor:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (![self checkValidAnnotType:annotType]) {
        return ;
    }
    self.annotTextColors[[NSNumber numberWithInt:[self filterAnnotType:annotType]]] = @(color);
    for (id<IAnnotPropertyListener> listender in self.annotPropertyListeners) {
        if ([listender respondsToSelector:@selector(onAnnotColorChanged:colorType:annotType:)])
            [listender onAnnotColorChanged:color colorType:FSAnnotPropertyColorTypeText annotType:annotType];
    }
}

- (void)setAnnotColor:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (![self checkValidAnnotType:annotType]) {
        return ;
    }
    self.annotColors[[NSNumber numberWithInt:[self filterAnnotType:annotType]]] = @(color);
    for (id<IAnnotPropertyListener> listender in self.annotPropertyListeners) {
        if ([listender respondsToSelector:@selector(onAnnotColorChanged:annotType:)])
            [listender onAnnotColorChanged:color annotType:annotType];
    }
}

- (int)getAnnotOpacity:(FSAnnotType)annotType {
    int filterAnnotType = [self filterAnnotType:annotType];
    int annotOpacity = ((NSNumber *) self.annotOpacities[[NSNumber numberWithInt:filterAnnotType]]).intValue;
    if (!annotOpacity) {
        float opacity = 1.f;
        Annotations *annotations = self.config.defaultSettings.annotations;
        if (annotType == FSAnnotHighlight) {
            opacity = annotations.highlight.opacity;
        }else if (annotType == FSAnnotUnderline) {
            opacity = annotations.underline.opacity;
        }else if (annotType == FSAnnotSquiggly) {
            opacity = annotations.squiggly.opacity;
        }else if (annotType == FSAnnotStrikeOut) {
            opacity = annotations.strikeout.opacity;
        }else if (annotType == FSAnnotCaret) {
            if (filterAnnotType == FSAnnotInsert) {
                opacity = annotations.insert.opacity;
            }else{
                opacity =  annotations.replace.opacity;
            }
        }else if (annotType == FSAnnotLine) {
            if (filterAnnotType == FSAnnotDistance) {
                 opacity = annotations.distance.opacity;
            }
            else if (filterAnnotType == FSAnnotArrowLine) {
                 opacity = annotations.arrow.opacity;
            }else{
                opacity = annotations.line.opacity;
            }
        }else if (annotType == FSAnnotSquare) {
            opacity = annotations.rectangle.opacity;
        }else if (annotType == FSAnnotCircle) {
            opacity = annotations.oval.opacity;
        }else if (annotType == FSAnnotInk) {
            opacity = annotations.pencil.opacity;
        }else if (annotType == FSAnnotPolygon) {
            if (filterAnnotType == FSAnnotCloud) {
                opacity = annotations.cloud.opacity;
            }else{
                opacity = annotations.polygon.opacity;
            }
        }else if (annotType == FSAnnotPolyLine) {
            opacity = annotations.polyline.opacity;
        }else if (annotType == FSAnnotFreeText) {
            if (filterAnnotType == FSAnnotCallout) {
                opacity = annotations.callout.opacity;
            }
            else if (filterAnnotType == FSAnnotTextbox) {
                 opacity = annotations.textbox.opacity;
            }else{
                opacity = annotations.typewriter.opacity;
            }
        }else if (annotType == FSAnnotNote) {
            opacity = annotations.note.opacity;
        }else if (annotType == FSAnnotFileAttachment) {
            opacity = annotations.attachment.opacity;
        }else if (annotType == FSAnnotScreen) {
            if (filterAnnotType == FSAnnotImage) {
                 opacity = annotations.image.opacity;
            }
        }
        annotOpacity = (int)(opacity * 100);
    }
    return annotOpacity;
}

- (void)setAnnotOpacity:(int)opacity annotType:(FSAnnotType)annotType {
    self.annotOpacities[[NSNumber numberWithInt:[self filterAnnotType:annotType]]] = @(opacity);
    for (id<IAnnotPropertyListener> listender in self.annotPropertyListeners) {
        if ([listender respondsToSelector:@selector(onAnnotOpacityChanged:annotType:)])
            [listender onAnnotOpacityChanged:opacity annotType:annotType];
    }
}

- (int)getAnnotLineWidth:(FSAnnotType)annotType {
    int filterAnnotType = [self filterAnnotType:annotType];
    NSNumber *widthNum = self.annotLineWidths[[NSNumber numberWithInt:[self filterAnnotType:annotType]]];
    if (widthNum != nil) {
        return widthNum.intValue;
    } else {
        int thickness = 2;
        Annotations *annotations = self.config.defaultSettings.annotations;
        if (annotType == FSAnnotLine) {
            if (filterAnnotType == FSAnnotDistance) {
                thickness = annotations.distance.thickness;
            }
            else if (filterAnnotType == FSAnnotArrowLine) {
                thickness = annotations.arrow.thickness;
            }else{
                thickness = annotations.line.thickness;
            }
        }else if (annotType == FSAnnotSquare) {
            thickness = annotations.rectangle.thickness;
        }else if (annotType == FSAnnotCircle) {
            thickness = annotations.oval.thickness;
        }else if (annotType == FSAnnotInk) {
            thickness = annotations.pencil.thickness;
        }else if (annotType == FSAnnotPolygon) {
            if (filterAnnotType == FSAnnotCloud) {
                thickness = annotations.cloud.thickness;
            }else{
                thickness = annotations.polygon.thickness;
            }
        }else if (annotType == FSAnnotPolyLine) {
            thickness = annotations.polyline.thickness;
        }
        return thickness;
    }
}

- (void)setAnnotLineWidth:(int)lineWidth annotType:(FSAnnotType)annotType {
    self.annotLineWidths[[NSNumber numberWithInt:[self filterAnnotType:annotType]]] = @(lineWidth);
    for (id<IAnnotPropertyListener> listener in self.annotPropertyListeners) {
        if ([listener respondsToSelector:@selector(onAnnotLineWidthChanged:annotType:)]) {
            [listener onAnnotLineWidthChanged:lineWidth annotType:annotType];
        }
    }
}

- (int)getAnnotFontSize:(FSAnnotType)annotType {
    int filterAnnotType = [self filterAnnotType:annotType];
    NSNumber *num = (NSNumber *) self.annotFontSizes[@(filterAnnotType)];
    if (num) {
        return num.intValue;
    } else {
        int textSize = 18;
        Annotations *annotations = self.config.defaultSettings.annotations;
        if (annotType == FSAnnotFreeText) {
            if (filterAnnotType == FSAnnotCallout) {
                textSize = annotations.callout.textSize;
            }
            else if (filterAnnotType == FSAnnotTextbox) {
                textSize = annotations.textbox.textSize;
            }else{
                textSize = annotations.typewriter.textSize;
            }
        }else if (annotType == FSAnnotRedact){
            textSize = annotations.redaction.textSize;
        }
        return textSize;
    }
}

- (void)setAnnotFontSize:(int)fontSize annotType:(FSAnnotType)annotType {
    self.annotFontSizes[@([self filterAnnotType:annotType])] = @(fontSize);
    for (id<IAnnotPropertyListener> listener in self.annotPropertyListeners) {
        if ([listener respondsToSelector:@selector(onAnnotFontSizeChanged:annotType:)]) {
            [listener onAnnotFontSizeChanged:fontSize annotType:annotType];
        }
    }
}

- (NSString *)getAnnotFontName:(FSAnnotType)annotType {
    int filterAnnotType = [self filterAnnotType:annotType];
    NSString *annotFontName = self.annotFontNames[@(filterAnnotType)];
    if (!annotFontName) {
        annotFontName = @"Courier";
        Annotations *annotations = self.config.defaultSettings.annotations;
        if (annotType == FSAnnotFreeText) {
            if (filterAnnotType == FSAnnotCallout) {
                annotFontName = annotations.callout.textFace;
            }
            else if (filterAnnotType == FSAnnotTextbox) {
                annotFontName = annotations.textbox.textFace;
            }else{
                annotFontName = annotations.typewriter.textFace;
            }
        }else if (annotType == FSAnnotRedact){
            annotFontName = annotations.redaction.textFace;
        }
    }
    return annotFontName;
}

- (void)setAnnotFontName:(NSString *)fontName annotType:(FSAnnotType)annotType {
    self.annotFontNames[@([self filterAnnotType:annotType])] = fontName;
    for (id<IAnnotPropertyListener> listener in self.annotPropertyListeners) {
        if ([listener respondsToSelector:@selector(onAnnotFontNameChanged:annotType:)]) {
            [listener onAnnotFontNameChanged:fontName annotType:annotType];
        }
    }
}

- (void)setNoteIcon:(int)noteIcon {
    if (noteIcon < 0) {
        noteIcon = 2;
    }
    _noteIcon = noteIcon;
    for (id<IAnnotPropertyListener> listener in self.annotPropertyListeners) {
        if ([listener respondsToSelector:@selector(onAnnotIconChanged:annotType:)]) {
            [listener onAnnotIconChanged:noteIcon annotType:FSAnnotNote];
        }
    }
}

- (void)setAttachmentIcon:(int)attachmentIcon {
    if (attachmentIcon < 0) {
        attachmentIcon = 1;
    }
    _attachmentIcon = attachmentIcon;
    for (id<IAnnotPropertyListener> listener in self.annotPropertyListeners) {
        if ([listener respondsToSelector:@selector(onAnnotIconChanged:annotType:)]) {
            [listener onAnnotIconChanged:attachmentIcon annotType:FSAnnotFileAttachment];
        }
    }
}

- (void)setScreenAnnotRotation:(FSRotation)screenAnnotRotation {
    _screenAnnotRotation = screenAnnotRotation;
    for (id<IAnnotPropertyListener> listener in self.annotPropertyListeners) {
        if ([listener respondsToSelector:@selector(onAnnotRotationChanged:annotType:)]) {
            [listener onAnnotRotationChanged:screenAnnotRotation annotType:FSAnnotScreen];
        }
    }
}

#pragma mark - IPropertyValueChangedListener
- (void)onProperty:(long)property changedFrom:(NSValue *)oldValue to:(NSValue *)newValue {
    FSAnnot *annot = self.currentAnnot;
    if (annot && ![annot isEmpty]) {
        //        BOOL addUndo;
        //        switch (annot.type) {
        //        case FSAnnotNote:
        //        case FSAnnotCircle:
        //        case FSAnnotSquare:
        //        case FSAnnotFreeText:
        //        // text markup
        //        case FSAnnotHighlight:
        //        case FSAnnotUnderline:
        //        case FSAnnotStrikeOut:
        //        case FSAnnotSquiggly:
        //
        //        case FSAnnotLine:
        //        case FSAnnotInk:
        //        case FSAnnotCaret:
        //        case FSAnnotStamp:
        //            addUndo = NO;
        //            break;
        //        default:
        //            addUndo = YES;
        //            break;
        //        }
        [self changeAnnot:annot property:property from:oldValue to:newValue];
        if (!self.canUpdateAnnotDefaultProperties) return;
    }

    FSAnnotType annotType = annot ? annot.type : _currentToolHandler.type;
    switch (property) {
    case PROPERTY_COLOR:
        [self setAnnotColor:[(NSNumber *) newValue unsignedIntValue] annotType:annotType];
        break;
    case PROPERTY_OPACITY:
        [self setAnnotOpacity:[(NSNumber *) newValue unsignedIntValue] annotType:annotType];
        break;
    case PROPERTY_ICONTYPE:
        self.noteIcon = [(NSNumber *) newValue intValue];
        break;
    case PROPERTY_ATTACHMENT_ICONTYPE:
        self.attachmentIcon = [(NSNumber *) newValue unsignedIntValue];
        break;
    case PROPERTY_LINEWIDTH:
        if ([[self.currentToolHandler getName] isEqualToString:Tool_Eraser]) {
            self.eraserLineWidth = [(NSNumber *) newValue unsignedIntValue];
        } else {
            [self setAnnotLineWidth:[(NSNumber *) newValue unsignedIntValue] annotType:annotType];
        }
        break;
    case PROPERTY_FONTSIZE:
        [self setAnnotFontSize:[(NSNumber *) newValue unsignedIntValue] annotType:annotType];
        break;
    case PROPERTY_FONTNAME:
        [self setAnnotFontName:(NSString *) [newValue nonretainedObjectValue] annotType:annotType];
        break;
    case PROPERTY_DISTANCE_UNIT:
        self.distanceUnit = (NSString *) [newValue nonretainedObjectValue];
            break;
    case PROPERTY_ROTATION:
        self.screenAnnotRotation = [Utility rotationForValue:newValue];
        break;
    default:
        break;
    }
}

- (void)changeAnnot:(FSAnnot *)annot property:(long)property from:(NSValue *)oldValue to:(NSValue *)newValue // addUndo:(BOOL)addUndo
{
    int pageIndex = annot.pageIndex;
    CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
    BOOL modified = YES;
    switch (property) {
    case PROPERTY_COLOR:
        if (annot.type == FSAnnotRedact) {
            FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
            redact.applyFillColor = [(NSNumber *) newValue unsignedIntValue];
        }else{
            annot.color = [(NSNumber *) newValue unsignedIntValue];
        }
        break;
    case PROPERTY_OPACITY:
        annot.opacity = [(NSNumber *) newValue unsignedIntValue] / 100.0f;
        break;
    case PROPERTY_ICONTYPE:
        if (annot.type == FSAnnotNote) {
            ([[FSNote alloc] initWithAnnot:annot]).icon = [(NSNumber *) newValue unsignedIntValue];
        } else {
            modified = NO;
        }
        break;
    case PROPERTY_ATTACHMENT_ICONTYPE:
        if (annot.type == FSAnnotFileAttachment) {
            ([[FSFileAttachment alloc] initWithAnnot:annot]).icon = [(NSNumber *) newValue unsignedIntValue];
        } else {
            modified = NO;
        }
        break;
    case PROPERTY_LINEWIDTH:
        annot.lineWidth = [(NSNumber *) newValue unsignedIntValue];
        break;
    case PROPERTY_FONTSIZE:{
            int newFontSize = [(NSNumber *) newValue unsignedIntValue];
            FSDefaultAppearance * (^updateDefaultAppearance)(FSDefaultAppearance *) = ^(FSDefaultAppearance *ap){
                ap.text_size = newFontSize;
                return ap;
            };
        if (annot.type == FSAnnotFreeText) {
            FSFreeText *freeText = [[FSFreeText alloc] initWithAnnot:annot];
            [freeText setDefaultAppearance:updateDefaultAppearance([freeText getDefaultAppearance])];
        }else if (annot.type == FSAnnotRedact){
            FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
            [redact setDefaultAppearance:updateDefaultAppearance([redact getDefaultAppearance])];
        }else if (annot.type == FSAnnotWidget){
            FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
            FSControl *control = [widget getControl];
            FSDefaultAppearance *ap = [control getDefaultAppearance];
            ap.flags |= FSDefaultAppearanceFlagFontSize;
            [control setDefaultAppearance:updateDefaultAppearance(ap)];
        }
        else {
            modified = NO;
        }
    }
        break;
    case PROPERTY_FONTNAME:{
        NSString *newFontName = (NSString *) [newValue nonretainedObjectValue];
        FSDefaultAppearance * (^updateDefaultAppearance)(FSDefaultAppearance *) = ^(FSDefaultAppearance *ap){
            FSDefaultAppearance *updateAp = nil;
            FSFont *originFont = ap.font;
            int fontID = [Utility toStandardFontID:newFontName];
            FSFont *font = nil;
            if (fontID == -1) {
                font = [[FSFont alloc] initWithName:newFontName styles:0 charset:FSFontCharsetDefault weight:0];
            } else {
                font = [[FSFont alloc] initWithFont_id:fontID];
            }
            ap.font = font;
            if ([newFontName caseInsensitiveCompare:[originFont getName]] != NSOrderedSame) {
                updateAp = ap;
            }
            return updateAp;
        };
        if (annot.type == FSAnnotFreeText) {
            FSFreeText *freeText = [[FSFreeText alloc] initWithAnnot:annot];
            FSDefaultAppearance *ap = updateDefaultAppearance([freeText getDefaultAppearance]);
            if (ap) [freeText setDefaultAppearance:ap];
        }else if (annot.type == FSAnnotRedact){
            FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
            FSDefaultAppearance *ap = updateDefaultAppearance([redact getDefaultAppearance]);
            if (ap) [redact setDefaultAppearance:ap];
        }else if (annot.type == FSAnnotWidget){
            FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
            FSControl *control = [widget getControl];
            FSDefaultAppearance *ap = updateDefaultAppearance([control getDefaultAppearance]);
            if (ap) [control setDefaultAppearance:ap];
        }
        else {
            modified = NO;
        }
    }
        break;
    case PROPERTY_DISTANCE_UNIT:
        if (annot.type == FSAnnotLine) {
            FSLine *lineAnnot = [[FSLine alloc] initWithAnnot:annot];
            if ([[lineAnnot getIntent] isEqualToString:@"LineDimension"]) {
                NSString *unitName = (NSString *) [newValue nonretainedObjectValue];
                annot.subject = unitName;
            }
        }
        break;
    case PROPERTY_ROTATION:
        if (annot.type == FSAnnotScreen) {
            FSRotation rotation = [Utility rotationForValue:newValue];
            if (rotation != 0) {
                rotation = 4 - rotation;
            }
            [[[FSScreen alloc] initWithAnnot:annot] setRotation:rotation];
        } else {
            modified = NO;
        }
        break;
        case PROPERTY_TEXTINPUT:{
            FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
            if ([newValue respondsToSelector:@selector(unsignedIntValue)]) {
                int color = [(NSNumber *) newValue unsignedIntValue];
                FSDefaultAppearance *ap = [redact getDefaultAppearance];
                ap.text_color = color;
                [redact setDefaultAppearance:ap];
                [self setAnnotTextColor:color annotType:FSAnnotRedact];
            }else {
                NSString *coverText = (NSString *) [newValue nonretainedObjectValue];
                redact.overlayText = coverText;
            }
        }
        break;
    case PROPERTY_TEXTNAME:{
        // fix Topbar shows an abnormal problem when the keyboard is retracted
        if (self.pdfViewCtrl.superview) {
            [self.pdfViewCtrl.superview setNeedsLayout];
            [self.pdfViewCtrl.superview layoutIfNeeded];
        }
        NSString *newName = (NSString *) [newValue nonretainedObjectValue];
        NSString *tmp = [newName stringByReplacingOccurrencesOfString:@"." withString:@""];
        BOOL shouldShowMenu = NO;
        if (self.shouldShowMenu) {
            shouldShowMenu = YES;
            [self hideMenu];
        }
        if (tmp.length) {
            if ([newName hasPrefix:@"."] || [newName hasSuffix:@"."] || [newName containsString:@".."]) {
                [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kError") message: FSLocalizedForKey(@"kFieldRenamFailed") actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                    if (shouldShowMenu) {
                        [self showMenu];
                    }
                }];
            }
            newName = [newName stringByReplacingOccurrencesOfString:@".." withString:@"."];
             while ([newName hasSuffix:@"."]) {
                newName = [newName substringToIndex:newName.length - 1];
            }
        }else{
             [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kError") message: FSLocalizedForKey(@"kFieldRenamFailed") actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                 if (shouldShowMenu) {
                     [self showMenu];
                 }
             }];
             return;
        }
        if (shouldShowMenu) {
            [self showMenu];
        }
        if (newName.length) {
            FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
            FSField *field = [widget getField];
            FSForm *form = [[FSForm alloc] initWithDocument:annot.getPage.getDocument];
            if (field.getControlCount == 1) {
                [form renameField:field new_field_name:newName];
            }else{
                 [form moveControl:widget.getControl field_name:newName];
            }
        }
    }
        break;
    case PROPERTY_FORMOPTION:{
        FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
        FSField *field = [widget getField];
        if ([newValue respondsToSelector:@selector(boolValue)]) {
            BOOL on = [(NSNumber *)newValue boolValue];
            if (field.getType == FSFieldTypeComboBox ) {
                if (on) {
                    field.flags |= FSFieldFlagComboEdit;
                }else{
                    field.flags &= ~FSFieldFlagComboEdit;
                }
            }
            else if (field.getType == FSFieldTypeListBox ) {
                if (on) {
                    field.flags |= FSFieldFlagChoiceMultiSelect;
                }else{
                    field.flags &= ~FSFieldFlagChoiceMultiSelect;
                }
            }
        }
        else if ([newValue respondsToSelector:@selector(nonretainedObjectValue)]) {
            FSChoiceOptionArray *options = [newValue nonretainedObjectValue];
            field.options = options;
        }
    }
    break;
    default:
        modified = NO;
        break;
    }
    if (modified) {
        FSDateTime *now = [Utility convert2FSDateTime:[NSDate date]];
        [annot setModifiedDateTime:now];

        FSRectF *oldAnnotRect = [[FSRectF alloc] initWithLeft1:annot.fsrect.left bottom1:annot.fsrect.bottom right1:annot.fsrect.right top1:annot.fsrect.top];
        [_pdfViewCtrl lockRefresh];
        [annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        
        if (annot.type == FSAnnotFreeText && property == PROPERTY_FONTSIZE ) {
            CGRect refreshrect = [_pdfViewCtrl convertPdfRectToPageViewRect:oldAnnotRect pageIndex:pageIndex];
            CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
            refreshrect = CGRectUnion(refreshrect, newRect);
            refreshrect = CGRectInset(refreshrect, -30, -30);
            [_pdfViewCtrl refresh:refreshrect pageIndex:pageIndex];
        }

        // keep annot in bounds
        {
            FSRectF *rect = annot.fsrect;
            FSPDFPage *page = [annot getPage];
            BOOL isOutOfBounds = NO;
            if (rect.bottom < 0) {
                rect.top -= rect.bottom;
                rect.bottom = 0;
                isOutOfBounds = YES;
            }
            float w = [page getWidth];
            if (rect.right > w) {
                rect.left -= rect.right - w;
                rect.right = w;
                isOutOfBounds = YES;
            }
            if (isOutOfBounds) {
                annot.fsrect = rect;
                [annot resetAppearanceStream];
            }
        }

        id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:annot];
        if ([annotHandler respondsToSelector:@selector(onAnnotChanged:property:from:to:)]) {
            [annotHandler onAnnotChanged:annot property:property from:oldValue to:newValue];
        }

        if ([self shouldDrawAnnot:annot]) {
            CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
            rect = CGRectUnion(rect, oldRect);
            rect = CGRectInset(rect, -20, -20);
            [_pdfViewCtrl refresh:rect pageIndex:pageIndex];
        }

        if (annot.type == FSAnnotCaret) {
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            if ([markup isGrouped]) {
                FSMarkupArray *groupAnnots = [markup getGroupElements];
                for (int i = 0; i < [groupAnnots getSize]; i++) {
                    FSAnnot *groupAnnot = [groupAnnots getAt:i];
                    if (groupAnnot && ![groupAnnot isEmpty] && ![groupAnnot.NM isEqualToString:annot.NM]) {
                        [self changeAnnot:groupAnnot property:property from:oldValue to:newValue];
                    }
                }
            }
        }
    }
}

- (void)registerAnnotPropertyListener:(id<IAnnotPropertyListener>)listener {
    [self.annotPropertyListeners addObject:listener];
}

- (void)unregisterAnnotPropertyListener:(id<IAnnotPropertyListener>)listener {
    if (listener) {
        [self.annotPropertyListeners removeObject:listener];
    }
}

- (void)showSearchBar:(BOOL)show {
    __block SearchModule *search = nil;
    [self.modules enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id<IModule> module = obj;
        if ([module respondsToSelector:@selector(getName)] &&
            [[module getName] isEqualToString:@"Search"]) {
            search = (SearchModule *) module;
            *stop = YES;
        }
    }];
        [search showSearchBar:show];
}

- (NSString *)getCurrentSelectedText {
    id<IToolHandler> toolHandler = [self getToolHandlerByName:Tool_Select];
    if (!toolHandler)
        return nil;
    SelectToolHandler *selHandler = (SelectToolHandler *) toolHandler;
    return [selHandler copyText];
}

#pragma mark - IRecoveryEventListener

- (void)onWillRecover {
    self.currentAnnot = nil;
    self.currentToolHandler = nil;
}

- (void)onRecovered {
    [FSLibrary setAnnotIconProviderCallback:_iconProvider];
    [FSLibrary setActionCallback:_actionHandler];
}

- (BOOL)shouldDrawAnnot:(FSAnnot *)annot {
    if (annot && ![annot isEmpty]) {
        id<IAnnotHandler> annotHandler = [self getAnnotHandlerByAnnot:annot];
        if ([annotHandler respondsToSelector:@selector(shouldDrawAnnot:inPDFViewCtrl:)]) {
            return [annotHandler shouldDrawAnnot:annot inPDFViewCtrl:self.pdfViewCtrl];
        }
    }
    return YES;
}

#pragma mark <UIPopoverPresentationControllerDelegate>

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    if (_settingbarPopoverCtr) {
        [self setHiddenSettingBar:YES];
        _settingbarPopoverCtr = nil;
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

#if _MAC_CATALYST_
- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView  * __nonnull * __nonnull)view{
    UIView *fromView = [self.bottomToolbar viewWithTag:FS_BOTTOMBAR_ITEM_READMODE_TAG];
    CGRect showRect = [_pdfViewCtrl convertRect:fromView.bounds fromView:fromView];
    *rect = showRect;
}
#endif

#pragma mark IDocEventListener

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
    _currentAnnot = nil;
    _currentToolHandler = nil;
    [self clearUndoRedo];
    self.menuControl.menuItems = nil;
}

#pragma mark thumbnail

- (void)showThumbnailView {
    if (!self.thumbnailViewController) {
        self.thumbnailViewController = [[FSThumbnailViewController alloc] initWithViewCtrl:self.pdfViewCtrl];
        self.thumbnailViewController.delegate = self;
        self.thumbnailViewController.pageManipulationDelegate = self.pdfViewCtrl;
        self.thumbnailViewController.pdfViewCtrl = self.pdfViewCtrl;
        self.thumbnailViewController.extensionsManager = self;
        [self registerRotateChangedListener:self.thumbnailViewController];
    }

    [self.pdfViewCtrl addSubview:self.thumbnailViewController.view];
    [UIView transitionWithView:self.pdfViewCtrl duration:0.8 options:UIViewAnimationOptionTransitionFlipFromRight animations:nil completion:nil];
}

- (void)removeThumbnailView{
    if (self.thumbnailViewController) {
        [self unregisterRotateChangedListener:self.thumbnailViewController];
        [self.thumbnailViewController.view removeFromSuperview];
        self.thumbnailViewController = nil; //to delete
    }
}

- (void)removeThumbnailCacheOfPageAtIndex:(NSUInteger)pageIndex {
    [self.thumbnailCache removeThumbnailCacheOfPageAtIndex:pageIndex];
}

- (void)clearThumbnailCachesForPDFAtPath:(NSString *)path {
    [self.thumbnailCache clearThumbnailCachesForPDFAtPath:path];
}

#pragma mark <FSThumbnailViewControllerDelegate>

- (void)exitThumbnailViewController:(FSThumbnailViewController *)thumbnailViewController {
    [self removeThumbnailView];
    [UIView transitionWithView:_pdfViewCtrl duration:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:nil completion:nil];
    [self.settingBar updateLayoutButtonsWithLayout:[_pdfViewCtrl getPageLayoutMode]];
}

- (void)thumbnailViewController:(FSThumbnailViewController *)thumbnailViewController openPage:(int)page {
    [self removeThumbnailView];
    [_pdfViewCtrl gotoPage:page animated:NO];
    [UIView transitionWithView:_pdfViewCtrl duration:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:nil completion:nil];
    [self.settingBar updateLayoutButtonsWithLayout:[_pdfViewCtrl getPageLayoutMode]];
}

- (void)thumbnailViewController:(FSThumbnailViewController *)thumbnailViewController getThumbnailForPageAtIndex:(NSUInteger)index thumbnailSize:(CGSize)thumbnailSize needPause:(BOOL (^__nullable)(void))needPause callback:(void (^__nonnull)(UIImage *))callback {
    [self.thumbnailCache getThumbnailForPageAtIndex:index withThumbnailSize:thumbnailSize needPause:needPause callback:callback];
}

- (void)buildToolbars {
    CGRect screenFrame = _pdfViewCtrl.bounds;

    maskView = [[UIControl alloc] initWithFrame:_pdfViewCtrl.bounds];
    maskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;

    self.topToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, 44)];
    self.topToolbar.barTintColor = NormalBarBackgroundColor;
 
    self.bottomToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, screenFrame.size.height - 49, screenFrame.size.width, 49)];
    self.bottomToolbar.barTintColor = NormalBarBackgroundColor;

    self.editDoneContentBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
    
    self.editDoneBar = [[TbBaseBar alloc] init];
    self.editDoneBar.contentView.frame = CGRectMake(0, 0, 200, 44);
    self.editDoneBar.top = NO;
    self.editDoneBar.hasDivide = NO;

    self.editBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];

    self.editBar.frame = CGRectMake(0, screenFrame.size.height - 49, screenFrame.size.width, 49);

    self.toolSetContentBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];
    
    self.toolSetBar = [[TbBaseBar alloc] init];
    self.toolSetBar.contentView.frame = CGRectMake(0, screenFrame.size.height - 49, screenFrame.size.width, 49);
    self.toolSetBar.hasDivide = NO;
    self.toolSetBar.top = NO;
    if (DEVICE_iPHONE) {
        self.toolSetBar.intervalWidth = 20;
    } else {
        self.toolSetBar.intervalWidth = 30;
    }

    self.more = [[MoreMenuView alloc] init];
    [self.more getContentView].frame = CGRectMake(_pdfViewCtrl.bounds.size.width - 300, 0, 300, _pdfViewCtrl.bounds.size.height);
    [self.more setMenuTitle:FSLocalizedForKey(@"kMore")];
    typeof(self) __weak weakSelf = self;
    self.more.onCancelClicked = ^{
        [weakSelf setHiddenMoreMenu:YES];
    };

    self.annotationToolsBar = [[AnnotationToolsBar alloc] initWithUIExtensionsManager:self];
    
    [self.editDoneContentBar addSubview:self.editDoneBar.contentView];
    [self.toolSetContentBar addSubview:self.toolSetBar.contentView];
    
    [self.pdfViewCtrl addSubview:self.topToolbar];
    [self.pdfViewCtrl addSubview:self.bottomToolbar];
    [self.pdfViewCtrl addSubview:self.editDoneContentBar];
    [self.pdfViewCtrl addSubview:self.editBar];
    [self.pdfViewCtrl addSubview:self.toolSetContentBar];
    [self.pdfViewCtrl addSubview:[self.more getContentView]];

    self.settingBar = [[SettingBar alloc] initWithUIExtensionsManager:self];
    self.settingBar.delegate = self;
    [self updateSettingBarContainer];

    [self setHiddenSettingBar:YES animated:NO];
    [self setHiddenEditDoneBar:YES animated:NO];
    [self setHiddenEditBar:YES animated:NO];
    [self setHiddenToolSetBar:YES animated:NO];
    [self setHiddenMoreMenu:YES animated:NO];
    
    //Install constraint to topbar/bottombar.
    UIToolbar* topToolbar = self.topToolbar;
    UIToolbar* bottomToolbar = self.bottomToolbar;
    [self.topToolbar setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSMutableArray *contraints = @[].mutableCopy;
    [contraints addObject:[topToolbar.heightAnchor constraintEqualToConstant:44]];
    if (@available(iOS 11.0, *)) {
        [contraints addObject:[topToolbar.leftAnchor constraintEqualToSystemSpacingAfterAnchor:self.pdfViewCtrl.safeAreaLayoutGuide.leftAnchor multiplier:1.0f]];
        [contraints addObject:[topToolbar.rightAnchor constraintEqualToSystemSpacingAfterAnchor:self.pdfViewCtrl.safeAreaLayoutGuide.rightAnchor multiplier:1.0f]];
        [contraints addObject:[topToolbar.topAnchor constraintEqualToSystemSpacingBelowAnchor:self.pdfViewCtrl.safeAreaLayoutGuide.topAnchor multiplier:1.0f]];
    } else {
        [contraints addObject:[topToolbar.leftAnchor constraintEqualToAnchor:self.pdfViewCtrl.leftAnchor]];
        [contraints addObject:[topToolbar.rightAnchor constraintEqualToAnchor:self.pdfViewCtrl.rightAnchor]];
        [contraints addObject:[topToolbar.topAnchor constraintLessThanOrEqualToAnchor:self.pdfViewCtrl.topAnchor constant:[Utility GetStatusBarHeight]]];
    }
    self.topToolbar.delegate = self;
    self.topToolbarVerticalConstraints = contraints;
    [self.pdfViewCtrl addConstraints:self.topToolbarVerticalConstraints];
    [self.bottomToolbar setTranslatesAutoresizingMaskIntoConstraints:NO];
    contraints = @[].mutableCopy;
    [contraints addObject:[bottomToolbar.heightAnchor constraintEqualToConstant:49]];
    if (@available(iOS 11.0, *)) {
        [contraints addObject:[bottomToolbar.leftAnchor constraintEqualToSystemSpacingAfterAnchor:self.pdfViewCtrl.safeAreaLayoutGuide.leftAnchor multiplier:1.0f]];
        [contraints addObject:[bottomToolbar.rightAnchor constraintEqualToSystemSpacingAfterAnchor:self.pdfViewCtrl.safeAreaLayoutGuide.rightAnchor multiplier:1.0f]];
        [contraints addObject:[bottomToolbar.bottomAnchor constraintEqualToSystemSpacingBelowAnchor:self.pdfViewCtrl.safeAreaLayoutGuide.bottomAnchor multiplier:1.0f]];
    } else {
        [contraints addObject:[bottomToolbar.leftAnchor constraintEqualToAnchor:self.pdfViewCtrl.leftAnchor]];
        [contraints addObject:[bottomToolbar.rightAnchor constraintEqualToAnchor:self.pdfViewCtrl.rightAnchor]];
        [contraints addObject:[bottomToolbar.bottomAnchor constraintEqualToAnchor:self.pdfViewCtrl.bottomAnchor]];
    }
    self.bottomToolbarVerticalConstraints = contraints;
    [self.pdfViewCtrl addConstraints:self.bottomToolbarVerticalConstraints];
    
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (void)updateSettingBarContainer {
    
    if (self.settingBarContainer) {
        [self.settingBarContainer removeFromSuperview];
    }
    
    CGFloat height = CGRectGetHeight(self.settingBar.contentView.frame);
    if (DEVICE_iPHONE) {
        CGRect screenFrame = _pdfViewCtrl.bounds;
        if (screenFrame.size.height < height || screenFrame.size.height - height < 100) {
            height = screenFrame.size.height - 100.f;
        }
    }
    
    self.settingBarContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.settingBar.contentView.frame), height)];
    self.settingBarContainer.backgroundColor = self.settingBar.contentView.backgroundColor;
    [self.settingBarContainer addSubview:self.settingBar.contentView];
    
    [self.settingBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && DEVICE_iPHONE) {
                make.left.equalTo(self.settingBarContainer.mas_safeAreaLayoutGuideLeft);
                make.right.equalTo(self.settingBarContainer.mas_safeAreaLayoutGuideRight);
                make.bottom.equalTo(self.settingBarContainer.mas_safeAreaLayoutGuideBottom);
                make.top.bottom.equalTo(self.settingBarContainer);
            }
            else{
                make.bottom.equalTo(self.settingBarContainer.mas_safeAreaLayoutGuideBottom);
                make.top.left.right.equalTo(self.settingBarContainer);
            }
        }else{
            make.left.top.right.bottom.equalTo(self.settingBarContainer);
        }
        make.height.mas_equalTo(height).priorityHigh();
    }];
    
    if (DEVICE_iPAD) {
        return;
    }
    
    [self.pdfViewCtrl addSubview:self.settingBarContainer];
    
    [self.settingBarContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.pdfViewCtrl);
    }];
}

- (void)buildItems {
    __weak typeof(self) weakSelf = self;

    // top toolbar
    self.backButton = [Utility createButtonWithImage:[UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    self.backButton.tag = FS_TOPBAR_ITEM_BACK_TAG;
    [self.backButton addTarget:self action:@selector(onClickBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.backButton.enabled = NO;

    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.backButton];
    backButtonItem.tag = FS_TOPBAR_ITEM_BACK_TAG;
    UIBarButtonItem *leftPadding = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    const CGFloat paddingWidth = 10.f;
    leftPadding.width = paddingWidth - [Utility getUIToolbarPaddingX];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    self.topToolbar.items = @[ leftPadding, backButtonItem, flexibleSpace ];
    

    // edit toolbar

    UIImage *commonBackBlue = [UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:commonBackBlue background:nil];
    doneItem.tag = 0;
    [self.editDoneBar addItem:doneItem displayPosition:Position_LT];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf setCurrentToolHandler:nil];
        if (weakSelf.currentAnnot) {
            [weakSelf setCurrentAnnot:nil];
        }
        if ([weakSelf getState] == STATE_ANNOTTOOL) {
            [weakSelf changeState:STATE_EDIT];
        }else{
            [weakSelf changeState:STATE_NORMAL];
        }
        [weakSelf.pdfViewCtrl endEditing:YES];
    };

    // bottom toolbar
    NSMutableArray<UIBarButtonItem *> *bottomBarItems = @[].mutableCopy;
    if (self.config.loadReadingBookmark || self.config.loadOutline || [self.config.tools fs_containsAnyObjectNotInArray:@[ Tool_Select, Tool_Eraser ]] || self.config.loadAttachment) {
        UIButton *panelButton = [self createButtonWithTitle:FSLocalizedForKey(@"kReadList") image:[UIImage imageNamed:@"read_panel" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        panelButton.contentEdgeInsets = UIEdgeInsetsMake((CGRectGetHeight(self.bottomToolbar.frame) - CGRectGetHeight(panelButton.frame)) / 2, 0, 0, 0);
        panelButton.tag = FS_BOTTOMBAR_ITEM_PANEL_TAG;
        [panelButton addTarget:self action:@selector(onClickBottomBarButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:panelButton];
        item.tag = FS_BOTTOMBAR_ITEM_PANEL_TAG;
        [bottomBarItems addObject:item];
    }

    UIButton *readModeButton = [self createButtonWithTitle:FSLocalizedForKey(@"kReadView") image:[UIImage imageNamed:@"read_mode" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    readModeButton.contentEdgeInsets = UIEdgeInsetsMake((CGRectGetHeight(self.bottomToolbar.frame) - CGRectGetHeight(readModeButton.frame)) / 2, 0, 0, 0);
    readModeButton.tag = FS_BOTTOMBAR_ITEM_READMODE_TAG;
    [readModeButton addTarget:self action:@selector(onClickBottomBarButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:readModeButton];
    item.tag = FS_BOTTOMBAR_ITEM_READMODE_TAG;
    [bottomBarItems addObject:item];

    if ([self.config.tools fs_containsAnyObjectNotInArray:@[ Tool_Select, Tool_Signature ]] || self.config.loadAttachment) {
        self.annotButton = [self createButtonWithTitle:FSLocalizedForKey(@"kReadComment") image:[UIImage imageNamed:@"read_annot" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        self.annotButton.contentEdgeInsets = UIEdgeInsetsMake((CGRectGetHeight(self.bottomToolbar.frame) - CGRectGetHeight(self.annotButton.frame)) / 2, 0, 0, 0);
        self.annotButton.tag = FS_BOTTOMBAR_ITEM_ANNOT_TAG;
        [self.annotButton addTarget:self action:@selector(onClickBottomBarButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.annotButton];
        item.tag = FS_BOTTOMBAR_ITEM_ANNOT_TAG;
        [bottomBarItems addObject:item];
    }

    NSMutableArray<UIBarButtonItem *> *tmpArray = @[].mutableCopy;
    for (UIBarButtonItem *item in bottomBarItems) {
        [tmpArray addObject:flexibleSpace];
        [tmpArray addObject:item];
    }
    [tmpArray addObject:flexibleSpace];
    bottomBarItems = tmpArray;
    self.bottomToolbar.items = bottomBarItems;
}

- (void)setViewsConstraint{
    [self.editDoneBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.equalTo(self.editDoneBar.contentView.superview);
    }];
    [self.toolSetBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.equalTo(self.toolSetBar.contentView.superview);
    }];
}

- (UIButton *)createButtonWithTitle:(NSString *)title image:(UIImage *)image {
    UIFont *textFont = [UIFont systemFontOfSize:9.f];
    CGSize titleSize = [Utility getTextSize:title fontSize:textFont.pointSize maxSize:CGSizeMake(400, 100)];
    float width = image.size.width;
    float height = image.size.height;
    CGRect frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width : width, titleSize.height + height);

    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [button setEnlargedEdge:ENLARGE_EDGE];

    [button setTitle:title forState:UIControlStateNormal];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height, 0);
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = textFont;
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];

    [button setImage:image forState:UIControlStateNormal];
    UIImage *translucentImage = [Utility imageByApplyingAlpha:image alpha:0.5];
    [button setImage:translucentImage forState:UIControlStateHighlighted];
    [button setImage:translucentImage forState:UIControlStateDisabled];
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);

    return button;
}

- (void)saveAndCloseCurrentDoc:(void (^_Nullable)(BOOL success))completion {
    NSString *filePath = self.pdfViewCtrl.filePath;
    
    if (self.preventOverrideFilePath && self.preventOverrideFilePath.length > 0 && ![filePath isEqualToString:self.preventOverrideFilePath]) {
        BOOL isOK = YES;
        isOK = [self.pdfViewCtrl saveDoc:self.preventOverrideFilePath flag:self.docSaveFlag];
        
        [self.pdfViewCtrl closeDoc:^() {
            if (completion) {
                completion(isOK);
            }
        }];
        
        return;
    }
    
    NSString *intermediateFilePath = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isAsync = ![fileManager fileExistsAtPath:filePath];
    if (filePath) {
        if (!isAsync) {
            NSString *tempDir = NSTemporaryDirectory();
            intermediateFilePath = [tempDir stringByAppendingPathComponent:[filePath lastPathComponent]];
            [self.pdfViewCtrl saveDoc:intermediateFilePath flag:self.docSaveFlag];
        } else {
            NSString *fileName = [filePath lastPathComponent];
            NSString *path = [DOCUMENT_PATH stringByAppendingPathComponent:fileName];
            if ([fileManager fileExistsAtPath:path]) {
                [fileManager removeItemAtPath:path error:nil];
            }
            NSLog(@"saving async doc to %@...", path);
            // todo wei, show alert view with cancel button
            [self.pdfViewCtrl saveDoc:path flag:self.docSaveFlag];
        }
    }
    filePath = self.pdfViewCtrl.filePath;
    [self.pdfViewCtrl closeDoc:^() {
        BOOL isOK = YES;
        if (filePath && !isAsync) {
            NSError *error = nil;
            if ([fileManager fileExistsAtPath:intermediateFilePath]) {
                [fileManager removeItemAtPath:filePath error:nil];
                isOK = [fileManager moveItemAtPath:intermediateFilePath toPath:filePath error:&error];
            }
        }
        if (completion) {
            completion(isOK);
        }
    }];
}

- (void)saveAndCloseCurrentDoc:(NSString *)filePath completion:(void (^_Nullable)(BOOL success))completion {
    
    NSString *intermediateFilePath = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isExists = [fileManager fileExistsAtPath:filePath];
    if (filePath) {
        if (isExists) {
            NSString *tempDir = NSTemporaryDirectory();
            intermediateFilePath = [tempDir stringByAppendingPathComponent:[filePath lastPathComponent]];
            [self.pdfViewCtrl saveDoc:intermediateFilePath flag:self.docSaveFlag];
        } else {
            [self.pdfViewCtrl saveDoc:filePath flag:self.docSaveFlag];
        }
    }
    
    [self.pdfViewCtrl closeDoc:^() {
        BOOL isOK = YES;
        if (filePath && isExists) {
            NSError *error = nil;
            if ([fileManager fileExistsAtPath:intermediateFilePath]) {
                [fileManager removeItemAtPath:filePath error:nil];
                isOK = [fileManager moveItemAtPath:intermediateFilePath toPath:filePath error:&error];
            }
        }
        if (completion) {
            completion(isOK);
        }
    }];
}

- (void)buttonSaveClick {
    [self.popover dismiss];
    self.popover = nil;
    self.isFileEdited = YES;
    
    [self saveAndCloseCurrentDoc:^(BOOL success) {
        if (self.goBack)
            self.goBack();
    }];
}

- (void)buttonSaveAsClick {
    [self.popover dismiss];
    self.popover = nil;
    self.isFileEdited = YES;
    
    [self documentSaveAS:nil error:nil];
}

-(void)documentSaveAS:(void (^_Nullable)(void))successed error:(void (^_Nullable)(void))error {
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Select;
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:^{
            if (destinationFolder.count == 0)
                return;
            
            __block void (^inputFileName)(NSString *path) = ^(NSString *path) {
                BOOL isDir = NO;
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if (![fileManager fileExistsAtPath:path isDirectory:&isDir]) {
                    return;
                }
                
                
                if (!isDir) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                                     inputFileName([path stringByDeletingLastPathComponent]);
                                                                                 });
                                                                             }];
                        [alertController addAction:cancelAction];
                        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    });
                    return;
                }
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kInputNewFileName") preferredStyle:UIAlertControllerStyleAlert];
                [alertController addTextFieldWithConfigurationHandler:^(UITextField *_Nonnull textField) {
                    textField.text = @"";
                }];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    UITextField *textField = alertController.textFields[0];
                    NSString *newName = textField.text;
                    
                    NSString *newFilePath = [path stringByAppendingString:[NSString stringWithFormat:@"/%@.%@", newName,[self->_pdfViewCtrl.filePath pathExtension]]];
                    NSString *oldFilePath = self->_pdfViewCtrl.filePath;
                                             
                    if (newName.length < 1) {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kInputNewFileName") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                inputFileName(path);
                            });
                        }];
                        
                        [alertController addAction:okAction];
                        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                        
                    }
                    else if ([fileManager fileExistsAtPath:newFilePath]) {
                        if ([newFilePath isEqualToString:oldFilePath]) {
                            [self saveAndCloseCurrentDoc:newFilePath completion:^(BOOL success) {
                                if (self.goBack)
                                    self.goBack();
                                if (successed != nil) {
                                    successed();
                                }
                            }];
                            
                            inputFileName = nil;
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                                       style:UIAlertActionStyleCancel
                                                                                     handler:^(UIAlertAction *action) {
                                                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                                                             inputFileName(path);
                                                                                         });
                                                                                     }];
                                [alertController addAction:cancelAction];
                                
                                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kReplace")
                                                                                 style:UIAlertActionStyleDefault
                                                                               handler:^(UIAlertAction *action) {
                                                                                   [fileManager removeItemAtPath:newFilePath error:nil];
                                                                                   [self saveAndCloseCurrentDoc:newFilePath completion:^(BOOL success) {
                                                                                       if (self.goBack)
                                                                                           self.goBack();
                                                                                       if (successed != nil) {
                                                                                           successed();
                                                                                       }
                                                                                   }];
                                                                                   
                                                                                   inputFileName = nil;
                                                                               }];
                                [alertController addAction:action];
                                [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                            });
                        }
                    }
                    else {
                        [self saveAndCloseCurrentDoc:newFilePath completion:^(BOOL success) {
                            if (self.goBack)
                                self.goBack();
                            if (successed != nil) {
                                successed();
                            }
                        }];
                        
                        inputFileName = nil;
                    }
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:action];
                [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            };
            
            inputFileName(destinationFolder[0]);
        }];
        
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
        if (error) {
            error();
        }
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    [selectDestinationNavController fs_setCustomTransitionAnimator:self.presentedVCAnimator];
    UIViewController *rootViewController = self.pdfViewCtrl.fs_viewController;
    [rootViewController presentViewController:selectDestinationNavController animated:YES completion:nil];
}

- (void)buttonDiscardChangeClick {
    [self.popover dismiss];
    self.popover = nil;
    self.isFileEdited = NO;
    [self.pdfViewCtrl closeDoc:nil];
    if (self.goBack)
        self.goBack();
}

- (void)setHiddenToolSetBar:(BOOL)hiddenToolSetBar {
    [self setHiddenToolSetBar:hiddenToolSetBar animated:YES];
}

- (void)setHiddenToolSetBar:(BOOL)hiddenToolSetBar animated:(BOOL)animated {
    _hiddenToolSetBar = hiddenToolSetBar;
    if (hiddenToolSetBar) {
         [self.toolSetContentBar mas_remakeConstraints:^(MASConstraintMaker *make) {
             make.left.right.equalTo(self.pdfViewCtrl);
             make.top.equalTo(self.pdfViewCtrl.mas_bottom);
        }];
    } else {
        [self.toolSetContentBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.pdfViewCtrl);
        }];
    }
    if (animated) {
         [UIView animateWithDuration:0.3
                          animations:^{
             [self.pdfViewCtrl layoutIfNeeded];
         }];
     }
}

- (void)setHiddenEditBar:(BOOL)hiddenEditBar {
    [self setHiddenEditBar:hiddenEditBar animated:YES];
}

- (void)setHiddenEditBar:(BOOL)hiddenEditBar animated:(BOOL)animated {
    if (_hiddenEditBar == hiddenEditBar) {
        return;
    }
    _hiddenEditBar = hiddenEditBar;
    if (hiddenEditBar) {
        [self.editBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.pdfViewCtrl);
            make.top.equalTo(self.pdfViewCtrl.mas_bottom);
        }];
    } else {
        [self.editBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.pdfViewCtrl);
        }];
    }
    if (animated) {
        [UIView animateWithDuration:0.3
                         animations:^{
            [self.pdfViewCtrl layoutIfNeeded];
        }];
    }

}

- (void)setHiddenEditDoneBar:(BOOL)hiddenEditDoneBar {
    [self setHiddenEditDoneBar:hiddenEditDoneBar animated:YES];
}

- (void)setHiddenEditDoneBar:(BOOL)hiddenEditDoneBar animated:(BOOL)animated {
    if (_hiddenEditDoneBar == hiddenEditDoneBar) {
        return;
    }
    _hiddenEditDoneBar = hiddenEditDoneBar;
    if (hiddenEditDoneBar) {
        [self.editDoneContentBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.pdfViewCtrl);
            make.bottom.equalTo(self.pdfViewCtrl.mas_top);
        }];

    } else {
        [self.editDoneContentBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self.pdfViewCtrl);
        }];
    }
    
    if (animated) {
        [UIView animateWithDuration:0.3
                         animations:^{
            [self.pdfViewCtrl layoutIfNeeded];
        }];
    }
}

- (void)setHiddenBottomToolbar:(BOOL)isHiddenBottomToolbar {
    if (!self.bottomToolbar) {
        _hiddenBottomToolbar = YES;
        return;
    }
    if (_hiddenBottomToolbar == isHiddenBottomToolbar) {
        return;
    }
    _hiddenBottomToolbar = isHiddenBottomToolbar;
    if(self.bottomToolbarVerticalConstraints)
        [self.pdfViewCtrl removeConstraints:self.bottomToolbarVerticalConstraints];
    if (_hiddenBottomToolbar) {
        [self.bottomToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(@49);
            make.left.equalTo(self.pdfViewCtrl.mas_left).offset(0);
            make.right.equalTo(self.pdfViewCtrl.mas_right).offset(0);
            make.top.equalTo(self.pdfViewCtrl.mas_bottom).offset(0);
        }];

    } else {
        [self.bottomToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(@49);
            make.left.equalTo(self.pdfViewCtrl.mas_left).offset(0);
            make.right.equalTo(self.pdfViewCtrl.mas_right).offset(0);
            if (@available(iOS 11.0, *)) {
                make.bottom.mas_equalTo(self.pdfViewCtrl.mas_safeAreaLayoutGuideBottom);
            } else {
                make.bottom.equalTo(self.pdfViewCtrl.mas_bottom).offset(0);
            }
        }];
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.pdfViewCtrl layoutIfNeeded];
    }];
    if ([self.delegate respondsToSelector:@selector(uiextensionsManager:onToolBar:hidden:)]) {
        [self.delegate uiextensionsManager:self onToolBar:FSBottomBar hidden:isHiddenBottomToolbar];
    }
}

- (void)setHiddenTopToolbar:(BOOL)hiddenTopToolbar {
    if (!self.topToolbar) {
        _hiddenTopToolbar = YES;
        return;
    }
    if (_hiddenTopToolbar == hiddenTopToolbar) {
        return;
    }
    _hiddenTopToolbar = hiddenTopToolbar;
    if(self.topToolbarVerticalConstraints)
        [self.pdfViewCtrl removeConstraints:self.topToolbarVerticalConstraints];
    if (hiddenTopToolbar) {
        [self.topToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(@44);
            make.left.equalTo(self.pdfViewCtrl.mas_left).offset(0);
            make.right.equalTo(self.pdfViewCtrl.mas_right).offset(0);
            make.bottom.equalTo(self.pdfViewCtrl.mas_top).offset(0);
        }];
    } else {
        [self.topToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(@44);
            make.left.equalTo(self.pdfViewCtrl.mas_left).offset(0);
            make.right.equalTo(self.pdfViewCtrl.mas_right).offset(0);
            if (@available(iOS 11.0, *)) {
                make.top.equalTo(self.pdfViewCtrl.mas_safeAreaLayoutGuideTop).offset(0);
            } else {
                make.top.equalTo(self.pdfViewCtrl.mas_top).offset([Utility GetStatusBarHeight]);
            }
        }];
    }
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.pdfViewCtrl layoutIfNeeded];
                     }];
    if ([self.delegate respondsToSelector:@selector(uiextensionsManager:onToolBar:hidden:)]) {
        [self.delegate uiextensionsManager:self onToolBar:FSTopBar hidden:hiddenTopToolbar];
    }
}

- (void)setHiddenMoreMenu:(BOOL)hiddenMoreMenu {
    [self setHiddenMoreMenu:hiddenMoreMenu animated:YES];
}

- (void)setHiddenMoreMenu:(BOOL)hiddenMoreMenu animated:(BOOL)animated {
    if (_hiddenMoreMenu == hiddenMoreMenu) {
        return;
    }
    _hiddenMoreMenu = hiddenMoreMenu;
    if (hiddenMoreMenu) {
        if (animated) {
            [UIView animateWithDuration:0.4
                animations:^{
                    self->maskView.alpha = 0.1f;
                }
                completion:^(BOOL finished) {
                    [self->maskView removeFromSuperview];
                }];
        } else {
            maskView.alpha = 0.1f;
            [maskView removeFromSuperview];
        }

        CGRect newFrame = [self.more getContentView].frame;

        newFrame.origin.x = _pdfViewCtrl.bounds.size.width;

        void (^hideMenu)(void) = ^{
            [self.more getContentView].frame = newFrame;
            [[self.more getContentView] mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.pdfViewCtrl.mas_top).offset(0);
                make.bottom.equalTo(self.pdfViewCtrl.mas_bottom).offset(0);
                if (DEVICE_iPHONE) {
                    make.left.equalTo(self.pdfViewCtrl.mas_right).offset(0);
                    make.width.mas_equalTo(newFrame.origin.x);
                } else {
                    make.left.equalTo(self.pdfViewCtrl.mas_right).offset(0);
                    make.width.mas_equalTo(300);
                }
            }];
        };
        if (animated) {
            [UIView animateWithDuration:0.4
                             animations:hideMenu];
        } else {
            hideMenu();
        }
    } else {
        self.currentToolHandler = nil;
        self.currentAnnot = nil;

        maskView.frame = _pdfViewCtrl.bounds;
        maskView.backgroundColor = [UIColor blackColor];
        maskView.alpha = 0.3f;
        maskView.tag = 201;
        [maskView addTarget:self action:@selector(dissmiss:) forControlEvents:UIControlEventTouchUpInside];

        [self.pdfViewCtrl bringSubviewToFront:[self.more getContentView]];
        [self.pdfViewCtrl insertSubview:maskView belowSubview:[self.more getContentView]];
        [maskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->maskView.superview.mas_left).offset(0);
            make.right.equalTo(self->maskView.superview.mas_right).offset(0);
            make.top.equalTo(self->maskView.superview.mas_top).offset(0);
            make.bottom.equalTo(self->maskView.superview.mas_bottom).offset(0);
        }];

        CGRect newFrame = [self.more getContentView].frame;

        if (DEVICE_iPHONE) {
            newFrame.origin.x = 0;
        } else {
            newFrame.origin.x = _pdfViewCtrl.bounds.size.width - [self.more getContentView].frame.size.width;
        }

        void (^showMenu)(void) = ^{
            [self.more getContentView].frame = newFrame;
            [[self.more getContentView] mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.pdfViewCtrl.mas_top).offset(0);
                make.bottom.equalTo(self.pdfViewCtrl.mas_bottom).offset(0);
                if (DEVICE_iPHONE) {
                    make.left.equalTo(self.pdfViewCtrl.mas_left).offset(0);
                    make.right.equalTo(self.pdfViewCtrl.mas_right).offset(0);
                } else {
                    make.width.mas_equalTo(300);
                    make.right.equalTo(self.pdfViewCtrl.mas_right).offset(0);
                }
            }];
        };
        if (animated) {
            [UIView animateWithDuration:0.4
                             animations:showMenu];
        } else {
            showMenu();
        }
    }
}

- (void)setHiddenSettingBar:(BOOL)hiddenSettingBar {
    if ([self canAutoHideTopBottomToolBar]) {
        if (hiddenSettingBar) {
            [self delayHideTopBottomToolBar];
        }else{
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoFullScreen) object:nil];
        }
    }
    [self setHiddenSettingBar:hiddenSettingBar animated:YES];
}
- (void)setHiddenSettingBar:(BOOL)hiddenSettingBar animated:(BOOL)animated {
    if (_hiddenSettingBar == hiddenSettingBar) {
        return;
    }
    
    [self.settingBar updateLayoutButtonsWithLayout:[self.pdfViewCtrl getPageLayoutMode]];
    
    if ([_pdfViewCtrl isDynamicXFA]) {
        ((UIButton *) [self.settingBar getItemView:REFLOW]).enabled = NO;
        ((UIButton *) [self.settingBar getItemView:PANZOOM]).enabled = NO;
        ((UIButton *) [self.settingBar getItemView:CROPPAGE]).enabled = NO;
    }
    
    _hiddenSettingBar = hiddenSettingBar;
    if (hiddenSettingBar) {
        if (DEVICE_iPHONE){
            if (animated) {
                [UIView animateWithDuration:0.4
                                 animations:^{
                                     self->_settingBarMaskView.alpha = 0.1f;
                                 }
                                 completion:^(BOOL finished) {
                                     [self->_settingBarMaskView removeFromSuperview];
                                 }];
            } else {
                _settingBarMaskView.alpha = 0.1f;
                [_settingBarMaskView removeFromSuperview];
            }
            
            CGRect newFrame = self.settingBarContainer.frame;
            newFrame.origin.y = _pdfViewCtrl.bounds.size.height;
            void (^hideBar)(void) = ^{
                self.settingBarContainer.frame = newFrame;
                [self.settingBarContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self->_pdfViewCtrl.mas_left).offset(0);
                    make.right.equalTo(self->_pdfViewCtrl.mas_right).offset(0);
                    make.top.equalTo(self->_pdfViewCtrl.mas_bottom).offset(0);
                    //                make.height.mas_equalTo(newFrame.size.height);
                }];
            };
            if (animated) {
                [UIView animateWithDuration:0.4
                                 animations:hideBar];
            } else {
                hideBar();
            }
        }else{
            [self.settingbarPopoverCtr dismissViewControllerAnimated:true completion:nil];
        }

    } else {
        if (DEVICE_iPHONE){
            if (!_settingBarMaskView) {
                _settingBarMaskView = [[UIControl alloc] initWithFrame:_pdfViewCtrl.bounds];
                _settingBarMaskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
                _settingBarMaskView.backgroundColor = [UIColor blackColor];
                _settingBarMaskView.alpha = 0.3f;
                _settingBarMaskView.tag = 203;
                [_settingBarMaskView addTarget:self action:@selector(hideSettingBar) forControlEvents:UIControlEventTouchUpInside];
            } else {
                _settingBarMaskView.frame = _pdfViewCtrl.bounds;
            }
            [_pdfViewCtrl bringSubviewToFront:self.settingBarContainer];
            [_pdfViewCtrl insertSubview:_settingBarMaskView belowSubview:self.settingBarContainer];
            [_settingBarMaskView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self->_pdfViewCtrl.mas_left).offset(0);
                make.right.equalTo(self->_pdfViewCtrl.mas_right).offset(0);
                make.top.equalTo(self->_pdfViewCtrl.mas_top).offset(0);
                make.bottom.equalTo(self->_pdfViewCtrl.mas_bottom).offset(0);
            }];
            
            [self.settingBar updateBtnLayout];
            CGRect frame = self.settingBarContainer.frame;
            frame.origin.y -= self.settingBarContainer.frame.size.height;
            void (^showBar)(void) = ^{
                self.settingBarContainer.frame = frame;
                [self.settingBarContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self->_pdfViewCtrl.mas_left).offset(0);
                    make.right.equalTo(self->_pdfViewCtrl.mas_right).offset(0);
                    make.bottom.equalTo(self->_pdfViewCtrl.mas_bottom).offset(0);
                    //                make.height.mas_equalTo(frame.size.height);
                }];
            };
            if (animated) {
                [UIView animateWithDuration:0.4
                                 animations:showBar];
            } else {
                showBar();
            }
        }else{
            [self.settingBar updateBtnLayout];
            if (CGRectEqualToRect(CGRectZero, self.settingBarContainer.bounds)) {
                return;
            }
            UIView *fromView = [self.bottomToolbar viewWithTag:FS_BOTTOMBAR_ITEM_READMODE_TAG];
            CGRect rect = [_pdfViewCtrl convertRect:fromView.bounds fromView:fromView];
            self.settingbarPopoverCtr.view = self.settingBarContainer;
            self.settingbarPopoverCtr.view.backgroundColor = ThemeCellBackgroundColor;
            self.settingbarPopoverCtr.preferredContentSize = self.settingBarContainer.bounds.size;
            self.settingbarPopoverCtr.modalPresentationStyle = UIModalPresentationPopover;
            self.settingbarPopoverCtr.popoverPresentationController.delegate = self;
            self.settingbarPopoverCtr.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
            self.settingbarPopoverCtr.popoverPresentationController.sourceView = _pdfViewCtrl;
            self.settingbarPopoverCtr.popoverPresentationController.sourceRect = rect;
            [self.pdfViewCtrl.fs_viewController presentViewController:self.settingbarPopoverCtr animated:true completion:nil];
        }

    }
}

- (UIViewController *)settingbarPopoverCtr {
    if (!_settingbarPopoverCtr) {
        _settingbarPopoverCtr = [[UIViewController alloc] init];
    }
    return _settingbarPopoverCtr;
}

- (void)hideSettingBar {
    self.hiddenSettingBar = YES;
}

- (BOOL)hiddenPanel {
    return self.panelController.isHidden;
}

- (void)setHiddenPanel:(BOOL)hiddenPanel {
    self.panelController.isHidden = hiddenPanel;
}

- (BOOL)canAutoHideTopBottomToolBar{
    if (!self.enableTopToolbar && self.enableBottomToolbar) {
        return self.isFullScreen && !self.hiddenBottomToolbar;
    }
    else if (self.enableTopToolbar && !self.enableBottomToolbar) {
        return self.isFullScreen && !self.hiddenTopToolbar;
    }
    else if (!self.enableTopToolbar && !self.enableBottomToolbar) {
        return (self.isFullScreen && !self.hiddenTopToolbar) || (self.isFullScreen && !self.hiddenBottomToolbar);
    }
    return self.isFullScreen;
}

- (void)dissmiss:(id)sender {
    UIControl *control = (UIControl *) sender;
    if (control.tag == 200) {

    } else if (control.tag == 201) {
        self.hiddenMoreMenu = YES;
    }
}

#pragma -mark rewrite configuration
- (void)setEnableHighlightForm:(BOOL)enableHighlightForm{
    if (_enableHighlightForm == enableHighlightForm) return;
    _enableHighlightForm = enableHighlightForm;
    FormAnnotHandler *handler = (FormAnnotHandler *) [self getAnnotHandlerByType:FSAnnotWidget];
    if (!handler) return;
    [handler setEnableHighlightForm:enableHighlightForm];
}

- (void)setHighlightFormColor:(UIColor *)highlightFormColor{
    if (CGColorEqualToColor(_highlightFormColor.CGColor, highlightFormColor.CGColor)) return;
    _highlightFormColor = highlightFormColor;
    FormAnnotHandler *handler = (FormAnnotHandler *) [self getAnnotHandlerByType:FSAnnotWidget];
    if (!handler) return;
    [handler setHighlightFormColor:highlightFormColor];
}

#pragma mark <ILayoutEventListener>

- (void)onLayoutModeChanged:(PDF_LAYOUT_MODE)oldLayoutMode newLayoutMode:(PDF_LAYOUT_MODE)newLayoutMode {
    if(oldLayoutMode == PDF_LAYOUT_MODE_UNKNOWN) return;
    [self changeState:STATE_NORMAL];
}

#pragma mark - handle fullScreen event

- (void)setIsFullScreen:(BOOL)isFullScreen {
    if (_isFullScreen == isFullScreen) {
        return;
    }
    _isFullScreen = isFullScreen;
    [self setFullScreen:isFullScreen];
}

- (void)setFullScreen:(BOOL)fullScreen{
    if (!self.isFullScreen) {
        _currentIsFullScreen = NO;
        return ;
    }
    _currentIsFullScreen = fullScreen;
    if (self.currentState == STATE_NORMAL || self.currentState == STATE_PANZOOM) {
        if (fullScreen) {
            self.hiddenTopToolbar = YES;
            self.hiddenBottomToolbar = YES;
        } else {
            self.hiddenTopToolbar = NO;
            self.hiddenBottomToolbar = NO;
            if ([self canAutoHideTopBottomToolBar]) {
                [self delayHideTopBottomToolBar];
            }
        }
    }
    for (id<IFullScreenListener> listener in self.fullScreenListeners) {
        if ([listener respondsToSelector:@selector(onFullScreen:)]) {
            [listener onFullScreen:fullScreen];
        }
    }
}

- (void)suspendAutoFullScreen {
    _isSuspendAutoFullScreen = YES;
    if([self canAutoHideTopBottomToolBar]) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoFullScreen) object:nil];
    }
}

- (void)resumeAutoFullScreen {
    _isSuspendAutoFullScreen = NO;
    if([self canAutoHideTopBottomToolBar]) {
        [self performSelector:@selector(autoFullScreen) withObject:nil afterDelay:5.f];
    }
}

-(void)delayHideTopBottomToolBar {
    if (_isSuspendAutoFullScreen)  return;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoFullScreen) object:nil];
    [self performSelector:@selector(autoFullScreen) withObject:nil afterDelay:5.f];
}

- (void)setNeedScreenLock:(BOOL)needScreenLock{
    if (_needScreenLock == needScreenLock) {
        return;
    }
    self.settingBar.screenLockBtn.hidden = !needScreenLock;
    [self.settingBar updateBtnLayout];
}

- (void)autoFullScreen{
    [self setFullScreen:YES];
}

- (void)registerFullScreenListener:(id<IFullScreenListener>)listener {
    if (self.fullScreenListeners) {
        [self.fullScreenListeners addObject:listener];
    }
}

- (void)unregisterFullScreenListener:(id<IFullScreenListener>)listener {
    if ([self.fullScreenListeners containsObject:listener]) {
        [self.fullScreenListeners removeObject:listener];
    }
}

#pragma mark - IPageNumberListener
- (void)registerPageNumberListener:(id<IPageNumberListener>)listener {
    if (self.pageNumberListeners) {
        [self.pageNumberListeners addObject:listener];
    }
}

- (void)unregisterPageNumerListener:(id<IPageNumberListener>)listener {
    if ([self.pageNumberListeners containsObject:listener]) {
        [self.pageNumberListeners removeObject:listener];
    }
}

#pragma mark - IRotationEventListener

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    self.isRotating = YES;
    if (self.shouldShowMenu) {
        [self hideMenu];
    }
    
    self.hiddenSettingBar = YES;

    for (id<IRotationEventListener> listener in self.rotateListeners) {
        if ([listener respondsToSelector:@selector(willRotateToInterfaceOrientation:duration:)]) {
            [listener willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
        }
    }

    [self.pdfViewCtrl willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.pdfViewCtrl willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    self.isRotating = NO;
    if (self.shouldShowMenu) {
        [self hideMenu];
    }

    for (id<IRotationEventListener> listener in self.rotateListeners) {
        if ([listener respondsToSelector:@selector(didRotateFromInterfaceOrientation:)]) {
            [listener didRotateFromInterfaceOrientation:fromInterfaceOrientation];
        }
    }

    [self.pdfViewCtrl didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    if (DEVICE_iPHONE && [self.pdfViewCtrl getDoc]) {
        [self.annotationToolsBar refreshLayout];
    }
}

- (void)registerRotateChangedListener:(id<IRotationEventListener>)listener {
    if (self.rotateListeners && ![self.rotateListeners containsObject:listener]) {
        [self.rotateListeners addObject:listener];
    }
}

- (void)unregisterRotateChangedListener:(id<IRotationEventListener>)listener {
    if ([self.rotateListeners containsObject:listener]) {
        [self.rotateListeners removeObject:listener];
    }
}

- (void)registerStateChangeListener:(id<IStateChangeListener>)listener {
    if (self.stateChangeListeners) {
        [self.stateChangeListeners addObject:listener];
    }
}

- (void)unregisterStateChangeListener:(id<IStateChangeListener>)listener {
    if ([self.stateChangeListeners containsObject:listener]) {
        [self.stateChangeListeners removeObject:listener];
    }
}

- (void)changeState:(int)state {
    int oldState = self.currentState;
    NSArray *supportStates = @[@(STATE_NORMAL), @(STATE_REFLOW), @(STATE_SEARCH), @(STATE_EDIT), @(STATE_ANNOTTOOL), @(STATE_PAGENAVIGATE), @(STATE_SIGNATURE), @(STATE_PANZOOM),@(STATE_CREATEFORM), @(STATE_SPEECH), @(STATE_FILLSIGN)];
    if (![supportStates containsObject:@(state)]) {
        self.currentState = 0;
        return ;
    }
    if (state == STATE_PAGENAVIGATE && !self.config.loadPageNavigation) {
        return;
    }
    
    if (state == STATE_SEARCH && !self.config.loadSearch) {
        return;
    }
    if (state == STATE_NORMAL) {
        [self setCurrentToolHandler:nil];
        [self setCurrentAnnot:nil];
        [self setCurrentWidget:nil];
    }
    
    self.currentState = state;
    
    if (oldState == STATE_REFLOW && oldState != state && state != STATE_PAGENAVIGATE) {
        [self enterReflowMode:NO];
    }
    if (oldState == STATE_SEARCH && oldState != state) {
        [self showSearchBar:NO];
    }
    if (oldState == STATE_PAGENAVIGATE && oldState != state) {
        [self quitGotoMode];
    }

    self.hiddenTopToolbar = YES;
    self.hiddenBottomToolbar = YES;
    self.hiddenEditBar = YES;
    self.hiddenEditDoneBar = YES;
    self.hiddenToolSetBar = YES;
    self.hiddenMoreMenu = YES;
    self.hiddenSettingBar = YES;
    if (self.panZoomView) {
        [self cancelPanZoom];
    }
    switch (state) {
        case STATE_NORMAL:
            self.hiddenTopToolbar = NO;
            self.hiddenBottomToolbar = NO;
            break;
        case STATE_EDIT:
            self.hiddenEditBar = NO;
            self.hiddenEditDoneBar = NO;
            break;
        case STATE_ANNOTTOOL:
            self.hiddenToolSetBar = NO;
            self.hiddenEditDoneBar = NO;
            break;
        case STATE_SEARCH:
            if (self.currentAnnot) [self setCurrentAnnot:nil];
            [self showSearchBar:YES];
            break;
        case STATE_PANZOOM:
            if (!self.enableTopToolbar) {
                [self enableTopToolbar:YES];
                self.enableTopToolbar = NO;
            }
            self.hiddenTopToolbar = NO;
            self.hiddenBottomToolbar = NO;
            [self setPanAndZoomMode];
            self.hiddenSettingBar = YES;
            [self delayHideTopBottomToolBar];
            break;
            
        case STATE_CREATEFORM:
            self.hiddenToolSetBar = NO;
            self.hiddenEditDoneBar = NO;
            if (![self enterCreateFormState]) {
                [self changeState:oldState];
                return;
            }
            break;
            
        case STATE_SPEECH:
            [self.speechModule speakFromPageIndex:[self.pdfViewCtrl getCurrentPage]];
            break;
            
        default:
            break;
    }
    
    if (state != STATE_PAGENAVIGATE && state != STATE_NORMAL) {
        if ([_pdfViewCtrl isDynamicXFA]) {
            [self changeState:oldState];
            return;
        }
    }
    
    if (state == STATE_REFLOW ) {
        [self enterReflowMode:YES];
    }
    
    if (state == STATE_SIGNATURE) {
        SignatureModule *module = (SignatureModule *)[self getModuleByName:@"Signature"];
        if (!module) {
            [self changeState:oldState];
            return;
        }
        
        if (self.currentAnnot) {
            [self setCurrentAnnot:nil];
        }
        SignatureModule *signModule = (SignatureModule *)module;
        [self setCurrentToolHandler:signModule.toolHandler];
        [signModule.toolHandler openCreateSign];
    }
    if (state == STATE_FILLSIGN) {
        FillSignModule *module = (FillSignModule *)[self getModuleByName:Module_Fill_Sign];
        if (!module) {
            [self changeState:oldState];
            return;
        }
        for (UIBarButtonItem *item in self.bottomToolbar.items) {
            if (item.customView.tag == FS_BOTTOMBAR_ITEM_FILLSIGN_TAG) {
                if (!((UIButton *)item.customView).enabled) {
                    [self changeState:oldState];
                    return;
                }
                break;
            }
        }
        if (self.currentAnnot) {
            [self setCurrentAnnot:nil];
        }
        [self setCurrentToolHandler:module.fillSignToolHandler];
    }
    
    if (self.stateChangeListeners) {
        for (id<IStateChangeListener> listener in self.stateChangeListeners) {
            if ([listener respondsToSelector:@selector(onStateChanged:)]) {
                [listener onStateChanged:state];
            }
        }
    }
}

- (void)enableTopToolbar:(BOOL)isEnabled {
    _enableTopToolbar = isEnabled;
    if (isEnabled) {
        if (self.topToolbar)
            return;

        if (self.topToolbarSaved) {
            self.topToolbar = self.topToolbarSaved;
//            [self.pdfViewCtrl addSubview:self.topToolbar];
            [self.topToolbar setHidden:NO];
        }
    } else {
        if (!self.topToolbar)
            return;
//        [self.topToolbar removeFromSuperview];
        [self.topToolbar setHidden:YES];
        self.topToolbarSaved = self.topToolbar;
        self.topToolbar = nil;
    }
}

- (void)enableBottomToolbar:(BOOL)isEnabled {
    _enableBottomToolbar = isEnabled;
    if (isEnabled) {
        if (self.bottomToolbar)
            return;

        if (self.bottomToolbarSaved) {
            self.bottomToolbar = self.bottomToolbarSaved;
            [self.pdfViewCtrl addSubview:self.bottomToolbar];
            [self.pdfViewCtrl addConstraints:self.bottomToolbarVerticalConstraints];
        }
    } else {
        if (!self.bottomToolbar)
            return;
        [self.pdfViewCtrl removeConstraints:self.bottomToolbarVerticalConstraints];
        [self.bottomToolbar removeFromSuperview];
        self.bottomToolbarSaved = self.bottomToolbar;
        self.bottomToolbar = nil;
    }
}

- (int)getState {
    return self.currentState;
}

#pragma mark - IDocEventListener

- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {

    if ([self canAutoHideTopBottomToolBar]) {
        [self setFullScreen:NO];
        [self delayHideTopBottomToolBar];
    }
    
    self.backButton.enabled = YES;
    if (document) {
        self.annotButton.enabled = [Utility canAddAnnot:_pdfViewCtrl];
    }
    
    if (![Utility canAssemble:_pdfViewCtrl]) {
        ((UIButton *) [self.settingBar getItemView:CROPPAGE]).enabled = NO;
        [_topToolBarItemsArr enumerateObjectsUsingBlock:^(UIBarButtonItem  *_Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if (item.tag == FS_TOPBAR_ITEM_BOOKMARK_TAG){
                item.enabled = NO;
            }
        }];
    }else{
        ((UIButton *) [self.settingBar getItemView:CROPPAGE]).enabled = YES;
        [_topToolBarItemsArr enumerateObjectsUsingBlock:^(UIBarButtonItem  *_Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if (item.tag == FS_TOPBAR_ITEM_BOOKMARK_TAG){
                item.enabled = YES;
            }
        }];
    }
    
    if ([_pdfViewCtrl isDynamicXFA]) {
        ((UIButton *) [self.settingBar getItemView:REFLOW]).enabled = NO;
        ((UIButton *) [self.settingBar getItemView:PANZOOM]).enabled = NO;
        ((UIButton *) [self.settingBar getItemView:CROPPAGE]).enabled = NO;
        
        [_topToolBarItemsArr enumerateObjectsUsingBlock:^(UIBarButtonItem  *_Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if (item.tag == FS_TOPBAR_ITEM_SEARCH_TAG){
                item.enabled = NO;
            }
        }];
    }else{
        ((UIButton *) [self.settingBar getItemView:REFLOW]).enabled = YES;
        ((UIButton *) [self.settingBar getItemView:PANZOOM]).enabled = YES;
        ((UIButton *) [self.settingBar getItemView:CROPPAGE]).enabled = YES;
        
        [_topToolBarItemsArr enumerateObjectsUsingBlock:^(UIBarButtonItem  *_Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if (item.tag == FS_TOPBAR_ITEM_SEARCH_TAG){
                item.enabled = YES;
            }
        }];
    }
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    id<IToolHandler> toolHandler = [self getToolHandlerByName:Tool_Freetext];
    if (toolHandler && [toolHandler isKindOfClass:[FtToolHandler class]]) {
        FtToolHandler *ftToolhandler = (FtToolHandler *) toolHandler;
        [ftToolhandler exitWithoutSave];
    }
    if(_isDocModified){
      self.isDocModified = NO;
    }
    NSString *filePath = self.pdfViewCtrl.filePath;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self clearThumbnailCachesForPDFAtPath:filePath];
    });
    
    [[AudioPlayerControl sharedControl] dismiss];
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

#pragma mark - IToolEventListener

- (void)onToolChanged:(NSString *)lastToolName CurrentToolName:(NSString *)toolName {
    if (toolName == nil) {
        //Dismiss annotation type.
        FSPDFViewCtrl *pdfViewCtrl = self.pdfViewCtrl;
        for (UIView *view in pdfViewCtrl.subviews) {
            if (view.tag == 2113) {
                [view removeFromSuperview];
            }
        }
    }
    if (toolName && ![toolName isEqualToString:Tool_Select] && ![toolName isEqualToString:Tool_Signature]) {
        if ([self getToolHandlerByName:toolName] != nil) {
            [self changeState:STATE_ANNOTTOOL];
        }
    }
}

- (void)onCurrentAnnotChanged:(FSAnnot *)lastAnnot currentAnnot:(FSAnnot *)currentAnnot {
}

#pragma mark <ISearchEventListener>

- (void)onSearchStarted {
    for (id<ISearchEventListener> listener in self.searchListeners) {
        if ([listener respondsToSelector:@selector(onSearchStarted)]) {
            [listener onSearchStarted];
        }
    }
}

- (void)onSearchCanceled {
    [self changeState:STATE_NORMAL];
    for (id<ISearchEventListener> listener in self.searchListeners) {
        if ([listener respondsToSelector:@selector(onSearchCanceled)]) {
            [listener onSearchCanceled];
        }
    }
}

#pragma mark - IPageEventListener
- (void)onPageVisible:(int)index {
}

- (void)onPageInvisible:(int)index {
}

#pragma mark - top and bottom toolbar events

- (void)onClickBackButton:(UIButton *)button {
    if (self.currentAnnot) {
        [self setCurrentAnnot:nil];
    }
    
    if (self.currentWidget) {
        [self setCurrentWidget:nil];
    }
    
    if (self.currentToolHandler) {
        [self setCurrentToolHandler:nil];
    }
    
    if ([self.delegate respondsToSelector:@selector(quitUIExtensionsManager:button:)]) {
        [self.delegate quitUIExtensionsManager:self button:button];
        return;
    }
    
    BOOL isAsync = (self.pdfViewCtrl.filePath != nil) && ![[NSFileManager defaultManager] fileExistsAtPath:self.pdfViewCtrl.filePath];
    if (!isAsync && /* ![self.pdfViewCtrl.currentDoc isModified] &&  todo maybe by DocEventCallback */ !_isDocModified) {
        [self.pdfViewCtrl closeDoc:nil];
        if (self.goBack)
            self.goBack();
        return;
    }

    BOOL canSave = [self.pdfViewCtrl isDynamicXFA] ? !self.isViewSignedDocument && [self.pdfViewCtrl isDynamicXFA] : !self.isViewSignedDocument && [Utility canModifyContents:self.pdfViewCtrl];

    if (self.isAutoSaveDoc && canSave) {
        [self buttonSaveClick];
        return;
    }
    
    UIView *menu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, canSave? 150 :100)];
    menu.backgroundColor = UIColor_DarkMode(menu.backgroundColor, ThemePopViewColor_Dark);
    int startIndex = 0;
    if (canSave) {
        UIButton *save = [[UIButton alloc] initWithFrame:CGRectMake(10, 50 * startIndex + 15, 130, 20)];
        [save setTitle:FSLocalizedForKey(@"kSave") forState:UIControlStateNormal];
        [save setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
        [save setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
        save.titleLabel.font = [UIFont systemFontOfSize:15];
        [save addTarget:self action:@selector(buttonSaveClick) forControlEvents:UIControlEventTouchUpInside];
        [menu addSubview:save];
        startIndex += 1;
    }
    
    UIButton *saveasBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 15 + 50 * startIndex  , 130, 20)];
    [saveasBtn setTitle:FSLocalizedForKey(@"kSaveAS") forState:UIControlStateNormal];
    [saveasBtn setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [saveasBtn setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    saveasBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [saveasBtn addTarget:self action:@selector(buttonSaveAsClick) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:saveasBtn];
    startIndex += 1;
    
    UIButton *discard = [[UIButton alloc] initWithFrame:CGRectMake(10, 15 + 50 * startIndex, 130, 20)];
    [discard setTitle:FSLocalizedForKey(isAsync ? @"kDonotSave" : @"kDiscardChange") forState:UIControlStateNormal];
    [discard setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [discard setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    discard.titleLabel.font = [UIFont systemFontOfSize:15];
    [discard addTarget:self action:@selector(buttonDiscardChangeClick) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:discard];
    startIndex += 1;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, 50 * (startIndex - 2), 130, 1)];
    line.backgroundColor = ThemePopViewDividingLineColor;
    [menu addSubview:line];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(10, 50 * (startIndex - 1), 130, 1)];
    line2.backgroundColor = ThemePopViewDividingLineColor;
    [menu addSubview:line2];
    
    self.popover = [Popover popover];
    
    void (^block)(void) = ^{
        [self _popoverSideEdgeConfig];
        CGPoint point = CGPointMake(CGRectGetMidX(self.backButton.frame), CGRectGetMaxY(self.backButton.frame));
        CGPoint startPoint = [self.backButton convertPoint:point toView:self.pdfViewCtrl];
        [self.popover showAtPoint:startPoint popoverPosition:DXPopoverPositionDown withContentView:menu inView:self.pdfViewCtrl];
    };
    
     block();
    
    [self suspendAutoFullScreen];
    
    @weakify(self)
    self.popover.didDismissHandler = ^{
        @strongify(self)
        [self resumeAutoFullScreen];
    };
    self.popover.didRotatedHandler = ^{
        block();
    };
}

- (void) _popoverSideEdgeConfig{
    if (IS_IPHONE_OVER_TEN) {
        NSInteger orientation =  [UIDevice currentDevice].orientation;
        if (orientation == 1 || orientation == 4) {
            self.popover.sideEdge = 4.f;
        }else {
            self.popover.sideEdge = 44.f;
        }
    }
}

- (void)onClickBottomBarButton:(UIButton *)button {
    if (self.currentAnnot) {
        [self setCurrentAnnot:nil];
    }
    if ([self.currentToolHandler isKindOfClass:[MultipleSelectionToolHandler class]]) {
        [self setCurrentToolHandler:nil];
    }
    switch (button.tag) {
    case FS_BOTTOMBAR_ITEM_PANEL_TAG:
        self.panelController.isHidden = NO;
        break;
    case FS_BOTTOMBAR_ITEM_READMODE_TAG:
        self.hiddenSettingBar = NO;
        break;
    case FS_BOTTOMBAR_ITEM_ANNOT_TAG:
        [self changeState:STATE_EDIT];
        break;
    default:
        break;
    }
}

#pragma mark <FSUndo>

- (void)addUndoItem:(UndoItem *)undoItem {
    if (!undoItem)
        return;
    [self.undoItems addObject:undoItem];
    [self.redoItems removeAllObjects];
    for (id<IFSUndoEventListener> listener in self.undoListeners) {
        if ([listener respondsToSelector:@selector(onUndoChanged)]) {
            [listener onUndoChanged];
        }
    }
}

- (void)removeUndoItem:(UndoItem *)undoItem {
    if (!undoItem)
        return;
    void (^removeUndoItem) (NSMutableArray *items) = ^(NSMutableArray *items){
        if ([items containsObject:undoItem]) {
            [items removeObject:undoItem];
        }else{
            [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[NSMutableArray class]]) {
                    NSMutableArray *undoItems = obj;
                    if (!undoItems.count) {
                        [items removeObject:undoItems];
                         *stop = YES;
                    }
                    if ([undoItems containsObject:undoItem]) {
                        [undoItems removeObject:undoItem];
                        if (!undoItems.count) [items removeObject:undoItems];
                        *stop = YES;
                    }
                }
            }];
        }
    };
    removeUndoItem(self.undoItems);
    removeUndoItem(self.redoItems);
    for (id<IFSUndoEventListener> listener in self.undoListeners) {
        if ([listener respondsToSelector:@selector(onUndoChanged)]) {
            [listener onUndoChanged];
        }
    }
}

- (void)addUndoItems:(NSMutableArray *)undoItems {
    if (!undoItems || undoItems.count == 0)
        return;
    [self.undoItems addObject:undoItems];
    [self.redoItems removeAllObjects];
    for (id<IFSUndoEventListener> listener in self.undoListeners) {
        if ([listener respondsToSelector:@selector(onUndoChanged)]) {
            [listener onUndoChanged];
        }
    }
}

-(void)removeUndoItems {
    NSMutableArray *tempUndoArray = [NSMutableArray arrayWithArray: self.undoItems];
    for (int i = 0 ; i < tempUndoArray.count; i++) {
        id undoObj = [tempUndoArray objectAtIndex:i];
        if ([undoObj isKindOfClass:[NSMutableArray class]]) {
            [self.undoItems removeObject:undoObj];
        }
    }
    NSMutableArray *tempredoArray = [NSMutableArray arrayWithArray: self.redoItems];
    for (int i = 0 ; i < tempredoArray.count; i++) {
        id undoObj = [tempredoArray objectAtIndex:i];
        if ([undoObj isKindOfClass:[NSMutableArray class]]) {
            [self.redoItems removeObject:undoObj];
        }
    }

    for (id<IFSUndoEventListener> listener in self.undoListeners) {
        if ([listener respondsToSelector:@selector(onUndoChanged)]) {
            [listener onUndoChanged];
        }
    }
}

- (BOOL)canUndo {
    return self.undoItems.count > 0;
}

- (BOOL)canRedo {
    return self.redoItems.count > 0;
}

- (void)undo {
    if (self.currentAnnot) {
        [self setCurrentAnnot:nil];
    }
    if (self.undoItems.count == 0)
        return;
    @synchronized(self) {
        for (id<IFSUndoEventListener> listener in self.undoListeners) {
            if ([listener respondsToSelector:@selector(onWillUndo)]) {
                [listener onWillUndo];
            }
        }

        id undoObj = [self.undoItems objectAtIndex:self.undoItems.count - 1];
        
        if ([undoObj isKindOfClass:[NSMutableArray class]]) {
            NSMutableArray *undoItems = (NSMutableArray *)undoObj;
            for (int i = 0 ; i < undoItems.count; i++) {
                UndoItem *item = [undoItems objectAtIndex:i];
                if (item.undo) {
                    item.undo(item);
                }
            }
        }else{
            UndoItem *item = (UndoItem *)undoObj;
            if (item.undo) {
                item.undo(item);
            }
        }
        
        [self.redoItems addObject:undoObj];
        [self.undoItems removeObject:undoObj];
        
        for (id<IFSUndoEventListener> listener in self.undoListeners) {
            if ([listener respondsToSelector:@selector(onUndoChanged)]) {
                [listener onUndoChanged];
            }
        }
        for (id<IFSUndoEventListener> listener in self.undoListeners) {
            if ([listener respondsToSelector:@selector(onDidUndo)]) {
                [listener onDidUndo];
            }
        }
    }
}

- (void)redo {
    if (self.currentAnnot) {
        [self setCurrentAnnot:nil];
    }
    if (self.redoItems.count == 0)
        return;
    @synchronized(self) {
        for (id<IFSUndoEventListener> listener in self.undoListeners) {
            if ([listener respondsToSelector:@selector(onWillRedo)]) {
                [listener onWillRedo];
            }
        }

        id undoObj = [self.redoItems objectAtIndex:self.redoItems.count - 1];
        
        if ([undoObj isKindOfClass:[NSMutableArray class]]) {
            NSMutableArray *undoItems = (NSMutableArray *)undoObj;
            for (int i = 0 ; i < undoItems.count; i++) {
                UndoItem *item = [undoItems objectAtIndex:i];
                if (item.redo) {
                    item.redo(item);
                }
            }
        }else{
            UndoItem *item = (UndoItem *)undoObj;
            if (item.redo) {
                item.redo(item);
            }
        }
        
        [self.undoItems addObject:undoObj];
        [self.redoItems removeObject:undoObj];

        for (id<IFSUndoEventListener> listener in self.undoListeners) {
            if ([listener respondsToSelector:@selector(onUndoChanged)]) {
                [listener onUndoChanged];
            }
        }
        for (id<IFSUndoEventListener> listener in self.undoListeners) {
            if ([listener respondsToSelector:@selector(onDidRedo)]) {
                [listener onDidRedo];
            }
        }
    }
}

- (void)clearUndoRedo {
    [self.undoItems removeAllObjects];
    [self.redoItems removeAllObjects];
    for (id<IFSUndoEventListener> listener in self.undoListeners) {
        if ([listener respondsToSelector:@selector(onUndoChanged)]) {
            [listener onUndoChanged];
        }
    }
}

#pragma mark print methods

+ (void)printDoc:(FSPDFDoc *)doc animated:(BOOL)animated jobName:(nullable NSString *)jobName delegate:(nullable id<UIPrintInteractionControllerDelegate>)delegate completionHandler:(nullable UIPrintInteractionCompletionHandler)completion {
    [Utility printDoc:doc animated:animated jobName:jobName delegate:delegate completionHandler:completion];
}

+ (void)printDoc:(FSPDFDoc *)doc fromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated jobName:(nullable NSString *)jobName delegate:(nullable id<UIPrintInteractionControllerDelegate>)delegate completionHandler:(nullable UIPrintInteractionCompletionHandler)completion {
    [Utility printDoc:doc fromRect:rect inView:view animated:animated jobName:jobName delegate:delegate completionHandler:completion];
}

@end

@interface ExAnnotIconProviderCallback ()

@property (nonatomic, strong) NSMutableArray *iconDocs;

@end

@implementation ExAnnotIconProviderCallback

- (NSString *)getProviderID {
    return @"FX";
}

- (NSString *)getProviderVersion {
    return @"0";
}

- (BOOL)hasIcon:(FSAnnotType)annotType iconName:(NSString *)iconName {
    if (annotType == FSAnnotNote || annotType == FSAnnotFileAttachment || annotType == FSAnnotStamp) {
        return YES;
    }
    return NO;
}
- (BOOL)canChangeColor:(FSAnnotType)annotType iconName:(NSString *)iconName {
    if (annotType == FSAnnotNote || annotType == FSAnnotFileAttachment) {
        return YES;
    }
    return NO;
}

- (FSShadingColor *)getShadingColor:(FSAnnotType)annotType iconName:(NSString *)iconName refColor:(unsigned int)refColor shadingIndex:(int)shadingIndex;
{
    FSShadingColor *shadingColor = [[FSShadingColor alloc] init];
    [shadingColor setFirst_color:refColor];
    [shadingColor setSecond_color:refColor];
    return shadingColor;
}

- (NSNumber *)getDisplayWidth:(FSAnnotType)annotType iconName:(NSString *)iconName {
    return [NSNumber numberWithFloat:32];
}

- (NSNumber *)getDisplayHeight:(FSAnnotType)annotType iconName:(NSString *)iconName {
    return [NSNumber numberWithFloat:32];
}

- (FSPDFPage *)getIcon:(FSAnnotType)annotType iconName:(NSString *)iconName color:(unsigned int)color {
    static NSArray *arrayNames = nil;
    if (!arrayNames) {
        arrayNames = [Utility getAllIconLowercaseNames];
    }

    NSInteger iconIndex = -1;
    if (annotType == FSAnnotNote || annotType == FSAnnotFileAttachment || annotType == FSAnnotStamp) {
        iconName = [iconName lowercaseString];
        if ([arrayNames containsObject:iconName]) {
            iconIndex = [arrayNames indexOfObject:iconName];
        }
    }

    if (iconIndex >= 0 && iconIndex < arrayNames.count) {
        if (!self.iconDocs) {
            self.iconDocs = [NSMutableArray arrayWithCapacity:arrayNames.count];
            for (int i = 0; i < arrayNames.count; i++) {
                [self.iconDocs addObject:[NSNull null]];
            }
        }

        FSPDFDoc *iconDoc = self.iconDocs[iconIndex];
        if ([iconDoc isEqual:[NSNull null]]) {
            NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:iconName ofType:@"pdf"];
            if (path) {
                iconDoc = [[FSPDFDoc alloc] initWithPath:path];
                FSErrorCode err = [iconDoc load:nil];
                if (FSErrSuccess == err) {
                    self.iconDocs[iconIndex] = iconDoc;
                }
            }
        }
        return [iconDoc isEqual:[NSNull null]] ? nil : [iconDoc getPage:0];
    }

    return nil;
}

- (FSShadingColor *)getShadingColor:(FSAnnotType)annotType iconName:(NSString *)iconName refColor:(unsigned int)refColor shadingIndex:(int)shadingIndex ShadingColor:(FSShadingColor *)out_shading_color {
    return [[FSShadingColor alloc] init];
}

@end


@implementation JSAlertViewDelegate

- (id)initWithType:(int)type;
{
    self = [super init];
    if(self)
    {
        self.type = type;
        self.retCode = -1;
    }
    return self;
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.type == 0 || self.type == 4) {
        self.retCode = 1;
    } else if (self.type == 1) {
        if (buttonIndex == 0) {
            self.retCode = 1;
        } else {
            self.retCode = 2;
        }
    } else if (self.type == 2) {
        if (buttonIndex == 0) {
            self.retCode = 4;
        } else {
            self.retCode = 3;
        }
    } else if (self.type == 3) {
        if (buttonIndex == 0) {
            self.retCode = 4;
        } else if (buttonIndex == 0) {
            self.retCode = 3;
        } else {
            self.retCode = 2;
        }
    } else
        self.retCode = 0;
}

@end


int _formCurrentPageIndex = 0;

@implementation ExActionHandler

- (id)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        self.delegate = nil;
    }
    return self;
}

- (int)getCurrentPage:(FSPDFDoc *)pdfDoc {
    if (_pdfViewCtrl.currentDoc == pdfDoc)
        return _formCurrentPageIndex;
    else
        return 0;
}

- (void)setCurrentPage:(FSPDFDoc *)pdfDoc pageIndex:(int)pageIndex {
    if (_pdfViewCtrl.currentDoc == pdfDoc)
        _formCurrentPageIndex = pageIndex;
}

- (FSRotation)getPageRotation:(FSPDFDoc *)pdfDoc pageIndex:(int)pageIndex {
    return 0;
}

- (BOOL)setPageRotation:(FSPDFDoc *)pdfDoc pageIndex:(int)pageIndex rotation:(FSRotation)rotation {
    return NO;
}

- (int)alert:(NSString *)msg title:(NSString *)title type:(int)type icon:(int)icon {
    if (!self.delegate) {
        self.delegate = [[JSAlertViewDelegate alloc] initWithType:type];
    }else{
        self.delegate.retCode = -1;
        self.delegate.type = type;
    }
    #if _MAC_CATALYST_
        if (self.extensionsManager.form_wait_catalyst) {
            dispatch_semaphore_signal(self.extensionsManager.form_wait_catalyst);
        }else{
            if (self.delegate.type == 0 || self.delegate.type == 4) {
                return 1;
            } else if (self.delegate.type == 1 || self.delegate.type == 3) {
                return 2;
            } else if (self.delegate.type == 2) {
                return 3;
            }
            return 0;
        }
    #endif
    dispatch_main_async_safe(^{
        TSAlertView *alertView = [[TSAlertView alloc] initWithTitle:title.length ? title : @"Alert"
                                                            message:msg
                                                           delegate:self.delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
        if (type == 0 || type == 4) {
            [alertView addButtonWithTitle:FSLocalizedForKey(@"kOK")];
            } else if (type == 1) {
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kOK")];
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kCancel")];
            } else if (type == 2) {
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kYes")];
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kNo")];
            } else if (type == 3) {
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kYes")];
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kNo")];
                [alertView addButtonWithTitle:FSLocalizedForKey(@"kCancel")];
            }
            [alertView show];
    });
    while (self.delegate.retCode == -1) {
        #if !_MAC_CATALYST_
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        #endif
    }
    return self.delegate.retCode;
}


- (FSIdentityProperties *)getIdentityProperties {
    FSIdentityProperties *ip = [[FSIdentityProperties alloc] init];
    [ip setCorporation:@"Foxit"];
    [ip setEmail:@"Foxit"];
    [ip setLogin_name:_extensionsManager.annotAuthor];
    [ip setName:_extensionsManager.annotAuthor];
    return ip;
}

- (BOOL)setDocChangeMark:(FSPDFDoc*)document change_mark:(BOOL)change_mark {
    _extensionsManager.isDocModified = change_mark;
    return YES;
}

- (BOOL)getDocChangeMark:(FSPDFDoc*)document {
    return _extensionsManager.isDocModified;
}

@end
