/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "LineAnnotHandler.h"
#import "LineToolHandler.h"

@interface LineAnnotHandler ()

@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic, strong) NSArray *measureRatioInfo;

// on pan, hide menu or property bar first and show it later
@property (nonatomic) BOOL shouldShowMenu;
@property (nonatomic) BOOL shouldShowPropertyBar;

@end

@implementation LineAnnotHandler

#pragma mark overrides

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-50, -50, -50, -50);
        self.colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
        self.startPoint = nil;
        self.endPoint = nil;
        self.measureRatioInfo = nil;
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotLine;
}

- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    int pageIndex = annot.pageIndex;
    FSLine *line = [[FSLine alloc] initWithAnnot:annot];
    CGPoint startPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[line getStartPoint] pageIndex:pageIndex];
    CGPoint endPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[line getEndPoint] pageIndex:pageIndex];
    CGPoint pvPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:point pageIndex:pageIndex];
    return [self isPoint:pvPoint
        inLineWithLineWidth:annot.lineWidth
                 startPoint:startPoint
                   endPoint:endPoint];
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    [super onAnnotSelected:annot];

    FSLine *line = [[FSLine alloc] initWithAnnot:annot];
    self.startPoint = [line getStartPoint];
    self.endPoint = [line getEndPoint];
    NSString *intent = annot.intent;
    LineToolHandler *toolHandler = [self.extensionsManager getToolHandlerByName:Tool_Line];
    if ([intent isEqualToString:@"LineDimension"]) {
        self.measureRatioInfo = [Utility getDistanceUnitInfo:[line getMeasureRatio]];
        toolHandler.isDistanceTool = YES;
        toolHandler.isArrowLine = NO;
    } else if ([intent isEqualToString:@"LineArrow"]) {
        toolHandler.isArrowLine = YES;
        toolHandler.isDistanceTool = NO;
    }
}

- (void)onAnnotDeselected:(FSAnnot *)annot {
    [super onAnnotDeselected:annot];
    self.startPoint = nil;
    self.endPoint = nil;
    self.measureRatioInfo = nil;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
    return options;
}

- (void)showStyle {
    [self.extensionsManager.propertyBar setColors:self.colors];

    FSAnnot *annot = self.extensionsManager.currentAnnot;
    FSLine *annot1 = [[FSLine alloc] initWithAnnot:annot];

    if ([[annot1 getIntent] isEqualToString:@"LineDimension"]) {
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_DISTANCE_UNIT | PROPERTY_LINEWIDTH frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_DISTANCE_UNIT stringValue:annot1 ? [annot1 getMeasureRatio] : self.extensionsManager.distanceUnit];
        [self.extensionsManager.propertyBar setDistanceLayoutsForbidEdit];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:annot.lineWidth];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_DISTANCE_UNIT lock:!annot1.canModifyAppearance];
    }else{
        [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
        [self.extensionsManager.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:annot.lineWidth];
    }
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];

    [self.extensionsManager.propertyBar addListener:self.extensionsManager];

    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    //move annotation or resize it
    BOOL canMove = [self canMoveAnnot:annot];
    BOOL canResize = [self canResizeAnnot:annot];
    if (!canMove && !canResize) {
        if (self.extensionsManager.currentAnnot) {
            [self showMenu];
        }
        return YES;
    }
    if (self.extensionsManager.currentAnnot != annot) {
        return NO;
    }

    FSLine *annot1 = [[FSLine alloc] initWithAnnot:annot];
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.shouldShowMenu = self.extensionsManager.shouldShowMenu;
        self.shouldShowPropertyBar = self.extensionsManager.shouldShowPropertyBar;
        [self hideMenu];
        [self.extensionsManager hidePropertyBar];

        self.startPoint = [annot1 getStartPoint];
        self.endPoint = [annot1 getEndPoint];

        CGPoint pvStartPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[annot1 getStartPoint] pageIndex:pageIndex];
        CGPoint pvEndPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[annot1 getEndPoint] pageIndex:pageIndex];
        UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        double newStartX = pvStartPoint.x;
        double newStartY = pvStartPoint.y;
        double newEndX = pvEndPoint.x;
        double newEndY = pvEndPoint.y;
        double LINE_DOT_MARGIN = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil].size.width / 2;
        if (pvStartPoint.x == pvEndPoint.x) {
            if (pvStartPoint.y < pvEndPoint.y) {
                newStartY -= LINE_DOT_MARGIN;
                newEndY += LINE_DOT_MARGIN;
            } else if (pvStartPoint.y > pvEndPoint.y) {
                newStartY += LINE_DOT_MARGIN;
                newEndY -= LINE_DOT_MARGIN;
            }
        } else if (pvStartPoint.x < pvEndPoint.x) {
            double angle = atan((pvEndPoint.y - pvStartPoint.y) / (pvEndPoint.x - pvStartPoint.x));
            newStartX = pvStartPoint.x - LINE_DOT_MARGIN * cos(angle);
            newStartY = pvStartPoint.y - LINE_DOT_MARGIN * sin(angle);
            newEndX = pvEndPoint.x + LINE_DOT_MARGIN * cos(angle);
            newEndY = pvEndPoint.y + LINE_DOT_MARGIN * sin(angle);
        } else if (pvStartPoint.x > pvEndPoint.x) {
            double angle = atan((pvEndPoint.y - pvStartPoint.y) / (pvStartPoint.x - pvEndPoint.x));
            newStartX = pvStartPoint.x + LINE_DOT_MARGIN * cos(angle);
            newStartY = pvStartPoint.y - LINE_DOT_MARGIN * sin(angle);
            newEndX = pvEndPoint.x - LINE_DOT_MARGIN * cos(angle);
            newEndY = pvEndPoint.y + LINE_DOT_MARGIN * sin(angle);
        }

        CGRect startPointRect = CGRectMake(newStartX - dragDot.size.width / 2, newStartY - dragDot.size.width / 2, dragDot.size.width * 2, dragDot.size.width);
        startPointRect = CGRectInset(startPointRect, -10, -10);
        CGRect endPointRect = CGRectMake(newEndX - dragDot.size.width / 2, newEndY - dragDot.size.width / 2, dragDot.size.width * 2, dragDot.size.width);
        endPointRect = CGRectInset(endPointRect, -10, -10);
        if (CGRectContainsPoint(startPointRect, point)) {
            self.editType = FSAnnotEditTypeStartPoint;
        } else if (CGRectContainsPoint(endPointRect, point)) {
            self.editType = FSAnnotEditTypeEndPoint;
        } else {
            self.editType = FSAnnotEditTypeFull;
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (self.editType == FSAnnotEditTypeUnknown) {
            return NO;
        }
        CGRect oldRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        CGPoint translationPoint = [recognizer translationInView:[self.pdfViewCtrl getPageView:pageIndex]];
        CGPoint lastPoint = CGPointMake(point.x - translationPoint.x, point.y - translationPoint.y);
        FSPointF *cp = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        FSPointF *lp = [self.pdfViewCtrl convertPageViewPtToPdfPt:lastPoint pageIndex:pageIndex];
        float tw = cp.x - lp.x;
        float th = cp.y - lp.y;

        FSPDFPage *page = [self.pdfViewCtrl.currentDoc getPage:pageIndex];
        FSRotation rotation = [page getRotation];
        CGSize pageSize = CGSizeMake([page getWidth], [page getHeight]);
        if (self.editType == FSAnnotEditTypeStartPoint || self.editType == FSAnnotEditTypeFull) {
            if (!annot.canModify) {
                return YES;
            }
            _startPoint.x += tw;
            _startPoint.y += th;
            // Not over page limit
            if (rotation != 1 && rotation != 3) {
                if (_startPoint.x < 0) {
                    _startPoint.x = 0;
                } else if (_startPoint.x > pageSize.width) {
                    _startPoint.x = pageSize.width;
                }
                if (_startPoint.y < 0) {
                    _startPoint.y = 0;
                } else if (_startPoint.y > pageSize.height) {
                    _startPoint.y = pageSize.height;
                }
            } else if (rotation == 1 || rotation == 3) {
                if (_startPoint.x < 0) {
                    _startPoint.x = 0;
                } else if (_startPoint.x > pageSize.height) {
                    _startPoint.x = pageSize.height;
                }
                if (_startPoint.y < 0) {
                    _startPoint.y = 0;
                } else if (_startPoint.y > pageSize.width) {
                    _startPoint.y = pageSize.width;
                }
            }
            annot1.startPoint = _startPoint;
        }
        if (self.editType == FSAnnotEditTypeEndPoint || self.editType == FSAnnotEditTypeFull) {
            if (!annot.canModify) {
                return YES;
            }
            _endPoint.x += tw;
            _endPoint.y += th;
            if (rotation != 1 && rotation != 3) {
                if (_endPoint.x < 0) {
                    _endPoint.x = 0;
                } else if (_endPoint.x > pageSize.width) {
                    _endPoint.x = pageSize.width;
                }
                if (_endPoint.y < 0) {
                    _endPoint.y = 0;
                } else if (_endPoint.y > pageSize.height) {
                    _endPoint.y = pageSize.height;
                }
            } else if (rotation == 1 || rotation == 3) {
                if (_endPoint.x < 0) {
                    _endPoint.x = 0;
                } else if (_endPoint.x > pageSize.height) {
                    _endPoint.x = pageSize.height;
                }
                if (_endPoint.y < 0) {
                    _endPoint.y = 0;
                } else if (_endPoint.y > pageSize.width) {
                    _endPoint.y = pageSize.width;
                }
            }
            annot1.endPoint = _endPoint;
        }
        [self.pdfViewCtrl lockRefresh];
        [annot1 resetAppearanceStream];
        [self.pdfViewCtrl unlockRefresh];

        [recognizer setTranslation:CGPointZero inView:[self.pdfViewCtrl getPageView:pageIndex]];

        self.annotImage = [Utility getAnnotImage:annot pdfViewCtrl:self.pdfViewCtrl];
        FSRectF *dibRect = [Utility convertToFSRect:_startPoint p2:_endPoint];
        CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:pageIndex];
        CGRect refreshRect = CGRectUnion(oldRect, newRect);
        refreshRect = UIEdgeInsetsInsetRect(refreshRect, self.annotRefreshRectEdgeInsets);
        [self.pdfViewCtrl refresh:refreshRect pageIndex:annot.pageIndex needRender:NO];
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        self.editType = FSAnnotEditTypeUnknown;
        self.startPoint = nil;
        self.endPoint = nil;
        if (self.shouldShowMenu) {
            [self showMenu];
        } else if (self.shouldShowPropertyBar) {
            [self.extensionsManager showPropertyBar];
        }
    }
    
    if ([[annot1 getIntent] isEqualToString:@"LineDimension"]){
        [self updateDistanceDataFromStartPoint:[annot1 getStartPoint] toEndPoint:[annot1 getEndPoint]];
    }
    return YES;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    if (self.extensionsManager.currentAnnot == annot && pageIndex == annot.pageIndex && self.annotImage) {
        FSLine *lineAnnot = [[FSLine alloc] initWithAnnot:annot];
        CGPoint startPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint ?: [lineAnnot getStartPoint] pageIndex:pageIndex];
        CGPoint endPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:self.endPoint ?: [lineAnnot getEndPoint] pageIndex:pageIndex];
        CGFloat lineWidth = [Utility convertWidth:annot.lineWidth fromPageViewToPDF:self.pdfViewCtrl pageIndex:pageIndex];

        CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        CGContextSaveGState(context);

        CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
        CGContextTranslateCTM(context, 0, rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
        CGContextDrawImage(context, rect, [self.annotImage CGImage]);

        CGContextRestoreGState(context);

        UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];

        CGPoint dotStartPoint, dotEndPoint;
        [self getDotStartPoint:&dotStartPoint dotEndPoint:&dotEndPoint withStartPoint:startPoint endPoint:endPoint lineWidth:lineWidth];
        dotStartPoint.x -= dragDot.size.width / 2;
        dotStartPoint.y -= dragDot.size.height / 2;
        dotEndPoint.x -= dragDot.size.width / 2;
        dotEndPoint.y -= dragDot.size.height / 2;
        [dragDot drawAtPoint:dotStartPoint];
        [dragDot drawAtPoint:dotEndPoint];

        if ([[lineAnnot getIntent] isEqualToString:@"LineDimension"]) {
            //Draw Distance Text
            NSMutableParagraphStyle *textStyle = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
            textStyle.alignment = NSTextAlignmentLeft;

            NSDictionary *textFontAttributes = @{
                NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12],
                NSForegroundColorAttributeName : UIColor.redColor,
                NSParagraphStyleAttributeName : textStyle
            };

            dotStartPoint.y -= 10;

            NSString *measureRatio = lineAnnot ? [lineAnnot getMeasureRatio] : self.extensionsManager.distanceUnit;
            float distance = [Utility getDistanceFromX:self.startPoint ?: [lineAnnot getStartPoint] toY:self.endPoint ?: [lineAnnot getEndPoint] withUnit:measureRatio];

            NSString *unitStr = lineAnnot ? [lineAnnot getMeasureUnit:0] : [_measureRatioInfo objectAtIndex:3];

            NSString *distanceContent = [NSString stringWithFormat:@"%.2f %@", distance, unitStr];
            [distanceContent drawAtPoint:dotStartPoint withAttributes:textFontAttributes];
        }
    }
}

#pragma mark private

- (BOOL)isPoint:(CGPoint)point inLineWithLineWidth:(float)lineWidth startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CGPoint thePoint = point;
    // ax + by + c = 0
    float a = startPoint.y - endPoint.y;
    float b = endPoint.x - startPoint.x;
    float c = startPoint.x * endPoint.y - startPoint.y * endPoint.x;
    float distanceSquare = powf((a * thePoint.x + b * thePoint.y + c), 2) / (a * a + b * b);
    const float tolerance = 20.0f + lineWidth / 2;
    if (distanceSquare < powf(tolerance, 2)) {
        if (((startPoint.x - tolerance < thePoint.x && thePoint.x < endPoint.x + tolerance) ||
             (endPoint.x - tolerance < thePoint.x && thePoint.x < startPoint.x + tolerance)) &&
            ((startPoint.y - tolerance < thePoint.y && thePoint.y < endPoint.y + tolerance) ||
             (endPoint.y - tolerance < thePoint.y && thePoint.y < startPoint.y + tolerance))) {
            return YES;
        }
    }
    return NO;
}

// get positions of starting and ending dot images
- (void)getDotStartPoint:(CGPoint *)dotStartPoint dotEndPoint:(CGPoint *)dotEndPoint withStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint lineWidth:(CGFloat)lineWidth {
    assert(dotStartPoint != nil);
    assert(dotEndPoint != nil);

    double angle = atan((endPoint.y - startPoint.y) / (endPoint.x - startPoint.x));
    if (angle < 0) {
        angle = -angle;
    }
    if (startPoint.x > endPoint.x && startPoint.y > endPoint.y) {
        startPoint = CGPointMake(startPoint.x - lineWidth * 0.5 * cos(angle), startPoint.y - lineWidth * 0.5 * sin(angle));
        endPoint = CGPointMake(endPoint.x + lineWidth * 0.5 * cos(angle), endPoint.y + lineWidth * 0.5 * sin(angle));
    } else if (startPoint.x > endPoint.x && startPoint.y < endPoint.y) {
        startPoint = CGPointMake(startPoint.x - lineWidth * 0.5 * cos(angle), startPoint.y + lineWidth * 0.5 * sin(angle));
        endPoint = CGPointMake(endPoint.x + lineWidth * 0.5 * cos(angle), endPoint.y - lineWidth * 0.5 * sin(angle));
    } else if (startPoint.x < endPoint.x && startPoint.y < endPoint.y) {
        startPoint = CGPointMake(startPoint.x + lineWidth * 0.5 * cos(angle), startPoint.y + lineWidth * 0.5 * sin(angle));
        endPoint = CGPointMake(endPoint.x - lineWidth * 0.5 * cos(angle), endPoint.y - lineWidth * 0.5 * sin(angle));
    } else if (startPoint.x < endPoint.x && startPoint.y > endPoint.y) {
        startPoint = CGPointMake(startPoint.x + lineWidth * 0.5 * cos(angle), startPoint.y - lineWidth * 0.5 * sin(angle));
        endPoint = CGPointMake(endPoint.x - lineWidth * 0.5 * cos(angle), endPoint.y + lineWidth * 0.5 * sin(angle));
    }

    double newStartX = startPoint.x;
    double newStartY = startPoint.y;
    double newEndX = endPoint.x;
    double newEndY = endPoint.y;
    double LINE_DOT_MARGIN = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil].size.width / 2;
    if (startPoint.x == endPoint.x) {
        if (startPoint.y < endPoint.y) {
            newStartY -= LINE_DOT_MARGIN;
            newEndY += LINE_DOT_MARGIN;
        } else if (startPoint.y > endPoint.y) {
            newStartY += LINE_DOT_MARGIN;
            newEndY -= LINE_DOT_MARGIN;
        }
    } else if (startPoint.x < endPoint.x) {
        double angle = atan((endPoint.y - startPoint.y) / (endPoint.x - startPoint.x));
        newStartX = startPoint.x - LINE_DOT_MARGIN * cos(angle);
        newStartY = startPoint.y - LINE_DOT_MARGIN * sin(angle);
        newEndX = endPoint.x + LINE_DOT_MARGIN * cos(angle);
        newEndY = endPoint.y + LINE_DOT_MARGIN * sin(angle);
    } else if (startPoint.x > endPoint.x) {
        double angle = atan((endPoint.y - startPoint.y) / (startPoint.x - endPoint.x));
        newStartX = startPoint.x + LINE_DOT_MARGIN * cos(angle);
        newStartY = startPoint.y - LINE_DOT_MARGIN * sin(angle);
        newEndX = endPoint.x - LINE_DOT_MARGIN * cos(angle);
        newEndY = endPoint.y + LINE_DOT_MARGIN * sin(angle);
    }
    dotStartPoint->x = newStartX;
    dotStartPoint->y = newStartY;
    dotEndPoint->x = newEndX;
    dotEndPoint->y = newEndY;
}

// get line annot's pageview rectangle (including drag dot)
- (CGRect)getRectForSelectedLine:(FSLine *)line {
    int pageIndex = line.pageIndex;
    CGPoint startPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[line getStartPoint] pageIndex:pageIndex];
    CGPoint endPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[line getEndPoint] pageIndex:pageIndex];
    CGFloat lineWidth = [Utility convertWidth:line.lineWidth fromPageViewToPDF:self.pdfViewCtrl pageIndex:pageIndex];
    CGPoint dotStartPoint, dotEndPoint;
    [self getDotStartPoint:&dotStartPoint dotEndPoint:&dotEndPoint withStartPoint:startPoint endPoint:endPoint lineWidth:lineWidth];
    UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    return CGRectInset([Utility convertToCGRect:dotStartPoint p2:dotEndPoint], -dragDot.size.width / 2, -dragDot.size.height / 2);
}

- (void)updateDistanceDataFromStartPoint:(FSPointF *)start toEndPoint:(FSPointF *)end {
    FSLine *lineAnnot = [[FSLine alloc] initWithAnnot:self.annot];

    NSString *measureRatio = lineAnnot ? [lineAnnot getMeasureRatio] : self.extensionsManager.distanceUnit;
    float distance = [Utility getDistanceFromX:start toY:end withUnit:measureRatio];

    NSString *unitStr = lineAnnot ? [lineAnnot getMeasureUnit:0] : [_measureRatioInfo objectAtIndex:3];

    NSString *distanceContent = [NSString stringWithFormat:@"%.2f %@", distance, unitStr];
    [lineAnnot setContent:distanceContent];
}

- (CGPoint)rotateVec:(float)px py:(float)py ang:(float)ang isChlen:(BOOL)isChlen newLine:(float)newLen {
    CGPoint point = CGPointMake(0, 0);
    float vx = px * cosf(ang) - py * sinf(ang);
    float vy = px * sinf(ang) + py * cosf(ang);
    if (isChlen) {
        float d = sqrtf(vx * vx + vy * vy);
        vx = vx / d * newLen;
        vy = vy / d * newLen;
        point.x = vx;
        point.y = vy;
    }
    return point;
}

@end
