/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "LineToolHandler.h"
#import "PaintUtility.h"

#define DEFAULT_RECT_WIDTH 200

@interface LineToolHandler ()

@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic, strong) FSLine *annot;

@property (nonatomic, assign) BOOL isEndDrawingLine;
@property (nonatomic, assign) int currentPageIndex;

@property (nonatomic, strong) PaintUtility *paintUtility;

@end

@implementation LineToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotLine;
        _isArrowLine = NO;
    }
    return self;
}

- (void)setStartPoint:(FSPointF *)startPoint {
    _startPoint = startPoint;
}

- (NSString *)getName {
    return Tool_Line;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
    _isEndDrawingLine = NO;
    self.annot = nil;
}

- (void)onDeactivate {
    _isEndDrawingLine = NO;
    if(self.annot)
    {
        [self updateLinePoints];
        FSAnnot* annot = self.annot;
        self.annot = nil;
        id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
        [annotHandler addAnnot:annot addUndo:YES];
    }
}


- (void)updateLinePoints {
    if(self.startPoint && self.endPoint) {
        [self.annot setStartPoint:self.startPoint];
        [self.annot setEndPoint:self.endPoint];
        [self.annot resetAppearanceStream];
    }
}

#pragma mark PageView Gesture+Touch

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    self.currentPageIndex = pageIndex;
    _isEndDrawingLine = NO;
    
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty]) {
        return NO;
    }

    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    float defaultRectWidth = [Utility convertWidth:DEFAULT_RECT_WIDTH fromPageViewToPDF:_pdfViewCtrl pageIndex:pageIndex];
    CGPoint startPoint = CGPointMake(point.x - defaultRectWidth / 2, point.y + defaultRectWidth / 2);
    CGPoint endPoint = CGPointMake(point.x + defaultRectWidth / 2, point.y - defaultRectWidth / 2);

    float marginX = 5.0;
    float marginY = 5.0;
    float maxX = pageView.frame.size.width - marginX;
    float maxY = pageView.frame.size.height - marginY;

    if (point.x < marginX || point.x > maxX || point.y < marginY || point.y > maxY) {
        return YES;
    }

    float tempStart = 0.0;
    float tempEnd = 0.0;
    if (startPoint.x < marginX) {
        tempStart = tempEnd = marginX - startPoint.x;
        if (endPoint.y - tempEnd < marginY) {
            tempEnd = endPoint.y - marginY;
        }
    }
    if (startPoint.y > maxY && point.x + point.y > maxY) {
        tempStart = tempEnd = startPoint.y - maxY;
        if (endPoint.x + tempEnd > maxX) {
            tempEnd = maxX - endPoint.x;
        }
    }
    if (endPoint.x > maxX) {
        tempStart = tempEnd = maxX - endPoint.x;
        if (startPoint.y - tempEnd > maxY) {
            tempStart = startPoint.y - maxY;
        }
    }
    if (endPoint.y < marginY && point.x + point.y < maxX) {
        tempStart = tempEnd = endPoint.y - marginY;
        if (startPoint.x + tempEnd < marginX) {
            tempStart = marginX - startPoint.x;
        }
    }
    startPoint.x += tempStart;
    startPoint.y -= tempStart;
    endPoint.x += tempEnd;
    endPoint.y -= tempEnd;

    self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:startPoint pageIndex:pageIndex];
    self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:endPoint pageIndex:pageIndex];

    FSRectF *dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
    FSLine *line = [self addAnnotToPage:pageIndex withRect:dibRect];
    if (!line || [line isEmpty]) {
        return YES;
    }
    if (_isArrowLine) {
        [line setLineEndStyle:FSMarkupEndingStyleOpenArrow];
        [line setIntent:@"LineArrow"];
    }
    
    [_pdfViewCtrl lockRefresh];
    [line setStartPoint:self.startPoint];
    [line setEndPoint:self.endPoint];
    [line resetAppearanceStream];
    [_pdfViewCtrl unlockRefresh];

    id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:line];
    [annotHandler addAnnot:line addUndo:YES];

    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect rect = [pageView frame];
    CGSize size = rect.size;
    
    void (^end)(void) = ^(){
        if (self.annot) {
            FSLine *annot = [[FSLine alloc] initWithAnnot:self.annot];
            [self updateLinePoints];
            if (self.isArrowLine) {
                CGPoint startPoint = [self->_pdfViewCtrl convertPdfPtToPageViewPt:annot.startPoint pageIndex:self.currentPageIndex];
                CGPoint endPoint = [self->_pdfViewCtrl convertPdfPtToPageViewPt:annot.endPoint pageIndex:self.currentPageIndex];
                if (!CGPointEqualToPoint(startPoint, endPoint)) {
                    [annot setLineEndStyle:FSMarkupEndingStyleOpenArrow];
                    [annot setIntent:@"LineArrow"];
                }
            }
            [self->_pdfViewCtrl lockRefresh];
            [annot resetAppearanceStream];
            [self->_pdfViewCtrl unlockRefresh];
            self.annot = nil;
            id<IAnnotHandler> annotHandler = [self->_extensionsManager getAnnotHandlerByAnnot:annot];
            [annotHandler addAnnot:annot addUndo:YES];
        }
    };
    
    if (point.x > size.width || point.y > size.height || point.x < 0 || point.y < 0){
        end();
        return NO;
    }

    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSRectF *dibRect = [[FSRectF alloc] init];
    
    if (self.startPoint && self.endPoint) {
        dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
    } else {
        [dibRect setLeft:dibPoint.x];
        [dibRect setBottom:dibPoint.y];
        [dibRect setRight:dibPoint.x + 0.1];
        [dibRect setTop:dibPoint.y + 0.1];
    }
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.currentPageIndex = pageIndex;
        _isEndDrawingLine = NO;
        
        if(self.annot)
            end();

        FSLine *line = [self addAnnotToPage:pageIndex withRect:dibRect];
        if (!line || [line isEmpty]) {
            return NO;
        }
        self.annot = line;
        self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.endPoint = self.startPoint;
        [_pdfViewCtrl lockRefresh];
        [line setStartPoint:self.startPoint];
        [line setEndPoint:self.endPoint];
//        [line resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        
        FSRectF *lineFSRect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:self.annot.lineWidth top1:self.annot.lineWidth];
        CGRect lineRect = [_pdfViewCtrl convertPdfRectToPageViewRect:lineFSRect pageIndex:pageIndex];
        float lineWidth = lineRect.size.width;
        
        self.paintUtility = [[PaintUtility alloc] init];
        self.paintUtility.annotLineWidth = lineWidth;
        self.paintUtility.annotColor = self.annot.color;
        self.paintUtility.annotOpacity = self.annot.opacity;
        
        return YES;
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (pageIndex != self.annot.pageIndex) {
            end();
            return NO;
        }

        self.endPoint = dibPoint;
//        [_pdfViewCtrl lockRefresh];
//        [self.annot setEndPoint:dibPoint];
//        [self.annot resetAppearanceStream];
//        [_pdfViewCtrl unlockRefresh];
//        FSRectF *pdfRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        //self.annot.fsrect = dibRect;

//        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:pdfRect pageIndex:pageIndex];
//        rect = CGRectInset(rect, -20, -20);
//        [_pdfViewCtrl refresh:rect pageIndex:pageIndex];
        
        
        [_pdfViewCtrl refresh:pageIndex];
        
        return YES;

    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        _isEndDrawingLine = YES;
        
        if (self.annot) {


            end();
        }
        return YES;
    }
    return NO;
}

- (FSLine *)addAnnotToPage:(int)pageIndex withRect:(FSRectF *)rect {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;

    FSLine *annot = [[FSLine alloc] initWithAnnot:[page addAnnot:self.type rect:rect]];
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.color = [_extensionsManager getPropertyBarSettingColor:self.type];
    annot.opacity = [_extensionsManager getAnnotOpacity:self.type] / 100.0f;
    annot.lineWidth = [_extensionsManager getAnnotLineWidth:self.type];
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    if (!_isArrowLine) {
        annot.subject = @"Line";
    } else if (_isArrowLine) {
//        [annot setIntent:@"LineArrow"];
        annot.subject = @"ArrowLine";
    }
    annot.flags = FSAnnotFlagPrint;
    return annot;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_isEndDrawingLine) {
        _isEndDrawingLine = YES;
        if (self.annot) {
            FSLine *annot = [[FSLine alloc] initWithAnnot:self.annot];
            if (_isArrowLine) {
                [annot setLineEndStyle:FSMarkupEndingStyleOpenArrow];
                [annot setIntent:@"LineArrow"];
            }
            [_pdfViewCtrl lockRefresh];
            [annot setStartPoint:self.startPoint];
            [annot setEndPoint:self.endPoint];
            [annot resetAppearanceStream];
            [_pdfViewCtrl unlockRefresh];
        }
    }
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

#pragma mark draw by ios
- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_isEndDrawingLine) {
        return ;
    }
    if (self.annot != nil && ![self.annot isEmpty] && self.currentPageIndex == pageIndex) {
        CGPoint startPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint pageIndex:pageIndex];
        CGPoint endPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.endPoint pageIndex:pageIndex];
        
        [self.paintUtility drawLineFromStartPoint:startPoint toEndPoint:endPoint inContext:context isArrowLine:_isArrowLine];
    }
}
@end
