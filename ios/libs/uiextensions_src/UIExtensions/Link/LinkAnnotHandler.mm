/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "LinkAnnotHandler.h"

static NSString *LINK_DES_TYPE = @"DesType";
static NSString *LINK_DES_INDEX = @"DesIndex";
static NSString *LINK_DES_RECT = @"DesRect";
static NSString *LINK_DES_URL = @"DesURL";
static NSString *LINK_DES_AREA = @"DesArea";
static NSString *LINK_DES_FILE = @"DesFile";

@interface LinkAnnotHandler ()

@property (nonatomic, strong) NSMutableDictionary *dictAnnotLink;
//@property (nonatomic, strong) NSDictionary *jumpDict;

- (void)reloadAnnotLink:(FSPDFPage *)dmpage;
- (void)loadAnnotLink:(FSPDFPage *)dmpage;

@end

@implementation LinkAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        _dictAnnotLink = [[NSMutableDictionary alloc] init];
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-2, -2, -2, -2);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotLink;
}

- (BOOL)shouldDrawAnnot:(FSAnnot *)annot inPDFViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    return YES;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer annot:(FSAnnot *)annot {
    if (!self.extensionsManager.enableLinks) {
        return NO;
    }
    if (![gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return NO;
    }
    CGPoint point = [gestureRecognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
    __block BOOL ret = NO;
    id linkArray = nil;
    @synchronized(_dictAnnotLink) {
        [self reloadAnnotLink:[self.pdfViewCtrl.currentDoc getPage:pageIndex]];
        linkArray = [_dictAnnotLink objectForKey:@(pageIndex)];
    }
    if (linkArray && linkArray != [NSNull null]) {
        [linkArray enumerateObjectsWithOptions:NSEnumerationReverse
                                    usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                        NSDictionary *linkDict = obj;
                                        NSArray *desAreaArray = [linkDict objectForKey:LINK_DES_AREA];
                                        [desAreaArray enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                                            NSArray *pointsArray = obj2;
                                            FSPointF *dibPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
                                            CGPoint point = CGPointMake(dibPoint.x, dibPoint.y);
                                            if ([Utility isPointInPolygon:point polygonPoints:pointsArray]) {
                                                [self.pdfViewCtrl refresh:CGRectZero pageIndex:pageIndex];
                                                ret = YES;
                                                *stop2 = YES;
                                                *stop = YES;
                                            }
                                        }];
                                    }];
    }
    return ret;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if (!self.extensionsManager.enableLinks) {
        return NO;
    }
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
    __block BOOL ret = NO;
    id linkArray = nil;
    @synchronized(_dictAnnotLink) {
        [self reloadAnnotLink:[self.pdfViewCtrl.currentDoc getPage:pageIndex]];
        linkArray = [_dictAnnotLink objectForKey:@(pageIndex)];
    }
    if (linkArray && linkArray != [NSNull null]) {
        [linkArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *linkDict = obj;
            NSArray *desAreaArray = [linkDict objectForKey:LINK_DES_AREA];
            [desAreaArray enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                NSArray *pointsArray = obj2;
                FSPointF *dibPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
                CGPoint point = CGPointMake(dibPoint.x, dibPoint.y);
                if ([Utility isPointInPolygon:point polygonPoints:pointsArray]) {
                    [self.pdfViewCtrl refresh:CGRectZero pageIndex:pageIndex];

                    BOOL isCustomLink = [self.extensionsManager onLinkOpen:linkDict LocationInfo:point];
                    if (!isCustomLink){
                        int type = [[linkDict objectForKey:LINK_DES_TYPE] intValue];
                        if (type == FSActionTypeGoto) {
                            [self jumpToPageWithDict:linkDict];
                        }
                        if (type == FSActionTypeURI) {
                            NSURL *URL = [NSURL URLWithString:[linkDict objectForKey:LINK_DES_URL]];
                            NSString *scheme = URL.scheme;
                            
                            BOOL isExit = [[UIApplication sharedApplication] canOpenURL:URL];
                            if (isExit) {
                                [[UIApplication sharedApplication] openURL:URL];
                            } else if (scheme && scheme.length > 0) {
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kNoAppropriateApplication") preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                                [alertController addAction:action];
                                [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                            } else {
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kInvalidUrl") preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                                [alertController addAction:action];
                                [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                            }
                        }
                        if (type == FSActionTypeLaunch) {
                            FSFileSpec *file = [linkDict objectForKey:LINK_DES_FILE];
                            if (![file isEmpty]) {
                                NSString *fileName = [file getFileName];
                                fileName = [fileName stringByReplacingOccurrencesOfString:@":" withString:@"/"];
                                NSString *path = [NSString stringWithFormat:@"%@%@%@", [self.extensionsManager.pdfViewCtrl.filePath stringByDeletingLastPathComponent], @"/", fileName];
                                NSFileManager *fileManager = [NSFileManager defaultManager];
                                if (!path || path == nil || ![fileManager fileExistsAtPath:path]) {
                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kUnfoundOrCannotOpen") preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                                    [alertController addAction:action];
                                    [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                    ret = YES;
                                    return;
                                }
                                
                                NSURL *URL = [[NSURL alloc] initFileURLWithPath:path];
                                NSString *ext = [path pathExtension];
                                if ([ext isEqualToString:@"pdf"]) {
                                    [self setDocWithPath:path dict:linkDict];
                                } else {
                                    BOOL success = [[UIApplication sharedApplication] openURL:URL];
                                    if (!success) {
                                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kNoAppropriateApplication") preferredStyle:UIAlertControllerStyleAlert];
                                        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                                        [alertController addAction:action];
                                        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                    }
                                }
                            }
                        }
                        if (type == FSActionTypeGoToR) {
                            FSFileSpec *file = [linkDict objectForKey:LINK_DES_FILE];
                            if (![file isEmpty]) {
                                NSString *fileName = [file getFileName];
                                fileName = [fileName stringByReplacingOccurrencesOfString:@":" withString:@"/"];
                                NSString *path = [NSString stringWithFormat:@"%@%@%@", [self.extensionsManager.pdfViewCtrl.filePath stringByDeletingLastPathComponent], @"/", fileName];
                                NSFileManager *fileManager = [NSFileManager defaultManager];
                                if (!path || path == nil || ![fileManager fileExistsAtPath:path]) {
                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kUnfoundOrCannotOpen") preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                                    [alertController addAction:action];
                                    [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                    ret = YES;
                                    return;
                                }

                                [self setDocWithPath:path dict:linkDict];
                            }
                        }
                    }
                    ret = YES;
                    *stop2 = YES;
                    *stop = YES;
                }
            }];
        }];
    }
    return ret;
}

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(nonnull UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *_Nullable)annot {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    [self onRealPageViewDraw:[self.pdfViewCtrl.currentDoc getPage:pageIndex] inContext:context pageIndex:pageIndex];
}

//draw link
- (void)onRealPageViewDraw:(FSPDFPage *)page inContext:(CGContextRef)context pageIndex:(int)pageIndex {
    if (!self.extensionsManager.enableHighlightLinks)
        return;
    [self loadAnnotLink:page];

    NSArray *array = nil;
    @try {
        array = [_dictAnnotLink objectForKey:[NSNumber numberWithInt:[page getIndex]]];
    } @catch (NSException *exception) {
    }
   
    if (array && ((id) array != [NSNull null])) {
        if (![SettingPreference getPDFHighlightLinks]) {
            return;
        }

        CGFloat red, green, blue, alpha;
        [self.extensionsManager.linksHighlightColor getRed:&red green:&green blue:&blue alpha:&alpha];
        CGContextSetRGBFillColor(context, red, green, blue, alpha);
        [array enumerateObjectsWithOptions:NSEnumerationReverse
                                usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                    NSDictionary *linkDict = obj;
                                    NSArray *desAreaArray = [linkDict objectForKey:LINK_DES_AREA];
                                    [desAreaArray enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                                        NSArray *pointArray = obj2;
                                        CGContextBeginPath(context);
                                        [pointArray enumerateObjectsUsingBlock:^(id obj3, NSUInteger idx3, BOOL *stop3) {
                                            CGPoint p = [obj3 CGPointValue];
                                            FSPointF *p2 = [[FSPointF alloc] init];
                                            p2.x = p.x;
                                            p2.y = p.y;
                                            CGPoint pp = [self.pdfViewCtrl convertPdfPtToPageViewPt:p2 pageIndex:[page getIndex]];
                                            p.x = pp.x;
                                            p.y = pp.y;

                                            if ([self.pdfViewCtrl getCropMode] != PDF_CROP_MODE_NONE) {
                                                UIEdgeInsets insets = [self.pdfViewCtrl getCropInsets:[page getIndex]];
                                                if (!UIEdgeInsetsEqualToEdgeInsets(insets, UIEdgeInsetsZero)) {
                                                    p.x = p.x >[self.pdfViewCtrl getPageViewWidth:[page getIndex]] ? [self.pdfViewCtrl getPageViewWidth:[page getIndex]] : p.x;
                                                    p.x = p.x < 0 ? 0 : p.x;
                                                }
                                            }
                                            
                                            if (idx3 == 0) {
                                                CGContextMoveToPoint(context, p.x, p.y);
                                            } else {
                                                CGContextAddLineToPoint(context, p.x, p.y);
                                            }
                                        }];
                                        CGContextClosePath(context);
                                        CGContextFillPath(context);
                                    }];
                                }];
    }
}

#pragma mark private

//- (void)setJumpDocWithPath:(NSString *)path dict:(NSDictionary *)dict {
//    FSPDFDoc *doc = [[FSPDFDoc alloc] initWithPath:path];
//    __block FSErrorCode status = [doc load:nil];
//    if (status == FSErrPassword) {
//        AlertView *alert = [[AlertView alloc] initWithTitle:nil
//                                                    message:@"kDocNeedPassword"
//                                         buttonClickHandler:^(AlertView *alertView, NSInteger buttonIndex) {
//                                             if (buttonIndex == 0) {
//                                                 return;
//                                             } else if (buttonIndex == 1) {
//                                                 NSString *password = [alertView textFieldAtIndex:1].text;
//                                                 status = [doc load:password];
//                                                 if (status == FSErrSuccess) {
//                                                     [self.extensionsManager.pdfViewCtrl openDoc:path password:password completion:nil];
//                                                     _jumpDict = [[NSDictionary alloc] initWithDictionary:dict];
//                                                 }
//                                             }
//                                         }
//                                          cancelButtonTitle:@"kCancel"
//                                          otherButtonTitles:@"kOK", nil];
//        [alert show];
//    } else if (status == FSErrSuccess) {
//        [self.extensionsManager.pdfViewCtrl openDoc:path password:nil completion:nil];
//        _jumpDict = [[NSDictionary alloc] initWithDictionary:dict];
//    }
//}

- (void)setDocWithPath:(NSString *)path dict:(NSDictionary *)dict {
    if (path == nil) {
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    void (^jumpblock)(BOOL) = ^(BOOL isNeedSave){
        if (self.extensionsManager.isMultiFileMode) {
            BOOL isOpen = [weakSelf.extensionsManager.delegate uiextensionsManager:weakSelf.extensionsManager openNewDocAtPath:path shouldCloseCurrentDoc:NO];
            if (isOpen) {
                [weakSelf jumpToPageWithDict:dict];
            }
            return ;
        }
        if (isNeedSave) {
            [weakSelf.extensionsManager saveAndCloseCurrentDoc:^(BOOL success) {
                if (success) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    strongSelf.extensionsManager.isDocModified = NO;
                    [strongSelf.pdfViewCtrl openDoc:path
                                           password:nil
                                         completion:^(FSErrorCode error) {
                                             [weakSelf jumpToPageWithDict:dict];
                                         }];
                }
            }];
        }else{
            weakSelf.extensionsManager.isDocModified = NO;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.pdfViewCtrl openDoc:path
                                   password:nil
                                 completion:^(FSErrorCode error) {
                                     
                                     [weakSelf jumpToPageWithDict:dict];
                                 }];
        }
    };
    
    if (self.extensionsManager.isDocModified && !self.extensionsManager.isMultiFileMode) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kShouldSaveFileTips") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            jumpblock(NO);
        }];
        
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            jumpblock(YES);
        }];
        
        [alertController addAction:cancelaction];
        [alertController addAction:okaction];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    }else{
        jumpblock(NO);
    }
}

- (void)jumpToPageWithDict:(NSDictionary *)dict {
    int jumpIndex = [[dict objectForKey:LINK_DES_INDEX] intValue];
    if (jumpIndex >= 0 && jumpIndex < [self.extensionsManager.pdfViewCtrl.currentDoc getPageCount]) {
        //prevent sometimes it's faster than return YES
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            FSPointF *point = [[FSPointF alloc] init];
            point.x = 0;
            if (dict[LINK_DES_RECT]) {
                FSRectF *desDibRect = [Utility CGRect2FSRectF:[dict[LINK_DES_RECT] CGRectValue]];
                point.y = desDibRect.top;
            } else {
                FSPDFPage *page = nil;
                @try {
                    page = [self.extensionsManager.pdfViewCtrl.currentDoc getPage:jumpIndex];
                } @catch (NSException *exception) {
                }
                point.y = page ? [page getHeight] : 0;
            }
            [self.pdfViewCtrl gotoPage:jumpIndex withDocPoint:point animated:YES];
        });
    }
}

#pragma mark link

- (void)loadAnnotLink:(FSPDFPage *)dmpage {
    NSMutableArray *array = nil;
    @synchronized(_dictAnnotLink) {
        @try {
            array = [_dictAnnotLink objectForKey:[NSNumber numberWithInt:[dmpage getIndex]]];
        } @catch (NSException *exception) {
        }
    }
    if (!array) {
        if (!dmpage || [dmpage isEmpty]) {
            return;
        }
        int linkCount = 0;
        @try {
            linkCount = [dmpage getAnnotCount];
            if (linkCount > 0) {
                array = [NSMutableArray array];
            }
        } @catch (NSException *exception) {
            return;
        }
        
        for (int i = 0; i < linkCount; i++) {
            FSAnnot *annot = [dmpage getAnnot:i];
            if (!annot || [annot isEmpty]) {
                continue;
            }

            if (FSAnnotLink != [annot getType])
                continue;
            FSLink *link = [[FSLink alloc] initWithAnnot:annot];
            FSAction *action = nil;
            @try {
                action = [link getAction];
            } @catch (NSException *exception) {
                NSLog(@"%@", exception.description);
            }
            while (action && ![action isEmpty]) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];

                BOOL support = NO;
                if ([action getType] == FSActionTypeGoto) {
                    FSGotoAction *gotoAction = [[FSGotoAction alloc] initWithAction:action];
                    FSDestination *dest = [gotoAction getDestination];
                    if (dest && ![dest isEmpty]) {
                        FSRectF *rect = [[FSRectF alloc] init];
                        rect.left = [dest getLeft];
                        rect.bottom = [dest getBottom];
                        rect.top = [dest getTop];
                        rect.right = [dest getRight];
                        int desIndex = [dest getPageIndex:[dmpage getDocument]];

                        CGRect desRect = [Utility FSRectF2CGRect:rect];
                        [dict setValue:@(FSActionTypeGoto) forKey:LINK_DES_TYPE];
                        [dict setValue:@(desIndex) forKey:LINK_DES_INDEX];
                        [dict setValue:[NSValue valueWithCGRect:desRect] forKey:LINK_DES_RECT];
                        
                    }else{
                        [dict setValue:@(FSActionTypeGoto) forKey:LINK_DES_TYPE];
                        [dict setValue:@(0) forKey:LINK_DES_INDEX];
                        [dict setValue:[NSValue valueWithCGRect:CGRectZero] forKey:LINK_DES_RECT];
                    }
                    support = YES;
                }
                if ([action getType] == FSActionTypeGoToR) {
                    FSRemoteGotoAction *gotoRAction = [[FSRemoteGotoAction alloc] initWithAction:action];
                    //fileSpec
                    FSFileSpec *file = [gotoRAction getFileSpec];
                    if (![file isEmpty]) {
                        [dict setValue:file forKey:LINK_DES_FILE];
                        support = YES;
                        [dict setValue:@(FSActionTypeGoToR) forKey:LINK_DES_TYPE];
                        FSDestination *dest = [gotoRAction getDestination];
                        if (dest && ![dest isEmpty]) {
                            FSRectF *rect = [[FSRectF alloc] init];
                            rect.left = [dest getLeft];
                            rect.bottom = [dest getBottom];
                            rect.top = [dest getTop];
                            rect.right = [dest getRight];
                            int desIndex = [dest getPageIndex:[dmpage getDocument]];

                            CGRect desRect = [Utility FSRectF2CGRect:rect];
                            [dict setValue:@(desIndex) forKey:LINK_DES_INDEX];
                            [dict setValue:[NSValue valueWithCGRect:desRect] forKey:LINK_DES_RECT];
                        } else {
                            [dict setValue:@0 forKey:LINK_DES_INDEX];
                        }
                    }
                }
                if ([action getType] == FSActionTypeLaunch) {
                    FSLaunchAction *launchAction = [[FSLaunchAction alloc] initWithAction:action];
                    //fileSpec
                    FSFileSpec* file = [launchAction getFileSpec];
                    [dict setValue:file forKey:LINK_DES_FILE];
                    [dict setValue:@(FSActionTypeLaunch) forKey:LINK_DES_TYPE];
                    support = YES;
                }

                if ([action getType] == FSActionTypeURI) {
                    FSURIAction *uriAction = [[FSURIAction alloc] initWithAction:action];
                    NSString *uri = [uriAction getURI];
                    [dict setValue:uri forKey:LINK_DES_URL];
                    [dict setValue:@(FSActionTypeURI) forKey:LINK_DES_TYPE];
                    support = YES;
                }
                if (support) {
                    NSArray *desArea = [self getAnnotationQuad:annot];
                    if (!desArea) {
                        FSRectF *rect = [annot getRect];
    
                        CGPoint point1;
                        point1.x = rect.left;
                        point1.y = rect.bottom;
                        NSValue *value1 = [NSValue valueWithCGPoint:point1];
                        CGPoint point2;
                        point2.x = rect.right;
                        point2.y = rect.bottom;
                        NSValue *value2 = [NSValue valueWithCGPoint:point2];
                        CGPoint point3;
                        point3.x = rect.right;
                        point3.y = rect.top;
                        NSValue *value3 = [NSValue valueWithCGPoint:point3];
                        CGPoint point4;
                        point4.x = rect.left;
                        point4.y = rect.top;
                        NSValue *value4 = [NSValue valueWithCGPoint:point4];
                        NSArray *arrayQuad = [NSArray arrayWithObjects:value1, value2, value3, value4, nil];
                        desArea = [NSArray arrayWithObject:arrayQuad];
                    }

                    if (desArea) {
                        [dict setValue:desArea forKey:LINK_DES_AREA];
                    }
                }
                
                if (dict.count > 0) {
                    [array addObject:dict];
                }
                if ([action getSubActionCount] > 0)
                    action = [action getSubAction:0];
                else
                    action = nil;
            }
        }
        @synchronized(_dictAnnotLink) {
            [_dictAnnotLink setObject:array ? array : [NSNull null] forKey:[NSNumber numberWithInt:[dmpage getIndex]]];
        }
    }
}

- (void)reloadAnnotLink:(FSPDFPage *)dmpage {
    @synchronized(self) {
        @synchronized(_dictAnnotLink) {
            [_dictAnnotLink removeObjectForKey:[NSNumber numberWithInt:[dmpage getIndex]]];
        }
        [self loadAnnotLink:dmpage];
    }
}

- (NSArray *)getAnnotationQuad:(FSAnnot *)annot {
    if ([annot getType] != FSAnnotLink)
        return nil;
    FSLink *link = [[FSLink alloc] initWithAnnot:annot];
    FSQuadPointsArray *quads = [link getQuadPoints];
    unsigned long quadCount = [quads getSize];
    if (quadCount == 0) {
        return nil;
    }

    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < quadCount; i++) {
        FSQuadPoints *quadPoints = [quads getAt:i];
        if (!quadPoints) {
            break;
        }

        CGPoint point1;
        point1.x = [[quadPoints getFirst] getX];
        point1.y = [[quadPoints getFirst] getY];
        NSValue *value1 = [NSValue valueWithCGPoint:point1];
        CGPoint point2;
        point2.x = [[quadPoints getSecond] getX];
        point2.y = [[quadPoints getSecond] getY];
        NSValue *value2 = [NSValue valueWithCGPoint:point2];
        CGPoint point3;
        point3.x = [[quadPoints getThird] getX];
        point3.y = [[quadPoints getThird] getY];
        NSValue *value3 = [NSValue valueWithCGPoint:point3];
        CGPoint point4;
        point4.x = [[quadPoints getFourth] getX];
        point4.y = [[quadPoints getFourth] getY];
        NSValue *value4 = [NSValue valueWithCGPoint:point4];
        NSArray *arrayQuad = [NSArray arrayWithObjects:value1, value2, value3, value4, nil];
        [array addObject:arrayQuad];
    }

    return array;
}

#pragma mark IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    //    if(_jumpDict){
    //        [self jumpToPageWithDict:_jumpDict];
    //        _jumpDict = nil;
    //    }
}
- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
    [self.dictAnnotLink removeAllObjects];
}

#pragma mark IPageEventListener
- (void)onPagesWillRemove:(NSArray<NSNumber *> *)indexes {
}

- (void)onPagesWillMove:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
}

- (void)onPagesWillRotate:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    [self.dictAnnotLink removeAllObjects];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    [self.dictAnnotLink removeAllObjects];
}

- (void)onPagesRotated:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    [self.dictAnnotLink removeAllObjects];
}

@end
