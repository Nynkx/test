/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ShapeUtil.h"
#import "FSAnnotExtent.h"

@implementation ShapeUtil

+ (NSArray *)getMovePointInRect:(CGRect)rect {
    if (rect.size.width == 0 || rect.size.height == 0) {
        return nil;
    }

    UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    CGFloat dragWidth = dragDot.size.width;
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - dragWidth / 2.0, rect.origin.y - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - dragWidth / 2.0, rect.origin.y + rect.size.height / 2.0 - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - dragWidth / 2.0, rect.origin.y + rect.size.height - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width / 2.0 - dragWidth / 2.0, rect.origin.y - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width / 2.0 - dragWidth / 2.0, rect.origin.y + rect.size.height - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - dragWidth / 2.0, rect.origin.y - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - dragWidth / 2.0, rect.origin.y + rect.size.height / 2.0 - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - dragWidth / 2.0, rect.origin.y + rect.size.height - dragWidth / 2.0, dragWidth, dragWidth)]];
    return array;
}

+ (NSArray *)getCornerMovePointInRect:(CGRect)rect {
    if (rect.size.width == 0 || rect.size.height == 0) {
        return nil;
    }

    UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    CGFloat dragWidth = dragDot.size.width;
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - dragWidth / 2.0, rect.origin.y - dragWidth / 2.0, dragWidth, dragWidth)]];
    //    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - dragWidth / 2.0, rect.origin.y + rect.size.height / 2.0 - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - dragWidth / 2.0, rect.origin.y + rect.size.height - dragWidth / 2.0, dragWidth, dragWidth)]];
    //    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width / 2.0 - dragWidth / 2.0, rect.origin.y - dragWidth / 2.0, dragWidth, dragWidth)]];
    //    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width / 2.0 - dragWidth / 2.0, rect.origin.y + rect.size.height - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - dragWidth / 2.0, rect.origin.y - dragWidth / 2.0, dragWidth, dragWidth)]];
    //    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - dragWidth / 2.0, rect.origin.y + rect.size.height / 2.0 - dragWidth / 2.0, dragWidth, dragWidth)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - dragWidth / 2.0, rect.origin.y + rect.size.height - dragWidth / 2.0, dragWidth, dragWidth)]];
    return array;
}

+ (FSAnnotEditType)getEditTypeWithPoint:(CGPoint)point rect:(CGRect)rect defaultEditType:(FSAnnotEditType)defaultEditType {
    CGFloat left = rect.origin.x;
    CGFloat top = rect.origin.y;
    CGFloat w = rect.size.width;
    CGFloat h = rect.size.height;
    NSArray *pointArray = [NSArray arrayWithObjects:
                                       [NSValue valueWithCGPoint:CGPointMake(left, top)],
                                       [NSValue valueWithCGPoint:CGPointMake(left, top + h / 2.0)],
                                       [NSValue valueWithCGPoint:CGPointMake(left, top + h)],
                                       [NSValue valueWithCGPoint:CGPointMake(left + w / 2.0, top)],
                                       [NSValue valueWithCGPoint:CGPointMake(left + w / 2.0, top + h)],
                                       [NSValue valueWithCGPoint:CGPointMake(left + w, top)],
                                       [NSValue valueWithCGPoint:CGPointMake(left + w, top + h / 2.0)],
                                       [NSValue valueWithCGPoint:CGPointMake(left + w, top + h)],
                                       [NSValue valueWithCGPoint:CGPointMake(left + w / 2.0, top + h / 2.0)],
                                       nil];
    __block NSUInteger minIdx;
    __block CGFloat minDiff = FLT_MAX;
    [pointArray enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        CGPoint pt = [obj CGPointValue];
        CGFloat diff = ABS(pt.x - point.x) + ABS(pt.y - point.y);
        if (diff < minDiff) {
            minIdx = idx;
            minDiff = diff;
        }
    }];
    const CGFloat tolerance = 25.0f;
    CGPoint closestPoint = [pointArray[minIdx] CGPointValue];
    if ([self isPoint:point closeToPoint:closestPoint tolerance:tolerance]) {
        return (FSAnnotEditType) minIdx;
    }
    return defaultEditType;
}

+ (FSAnnotEditType)getAnnotEditTypeWithPoint:(CGPoint)point rect:(CGRect)rect keepAspectRatio:(BOOL)keepAspectRatio defaultEditType:(FSAnnotEditType)defaultEditType {
    FSAnnotEditType editType = (FSAnnotEditType) [ShapeUtil getEditTypeWithPoint:point rect:rect defaultEditType:(FSAnnotEditType) defaultEditType];
    // if keep aspect ratio, the 4 middle dots are not drawn
    if (keepAspectRatio) {
        if (editType == FSAnnotEditTypeMiddleTop ||
            editType == FSAnnotEditTypeMiddleBottom ||
            editType == FSAnnotEditTypeLeftMiddle ||
            editType == FSAnnotEditTypeRightMiddle) {
            editType = FSAnnotEditTypeFull;
        }
    }
    return editType;
}

+ (NSInteger)getFillSignEditTypeWithPoint:(CGPoint)point rect:(CGRect)rect keepAspectRatio:(BOOL)keepAspectRatio defaultEditType:(NSInteger)defaultEditType {
    NSInteger editType = (NSInteger) [ShapeUtil getEditTypeWithPoint:point rect:rect defaultEditType:(NSInteger) defaultEditType];
    // if keep aspect ratio, the 4 middle dots are not drawn
    if (keepAspectRatio) {
        if (editType == FSAnnotEditTypeMiddleTop ||
            editType == FSAnnotEditTypeMiddleBottom ||
            editType == FSAnnotEditTypeLeftMiddle ||
            editType == FSAnnotEditTypeRightMiddle) {
            editType = FSAnnotEditTypeFull;
        }
    }
    return editType;
}

+ (FSAnnotEditType)getLineEditTypeWithPoint:(CGPoint)point line:(FSLine *_Nonnull)line pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    int pageIndex = line.pageIndex;
    CGPoint startPoint = [pdfViewCtrl convertPdfPtToPageViewPt:[line getStartPoint] pageIndex:pageIndex];
    CGPoint endPoint = [pdfViewCtrl convertPdfPtToPageViewPt:[line getEndPoint] pageIndex:pageIndex];
    BOOL isStartPointCloser = (ABS(startPoint.x - point.x) + ABS(startPoint.y - point.y)) < (ABS(endPoint.x - point.x) + ABS(endPoint.y - point.y));
    if (isStartPointCloser) {
        if ([ShapeUtil isPoint:startPoint closeToPoint:point tolerance:25.0f]) {
            return FSAnnotEditTypeStartPoint;
        }
    } else {
        if ([ShapeUtil isPoint:endPoint closeToPoint:point tolerance:25.0f]) {
            return FSAnnotEditTypeEndPoint;
        }
    }
    return FSAnnotEditTypeFull;
}

+ (BOOL)isPoint:(CGPoint)p1 closeToPoint:(CGPoint)p2 tolerance:(CGFloat)tolerance {
    CGFloat dx = p1.x - p2.x;
    CGFloat dy = p1.y - p2.y;
    return (dx * dx + dy * dy) < tolerance * tolerance;
}

+ (NSArray *)getCornerPointOfRect:(CGRect)rect {
    if (rect.size.width == 0 || rect.size.height == 0) {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x, rect.origin.y)]];
    [array addObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x , rect.origin.y + rect.size.height)]];
    [array addObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)]];
    [array addObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)]];
    return array;
}

@end
