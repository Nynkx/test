/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ShapeAnnotHandler.h"

@implementation ShapeAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.pageEdgeInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
        self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-30, -30, -30, -30);
        self.annotBorderEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotCircle;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
    return options;
}

- (void)showMenu {
    if (!self.annot) {
        return;
    }
    int pageIndex = self.annot.pageIndex;
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
    CGRect dvRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:pageIndex];
    [self.extensionsManager.menuControl setRect:dvRect margin:20]; // enlarge distance from menu to annot
    [self.extensionsManager.menuControl showMenu];
}

- (void)showStyle {
    [self.extensionsManager.propertyBar setColors:@[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff]];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:annot.lineWidth];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];

    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:@[ [self.pdfViewCtrl getDisplayView] ]];
}

- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot {
    CGSize minSize = [super minSizeForResizingAnnot:annot];
    if ([annot getType] == FSAnnotSquare) {
        FSSquare *square = [[FSSquare alloc] initWithAnnot:annot];
        FSBorderInfo *borderInfo = [square getBorderInfo];
        if ([borderInfo getStyle] == FSBorderInfoCloudy) {
            int pageIndex = square.pageIndex;
            CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[square getRect] pageIndex:pageIndex];
            CGRect innerRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[square getInnerRect] pageIndex:pageIndex];
            const CGFloat minInnerWidth = 5.0;
            const CGFloat minInnerHeight = 5.0;
            minSize.width = MAX(minSize.width, rect.size.width + minInnerWidth - innerRect.size.width);
            minSize.height = MAX(minSize.height, rect.size.height + minInnerHeight - innerRect.size.height);
        }
    }
    return minSize;
}

@end
