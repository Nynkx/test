/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "Const.h"
#import <Foundation/Foundation.h>
#import <FoxitRDK/FSPDFViewControl.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FSAnnotEditType) {
    FSAnnotEditTypeUnknown = -1,
    FSAnnotEditTypeLeftTop = 0,
    FSAnnotEditTypeLeftMiddle,
    FSAnnotEditTypeLeftBottom,
    FSAnnotEditTypeMiddleTop,
    FSAnnotEditTypeMiddleBottom,
    FSAnnotEditTypeRightTop,
    FSAnnotEditTypeRightMiddle,
    FSAnnotEditTypeRightBottom,
    FSAnnotEditTypeFull,
    FSAnnotEditTypeStartPoint, // for line annot
    FSAnnotEditTypeEndPoint,   // for line annot
};

NS_ASSUME_NONNULL_BEGIN

@interface ShapeUtil : NSObject

+ (NSArray *)getMovePointInRect:(CGRect)rect;
+ (NSArray *)getCornerMovePointInRect:(CGRect)rect;
+ (NSArray *)getCornerPointOfRect:(CGRect)rect;

+ (FSAnnotEditType)getEditTypeWithPoint:(CGPoint)point rect:(CGRect)rect defaultEditType:(FSAnnotEditType)defaultEditType;
+ (FSAnnotEditType)getAnnotEditTypeWithPoint:(CGPoint)point rect:(CGRect)rect keepAspectRatio:(BOOL)keepAspectRatio defaultEditType:(FSAnnotEditType)defaultEditType;
+ (FSAnnotEditType)getLineEditTypeWithPoint:(CGPoint)point line:(FSLine *)line pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl;

+ (NSInteger)getFillSignEditTypeWithPoint:(CGPoint)point rect:(CGRect)rect keepAspectRatio:(BOOL)keepAspectRatio defaultEditType:(NSInteger)defaultEditType;
@end

NS_ASSUME_NONNULL_END
