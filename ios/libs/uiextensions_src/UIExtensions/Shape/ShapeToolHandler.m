/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ShapeToolHandler.h"
#import "FSAnnotExtent.h"
#import "Utility.h"
#import "PaintUtility.h"

@interface ShapeToolHandler ()

@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;

@property (nonatomic, assign) BOOL isEndDrawing;
@property (nonatomic, assign) int currentPageIndex;

@property (nonatomic, strong) PaintUtility *paintUtility;
@property (nonatomic, strong) NSMutableDictionary *tempPointsDict;

@end

@implementation ShapeToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotCircle;
        self.tempPointsDict = @{}.mutableCopy;
    }
    return self;
}

- (NSString *)getName {
    return Tool_Shape;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
    _isEndDrawing = NO;
    self.annot = nil;
    
    self.tempPointsDict = @{}.mutableCopy;
}

- (void)onDeactivate {
    _isEndDrawing = NO;
    if (self.annot) {
        FSAnnot *annot = self.annot;
        [_pdfViewCtrl lockRefresh];
        [self updateAnnotRect];
        [annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        [_pdfViewCtrl refresh:self.annot.pageIndex];
    }
    self.annot = nil;
    [self.tempPointsDict removeAllObjects];
    self.tempPointsDict = nil;
}

-(void) updateAnnotRect {
    if(self.startPoint && self.endPoint)
        self.annot.fsrect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

#define DEFAULT_RECT_WIDTH 200

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    self.currentPageIndex = pageIndex;
    _isEndDrawing = NO;
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];

    float defaultRectWidth = [Utility convertWidth:DEFAULT_RECT_WIDTH fromPageViewToPDF:_pdfViewCtrl pageIndex:pageIndex];
    CGPoint startPoint = CGPointMake(point.x - defaultRectWidth / 2, point.y + defaultRectWidth / 2);
    CGPoint endPoint = CGPointMake(point.x + defaultRectWidth / 2, point.y - defaultRectWidth / 2);

    self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:startPoint pageIndex:pageIndex];
    self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:endPoint pageIndex:pageIndex];
    FSRectF *dibRect = nil;

    dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
    float marginX = [Utility getAnnotMinXMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
    float marginY = [Utility getAnnotMinYMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    float pdfWidth = [page getWidth];
    float pdfHeight = [page getHeight];

    if (pdfHeight - dibRect.top < 0) {
        dibRect.bottom += pdfHeight - dibRect.top - marginY;
        dibRect.top = pdfHeight - marginY;
    }
    if (dibRect.bottom < 0) {
        dibRect.top += -dibRect.bottom + marginY;
        dibRect.bottom = marginY;
    }
    if (pdfWidth - dibRect.right < 0) {
        dibRect.left += pdfWidth - dibRect.right - marginX;
        dibRect.right = pdfWidth - marginX;
    }
    if (dibRect.left < 0) {
        dibRect.right += -dibRect.left + marginX;
        dibRect.left = marginX;
    }

    FSAnnot *annot = [self addAnnotToPage:pageIndex withRect:dibRect];
    if (!annot || [annot isEmpty]) {
        return YES;
    }
    [_pdfViewCtrl lockRefresh];
    [annot resetAppearanceStream];
    [_pdfViewCtrl unlockRefresh];

    id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
    @try {
        [annotHandler addAnnot:annot addUndo:YES];
    } @catch (NSException *exception) {
    } @finally {
    }

    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    
    [self.tempPointsDict setObject:@(point) forKey:@(pageIndex)];
    
//    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
//    CGRect rect = [pageView frame];
//    CGSize size = rect.size;
//    if (point.x > size.width || point.y > size.height || point.x < 0 || point.y < 0)
//        return NO;

    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSRectF *dibRect = nil;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.currentPageIndex = pageIndex;
        _isEndDrawing = NO;
        
        dibRect = [[FSRectF alloc] init];
        [dibRect setLeft:dibPoint.x];
        [dibRect setBottom:dibPoint.y];
        [dibRect setRight:dibPoint.x + 0.1];
        [dibRect setTop:dibPoint.y + 0.1];
        if (self.annot) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
            @try {
                [self updateAnnotRect];
                [annotHandler addAnnot:self.annot addUndo:YES];
            } @catch (NSException *exception) {
            } @finally {
                self.annot = nil;
            }
        }
        self.annot = [self addAnnotToPage:pageIndex withRect:dibRect];
        if (!self.annot) {
            return YES;
        }
        
        self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.endPoint   = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        
        FSRectF *lineFSRect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:self.annot.lineWidth top1:self.annot.lineWidth];
        CGRect lineRect = [_pdfViewCtrl convertPdfRectToPageViewRect:lineFSRect pageIndex:pageIndex];
        float lineWidth = lineRect.size.width;
        
        self.paintUtility = [[PaintUtility alloc] init];
        self.paintUtility.annotLineWidth = lineWidth;
        self.paintUtility.annotColor = self.annot.color;
        self.paintUtility.annotOpacity = self.annot.opacity;
        
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:pageIndex];
        rect = CGRectInset(rect, -10, -10);
        [_pdfViewCtrl refresh:pageIndex];

    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        if (pageIndex != self.annot.pageIndex) {
            point = [self.tempPointsDict[@(self.annot.pageIndex)] CGPointValue];
            pageIndex = self.annot.pageIndex;
        }
        
        float marginX = [Utility getAnnotMinXMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];
        float marginY = [Utility getAnnotMinYMarginInPDF:_pdfViewCtrl pageIndex:pageIndex];

        UIView *pageview = [_pdfViewCtrl getPageView:pageIndex];
        
        if (point.y < marginY) {
            point.y = marginY;
        }
        
        if (point.y > pageview.bounds.size.height - marginY) {
            point.y = pageview.bounds.size.height - marginY;
        }
        
        if (point.x < marginX) {
            point.x = marginX;
        }
        
        if (point.x > pageview.bounds.size.width - marginX) {
            point.x = pageview.bounds.size.width - marginX;
        }
        
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        
        [_pdfViewCtrl lockRefresh];
        self.annot.fsrect = dibRect;
        [_pdfViewCtrl unlockRefresh];
        
        if (self.type == FSAnnotSquare) {
            if (self.annot.fsrect.top - self.annot.fsrect.bottom < 5.0 || self.annot.fsrect.right - self.annot.fsrect.left < 5.0) {
                FSAnnot* annot = self.annot;
                BOOL result = [self removeAnnot:annot];
                if (result) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self->_pdfViewCtrl refresh:pageIndex];
                    });
                }
                
                return NO;
            }
        }
        
        if (self.annot) {
            [_pdfViewCtrl lockRefresh];
            [self.annot resetAppearanceStream];
            [_pdfViewCtrl unlockRefresh];
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
            @try {
                [annotHandler addAnnot:self.annot addUndo:YES];
            } @catch (NSException *exception) {
            } @finally {
                self.annot = nil;
            }
        }
        _isEndDrawing = YES;
        return YES;
    }
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)removeAnnot:(FSAnnot *)annot {
    FSPDFPage *page = [annot getPage];
    BOOL result = [page removeAnnot:annot];
    
    self.annot = nil;
    return result;
}

- (FSAnnot *)addAnnotToPage:(int)pageIndex withRect:(FSRectF *)rect {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;

    FSAnnot *annot = nil;
    @try {
        annot = [page addAnnot:self.type rect:rect];
    } @catch (NSException *exception) {
    } @finally {
    }
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.color = [_extensionsManager getPropertyBarSettingColor:self.type];
    annot.opacity = [_extensionsManager getAnnotOpacity:self.type] / 100.0f;
    annot.lineWidth = [_extensionsManager getAnnotLineWidth:self.type];
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    if (self.type == FSAnnotCircle) {
        annot.subject = @"Circle";
    } else if (self.type == FSAnnotSquare) {
        annot.subject = @"Rectangle";
    }
    annot.flags = FSAnnotFlagPrint;
    return annot;
}

#pragma mark draw by ios
- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_isEndDrawing) {
        return ;
    }
    if (self.annot != nil && ![self.annot isEmpty] && self.currentPageIndex == pageIndex) {
        CGPoint startPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint pageIndex:pageIndex];
        CGPoint endPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.endPoint pageIndex:pageIndex];
        
        CGRect rect = [self getRectFromStartPoint:startPoint endPoint:endPoint];
        [self.paintUtility drawShapWithRect:rect type:self.type inContext:context];
    }
}

- (CGRect)getRectFromStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CGFloat xminValue = MIN(startPoint.x, endPoint.x);
    CGFloat yminValue = MIN(startPoint.y, endPoint.y);
    
    CGFloat xmaxValue = MAX(startPoint.x, endPoint.x);
    CGFloat ymaxValue = MAX(startPoint.y, endPoint.y);
    
    CGRect result = CGRectMake(xminValue, yminValue, fabs(xmaxValue-xminValue), fabs(ymaxValue-yminValue));
    return result;
}
@end
