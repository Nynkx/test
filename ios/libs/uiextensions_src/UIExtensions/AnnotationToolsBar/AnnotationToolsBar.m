/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AnnotationToolsBar.h"

const int BOTTOM_NOTE_TAG = 100;
const int BOTTOM_FREE_WRITER_TAG = 110;
const int BOTTOM_HIGHLIGHT_TAG = 120;
const int BOTTOM_LINE_TAG = 130;
const int BOTTOM_PENCIL_TAG = 140;
const int BOTTOM_DISTANCE_TAG = 150;
const int BOTTOM_MULTI_SELECT_TAG = 160;
const int BOTTOM_ITEM_TAG_MULTIPLE = 10;

@interface UIButton (ChangeImage)
- (void)changeNeedImage:(UIImage *)image;
@end
@interface AnnotationToolsBar (){
    UIButton *_lastSelectedBtn;
}

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *leftContentView;
@property (nonatomic, strong) UIView *divideView;
@property (nonatomic, strong) UIView *rightContentView;
@property (nonatomic, strong) NSMutableArray *bottomBtns;

@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@end

@implementation AnnotationToolsBar

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager{
    self = [super init];
    if (self) {
        self.extensionsManager = extensionsManager;
        self.contentView = [[UIView alloc] init];
        
        self.leftContentView = [[UIView alloc] init];
        self.leftContentView.backgroundColor = self.contentView.backgroundColor;
        self.rightContentView = [[UIView alloc] init];
        self.rightContentView.backgroundColor = self.contentView.backgroundColor;
        
        self.divideView = [[UIView alloc] init];
        self.divideView.backgroundColor = DividingLineColor;
        
        [self.contentView addSubview:self.leftContentView];
        [self.contentView addSubview:self.divideView];
        [self.contentView addSubview:self.rightContentView];
        
        self.bottomBtns = [NSMutableArray arrayWithCapacity:7];

        NSArray *bottomBtnImages = @[ @"annot_note", @"annot_freetext", @"annot_highlight", @"annot_line", @"annot_pencil", @"annot_distance_tool" , @"annot_multiple_select"];
        
        NSSet<NSString *> *tools = self.extensionsManager.config.tools;
        for (int i = 0;  i < bottomBtnImages.count; i ++) {
            UIImage *itemImage = ImageNamed(bottomBtnImages[i]);
            int tag = BOTTOM_ITEM_TAG_MULTIPLE * BOTTOM_ITEM_TAG_MULTIPLE + i * BOTTOM_ITEM_TAG_MULTIPLE;
            
            NSArray *bottomItems = [self bottomItemsWithTag:tag];
            BOOL needBackground = bottomItems.count > 1;
            
            MenuItem *firstMenuItem = [bottomItems firstObject];
            if (firstMenuItem) {
                if (![firstMenuItem.title isEqualToString:FSLocalizedForKey(@"kHighlight")]) {
                    itemImage = firstMenuItem.image;
                }
                tag = firstMenuItem.tag;
            }
            UIButton *btn = [AnnotationToolsBar createItemWithImage:itemImage imageSelected:itemImage imageDisable:itemImage background:needBackground ? ImageNamed(@"lower_right_trianglegb") : nil];
            btn.tag = tag;
            [btn addTarget:self action:@selector(onTapClicked:) forControlEvents:UIControlEventTouchUpInside];
            if (i != bottomBtnImages.count - 1) {
                if (i < 5) {
                    btn.enabled = bottomItems.count;
                    if (needBackground) {
                        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressClicked:)];
                        longPress.minimumPressDuration = 0.8; //press time
                        [btn addGestureRecognizer:longPress];
                    }
                }else{
                    btn.enabled  = [tools containsObject:Tool_Distance];
                }
                [self.leftContentView addSubview:btn];
            }else{
                btn.enabled  = [tools containsObject:Tool_Multiple_Selection];
                [self.rightContentView addSubview:btn];
            }
            [_bottomBtns addObject:btn];
        }
        
        [extensionsManager.editBar addSubview:self.contentView];
        [self refreshLayout];
    }
    return self;
}

- (void)refreshLayout {
    
    __block int needViewCount = 0;
    [_bottomBtns enumerateObjectsUsingBlock:^(UIButton  *_Nonnull btn, NSUInteger idx, BOOL * _Nonnull stop) {
        btn.hidden = !btn.enabled;
        if (!btn.hidden) {
            needViewCount ++;
        }
    }];
    
    self.rightContentView.hidden = NO;
    [self.rightContentView.subviews enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull btn, NSUInteger idx, BOOL * _Nonnull stop) {
        if (!btn.enabled) {
            self.rightContentView.hidden = !btn.enabled;
            *stop = YES;
        }
    }];
    
    UIView *pdfView = self.extensionsManager.pdfViewCtrl;
    CGFloat minWidth = MIN(pdfView.fs_width, pdfView.fs_height);
    if (DEVICE_iPAD) minWidth = IPAD_PREFERENCE_BAR_WIDTH;
    CGFloat needMinWidth = minWidth / _bottomBtns.count * needViewCount;
    [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView.superview);
        make.centerX.equalTo(self.contentView.superview);
        if (DEVICE_iPHONE) {
            make.left.right.equalTo(self.contentView.superview).priority(MASLayoutPriorityDefaultMedium);
            make.width.lessThanOrEqualTo(@(needMinWidth));
        }else{
            make.width.equalTo(@(needMinWidth));
        }
    }];
    
    [self.leftContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.height.equalTo(self.contentView);
        if (self.rightContentView.hidden) make.right.equalTo(self.contentView);
    }];
    
    if (!self.rightContentView.hidden) {
        [self.divideView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.leftContentView.mas_right);
            make.right.equalTo(self.rightContentView.mas_left);
            make.top.equalTo(self.contentView).offset(10);
            make.bottom.equalTo(self.contentView).offset(-10);
            make.width.mas_equalTo(DIVIDE_VIEW_THICKNES);
        }];
    }
    
    UIButton *lastBtn = nil;
    int count = (int)self.leftContentView.subviews.count;
    for (int i = 0; i < count; i ++) {
        UIButton *btn = self.leftContentView.subviews[i];
        if (!btn.hidden) {
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.equalTo(self.leftContentView);
                if (lastBtn) {
                    make.width.mas_equalTo(lastBtn);
                }
                make.left.mas_equalTo(lastBtn ? lastBtn.mas_right : self.leftContentView.mas_left);
                make.centerY.equalTo(self.leftContentView);
            }];
            lastBtn = btn;
        }
    }
    
    [lastBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.leftContentView);
    }];
    
    if (!self.rightContentView.hidden) {
        [self.rightContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.top.height.equalTo(self.contentView);
            if (lastBtn) {
                make.width.equalTo(lastBtn);
            }else{
                make.left.equalTo(self.contentView);
            }
        }];
        
        [self.rightContentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull btn, NSUInteger idx, BOOL * _Nonnull stop) {
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.left.right.equalTo(btn.superview);
            }];
        }];
    }
}

- (void)onTapClicked:(UIButton *)btn{
    if (_lastSelectedBtn != btn) {
        _lastSelectedBtn.selected = NO;
        btn.selected = YES;
        _lastSelectedBtn = btn;
    }

    int tag = (int)btn.tag;
    int index = 0;
    if (btn.tag >= BOTTOM_NOTE_TAG && btn.tag < BOTTOM_FREE_WRITER_TAG){
        index = tag - BOTTOM_NOTE_TAG;
        if (index == 0) {
            !self.noteClicked?:self.noteClicked();
        }else if (index == 1){
            !self.attachmentClicked?:self.attachmentClicked();
        }else if (index == 2){
            !self.stampClicked?:self.stampClicked();
        }else if (index == 3){
            !self.imageClicked?:self.imageClicked();
        }else if (index == 4){
            !self.audioClicked?:self.audioClicked();
        }else if (index == 5){
            !self.videoClicked?:self.videoClicked();
        }
    }else if(btn.tag >= BOTTOM_FREE_WRITER_TAG && btn.tag < BOTTOM_HIGHLIGHT_TAG){
        index = tag - BOTTOM_FREE_WRITER_TAG;
        if (index == 0) {
            !self.typerwriterClicked?:self.typerwriterClicked();
        }else if (index == 1){
            !self.calloutClicked?:self.calloutClicked();
        }else if (index == 2){
            !self.textboxClicked?:self.textboxClicked();
        }
    }else if(btn.tag >= BOTTOM_HIGHLIGHT_TAG && btn.tag < BOTTOM_LINE_TAG){
        index = tag - BOTTOM_HIGHLIGHT_TAG;
        if (index == 0) {
            !self.highLightClicked?:self.highLightClicked();
        }else if (index == 1){
            !self.underLineClicked?:self.underLineClicked();
        }else if (index == 2){
            !self.breakLineClicked?:self.breakLineClicked();
        }else if (index == 3){
            !self.strikeOutClicked?:self.strikeOutClicked();
        }else if (index == 4){
            !self.insertClicked?:self.insertClicked();
        }else if (index == 5){
            !self.replaceClicked?:self.replaceClicked();
        }
    }else if(btn.tag >= BOTTOM_LINE_TAG && btn.tag < BOTTOM_PENCIL_TAG){
        index = tag - BOTTOM_LINE_TAG;
        if (index == 0) {
            !self.lineClicked?:self.lineClicked();
        }else if (index == 1){
            !self.arrowsClicked?:self.arrowsClicked();
        }else if (index == 2){
            !self.polyLineClicked?:self.polyLineClicked();
        }else if (index == 3){
            !self.rectClicked?:self.rectClicked();
        }else if (index == 4){
            !self.circleClicked?:self.circleClicked();
        }else if (index == 5){
            !self.polygonClicked?:self.polygonClicked();
        }else if (index == 6){
            !self.cloudClicked?:self.cloudClicked();
        }
    }else if(btn.tag >= BOTTOM_PENCIL_TAG && btn.tag < BOTTOM_DISTANCE_TAG){
        index = tag - BOTTOM_PENCIL_TAG;
        if (index == 0) {
            !self.pencileClicked?:self.pencileClicked();
        }else if (index == 1){
            !self.eraserClicked?:self.eraserClicked();
        }
    }else if(btn.tag == BOTTOM_DISTANCE_TAG){
        !self.distanceClicked?:self.distanceClicked();
    }else if (btn.tag == BOTTOM_MULTI_SELECT_TAG){
        !self.multipleSelectClicked?:self.multipleSelectClicked();
    }
}

- (void)onLongPressClicked:(UILongPressGestureRecognizer *)recognizer{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        UIView *btn = recognizer.view;
        self.extensionsManager.menuControl.menuItems = [self bottomItemsWithTag:(int)(btn.tag / BOTTOM_ITEM_TAG_MULTIPLE) * BOTTOM_ITEM_TAG_MULTIPLE];
        [self.extensionsManager.menuControl setRect:[self.leftContentView convertRect:btn.frame toView:self.extensionsManager.pdfViewCtrl]];
        [self.extensionsManager.menuControl showMenu];
    }
}

- (NSArray<MenuItem *> *)bottomItemsWithTag:(int)tag {
    UIExtensionsConfig *config = self.extensionsManager.config;
    NSSet<NSString *> *tools = config.tools;
    NSMutableArray<MenuItem *> *items = @[].mutableCopy;
    NSArray *bottomToolNames = nil;
    if (tag == BOTTOM_NOTE_TAG) {
        bottomToolNames = @[ Tool_Note, Tool_Attachment, Tool_Stamp, Tool_Image, Tool_Audio, Tool_Video ];
    }else if (tag == BOTTOM_FREE_WRITER_TAG){
        bottomToolNames = @[ Tool_Freetext, Tool_Callout, Tool_Textbox ];
    }else if (tag == BOTTOM_HIGHLIGHT_TAG){
        bottomToolNames = @[ Tool_Highlight, Tool_Underline, Tool_Squiggly, Tool_Strikeout, Tool_Insert, Tool_Replace ];
    }else if (tag == BOTTOM_LINE_TAG){
        bottomToolNames = @[ Tool_Line, Tool_Arrow, Tool_PolyLine, Tool_Rectangle, Tool_Oval, Tool_Polygon, Tool_Cloud ];
    }else if (tag == BOTTOM_PENCIL_TAG){
        bottomToolNames = @[ Tool_Pencil, Tool_Eraser ];
    }
    for (int i = 0; i < bottomToolNames.count; i ++) {
        NSString *bottomToolName = bottomToolNames[i];
        BOOL canAdd = NO;
        if ([tools containsObject:bottomToolName]) canAdd = YES;
        else if (config.loadAttachment && [bottomToolName isEqualToString:Tool_Attachment]) canAdd = YES;
        if(canAdd){
            NSString *imageName = [NSString stringWithFormat:@"annot_%@",[bottomToolName lowercaseString]];
            if ([bottomToolName isEqualToString:Tool_Highlight]) imageName = @"annot_high_white";
            NSString *localizedToolName = [NSString stringWithFormat:@"k%@",bottomToolName];
            if ([localizedToolName isEqualToString:@"kOval"] ) localizedToolName = @"kCircle";
            if ([localizedToolName isEqualToString:@"kFreetext"] ) localizedToolName = @"kTypewriter";
            if ([localizedToolName isEqualToString:@"kInsert"] ) localizedToolName = @"kInsertText";
            if ([localizedToolName isEqualToString:@"kReplace"] ) localizedToolName = @"kReplaceText";
            MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(localizedToolName) image:ImageNamed(imageName) object:self action:@selector(onItemClicked:)];
            item.tag = tag + i;
            [items addObject:item];
        }
    }
    return items.copy;
}
                                   
- (void)onItemClicked:(MenuItem *)item{
    int index = item.tag / BOTTOM_ITEM_TAG_MULTIPLE - BOTTOM_ITEM_TAG_MULTIPLE;
    if (index > 6 || index < 0) return;
    UIButton *btn = _bottomBtns[item.tag / BOTTOM_ITEM_TAG_MULTIPLE - BOTTOM_ITEM_TAG_MULTIPLE];
    btn.tag = item.tag;
    [btn changeNeedImage:[item.title isEqualToString:FSLocalizedForKey(@"kHighlight")] ? ImageNamed(@"annot_highlight") : item.image];
    [self onTapClicked:btn];
}

+ (UIButton *)createItemWithImage:(UIImage *)imageNormal
                      imageSelected:(UIImage *)imageSelected
                       imageDisable:(UIImage *)imageDisabled
                         background:(UIImage *)background {
    float width = imageNormal.size.width;
    float height = imageNormal.size.height;
    UIImage *backgroundSelected = [Utility imageByApplyingAlpha:background alpha:0.5];
    if (background) {
        width = background.size.width;
        height = background.size.height;
    }
    CGRect buttonFrame = CGRectMake(0, 0, width, height);
    UIButton *button = [[UIButton alloc] initWithFrame:buttonFrame];
    button.contentMode = UIViewContentModeScaleAspectFit;
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    button.imageEdgeInsets = UIEdgeInsetsMake(-3, 0, 0, 0);
    if (background) {
        [button setBackgroundImage:background forState:UIControlStateNormal];
        [button setBackgroundImage:backgroundSelected forState:UIControlStateHighlighted];
    }
    [button setImage:imageNormal forState:UIControlStateNormal];
    [button setImage:[Utility imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateHighlighted];
    
    [button setImage:imageSelected forState:UIControlStateSelected];
    [button setImage:[Utility imageByApplyingAlpha:imageDisabled alpha:0.5] forState:UIControlStateDisabled];
    return button;
}

@end

@implementation UIButton (ChangeImage)

- (void)changeNeedImage:(UIImage *)image{
    [self setImage:image forState:UIControlStateNormal];
    [self setImage:[Utility imageByApplyingAlpha:image alpha:0.5] forState:UIControlStateHighlighted];
    
    [self setImage:image forState:UIControlStateSelected];
    [self setImage:[Utility imageByApplyingAlpha:image alpha:0.5] forState:UIControlStateDisabled];
}

@end
