/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MoreView.h"

@interface MoreMenuGroup ()

@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation MoreMenuGroup

- (id)init {
    if (self = [super init]) {
        _items = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSMutableArray *)getItems {
    return _items;
}

- (void)setItems:(NSMutableArray *)arr{
    _items = arr;
}

@end
