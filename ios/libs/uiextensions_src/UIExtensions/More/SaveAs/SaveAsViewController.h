/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import <UIKit/UIKit.h>

@class UIExtensionsManager;
NS_ASSUME_NONNULL_BEGIN
typedef void (^SaveAsCallBack) (NSError * _Nullable error, NSString * _Nullable savePath);
@interface SaveAsViewController : UIViewController
@property (nonatomic, copy) SaveAsCallBack saveAsCallBack;
@property (nonatomic, assign) BOOL isScanSave;
- (instancetype)init;
- (void)setPdfFilePath:(NSString *)pdfFilePath;
- (void)setUIExtensionsManager:(UIExtensionsManager *)extensionsManager;
@end

NS_ASSUME_NONNULL_END
