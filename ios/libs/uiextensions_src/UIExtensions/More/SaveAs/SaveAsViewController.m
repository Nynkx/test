/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SaveAsViewController.h"
#import "SelectStyleTableViewCell.h"
#import "PasswordModule.h"
#import "QualityViewController.h"

static NSString *const SelectStyleTableViewCellID = @"SelectStyleTableViewCell";
@interface SaveAsViewController ()<UITableViewDelegate, UITableViewDataSource, IDocEventListener>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *cancelBtn;
@property (nonatomic, weak) IBOutlet UIButton *okBtn;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, copy) NSArray *styleData;
@property (nonatomic, strong) SelectStyleObj *currentCellObj;
@property (nonatomic, copy) NSString *pdfFilePath;
@property (nonatomic, copy) NSString *pdfFileName;
@property (nonatomic, copy) NSString *saveAsPdfFilePath;
@property (nonatomic, assign) BOOL canFlattenCopy;
@property (nonatomic, assign) BOOL canOptimizeDoc;
@property (nonatomic, assign) FSImageSettingsImageCompressQuality colorCompressQuality;
@property (nonatomic, assign) FSMonoImageSettingsMonoImageCompressQuality monoCompressQuality;
@property (nonatomic, assign) BOOL isCompress;
@property (nonatomic, assign) unsigned long long beforeCompressSize;
@property (nonatomic, assign) unsigned long long afterCompressSize;
@property (nonatomic, assign) BOOL isScanTrial;
@end

@implementation SaveAsViewController

- (instancetype)init{
    return [super fs_storyBoardViewControllerWithName:@"MoreView" identifier:@"SaveAsViewController"];;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kSaveAS");
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.cancelBtn setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    [self.okBtn setTitle:FSLocalizedForKey(@"kOK") forState:UIControlStateNormal];
    
    [self creatstyleData];
    
    [self tableView:self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.backgroundColor = UIColor_DarkMode(self.tableView.backgroundColor, PageBackgroundColor_Dark);
    self.tableView.separatorColor = DividingLineColor;
    
    _colorCompressQuality = FSImageSettingsImageCompressQualityMedium;
    _monoCompressQuality = FSMonoImageSettingsImageCompressQualityLossless;
    _isCompress = NO;
    _canOptimizeDoc = YES;

}

- (void)tableView:(UITableView *)tableView selectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:0];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)creatstyleData{
    NSArray *tempArr = @[ FSLocalizedForKey(@"kOriginalDocument"), FSLocalizedForKey(@"kFlattenedCopy"), FSLocalizedForKey(@"kReducedFileSizeCopy")];
    NSMutableArray *styleData = @[].mutableCopy;
    for (int i = 0 ; i < tempArr.count; i++) {
        if (i == 1) {
            if (!self.canFlattenCopy) continue;
        }else if (i == 2){
            @try {
                if (![FSLibrary hasModuleLicenseRight:FSModuleNameOptimizer] || !self.canOptimizeDoc) continue;
            } @catch (NSException *exception) {
                continue;
            }
        }
        SelectStyleObj *obj = [[SelectStyleObj alloc] init];
        obj.styleText = tempArr[i];
        obj.selected = NO;
        obj.styleID = i;
        if (i == 0) {
            obj.selected = YES;
            self.currentCellObj = obj;
        }
        [styleData addObject:obj];
    }
    self.styleData = styleData.copy;
}

- (void)setUIExtensionsManager:(UIExtensionsManager *)extensionsManager{
    _extensionsManager = extensionsManager;
    _pdfViewCtrl = extensionsManager.pdfViewCtrl;
    self.pdfFilePath = [self.pdfViewCtrl filePath];
    FSPDFViewCtrl *pdfViewCtrl = self.pdfViewCtrl;
    BOOL docHasSignature = [Utility isDocumentSigned:[pdfViewCtrl currentDoc]];
    BOOL allowEdit = docHasSignature ? NO : [Utility canModifyContents:pdfViewCtrl];
    self.canFlattenCopy = !(!allowEdit || ![Utility canAddAnnot:pdfViewCtrl]);
    self.canOptimizeDoc = self.canFlattenCopy && ![[pdfViewCtrl currentDoc] isXFA] && ![Utility simpleCheckPDFA:[pdfViewCtrl currentDoc]];
}

- (void)setPdfFilePath:(NSString *)pdfFilePath{
    _pdfFilePath = pdfFilePath;
    if (pdfFilePath.length) {
        self.pdfFileName = [[pdfFilePath lastPathComponent] stringByDeletingPathExtension];
        self.saveAsPdfFilePath = [NSString stringWithFormat:@"%@/%@",DOCUMENT_PATH,self.pdfFileName];
    }
}

- (IBAction)clickedCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)clickedOK:(id)sender {
    if (self.currentCellObj.styleID == 2) {
        self.beforeCompressSize = [self pdfFileSize];
    }
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Select;
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        void (^saveAs)(NSString *) = ^(NSString *pdfFilePath){
            [controller dismissViewControllerAnimated:NO completion:nil];
            self.saveAsPdfFilePath = pdfFilePath;
            if (self.saveAsPdfFilePath.length) {
                    dispatch_semaphore_t wait = dispatch_semaphore_create(0);
                    void (^dismissViewAndOpenDoc)(void) = ^(){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self dismissViewControllerAnimated:YES completion:^{
                                if (self.extensionsManager) {
                                    void(^popCompressTipBlcok)(void) = ^(){
                                        if (self.beforeCompressSize > 0) {
                                            self.afterCompressSize = [self pdfFileSize];
                                            UIImageView *imageView = [[UIImageView alloc] initWithImage:ImageNamed(@"compression")];
                                            if (self.afterCompressSize > self.beforeCompressSize) {
                                                imageView = [[UIImageView alloc] initWithImage:ImageNamed(@"compression_ bigger")];
                                            }
                                            [self.extensionsManager.pdfViewCtrl fs_showHUDCustomView:imageView msg:[NSString stringWithFormat:FSLocalizedForKey(@"kOptimizeSizeTip"),[Utility displayFileSize:self.beforeCompressSize],[Utility displayFileSize:self.afterCompressSize]]];
                                        }
                                    };
                                    if ([self.extensionsManager.delegate respondsToSelector:@selector(uiextensionsManager:openNewDocAtPath:shouldCloseCurrentDoc:)]) {
                                        [self.extensionsManager.delegate uiextensionsManager:self.extensionsManager openNewDocAtPath:pdfFilePath shouldCloseCurrentDoc:YES];
                                        popCompressTipBlcok();
                                    } else {
                                        [self.extensionsManager.pdfViewCtrl openDoc:pdfFilePath
                                                                           password:self.extensionsManager.passwordModule.inputPassword
                                                                         completion:^(FSErrorCode error) {
                                            if (error != FSErrSuccess) {
                                                [self.extensionsManager.pdfViewCtrl closeDoc:nil];
                                                if (self.extensionsManager.goBack)
                                                    self.extensionsManager.goBack();
                                            }else{
                                                popCompressTipBlcok();
                                            }
                                        }];
                                    }
                                    self.extensionsManager.isDocModified = NO;
                                    self.extensionsManager.hiddenMoreMenu = YES;
                                }else{
                                    !self.saveAsCallBack ? : self.saveAsCallBack(nil, pdfFilePath);
                                }
                                dispatch_semaphore_signal(wait);
                            }];
                        });
                        dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
                    };

                    if (self.currentCellObj.styleID == 1) {
                        UIAlertController *alertController = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kWarning")
                                                  message:FSLocalizedForKey(@"kFlattenedCopyTip")
                                                  actions:@[FSLocalizedForKey(@"kCancel"),FSLocalizedForKey(@"kOK")]
                                               actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                                                   if (index == 1) {
                                                       [self.view fs_showHUDLoading:FSLocalizedForKey(@"kSaving") process:^{
                                                           FSXFADoc *xfaDoc = [self.pdfViewCtrl getXFADoc];
                                                           if (![xfaDoc isEmpty] && xfaDoc && [xfaDoc getType] == FSXFADocDynamic) {
                                                               [xfaDoc flattenTo:self.saveAsPdfFilePath];
                                                           }else{
                                                               FSPDFDoc *doc = [self.pdfViewCtrl currentDoc];
                                                               for (int i = 0 ; i < [doc getPageCount]; i ++){
                                                                   FSPDFPage *page = [doc getPage:i];
                                                                   [page flatten:YES options:FSPDFPageFlattenAll];
                                                               }
                                                               [self.pdfViewCtrl saveDoc:self.saveAsPdfFilePath flag:FSPDFDocSaveFlagNormal];
                                                           }
                                                           dismissViewAndOpenDoc();
                                                       }];
                                                   }
                        }];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }else{
                        void (^block) (void) = ^(){
                            if (self.pdfViewCtrl) {
                                if ([[self.pdfViewCtrl currentDoc] getSignatureCount] > 0) {
                                    [self.pdfViewCtrl saveDoc:self.saveAsPdfFilePath flag:FSPDFDocSaveFlagIncremental];
                                }else{
                                    [self.pdfViewCtrl saveDoc:self.saveAsPdfFilePath flag:FSPDFDocSaveFlagNormal];
                                }                            }
                            dismissViewAndOpenDoc();
                        };
                        if (self.currentCellObj.styleID == 2) {

                            [self.view fs_showHUDLoading:FSLocalizedForKey(@"kOptimizing") process:^{
                                
                                FSImageSettings *imageSettings = [[FSImageSettings alloc] init];
                                [imageSettings setQuality:self.colorCompressQuality];
                                [imageSettings setCompressionMode:FSImageSettingsImageCompressjpeg];
                                
                                FSMonoImageSettings *monoSettings = [[FSMonoImageSettings alloc] init];
                                [monoSettings setQuality:self.monoCompressQuality];
                                [monoSettings setCompressionMode:FSMonoImageSettingsImageCompressjbig2];
                               
                                FSOptimizerSettings *optitmizeSettings = [[FSOptimizerSettings alloc] init];
                                [optitmizeSettings setColorGrayImageSettings:imageSettings];
                                [optitmizeSettings setMonoImageSettings:monoSettings];
                                [optitmizeSettings setOptimizerOptions:FSOptimizerSettingsOptimizerCompressImages];
                                
                                FSProgressive *progressive = [FSOptimizer optimize:[self.pdfViewCtrl currentDoc] settings:optitmizeSettings pause:nil];
                                if (progressive) {
                                    FSProgressiveState proState = FSProgressiveToBeContinued;
                                    while (proState == FSProgressiveToBeContinued) {
                                        proState = [progressive resume];
                                    }
                                    if (proState != FSProgressiveFinished){
                                        return;
                                    }
                                }
                                block();
                            }];
                        }else{
                            [self.view fs_showHUDLoading:FSLocalizedForKey(@"kSaving") process:^{
                                block();
                            }];
                        }
                    }
            }
        };
        
        NSString *pdfFilePath = [[destinationFolder[0] stringByAppendingPathComponent:self.pdfFileName] stringByAppendingPathExtension:[self.pdfFilePath pathExtension]];
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        if ([fileManager fileExistsAtPath:pdfFilePath]) {
            
            if (self.extensionsManager.isViewSignedDocument) {
                AlertViewCtrlShow(FSLocalizedForKey(@"kWarning"), FSLocalizedForKey(@"kCouldNotSavedTip"));
                return;
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                       style:UIAlertActionStyleCancel
                                                                     handler:^(UIAlertAction *action) {
                                                                         [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                                                                         [controller.buttonDone setEnabled:YES];
                                                                     }];
                UIAlertAction *replaceAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kReplace")
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                        [fileManager removeItemAtPath:pdfFilePath error:nil];
                                                                        saveAs(pdfFilePath);
                                                                      }];
                [alertController addAction:cancelAction];
                [alertController addAction:replaceAction];
                [controller presentViewController:alertController animated:YES completion:nil];
            });
        } else {
            saveAs(pdfFilePath);
        }
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    [self presentViewController:selectDestinationNavController
                                     animated:YES
                                   completion:nil];
    
}

- (unsigned long long)pdfFileSize{
    unsigned long long fileSize = 0;
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:self.pdfViewCtrl.filePath]) {
        fileSize = [[manager attributesOfItemAtPath:self.pdfViewCtrl.filePath error:nil] fileSize];
    }
    return fileSize;
}

#pragma mak UITableViewDataSource UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return self.styleData.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return FSLocalizedForKey(@"kFileName");
    }
    return FSLocalizedForKey(@"kFormat");
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *cellIdentifier = @"UITableViewCellID";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.textLabel.text = self.pdfFileName;
        return cell;
    }
    SelectStyleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SelectStyleTableViewCellID];
    SelectStyleObj *obj = self.styleData[indexPath.row];
    if (obj.styleID == 2) {
        CGFloat btnW = 70;
        CGFloat ImageLeft = btnW - 10;
        UIButton *quality = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnW, cell.fs_height)];
        quality.titleLabel.textAlignment = NSTextAlignmentRight;
        [quality setImage:ImageNamed(@"webview_next") forState:UIControlStateNormal];
        [quality setImageEdgeInsets:UIEdgeInsetsMake(0, ImageLeft, 0, 0)];
        @weakify(self)
        [quality setBlockForControlEvents:UIControlEventTouchUpInside block:^(UIButton * _Nonnull sender) {
            @strongify(self)
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MoreView" bundle:[NSBundle bundleForClass:[self class]]];
            QualityViewController *qualityVC = [storyboard instantiateViewControllerWithIdentifier:@"QualityViewController"];
            qualityVC.colorCompressQuality = self.colorCompressQuality;
            qualityVC.monoCompressQuality = self.monoCompressQuality;
            qualityVC.didSelectColorBlock = ^(NSUInteger compressQuality, NSString * _Nullable qualityTitle) {
                self.colorCompressQuality = compressQuality + 1;
            };
            qualityVC.didSelectMonoBlock = ^(NSUInteger compressQuality, NSString * _Nullable qualityTitle) {
                self.monoCompressQuality = compressQuality + 1;
            };
            [self.navigationController pushViewController:qualityVC animated:YES];
        }];
        cell.accessoryView = quality;
    }else{
        cell.accessoryView = nil;
    }
    [cell setSelectStyleObj:obj];
    cell.backgroundColor = ThemeCellBackgroundColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (self.styleData.count > 1) {
            NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:[self.styleData indexOfObject:self.currentCellObj] inSection:1];
            self.currentCellObj.selected = NO;
            SelectStyleObj *obj = self.styleData[indexPath.row];
            obj.selected = YES;
            self.currentCellObj = obj;
            
            [tableView reloadRowsAtIndexPaths:@[lastIndexPath, indexPath] withRowAnimation:0];
        }
    }else{
        InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kRename")
                                                                       message:nil
                                                            buttonClickHandler:^(UIView *alertView, NSInteger buttonIndex) {
                                                                if (buttonIndex == 0) {
                                                                    return;
                                                                }
                                                                InputAlertView *inputAlert = (InputAlertView *) alertView;
                                                                NSString *fileName = inputAlert.inputTextField.text;
                                                                
                                                                if ([fileName rangeOfString:@"/"].location != NSNotFound) {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kIllegalNameWarning") preferredStyle:UIAlertControllerStyleAlert];
                                                                        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                                                                         style:UIAlertActionStyleCancel
                                                                                                                       handler:nil];
                                                                        [alertController addAction:action];
                                                                        [self presentViewController:alertController animated:YES completion:nil];
                                                                    });
                                                                    return;
                                                                } else if (fileName.length == 0) {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                    });
                                                                    return;
                                                                }
                                                                self.pdfFileName = fileName;
                                                                [self.tableView reloadData];
                                                            }
                                                             cancelButtonTitle:FSLocalizedForKey(@"kCancel")
                                                             otherButtonTitles:FSLocalizedForKey(@"kOK"), nil];
        inputAlertView.style = TSAlertViewStyleInputText;
        inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
        inputAlertView.usesMessageTextView = NO;
        inputAlertView.inputTextField.text = [self.pdfFileName stringByDeletingPathExtension];
        [inputAlertView show];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

