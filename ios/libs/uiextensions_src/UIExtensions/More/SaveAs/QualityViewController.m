/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "QualityViewController.h"
#import "SelectStyleTableViewCell.h"

static NSString *const SelectStyleTableViewCellID = @"SelectStyleTableViewCell";
@interface QualityViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, copy) NSArray *styleData;
@property (nonatomic, strong) SelectStyleObj *currentColorObj;
@property (nonatomic, strong) SelectStyleObj *currentMonoObj;
@end

@implementation QualityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kQuality");
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self creatstyleData];
    
    [self tableView:self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];

        
    [self tableView:self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.backgroundColor = UIColor_DarkMode(self.tableView.backgroundColor, PageBackgroundColor_Dark);
    self.tableView.separatorColor = DividingLineColor;
}

- (void)tableView:(UITableView *)tableView selectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:0];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)creatstyleData{
    NSArray *tempArr = @[ FSLocalizedForKey(@"kMinimum"), FSLocalizedForKey(@"kLow"), FSLocalizedForKey(@"kMedium"), FSLocalizedForKey(@"kHigh"), FSLocalizedForKey(@"kMaximum")];
    NSMutableArray *styleData = @[].mutableCopy;
    NSMutableArray *tempStyle = @[].mutableCopy;
    for (int i = 0 ; i < tempArr.count; i++) {
        SelectStyleObj *obj = [[SelectStyleObj alloc] init];
        obj.styleText = tempArr[i];
        obj.selected = NO;
        obj.styleID = i;
        if (i == (self.colorCompressQuality - 1)) {
            obj.selected = YES;
            self.currentColorObj = obj;
        }
        [tempStyle addObject:obj];
    }
    [styleData addObject:tempStyle.copy];
    tempArr = @[ FSLocalizedForKey(@"kLossy"), FSLocalizedForKey(@"kLossless")];
    tempStyle = @[].mutableCopy;
    for (int i = 0 ; i < tempArr.count; i++) {
        SelectStyleObj *obj = [[SelectStyleObj alloc] init];
        obj.styleText = tempArr[i];
        obj.selected = NO;
        obj.styleID = i;
        if (i == (self.monoCompressQuality - 1)) {
            obj.selected = YES;
            self.currentMonoObj = obj;
        }
        [tempStyle addObject:obj];
    }
    [styleData addObject:tempStyle.copy];
    self.styleData = styleData.copy;
}

- (IBAction)clickedCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mak UITableViewDataSource UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.styleData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.styleData[section] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return FSLocalizedForKey(@"kGrayscaleImages");
    }
    return FSLocalizedForKey(@"kMonochromeImages");
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SelectStyleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SelectStyleTableViewCellID];
    SelectStyleObj *obj = self.styleData[indexPath.section][indexPath.row];
    [cell setSelectStyleObj:obj];
    cell.backgroundColor = ThemeCellBackgroundColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *tmp = self.styleData[indexPath.section];
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:[tmp indexOfObject:self.currentColorObj] inSection:indexPath.section];
    SelectStyleObj *obj = tmp[indexPath.row];
    if (indexPath.section == 0) {
        self.currentColorObj.selected = NO;
        self.currentColorObj = obj;
        !self.didSelectColorBlock ? : self.didSelectColorBlock(indexPath.row, obj.styleText);
    }else{
        self.currentMonoObj.selected = NO;
        self.currentMonoObj = obj;
        !self.didSelectMonoBlock ? : self.didSelectMonoBlock(indexPath.row, obj.styleText);
    }
    obj.selected = YES;
    [tableView reloadRowsAtIndexPaths:@[lastIndexPath, indexPath] withRowAnimation:0];
}

/*
 #pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
