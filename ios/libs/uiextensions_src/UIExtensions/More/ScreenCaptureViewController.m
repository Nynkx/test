/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ScreenCaptureViewController.h"
#import "ScreenCaptureView.h"
#import "Const.h"
#import "Utility.h"

#define IOS11_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 11.0)

@interface ScreenCaptureViewController () <UIPopoverPresentationControllerDelegate>
@end

@implementation ScreenCaptureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    if (@available(iOS 11.0, *)) {
        self.buttonClose.translatesAutoresizingMaskIntoConstraints = false;
        [self.buttonClose.topAnchor constraintEqualToAnchor:self.buttonClose.superview.safeAreaLayoutGuide.topAnchor].active = true;
        [self.buttonClose.rightAnchor constraintEqualToAnchor:self.buttonClose.superview.safeAreaLayoutGuide.rightAnchor constant:-20].active = true;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.imgView.contentMode = UIViewContentModeScaleAspectFit;
    self.imgView.image = self.img;
    
    __weak __typeof__(self) weakSelf = self;
    
    self.screenCaptureView.rectSelectedHandler = ^(CGRect rect)
    {
        UIImage * img = [Utility screenShot:weakSelf.view];
        UIImage *image = [Utility cropImage:img rect:UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(2, 2, 2, 2))];
        
        if (image)
        {
            NSMutableArray *activities = [NSMutableArray array];
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:@[image] applicationActivities:activities];
            
            weakSelf.activityController = activityController;
            activityController.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError)
            {
                if(weakSelf.screenCaptureCompelementHandler)
                {
                    weakSelf.screenCaptureCompelementHandler(weakSelf.screenCaptureView.captureRect);
                }
                [weakSelf.screenCaptureView reset];
            };
            if (DEVICE_iPHONE)
            {
                [[Utility getTopMostViewController] presentViewController:activityController animated:YES completion:nil];
            }
            else
            {
                activityController.modalPresentationStyle = UIModalPresentationPopover;
                activityController.popoverPresentationController.sourceRect = CGRectMake(weakSelf.view.bounds.size.width / 2, weakSelf.view.bounds.size.height / 2, 100, 100);
                activityController.popoverPresentationController.sourceView = weakSelf.screenCaptureView;
                activityController.popoverPresentationController.delegate = weakSelf;
                [weakSelf presentViewController:activityController animated:YES completion:nil];
            }
        }
    };
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    //    [_imgView release];
    //    [_screenCaptureView release];
    //    [_buttonClose release];

    self.img = nil;
    self.screenCaptureClosedHandler = nil;
    
    //[super dealloc];
}

/*
- (void)viewDidUnload
{
    [self setImgView:nil];
    [self setScreenCaptureView:nil];
    [self setButtonClose:nil];
    [super viewDidUnload];
}
*/

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;
//}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)buttonClose:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
    if (self.screenCaptureClosedHandler)
    {
        self.screenCaptureClosedHandler();
    }
}

#pragma mark <UIPopoverPresentationControllerDelegate>

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    [self.screenCaptureView reset];
}

@end

#if _MAC_CATALYST_
//fix mac catalyst bug: UIActivityViewController add image to photo
@interface NSObject (FixCatalystBug)

@end

@implementation NSObject  (FixCatalystBug)

+ (void)load{
    Class cls = NSClassFromString(@"NSXPCDecoder");
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        SEL selectors[] = {
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Wundeclared-selector"
            @selector(_validateAllowedClass:forKey:allowingInvocations:)
            #pragma clang diagnostic pop
        };
        
        for (NSUInteger index = 0; index < sizeof(selectors) / sizeof(SEL); ++index) {
            SEL originalSel = selectors[index];
            SEL swizzledSel = NSSelectorFromString([@"fs_swizzled_" stringByAppendingString:NSStringFromSelector(originalSel)]);
            [cls fs_exchangeImpWithOriginalSel:originalSel swizzledSel:swizzledSel];
        }
    });
}

- (BOOL)fs_swizzled__validateAllowedClass:(Class)cls forKey:(id)key allowingInvocations:(id)allowingInvocations{
    BOOL _validate = NO;
    @try {
        _validate = [self fs_swizzled__validateAllowedClass:cls forKey:key allowingInvocations:allowingInvocations];
    } @catch (NSException *exception) {
        if ([key isEqualToString:@"NS.objects"] && [cls isKindOfClass:[NSURL class]]) {
            _validate = YES;
        }
    }
    return _validate;
}

@end
#endif
