/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MoreModule.h"
#import "FileInformationViewController.h"
#import "MoreView.h"
#import "ScreenCaptureViewController.h"
#import "ScreenCaptureNaviController.h"
#import "SaveAsViewController.h"

@interface MoreModule ()

@property (nonatomic, weak) UIButton *moreButton;
@property (nonatomic, weak) MoreMenuView *moreMenu;

@property (nonatomic, strong) MoreMenuGroup *othersGroup;
@property (nonatomic, strong) MoreMenuItem *saveItem;
@property (nonatomic, strong) MoreMenuItem *fileInfoItem;
@property (nonatomic, strong) MoreMenuItem *reduceFileSizeItem;
@property (nonatomic, strong) MoreMenuItem *WirelessPrintItem;
@property (nonatomic, strong) MoreMenuItem *cropScreenItem;

@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@end

@implementation MoreModule

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        self.extensionsManager = extensionsManager;
        self.pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
    }
    return self;
}

- (void)loadModule {
    [_extensionsManager registerRotateChangedListener:self];
    self.moreMenu = _extensionsManager.more;

    UIButton *moreButton = [Utility createButtonWithImage:[UIImage imageNamed:@"common_read_more" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    moreButton.tag = FS_TOPBAR_ITEM_MORE_TAG;
    [moreButton addTarget:self action:@selector(onClickMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    _extensionsManager.topToolbar.items = ({
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:moreButton];
        item.tag = FS_TOPBAR_ITEM_MORE_TAG;
        NSMutableArray *items = (_extensionsManager.topToolbar.items ?: @[]).mutableCopy;
        [items addObject:item];
        items;
    });
    self.moreButton = moreButton;

    self.othersGroup = [self.moreMenu getGroup:TAG_GROUP_FILE];
    if (!self.othersGroup) {
        self.othersGroup = [[MoreMenuGroup alloc] init];
        self.othersGroup.title = FSLocalizedForKey(@"kOtherDocumentsFile");
        self.othersGroup.tag = TAG_GROUP_FILE;
        [self.moreMenu addGroup:self.othersGroup];
    }

    self.fileInfoItem = [[MoreMenuItem alloc] init];
    self.fileInfoItem.tag = TAG_ITEM_FILEINFO;
    self.fileInfoItem.callBack = self;
    self.fileInfoItem.text = FSLocalizedForKey(@"kFileInformation");
    
    self.saveItem = [[MoreMenuItem alloc] init];
    self.saveItem.tag = TAG_ITEM_SAVE_AS;
    self.saveItem.callBack = self;
    self.saveItem.text = FSLocalizedForKey(@"kSaveAS");

    self.reduceFileSizeItem = [[MoreMenuItem alloc] init];
    self.reduceFileSizeItem.tag = TAG_ITEM_REDUCEFILESIZE;
    self.reduceFileSizeItem.callBack = self;
    self.reduceFileSizeItem.text = FSLocalizedForKey(@"kReduceFileSize");
    

    self.WirelessPrintItem = [[MoreMenuItem alloc] init];
    self.WirelessPrintItem.tag = TAG_ITEM_WIRELESSPRINT;
    self.WirelessPrintItem.callBack = self;
    self.WirelessPrintItem.text = FSLocalizedForKey(@"kAirPrint");
    
    self.cropScreenItem = [[MoreMenuItem alloc] init];
    self.cropScreenItem.tag = TAG_ITEM_SCREENCAPTURE;
    self.cropScreenItem.callBack = self;
    self.cropScreenItem.text = FSLocalizedForKey(@"kScreenCapture");
    

    [self.moreMenu addMenuItem:self.othersGroup.tag withItem:self.fileInfoItem];
    [self.moreMenu addMenuItem:self.othersGroup.tag withItem:self.saveItem];
    [self.moreMenu addMenuItem:self.othersGroup.tag withItem:self.reduceFileSizeItem];
    [self.moreMenu addMenuItem:self.othersGroup.tag withItem:self.WirelessPrintItem];
    [self.moreMenu addMenuItem:self.othersGroup.tag withItem:self.cropScreenItem];
}

- (void)onClickMoreButton:(UIButton *)button {
    _extensionsManager.currentAnnot = nil;
    _extensionsManager.currentWidget = nil;
    _extensionsManager.hiddenMoreMenu = NO;
}

- (void)onClick:(MoreMenuItem *)item {
    if (item.tag == TAG_ITEM_FILEINFO) {
        _extensionsManager.hiddenMoreMenu = YES;
        [self fileInfo];
    }else if (item.tag == TAG_ITEM_SAVE_AS){
        SaveAsViewController *VC = [[SaveAsViewController alloc] init];
        FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:VC];
        [VC setUIExtensionsManager:self.extensionsManager];
        [self.pdfViewCtrl.fs_viewController presentViewController:nav animated:YES completion:nil];
    }
    else if (item.tag == TAG_ITEM_REDUCEFILESIZE) {
        [self reduceFileSize];
    } else if (item.tag == TAG_ITEM_WIRELESSPRINT) {
        [self wirelessPrint];
    } else if(item.tag == TAG_ITEM_SCREENCAPTURE) {
        _extensionsManager.hiddenMoreMenu = YES;
        [self cropScreen];
    }
}

- (void)fileInfo {
    FileInformationViewController *fileInfoCtr = [[FileInformationViewController alloc] initWithNibName:nil bundle:[NSBundle bundleForClass:[self class]]];
    [fileInfoCtr setUIExtensionsManager:_extensionsManager];
    FSNavigationController *fileInfoNavCtr = [[FSNavigationController alloc] initWithRootViewController:fileInfoCtr];
    fileInfoNavCtr.delegate = fileInfoCtr;
    if (DEVICE_iPAD) {
        fileInfoNavCtr.modalPresentationStyle = UIModalPresentationFormSheet;
        fileInfoNavCtr.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    }
    [self.pdfViewCtrl.fs_viewController presentViewController:fileInfoNavCtr animated:YES completion:nil];
}

- (void)reduceFileSize {
    if ([_pdfViewCtrl isRMSProtected] && ![_pdfViewCtrl isOwner]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kReduceFileSizeDescription") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       self.extensionsManager.docSaveFlag = FSPDFDocSaveFlagRemoveRedundantObjects|FSPDFDocSaveFlagXRefStream;
                                                       self.extensionsManager.isDocModified = YES;
                                                       self->_extensionsManager.hiddenMoreMenu = YES;
                                                   }];
    [alertController addAction:cancelAction];
    [alertController addAction:action];
    [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)wirelessPrint {
    if (![Utility canPrint:_pdfViewCtrl]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    UIPrintInteractionCompletionHandler completion = ^(UIPrintInteractionController *_Nonnull printInteractionController, BOOL completed, NSError *_Nullable error) {
        if (error) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(error.localizedDescription) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        }
    };
    NSString *fileName = self.pdfViewCtrl.filePath.lastPathComponent;
    if ([self.pdfViewCtrl isDynamicXFA]) {
        if (DEVICE_iPHONE) {
            [Utility printXFADoc:[self.pdfViewCtrl getXFADoc] animated:YES jobName:fileName delegate:nil completionHandler:completion];
        } else {
            CGRect fromRect = [self.pdfViewCtrl convertRect:self.moreButton.bounds fromView:self.moreButton];
            [Utility printXFADoc:[self.pdfViewCtrl getXFADoc] fromRect:fromRect inView:self.pdfViewCtrl animated:YES jobName:fileName delegate:nil completionHandler:completion];
        }
        return ;
    }
    
    if (DEVICE_iPHONE) {
        [Utility printDoc:self.pdfViewCtrl.currentDoc animated:YES jobName:fileName delegate:nil completionHandler:completion];
    } else {
        CGRect fromRect = [self.pdfViewCtrl convertRect:self.moreButton.bounds fromView:self.moreButton];
        [Utility printDoc:self.pdfViewCtrl.currentDoc fromRect:fromRect inView:self.pdfViewCtrl animated:YES jobName:fileName delegate:nil completionHandler:completion];
    }
}

- (void)cropScreen {
    if (![Utility canExtractContents:_pdfViewCtrl]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    if (![Utility canCopyForAssess:self.pdfViewCtrl]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    __weak __typeof__(self) weakSelf = self;
    void(^start)(void) = ^(void)
    {
        [weakSelf.extensionsManager setFullScreen:YES];

        UIImage *img = [Utility screenShot:[self->_extensionsManager.pdfViewCtrl getDisplayView]];
        ScreenCaptureViewController *screenCaptureViewController = [[ScreenCaptureViewController alloc] initWithNibName:@"ScreenCaptureViewController" bundle:[NSBundle bundleForClass:[self class]]];

        screenCaptureViewController.img = img;
        screenCaptureViewController.screenCaptureCompelementHandler = ^(CGRect area) {
        };
        screenCaptureViewController.screenCaptureClosedHandler = ^() {
            [weakSelf.extensionsManager setFullScreen:NO];
        };
        
        ScreenCaptureNaviController *shotNavCtr = [[ScreenCaptureNaviController alloc] initWithRootViewController:screenCaptureViewController];
        if (DEVICE_iPAD) {
            shotNavCtr.modalPresentationStyle = UIModalPresentationFormSheet;
            shotNavCtr.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        }
        [self.pdfViewCtrl.fs_viewController presentViewController:shotNavCtr animated:YES completion:nil];
    };
    
    start();
}

@end
