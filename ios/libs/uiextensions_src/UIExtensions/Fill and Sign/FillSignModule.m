/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FillSignModule.h"
#import "DigitalSignatureAnnotHandler.h"
#import "Utility.h"
#import "UIButton+Extensions.h"
#import "UIExtensionsConfig.h"
#import "CerTrustedListViewCtr.h"
#import "FillSignToolHandler.h"
#import "FillSignProfileController.h"
#import "FillSignMenu.h"

@interface FillSignModule ()<MoreMenuItemAction, IAnnotEventListener>
@property (nonatomic, strong) TbBaseItem *signItem;

@property (nonatomic, strong) FSToolbar *topToolBar;
@property (nonatomic, strong) FSToolbar *bottomToolBar;
@property (nonatomic, assign) int oldState;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) FSItem *listItem;
@property (nonatomic, strong) UIButton *fillSignButton;
@property (nonatomic, assign) BOOL toolBarHiden;
@property (nonatomic, weak) FSItem *selectedItem;
@property (nonatomic, copy) NSArray *preDefineContent;
@property (nonatomic, copy) NSString *selectPreDefineStr;
@end

@implementation FillSignModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return Module_Fill_Sign;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
        FillSignToolHandler* fillSignToolHandler = [[FillSignToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:fillSignToolHandler];
        [_extensionsManager registerRotateChangedListener:fillSignToolHandler];
        self.fillSignToolHandler = fillSignToolHandler;
        
        DigitalSignatureAnnotHandler* annotHandler = [[DigitalSignatureAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_pdfViewCtrl registerScrollViewEventListener:annotHandler];
        [_extensionsManager registerRotateChangedListener:annotHandler];
        [_extensionsManager registerAnnotHandler:annotHandler];
        [_pdfViewCtrl registerDocEventListener:self];
         
    }
    return self;
}

- (void)loadModule {
    [self initToolBar];
    [_extensionsManager registerToolEventListener:self];
    [_extensionsManager registerStateChangeListener:self];
    [_extensionsManager registerAnnotEventListener:self];

    [_extensionsManager addObserver:self forKeyPath:@"isDocModified" options:NSKeyValueObservingOptionNew context:nil];
    
    NSMutableArray<UIBarButtonItem *> *tmpArray = _extensionsManager.bottomToolbar.items.mutableCopy;
    
    if (_extensionsManager.config.fillSign) {
        self.fillSignButton = [self createButtonWithTitle:FSLocalizedForKey(@"kFill") image:ImageNamed(@"fill")];
        self.fillSignButton.contentEdgeInsets = UIEdgeInsetsMake((CGRectGetHeight(_extensionsManager.bottomToolbar.frame)-CGRectGetHeight(self.fillSignButton.frame))/2, 0, 0, 0);
        self.fillSignButton.tag = FS_BOTTOMBAR_ITEM_FILLSIGN_TAG;
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.fillSignButton];
        item.tag = FS_BOTTOMBAR_ITEM_FILLSIGN_TAG;
        [tmpArray addObject:item];
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [tmpArray addObject:flexibleSpace];
        
        [self.fillSignButton addTarget:self action:@selector(onTapFillSginButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    _extensionsManager.bottomToolbar.items = tmpArray;
    self.preDefineContent = [FillSignProfileController getPreDefineConetArray];
}

#pragma mark - MoreMenuItemAction
- (void)onClick:(MoreMenuItem *)item {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
    CerTrustedListViewCtr *VC = [storyboard instantiateViewControllerWithIdentifier:@"CerTrustedListViewCtr"];
    FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:VC];
    [nav fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    [_pdfViewCtrl.fs_viewController presentViewController:nav animated:YES completion:nil];
}

- (void)onTapFillSginButton:(UIButton *)button {
    [_extensionsManager changeState:STATE_FILLSIGN];
}

- (void)initToolBar {
    UIView *superView = _pdfViewCtrl;
    _topToolBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];

    _cancelBtn = [[UIButton alloc] init];
    [_cancelBtn setImage:ImageNamed(@"common_back_black") forState:UIControlStateNormal];
    [_cancelBtn addTarget:self action:@selector(cancelSignature) forControlEvents:UIControlEventTouchUpInside];
    [_topToolBar addSubview:_cancelBtn];

    TbBaseItem *titleItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kFill")];
    titleItem.textColor = BarLikeBlackTextColor;
    [_topToolBar addSubview:titleItem.contentView];
    CGSize size = titleItem.contentView.frame.size;
    [titleItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(titleItem.contentView.superview);
        make.size.mas_equalTo(size);
    }];

    _bottomToolBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];
    NSArray *bottomItemImages = @[ @"fill_sign_text",@"fill_sign_profile", @"fill_sign_check", @"fill_sign_roundrect"];
    NSInteger tag = FSFillSignBootomItemGroupText;
    for (int i = 0; i < bottomItemImages.count; i++) {
        if (i == 4 && !_extensionsManager.config.loadSignature) continue;
        UIImage *itemImage = ImageNamed(bottomItemImages[i]);
        UIImage *itemBackImage = ImageNamed(@"lower_right_trianglegb");
        if (i == 1 || i == 4) itemBackImage = nil;
        FSItem *bottomItem = [FSItem createItemWithImage:itemImage imageSelected:itemImage imageDisable:[Utility unableImage:itemImage] background:itemBackImage];
        @weakify(self)
        bottomItem.onTapClick = ^(FSItem * _Nonnull item) {
            @strongify(self)
            [self onTapClickItem:item];
        };
        if (i != 1 && i != 4){
            bottomItem.onLongPress = ^(FSItem * _Nonnull item) {
                @strongify(self)
                item.selected = NO;
                [item setBackgroundColor:[UIColor fs_clearColor]];
                self->_extensionsManager.menuControl.menuItems = [self bottomItemsWithTag:item.tag];
                [self->_extensionsManager.menuControl setRect:[item.superview convertRect:item.frame toView:self->_pdfViewCtrl]];
                [self->_extensionsManager.menuControl showMenu];
            };
        }
        bottomItem.tag = (tag << 4);

        if (i == 2) {
            FSItem *lastItem = [_bottomToolBar getItemForIndex:1 position:FSItemPositionCenter];
            UIView *divideView = [[UIView alloc] init];
            divideView.backgroundColor = DividingLineColor;
            [_bottomToolBar addSubview:divideView];
            [divideView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastItem.mas_right).offset(ITEM_MARGIN_LITTLE_NORMAL/2);
                make.top.equalTo(divideView.superview).offset(ITEM_MARGIN_LITTLE_NORMAL);
                make.bottom.equalTo(divideView.superview).offset(-ITEM_MARGIN_LITTLE_NORMAL);
                make.width.mas_equalTo(DIVIDE_VIEW_THICKNES);
            }];
            _bottomToolBar.entiretyInterval = ITEM_MARGIN_LITTLE_NORMAL + DIVIDE_VIEW_THICKNES;
        }
        [_bottomToolBar addItem:bottomItem position:FSItemPositionCenter];
        if (i == 2) _bottomToolBar.entiretyInterval = TOOLBAR_ITEM_INVALID_MARGIN_VALUE;
        tag += 1;
    }
    
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(26);
        make.left.mas_equalTo(self->_cancelBtn.superview).offset(10);
        make.centerY.mas_equalTo(titleItem.contentView);
    }];

    [superView addSubview:_topToolBar];
    [superView addSubview:_bottomToolBar];

    [self setToolBarHiden:YES animation:NO];
}

- (void)onTapClickItem:(FSItem *)item{
    if ([FillSignMenu sharedMenuController].isMenuVisible) {
        [self.fillSignToolHandler deactiveFillSign];
    }
    if ([UIMenuController sharedMenuController].isMenuVisible) {
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    }
    FillSignToolHandler *toolHandler = self.fillSignToolHandler;
    NSUInteger groupTag = [self groupTagByMenuItemTag:item.tag];
    if (self.selectedItem != item) {
        self.selectedItem.selected = NO;
        [self.selectedItem setBackgroundColor:[UIColor fs_clearColor]];
        self.selectedItem = item;
    }
    item.selected = !item.selected;
    if (item.selected) {
        [item setBackgroundColor:ItemSelectedBackgroundColor];
        
        toolHandler.fillSignType = item.tag;
        if (toolHandler != _extensionsManager.currentToolHandler) {
            [_extensionsManager setCurrentToolHandler:toolHandler];
        }
    }else{
        [item setBackgroundColor:[UIColor fs_clearColor]];
        toolHandler.fillSignType = FSFillSignTypeUnknow;
    }
    if (groupTag == FSFillSignBootomItemGroupProfile) {
        if (groupTag == FSFillSignBootomItemGroupProfile) {
            [self cancelSelectItem];
            FillSignProfileController *profileCtr = [[FillSignProfileController alloc] init];
            profileCtr.selectCall = ^(FillSignProfileController * _Nonnull ctr, NSString * _Nonnull selectStr) {
                self.selectPreDefineStr = [NSString stringWithString:selectStr];
                item.selected = YES;
                toolHandler.fillSignType = item.tag;
                [item setBackgroundColor:ItemSelectedBackgroundColor];
                [Utility showAnnotationType:FSLocalizedForKey(@"kFillClickPrompt") type:FSAnnotUnknownType pdfViewCtrl:self->_pdfViewCtrl belowSubview:self.bottomToolBar];
                self.selectedItem = item;
            };
            profileCtr.updateCall = ^(FillSignProfileController * _Nonnull ctr, NSArray * _Nonnull contenArray) {
                self.preDefineContent = [NSArray arrayWithArray:contenArray];
            };
            
            FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:profileCtr];
            if (DEVICE_iPHONE){
                nav.modalPresentationStyle = UIModalPresentationFullScreen;
            }else{
                nav.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [_pdfViewCtrl.fs_viewController presentViewController:nav animated:YES completion:^{
                
            }];
        }
    }
}

- (NSArray<MenuItem *> *)bottomItemsWithTag:(NSUInteger)tag {
    NSMutableArray<MenuItem *> *items = @[].mutableCopy;
    NSArray *bottomToolNames = nil;
    NSArray *localizedToolNames = nil;
    NSString *prefix = @"fill_sign";
    NSUInteger uTag = [self groupTagByMenuItemTag:tag];
    if (uTag == FSFillSignBootomItemGroupText) {
        bottomToolNames = @[ @"text", @"segtext" ];
        localizedToolNames = @[ FSLocalizedForKey(@"kAddText"), FSLocalizedForKey(@"kAddCombField") ];
    }else if (uTag == FSFillSignBootomItemGroupSymbol){
        bottomToolNames = @[ @"check", @"X", @"dot" ];
        localizedToolNames = @[ FSLocalizedForKey(@"kAddCheck"), FSLocalizedForKey(@"kAddX"), FSLocalizedForKey(@"kAddDot") ];
    }else if (uTag == FSFillSignBootomItemGroupShape){
        bottomToolNames = @[ @"roundrect", @"line"];
        localizedToolNames = @[ FSLocalizedForKey(@"kAddRoundedRectangle"), FSLocalizedForKey(@"kAddLine") ];
    }
    for (NSUInteger i = 0; i < bottomToolNames.count; i ++) {
        NSString *bottomToolName = bottomToolNames[i];
        NSString *imageName = [NSString stringWithFormat:@"%@_%@",prefix, bottomToolName];
        NSString *localizedToolName = localizedToolNames[i];
        MenuItem *item = [[MenuItem alloc] initWithTitle:localizedToolName image:ImageNamed(imageName) object:self action:@selector(onItemClicked:)];
        item.tag = (int)((uTag << 4) + i);
        [items addObject:item];
    }
    return items.copy;
}

- (void)onItemClicked:(MenuItem *)menuItem{
    NSUInteger index = [self groupTagByMenuItemTag:menuItem.tag];
    FSItem *item = [_bottomToolBar getItemForIndex:index - 1 position:FSItemPositionCenter];
    if (item) {
        [item setImage:menuItem.image forState:UIControlStateNormal];
        [item setImage:[Utility imageByApplyingAlpha:menuItem.image alpha:0.5] forState:UIControlStateHighlighted];
        [item setImage:menuItem.image forState:UIControlStateSelected];
        [item setImage:[Utility imageByApplyingAlpha:menuItem.image alpha:0.5] forState:UIControlStateDisabled];
        item.tag = menuItem.tag;
        !item.onTapClick ? : item.onTapClick(item);
    }
}

- (NSUInteger)groupTagByMenuItemTag:(NSUInteger)tag{
    if((tag >> 4) > 0) return (tag >> 4);
    return tag;
}

- (void)updateItem{
    BOOL isDynamicXFA = [_pdfViewCtrl isDynamicXFA];
    if (isDynamicXFA) {
        self.fillSignButton.enabled = NO;
        return;
    }
    BOOL modifyDoc = [Utility canModifyContentsInDocument:[_pdfViewCtrl currentDoc]];
    BOOL hasForm = [[_pdfViewCtrl currentDoc] hasForm];
    BOOL sign = [Utility canAddSign:_pdfViewCtrl];
    BOOL fill = [Utility canFillForm:_pdfViewCtrl];
    self.fillSignButton.enabled = !(hasForm || !modifyDoc || !sign || !fill);
}

- (void)cancelSignature {
    
    id<IToolHandler> toolHander = [_extensionsManager currentToolHandler];
    if (toolHander != self.fillSignToolHandler) {
        return;
    }
    [self.fillSignToolHandler deactiveFillSign];
    self.selectedItem.selected = NO;
    [self.selectedItem setBackgroundColor:[UIColor fs_clearColor]];
    self.selectedItem = nil;
    [_extensionsManager setCurrentToolHandler:nil];
    [_extensionsManager changeState:STATE_NORMAL];
}

- (void)setToolBarHiden:(BOOL)toolBarHiden{
    [self setToolBarHiden:toolBarHiden animation:YES];
}

- (void)setToolBarHiden:(BOOL)toolBarHiden animation:(BOOL)animation{
    if (_toolBarHiden == toolBarHiden) {
        return;
    }
    if (toolBarHiden) {
        [_topToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self->_pdfViewCtrl);
            make.bottom.equalTo(self->_pdfViewCtrl.mas_top);
        }];
        
        [_bottomToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self->_pdfViewCtrl);
            make.top.equalTo(self->_pdfViewCtrl.mas_bottom);
        }];
    } else {
        [_topToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.left.right.equalTo(self->_pdfViewCtrl);
        }];
        
        [_bottomToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self->_pdfViewCtrl);
        }];
    }
    
    _toolBarHiden = toolBarHiden;
    
    if (animation) {
        [UIView animateWithDuration:0.3f animations:^{
            [self->_pdfViewCtrl layoutIfNeeded];
        }];
    }
}

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<NSString*, id> *)change context:(nullable void *)context{

    if([keyPath isEqualToString:@"isDocModified"])
    {
        BOOL isDocModified = [[change objectForKey:@"new"] boolValue];
        if (isDocModified) {
            [self updateItem];
        }
    }
}


#pragma mark IHandlerEventListener

- (void)onToolChanged:(NSString *)lastToolName CurrentToolName:(NSString *)toolName {
    if ([lastToolName isEqualToString:Tool_Signature] && ![toolName isEqualToString:Tool_Fill_Sign]) {
        [self setToolBarHiden:YES];
        if (toolName == nil && [_extensionsManager getState] != STATE_PANZOOM) {
            [_extensionsManager changeState:STATE_NORMAL];
        }
    }
}

- (void)annotItemClicked {
    [_extensionsManager.toolSetBar removeAllItems];
}

#pragma mark - IStateChangeListener

- (void)onStateChanged:(int)state {
    if (state == STATE_FILLSIGN) {
        [self setToolBarHiden:NO];
    } else {
        [self cancelSelectItem];
        [self setToolBarHiden:YES];
    }
}

#pragma mark <IDocEventListener>
- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    
    if (document) {
        [self updateItem];
    }
}

#pragma mark create button
- (UIButton *)createButtonWithTitle:(NSString *)title image:(UIImage *)image {
    UIFont *textFont = [UIFont systemFontOfSize:9.f];
    CGSize titleSize = [Utility getTextSize:title fontSize:textFont.pointSize maxSize:CGSizeMake(400, 100)];
    float width = image.size.width;
    float height = image.size.height;
    CGRect frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width : width, titleSize.height + height);
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [button setEnlargedEdge:ENLARGE_EDGE];
    
    [button setTitle:title forState:UIControlStateNormal];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height, 0);
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = textFont;
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
    
    [button setImage:image forState:UIControlStateNormal];
    UIImage *translucentImage = [Utility imageByApplyingAlpha:image alpha:0.5];
    [button setImage:translucentImage forState:UIControlStateHighlighted];
    [button setImage:translucentImage forState:UIControlStateDisabled];
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    
    return button;
}

- (void)cancelSelectItem
{
    self.selectedItem.selected = NO;
    [self.selectedItem setBackgroundColor:[UIColor fs_clearColor]];
    self.selectedItem = nil;
    self.fillSignToolHandler.fillSignType = FSFillSignTypeUnknow;
}

- (NSString *)getPreDefineStr
{
    NSUInteger tag = [self groupTagByMenuItemTag:self.selectedItem.tag];
    if (tag != FSFillSignBootomItemGroupProfile) return @"";
    return self.selectPreDefineStr;
}

- (NSArray *)getPreDefineContentArray{
    return self.preDefineContent;
}
@end
