/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignHandler.h"
#import "FillSignSymbolHandler.h"
#import "FillSignLineHandler.h"
#import "FillSignRoundRectHandler.h"
#import "FillSignTextHandler.h"
#import "MagnifierView.h"
#import "FillSignToolHandler.h"
#import "ShapeUtil.h"

@interface FillSignHandler ()<FillSignMenuLayout>{
    MagnifierView *_magnifierView;
}
@property (nonatomic, assign) CGSize dragDotSize;
@property (nonatomic) FSFillSignEditType editType;

@end

@implementation FillSignHandler

+ (CGAffineTransform)matrix2DToCGAffineTransform:(FSMatrix2D *)matrix{
    CGAffineTransform transform;
    transform.a = matrix.a;
    transform.b = matrix.b;
    transform.c = matrix.c;
    transform.d = matrix.d;
    transform.tx = matrix.e;
    transform.ty = matrix.f;
    return transform;
}

+ (instancetype)containerWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex fillSignType:(FSFillSignType)fillSignType{
    FillSignHandler *fillSignView;
    switch (fillSignType) {
        case FSFillSignTypeText:
        case FSFillSignTypeSegText:
        case FSFillSignTypeProfile:
        {
            fillSignView = [[FillSignTextHandler alloc] initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex fillSignType:fillSignType];
            break;
        }
        case FSFillSignTypeCheck:
        case FSFillSignTypeX:
        case FSFillSignTypeDot:
        {
            fillSignView = [[FillSignSymbolHandler alloc] initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex fillSignType:fillSignType];
            break;
        }
        case FSFillSignTypeLine:
        {
            fillSignView = [[FillSignLineHandler alloc] initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex];
            break;
        }
        case FSFillSignTypeRoundrect:
        {
            fillSignView = [[FillSignRoundRectHandler alloc] initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex];
            break;
        }
        default:
            break;
    }
    if (fillSignView) {
        return fillSignView;
    }
    return nil;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex fillSignType:(FSFillSignType)type{
    self = [self initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex];
    if (self) {
        self.type = type;
    }
    return self;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex{
    self = [self init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _type = FSFillSignTypeUnknow;
        self.status = FillSignEditStatus_Normal;
        
        self.pageIndex = pageIndex;
        self.pageRatation = (int)[[_pdfViewCtrl currentDoc] getPage:pageIndex].rotation;
        self.rotation = (self.pageRatation + [_pdfViewCtrl getViewRotation]) % 4;
        self.pdf2Pagetransform = [FillSignHandler matrix2DToCGAffineTransform:[_pdfViewCtrl getDisplayMatrix:pageIndex]];
        self.pdf2PagetransformInvert = CGAffineTransformInvert(_pdf2Pagetransform);
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {

        _edgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
        _dragDot = [UIImage fs_ImageNamed:@"annotation_drag"];
        _frame = CGRectZero;
        _bounds = CGRectZero;
        _center = CGPointZero;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareMenuDidHide) name:UIMenuControllerDidHideMenuNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareMenuShow) name:UIMenuControllerWillShowMenuNotification object:nil];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)shareMenuShow
{
//    if (self.superview != nil) {
        [[FillSignMenu sharedMenuController] setMenuVisible:NO animated:NO];
//    }
}

- (void)shareMenuDidHide
{
//    if (self.superview != nil) {
        [[FillSignMenu sharedMenuController] setMenuVisible:YES animated:NO];
//    }
}

- (BOOL)canBiggerEnable:(FillSignMenu *)menu
{
    return YES;
}

- (BOOL)canSmallerEnable:(FillSignMenu *)menu
{
    return YES;
}

- (FSFillSignObject *)getPDFFillSignObj
{
    return _pdfFillSignObj;
}

- (BOOL)canMove{
    return YES;
}

- (BOOL)canResize{
    return YES;
}

- (BOOL)keepAspectRatioForResizing{
    return NO;
}

- (CGSize)minSizeForResizing {
    return CGSizeMake(10, 10);
}

- (CGSize)maxSizeForResizing {
    UIView *pageView = [_pdfViewCtrl getPageView:self.pageIndex];
    return pageView.bounds.size;
}

- (void)setFrame:(CGRect)frame{
    _frame = frame;
    _bounds.size = frame.size;
    _center = CGPointMake(CGRectGetMidX(_frame), CGRectGetMidY(_frame));
}

- (void)setBounds:(CGRect)bounds{
    _bounds = bounds;
    _frame.size = bounds.size;
    _center = CGPointMake(CGRectGetMidX(_frame), CGRectGetMidY(_frame));
}

- (void)setCenter:(CGPoint)center{
    _center = center;
    _frame = CGRectMake(center.x - CGRectGetWidth(_frame)/2, center.y - CGRectGetHeight(_frame)/2, CGRectGetWidth(_frame), CGRectGetHeight(_frame));
}

- (void)onTapGesture:(UITapGestureRecognizer *)guest
{
}

- (void)onPanGesture:(UIPanGestureRecognizer *)recognizer endGesture:(BOOL)end
{
    FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
    BOOL canMove = [self canMove];
    BOOL canResize = [self canResize];
    if (!canMove && !canResize) {
        if (![_pdfFillSignObj isEmpty]) {
            [fillSignMenu setMenuVisible:YES animated:NO];
        }
        return;
    }
    
    UIView *pageView = [_pdfViewCtrl getPageView:self.pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    CGRect fillSignRect = [self frame];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [fillSignMenu setMenuVisible:NO animated:NO];
        [self showMagnifier:self.pageIndex point:point];
        if (!canResize || self.type == FSFillSignTypeLine) {
            _editType = FSFillSignEditTypeFull;
        } else {
            BOOL keepAspectRatio = [self keepAspectRatioForResizing];
            _editType = [ShapeUtil getFillSignEditTypeWithPoint:point rect:CGRectInset(fillSignRect, -10, -10) keepAspectRatio:keepAspectRatio defaultEditType:FSFillSignEditTypeFull];
        }
        _isMove = YES;
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [recognizer translationInView:pageView];
        CGRect newFillSignRect = [self newFillSignRectForPanTranslation:translation editType:self.editType];
        
        BOOL keepAspectRatio = [self keepAspectRatioForResizing];
        if (keepAspectRatio) {
            CGPoint moveMagnifierPoint = point;
            CGRect moveMagnifierRect = CGRectInset(newFillSignRect, -15, -20);
            if (self.editType == FSFillSignEditTypeRightBottom) {
                moveMagnifierPoint = CGPointMake(CGRectGetMaxX(moveMagnifierRect), CGRectGetMaxY(moveMagnifierRect));
            } else if (self.editType == FSFillSignEditTypeLeftTop) {
                moveMagnifierPoint = CGPointMake(CGRectGetMinX(moveMagnifierRect), CGRectGetMinY(moveMagnifierRect));
            } else if (self.editType == FSFillSignEditTypeRightTop) {
                moveMagnifierPoint = CGPointMake(CGRectGetMaxX(moveMagnifierRect), CGRectGetMinY(moveMagnifierRect));
            } else if (self.editType == FSFillSignEditTypeLeftBottom) {
                moveMagnifierPoint = CGPointMake(CGRectGetMinX(moveMagnifierRect), CGRectGetMaxY(moveMagnifierRect));
            }
            [self moveMagnifier:self.pageIndex point:moveMagnifierPoint];
        }else{
            [self moveMagnifier:self.pageIndex point:point];
        }
        
        CGRect refreshRect = CGRectUnion(newFillSignRect, fillSignRect);
        refreshRect = CGRectInset(refreshRect, -30, -30);
        [_pdfViewCtrl refresh:refreshRect pageIndex:self.pageIndex];
        self.frame = newFillSignRect;
        translation = [self updatedPanTranslation:translation oldFillSignRect:fillSignRect newFillSignRect:newFillSignRect];
        [recognizer setTranslation:translation inView:pageView];
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        _editType = FSFillSignEditTypeUnknown;

        [self updatePDFRect];
        [self updateFillSignObjPosition];

        CGRect rect = self.frame;
        rect = CGRectInset(rect, -15, -15);
        FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:_type];
        [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
        [fillSignMenu setMenuVisible:YES animated:NO];

        _extensionsManager.isDocModified = YES;
        [self closeMagnifier];
        _isMove = NO;
    }
    if (end) {
        [self updatePDFRect];
        [self updateFillSignObjPosition];

        CGRect rect = self.frame;
        rect = CGRectInset(rect, -15, -15);
        FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:_type];
        [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
        [fillSignMenu setMenuVisible:YES animated:NO];

        _extensionsManager.isDocModified = YES;
        [self closeMagnifier];
    }
}

- (BOOL)isHitFillSignObjWithPoint:(CGPoint)point {
    CGRect pvRect = self.frame;
    pvRect = CGRectInset(pvRect, -15, -15);
    if (CGRectContainsPoint(pvRect, point)) {
        return YES;
    }
    return NO;
}

- (CGSize)getFormItemSize:(CGFloat)xoffset position:(FillSignXPosition)position
{
    FillSignToolHandler *toolHandler = (FillSignToolHandler *)[self.extensionsManager getToolHandlerByName:Tool_Fill_Sign];
    CGSize itemSize = [toolHandler.fillSignPreferences[@(self.type)] CGSizeValue];
    return itemSize;
}

- (UIEdgeInsets)pageEdgeInsets{
    return UIEdgeInsetsMake(self.edgeInsets.top + self.dragDotSize.height/2, self.edgeInsets.left + self.dragDotSize.width/2, self.edgeInsets.bottom + self.dragDotSize.height/2, self.edgeInsets.right + self.dragDotSize.width/2);
}

- (void)recordFormItemSize:(CGSize)size{
    FillSignToolHandler *toolHandler = (FillSignToolHandler *)[self.extensionsManager getToolHandlerByName:Tool_Fill_Sign];
    toolHandler.fillSignPreferences[@(self.type)] = [NSNumber valueWithCGSize:size];
}

- (void)setPositionLTForFillHandler:(CGPoint)pt{
    CGPoint pdfpt = CGPointApplyAffineTransform(pt, _pdf2PagetransformInvert);
    CGSize sizepdf = [self getFormItemSize:pt.x position:FillSignXPosition_Left];
    
    if (self.rotation == 1 || self.rotation == 3) {
        CGFloat tmp = sizepdf.width;
        sizepdf.width = sizepdf.height;
        sizepdf.height = tmp;
    }
    CGRect pdfRect = CGRectMake(pdfpt.x, pdfpt.y - sizepdf.height, sizepdf.width, sizepdf.height);
    CGRect viewRect = CGRectApplyAffineTransform(pdfRect, _pdf2Pagetransform);
    CGPoint viewPoint = viewRect.origin;
    self.center = viewPoint;
    self.bounds = CGRectMake(0, 0, viewRect.size.width,  viewRect.size.height);
    [self updatePDFRect];
}

- (void)setPositionForFillHandler:(CGPoint)pt
{
    CGPoint pdfpt = CGPointApplyAffineTransform(pt, _pdf2PagetransformInvert);
    CGSize sizepdf = [self getFormItemSize:pt.x position:FillSignXPosition_Center];
    
    if (self.rotation == 1 || self.rotation == 3) {
        CGFloat tmp = sizepdf.width;
        sizepdf.width = sizepdf.height;
        sizepdf.height = tmp;
    }
    CGRect pdfRect = CGRectMake(pdfpt.x - sizepdf.width * 0.5, pdfpt.y - sizepdf.height * 0.5, sizepdf.width, sizepdf.height);
    CGRect viewRect = CGRectApplyAffineTransform(pdfRect, _pdf2Pagetransform);
    CGPoint viewPoint = viewRect.origin;
    UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
    
    if (CGRectGetMaxX(viewRect) > CGRectGetWidth(pageView.frame) - self.edgeInsets.right) {
        viewPoint.x -= (CGRectGetMaxX(viewRect) - CGRectGetWidth(pageView.frame) + self.edgeInsets.right);
    }
    self.center = viewPoint;
    self.bounds = CGRectMake(0, 0, viewRect.size.width,  viewRect.size.height);
    [self updatePDFRect];
}

- (void)setPDFRect:(CGRect)rect
{
    _PDFRect = rect;
    _oldPDFRect = _PDFRect;
    CGRect viewRect = CGRectApplyAffineTransform(_PDFRect, _pdf2Pagetransform);
    CGPoint viewPoint = viewRect.origin;
    self.center = viewPoint;
    self.bounds = CGRectMake(0, 0, viewRect.size.width,  viewRect.size.height);
}

- (void)resetPosition
{
    _pdf2Pagetransform = [FillSignHandler matrix2DToCGAffineTransform:[self.pdfViewCtrl getDisplayMatrix:self.pageIndex]];
    _pdf2PagetransformInvert = CGAffineTransformInvert(_pdf2Pagetransform);
    
    CGRect viewRect = CGRectApplyAffineTransform(_PDFRect, _pdf2Pagetransform);
    CGPoint viewPoint = viewRect.origin;
    self.center = viewPoint;
    self.bounds = CGRectMake(0, 0, viewRect.size.width,  viewRect.size.height);
    
    if (_isOnchange) return;
    [self updateFillSignMenuRect];
}

- (void)updateFillSignMenuRect{
    if (!_pdfFillSignObj || [_pdfFillSignObj isEmpty]) return;
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[_pdfFillSignObj getRect] pageIndex:self.pageIndex];
    rect = CGRectInset(rect, -15, -15);
    FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
    UIView *pageView = [_pdfViewCtrl getPageView:self.pageIndex];
    if (fillSignMenu.isMenuVisible) {
        [fillSignMenu updateTargetRect:rect inView:pageView];
    }else{
        FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:_type];
        [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
        [fillSignMenu setMenuVisible:YES animated:NO];
    }
}

- (void)updatePDFRect{
    CGRect frame = self.frame;
    _oldPDFRect = _PDFRect;
    FSRectF *rectf = [self.pdfViewCtrl convertPageViewRectToPdfRect:frame pageIndex:self.pageIndex];
    _PDFRect = CGRectMake(rectf.left, rectf.bottom, ABS(rectf.right - rectf.left), ABS(rectf.top - rectf.bottom));
    _rect = _PDFRect;
    CGAffineTransform tranform = CGAffineTransformMakeRotation(self.rotation * 90 * M_PI/180.0);
    _rect.size =  CGRectApplyAffineTransform(_PDFRect, tranform).size;
    
    if (_isOnchange) return;
    [self updateFillSignMenuRect];
}

- (void)createFillSignObj
{
    FSPDFDoc *pPdfDoc = [_pdfViewCtrl currentDoc];
    FSPDFPage *pPage = [pPdfDoc getPage:self.pageIndex];
    if (pPage && ![pPage isParsed]) [pPage startParse:0 pause:nil is_reparse:NO];
    
    FSFillSign *fillSign = [[FSFillSign alloc] initWithPage:pPage];
    FSPointF *pointf = [[FSPointF alloc] initWithX:CGRectGetMinX(_PDFRect) Y:CGRectGetMinY(_PDFRect)];
    
    FSFillSignObject *fillSignObject;
    
    BOOL isGraphics = (self.type == FSFillSignTypeCheck || self.type == FSFillSignTypeX || self.type == FSFillSignTypeDot ||
                       self.type == FSFillSignTypeLine || self.type == FSFillSignTypeRoundrect);
    if (isGraphics) {
        fillSignObject = [fillSign addObject:[self getPDFFillSignTypeFromViewType:_type] point:pointf width:CGRectGetWidth(_rect) height:CGRectGetHeight(_rect) rotation:self.rotation];
        [fillSignObject generateContent];
        [self updateFillSignObj:fillSignObject];
        [_pdfViewCtrl refresh:self.pageIndex needRender:YES];
    }
    
    if (isGraphics || self.type == FSFillSignTypeProfile) {
        self.extensionsManager.isDocModified = YES;
    }
    
    _fillSign = fillSign;
    [self setFillSignObj:fillSignObject];
}

- (void)updateFillSignObj:(FSFillSignObject *)fillSignObj
{
    _pdfFillSignObj = fillSignObj;
}

- (void)setFillSignObj:(FSFillSignObject *)fillSignObject{
    _pdfFillSignObj = fillSignObject;
    FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
    [fillSignMenu registerFillSignMenuLayout:self];
    UIView *pageView = [_pdfViewCtrl getPageView:self.pageIndex];
    CGRect rect = self.frame;
    rect = CGRectInset(rect, -15, -15);
    FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:_type];
    if (fillSignMenu.isMenuVisible) {
        if (fillSignMenu.type != itemtype) {
            [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
            [fillSignMenu setMenuVisible:YES animated:NO];
        }else{
            [fillSignMenu updateTargetRect:rect inView:pageView];
        }
    }else{
        [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
        [fillSignMenu setMenuVisible:YES animated:NO];
    }
    [_pdfViewCtrl refresh:self.pageIndex needRender:YES];
}

- (void)updateFillSignObjPosition{
    if ([_fillSign isEmpty] || !_fillSign) return;
    if (NULL != _pdfFillSignObj) {
        FSFillSignObject *fillSignObject = [[FSFillSignObject alloc] initWithOther:_pdfFillSignObj];
        
        if ([fillSignObject isEmpty] || !fillSignObject) return;
        

        FSPointF *movePointf = [[FSPointF alloc] initWithX:CGRectGetMinX(_PDFRect) Y:CGRectGetMinY(_PDFRect)];
        [fillSignObject move:movePointf width:CGRectGetWidth(_rect) height:CGRectGetHeight(_rect) rotation:self.rotation];
        [fillSignObject generateContent];
        [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
        _pdfFillSignObj = fillSignObject;
    }
}

- (void)smallerCall
{
    [self recordFormItemSize:self.rect.size];
    [_pdfViewCtrl refresh:self.pageIndex needRender:YES];
}

- (void)biggerCall
{
    [self recordFormItemSize:self.rect.size];
    [_pdfViewCtrl refresh:self.pageIndex needRender:YES];
}

- (void)removeFillSignItem:(BOOL)notify
{
    if (_pdfFillSignObj && ![_pdfFillSignObj isEmpty]) {
        FSFillSignObject *fillSignObject = [[FSFillSignObject alloc] initWithOther:_pdfFillSignObj];
        
        if (_fillSign && ![_fillSign isEmpty]) {
            [self.pdfViewCtrl lockRefresh];
            [_fillSign removeObject:fillSignObject];
            [self.pdfViewCtrl unlockRefresh];
        }

        if (!(self.type == FSFillSignTypeText || self.type == FSFillSignTypeSegText)) {
//            Fill_Sign_UndoItem *undoItem = [Fill_Sign_UndoItem CreateWithType:self.type fillSignObject:_pdfFillSignObj pdfRect:_pdfRect ObjNum:objnum Status:Fill_Sign_UndoItem_Del];
//            if (self.isReplace) {
//                undoItem.groupID = self.groupid;
//            }
//            [[APPDELEGATE.app.read getDocMgr].currentDoc addUndoItem:undoItem];
        }
        [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
        if (notify) {
          self.extensionsManager.isDocModified = YES;
        }
        _pdfFillSignObj = NULL;
    }
}

- (void)deactiveFillHandler
{
    [[FillSignMenu sharedMenuController] unregisterFillSignMenuLayout:self];
    FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
    [fillSignMenu setMenuVisible:NO animated:NO];
    [_pdfViewCtrl refresh:self.pageIndex needRender:YES];
}

- (void)fillSignToolDeactiveFillSign{
    FillSignToolHandler *fillSignTool = (FillSignToolHandler *)[self.extensionsManager getToolHandlerByName:Tool_Fill_Sign];
    [fillSignTool deactiveFillSign];
}

- (BOOL)canBiggerEnable:(FillSignMenu *)menu inView:(UIView *)inView
{
    CGSize size = self.PDFRect.size;
    CGSize newSize;
    newSize.width = size.width * 1.1;
    newSize.height = size.height * 1.1;
    
    CGSize newViewSize = CGSizeApplyAffineTransform(newSize, self.pdf2Pagetransform);
    newViewSize.width = ABS(newViewSize.width);
    newViewSize.height = ABS(newViewSize.height);
    UIEdgeInsets pageEdgeInsets = self.pageEdgeInsets;
    CGRect maxRect = UIEdgeInsetsInsetRect(inView.bounds, pageEdgeInsets);
    
    if (newViewSize.width > maxRect.size.width) return NO;
    if (newViewSize.height > maxRect.size.height) return NO;
    if ((CGRectGetMinX(self.frame) - pageEdgeInsets.left) < (newViewSize.width - self.bounds.size.width) * 0.5) return NO;
    if ((CGRectGetMinY(self.frame) - pageEdgeInsets.top) < (newViewSize.height - self.bounds.size.height) * 0.5) return NO;
    
    CGRect newFrame = self.frame;
    newFrame.origin.x -= (newViewSize.width - self.bounds.size.width);
    newFrame.origin.y -= (newViewSize.height - self.bounds.size.height);
    newFrame.size = newViewSize;
    if ((CGRectGetMaxX(newFrame) + pageEdgeInsets.right) >= (CGRectGetWidth(inView.frame))) return NO;
    if ((CGRectGetMaxY(newFrame) + pageEdgeInsets.bottom) >= (CGRectGetHeight(inView.frame))) return NO;
    return YES;
}


- (BOOL)canSmallerEnable:(FillSignMenu *)menu inView:(UIView *)inView
{
    
    CGSize size = self.rect.size;
    size.width /= 1.1;
    size.height /= 1.1;
    CGSize minSize = [self minSizeForResizing];
    if (size.width < minSize.width && ABS(size.width - minSize.width) > 0.5) return NO;
    if (self.type != FSFillSignTypeLine && size.height < minSize.height && ABS(size.height - minSize.height) > 0.5) return NO;
    return YES;
}

- (FSFillSignFillSignObjectType)getPDFFillSignTypeFromViewType:(FSFillSignType)type
{
    switch (type) {
        case FSFillSignTypeText:
            return FSFillSignFillSignObjectTypeText;
            break;
        case FSFillSignTypeSegText:
            return FSFillSignFillSignObjectTypeText;
            break;
        case FSFillSignTypeProfile:
            return FSFillSignFillSignObjectTypeText;
            break;
        case FSFillSignTypeCheck:
            return FSFillSignFillSignObjectTypeCheckMark;
            break;
        case FSFillSignTypeX:
            return FSFillSignFillSignObjectTypeCrossMark;
            break;
        case FSFillSignTypeDot:
            return FSFillSignFillSignObjectTypeDot;
            break;
        case FSFillSignTypeLine:
            return FSFillSignFillSignObjectTypeLine;
            break;
        case FSFillSignTypeRoundrect:
            return FSFillSignFillSignObjectTypeRoundRectangle;
            break;
        default:
            return (FSFillSignFillSignObjectType)0xFFFFFFFF;
            break;
    }
}

- (FSFillSignItemtype)getFillSignItemTypeByFSFillSignType:(FSFillSignType)signType
{
    switch (signType) {
        case FSFillSignTypeText:
            return FSFillSignItemtypeText;
            break;
        case FSFillSignTypeSegText:
            return FSFillSignItemtypeSegText;
            break;
        case FSFillSignTypeCheck:
            return FSFillSignItemtypeCheck;
            break;
        case FSFillSignTypeX:
            return FSFillSignItemtypeX;
            break;
        case FSFillSignTypeDot:
            return FSFillSignItemtypeDot;
            break;
        case FSFillSignTypeLine:
            return FSFillSignItemtypeLine;
            break;
        case FSFillSignTypeRoundrect:
            return FSFillSignItemtypeRoundrect;
            break;
        default:
            return FSFillSignItemtypeText;
            break;
    }
}

#pragma mark - Magnifier

- (void)showMagnifier:(int)pageIndex point:(CGPoint)point
{
    if(_magnifierView == nil)
    {
        UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
        _magnifierView = [[MagnifierView alloc] init];
        _magnifierView.viewToMagnify = [_pdfViewCtrl getDisplayView];
        _magnifierView.touchPoint = point;
        _magnifierView.magnifyPoint = [pageView convertPoint:point toView:[_pdfViewCtrl getDisplayView]];
        [pageView addSubview:_magnifierView];
        double delayInSeconds = .05;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self->_magnifierView setNeedsDisplay];
        });
    }
}

- (void)moveMagnifier:(int)pageIndex point:(CGPoint)point
{
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    _magnifierView.touchPoint = point;
    _magnifierView.magnifyPoint = [pageView convertPoint:point toView:[_pdfViewCtrl getDisplayView]];
    [_magnifierView setNeedsDisplay];
}

- (void)closeMagnifier
{
    [_magnifierView removeFromSuperview];
    _magnifierView = nil;
}


- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context{
    CGRect rect = self.frame;
    if (!_isMove) {
        rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[_pdfFillSignObj getRect] pageIndex:self.pageIndex];
        [self updateFillSignMenuRect];
    }
    if ([self canResize]) {
        self.dragDotSize = _dragDot.size;
        if (self.type != FSFillSignTypeLine) {
            rect = CGRectInset(rect, -15, -15);
            CGContextSetLineWidth(context, 2.0);
            CGFloat dashArray[] = {3, 3, 3, 3};
            CGContextSetLineDash(context, 3, dashArray, 4);
            UIColor *dashLineColor = ThemeLikeBlueColor;
            CGContextSetStrokeColorWithColor(context, [dashLineColor CGColor]);
            CGContextStrokeRect(context, rect);
            
            BOOL keepAspectRatio = [self keepAspectRatioForResizing];
            NSArray *movePointArray = keepAspectRatio ? [ShapeUtil getCornerMovePointInRect:rect] : [ShapeUtil getMovePointInRect:rect];
            
            [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                CGRect dotRect = [obj CGRectValue];
                CGPoint point = CGPointMake(dotRect.origin.x, dotRect.origin.y);
                [_dragDot drawAtPoint:point];
            }];
        }else{
            CGPoint point1 = CGPointMake(CGRectGetMinX(rect) - _dragDotSize.width, rect.origin.y + rect.size.height * 0.5 - _dragDotSize.width * 0.5);
            CGPoint point2 = CGPointMake(CGRectGetMaxX(rect), rect.origin.y + rect.size.height * 0.5 - _dragDotSize.width * 0.5);
            NSArray *movePointArray = @[ [NSNumber valueWithCGPoint:point1], [NSNumber valueWithCGPoint:point2] ];
            
            [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                CGPoint point = [obj CGPointValue];
                [_dragDot drawAtPoint:point];
            }];
        }

    }else{
        self.dragDotSize = CGSizeZero;
    }
}

#pragma mark private

- (CGRect)newFillSignRectForPanTranslation:(CGPoint)translation editType:(FSFillSignEditType)editType {
    int pageIndex = self.pageIndex;
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    UIEdgeInsets pageEdgeInsets = self.pageEdgeInsets;
    CGRect pageInsetRect = UIEdgeInsetsInsetRect(pageView.bounds, pageEdgeInsets);
    CGRect fillSignRect = [self frame];
    CGFloat w = CGRectGetWidth(fillSignRect);
    CGFloat h = CGRectGetHeight(fillSignRect);
    CGSize minSize = [self minSizeForResizing];
    CGSize maxSize = [self maxSizeForResizing];
    CGFloat minW = minSize.width;
    CGFloat maxW = maxSize.width;
    CGFloat minH = minSize.height;
    CGFloat maxH = maxSize.height;
    CGFloat dx = 0;
    CGFloat dy = 0;
    CGFloat dw = 0;
    CGFloat dh = 0;
    if (editType == FSFillSignEditTypeFull) {
        dx = translation.x;
        dy = translation.y;
        dx = MIN(dx, CGRectGetMaxX(pageInsetRect) - CGRectGetMaxX(fillSignRect));
        dx = MAX(dx, CGRectGetMinX(pageInsetRect) - CGRectGetMinX(fillSignRect));
        dy = MIN(dy, CGRectGetMaxY(pageInsetRect) - CGRectGetMaxY(fillSignRect));
        dy = MAX(dy, CGRectGetMinY(pageInsetRect) - CGRectGetMinY(fillSignRect));
    } else {
        BOOL keepAspectRatio = [self keepAspectRatioForResizing];
        if (!keepAspectRatio) {
            if (editType == FSFillSignEditTypeLeftTop ||
                editType == FSFillSignEditTypeLeftMiddle ||
                editType == FSFillSignEditTypeLeftBottom) { // left
                dw = -translation.x;
                dw = MIN(dw, maxW - w);
                dw = MAX(dw, minW - w);
                dw = MIN(dw, CGRectGetMinX(fillSignRect) - CGRectGetMinX(pageInsetRect));
                dx = -dw;
            } else if (editType == FSFillSignEditTypeRightTop ||
                       editType == FSFillSignEditTypeRightMiddle ||
                       editType == FSFillSignEditTypeRightBottom) { // right
                dw = translation.x;
                dw = MIN(dw, maxW - w);
                dw = MAX(dw, minW - w);
                dw = MIN(dw, CGRectGetMaxX(pageInsetRect) - CGRectGetMaxX(fillSignRect));
            }
            if (editType == FSFillSignEditTypeLeftTop ||
                editType == FSFillSignEditTypeMiddleTop ||
                editType == FSFillSignEditTypeRightTop) { // top
                dh = -translation.y;
                dh = MIN(dh, maxH - h);
                dh = MAX(dh, minH - h);
                dh = MIN(dh, CGRectGetMinY(fillSignRect) - CGRectGetMinY(pageInsetRect));
                dy = -dh;
            } else if (editType == FSFillSignEditTypeLeftBottom ||
                       editType == FSFillSignEditTypeMiddleBottom ||
                       editType == FSFillSignEditTypeRightBottom) { // bottom
                dh = translation.y;
                dh = MIN(dh, maxH - h);
                dh = MAX(dh, minH - h);
                dh = MIN(dh, CGRectGetMaxY(pageInsetRect) - CGRectGetMaxY(fillSignRect));
            }
        } else { // keep aspect ratio
            if (editType == FSFillSignEditTypeRightBottom) {
                CGFloat tmp = (w * translation.x + h * translation.y) / (w * w + h * h);
                dw = w * tmp;
                dh = h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(pageInsetRect) - CGRectGetMinX(fillSignRect));
                maxH = MIN(maxH, CGRectGetMaxY(pageInsetRect) - CGRectGetMinY(fillSignRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
            } else if (editType == FSFillSignEditTypeLeftTop) {
                CGFloat tmp = (w * translation.x + h * translation.y) / (w * w + h * h);
                dw = -w * tmp;
                dh = -h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(fillSignRect) - CGRectGetMinX(pageInsetRect));
                maxH = MIN(maxH, CGRectGetMaxY(fillSignRect) - CGRectGetMinY(pageInsetRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
                dx = -dw;
                dy = -dh;
            } else if (editType == FSFillSignEditTypeRightTop) {
                CGFloat tmp = (w * translation.x - h * translation.y) / (w * w + h * h);
                dw = w * tmp;
                dh = h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(pageInsetRect) - CGRectGetMinX(fillSignRect));
                maxH = MIN(maxH, CGRectGetMaxY(fillSignRect) - CGRectGetMinY(pageInsetRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
                dy = -dh;
            } else if (editType == FSFillSignEditTypeLeftBottom) {
                CGFloat tmp = (w * translation.x - h * translation.y) / (w * w + h * h);
                dw = -w * tmp;
                dh = -h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(fillSignRect) - CGRectGetMinX(pageInsetRect));
                maxH = MIN(maxH, CGRectGetMaxY(pageInsetRect) - CGRectGetMinY(fillSignRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
                dx = -dw;
            }
        }
    }
    CGRect newRect = fillSignRect;
    newRect.origin.x += dx;
    newRect.origin.y += dy;
    newRect.size.width += dw;
    newRect.size.height += dh;
    return newRect;
}

- (CGPoint)updatedPanTranslation:(CGPoint)translation oldFillSignRect:(CGRect)oldFillSignRect newFillSignRect:(CGRect)newFillSignRect {
    CGFloat absorbedTranslationX = 0;
    CGFloat absorbedTranslationY = 0;
    if (_editType == FSFillSignEditTypeFull) {
        absorbedTranslationX = CGRectGetMinX(newFillSignRect) - CGRectGetMinX(oldFillSignRect);
        absorbedTranslationY = CGRectGetMinY(newFillSignRect) - CGRectGetMinY(oldFillSignRect);
    } else {
        if (_editType == FSFillSignEditTypeLeftTop ||
            _editType == FSFillSignEditTypeLeftMiddle ||
            _editType == FSFillSignEditTypeLeftBottom) { // left
            absorbedTranslationX = CGRectGetMinX(newFillSignRect) - CGRectGetMinX(oldFillSignRect);
        } else if (_editType == FSFillSignEditTypeRightTop ||
                   _editType == FSFillSignEditTypeRightMiddle ||
                   _editType == FSFillSignEditTypeRightBottom) { // right
            absorbedTranslationX = CGRectGetMaxX(newFillSignRect) - CGRectGetMaxX(oldFillSignRect);
        } else {
            absorbedTranslationX = translation.x;
        }

        if (_editType == FSFillSignEditTypeLeftTop ||
            _editType == FSFillSignEditTypeMiddleTop ||
            _editType == FSFillSignEditTypeRightTop) { // top
            absorbedTranslationY = CGRectGetMinY(newFillSignRect) - CGRectGetMinY(oldFillSignRect);
        } else if (_editType == FSFillSignEditTypeLeftBottom ||
                   _editType == FSFillSignEditTypeMiddleBottom ||
                   _editType == FSFillSignEditTypeRightBottom) { // bottom
            absorbedTranslationY = CGRectGetMaxY(newFillSignRect) - CGRectGetMaxY(oldFillSignRect);
        } else {
            absorbedTranslationY = translation.y;
        }
    }
    translation.x -= absorbedTranslationX;
    translation.y -= absorbedTranslationY;
    return translation;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (self.status == FillSignEditStatus_Editing) {
        [self fillSignToolDeactiveFillSign];
        return;
    }
    if (!_pdfFillSignObj || [_pdfFillSignObj isEmpty]) return;
    FSRectF *rectf = [_pdfFillSignObj getRect];
    _PDFRect = CGRectMake(rectf.left, rectf.bottom, ABS(rectf.right - rectf.left), ABS(rectf.top - rectf.bottom));
    [self resetPosition];
    self.frame = [self.pdfViewCtrl convertPdfRectToPageViewRect:rectf pageIndex:self.pageIndex];
}

@end
