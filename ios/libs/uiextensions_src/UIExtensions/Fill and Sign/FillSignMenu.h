/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    FSFillSignItemtypeNone = 0UL,
    FSFillSignItemtypeSmaller = (1UL << 0),
    FSFillSignItemtypeBigger = (1UL << 1),
    FSFillSignItemtypeDelete = (1UL << 2),
    FSFillSignItemtypeCheck = (1UL << 3),
    FSFillSignItemtypeX = (1UL << 4),
    FSFillSignItemtypeDot = (1UL << 5),
    FSFillSignItemtypeLine = (1UL << 6),
    FSFillSignItemtypeRoundrect = (1UL << 7),
    FSFillSignItemtypeText = (1UL << 8),
    FSFillSignItemtypeSegText = (1UL << 9),
    FSFillSignItemtypeMore = (1UL << 10),
} FSFillSignItemtype;


typedef enum : NSUInteger {
    FillSignMenuArrowDirection_Down,
    FillSignMenuArrowDirection_Up,
} FillSignMenuArrowDirection;

@class FillSignMenu;
@protocol FillSignMenuLayout <NSObject>

@optional
- (BOOL)canBiggerEnable:(FillSignMenu *)menu inView:(UIView *)inView;
- (BOOL)canSmallerEnable:(FillSignMenu *)menu inView:(UIView *)inView;

@end

@protocol FillSignMenuEvent <NSObject>

@optional
- (void)didSelectedMenu:(FillSignMenu *)menu type:(FSFillSignItemtype)type;

@end

typedef void(^ToolbarSelectCall)(FillSignMenu *bar,  FSFillSignItemtype type);

@interface FillSignMenu : NSObject

+ (FillSignMenu *)sharedMenuController;

@property(nonatomic,getter=isMenuVisible) BOOL menuVisible;
- (void)setMenuVisible:(BOOL)menuVisible animated:(BOOL)animated;

- (void)setTargetRect:(CGRect)targetRect fillSignType:(FSFillSignItemtype)type inView:(UIView *)targetView;
@property(nonatomic) FillSignMenuArrowDirection arrowDirection;

- (void)updateTargetRect:(CGRect)targetRect inView:(UIView *)targetView;
- (void)updateFrame;

@property(nonatomic,readonly) CGRect menuFrame;
@property (nonatomic) FSFillSignItemtype type;

- (void)registerFillSignMenuLayout:(id<FillSignMenuLayout>)listener;
- (void)unregisterFillSignMenuLayout:(id<FillSignMenuLayout>)listener;
- (void)registerFillSignMenuEvent:(id<FillSignMenuEvent>)listener;
- (void)unregisterFillSignMenuEvent:(id<FillSignMenuEvent>)listener;
@end

NS_ASSUME_NONNULL_END
