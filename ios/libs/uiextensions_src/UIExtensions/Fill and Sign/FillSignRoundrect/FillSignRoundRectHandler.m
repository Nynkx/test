/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignRoundRectHandler.h"

#define MINWIDTH 45
#define MINHEIGH 30
#define RATEWIDTH 1.1

@interface FillSignRoundRectHandler ()<IDrawEventListener>

@end

@implementation FillSignRoundRectHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex{
    self = [super initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex];
    if (self) {
        self.type = FSFillSignTypeRoundrect;
    }
    return self;
}

- (CGSize)getFormItemSize:(CGFloat)xoffset position:(FillSignXPosition)position
{
    CGSize itemSize = [super getFormItemSize:xoffset position:position];
    if (CGSizeEqualToSize(itemSize, CGSizeZero)) {
        return CGSizeMake(MINWIDTH, MINHEIGH);
    }
    return itemSize;
}

- (CGSize)minSizeForResizing {
    return CGSizeMake(MINWIDTH, MINHEIGH);
}

- (void)smallerCall
{
    CGSize size = self.PDFRect.size;
    CGSize newSize;
    newSize.width = size.width / 1.1;
    newSize.height = size.height / 1.1;
    
    CGSize newViewSize = CGSizeApplyAffineTransform(newSize, self.pdf2Pagetransform);
    newViewSize.width = ABS(newViewSize.width);
    newViewSize.height = ABS(newViewSize.height);
    CGPoint center = self.center;
    center.x -= (newViewSize.width - self.bounds.size.width) * 0.5;
    center.y -= (newViewSize.height - self.bounds.size.height) * 0.5;
    self.bounds = CGRectMake(0, 0, ABS(newViewSize.width), ABS(newViewSize.height));
    self.center = center;
    
    [self updatePDFRect];
    [self updateFillSignObjPosition];
    [super smallerCall];
    self.extensionsManager.isDocModified = YES;
}

- (void)biggerCall
{
    CGSize size = self.PDFRect.size;
    CGSize newSize;
    newSize.width = size.width * 1.1;
    newSize.height = size.height * 1.1;
    
    CGSize newViewSize = CGSizeApplyAffineTransform(newSize, self.pdf2Pagetransform);
    newViewSize.width = ABS(newViewSize.width);
    newViewSize.height = ABS(newViewSize.height);
    CGPoint center = self.center;
    center.x -= (newViewSize.width - self.bounds.size.width) * 0.5;
    center.y -= (newViewSize.height - self.bounds.size.height) * 0.5;
    self.bounds = CGRectMake(0, 0, ABS(newViewSize.width), ABS(newViewSize.height));
    self.center = center;
    [self updatePDFRect];
    [self updateFillSignObjPosition];
    [super biggerCall];
    [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
    self.extensionsManager.isDocModified = YES;
}
@end
