/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FSFillSignEditType) {
    FSFillSignEditTypeUnknown = -1,
    FSFillSignEditTypeLeftTop = 0,
    FSFillSignEditTypeLeftMiddle,
    FSFillSignEditTypeLeftBottom,
    FSFillSignEditTypeMiddleTop,
    FSFillSignEditTypeMiddleBottom,
    FSFillSignEditTypeRightTop,
    FSFillSignEditTypeRightMiddle,
    FSFillSignEditTypeRightBottom,
    FSFillSignEditTypeFull,
    FSFillSignEditTypeStartPoint,
    FSFillSignEditTypeEndPoint,
};

typedef NS_ENUM(NSUInteger, FSFillSignBootomItemGroup) {
    FSFillSignBootomItemGroupNone = (0x0UL),
    FSFillSignBootomItemGroupText = (0x1UL),
    FSFillSignBootomItemGroupProfile = (0x2UL),
    FSFillSignBootomItemGroupSymbol = (0x3UL),
    FSFillSignBootomItemGroupShape = (0x4UL),
};

typedef NS_ENUM(NSUInteger, FSFillSignType) {
    FSFillSignTypeUnknow = 0,
    FSFillSignTypeText = (FSFillSignBootomItemGroupText << 4),
    FSFillSignTypeSegText = (FSFillSignBootomItemGroupText << 4) + 1,
    FSFillSignTypeCheck = (FSFillSignBootomItemGroupSymbol << 4),
    FSFillSignTypeX = (FSFillSignBootomItemGroupSymbol << 4) + 1,
    FSFillSignTypeDot = (FSFillSignBootomItemGroupSymbol << 4) + 2,
    FSFillSignTypeLine = (FSFillSignBootomItemGroupShape << 4) + 1,
    FSFillSignTypeRoundrect = (FSFillSignBootomItemGroupShape << 4),
    FSFillSignTypeProfile = (FSFillSignBootomItemGroupProfile << 4),
};

typedef NS_ENUM(NSUInteger, FillSignEditStatus){
    FillSignEditStatus_Normal,
    FillSignEditStatus_Select,
    FillSignEditStatus_Editing,
};

typedef NS_ENUM(NSUInteger, FillSignXPosition){
    FillSignXPosition_Left,
    FillSignXPosition_Center,
};

@interface FillSignUtility : NSObject

@end

NS_ASSUME_NONNULL_END
