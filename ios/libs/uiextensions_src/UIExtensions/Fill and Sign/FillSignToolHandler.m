/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FillSignToolHandler.h"
#import "FillSignModule.h"

#import "FillSignMenu.h"
#import "FillSignUtility.h"

#import "FillSignTextHandler.h"

@interface FillSignToolHandler () <UITextViewDelegate, FillSignMenuEvent>
@property (nonatomic, weak) FillSignModule *module;
@property (nonatomic, strong) FillSignHandler *editFillHandler;
@property (nonatomic, assign) int longPressPageIndex;
@property (nonatomic, strong) FSPointF *longPressPt;
@property (nonatomic, assign) BOOL shouldShowMenu;
@property (nonatomic, assign) BOOL isOnSelect;
@end

@implementation FillSignToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    int _currentPageIndex;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [_pdfViewCtrl registerScrollViewEventListener:self];
        
        type = FSAnnotUnknownType;
        _fillSignPreferences = @{}.mutableCopy;
         FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
        [fillSignMenu registerFillSignMenuEvent:self];
    }
    return self;
}

- (NSString *)getName {
    return Tool_Fill_Sign;
}

- (BOOL)isEnabled {
    return YES;
}

- (FillSignModule *)module{
    if (!_module) {
        _module = (FillSignModule *)[_extensionsManager getModuleByName:Module_Fill_Sign];
    }
    return _module;
}

- (void)didSelectedMenu:(FillSignMenu *)menu type:(FSFillSignItemtype)type{
    switch (type) {
        case FSFillSignItemtypeSmaller:
        {
            if (self.editFillHandler) {
                [self.editFillHandler smallerCall];
            }
            break;
        }
        case FSFillSignItemtypeBigger:
        {
            if (self.editFillHandler) {
                [self.editFillHandler biggerCall];
            }
            break;
        }
        case FSFillSignItemtypeDelete:
        {
            if (self.editFillHandler) {
                [self.editFillHandler removeFillSignItem: YES];
                [self.editFillHandler deactiveFillHandler];
                if ([self.editFillHandler type] == FSFillSignTypeText || [self.editFillHandler type] == FSFillSignTypeSegText) {
                    [self.editFillHandler removeFillSignItem: YES];
                }
                self.editFillHandler = nil;
            }
            break;
        }
        case FSFillSignItemtypeText:
        case FSFillSignItemtypeSegText:
        case FSFillSignItemtypeCheck:
        case FSFillSignItemtypeX:
        case FSFillSignItemtypeDot:
        case FSFillSignItemtypeLine:
        case FSFillSignItemtypeRoundrect:
        {
            int pageIndex;
            CGPoint pt;
            NSString *content;
            CGFloat fontSize = 12;
            BOOL isonCenter = NO;
            NSString *groupid = [Utility getUUID];
            if (self.editFillHandler) {
                if ([self.editFillHandler type] == FSFillSignTypeText || [self.editFillHandler type] == FSFillSignTypeSegText || [self.editFillHandler type] == FSFillSignTypeProfile) {
                    content = [(FillSignTextHandler *)self.editFillHandler getContent];
                    fontSize = [(FillSignTextHandler *)self.editFillHandler getFontSize];
                    [self.editFillHandler deactiveFillHandler];
                }
                CGRect pdfRect = self.editFillHandler.getPDFRect;
                pageIndex = self.editFillHandler.pageIndex;
        
                CGPoint pdfPoint = CGPointMake(pdfRect.origin.x, pdfRect.origin.y + pdfRect.size.height);
                pt = CGPointApplyAffineTransform(pdfPoint, self.editFillHandler.pdf2Pagetransform);
                self.editFillHandler.isReplace = YES;
                self.editFillHandler.groupid = groupid;
                [self.editFillHandler removeFillSignItem: NO];
            } else {
                pageIndex = self.longPressPageIndex;
                pt = [self->_pdfViewCtrl convertPdfPtToPageViewPt:self.longPressPt pageIndex:pageIndex];
                isonCenter = YES;
            }
            
            FSFillSignType signType = [self getFSFillSignTypeTypeByFillSignItem:type];
            if ((signType == FSFillSignTypeText || signType == FSFillSignTypeSegText) && self.editFillHandler && ([self.editFillHandler type] == FSFillSignTypeText || [self.editFillHandler type] == FSFillSignTypeSegText || [self.editFillHandler type] == FSFillSignTypeProfile)) {
                FillSignHandler *fillHandler = self.editFillHandler;
                FillSignTextHandler *textContainView = (FillSignTextHandler *)fillHandler;
                [textContainView switchTextType:signType];
            } else {
                if ([self.editFillHandler type] == FSFillSignTypeText || [self.editFillHandler type] == FSFillSignTypeSegText || [self.editFillHandler type] == FSFillSignTypeProfile) {
                    ((FillSignTextHandler *)self.editFillHandler).isAddText = NO;
                    [self deactiveFillSign];
                } else {
                    self.editFillHandler = nil;
                }
                 FillSignHandler *fillHandler = [FillSignHandler containerWithUIExtensionsManager:self->_extensionsManager pageIndex:pageIndex fillSignType:signType];
                if (isonCenter) {
                    [fillHandler setPositionForFillHandler:pt];
                } else {
                    fillHandler.isReplace = YES;
                    fillHandler.groupid = groupid;
                    [fillHandler setPositionLTForFillHandler:pt];
                }
                [fillHandler createFillSignObj];
                if ([fillHandler type] == FSFillSignTypeText || [fillHandler type] == FSFillSignTypeSegText) {
                    [(FillSignTextHandler *)fillHandler setContent:content];
//                    FillSignTextHandler *textContainView = (FillSignTextHandler *)fillHandler;
//                    [textContainView setInitKerningWidth];
//                    [textContainView activeKeyboard];
                }
                
                self.editFillHandler = fillHandler;
            }
            
            if ([self.editFillHandler type] == FSFillSignTypeText || [self.editFillHandler type] == FSFillSignTypeSegText) {
                // Here: only trigger for text, it will be triggered in createFillSignObj for graphcis
                self->_extensionsManager.isDocModified = YES;
            }
            break;
        }
        default:
            break;
    }
}

- (void)deactiveFillSign
{
    if (self.editFillHandler) {
        [self.editFillHandler deactiveFillHandler];
        self.editFillHandler = nil;
    }else{
        [[FillSignMenu sharedMenuController] setMenuVisible:NO animated:NO];
    }
}

- (void)setEditFillHandler:(FillSignHandler *)editFillHandler{
    if (self.editFillHandler == editFillHandler)  return;
    if (editFillHandler) {
        [_extensionsManager registerRotateChangedListener:editFillHandler];
    }else{
        [_extensionsManager unregisterRotateChangedListener:self.editFillHandler];
    }
    _editFillHandler = editFillHandler;
}

- (void)onActivate {

}

- (void)onDeactivate {

}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    if ([_pdfViewCtrl.currentDoc hasForm]) {
        return NO;
    }
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point1 = [recognizer locationInView:pageView];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _isOnSelect = NO;
        
        BOOL isonSelectView = NO;
        FSFillSign *fillSign;
        FSFillSignObject *fillSignObj = [self isClickOnFillItemAreaPageIndex:pageIndex position:point1 fillSign:&fillSign isOnSelectView:&isonSelectView];

        if (self.editFillHandler && [self.editFillHandler getPDFFillSignObj] != fillSignObj) {
            _isOnSelect = NO;
            [_editFillHandler deactiveFillHandler];
            self.editFillHandler = nil;
            return YES;
        }else{
            if (self.editFillHandler && [self.editFillHandler getPDFFillSignObj] == fillSignObj) {
                _isOnSelect = YES;
            }
        }
    }
    
    if (_isOnSelect) {
        if (recognizer.state != UIGestureRecognizerStateBegan && recognizer.state != UIGestureRecognizerStateChanged) {
            _isOnSelect = NO;
        }
        return YES;
    }
    
    if (self.editFillHandler) {
        [_editFillHandler deactiveFillHandler];
        self.editFillHandler = nil;
    }
    
    if (_fillSignType == FSFillSignTypeUnknow && recognizer.state == UIGestureRecognizerStateBegan) {
        FSPointF *pointf = [_pdfViewCtrl convertPageViewPtToPdfPt:point1 pageIndex:pageIndex];
        _longPressPt = pointf;
        _longPressPageIndex = pageIndex;
        
        FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
        [fillSignMenu setMenuVisible:NO animated:NO];
        [fillSignMenu setTargetRect:CGRectMake(point1.x, point1.y, 20, 10) fillSignType:FSFillSignItemtypeNone inView:pageView];
        [fillSignMenu setMenuVisible:YES animated:NO];
         return YES;
    }
    
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    if ([[_pdfViewCtrl currentDoc] hasForm]) return YES;
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point1 = [recognizer locationInView:pageView];
    BOOL isonSelectView = NO;
    
    FSFillSign *fillSign;
    FSFillSignObject *fillSignObj = [self isClickOnFillItemAreaPageIndex:pageIndex position:point1 fillSign:&fillSign isOnSelectView:&isonSelectView];
    
    if (self.editFillHandler && isonSelectView && [self.editFillHandler isKindOfClass:[FillSignTextHandler class]]) {
        [self.editFillHandler onTapGesture:recognizer];
        return YES;
    }
    
    if (![fillSignObj isEmpty] && fillSignObj) {
        if (_editFillHandler) {
            BOOL isNeedRegetForm = [_editFillHandler isKindOfClass:[FillSignTextHandler class]];
            [_editFillHandler deactiveFillHandler];
            self.editFillHandler = nil;
            if (isNeedRegetForm) {
                fillSignObj = NULL;
                BOOL isonSelectView = NO;
                fillSignObj = [self isClickOnFillItemAreaPageIndex:pageIndex position:point1 fillSign:&fillSign isOnSelectView:&isonSelectView];
            }
        }
        
        if (NULL == fillSignObj) return YES;
        FSFillSignType fillSignType = [self getFillSignTypeFromFillSignObject:fillSignObj];
        if (fillSignType == FSFillSignTypeUnknow) return NO;

        FSRectF *fillSignObjRect = [fillSignObj getRect];
        
        FillSignHandler *fillHandler = [FillSignHandler containerWithUIExtensionsManager:_extensionsManager pageIndex:pageIndex fillSignType:fillSignType];
        if (fillSignType == FSFillSignTypeSegText) {
            [(FillSignTextHandler *)fillHandler setInitKerningWidth];
        }
        fillHandler.status = FillSignEditStatus_Select;
        self.editFillHandler = fillHandler;
        [fillHandler setPDFRect:CGRectMake(fillSignObjRect.left, fillSignObjRect.bottom, fillSignObjRect.right - fillSignObjRect.left, fillSignObjRect.top - fillSignObjRect.bottom)];
        fillHandler->_fillSign = fillSign;
        [fillHandler setFillSignObj:fillSignObj];
        
        return YES;
    }
    
    if (_fillSignType == FSFillSignTypeUnknow){
        if (_editFillHandler) {
            [_editFillHandler deactiveFillHandler];
            self.editFillHandler = nil;
            return YES;
        }else{
            FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
            if (fillSignMenu.isMenuVisible) {
                [fillSignMenu setMenuVisible:NO animated:NO];
                return YES;
            }
        }
        return NO;
    }
    
    if (_editFillHandler) {
        [_editFillHandler deactiveFillHandler];
        self.editFillHandler = nil;
    }
    FillSignHandler *fillHandler = [FillSignHandler containerWithUIExtensionsManager:_extensionsManager pageIndex:pageIndex fillSignType:_fillSignType];
    if (fillHandler.type  == FSFillSignTypeSegText){
        [(FillSignTextHandler *)fillHandler setInitKerningWidth];
    }
    self.editFillHandler = fillHandler;
    [fillHandler setPositionForFillHandler:point1];
    [fillHandler createFillSignObj];
    
    if (_fillSignType == FSFillSignTypeProfile) {
        NSString *str = [self.module getPreDefineStr];
        [(FillSignTextHandler *)fillHandler setContent:str];
        [self.module cancelSelectItem];
    }

    if (fillHandler.type == FSFillSignTypeText || fillHandler.type  == FSFillSignTypeSegText) {
        FillSignTextHandler *textContainView = (FillSignTextHandler *)fillHandler;
        if (_fillSignType != FSFillSignTypeProfile) {
            [textContainView activeKeyboard];
        }
    }
    
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    if (self.fillSignType == FSFillSignTypeUnknow && !self.editFillHandler) {
        return NO;
    }
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _isOnSelect = NO;
        FSFillSignObject *fillSignObj = [self.editFillHandler getPDFFillSignObj];
        
        if ([self.editFillHandler isKindOfClass:[FillSignTextHandler class]]) {
            _isOnSelect = YES;
        }else{
            if (!fillSignObj || [fillSignObj isEmpty]) {
                _isOnSelect = NO;
                [_editFillHandler deactiveFillHandler];
                self.editFillHandler = nil;
                return YES;
            }else{
                if (fillSignObj && ![fillSignObj isEmpty]) {
                    _isOnSelect = YES;
                }
            }
        }

    }
    
    if (_isOnSelect) {
        if (recognizer.state != UIGestureRecognizerStateBegan && recognizer.state != UIGestureRecognizerStateChanged) {
            _isOnSelect = NO;
        }
        if (self.editFillHandler) {
            if (self.editFillHandler.pageIndex != pageIndex || !CGRectContainsPoint(pageView.bounds, point)) {
                [self.editFillHandler onPanGesture:recognizer endGesture:YES];
            }else{
               [self.editFillHandler onPanGesture:recognizer endGesture:NO];
            }
            
            return YES;
        }
    }
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (self.fillSignType == FSFillSignTypeUnknow && !self.editFillHandler) {
        return NO;
    }
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [gestureRecognizer locationInView:pageView];
    return [self isHitFillSignObjWithPoint:point];
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if ([_extensionsManager getState] != STATE_FILLSIGN) return;
    if (self.editFillHandler && self.editFillHandler.pageIndex == pageIndex) {
        [self.editFillHandler onDraw:pageIndex inContext:context];
    }
}

- (BOOL)isHitFillSignObjWithPoint:(CGPoint)point {
    if (self.editFillHandler) {
        return [self.editFillHandler isHitFillSignObjWithPoint:point];
    }
    return YES;
}

- (UIFont *)getSysFont:(NSString *)name size:(float)size {
    UIFont *font = [UIFont fontWithName:[Utility convert2SysFontString:name] size:size];
    if (!font) {
        font = [UIFont systemFontOfSize:size];
    }
    return font;
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView isFirstResponder]) {
        if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage]) {
            return NO;
        }
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    FSPDFPage *currentPage = [_pdfViewCtrl.currentDoc getPage:_currentPageIndex];
    FSRectF *rect = [_pdfViewCtrl convertPageViewRectToPdfRect:[[_pdfViewCtrl getPageView:_currentPageIndex] bounds] pageIndex:_currentPageIndex];
    float currentPageW = currentPage.getWidth;
    float pageViewWidth = [_pdfViewCtrl getPageViewWidth:_currentPageIndex];
    float pageWidth = pageViewWidth;
    
    float noneSpacing = rect.right - rect.left - currentPageW;
    if (( currentPage.rotation == FSRotation0 || currentPage.rotation == FSRotation180)&& noneSpacing > 0){
        pageWidth = ((currentPageW - noneSpacing)/currentPageW) * pageViewWidth;
    }
    
    CGSize oneSize = [Utility getTestSize:textView.font];
    CGPoint point = textView.frame.origin;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size = [textView.text boundingRectWithSize:CGSizeMake(pageWidth - point.x - oneSize.width, 99999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : textView.font, NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;

    size.width += oneSize.width;
    size.height += oneSize.height;
    CGRect frame = textView.frame;
    frame.size = size;
    textView.frame = frame;
    float textViewHeight = textView.frame.origin.y + textView.frame.size.height;
    float textViewWidth = textView.frame.origin.x + textView.frame.size.width;

    if (textViewHeight >= ([_pdfViewCtrl getPageViewHeight:_currentPageIndex] - 20)) {
        CGRect textViewFrame = textView.frame;
        textViewFrame.origin.y -= (textViewHeight - [_pdfViewCtrl getPageViewHeight:_currentPageIndex]);
        textView.frame = textViewFrame;
    }
    if (textViewWidth >= (pageWidth - 20)) {
        CGRect textViewFrame = textView.frame;
        textViewFrame.origin.x -= (textViewWidth - pageWidth);
        textView.frame = textViewFrame;
    }

    if (textView.frame.size.height >= ([_pdfViewCtrl getPageViewHeight:_currentPageIndex] - 20)) {
        [textView endEditing:YES];
    }
    if (textView.frame.size.width >= (pageWidth - 20)) {
        [textView endEditing:YES];
    }
}

- (FSFillSignObject *)isClickOnFillItemAreaPageIndex:(int)pageIndex position:(CGPoint)point fillSign:(FSFillSign **)pfillSign isOnSelectView:(BOOL *)isonselectview
{
    FSPointF *fpoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    
    if (self.editFillHandler) {
        CGRect rect = self.editFillHandler.frame;
        rect = CGRectInset(rect, -30, -30);
        if (CGRectContainsPoint(rect, point)) {
            *isonselectview = YES;
            return (FSFillSignObject *)self.editFillHandler.getPDFFillSignObj;
        }
    }
    

    FSPDFDoc *pDoc = [_pdfViewCtrl currentDoc];
    FSPDFPage *pPage = [pDoc getPage:pageIndex];
    if (!pPage || [pPage isEmpty]) return nil;
    if (!pPage.isParsed) [pPage startParse:0 pause:nil is_reparse:NO];
    FSFillSign *fillSign = [[FSFillSign alloc] initWithPage:pPage];
    *pfillSign = fillSign;
    
    FSFillSignObject *fillSignObj = [fillSign getObjectAtPoint:fpoint];
    
    FSRectF *tmp = [_pdfViewCtrl convertPageViewRectToPdfRect:CGRectMake(0, 0, 10, 10) pageIndex:pageIndex];
    
    float width = ABS(tmp.right - tmp.left);
    for (int i = 1; i < ceil(width); i++) {
         if (!fillSignObj || [fillSignObj isEmpty]) {
             FSPointF *fpoint1 = [[FSPointF alloc] initWithX:fpoint.x - i Y:fpoint.y + i];
             for (int k = 0; k < (i * 2 - 1); k++) {
                 FSPointF *ftmp = [[FSPointF alloc] initWithX:fpoint1.x Y:fpoint1.y - k];
                 fillSignObj = [fillSign getObjectAtPoint:ftmp];
                 if (![fillSignObj isEmpty] && fillSignObj) break;
             }
             if (![fillSignObj isEmpty]) break;
             
             FSPointF *fpoint2 = [[FSPointF alloc] initWithX:fpoint.x - i Y:fpoint.y - i];
             for (int k = 0; k < (i * 2 - 1); k++) {
                 FSPointF *ftmp = [[FSPointF alloc] initWithX:fpoint2.x + k Y:fpoint2.y];
                 fillSignObj = [fillSign getObjectAtPoint:ftmp];
                 if (![fillSignObj isEmpty] && fillSignObj) break;
             }
             if (![fillSignObj isEmpty]) break;
             
             FSPointF *fpoint3 = [[FSPointF alloc] initWithX:fpoint.x + i Y:fpoint.y - i];
             for (int k = 0; k < (i * 2 - 1); k++) {
                 FSPointF *ftmp = [[FSPointF alloc] initWithX:fpoint3.x Y:fpoint3.y + k];
                 fillSignObj = [fillSign getObjectAtPoint:ftmp];
                 if (![fillSignObj isEmpty] && fillSignObj) break;
             }
             if (![fillSignObj isEmpty]) break;
             
             FSPointF *fpoint4 = [[FSPointF alloc] initWithX:fpoint.x + i Y:fpoint.y + i];
             for (int k = 0; k < (i * 2 - 1); k++) {
                 FSPointF *ftmp = [[FSPointF alloc] initWithX:fpoint4.x - k Y:fpoint4.y];
                 fillSignObj = [fillSign getObjectAtPoint:ftmp];
                 if (![fillSignObj isEmpty] && fillSignObj) break;
             }
             if (![fillSignObj isEmpty] && fillSignObj) break;
         }else{
             break;
         }
    }
    if (![fillSignObj isEmpty] && fillSignObj) {
        CGRect cgrect = [_pdfViewCtrl convertPdfRectToPageViewRect:[fillSignObj getRect] pageIndex:pageIndex];
        if (CGRectContainsPoint(cgrect, point)) {
            return fillSignObj;
        }
    }
    return nil;
}

- (FSFillSignType)getFillSignTypeFromFillSignObject:(FSFillSignObject *)fillSignObject
{
    if (!fillSignObject || [fillSignObject isEmpty]) return FSFillSignTypeUnknow;
    
    if ([fillSignObject getType] == FSFillSignFillSignObjectTypeCheckMark) {
        return FSFillSignTypeCheck;
    }
    if ([fillSignObject getType] == FSFillSignFillSignObjectTypeCrossMark) {
        return FSFillSignTypeX;
    }
    if ([fillSignObject getType] == FSFillSignFillSignObjectTypeDot) {
        return FSFillSignTypeDot;
    }
    if ([fillSignObject getType] == FSFillSignFillSignObjectTypeLine) {
        return FSFillSignTypeLine;
    }
    if ([fillSignObject getType] == FSFillSignFillSignObjectTypeRoundRectangle) {
        return FSFillSignTypeRoundrect;
    }
    if ([fillSignObject getType] == FSFillSignFillSignObjectTypeText) {
        FSTextFillSignObject *textFillSignObject = [[FSTextFillSignObject alloc] initWithFillsign_object:fillSignObject];
        if (![textFillSignObject isCombFieldMode]) {
            return FSFillSignTypeText;
        }else{
            return FSFillSignTypeSegText;
        }
    }
    return FSFillSignTypeUnknow;
}

- (FSFillSignItemtype)getFillSignItemTypeByFSFillSignType:(FSFillSignType)signType
{
    switch (signType) {
        case FSFillSignTypeText:
            return FSFillSignItemtypeText;
            break;
        case FSFillSignTypeSegText:
            return FSFillSignItemtypeSegText;
            break;
        case FSFillSignTypeCheck:
            return FSFillSignItemtypeCheck;
            break;
        case FSFillSignTypeX:
            return FSFillSignItemtypeX;
            break;
        case FSFillSignTypeDot:
            return FSFillSignItemtypeDot;
            break;
        case FSFillSignTypeLine:
            return FSFillSignItemtypeLine;
            break;
        case FSFillSignTypeRoundrect:
            return FSFillSignItemtypeRoundrect;
            break;
        default:
            return FSFillSignItemtypeText;
            break;
    }
}

- (FSFillSignType)getFSFillSignTypeTypeByFillSignItem:(FSFillSignItemtype)itemType
{
    switch (itemType) {
        case FSFillSignItemtypeText:
            return FSFillSignTypeText;
            break;
        case FSFillSignItemtypeSegText:
            return FSFillSignTypeSegText;
            break;
        case FSFillSignItemtypeCheck:
            return FSFillSignTypeCheck;
            break;
        case FSFillSignItemtypeX:
            return FSFillSignTypeX;
            break;
        case FSFillSignItemtypeDot:
            return FSFillSignTypeDot;
            break;
        case FSFillSignItemtypeLine:
            return FSFillSignTypeLine;
            break;
        case FSFillSignItemtypeRoundrect:
            return FSFillSignTypeRoundrect;
            break;
        default:
            return FSFillSignTypeText;
            break;
    }
}

#pragma mark <IScrollViewEventListener>

- (void)onScrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([FillSignMenu sharedMenuController].isMenuVisible) {
        _shouldShowMenu = YES;
        [[FillSignMenu sharedMenuController] setMenuVisible:NO animated:NO];
    }
}

- (void)onScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate && _shouldShowMenu) {
        _shouldShowMenu = NO;
        [[FillSignMenu sharedMenuController] setMenuVisible:YES animated:NO];
    }
}

- (void)onScrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([FillSignMenu sharedMenuController].isMenuVisible) {
        _shouldShowMenu = YES;
        [[FillSignMenu sharedMenuController] setMenuVisible:NO animated:NO];
    }
}

- (void)onScrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (_shouldShowMenu) {
        _shouldShowMenu = NO;
        [[FillSignMenu sharedMenuController] setMenuVisible:YES animated:NO];
    }
}

- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView {
    if ([FillSignMenu sharedMenuController].isMenuVisible) {
        _shouldShowMenu = YES;
        [[FillSignMenu sharedMenuController] setMenuVisible:NO animated:NO];
    }
}

- (void)onScrollViewDidEndZooming:(UIScrollView *)scrollView {
    if (_shouldShowMenu && self.editFillHandler) {
        _shouldShowMenu = NO;
        [[FillSignMenu sharedMenuController] setMenuVisible:YES animated:NO];
    }
}

#pragma mark <IFSUndoEventListener>

- (void)onWillUndo {
    
}

- (void)onWillRedo {
    
}

@synthesize type;

@end
