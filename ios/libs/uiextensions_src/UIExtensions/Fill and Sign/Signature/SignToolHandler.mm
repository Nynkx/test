/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SignToolHandler.h"

#import "AlertView.h"
#import "AnnotationSignature.h"

#import "DigitalSignatureAnnotHandler.h"
#import "FileSelectDestinationViewController.h"
#import "MenuControl.h"
#import "MenuItem.h"
#import "ShapeUtil.h"
#import "SignatureListViewController.h"
#import "SignatureViewController.h"
#import "FillSignModule.h"

@interface SignToolHandler () <SignatureListDelegate>

@property (nonatomic, strong) UIImage *annotImage;
@property (nonatomic, strong) SignatureListViewController *signatureListCtr;
@property (nonatomic, strong) UIControl *maskView;
@property (nonatomic, assign) int currentPageIndex;
@property (nonatomic, assign) CGPoint currentPoint;
@property (nonatomic, assign) BOOL shouldShowMenu;
@property (nonatomic, assign) BOOL isShowList;
@property (nonatomic, strong) NSObject *currentCtr;
@property (nonatomic, strong) DigitalSignatureAnnotHandler *annotHandler;
@end

@implementation SignToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
    FSAnnotEditType _editType;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotWidget;
        self.docChanging = nil;
        self.isPerformOnce = YES;
        self.isDeleting = NO;

        __weak UIExtensionsManager *weakExtMgr = extensionsManager;
        self.docChanging = ^(NSString *newDocPath) {
            if ([weakExtMgr.delegate respondsToSelector:@selector(uiextensionsManager:openNewDocAtPath:shouldCloseCurrentDoc:)]) {
//                weakExtMgr.isDocModified = NO; //don't save doc for signing.
                [weakExtMgr.delegate uiextensionsManager:weakExtMgr openNewDocAtPath:newDocPath shouldCloseCurrentDoc:YES];
            } else {
                [weakExtMgr.pdfViewCtrl openDoc:newDocPath password:nil completion:^(FSErrorCode error) {
                    if (error != FSErrSuccess) {
                        [weakExtMgr.pdfViewCtrl closeDoc:nil];
                        if (weakExtMgr.goBack)
                            weakExtMgr.goBack();
                    }
                }];
            }
        };
        self.getDocPath = ^NSString *() {
            return weakExtMgr.pdfViewCtrl.filePath;
        };

        //Create signature data store directory.
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isDirectory;
        NSError *error = nil;
        if (![fileManager fileExistsAtPath:SIGNATURE_PATH isDirectory:&isDirectory] || !isDirectory) {
            BOOL success = [fileManager createDirectoryAtPath:SIGNATURE_PATH withIntermediateDirectories:YES attributes:nil error:&error];
            if (!success) {
                FoxitLog(@"Fail to create %@. %@", SIGNATURE_PATH, [error localizedDescription]);
            }
        }
    }
    return self;
}

- (SignatureListViewController *)signatureListCtr {
    if (!_signatureListCtr) {
        _signatureListCtr = [[SignatureListViewController alloc] init];
        _signatureListCtr.modalPresentationStyle = UIModalPresentationFullScreen;
        _signatureListCtr.extensionManager = _extensionsManager;
    }
    return _signatureListCtr;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.shouldShowMenu = NO;
        self.isShowList = NO;
        self.currentCtr = nil;
    }
    return self;
}

- (NSString *)getName {
    return Tool_Signature;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
    self.isAdded = NO;
}

- (void)onDeactivate {
    if (!_isDeleting)
        [self delete];
}

- (BOOL)isHitAnnot:(AnnotationSignature *)annot point:(FSPointF *)point {
    CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.rect pageIndex:annot.pageIndex];
    pvRect = CGRectInset(pvRect, -30, -30);
    CGPoint pvPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:point pageIndex:annot.pageIndex]; //pageView docToPageViewPoint:point];
    if (CGRectContainsPoint(pvRect, pvPoint)) {
        return YES;
    }
    return NO;
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    self.currentPageIndex = pageIndex;
    CGPoint point = CGPointZero;
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    if (recognizer) {
        point = [recognizer locationInView:pageView];
    } else {
        point = _signatureStartPoint;
    }
    if (!_isAdded) {
        _maxWidth = pageView.frame.size.width;
        _minWidth = 10;
        _maxHeight = pageView.frame.size.height;
        _minHeight = 10;

        NSString *name = [AnnotationSignature getSignatureSelected];
        //    #ifdef RMS
        if ([_extensionsManager.pdfViewCtrl isRMSProtected]) {
            NSMutableArray *signList = [AnnotationSignature getSignatureList];
            AnnotationSignature *signnature = [AnnotationSignature getSignature:name];
            
            if (signnature.certMD5 && signnature.certPasswd && signnature.certFileName) {
                [AnnotationSignature setSignatureSelected:[[self getHandleSignArray:signList] firstObject]];
            }
            name = [AnnotationSignature getSignatureSelected];
        }
        //    #endif
        UIImage *image = [AnnotationSignature getSignatureImage:name];
        
        CGSize imageSize = [self adjustmentImageSize:image.size];
        
        image = [Utility scaleToSize:image size:CGSizeMake(imageSize.width / 2, imageSize.height / 2)];
        if (!image) {
            _signatureStartPoint = point;
            [self signList];
            return YES;
        }

        self.signature = [AnnotationSignature getSignature:name];
        self.signature.pageIndex = pageIndex;
        CGSize signSize = image.size;
        CGRect signRect = CGRectMake(point.x - signSize.width / 2, point.y - signSize.height / 2, signSize.width, signSize.height);
        if (signRect.origin.x < 0) {
            signRect = CGRectMake(0, signRect.origin.y, signSize.width, signSize.height);
        }
        if (signRect.origin.y < 0) {
            signRect = CGRectMake(signRect.origin.x, 0, signSize.width, signSize.height);
        }
        if ((signRect.origin.x + signRect.size.width) > _maxWidth) {
            signRect = CGRectMake(_maxWidth - signSize.width, signRect.origin.y, signSize.width, signSize.height);
        }
        if ((signRect.origin.y + signRect.size.height) > _maxHeight) {
            signRect = CGRectMake(signRect.origin.x, _maxHeight - signSize.height, signSize.width, signSize.height);
        }
        self.signature.rect = [_pdfViewCtrl convertPageViewRectToPdfRect:signRect pageIndex:pageIndex];

        self.annotImage = image;
        _isAdded = YES;

        NSMutableArray *array = [NSMutableArray array];

        MenuItem *signItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSignAction") object:self action:@selector(sign)];
        MenuItem *deleteItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kDelete") object:self action:@selector(delete)];

        [array addObject:signItem];
        [array addObject:deleteItem];

        CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:signRect pageIndex:pageIndex];
        _extensionsManager.menuControl.menuItems = array;

        [_extensionsManager.menuControl setRect:dvRect margin:20];
        [_extensionsManager.menuControl showMenu];

        self.shouldShowMenu = YES;
        signRect = CGRectInset(signRect, -30, -30);
        [_pdfViewCtrl refresh:signRect pageIndex:pageIndex needRender:YES];
    } else {
        [self delete];
    }
    return YES;
}

- (CGSize)adjustmentImageSize:(CGSize)size{
    CGFloat W_H_Ratio = size.width/size.height;
    if (size.width > _maxWidth || size.height > _maxHeight) {
        if (size.width >= _maxWidth && size.height <= _maxHeight) {
            size.width = _maxWidth;
            size.height = size.width / W_H_Ratio;
        }
        else if (size.width <= _maxWidth && size.height >= _maxHeight) {
            size.height = _maxHeight;
            size.width = size.height * W_H_Ratio;
        }
        if (size.width >= _maxWidth && size.height >= _maxHeight) {
            size.width = _maxWidth;
            size.height = size.width / W_H_Ratio;
            return [self adjustmentImageSize:size];
        }
    }
    return size;
}

- (FSSignature *)createSignature:(FSPDFPage *)page withParam:(DIGITALSIGNATURE_PARAM *)param {
    FSSignature *sign = NULL;
    FSRectF *rect = param.rect;

    //Add an unsigned signature field with blank appearance on the specified position of page.
    sign = [page addSignature:rect];
    if (nil == sign)
        return nil;

    sign.filter = @"Adobe.PPKLite";
    sign.subFilter = param.subfilter;

    NSDate *now = [NSDate date];
    FSDateTime *time = [Utility convert2FSDateTime:now];
    [sign setSignTime:time];
    
    FSControl *control = [sign getControl:0];
    if (control == nil || control.isEmpty) {
        return nil;
    }
    FSWidget *widget = [control getWidget];
    if (widget == nil || widget.isEmpty) {
        return nil;
    }
    FSRotation rotation = (FSRotation )(([_pdfViewCtrl getViewRotation] + [page getRotation]) % 4);
    [widget setMKRotation:rotation];
    [widget setNM:[Utility getUUID]];
    
    [sign setAppearanceFlags:FSSignatureAPFlagBitmap];

    NSData *data = [AnnotationSignature getSignatureData:param.sigName];
    FSBitmap *bmp = [Utility imgDataToBitmap:data];
    [sign setBitmap:bmp];
    for (int i = 0; i < [sign getControlCount]; i++) {
        [_pdfViewCtrl lockRefresh];
        [[[sign getControl:i] getWidget] resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
    }

    return sign;
}

- (void)initSignature:(FSSignature *)sign withParam:(DIGITALSIGNATURE_PARAM *)param {
    //Add an unsigned signature field with blank appearance on the specified position of page.
    sign.filter = @"Adobe.PPKLite";
    sign.subFilter = param.subfilter;

    NSDate *now = [NSDate date];
    FSDateTime *time = [Utility convert2FSDateTime:now];
    [sign setSignTime:time];
    [sign setAppearanceFlags:FSSignatureAPFlagBitmap];

    NSData *data = [NSData dataWithContentsOfFile:param.imagePath];
    FSBitmap *bmp = [Utility imgDataToBitmap:data];
    [sign setBitmap:bmp];

    for (int i = 0; i < [sign getControlCount]; i++) {
        [_pdfViewCtrl lockRefresh];
        [[[sign getControl:i] getWidget] resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
    }
}

- (BOOL)signSignature:(FSSignature *)sign withParam:(DIGITALSIGNATURE_PARAM *)param {
    @try {
        FSProgressive *ret = [sign startSign:param.certFile cert_password:param.certPwd digest_algorithm:FSSignatureDigestSHA1 save_path:param.signFilePath client_data:nil pause:nil];

        if (ret != nil) {
            FSProgressiveState state = [ret resume];
            while (FSProgressiveToBeContinued == state) {
                state = [ret resume];
            }
            return FSProgressiveFinished == state;
        } else {
            return YES;
        }
    } @catch (NSException *exception) {
        return NO;
    }
    
}

- (FSSignatureStates)verifyDigitalSignature:(NSString *)fileName signature:(FSSignature *)signature {
    @try {
        FSProgressive *ret = [signature startVerify:nil pause:nil];
        if (ret != nil) {
            FSProgressiveState state = [ret resume];
            while (FSProgressiveToBeContinued == state) {
                state = [ret resume];
            }
        }
    } @catch (NSException *exception) {
        
    }
    return (FSSignatureStates)[signature getState];
}

- (NSMutableArray *)getHandleSignArray:(NSArray *)listArray {
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    for (NSString *sigName in listArray) {
        AnnotationSignature *sig = [AnnotationSignature getSignature:sigName];
        if (!(sig.certMD5 && sig.certPasswd && sig.certFileName)) {
            [mutableArray addObject:sigName];
        }
    }
    return mutableArray;
}

- (void)changedSignImage {
    NSString *name = [AnnotationSignature getSignatureSelected];
    UIImage *image = [AnnotationSignature getSignatureImage:name];
    image = [Utility scaleToSize:image size:CGSizeMake(image.size.width / 2, image.size.height / 2)];
    CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.currentPageIndex];
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(oldRect), CGRectGetMidY(oldRect));
    self.signature = [AnnotationSignature getSignature:name];
    self.signature.pageIndex = self.currentPageIndex;
    CGSize signSize = image.size;
    CGRect signRect = CGRectMake(centerPoint.x - signSize.width / 2, centerPoint.y - signSize.height / 2, signSize.width, signSize.height);
    self.signature.rect = [_pdfViewCtrl convertPageViewRectToPdfRect:signRect pageIndex:self.currentPageIndex];
    self.annotImage = image;

    CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.currentPageIndex];
    newRect = CGRectUnion(newRect, oldRect);
    newRect = CGRectInset(newRect, -60, -60);
    [_pdfViewCtrl refresh:newRect pageIndex:self.currentPageIndex needRender:YES];
}

- (void)sign {
    if (!_isAdded) {
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.currentPageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.currentPageIndex needRender:YES];

        return;
    }

    BOOL isSaveTip = [Preference getBoolValue:Module_Signature type:@"SignSaveTip" delaultValue:NO];
    if (!isSaveTip) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kConfirmSign") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 [self delete];
                                                                 [self->_extensionsManager setCurrentToolHandler:nil];

                                                                 CGRect newRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.currentPageIndex];
                                                                 newRect = CGRectInset(newRect, -30, -30);
                                                                 [self->_pdfViewCtrl refresh:newRect pageIndex:self.currentPageIndex needRender:YES];

                                                                 self.currentCtr = nil;
                                                             }];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) { // yes
                                                           [self addSign];
                                                           self.currentCtr = nil;
                                                           [Preference setBoolValue:Module_Signature type:@"SignSaveTip" value:YES];
                                                       }];
        [alertController addAction:cancelAction];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        self.currentCtr = alertController;
    } else {
        [self addSign];
//        _extensionsManager.isDocModified = YES;
    }
}

- (void)addSignAsImage {
    AnnotationSignature *signAnnot = [AnnotationSignature createWithDefaultOptionForPageIndex:self.signature.pageIndex rect:self.signature.rect];
    signAnnot.data = [AnnotationSignature getSignatureData:self.signature.name];
    signAnnot.contents = @"FoxitMobilePDF/Erutangis";
    signAnnot.name = @"FoxitMobilePDF/Erutangis";
    signAnnot.author = _extensionsManager.annotAuthor;
    FSBitmap *dib = [Utility imgDataToBitmap:signAnnot.data];
    if (!dib)
        return;
    FSPDFDoc *doc = [_pdfViewCtrl currentDoc];
    FSPDFPage *page = [doc getPage:self.signature.pageIndex];
    if (!page || [page isEmpty])
        return;
    if (![page isParsed]) {
        FSProgressive *progress = [page startParse:FSPDFPageParsePageNormal pause:nil is_reparse:NO];
        FSProgressiveState state = FSProgressiveToBeContinued;
        while (state == FSProgressiveToBeContinued) {
            state = [progress resume];
        }
        if (state != FSProgressiveFinished) return;
    }
    FSImageObject *imageObj = [FSImageObject create:doc];
    FSBitmap* mask = [[FSBitmap alloc] init];
    [imageObj setBitmap:dib mask:mask];

    FSMatrix2D *matrix = [[FSMatrix2D alloc] init];
    FSRectF *rect = self.signature.rect;
    int imageWidth = rect.right - rect.left;
    int imageHeight = rect.top - rect.bottom;
    FSRotation rotate = [page getRotation];
    
    FSRotation viewrotation = (FSRotation)[_pdfViewCtrl getViewRotation];
    int endrotate = (rotate + viewrotation) % 4;
    rotate = (FSRotation)endrotate;
    
    switch (rotate) {
    case FSRotation0:
        [matrix set:imageWidth b:0 c:0 d:imageHeight e:rect.left f:rect.bottom];
        break;
    case FSRotation90:
        [matrix set:0 b:imageHeight c:-imageWidth d:0 e:rect.left + imageWidth f:rect.bottom];
        break;
    case FSRotation270:
        [matrix set:0 b:-imageHeight c:imageWidth d:0 e:rect.left f:rect.bottom + imageHeight];
        break;
    case FSRotation180:
        [matrix set:-imageWidth b:0 c:0 d:-imageHeight e:rect.left + imageWidth f:rect.bottom + imageHeight];
        break;
    default:
        break;
    }

    [imageObj setMatrix:matrix];
    long lastObjPos = [page getLastGraphicsObjectPosition:FSGraphicsObjectTypeAll];
    [page insertGraphicsObject:lastObjPos graphics_object:imageObj];
    [page generateContent];
    [Utility parsePage:page];
    [_pdfViewCtrl refresh:self.signature.pageIndex needRender:YES];
    _extensionsManager.isDocModified = YES;
}

- (void)addSignAsStamp {
    AnnotationSignature *signAnnot = [AnnotationSignature createWithDefaultOptionForPageIndex:self.signature.pageIndex rect:self.signature.rect];

    signAnnot.data = [AnnotationSignature getSignatureData:self.signature.name];
    signAnnot.contents = @"FoxitMobilePDF/Erutangis";
    signAnnot.name = @"FoxitMobilePDF/Erutangis";

    {
        FSRectF *rect = signAnnot.rect;
        //patch rect to avoid sdk parm check
        if (rect.left != 0 && rect.left == rect.right) {
            rect.right++;
        }
        if (rect.bottom != 0 && rect.bottom == rect.top) {
            rect.top++;
        }

        FSPDFPage *pdfPage = [[_pdfViewCtrl getDoc] getPage:signAnnot.pageIndex];
        signAnnot.signature = [pdfPage addAnnot:FSAnnotStamp rect:rect];
        if (signAnnot.signature) {
            //Set name (uuid)
            [signAnnot.signature setUniqueID:[Utility getUUID]];
            //Set author
            //Set add and modify time
            NSDate *now = [NSDate date];
            [signAnnot.signature setCreateDate:now];
            [signAnnot.signature setModifiedDate:now];

            FSBitmap *dib = [Utility imgDataToBitmap:signAnnot.data];
            [[[FSStamp alloc] initWithAnnot:signAnnot.signature] setBitmap:dib];
            [_pdfViewCtrl lockRefresh];
            [signAnnot.signature resetAppearanceStream];
            [_pdfViewCtrl unlockRefresh];
        }
    }
    _isAdded = NO;
    self.annotImage = nil;
}

- (void)addSign {
    NSString *imagePath = [SIGNATURE_PATH stringByAppendingPathComponent:[self.signature.name stringByAppendingString:@"_i"]];
    FSRectF *rect = self.signature.rect;
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:self.signature.pageIndex];

    if (rect.left != 0 && rect.left == rect.right) {
        rect.right++;
    }
    if (rect.bottom != 0 && rect.bottom == rect.top) {
        rect.top++;
    }

    BOOL isDigitalSignature = NO;
    if (self.signature.certFileName && self.signature.certPasswd && self.signature.certMD5) {
        isDigitalSignature = YES;
    }
    
    if (!isDigitalSignature) {
        [self addSignAsImage];
    } else {
        [self addDigitalSign:page signArea:rect signImagePath:imagePath];
    }
    _isAdded = NO;
    
    CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
    newRect = CGRectInset(newRect, -30, -30);
    [_pdfViewCtrl refresh:newRect pageIndex:self.signature.pageIndex needRender:YES];

    if (self.isPerformOnce){
        FillSignModule *fillSign = (FillSignModule *)[_extensionsManager getModuleByName:Module_Fill_Sign];
        [fillSign cancelSelectItem];
    }
        
    [_extensionsManager removeThumbnailCacheOfPageAtIndex:self.signature.pageIndex];
    
    if (_pdfViewCtrl.getCropMode == PDF_CROP_MODE_CONTENTSBOX) {
//        [self adjustmentCropPageIndexView:self.signature.pageIndex];
        [_pdfViewCtrl setCropMode:PDF_CROP_MODE_CONTENTSBOX];
    }
}

- (void)adjustmentCropPageIndexView:(int)index{
    UIView *pageView = [_pdfViewCtrl getPageView:index];
    UIView *overlayView = [_pdfViewCtrl getOverlayView:index];
    UIEdgeInsets cropInsets = [_pdfViewCtrl getCropInsets:index];
    
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:index];
    CGSize pageSize = CGSizeMake(page.getWidth, page.getHeight);
    
    FSRotation pvcRotation = (FSRotation)_pdfViewCtrl.getViewRotation;
    if (pvcRotation == FSRotation90 || pvcRotation == FSRotation270) {
        int tmp = pageSize.width;
        pageSize.width =  pageSize.height;
        pageSize.height = tmp;
    }
    if (!UIEdgeInsetsEqualToEdgeInsets(cropInsets, UIEdgeInsetsZero)) {
        pageSize.width -= cropInsets.left + cropInsets.right;
        pageSize.height -= cropInsets.top + cropInsets.bottom;
    }
    
    CGSize adjusSize = CGSizeMake(pageSize.width * _pdfViewCtrl.getScale, pageSize.height * _pdfViewCtrl.getScale);
    CGRect pageViewRect = pageView.frame;
    pageViewRect.size = adjusSize;
    pageView.frame = pageViewRect;
    overlayView.frame = pageViewRect;
}

- (void)signDigitalSignature: (FSPDFPage *)page signArea:(FSRectF *)rect signImagePath:(NSString *)imagePath savePath:(NSString*) pdfFilePath {
    if (pdfFilePath == nil) return;
    DIGITALSIGNATURE_PARAM *param = [[DIGITALSIGNATURE_PARAM alloc] init];
    param.certFile = [SIGNATURE_PATH stringByAppendingPathComponent:self.signature.certMD5];
    param.certPwd = self.signature.certPasswd;
    param.subfilter = @"adbe.pkcs7.detached";
    param.imagePath = imagePath;
    param.rect = rect;
    param.sigName = self.signature.name;
    param.signFilePath = pdfFilePath;
    FSSignature *signature = [self createSignature:page withParam:param];
    
    @try {
        BOOL isSuccess = [self signSignature:signature withParam:param];
        if (isSuccess) {
            [[self->_pdfViewCtrl getDoc] removeSignature:signature];
            [self delete];
            [self->_extensionsManager setCurrentToolHandler:nil];
            [self->_extensionsManager changeState:STATE_NORMAL];
            
            double delayInSeconds = 0.4;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kSaveSignedDocSuccess") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction *action) {
                                                                         double delayInSeconds = 0.4;
                                                                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                                                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                                                                             if (self.docChanging) {
                                                                                 self.docChanging(pdfFilePath);
                                                                             }
                                                                         });
                                                                         return;
                                                                     }];
                [alertController addAction:cancelAction];
                [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                
            });
        } else {
            [[self->_pdfViewCtrl getDoc] removeSignature:signature];
            [self delete];
            [self->_extensionsManager setCurrentToolHandler:nil];
            [self->_extensionsManager changeState:STATE_NORMAL];
            
            double delayInSeconds = 0.4;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kSaveSignedDocFailure") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:action];
                [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            });
        }
    } @catch (NSException *exception) {
        [[self->_pdfViewCtrl getDoc] removeSignature:signature];
        [self delete];
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_NORMAL];
        
        double delayInSeconds = 0.4;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kSaveSignedDocFailure") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        });
    }
}

- (void)addDigitalSign:(FSPDFPage *)page signArea:(FSRectF *)rect signImagePath:(NSString *)imagePath {
    BOOL isAutoSaveSignedDoc = self->_extensionsManager.isAutoSaveSignedDoc;
    if (isAutoSaveSignedDoc) {
        NSString* userSavePath = self->_extensionsManager.signedDocSavePath;
        if (userSavePath == nil) {
            userSavePath = self->_pdfViewCtrl.filePath;
            if (userSavePath == nil) return;
            userSavePath = [userSavePath.stringByDeletingPathExtension stringByAppendingString:@"-signed.pdf"];
        }
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:userSavePath]) {
            [fileManager removeItemAtPath:userSavePath error:nil];
        }
        [self signDigitalSignature:page signArea:rect signImagePath:imagePath savePath:userSavePath];
        return;
    }
    
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Select;
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:nil];
        typedef void (^DumbBlock)(void);
        DumbBlock __block inputFileName;
        __weak DumbBlock weakInputFileName = nil;
        __block NSSet *blockContainer = nil;
        weakInputFileName = inputFileName = ^() {
            InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kInputNewFileName")
                                                                           message:nil
                                                                buttonClickHandler:^(UIView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    return;
                }
                InputAlertView *inputAlert = (InputAlertView *) alertView;
                NSString *fileName = inputAlert.inputTextField.text;
                
                if ([fileName rangeOfString:@"/"].location != NSNotFound || fileName.length == 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kIllegalNameWarning") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                               style:UIAlertActionStyleCancel
                                                                             handler:^(UIAlertAction *action) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //                                                                                                                                         weakInputFileName();
                                for (DumbBlock input in blockContainer) {
                                    input();
                                }
                            });
                            return;
                        }];
                        [alertController addAction:cancelAction];
                        [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    });
                    return;
                }
                
                NSString *pdfFilePath = [destinationFolder[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", fileName]];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if ([fileManager fileExistsAtPath:pdfFilePath]) {
                    double delayInSeconds = 0.3;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                               style:UIAlertActionStyleCancel
                                                                             handler:^(UIAlertAction *action) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //                                                                                                                                         weakInputFileName();
                            });
                        }];
                        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kReplace")
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction *action) {
                            [fileManager removeItemAtPath:pdfFilePath error:nil];
                            [self signDigitalSignature:page signArea:rect signImagePath:imagePath savePath:pdfFilePath];
                            inputFileName = nil;
                        }];
                        [alertController addAction:cancelAction];
                        [alertController addAction:action];
                        [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    });
                    return;
                }
                [self signDigitalSignature:page signArea:rect signImagePath:imagePath savePath:pdfFilePath];
                inputFileName = nil;
            }
                                                                 cancelButtonTitle:@"kCancel"
                                                                 otherButtonTitles:@"kOK", nil];
            inputAlertView.style = TSAlertViewStyleInputText;
            inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
            inputAlertView.usesMessageTextView = NO;
            [inputAlertView show];
        };
        blockContainer = [NSSet setWithObject:inputFileName];
        inputFileName();
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    [_pdfViewCtrl.fs_viewController presentViewController:selectDestinationNavController animated:YES completion:nil];
}

- (void)signList {
    typeof(self) __weak weakSelf = self;
    UIViewController *rootViewController = _pdfViewCtrl.fs_viewController;
    
    if ([AnnotationSignature getSignatureList].count <= 0) {
        SignatureViewController *signatureCtr = [[SignatureViewController alloc] initWithUIExtensionsManager:_extensionsManager];
        [signatureCtr fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];
        signatureCtr.currentSignature = nil;
        signatureCtr.saveHandler = ^{
            [weakSelf showAnnotMenu];
            weakSelf.currentCtr = nil;
        };
        signatureCtr.cancelHandler = ^{
            [weakSelf showAnnotMenu];
            weakSelf.currentCtr = nil;
            SignToolHandler *strongSelf = weakSelf;
            assert(strongSelf);
            if (strongSelf) {
                [strongSelf->_extensionsManager setCurrentToolHandler:nil];
            }
        };

        dispatch_async(dispatch_get_main_queue(), ^{
            [rootViewController presentViewController:signatureCtr
                                             animated:NO
                                           completion:^{
                                               weakSelf.currentCtr = signatureCtr;
                                           }];
        });
        return;
    };

    if (DEVICE_iPHONE) {
        [rootViewController presentViewController:self.signatureListCtr
                                         animated:YES
                                       completion:^{
                                           self.isShowList = YES;
                                       }];
    } else {
        CGRect screenFrame = _pdfViewCtrl.bounds;
        self.signatureListCtr.view.frame = CGRectMake(screenFrame.size.width - 300, 0, 300, screenFrame.size.height);
        self.signatureListCtr.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;

        self.maskView = [[UIControl alloc] initWithFrame:_pdfViewCtrl.bounds];
        self.maskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
        self.maskView.backgroundColor = [UIColor blackColor];
        self.maskView.alpha = 0.3f;
        self.maskView.tag = 200;
        [self.maskView addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [_pdfViewCtrl addSubview:self.maskView];
        [_pdfViewCtrl addSubview:self.signatureListCtr.view];
        CGRect preFrame = CGRectMake(screenFrame.size.width, 0, 300, screenFrame.size.height);
        CGRect afterFrame = CGRectMake(screenFrame.size.width - 300, 0, 300, screenFrame.size.height);
        self.signatureListCtr.view.frame = preFrame;
        self.signatureListCtr.view.backgroundColor = [UIColor redColor];
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.signatureListCtr.view.frame = afterFrame;
                         }];
        self.isShowList = YES;
    }

    self.signatureListCtr.delegate = self;
}

- (void)openCreateSign {
    typeof(self) __weak weakSelf = self;
    if ([AnnotationSignature getSignatureList].count <= 0) {
        SignatureViewController *signatureCtr = [[SignatureViewController alloc] initWithUIExtensionsManager:_extensionsManager];
        [signatureCtr fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];
        signatureCtr.currentSignature = nil;
        signatureCtr.saveHandler = ^{
            weakSelf.currentCtr = nil;
        };
        signatureCtr.cancelHandler = ^{
            weakSelf.currentCtr = nil;
            [self->_extensionsManager setCurrentToolHandler:nil];
        };
        //        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [_pdfViewCtrl.fs_viewController presentViewController:signatureCtr
                                         animated:NO
                                       completion:^{
                                           weakSelf.currentCtr = signatureCtr;
                                       }];
        return;
    };
}

- (void)dismiss {
    CGRect screenFrame = _pdfViewCtrl.bounds;
    CGRect preFrame = CGRectMake(screenFrame.size.width - 300, 0, 300, screenFrame.size.height);
    CGRect afterFrame = CGRectMake(screenFrame.size.width, 0, 300, screenFrame.size.height);

    self.signatureListCtr.view.frame = preFrame;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.signatureListCtr.view.frame = afterFrame;
                         [self.signatureListCtr.view removeFromSuperview];
                         [self.maskView removeFromSuperview];
                     }];

    CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
    CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:self.signature.pageIndex];
    if (_isAdded) {
        self.shouldShowMenu = YES;

        [_extensionsManager.menuControl setRect:dvRect margin:20];
        [_extensionsManager.menuControl showMenu];
        dvRect = CGRectInset(dvRect, -10, -10);
        [_pdfViewCtrl refresh:dvRect pageIndex:self.signature.pageIndex needRender:YES];
    }

    self.isShowList = NO;
    
    if ([_extensionsManager.pdfViewCtrl isRMSProtected]) {
        NSMutableArray *signList = [AnnotationSignature getSignatureList];
        NSMutableArray *handlerList = [self getHandleSignArray:signList];
        if (handlerList.count <= 0) {
            [_extensionsManager setCurrentToolHandler:nil];
        }
    }else{
        if ([AnnotationSignature getSignatureList].count <= 0) {
            [_extensionsManager setCurrentToolHandler:nil];
        }
    }
}

- (void) delete {
    _isDeleting = YES;
    _isAdded = NO;
    self.annotImage = nil;

    if (self.signature) {
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.signature.pageIndex needRender:YES];
        //    if (self.isPerformOnce)
        //        [_extensionsManager setCurrentToolHandler:nil];
    } else {
        [_pdfViewCtrl refresh:_extensionsManager.currentPageIndex];
    }
    _isDeleting = NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    if (self != _extensionsManager.currentToolHandler) {
        return NO;
    }

    if (!_isAdded) {
        return NO;
    }

    if (pageIndex != self.signature.pageIndex) {
        return NO;
    }
    UIView *pageView = [_pdfViewCtrl getPageView:self.signature.pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];

    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if (![self isHitAnnot:self.signature point:pdfPoint]) {
            return NO;
        }

        if (_extensionsManager.menuControl.isMenuVisible) {
            [_extensionsManager.menuControl setMenuVisible:NO animated:YES];
        }

        _editType = FSAnnotEditTypeFull;

        NSArray *movePointArray = [ShapeUtil getMovePointInRect:CGRectInset([_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex], -5, -5)];
        [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            CGRect dotRect = [obj CGRectValue];

            dotRect = CGRectInset(dotRect, -20, -20);

            if (CGRectContainsPoint(dotRect, point)) {
                self->_editType = (FSAnnotEditType) idx;
                *stop = YES;
            }
        }];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (![self isHitAnnot:self.signature point:pdfPoint]) {
            return NO;
        }

        CGPoint translationPoint = [recognizer translationInView:pageView];
        [recognizer setTranslation:CGPointZero inView:pageView];
        float tw = translationPoint.x;
        float th = translationPoint.y;
        CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        FSRectF *rect = [Utility CGRect2FSRectF:oldRect];

        if (_editType == FSAnnotEditTypeLeftTop ||
            _editType == FSAnnotEditTypeLeftMiddle ||
            _editType == FSAnnotEditTypeLeftBottom ||
            _editType == FSAnnotEditTypeFull) {
            rect.left += tw;
            if (_editType != FSAnnotEditTypeFull) {
                // Not left over right
                if ((rect.left + _minWidth) > rect.right) {
                    rect.right = rect.left + _minWidth;
                } else if (ABS(rect.right - rect.left) > _maxWidth) {
                    rect.left -= tw;
                }
            }
        }
        if (_editType == FSAnnotEditTypeRightTop ||
            _editType == FSAnnotEditTypeRightMiddle ||
            _editType == FSAnnotEditTypeRightBottom ||
            _editType == FSAnnotEditTypeFull) {
            rect.right += tw;
            if (_editType != FSAnnotEditTypeFull) {
                if ((rect.left + _minWidth) > rect.right) {
                    rect.left = rect.right - _minWidth;
                } else if (ABS(rect.right - rect.left) > _maxWidth) {
                    rect.right -= tw;
                }
            }
        }
        if (_editType == FSAnnotEditTypeLeftTop ||
            _editType == FSAnnotEditTypeMiddleTop ||
            _editType == FSAnnotEditTypeRightTop ||
            _editType == FSAnnotEditTypeFull) {
            rect.top += th;
            if (_editType != FSAnnotEditTypeFull) {
                if ((rect.top + _minHeight) > rect.bottom) {
                    rect.bottom = rect.top + _minHeight;
                } else if (ABS(rect.bottom - rect.top) > _maxHeight) {
                    rect.top -= th;
                }
            }
        }
        if (_editType == FSAnnotEditTypeLeftBottom ||
            _editType == FSAnnotEditTypeMiddleBottom ||
            _editType == FSAnnotEditTypeRightBottom ||
            _editType == FSAnnotEditTypeFull) {
            rect.bottom += th;
            if (_editType != FSAnnotEditTypeFull) {
                if ((rect.top + _minHeight) > rect.bottom) {
                    rect.top = rect.bottom - _minHeight;
                } else if (ABS(rect.bottom - rect.top) > _maxHeight) {
                    rect.bottom -= th;
                }
            }
        }

        CGRect newRect = [Utility FSRectF2CGRect:rect];
        rect = [_pdfViewCtrl convertPageViewRectToPdfRect:newRect pageIndex:self.signature.pageIndex];

        if (!(newRect.origin.x <= 0 || newRect.origin.x + newRect.size.width >= pageView.frame.size.width || newRect.origin.y <= 0 || newRect.origin.y + newRect.size.height >= pageView.frame.size.height)) {
            self.signature.rect = rect;

            CGRect refreshRect = CGRectUnion(newRect, oldRect);
            refreshRect = CGRectInset(refreshRect, -80, -80);
            [_pdfViewCtrl refresh:refreshRect pageIndex:self.signature.pageIndex needRender:NO];
        }
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        _editType = FSAnnotEditTypeUnknown;
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        CGRect showRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:self.signature.pageIndex];

        [_extensionsManager.menuControl setRect:showRect margin:20];
        [_extensionsManager.menuControl showMenu];

        showRect = CGRectInset(showRect, -80, -80);
        [_pdfViewCtrl refresh:showRect pageIndex:self.signature.pageIndex needRender:NO];
    }
    return YES;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    UIView *pageView = [_pdfViewCtrl getPageView:self.signature.pageIndex];
    CGPoint point = [gestureRecognizer locationInView:pageView];
    FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:self.signature.pageIndex];
    if (_extensionsManager.currentToolHandler == self) {
        if (!_isAdded) {
            return YES;
        } else {
            if ([self isHitAnnot:self.signature point:pdfPoint]) {
                return YES;
            }
        }
    }
    if (pageIndex == self.signature.pageIndex && [self isHitAnnot:self.signature point:pdfPoint]) {
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

#pragma mark IRotateChangedListener

- (void)onRotateChangedBefore:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self dismissAnnotMenu];
}

- (void)onRotateChangedAfter:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self showAnnotMenu];
}

#pragma mark IDvTouchEventListener

- (void)onScrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    [self showAnnotMenu];
}

- (void)onScrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self showAnnotMenu];
}

- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndZooming:(UIScrollView *)scrollView {
    [self showAnnotMenu];
}

- (void)showAnnotMenu {
    if (_isAdded) {
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        CGRect showRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:self.signature.pageIndex];
        if (self.shouldShowMenu) {
            [_extensionsManager.menuControl setRect:showRect margin:20];
            [_extensionsManager.menuControl showMenu];
            showRect = CGRectInset(showRect, -80, -80);
            [_pdfViewCtrl refresh:showRect pageIndex:self.signature.pageIndex needRender:YES];
        }
    }
}

- (void)dismissAnnotMenu {
    if (_isAdded) {
        if (_extensionsManager.menuControl.isMenuVisible) {
            [_extensionsManager.menuControl setMenuVisible:NO animated:YES];
        }
    }
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_extensionsManager.currentToolHandler != self || !_isAdded) {
        return;
    }

    if (pageIndex != self.signature.pageIndex) {
        return;
    }

    if (self.annotImage == nil) {
        return;
    }

    CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
    if (self.annotImage) {
        CGContextSaveGState(context);

        CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
        CGContextTranslateCTM(context, 0, rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
        CGContextDrawImage(context, rect, [self.annotImage CGImage]);

        CGContextRestoreGState(context);

        rect = CGRectInset(rect, -5, -5);

        CGContextSetLineWidth(context, 2.0);
        CGFloat dashArray[] = {3, 3, 3, 3};
        CGContextSetLineDash(context, 3, dashArray, 4);
        CGContextSetStrokeColorWithColor(context, [[UIColor colorWithRGB:self.signature.color] CGColor]);
        CGContextStrokeRect(context, rect);

        UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        NSArray *movePointArray = [ShapeUtil getMovePointInRect:rect];
        [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            CGRect dotRect = [obj CGRectValue];
            CGPoint point = CGPointMake(dotRect.origin.x - 2, dotRect.origin.y - 2);
            [dragDot drawAtPoint:point];
        }];
    }
}

#pragma mark - IDocEventListener

- (void)onDocWillClose:(FSPDFDoc *)document;
{
    self.shouldShowMenu = NO;
    _isAdded = NO;
    if (self.isShowList) {
        if (DEVICE_iPHONE) {
            [self.signatureListCtr dismissViewControllerAnimated:YES
                                                      completion:^{
                                                          self.isShowList = NO;
                                                      }];
        } else {
            [self dismiss];
        }
    }
    if (self.currentCtr) {
        if ([self.currentCtr isKindOfClass:[SignatureViewController class]]) {
            [(SignatureViewController *) self.currentCtr dismissViewControllerAnimated:NO
                                                                            completion:^{
                                                                                self.currentCtr = nil;
                                                                            }];
        } else if ([self.currentCtr isKindOfClass:[UIAlertController class]]) {
            [(UIAlertController *) self.currentCtr dismissViewControllerAnimated:NO completion:nil];
            self.currentCtr = nil;
        }
    }
}

#pragma mark SignatureListDelegate

- (void)signatureListViewController:(SignatureListViewController *)signatureListViewController openSignature:(AnnotationSignature *)signature {
    if (DEVICE_iPHONE) {
        void(^completion)() = ^{
            self.isShowList = NO;
        };
        if (signatureListViewController.presentingViewController) {
            [signatureListViewController dismissViewControllerAnimated:YES
                                                            completion:completion];
        }else{
            completion();
        }

    } else {
        [self dismiss];
    }
    SignatureViewController *signatureCtr = [[SignatureViewController alloc] initWithUIExtensionsManager:_extensionsManager];
    [signatureCtr fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    signatureCtr.currentSignature = signature;
    typeof(self) __weak weakSelf = self;
    signatureCtr.saveHandler = ^{
        if (weakSelf.isAdded) {
            [weakSelf changedSignImage];
        }
        [weakSelf showAnnotMenu];
        weakSelf.currentCtr = nil;
    };
    signatureCtr.cancelHandler = ^{
        [weakSelf showAnnotMenu];
        weakSelf.currentCtr = nil;
    };

    //    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    double delayInSeconds = 0.05;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [self->_pdfViewCtrl.fs_viewController presentViewController:signatureCtr
                                         animated:NO
                                       completion:^{
                                           self.currentCtr = signatureCtr;
                                       }];
    });
}

- (void)signatureListViewController:(SignatureListViewController *)signatureListViewController selectSignature:(AnnotationSignature *)signature {
    if (DEVICE_iPHONE) {
        self.isShowList = NO;
        [self changedSignImage];
        [self showAnnotMenu];
    } else {
        [self dismiss];
        [self changedSignImage];
    }
}

- (void)signatureListViewController:(SignatureListViewController *)signatureListViewController deleteSignature:(AnnotationSignature *)signature {
    UIImage *image = [AnnotationSignature getSignatureImage:self.signature.name];
    image = [Utility scaleToSize:image size:CGSizeMake(image.size.width / 2, image.size.height / 2)];
    if (!image) {
        _isAdded = NO;
        if (_extensionsManager.menuControl.isMenuVisible) {
            [_extensionsManager.menuControl setMenuVisible:NO animated:YES];
        }
        self.annotImage = nil;
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.signature.pageIndex needRender:NO];
    } else {
        self.annotImage = image;
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.signature.pageIndex needRender:NO];
        [self showAnnotMenu];
    }
}

- (void)cancelSignature {
    if (DEVICE_iPHONE) {
        [self showAnnotMenu];
        
        if ([_extensionsManager.pdfViewCtrl isRMSProtected]) {
            NSMutableArray *signList = [AnnotationSignature getSignatureList];
            NSMutableArray *handlerList = [self getHandleSignArray:signList];
            if (handlerList.count <= 0) {
                [_extensionsManager setCurrentToolHandler:nil];
            }
        }else{
            if ([AnnotationSignature getSignatureList].count <= 0) {
                [_extensionsManager setCurrentToolHandler:nil];
            }
        }
    }
}

@end
