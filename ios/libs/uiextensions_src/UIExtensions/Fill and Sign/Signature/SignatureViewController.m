/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SignatureViewController.h"
#import "AlertView.h"
#import "AnnotationSignature.h"

#import "DigitalCertSelectCtr.h"
#import "SignatureView.h"
#import "TbBaseBar.h"
#import "UIExtensionsConfig+private.h"

@interface SignatureViewController ()

@property (nonatomic, strong) FSToolbar *topBar;
@property (nonatomic, strong) TbBaseBar *topBaseBar;
@property (nonatomic, strong) TbBaseItem *cancelItem;
@property (nonatomic, strong) TbBaseItem *clearItem;
@property (nonatomic, strong) TbBaseItem *saveItem;
@property (nonatomic, strong) TbBaseItem *propertyButton;
@property (nonatomic, strong) SignatureView *viewSignature;
@property (nonatomic, assign) int currentColor;
@property (nonatomic, assign) float currentLineWidth;
@property (nonatomic, strong) UIView *bottomBar;
@property (nonatomic, strong) UIButton *createCertBtn;
@property (nonatomic, copy) NSString *currentCertFileName;
@property (nonatomic, copy) NSString *currentCertPasswd;
@property (nonatomic, copy) NSString *currentCertMD5;
@property (nonatomic, strong) NSData *currentDib;
@property (nonatomic, assign) CGRect currentRectSigPart;
@property (nonatomic, assign) BOOL isContentCert;
@property (nonatomic, assign) BOOL isShowPropertyBar;

@property (nonatomic) BOOL statusBarHidden;

@end

@implementation SignatureViewController

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        self.extensionsManager = extensionsManager;
        _statusBarHidden = false;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSubView];
}

/*
- (void)viewDidUnload {
    [super viewDidUnload];
    self.viewSignature = nil;
}
*/

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.statusBarHidden = true;
    if (self.currentSignature) {
        [self loadSignature];
    }
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden {
    _statusBarHidden = statusBarHidden;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

#if !_MAC_CATALYST_
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (BOOL)shouldAutorotate{
    if (DEVICE_iPHONE) {
         NSDictionary *dict = [[NSBundle mainBundle] infoDictionary];
           NSArray *supportedInterfaceOrientations = dict[@"UISupportedInterfaceOrientations"];
           for (NSString *orientation in supportedInterfaceOrientations) {
               if ([orientation isEqualToString:@"UIInterfaceOrientationLandscapeLeft"] || [orientation isEqualToString:@"UIInterfaceOrientationLandscapeRight"]) {
                   return YES;
               }
           }
         return NO;
    }
    return YES;
}
#endif

- (void)initSubView {
    self.view.opaque = true;
    
    self.topBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
    [self.view addSubview:self.topBar];
    [self.topBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
    }];
    
    self.topBaseBar = [[TbBaseBar alloc] init];
    self.view.backgroundColor = ThemeViewBackgroundColor;
    [self.topBar addSubview:self.topBaseBar.contentView];
    [self.topBaseBar.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(self.topBaseBar.contentView.superview);
    }];

    self.cancelItem = [TbBaseItem createItemWithImage:ImageNamed(@"sign_cancel")];
    CGSize itemSize = self.cancelItem.contentView.frame.size;
    [self.topBaseBar.contentView addSubview:self.cancelItem.contentView];
    [self.cancelItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topBaseBar.contentView.mas_left).offset(10);
        make.centerY.mas_equalTo(self.topBaseBar.contentView.mas_centerY);
        make.width.mas_equalTo(itemSize.width);
        make.height.mas_equalTo(itemSize.height);
    }];

    typeof(self) __weak weakSelf = self;
    self.cancelItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf cancelSign];
    };
    
    self.saveItem = [TbBaseItem createItemWithImage:ImageNamed(@"sign_save")];
    itemSize = self.saveItem.contentView.frame.size;
    [self.topBaseBar.contentView addSubview:self.saveItem.contentView];
    [self.saveItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.topBaseBar.contentView.mas_right).offset(-10);
        make.centerY.mas_equalTo(self.topBaseBar.contentView.mas_centerY);
        make.width.mas_equalTo(itemSize.width);
        make.height.mas_equalTo(itemSize.height);
    }];
    self.saveItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf saveSign];
    };
    self.saveItem.enable = NO;

    self.clearItem = [TbBaseItem createItemWithImage:ImageNamed(@"sign_clear")];
    itemSize = self.clearItem.contentView.frame.size;
    [self.topBaseBar.contentView addSubview:self.clearItem.contentView];
    [self.clearItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.saveItem.contentView.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.topBaseBar.contentView.mas_centerY);
        make.width.mas_equalTo(itemSize.width);
        make.height.mas_equalTo(itemSize.height);
    }];
    self.clearItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf clearSign];
    };

    self.propertyButton = [TbBaseItem createItemWithImage:nil];
    self.propertyButton.contentView.fs_size = itemSize;
    self.propertyButton.button.fs_size = itemSize;
    [self.topBaseBar.contentView addSubview:self.propertyButton.contentView];
    [self.propertyButton.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.clearItem.contentView.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.topBaseBar.contentView.mas_centerY);
        make.width.mas_equalTo(itemSize.width);
        make.height.mas_equalTo(itemSize.height);
    }];
    self.propertyButton.onTapClick = ^(TbBaseItem *item) {
        [weakSelf propertySign:item];
    };

    UIView *divideView = [[UIView alloc] init];
    divideView.backgroundColor = UIColor_DarkMode([UIColor colorWithRed:0xE2 / 255.0f green:0xE2 / 255.0f blue:0xE2 / 255.0f alpha:1], BarDividingLineColor_Dark) ;
    [self.topBaseBar.contentView addSubview:divideView];
    [divideView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(divideView.superview.mas_left);
        make.height.mas_equalTo(DIVIDE_VIEW_THICKNES);
        make.bottom.mas_equalTo(divideView.superview.mas_bottom);
        make.right.mas_equalTo(divideView.superview.mas_right);
    }];

    [self setSignatureDefaultOption];
    //This two lines can make it fullscreen under ios7
    if (DEVICE_iPHONE) {
        self.extendedLayoutIncludesOpaqueBars = YES; //replace "self.wantsFullScreenLayout = YES;" by deprecation
    }

    // Do any additional setup after loading the view from its nib.

    self.viewSignature = [[SignatureView alloc] initWithPdfViewCtrl:self.extensionsManager.pdfViewCtrl];
    [self.view addSubview:self.viewSignature];

    self.viewSignature.signHasChangedCallback = ^(BOOL hasChanged) {
        if (hasChanged && [weakSelf.viewSignature getCurrentImage]) {
            weakSelf.saveItem.enable = YES;
        } else {
            weakSelf.saveItem.enable = NO;
        }
    };

    [self.viewSignature mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topBaseBar.contentView.mas_bottom);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        } else {
            make.bottom.left.right.equalTo(self.view);
        }
    }];

    self.bottomBar = [[UIView alloc] init];
    self.createCertBtn = [[UIButton alloc] init];

    self.bottomBar.backgroundColor = NormalBarBackgroundColor;
    [self.view addSubview:self.bottomBar];
    [self.bottomBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.bottomBar.superview.mas_bottom);
        make.left.mas_equalTo(self.bottomBar.superview.mas_left);
        make.right.mas_equalTo(self.bottomBar.superview.mas_right);
        make.height.mas_equalTo(49);
    }];

    if (![_extensionsManager.pdfViewCtrl isRMSProtected]) {
        if (self.currentSignature) {
            self.currentCertFileName = self.currentSignature.certFileName;
            self.currentCertMD5 = self.currentSignature.certMD5;
            self.currentCertPasswd = self.currentSignature.certPasswd;
        }
        
        if (self.currentCertFileName) {
            NSString *title = [FSLocalizedForKey(@"kCurrentSelectCert") stringByAppendingString:self.currentCertFileName];
            [self.createCertBtn setTitle:title forState:UIControlStateNormal];
        } else {
            [self.createCertBtn setTitle:FSLocalizedForKey(@"kSelectAddCert") forState:UIControlStateNormal];
        }
        
        [self.createCertBtn setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
        self.createCertBtn.titleLabel.font = [UIFont systemFontOfSize:18.f];
        self.createCertBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.createCertBtn addTarget:self action:@selector(selectCert) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomBar addSubview:self.createCertBtn];
        [_createCertBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self->_createCertBtn.superview.mas_bottom);
            make.top.mas_equalTo(self->_createCertBtn.superview.mas_top);
            make.centerX.mas_equalTo(self->_createCertBtn.superview.mas_centerX);
            make.width.mas_equalTo(300);
        }];
    }

    self.currentColor = [Preference getIntValue:[self getName] type:@"Color" defaultValue:_extensionsManager.config.defaultSettings.signature.color.rgbValue];
    self.currentLineWidth = [Preference getFloatValue:[self getName] type:@"Linewidth" defaultValue:_extensionsManager.config.defaultSettings.signature.thickness];
    self.viewSignature.color = self.currentColor;
    self.viewSignature.diameter = self.currentLineWidth < 2.0 ? 2.0:self.currentLineWidth;
    [self.propertyButton setInsideCircleColor:self.currentColor];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    self.statusBarHidden = false;
    if (self.viewSignature.hasChanged) {
        self.currentDib = [[self.viewSignature getCurrentDib] copy];
        self.currentRectSigPart = self.viewSignature.rectSigPart;
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
#if _MAC_CATALYST_
    [self.viewSignature setNeedsDisplay];
#endif

    if (self.extensionsManager.propertyBar.isShowing) {
        _isShowPropertyBar = YES;
        [self.extensionsManager.propertyBar dismissPropertyBar];
    } else {
        _isShowPropertyBar = NO;
    }
}

- (void)selectCert {
    DigitalCertSelectCtr *digitalCertSelectCtr = [[DigitalCertSelectCtr alloc] init];
    FSNavigationController *digitalCertNav = [[FSNavigationController alloc] initWithRootViewController:digitalCertSelectCtr];
    [digitalCertNav fs_setCustomTransitionAnimator:self.extensionsManager.presentedVCAnimator];
    digitalCertSelectCtr.title = FSLocalizedForKey(@"kSelectAddCert");
    [[Utility getTopMostViewController] presentViewController:digitalCertNav
                       animated:YES
                     completion:^{

                     }];
    typeof(self) __weak weakSelf = self;
    digitalCertSelectCtr.doneOperator = ^(NSString *path, NSString *passwd, NSString *md5) {
        NSString *title = [FSLocalizedForKey(@"kCurrentSelectCert") stringByAppendingString:[path lastPathComponent]];
        SignatureViewController *strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf->_createCertBtn setTitle:title forState:UIControlStateNormal];
        }
        weakSelf.currentCertFileName = [path lastPathComponent];
        weakSelf.currentCertPasswd = passwd;
        weakSelf.currentCertMD5 = md5;
        [AnnotationSignature setCertFileToSiganatureSpace:md5 path:path];
        weakSelf.viewSignature.hasChanged = YES;
        weakSelf.isContentCert = YES;
    };
    digitalCertSelectCtr.cancelOperator = ^(NSString *path, NSString *passwd, NSString *md5) {
    };
}

- (void)updateSignature {
    self.currentSignature.rectSigPart = self.viewSignature.rectSigPart;
    self.currentSignature.color = self.viewSignature.color;
    self.currentSignature.diameter = self.viewSignature.diameter;
    self.currentSignature.certFileName = self.currentCertFileName;
    self.currentSignature.certMD5 = self.currentCertMD5;
    self.currentSignature.certPasswd = self.currentCertPasswd;
    [self setSignatureImage:self.currentSignature.name];
    [self.currentSignature update];
    [AnnotationSignature setSignatureSelected:self.currentSignature.name];
}

- (void)setSignatureImage:(NSString *)name {
    UIImage *img = [self.viewSignature getCurrentImage];
    [AnnotationSignature setSignatureImage:name img:img];
    NSData *data = [self.viewSignature getCurrentDib];
    [AnnotationSignature setSignatureDib:name data:data];
}

- (void)loadSignature {
    NSData *data = [AnnotationSignature getSignatureDib:self.currentSignature.name];
    CGRect rectSigPart = self.currentSignature.rectSigPart;
    if (self.viewSignature.hasChanged) {
        data = self.currentDib;
        rectSigPart = self.currentRectSigPart;
        [self.viewSignature loadSignature:data rect:rectSigPart];
        self.viewSignature.hasChanged = YES;
    } else {
        [self.viewSignature loadSignature:data rect:rectSigPart];
    }
}

- (void)addNewSignature {
    AnnotationSignature *sig = [[AnnotationSignature alloc] init];
    sig.rectSigPart = self.viewSignature.rectSigPart;
    sig.color = self.viewSignature.color;
    sig.diameter = self.viewSignature.diameter;
    sig.certFileName = self.currentCertFileName;
    sig.certMD5 = self.currentCertMD5;
    sig.certPasswd = self.currentCertPasswd;
    NSString *newName = [sig add];
    [self setSignatureImage:newName];
    [AnnotationSignature setSignatureSelected:newName];
}

- (void)setSignatureDefaultOption {
    AnnotationSignature *option = [AnnotationSignature getSignatureOption];
    self.viewSignature.color = option.color;
    self.viewSignature.diameter = option.diameter;
}

#pragma mark - event methods

- (NSString *)getName {
    return Module_Signature;
}

- (void)propertySign:(TbBaseItem *)item {
    NSArray *colors = @[ @0xcc6666, @0x993333 ,@0x6f332e,@0x336666,@0x666633,@0x336600,@0x663366,@0x330066,@0x000066,@0x333366,@0x666600,@0x663333,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff ];
    [self.extensionsManager.propertyBar setColors:colors];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_LINEWIDTH frame:CGRectMake(0, SCREENHEIGHT, SCREENWIDTH, 500)];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:self.currentColor];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:self.currentLineWidth];
    [self.extensionsManager.propertyBar addListener:self];
    CGRect rect = [self.propertyButton.contentView convertRect:self.propertyButton.contentView.bounds toView:self.view];

    if (DEVICE_iPHONE) {
        [self.extensionsManager.propertyBar showPropertyBar:rect inView:self.view viewsCanMove:nil];
    } else {
        [self.extensionsManager.propertyBar showPropertyBar:item.contentView.bounds inView:item.contentView viewsCanMove:nil];
    }
}

#pragma mark topBaseBar method

- (void)cancelSign {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO
                                 completion:^{
                                     if (self.cancelHandler) {
                                         self.cancelHandler();
                                     }
                                 }];
    });
}

- (void)clearSign {
    [self.viewSignature clear];
    self.viewSignature.hasChanged = YES;
}

- (void)saveSign {
    if (self.viewSignature.hasChanged) {
        if (self.currentSignature == nil) //new
        {
            if (_isFieldSig) {
                if (_isContentCert) {
                    [self addNewSignature];
                } else {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFieldSignWithoutCert") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:action];
                    [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
                    return;
                }
            } else {
                [self addNewSignature];
            }
        } else //edit
        {
            [self updateSignature];
        }
    }

    [self dismissViewControllerAnimated:NO
                             completion:^{
                                 if (self.saveHandler) {
                                     self.saveHandler();
                                 }
                             }];
}

- (UIImage *)getImageWithWarning {
    UIImage *img = [self.viewSignature getCurrentImage];
    if (!img) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kSignatureEmpty") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
    }
    return img;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    if (DEVICE_iPHONE) {
//        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
//    }
//    return YES;
//}

//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if (DEVICE_iPHONE) {
//        return UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskLandscapeLeft;
//    }
//    return UIInterfaceOrientationMaskAll;
//}

//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    if (_isShowPropertyBar) {
//        [self.extensionsManager.propertyBar dismissPropertyBar];
//    }
//}
//
//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    if (_isShowPropertyBar) {
//        [self.extensionsManager.propertyBar dismissPropertyBar];
//    }
//}

#pragma mark -IPropertyValueChangedListener

- (void)onProperty:(long)property changedFrom:(NSValue *)oldValue to:(NSValue *)newValue {
    if (property == PROPERTY_COLOR) {
        int color = ((NSNumber *) newValue).intValue;
        self.viewSignature.color = color;
        self.currentColor = color;
        [Preference setIntValue:[self getName] type:@"Color" value:color];
        [self.propertyButton setInsideCircleColor:color];
    }
    if (property == PROPERTY_LINEWIDTH) {
        float f = ((NSNumber *) newValue).floatValue; // 1~12
        self.viewSignature.diameter = f;
        self.currentLineWidth = f;
        [Preference setFloatValue:[self getName] type:@"Linewidth" value:f];
    }
}

@end
