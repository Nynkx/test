/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SignatureModule.h"
#import "DigitalSignatureAnnotHandler.h"

#import "Utility.h"
#import "UIButton+Extensions.h"
#import "UIExtensionsConfig.h"
#import "CerTrustedListViewCtr.h"

#define isPhoneXTopHeight IS_IPHONE_OVER_TEN ? 88 : 64
@interface SignatureModule ()<MoreMenuItemAction>
@property (nonatomic, strong) TbBaseItem *signItem;

@property (nonatomic, strong) FSToolbar *topToolBar;
@property (nonatomic, strong) FSToolbar *bottomToolBar;
@property (nonatomic, assign) int oldState;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) TbBaseItem *listItem;
@property (nonatomic, strong) UIButton *signatureButton;
@property (nonatomic, assign) BOOL toolBarHiden;

@end

@implementation SignatureModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Signature";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
        SignToolHandler* toolHandler = [[SignToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        [_extensionsManager registerRotateChangedListener:toolHandler];
        [_extensionsManager.pdfViewCtrl registerDocEventListener:toolHandler];
        
        DigitalSignatureAnnotHandler* annotHandler = [[DigitalSignatureAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_pdfViewCtrl registerScrollViewEventListener:annotHandler];
        [_extensionsManager registerRotateChangedListener:annotHandler];
        [_extensionsManager registerAnnotHandler:annotHandler];
        self.toolHandler = (SignToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Signature];
        [_pdfViewCtrl registerDocEventListener:self];
    }
    return self;
}

- (void)loadModule {
    [self initToolBar];
    [_extensionsManager registerToolEventListener:self];
    [_extensionsManager registerStateChangeListener:self];
    
    NSMutableArray<UIBarButtonItem *> *tmpArray = _extensionsManager.bottomToolbar.items.mutableCopy;
    
    if (_extensionsManager.config.loadSignature) {
        self.signatureButton = [self createButtonWithTitle:FSLocalizedForKey(@"kSignatureTitle") image:[UIImage imageNamed:@"annot_sign" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        self.signatureButton.contentEdgeInsets = UIEdgeInsetsMake((CGRectGetHeight(_extensionsManager.bottomToolbar.frame)-CGRectGetHeight(self.signatureButton.frame))/2, 0, 0, 0);
        self.signatureButton.tag = FS_BOTTOMBAR_ITEM_SIGNATURE_TAG;
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.signatureButton];
        item.tag = FS_BOTTOMBAR_ITEM_SIGNATURE_TAG;
        [tmpArray addObject:item];
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [tmpArray addObject:flexibleSpace];
        
        [self.signatureButton addTarget:self action:@selector(onTapSignatureButton:) forControlEvents:UIControlEventTouchUpInside];
        
        MoreMenuGroup *group = [_extensionsManager.more getGroup:TAG_GROUP_PROTECT];
        if (!group) {
            group = [[MoreMenuGroup alloc] init];
            group.title = FSLocalizedForKey(@"kSecurity");
            group.tag = TAG_GROUP_PROTECT;
            [_extensionsManager.more addGroup:group];
        }
        
        MoreMenuItem *cerItem = [[MoreMenuItem alloc] init];
        cerItem.tag = TAG_ITEM_CERTIFICATE;
        cerItem.text = FSLocalizedForKey(@"kTrustedCert");
        cerItem.callBack = self;
        [_extensionsManager.more addMenuItem:TAG_GROUP_PROTECT withItem:cerItem];
    }
    _extensionsManager.bottomToolbar.items = tmpArray;
}

#pragma mark - MoreMenuItemAction
- (void)onClick:(MoreMenuItem *)item {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
    CerTrustedListViewCtr *VC = [storyboard instantiateViewControllerWithIdentifier:@"CerTrustedListViewCtr"];
    FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:VC];
    [nav fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    [_pdfViewCtrl.fs_viewController presentViewController:nav animated:YES completion:nil];
}

- (void)onTapSignatureButton:(UIButton *)button {
    [_extensionsManager changeState:STATE_SIGNATURE];
    
//    if (_extensionsManager.currentAnnot) {
//        [_extensionsManager setCurrentAnnot:nil];
//    }
//    [_extensionsManager setCurrentToolHandler:self.toolHandler];
//    [self.toolHandler openCreateSign];
}

- (void)initToolBar {
    UIView *superView = _pdfViewCtrl;
    _topToolBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];

    _cancelBtn = [[UIButton alloc] init];
    [_cancelBtn setImage:[UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [_cancelBtn addTarget:self action:@selector(cancelSignature) forControlEvents:UIControlEventTouchUpInside];
    [_topToolBar addSubview:_cancelBtn];

    TbBaseItem *titleItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kSignatureTitle")];
    titleItem.textColor = BarLikeBlackTextColor;
    [_topToolBar addSubview:titleItem.contentView];
    CGSize size = titleItem.contentView.frame.size;
    [titleItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(titleItem.contentView.superview);
        make.size.mas_equalTo(size);
    }];

    _bottomToolBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];
    
    _listItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kSignListIconTitle") imageNormal:[UIImage imageNamed:@"sign_list" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"sign_list" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"sign_list" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] background:nil imageTextRelation:RELATION_BOTTOM];
    _listItem.textColor = BlackThemeTextColor;
    _listItem.textFont = [UIFont systemFontOfSize:12.f];
    [_bottomToolBar addSubview:_listItem.contentView];
    
    __weak typeof(self) weakSelf = self;
    _listItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf.toolHandler delete];
        [weakSelf.toolHandler signList];
    };
    
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(26);
        make.left.mas_equalTo(self->_cancelBtn.superview).offset(10);
        make.centerY.mas_equalTo(titleItem.contentView);
    }];

    [_listItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self->_listItem.contentView.frame.size);
        make.center.mas_equalTo(self->_listItem.contentView.superview);
    }];

    [superView addSubview:_topToolBar];
    [superView addSubview:_bottomToolBar];

    [self setToolBarHiden:YES animation:NO];
}

- (void)cancelSignature {
    [self.toolHandler delete];
    [_extensionsManager setCurrentToolHandler:nil];
    [_extensionsManager changeState:STATE_NORMAL];
}

- (void)setToolBarHiden:(BOOL)toolBarHiden{
    [self setToolBarHiden:toolBarHiden animation:YES];
}

- (void)setToolBarHiden:(BOOL)toolBarHiden animation:(BOOL)animation{
    if (_toolBarHiden == toolBarHiden) {
        return;
    }
    if (toolBarHiden) {
        [_topToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self->_pdfViewCtrl);
            make.bottom.equalTo(self->_pdfViewCtrl.mas_top);
        }];
        
        [_bottomToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self->_pdfViewCtrl);
            make.top.equalTo(self->_pdfViewCtrl.mas_bottom);
        }];
    } else {
        [_topToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.left.right.equalTo(self->_pdfViewCtrl);
        }];
        
        [_bottomToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self->_pdfViewCtrl);
        }];
    }
    
    _toolBarHiden = toolBarHiden;
    
    if (animation) {
        [UIView animateWithDuration:0.3f animations:^{
            [self->_pdfViewCtrl layoutIfNeeded];
        }];
    }
}

#pragma mark IHandlerEventListener

- (void)onToolChanged:(NSString *)lastToolName CurrentToolName:(NSString *)toolName {
    if ([toolName isEqualToString:Tool_Signature]) {
        [self annotItemClicked];
    } else if ([lastToolName isEqualToString:Tool_Signature]) {
        [self setToolBarHiden:YES];
        if (toolName == nil && [_extensionsManager getState] != STATE_PANZOOM) {
            [_extensionsManager changeState:STATE_NORMAL];
        }
    }
}

- (void)annotItemClicked {
    [_extensionsManager.toolSetBar removeAllItems];
}

- (void)setToolBarItemHidden:(BOOL)toolBarItemHidden {
    if (toolBarItemHidden && self.signItem) {
        [_extensionsManager.toolSetBar removeItem:self.signItem];
    } else {
        if (!self.signItem) {
            self.signItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"sign_list" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] background:[UIImage imageNamed:@"annotation_toolitembg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
            self.signItem.tag = 3;
            UIExtensionsManager *extensionsManager = _extensionsManager; // avoid strong-reference to self
            self.signItem.onTapClick = ^(TbBaseItem *item) {
                SignToolHandler *signToolHandler = (SignToolHandler *) [extensionsManager getToolHandlerByName:Tool_Signature];
                [signToolHandler signList];
            };
        }
        [_extensionsManager.toolSetBar addItem:self.signItem displayPosition:Position_CENTER];
    }
}

#pragma mark - IStateChangeListener

- (void)onStateChanged:(int)state {
    if (state == STATE_SIGNATURE) {
        [self setToolBarHiden:NO];
    } else {
        [self setToolBarHiden:YES];
    }
}

#pragma mark <IDocEventListener>
- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    if (document) {
        self.signatureButton.enabled = [Utility canAddSignToDocument:[_pdfViewCtrl currentDoc]] && [Utility canAddSign:_pdfViewCtrl] && ![_pdfViewCtrl isDynamicXFA] && [Utility canFillForm:_pdfViewCtrl];
    }
}

#pragma mark create button
- (UIButton *)createButtonWithTitle:(NSString *)title image:(UIImage *)image {
    UIFont *textFont = [UIFont systemFontOfSize:9.f];
    CGSize titleSize = [Utility getTextSize:title fontSize:textFont.pointSize maxSize:CGSizeMake(400, 100)];
    float width = image.size.width;
    float height = image.size.height;
    CGRect frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width : width, titleSize.height + height);
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [button setEnlargedEdge:ENLARGE_EDGE];
    
    [button setTitle:title forState:UIControlStateNormal];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height, 0);
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = textFont;
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
    
    [button setImage:image forState:UIControlStateNormal];
    UIImage *translucentImage = [Utility imageByApplyingAlpha:image alpha:0.5];
    [button setImage:translucentImage forState:UIControlStateHighlighted];
    [button setImage:translucentImage forState:UIControlStateDisabled];
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    
    return button;
}


@end
