/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FillSignTextObjInfo : NSObject

@property (nonatomic,assign) void* pdfFont;
@property (nonatomic,assign) float fontSize;
@property (nonatomic,assign) CGPoint position;
@property (nonatomic,strong) NSMutableArray *charcodes;
@property (nonatomic,strong) NSMutableArray *kernings;

@end

NS_ASSUME_NONNULL_END
