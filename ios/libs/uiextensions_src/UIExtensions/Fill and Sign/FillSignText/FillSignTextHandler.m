/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignTextHandler.h"
#import "FSTextView.h"
#import "FSTextMatchingContainer.h"
#import "FillSignModule.h"
#import "FillSignTextObjInfo.h"
#import "PrivateDefine.h"
#import <CoreText/CTFont.h>
#import "FillSignToolHandler.h"
#import "IQKeyboardManager.h"

#define NORMALSIZE 10
#define MINSIZE 1
#define MAXSIZE 32
#define RATESIZE 1.1

#define TEXT_FONT_SIZE @"TEXT_FONT_SIZE"
#define TEXT_CHARSPACE_SIZE @"TEXT_CHARSPACE_SIZE"

@interface FillSignTextHandler ()<UITextViewDelegate, IScrollViewEventListener>
@property (nonatomic, strong) FSTextView *textview;
@property (nonatomic, weak) FSTextMatchingContainer *matchContainer;
@property (nonatomic, strong) NSArray *preDefineContentArray;
@property (nonatomic, assign) BOOL needGenerate;
@property (nonatomic, assign) CGFloat kerningWidth;

@property (nonatomic) CGPoint lastPt;
@property (nonatomic) BOOL isOnCtrolPt;
@property (nonatomic, strong) NSMutableArray *oriTextObjInfos;
@property (nonatomic, assign) CGRect oriRect;
@property (nonatomic, assign) float oriKerning;
@property (nonatomic) BOOL ismodify;
@property (nonatomic, weak) NSMutableDictionary *fillSignPreferences;
@property (nonatomic, strong) FSRectF *rotateRectf;
@property (nonatomic, strong) FSRectF *zoomRectf;
@end

@implementation FillSignTextHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex fillSignType:(FSFillSignType)type{
    self = [super initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex];
    if (self) {
        
        FillSignToolHandler *toolHandler = (FillSignToolHandler *)[extensionsManager getToolHandlerByName:Tool_Fill_Sign];
        _fillSignPreferences = toolHandler.fillSignPreferences;
        
        self.type = type;
        _needGenerate = YES;
        
        _textview = [[FSTextView alloc] init];
        _textview.getTextView.backgroundColor = [UIColor fs_clearColor];
        _textview.getTextView.layer.borderColor = ThemeLikeBlueColor.CGColor;
        _textview.getTextView.layer.borderWidth = 1.0;
        
        _textview.getTextView.userInteractionEnabled = NO;
        if (type == FSFillSignTypeText || type == FSFillSignTypeProfile) {
            //_textview.getTextView.font = [UIFont fontWithName:@"Helvetica" size:NORMALSIZE];
        }else{
            //_textview.getTextView.font = [UIFont fontWithName:@"Courier" size:NORMALSIZE];
            float charspace = [self getFillSignPreferenceFloatValue:TEXT_CHARSPACE_SIZE defaultValue:4.2];
            _textview.isSegText = YES;
            _kerningWidth = charspace;
            self.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        
        float fontszie =  [self getFillSignPreferenceFloatValue:TEXT_FONT_SIZE defaultValue:NORMALSIZE];
        CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontszie, fontszie), self.pdf2Pagetransform);
        CGFloat viewfontsize = widthRect.size.width;
        _textview.getTextView.font = [UIFont fontWithName:@"Courier" size:viewfontsize];
        _textview.getTextView.textColor = UIColorHex(0x000000);
        
        //_textview.getTextView.layer.anchorPoint = CGPointMake(0.0, 0.0);
        //_textview.getTextView.center = CGPointMake(0.0, 0.0);
        _textview.getTextView.delegate = self;
        _textview.getTextView.scrollEnabled = NO;
        
        FSTextMatchingContainer *matchContainer = [[FSTextMatchingContainer alloc] init];
        _matchContainer = matchContainer;
        
        @weakify(self)
        _matchContainer.itemClickCall = ^(NSString * _Nonnull str) {
            @strongify(self)
            UITextView *textView = self.textview.getTextView;
            textView.text = str;
            [self textViewDidChange:textView];
        };
        _matchContainer.doneClickCall = ^{
            @strongify(self)
            [self fillSignToolDeactiveFillSign];
        };
        _matchContainer.bounds = CGRectMake(0, 0, SCREENWIDTH, 49);
        [_matchContainer setBackgroundColor:NormalBarBackgroundColor];
        _textview.getTextView.inputAccessoryView = _matchContainer;
        
        [_textview.getTextView sizeToFit];
        
        UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
        [pageView addSubview:_textview.getTextView];
        
        [[IQKeyboardManager sharedManager] setEnable:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasShown:)
                                                     name:UIKeyboardDidShowNotification object:nil];
        _oriTextObjInfos = [NSMutableArray array];
        _ismodify = NO;
        [self.pdfViewCtrl registerScrollViewEventListener:self];
        _isAddText = YES;
    }
    return self;
}



- (void)dealloc{
    [self.textview.getTextView removeFromSuperview];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.pdfViewCtrl setBottomOffset:0];
}

- (BOOL)canResize{
    return NO;
}

- (void)switchTextType:(FSFillSignType)type{
    self.type = type;
    UITextView *textView = self.textview.getTextView;
    NSString *text = textView.text;
    [self setContent:@""];
    if (type == FSFillSignTypeText) {
        _textview.isSegText = NO;
        _kerningWidth = 0;
        self.edgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
        [self.textview setKerningWidth:0];
    }else{
        float charspace = [self getFillSignPreferenceFloatValue:TEXT_CHARSPACE_SIZE defaultValue:4.2];
        _textview.isSegText = YES;
        _kerningWidth = charspace;
        self.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        
        CGRect widthRect = CGRectMake(0, 0, _kerningWidth, _kerningWidth);
        CGFloat viewkering = widthRect.size.width;
        [self.textview setKerningWidth:viewkering];
    }
    [self setContent:text];
    [self textViewDidChange:self.textview.getTextView];
}

- (void)deactiveFillHandler{
    if (!_isAddText) return;
    if (!_needGenerate && self.type != FSFillSignTypeProfile) {
        if (!self.textview.getTextView.text.length){
            [super deactiveFillHandler];
            return;
        }
    }
    [self removeFillSignItem: YES];
    [super deactiveFillHandler];
    FSPDFDoc *pPdfDoc = [self.pdfViewCtrl currentDoc];
    FSPDFPage *pPage = [pPdfDoc getPage:self.pageIndex];
    if (!pPage || [pPage isEmpty]) return;

    NSMutableArray *textinfos = [NSMutableArray array];
    
    FSTextFillSignObject *textFillSignObject = [self.textview generatePDFTextWithFillSignHandler:self rect:self.rect roation:self.rotation textinfo:textinfos];
    if ([textFillSignObject isEmpty] || !textFillSignObject) {
        return;
    }
    [self setFillSignSegInfo:textFillSignObject];
    _pdfFillSignObj = textFillSignObject;
    [textFillSignObject generateContent];
    _needGenerate = NO;
//    self.extensionsManager.isDocModified = YES;
}

- (NSString *)getContent
{
    return self.textview.getTextView.text;
}

- (NSArray *)preDefineContentArray
{
    if (_preDefineContentArray == nil) {
        id<IModule> module = [self.extensionsManager getModuleByName:Module_Fill_Sign];
        FillSignModule *signatureModule = (FillSignModule *)module;
        _preDefineContentArray = [NSArray arrayWithArray:[signatureModule getPreDefineContentArray]];
    }
    return _preDefineContentArray;
}

- (void)setContent:(NSString *)str;
{
    self.textview.getTextView.text = str;
    [self.textview.getTextView sizeToFit];
    [self textViewDidChange:_textview.getTextView];
    
}

- (float)getFillSignPreferenceFloatValue:(NSString *)type defaultValue:(float)defaultValue{
    NSNumber *value = _fillSignPreferences[type];
    if (!value || ![value isKindOfClass:[NSNumber class]]) return defaultValue;
    return value.floatValue;
}

- (void)setFillSignPreferenceFloatValue:(NSString *)type value:(float)value{
    _fillSignPreferences[type] = @(value);
}

- (CGSize)getFormItemSize:(CGFloat)xoffset position:(FillSignXPosition)position
{
    CGSize itemSize = [super getFormItemSize:xoffset position:position];
    if (CGSizeEqualToSize(itemSize, CGSizeZero)) {
        UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
        CGRect rect = pageView.bounds;
        CGFloat maxWidth;
        if (position == FillSignXPosition_Left) {
            maxWidth = rect.size.width - xoffset;
        }else{
            CGFloat w = MIN(ABS(xoffset), ABS(rect.size.width - xoffset));
            maxWidth = 2 * w;
        }
        itemSize = CGSizeMake(maxWidth, CGFLOAT_MAX);
    }
    CGSize size = [_textview.getTextView sizeThatFits:itemSize];
    CGSize pdfSize = CGSizeApplyAffineTransform(size, self.pdf2PagetransformInvert);
    pdfSize.width = ABS(pdfSize.width);
    pdfSize.height = ABS(pdfSize.height);
    return pdfSize;
}

- (CGFloat)getFontSize
{
    CGFloat fontSize = self.textview.getTextView.font.pointSize;
    return fontSize;
}

- (void)setFontSize:(CGFloat)fontSize
{
    self.textview.getTextView.font = [UIFont fontWithName:@"Courier" size:fontSize];
}

- (void)updateFillSignObjPosition{
    if ([_fillSign isEmpty] || !_fillSign) return;
    if (NULL != _pdfFillSignObj) {
        FSTextFillSignObject *textFillSignObject = [[FSTextFillSignObject alloc] initWithFillsign_object:_pdfFillSignObj];
        FSPointF *movePointf = [[FSPointF alloc] initWithX:CGRectGetMinX(self.PDFRect) Y:CGRectGetMinY(self.PDFRect)];
        FSFillSignObject *fillSignObject = [_fillSign addTextObject:textFillSignObject.getTextDataArray point:movePointf width:CGRectGetWidth(self.rect) height:CGRectGetHeight(self.rect) rotation:self.rotation is_comb_field_mode:textFillSignObject.isCombFieldMode];
        
        if ([fillSignObject isEmpty] || !fillSignObject) return;
        [self removeFillSignItem: NO];
        [fillSignObject generateContent];
        [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
        _pdfFillSignObj = fillSignObject;
    }
}

- (void)smallerCall
{
    _needGenerate = YES;
    _ismodify = YES;
    
    CGFloat fontSize = self.textview.getTextView.font.pointSize;
    CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSize, fontSize), self.pdf2PagetransformInvert);
    CGFloat fontSizesize = widthRect.size.width;
    fontSizesize /= RATESIZE;
    CGRect widthRect1 = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSizesize, fontSizesize), self.pdf2Pagetransform);
    CGFloat fontSizesizeView = widthRect1.size.width;
    
    _textview.getTextView.font = [UIFont fontWithName:@"Courier" size:fontSizesizeView];
    [self textViewDidChange:_textview.getTextView];
    [self updateFillSignObjPosition];
    [super smallerCall];
    [self setFillSignPreferenceFloatValue:TEXT_FONT_SIZE value:fontSize];
    self.extensionsManager.isDocModified = YES;
}

- (void)biggerCall
{
    _needGenerate = YES;
    _ismodify = YES;
    
    CGFloat fontSize = self.textview.getTextView.font.pointSize;
    CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSize, fontSize), self.pdf2PagetransformInvert);
    CGFloat fontSizesize = widthRect.size.width;
    fontSizesize *= RATESIZE;
    CGRect widthRect1 = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSizesize, fontSizesize), self.pdf2Pagetransform);
    CGFloat fontSizesizeView = widthRect1.size.width;

    _textview.getTextView.font = [UIFont fontWithName:@"Courier" size:fontSizesizeView];
    [self textViewDidChange:_textview.getTextView];
    [self updateFillSignObjPosition];
    [super biggerCall];
    [self setFillSignPreferenceFloatValue:TEXT_FONT_SIZE value:fontSizesize];
    self.extensionsManager.isDocModified = YES;
}

- (BOOL)canBiggerEnable:(FillSignMenu *)menu inView:(UIView *)inView
{
    CGFloat fontSize = self.textview.getTextView.font.pointSize;
    CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSize, fontSize), self.pdf2PagetransformInvert);
    CGFloat pdffontsize = widthRect.size.width;
    pdffontsize *= RATESIZE;
    if (pdffontsize > MAXSIZE) return NO;
    return YES;
}

- (BOOL)canSmallerEnable:(FillSignMenu *)menu inView:(UIView *)inView
{
    CGFloat fontSize = self.textview.getTextView.font.pointSize;
    CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSize, fontSize), self.pdf2PagetransformInvert);
    CGFloat pdffontsize = widthRect.size.width;
    pdffontsize /= RATESIZE;
    if (pdffontsize < MINSIZE) return NO;
    return YES;
}

- (void)resetPosition
{
    CGAffineTransform pretransform = self.pdf2PagetransformInvert;
    [super resetPosition];
    CGFloat fontSize = self.textview.getTextView.font.pointSize;
    CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontSize, fontSize), pretransform);
    CGFloat pdffontsize = widthRect.size.width;
    
    CGRect viewFontRect = CGRectApplyAffineTransform(CGRectMake(0, 0, pdffontsize, pdffontsize), self.pdf2Pagetransform);
    CGFloat viewfontsize = viewFontRect.size.width;
    
    if (self.type == FSFillSignTypeSegText) {
        [self setInitKerningWidth];
    }
    _textview.getTextView.font = [UIFont fontWithName:@"Courier" size:viewfontsize];
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.textview.getTextView.frame = frame;
    [self updateMenu];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.textview.getTextView.frame = self.frame;
    [self updateMenu];
}

- (void)setCenter:(CGPoint)center{
    [super setCenter:center];
    self.textview.getTextView.frame = self.frame;
}

- (void)updateMenu{
    CGRect rect = self.frame;
    rect = CGRectInset(rect, -15, -15);
    FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
    UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
    if (fillSignMenu.isMenuVisible) {
        [fillSignMenu updateTargetRect:rect inView:pageView];
    }else{
        FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:self.type];
        [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
        [fillSignMenu setMenuVisible:YES animated:NO];
    }
    
}

- (void)onTapGesture:(UITapGestureRecognizer *)guest
{
    if (self.status != FillSignEditStatus_Editing) {
        [self activeKeyboard];
    }
}

- (void)onPanGesture:(UIPanGestureRecognizer *)guest endGesture:(BOOL)end
{
    _needGenerate = YES;
    _ismodify = YES;
    if (self.type == FSFillSignTypeSegText) {
        UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
        CGPoint point = [guest locationInView:pageView];
        FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
        CGRect textrect = self.textview.getTextView.bounds;
        float width = 15;
        CGRect rect1 = CGRectMake(self.frame.origin.x + self.frame.size.width - width, self.frame.origin.y + self.frame.size.height * 0.5 - width, width * 2, width * 2);
        
        if (guest.state == UIGestureRecognizerStateBegan) {
            _lastPt = point;
            _isOnCtrolPt = NO;
            if (CGRectContainsPoint(rect1, point) ) {
                _isOnCtrolPt = YES;
            }
            
            if (_isOnCtrolPt) {
                self.isOnchange = YES;
                [fillSignMenu setMenuVisible:NO animated:NO];
                [self showMagnifier:self.pageIndex point:point];
            }else{
                if (self.status != FillSignEditStatus_Editing) [super onPanGesture:guest endGesture:end];
            }
        }else if (guest.state == UIGestureRecognizerStateChanged) {
            if (_isOnCtrolPt) {
                if (!self.isOnchange)
                {
                    [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
                    return;
                }
                CGPoint offset = CGPointMake(point.x - _lastPt.x, point.y - _lastPt.y);
                CGPoint fp1 = CGPointApplyAffineTransform(point, self.pdf2PagetransformInvert);
                CGPoint fp2 = CGPointApplyAffineTransform(_lastPt, self.pdf2PagetransformInvert);

                NSInteger count = [self.textview getLineMaxWord];
                if (count > 1) {
                    CGFloat kening = (fp1.x - fp2.x) / (count - 1) + self.kerningWidth;
                    if (kening > 1.0) {
                        [self setKerningWidth:kening];
                        CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, _kerningWidth, _kerningWidth), self.pdf2Pagetransform);
                        CGFloat viewkering = widthRect.size.width;
                        [self.textview setKerningWidth:viewkering];
                        
                        NSString *text = [self getContent];
                        [self setContent:@""];
                        [self setContent:text];
                        
                        CGSize size = self.bounds.size;
                        self.bounds = CGRectMake(0, 0, size.width, size.height);
                        [self textViewDidChange:self.textview.getTextView];
                    }
                }
                
                _lastPt = point;
                [self moveMagnifier:self.pageIndex point:point];
            }else{
                if (self.status != FillSignEditStatus_Editing) [super onPanGesture:guest endGesture:end];
            }
        }else if (guest.state == UIGestureRecognizerStateEnded){
            if (_isOnCtrolPt) {
                if (!self.isOnchange)
                {
                    [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
                    return;
                }
                self.isOnchange = NO;
                [self textViewDidChange:self.textview.getTextView];
                [self updateFillSignObjPosition];
                
                CGRect rect = self.frame;
                rect = CGRectInset(rect, -15, -15);
                FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:self.type];
                [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
                [fillSignMenu setMenuVisible:YES animated:NO];
                
                self.extensionsManager.isDocModified = YES;
                _lastPt = CGPointZero;
                [self closeMagnifier];
            }else{
                if (self.status != FillSignEditStatus_Editing) [super onPanGesture:guest endGesture:end];
            }
        }
        [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
        
        if (end) {
            if (_isOnCtrolPt) {
                self.isOnchange = NO;
                [self updatePDFRect];
                [self updateFillSignObjPosition];
                
                CGRect rect = self.frame;
                rect = CGRectInset(rect, -15, -15);
                FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:self.type];
                [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
                [fillSignMenu setMenuVisible:YES animated:NO];
                
                self.extensionsManager.isDocModified = YES;
                _lastPt = CGPointZero;
                [self closeMagnifier];
            }
        }
    }else{
        [super onPanGesture:guest endGesture:end];
    }
}

- (BOOL)isHitFillSignObjWithPoint:(CGPoint)point{
    return [super isHitFillSignObjWithPoint:point];
}

- (void)setKerningWidth:(CGFloat)kerningWidth
{
    _kerningWidth = kerningWidth;
    if (kerningWidth > 1) {
        [self setFillSignPreferenceFloatValue:TEXT_CHARSPACE_SIZE value:kerningWidth];
    }
}

- (void)activeKeyboard
{
    if (_textview != nil) {
        [_textview.getTextView becomeFirstResponder];
        self.status = FillSignEditStatus_Editing;
        self.textview.getTextView.userInteractionEnabled = YES;
    }
}

- (NSArray *)matchContentResultByStr:(NSString *)str
{
    if (str.length < 2) {
        return nil;
    }
    
    NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:self.preDefineContentArray];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy/MM/dd";
    NSString *dateString = [df stringFromDate:[NSDate date]];
    [tmpArr addObject:dateString];
    
    if (tmpArr.count < 1) {
        return nil;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *value in tmpArr) {
        NSString *retStr = [value commonPrefixWithString:str options:NSCaseInsensitiveSearch];
        if ([[retStr lowercaseString] isEqualToString:[str lowercaseString]]) {
            [array addObject:[NSString stringWithString:value]];
        }
    }
    return array;
}

- (void)textViewDidChange:(UITextView *)textView
{
    _needGenerate = YES;
    _ismodify = YES;
    NSArray *matchArr = [self matchContentResultByStr:textView.text];
    [_matchContainer setMatchingContent:matchArr];

    UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
    CGSize size = [_textview.getTextView sizeThatFits:CGSizeMake((pageView.bounds.size.width - CGRectGetMinX(self.frame)) - self.edgeInsets.right, CGFLOAT_MAX)];
    if (CGSizeEqualToSize(size, _textview.getTextView.bounds.size)) {
        [self updateMenu];
        return;
    }
    if (size.width > (pageView.bounds.size.width - CGRectGetMinX(self.frame)) - self.edgeInsets.right) size.width = (pageView.bounds.size.width - CGRectGetMinX(self.frame)) - self.edgeInsets.right;
    if (size.height > (pageView.bounds.size.height - CGRectGetMinY(self.frame)) - self.edgeInsets.bottom) {
        CGRect frame = self.frame;
        frame.origin.y -= (size.height - (pageView.bounds.size.height - CGRectGetMinY(self.frame) - self.edgeInsets.bottom));
        self.frame = frame;
        [self.pdfViewCtrl setBottomOffset:49];
    }
    self.bounds = CGRectMake(0, 0, size.width, size.height);
    [self updatePDFRect];
    [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@""]) {
        return YES;
    }
    NSString *regex = @"[^\\u0000-\\uFFFF]";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    self.extensionsManager.isDocModified = YES;
    return ![pred evaluateWithObject:text];
}

- (void)updateFillSignObj:(void *)fillSignObj
{

}

- (void)setFillSignObj:(FSFillSignObject *)fillSignObject{
    [super setFillSignObj:fillSignObject];
    _needGenerate = NO;
    if (!fillSignObject || [fillSignObject isEmpty]) return;
    
    [self setFillSignSegInfo:fillSignObject];
    _oriRect = self.PDFRect;
    _oriKerning = _kerningWidth;
    
    NSAttributedString *str = [self getTextObjsBy:fillSignObject];
    [self.textview.getTextView setAttributedText:str];
    
    if (str.length) {
        UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
        CGSize size = [_textview.getTextView sizeThatFits:CGSizeMake((CGRectGetWidth(pageView.bounds) - CGRectGetMinX(self.frame)) - self.edgeInsets.right, CGFLOAT_MAX)];
        self.bounds = CGRectMake(0, 0, size.width, size.height);
        [self updatePDFRect];
    }
    
    [self removeFillSignItem: NO];

    _needGenerate = YES;
}

- (void)setFillSignSegInfo:(FSFillSignObject *)fillSignObject{
    if (self.type == FSFillSignTypeSegText) {
        if ([fillSignObject isEmpty]) return;
        
        FSTextFillSignObject *textFillSignObject = [[FSTextFillSignObject alloc] initWithFillsign_object:fillSignObject];
        if (![textFillSignObject isEmpty]) {
            FSTextFillSignObjectDataArray *textDataArr = textFillSignObject.getTextDataArray;
            for (int i = 0; i < textDataArr.getSize; i ++) {
                FSTextFillSignObjectData *textFillSignObjectData = [textDataArr getAt:i];
                FSTextState *textState = textFillSignObjectData.text_state;
                if (textState.charspace) {
                    _kerningWidth = textState.charspace;
                    break;
                }
            }
            [self setInitKerningWidth];
        }
    }
}

- (void)setInitKerningWidth
{
    if (self.type == FSFillSignTypeSegText) {
        CGRect widthRect = CGRectMake(0, 0, _kerningWidth, _kerningWidth);
        CGFloat viewkering = widthRect.size.width;
        [self.textview setKerningWidth:viewkering];
    }
}

- (void)removeFillSignItem:(BOOL)notify
{
    _needGenerate = NO;
    [super removeFillSignItem:notify];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
}

-(void)onDraw:(int)pageIndex inContext:(CGContextRef)context
{
    if (!self.textview.getTextView.superview && self.pageIndex == pageIndex) {
        [self resetPosition];
        [[self.pdfViewCtrl getPageView:self.pageIndex] addSubview:self.textview.getTextView];
        [self updateMenu];
    }
    if (self.type == FSFillSignTypeSegText) {
        if (self.textview.getTextView.text.length > 1) {
            CGRect rect = self.frame;
            UIImage *dragDot = [UIImage fs_ImageNamed:@"fill_sign_stretch"];
            CGSize size = CGSizeMake(15, 15);
            CGPoint point = CGPointMake(rect.origin.x + rect.size.width - size.width * 0.5, rect.origin.y + rect.size.height * 0.5 - size.width * 0.5);
            [dragDot drawInRect:CGRectMake(point.x, point.y, size.width, size.height)];
        }
    }
}

- (NSMutableAttributedString *)getTextObjsBy:(FSFillSignObject *)fillSignObject{
    if ([fillSignObject isEmpty]) return nil;
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] init];
    FSTextFillSignObject *textFillSignObject = [[FSTextFillSignObject alloc] initWithFillsign_object:fillSignObject];
    if (![textFillSignObject isEmpty]) {
        FSTextFillSignObjectDataArray *textDataArr = textFillSignObject.getTextDataArray;
        for (int i = 0; i < textDataArr.getSize; i ++) {
            FSTextFillSignObjectData *textFillSignObjectData = [textDataArr getAt:i];
            NSString *str = textFillSignObjectData.text;
            if (!str.length) continue;
            
            FSTextState *textState = textFillSignObjectData.text_state;
            
            CFStringRef fontName = (__bridge CFStringRef)(textState.font.getName);
            CGFontRef cgfont = CGFontCreateWithFontName(fontName);
            CGFloat fontsize = textState.font_size;
            CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, fontsize, fontsize), self.pdf2Pagetransform);
            CGFloat viewfontsize = widthRect.size.width;
            CTFontRef ctfont = CTFontCreateWithGraphicsFont(cgfont, viewfontsize, NULL, NULL);
            UIFont *ocfont = (__bridge_transfer UIFont *)ctfont;
            NSAttributedString *attributstr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:ocfont,NSLigatureAttributeName:@0,NSKernAttributeName:@0}];
            [attStr appendAttributedString:attributstr];
            CFRelease(cgfont);
        }
    }
    return attStr;
}

- (BOOL)isSameLine:(float)y1Position p2:(float)y2Position
{
    if (ABS(y1Position - y2Position) < 1.0) return YES;
    return NO;
}

- (FSFillSignType)getPDFFSFillSignTypeFromViewType:(FSFillSignType)type
{
    return FSFillSignTypeText;
}

#pragma mark keyboard hander

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
    if (pageView) {
        [[FillSignMenu sharedMenuController] updateFrame];
    }
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    FSRectF *rectf = [self.pdfViewCtrl convertPageViewRectToPdfRect:self.frame pageIndex:self.pageIndex];
    self.rotateRectf = rectf;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (self.status == FillSignEditStatus_Editing) {
        [self fillSignToolDeactiveFillSign];
        return;
    }
    
    FSRectF *rectf = [_pdfFillSignObj getRect];
    if (!_pdfFillSignObj || [_pdfFillSignObj isEmpty]) rectf = self.rotateRectf;
    self.PDFRect = CGRectMake(rectf.left, rectf.bottom, ABS(rectf.right - rectf.left), ABS(rectf.top - rectf.bottom));
    [self resetPosition];
    self.frame = [self.pdfViewCtrl convertPdfRectToPageViewRect:rectf pageIndex:self.pageIndex];
    [[self.pdfViewCtrl getPageView:self.pageIndex] addSubview:self.textview.getTextView];
    if (self.type == FSFillSignTypeSegText) {
        [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
    }
}

#pragma mark <IScrollViewEventListener>

- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView{
    FSRectF *rectf = [self.pdfViewCtrl convertPageViewRectToPdfRect:self.frame pageIndex:self.pageIndex];
    self.zoomRectf = rectf;
}

- (void)onScrollViewDidEndZooming:(UIScrollView *)scrollView {
    if (self.status == FillSignEditStatus_Editing) {
        [self fillSignToolDeactiveFillSign];
        return;
    }
    
    FSRectF *rectf = [_pdfFillSignObj getRect];
    if (!_pdfFillSignObj || [_pdfFillSignObj isEmpty]) rectf = self.zoomRectf;
    self.PDFRect = CGRectMake(rectf.left, rectf.bottom, ABS(rectf.right - rectf.left), ABS(rectf.top - rectf.bottom));
    [self resetPosition];
    self.frame = [self.pdfViewCtrl convertPdfRectToPageViewRect:rectf pageIndex:self.pageIndex];
    [[self.pdfViewCtrl getPageView:self.pageIndex] addSubview:self.textview.getTextView];
    if (self.type == FSFillSignTypeSegText) {
        [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
    }
}



@end
