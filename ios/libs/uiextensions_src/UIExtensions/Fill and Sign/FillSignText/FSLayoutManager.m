/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSLayoutManager.h"
#import "PrivateDefine.h"

@interface FSLayoutManager()

@end

@implementation FSLayoutManager

-(void)drawBackgroundForGlyphRange:(NSRange)glyphsToShow atPoint:(CGPoint)origin {
    [super drawBackgroundForGlyphRange:glyphsToShow atPoint:origin];
    
    if (!_isSegText) return;
    
    NSRange range = [self characterRangeForGlyphRange:glyphsToShow
                                     actualGlyphRange:NULL];
    NSRange glyphRange = [self glyphRangeForCharacterRange:range
                                      actualCharacterRange:NULL];
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, origin.x, origin.y);
    
    __block NSRange maxlen = NSMakeRange(0, 0);
    __block NSTextContainer *container;
    [self enumerateLineFragmentsForGlyphRange:glyphRange usingBlock:^(CGRect rect, CGRect usedRect, NSTextContainer * _Nonnull textContainer, NSRange glyphRange, BOOL * _Nonnull stop) {
        if (glyphRange.length > maxlen.length) {
            maxlen = glyphRange;
            container = textContainer;
        }
    }];
    
    NSMutableArray *pointArray = [NSMutableArray array];
    for (NSInteger i = maxlen.location; i < maxlen.location + maxlen.length; i++) {
        CGRect rect = [self boundingRectForGlyphRange:NSMakeRange(i, 1) inTextContainer:container];
        [pointArray addObject:[NSValue valueWithCGRect:rect]];
    }
    
    _maxCount = pointArray.count;
    CGSize size = container.size;
    CGContextBeginPath(context);
    if (pointArray.count > 1) {
        for (int i = 0; i < pointArray.count - 1; i++) {
            CGRect rect = [pointArray[i] CGRectValue];
            CGContextMoveToPoint(context, rect.origin.x + rect.size.width - _kerning * 0.5, 0);
            CGContextAddLineToPoint(context, rect.origin.x + rect.size.width  - _kerning * 0.5, size.height);
        }
    }
    
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, ThemeLikeBlueColor.CGColor);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}


- (void)setLineFragmentRect:(CGRect)fragmentRect forGlyphRange:(NSRange)glyphRange usedRect:(CGRect)usedRect
{
    [super setLineFragmentRect:fragmentRect forGlyphRange:glyphRange usedRect:usedRect];
    //NSLog(@"fragmentRect:%@  glyphRange:%@ usedRect:%@",NSStringFromCGRect(fragmentRect),NSStringFromRange(glyphRange),NSStringFromCGRect(usedRect));
}

- (void)setLocation:(CGPoint)location forStartOfGlyphRange:(NSRange)glyphRange
{
    [super setLocation:location forStartOfGlyphRange:glyphRange];
    //NSLog(@"Location:%f  glyphRange:%@",location,NSStringFromRange(glyphRange));
}

- (void)showCGGlyphs:(const CGGlyph *)glyphs positions:(const CGPoint *)positions count:(NSUInteger)glyphCount font:(UIFont *)font matrix:(CGAffineTransform)textMatrix attributes:(NSDictionary<NSAttributedStringKey, id> *)attributes inContext:(CGContextRef)graphicsContext
{
//    CGPoint newPositions[glyphCount];
//    for (int i = 0; i < glyphCount; i++) {
//        NSLog(@"%d glyph position(%f,%f)",i , positions[i].x, positions[i].y);
//        newPositions[i].x += positions[i].x + 15;
//        newPositions[i].y += positions[i].y;
//    }
//    NSLog(@"font:%@  matrix:%@ attributes:%@ Context:%p",font,NSStringFromCGAffineTransform(textMatrix),attributes,graphicsContext);

    [super showCGGlyphs:glyphs positions:positions count:glyphCount font:font matrix:textMatrix attributes:attributes inContext:graphicsContext];
}

@end
