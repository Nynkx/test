/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface FillSignTextHandler: FillSignHandler
@property (nonatomic, assign) BOOL isAddText;
- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex fillSignType:(FSFillSignType)type;

- (void)switchTextType:(FSFillSignType)type;
- (void)setInitKerningWidth;
- (void)activeKeyboard;
- (CGFloat)getFontSize;
- (void)setFontSize:(CGFloat)fontSize;

- (NSString *)getContent;
- (void)setContent:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
