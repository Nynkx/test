/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSTextMatchingContainer.h"
#import "PrivateDefine.h"

@interface FSTextMatchingContainer()
@property (nonatomic, strong) NSArray *contentArray;
@property (nonatomic, strong) UIScrollView *contentView;
@property (nonatomic, strong) UIButton *done;
@end

@implementation FSTextMatchingContainer

- (instancetype)init
{
    if (self = [super init]) {
        _contentView = [[UIScrollView alloc] init];
        _done = [[UIButton alloc] init];
        [_done setImage:[UIImage fs_ImageNamed:@"common_keyboard_done"] forState:UIControlStateNormal];
        [_done addTarget:self action:@selector(doneClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_contentView];
        
        [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_contentView.superview.fs_mas_left);
            make.top.mas_equalTo(_contentView.superview.fs_mas_top);
            make.right.mas_equalTo(_contentView.superview.fs_mas_right);
            make.height.mas_equalTo(_contentView.superview.self.fs_height);
        }];
        if (DEVICE_iPHONE) {
            [self addSubview:_done];
            
            [_done mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(_done.superview.fs_mas_right).offset(-ITEM_MARGIN_NORMAL);
                make.centerY.mas_equalTo(_done.superview);
                make.size.mas_equalTo(CGSizeMake(32, 32));
            }];
        }
    }
    return self;
}

- (void)setMatchingContent:(NSArray *)array
{
    _contentArray = [NSArray arrayWithArray:array];
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    CGFloat scrollviewSize = [self initContentBtn];
    _contentView.contentSize = CGSizeMake(scrollviewSize, self.frame.size.height);
}

- (CGFloat)initContentBtn
{
    CGFloat xoffset = 12;
    for (int i = 0; i < _contentArray.count; i++) {
        UIButton *btn = [[UIButton alloc] init];
        btn.layer.cornerRadius = 14.0;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:_contentArray[i] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:15];
        [btn setBackgroundColor:ThemeLikeBlueColor];
        [btn setTitleColor:WhiteThemeTextColor forState:UIControlStateNormal];
        CGSize size = btn.intrinsicContentSize;
        size.width += 28;
        btn.frame = CGRectMake(xoffset, (49 - size.height) * 0.5, size.width, size.height);
        [self.contentView addSubview:btn];
        xoffset += (size.width + 10);
    }
    return xoffset;
}

- (void)btnClick:(UIButton *)btn
{
    if (self.itemClickCall) {
        self.itemClickCall(btn.titleLabel.text);
    }
}

- (void)layoutSubviews
{
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    CGFloat scrollviewSize = [self initContentBtn];
    [super layoutSubviews];
    
    [_contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(_contentView.superview.self.fs_height);
    }];
    _contentView.contentSize = CGSizeMake(scrollviewSize, self.frame.size.height);
}

- (void)doneClick:(UIButton *)btn
{
    if (self.doneClickCall) {
        self.doneClickCall();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
