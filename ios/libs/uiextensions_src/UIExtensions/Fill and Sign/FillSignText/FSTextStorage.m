/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSTextStorage.h"

@implementation FSTextStorage
{
    NSTextStorage *_imp;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _imp = [NSTextStorage new];
    }
    
    return self;
}


#pragma mark - Reading Text

- (NSString *)string
{
    return _imp.string;
}

- (NSDictionary *)attributesAtIndex:(NSUInteger)location effectiveRange:(NSRangePointer)range
{
    return [_imp attributesAtIndex:location effectiveRange:range];
}

- (void)setKerning:(CGFloat)kerning
{
    _kerning = kerning;
}

#pragma mark - Text Editing

- (void)replaceCharactersInRange:(NSRange)range withAttributedString:(NSAttributedString *)attrString
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithAttributedString:attrString];
//    if (range.location > 0) {
//        CharspaceAttchment *attachment = [[CharspaceAttchment alloc] init];
//        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
//        [attrStr insertAttributedString:attachmentString atIndex:0];
//    }
    //[attrStr addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(0, attrStr.length)];
    for (NSUInteger i = 0; i < attrString.length; i ++) {
        [attrStr addAttribute:NSKernAttributeName value:[NSNumber numberWithInteger:_kerning] range:NSMakeRange(i, 1)];
        [attrStr addAttribute:NSLigatureAttributeName value:@0 range:NSMakeRange(i, 1)];
        [attrStr addAttribute:NSForegroundColorAttributeName value:UIColorHex(0x000000) range:NSMakeRange(i, 1)];
    }
    [super replaceCharactersInRange:range withAttributedString:attrStr];
}

- (void)replaceCharactersInRange:(NSRange)range withString:(NSString *)str
{
    // Normal replace
    NSAssert(_imp != nil, @"Content has not been set!");
    [self beginEditing];
    [_imp replaceCharactersInRange:range withString:str];
    [self edited:NSTextStorageEditedCharacters range:range changeInLength:(NSInteger)str.length - (NSInteger)range.length];
    
//    for (NSUInteger i = 0; i < str.length ; i = i + 4) {
//        [self addAttribute:NSKernAttributeName value:[NSNumber numberWithInteger:i] range:NSMakeRange(range.location + i, 4)];
//    }
    // Regular expression matching all iWords -- first character i, followed by an uppercase alphabetic character, followed by at least one other character. Matches words like iPod, iPhone, etc.
//    static NSDataDetector *linkDetector;
//    linkDetector = linkDetector ?: [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:NULL];
//
//    // Clear text color of edited range
//    NSRange paragaphRange = [self.string paragraphRangeForRange: NSMakeRange(range.location, str.length)];
//    [self removeAttribute:NSLinkAttributeName range:paragaphRange];
//    [self removeAttribute:NSBackgroundColorAttributeName range:paragaphRange];
//    [self removeAttribute:NSUnderlineStyleAttributeName range:paragaphRange];
//
//    // Find all iWords in range
//    [linkDetector enumerateMatchesInString:self.string options:0 range:paragaphRange usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
//        // Add red highlight color
//        [self addAttribute:NSLinkAttributeName value:result.URL range:result.range];
//        [self addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:result.range];
//        [self addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:result.range];
//    }];
    
//    if (range.location > 0) {
//        CharspaceAttchment *attachment = [[CharspaceAttchment alloc] init];
//        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
//        [self insertAttributedString:attachmentString atIndex:range.location];
//    }
    
    
    [self endEditing];
}

- (void)setAttributes:(NSDictionary *)attrs range:(NSRange)range
{
    [self beginEditing];
    [_imp setAttributes:attrs range:range];
    [self edited:NSTextStorageEditedAttributes range:range changeInLength:0];
    [self endEditing];
}

@end
