/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSTextView.h"
#import "FSLayoutManager.h"
#import "FSTextStorage.h"
#import <CoreText/CoreText.h>
#import "FillSignTextHandler.h"

@interface FSUITextView : UITextView

@end
@implementation FSUITextView

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
}

- (void)setBounds:(CGRect)bounds{
    [super setBounds:bounds];
}

@end

@interface FSTextView()
@property (nonatomic, strong) FSUITextView *textView;
@property (nonatomic, strong) FSTextStorage *textStorage;
@property (nonatomic, strong) FSLayoutManager *layoutManager;
@end

@implementation FSTextView

- (instancetype)init
{
    if (self = [super init]) {
        CGRect frame = CGRectZero;
        _textStorage = [[FSTextStorage alloc] init];
        _textStorage.kerning = 0.0;
        _layoutManager = [[FSLayoutManager alloc] init];
        _layoutManager.kerning = 0.0;
        [_textStorage addLayoutManager:_layoutManager];
        NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:frame.size];
        [_layoutManager addTextContainer:textContainer];
        _textView = [[FSUITextView alloc] initWithFrame:frame textContainer:textContainer];
        _textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _textView.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super init]) {
        _textStorage = [[FSTextStorage alloc] init];
        _textStorage.kerning = 0.0;
        _layoutManager = [[FSLayoutManager alloc] init];
        _layoutManager.kerning = 0.0;
        [_textStorage addLayoutManager:_layoutManager];
        NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:frame.size];
        [_layoutManager addTextContainer:textContainer];
        _textView = [[FSUITextView alloc] initWithFrame:frame textContainer:textContainer];
        _textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _textView.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}

- (UITextView *)getTextView
{
    return _textView;
}

- (NSInteger)getLineMaxWord
{
    return _layoutManager.maxCount;
}

- (void)setKerningWidth:(CGFloat)kening
{
    if (kening < 0.0001) {
        _layoutManager.isSegText = NO;
        _textStorage.kerning = 0.0;
        _layoutManager.kerning = 0.0;
    }else{
        _layoutManager.isSegText = YES;
        _textStorage.kerning = kening;
        _layoutManager.kerning = kening;
    }
}

- (NSArray *)getDrawPDFText
{
    //NSRange glyphRange = [_layoutManager glyphRangeForCharacterRange:NSMakeRange(0, _textStorage.mutableString.length) actualCharacterRange:NULL];
    NSRange glyphRange = [_layoutManager glyphRangeForTextContainer:_textView.textContainer];
    __block NSMutableArray *arrayLines = [NSMutableArray array];
    [_layoutManager enumerateLineFragmentsForGlyphRange:glyphRange usingBlock:^(CGRect rect, CGRect usedRect, NSTextContainer * _Nonnull textContainer, NSRange glyphRange, BOOL * _Nonnull stop) {
        [self.textStorage enumerateAttributesInRange:glyphRange options:NSAttributedStringEnumerationReverse usingBlock:^(NSDictionary<NSAttributedStringKey,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
            CGPoint ori = [self.layoutManager locationForGlyphAtIndex:range.location];
            ori.y += rect.origin.y;
            ori.x += rect.origin.x;
            if (NSEqualRanges(glyphRange, range)) {
                //- (CGPoint)locationForGlyphAtIndex:(NSUInteger)glyphIndex;
//                for (int i = 0; i < arrayLines.count; i++) {
//                    NSDictionary *dic = arrayLines[i];
//                    if (NSEqualRanges([dic[@"glyphRange"] rangeValue], range)) {
//                        CGPoint pt = [dic[@"pointLocation"] CGPointValue];
//                        if (ori.x < pt.x) {
//                            [arrayLines removeObjectAtIndex:i];
//                            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithCGRect:rect],@"rect",[NSValue valueWithCGRect:usedRect],@"usedRect",textContainer,@"textContainer",[NSValue valueWithRange:glyphRange],@"glyphRange",attrs,@"attrs",[NSValue valueWithCGPoint:ori],@"pointLocation",nil];
//                            [arrayLines addObject:dic];
//                        }
//                        return;
//                    }
//                }
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithCGRect:rect],@"rect",[NSValue valueWithCGRect:usedRect],@"usedRect",textContainer,@"textContainer",[NSValue valueWithRange:glyphRange],@"glyphRange",attrs,@"attrs",[NSValue valueWithCGPoint:ori],@"pointLocation",nil];
                [arrayLines addObject:dic];
            }else{
//                for (int i = 0; i < arrayLines.count; i++) {
//                    NSDictionary *dic = arrayLines[i];
//                    if (NSEqualRanges([dic[@"glyphRange"] rangeValue], range)) {
//                        CGPoint pt = [dic[@"pointLocation"] CGPointValue];
//                        if (ori.x < pt.x) {
//                            [arrayLines removeObjectAtIndex:i];
//                            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithCGRect:rect],@"rect",[NSValue valueWithCGRect:usedRect],@"usedRect",textContainer,@"textContainer",[NSValue valueWithRange:glyphRange],@"glyphRange",attrs,@"attrs",[NSValue valueWithCGPoint:ori],@"pointLocation",nil];
//                            [arrayLines addObject:dic];
//                        }
//                        return;
//                    }
//                }
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithCGRect:rect],@"rect",[NSValue valueWithCGRect:usedRect],@"usedRect",textContainer,@"textContainer",[NSValue valueWithRange:range],@"glyphRange",attrs,@"attrs",[NSValue valueWithCGPoint:ori],@"pointLocation",nil];
                [arrayLines addObject:dic];
            }
        }];
    }];
    
    return arrayLines;
}

- (nullable FSTextFillSignObject *)generatePDFTextWithFillSignHandler:(FillSignHandler *)fillSignHandler rect:(CGRect)rect roation:(int)rotation textinfo:(NSMutableArray *)textinfo{
    NSArray *drawlinesInfo = [self getDrawPDFText];
    if (drawlinesInfo.count < 1) return NULL;
    
    FSFillSign *fillSign = fillSignHandler->_fillSign;
    if ([fillSign isEmpty] || !fillSign) return nil;
    
    CGAffineTransform pdf2PagetransformInvert = fillSignHandler.pdf2PagetransformInvert;
    CGRect kerningRect = CGRectApplyAffineTransform(CGRectMake(0, 0, _textStorage.kerning, _textStorage.kerning), pdf2PagetransformInvert);
    CGFloat kerning = kerningRect.size.width;
    
    FSTextFillSignObjectDataArray *textDataArray = [[FSTextFillSignObjectDataArray alloc] init];
    CGPoint lastPos = CGPointZero;
    NSString *lastStr = @"";
    for (int i = 0; i < drawlinesInfo.count; i++) {
        NSDictionary *dic = drawlinesInfo[i];
        NSDictionary *attrs = dic[@"attrs"];
        UIFont *font = attrs[@"NSFont"];

        NSRange rang = [dic[@"glyphRange"] rangeValue];
        NSString *str = [[_textStorage string] substringWithRange:rang];
        str = [str stringByTrimmingCharactersInSet:[NSMutableCharacterSet controlCharacterSet]];

        CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, font.pointSize, font.pointSize), pdf2PagetransformInvert);
        CGFloat fontsize = widthRect.size.width;
        
        CGPoint position = [dic[@"pointLocation"] CGPointValue];
        
        if (i < drawlinesInfo.count-1) {
            NSDictionary *nextDic = drawlinesInfo[i+1];
            CGPoint nextPos = [nextDic[@"pointLocation"] CGPointValue];
            if (nextPos.y == position.y) {
                lastStr = [lastStr stringByAppendingString:str];
                lastPos = position;
                continue;
            }else{
                lastStr = @"";
            }
        }else{
            if (lastPos.y != position.y) {
                lastStr = @"";
            }
        }
        lastPos = position;
        
        str = [lastStr stringByAppendingString:str];
        str = [str stringByAppendingString:@"\n"];
        FSTextState *textState = [[FSTextState alloc] init];
        FSFont *fsfont = [[FSFont alloc] initWithFont_id:FSFontStdIDCourier];
        textState.font = fsfont;
        textState.font_size = fontsize;
        textState.charspace = kerning;
        textState.wordspace = 0;
        textState.textmode = FSTextStateModeFill;

        FSTextFillSignObjectData *data = [[FSTextFillSignObjectData alloc] initWithText_state:textState text:str];
        [textDataArray add:data];
        lastPos = position;
    }
    FSPointF *pointf = [[FSPointF alloc] initWithX:CGRectGetMinX(rect) Y:CGRectGetMinY(rect)];
    FSFillSignObject *fillSignObject = [fillSign addTextObject:textDataArray point:pointf width:CGRectGetWidth(rect) height:CGRectGetHeight(rect) rotation:rotation is_comb_field_mode:_isSegText];
    FSTextFillSignObject *textFillSignObject = [[FSTextFillSignObject alloc] initWithFillsign_object:fillSignObject];
    
    [textFillSignObject generateContent];
    return textFillSignObject;
}



-(float)kerningForCharacter0:(unichar)c0 character1:(unichar)c1 font:(UIFont *)font;
{
    unichar c0c1[2] = { c0, c1 };

    CTFontRef core_text_font = ((__bridge CTFontRef)font);

    CGGlyph glyphs[2];
    CGSize glyph_advances[2];

    CTFontGetGlyphsForCharacters(core_text_font, c0c1, glyphs, 2);
    CTFontGetAdvancesForGlyphs(core_text_font, kCTFontOrientationDefault, glyphs, glyph_advances, 2);

    NSString *c0c1_s = [NSString stringWithCharacters:c0c1 length:2];

    float kerning = 0;

    NSAttributedString *as = [[NSAttributedString alloc] initWithString:c0c1_s attributes:@{NSFontAttributeName:font}];

    CTTypesetterRef typesetter = CTTypesetterCreateWithAttributedString((CFAttributedStringRef) as);
    CTLineRef line = CTTypesetterCreateLine(typesetter, CFRangeMake(0, 0));
    CFRelease(typesetter);
    CGFloat offset = CTLineGetOffsetForStringIndex(line, 1, NULL);
    CFRelease(line);
    kerning = offset - glyph_advances[0].width;

    return kerning;
}
@end
