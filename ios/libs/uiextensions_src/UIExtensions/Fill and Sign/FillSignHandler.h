/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import <UIKit/UIKit.h>
#import "FillSignMenu.h"
#import "FillSignUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface FillSignHandler : NSObject<IRotationEventListener>
{
    @public
    FSFillSignObject *_pdfFillSignObj;
    FSFillSign *_fillSign;
}
@property (nonatomic, assign) FSFillSignType type;
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (nonatomic, assign) FillSignEditStatus status;
@property (nonatomic, assign) int pageIndex;
@property (nonatomic, assign, getter=getOldPDFRect, readonly) CGRect oldPDFRect;
@property (nonatomic, assign, getter=getPDFRect, readonly) CGRect PDFRect;
@property (nonatomic, assign, readonly) CGRect rect;
@property (nonatomic) CGAffineTransform pdf2Pagetransform;
@property (nonatomic) CGAffineTransform pdf2PagetransformInvert;
@property (nonatomic) UIEdgeInsets edgeInsets;
@property (nonatomic, strong) UIImage *dragDot;
@property (nonatomic) BOOL isOnchange;
@property (nonatomic) int pageRatation;
@property (nonatomic) int rotation;
@property (nonatomic) BOOL isReplace;
@property (nonatomic, copy) NSString *groupid;
@property (nonatomic) CGRect            frame;
@property (nonatomic) CGRect            bounds;
@property (nonatomic) CGPoint           center;
@property (nonatomic) BOOL isMove;

+ (instancetype)containerWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex fillSignType:(FSFillSignType)fillSignType;

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex;

- (FSFillSignObject *)getPDFFillSignObj;
- (void)setPositionForFillHandler:(CGPoint)pt;
- (void)setPositionLTForFillHandler:(CGPoint)pt;
- (void)setPDFRect:(CGRect)rect;
- (void)resetPosition;
- (void)updatePDFRect;
- (void)updateFillSignObjPosition;
- (void)removeFillSignItem:(BOOL)notify;

- (void)createFillSignObj;
- (void)setFillSignObj:(FSFillSignObject *)fillSignObject;

- (void)smallerCall;
- (void)biggerCall;

- (void)deactiveFillHandler;
- (void)fillSignToolDeactiveFillSign;

- (CGSize)getFormItemSize:(CGFloat)xoffset position:(FillSignXPosition)position;
- (UIEdgeInsets)pageEdgeInsets;
- (void)recordFormItemSize:(CGSize)size;
- (void)onPanGesture:(UIPanGestureRecognizer *)guest endGesture:(BOOL)end;
- (void)onTapGesture:(UITapGestureRecognizer *)guest;

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context;
- (void)showMagnifier:(int)pageIndex point:(CGPoint)point;
- (void)moveMagnifier:(int)pageIndex point:(CGPoint)point;
- (void)closeMagnifier;

- (BOOL)isHitFillSignObjWithPoint:(CGPoint)point;

- (FSFillSignItemtype)getFillSignItemTypeByFSFillSignType:(FSFillSignType)signType;
@end

NS_ASSUME_NONNULL_END
