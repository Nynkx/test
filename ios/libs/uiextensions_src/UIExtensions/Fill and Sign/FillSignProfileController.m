/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignProfileController.h"
#import "IQKeyboardManager.h"

#define FILLSIGNPROFILE [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"Preferences/com.foxit.fillsignProfile.plist"]

@interface FillSignHeaderView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
- (instancetype)initWithTitle:(NSString *)title;
@end

@implementation FillSignHeaderView

- (instancetype)initWithTitle:(NSString *)title
{
    if (self = [super init]) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = title;
        _titleLabel.font = [UIFont systemFontOfSize:12];
        _titleLabel.textColor = GrayThemeTextColor;
        [self addSubview:_titleLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.left.mas_equalTo(self.fs_mas_left).offset(14);
    }];
}

@end

@interface FillSignProfileCell : UITableViewCell
@property (nonatomic, assign) BOOL isCustomFieldCell;
@property (nonatomic, strong) UILabel *keyLabel;
@property (nonatomic, strong) UITextField *keyTextField;
@property (nonatomic, strong) UITextField *valueTextField;
@property (nonatomic, strong) NSIndexPath *indexPath;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier isCustomFieldCell:(BOOL)isCustomFieldCell;
@end

@implementation FillSignProfileCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier isCustomFieldCell:(BOOL)isCustomFieldCell
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = ThemeCellBackgroundColor;
        _isCustomFieldCell = isCustomFieldCell;
        if (isCustomFieldCell) {
            _keyTextField = [[UITextField alloc] init];
            _keyTextField.layer.anchorPoint = CGPointMake(0, 0.5);
            _keyTextField.font = [UIFont systemFontOfSize:15];
            _keyTextField.textColor = BlackThemeTextColor;
            [self.contentView addSubview:_keyTextField];
        }else{
            _keyLabel = [[UILabel alloc] init];
            _keyLabel.layer.anchorPoint = CGPointMake(0, 0.5);
            _keyLabel.font = [UIFont systemFontOfSize:15];
            _keyLabel.textColor = BlackThemeTextColor;
            [self.contentView addSubview:_keyLabel];
        }
        _valueTextField = [[UITextField alloc] init];
        _valueTextField.layer.anchorPoint = CGPointMake(0, 0.5);
        _valueTextField.font = [UIFont systemFontOfSize:15];
        _valueTextField.textColor = BlackThemeTextColor;
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:_valueTextField];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGSize contentViewSize = self.contentView.frame.size;
    if (_isCustomFieldCell) {
        _keyTextField.center = CGPointMake(14, contentViewSize.height * 0.5);
        _keyTextField.bounds = CGRectMake(0, 0, contentViewSize.width * 0.5 - 14 - 20, 21);
    }else{
        _keyLabel.center = CGPointMake(14, contentViewSize.height * 0.5);
        _keyLabel.bounds = CGRectMake(0, 0, contentViewSize.width * 0.5 - 14 - 20, 21);
    }
    _valueTextField.center = CGPointMake(contentViewSize.width * 0.5 - 20 + 2, contentViewSize.height * 0.5);
    _valueTextField.bounds = CGRectMake(0, 0, contentViewSize.width - (contentViewSize.width * 0.5 - 20 + 2) - 14, 21);
}

@end

@interface FillSignProfileController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *bottomToolbar;
@property (nonatomic, strong) NSMutableArray *nameArray;
@property (nonatomic, strong) NSMutableArray *addArray;
@property (nonatomic, strong) NSMutableArray *contactArray;
@property (nonatomic, strong) NSMutableArray *dateArray;
@property (nonatomic, strong) NSMutableArray *customArray;
@property (nonatomic, strong) NSArray *textTips;

@property (nonatomic, strong) UIBarButtonItem *closeItem;
@property (nonatomic, strong) UIBarButtonItem *editItem;
@property (nonatomic, strong) UIBarButtonItem *doneItem;
@property (nonatomic, strong) NSArray *contentArray;

@property (nonatomic, strong) UITextField *editingField;
@property (nonatomic, assign) BOOL isModified;

@property (nonatomic, copy) NSArray *headerTitles;
@end

@implementation FillSignProfileController

+ (NSArray *)getPreDefineConetArray
{
    FillSignProfileController *ctr = [[FillSignProfileController alloc] init];
    [ctr loadDateFromProfileFile];
    return [ctr loadPredefineValue];
}

- (void)loadDateFromProfileFile
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:FILLSIGNPROFILE]) {
        NSDictionary *tmp = [NSDictionary dictionaryWithContentsOfFile:FILLSIGNPROFILE];
        
        _nameArray = [NSMutableArray array];
        
        NSArray *names =  tmp[@"kName"];
        NSArray *keys = @[ @"kProfileFullName", @"kProfileFirstName", @"kProfileMiddleName", @"kProfileLastName" ];
        for (NSString *key in keys) {
            BOOL isExist = NO;
            for (NSDictionary *dict in names ) {
                if ([[[dict allKeys] firstObject] isEqualToString:key]) {
                    [_nameArray addObject:@{key:dict[key]}];
                    isExist = YES;
                    break;
                }
            }
            if (!isExist) {
                [_nameArray addObject:@{key:@""}];
            }
        }
        
        _addArray = [NSMutableArray array];
        if (tmp[@"kProfileAddress"]) {
            [_addArray addObjectsFromArray:[tmp[@"kProfileAddress"] copy]];
        }
        
        _contactArray = [NSMutableArray array];
        if (tmp[@"kProfileContactInfo"]) {
            [_contactArray addObjectsFromArray:[tmp[@"kProfileContactInfo"] copy]];
        }
        
        _dateArray = [NSMutableArray array];
        if (tmp[@"kProfileDates"]) {
            [_dateArray addObjectsFromArray:[tmp[@"kProfileDates"] copy]];
        }
        
        _customArray = [NSMutableArray array];
        if (tmp[@"kProfileCustomfield"]) {
            [_customArray addObjectsFromArray:[tmp[@"kProfileCustomfield"] copy]];
        }
        _modeDatas = [NSMutableDictionary dictionary];
        [_modeDatas setDictionary: @{@"kName":_nameArray,@"kProfileAddress":_addArray,@"kProfileContactInfo":_contactArray,@"kProfileDates":_dateArray,@"kProfileCustomfield":_customArray}];
    }else{
        _nameArray = [NSMutableArray array];
        [_nameArray addObjectsFromArray:@[@{@"kProfileFullName":@""},@{@"kProfileFirstName":@""},@{@"kProfileMiddleName":@""},@{@"kProfileLastName":@""}]];
        _addArray = [NSMutableArray array];
        [_addArray addObjectsFromArray:@[@{@"kProfileStreet1":@""},@{@"kProfileStreet2":@""},@{@"kProfileCity":@""},@{@"kProfileState":@""},@{@"kProfileZip":@""},@{@"kProfileCountry":@""}]];
        _contactArray = [NSMutableArray array];
        [_contactArray addObjectsFromArray:@[@{@"kProfileEmail":@""},@{@"kProfileTel":@""}]];
        _dateArray = [NSMutableArray array];
        [_dateArray addObjectsFromArray:@[@{@"kProfileDate":@""},@{@"kProfileBirthDate":@""}]];
        _customArray = [NSMutableArray array];
        _modeDatas = [NSMutableDictionary dictionary];
        [_modeDatas setDictionary: @{@"kName":_nameArray,@"kProfileAddress":_addArray,@"kProfileContactInfo":_contactArray,@"kProfileDates":_dateArray,@"kProfileCustomfield":_customArray}];
        [_modeDatas writeToFile:FILLSIGNPROFILE atomically:YES];
    }
}


- (NSArray *)loadPredefineValue
{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in _nameArray) {
        NSArray *tmp = [dic allValues];
        NSString *tmpstr = [tmp lastObject];
        if (tmp.count > 0 && tmpstr.length > 0) [array addObjectsFromArray:tmp];
    }
    for (NSDictionary *dic in _addArray) {
        NSArray *tmp = [dic allValues];
        NSString *tmpstr = [tmp lastObject];
        if (tmp.count > 0 && tmpstr.length > 0) [array addObjectsFromArray:tmp];
    }
    for (NSDictionary *dic in _contactArray) {
        NSArray *tmp = [dic allValues];
        NSString *tmpstr = [tmp lastObject];
        if (tmp.count > 0 && tmpstr.length > 0) [array addObjectsFromArray:tmp];
    }
    for (NSDictionary *dic in _dateArray) {
        NSArray *tmp = [dic allValues];
        NSString *tmpstr = [tmp lastObject];
        if (tmp.count > 0 && tmpstr.length > 0) [array addObjectsFromArray:tmp];
    }
    for (NSDictionary *dic in _customArray) {
        NSArray *tmp = [dic allValues];
        NSString *tmpstr = [tmp lastObject];
        if (tmp.count > 0 && tmpstr.length > 0) [array addObjectsFromArray:tmp];
    }
    return array;
}

- (void)saveData2ModebyIndexPath:(NSIndexPath *)indexPath cell:(FillSignProfileCell*)cell
{
    switch (indexPath.section) {
        case 0:
        {
            NSDictionary *dic = _nameArray[indexPath.row];
            NSArray *tmp = [dic allKeys];
            NSString *tmpstr = [tmp lastObject];
            NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
            [_nameArray replaceObjectAtIndex:indexPath.row withObject:tmpdic];
            break;
        }
        case 1:
        {
            NSDictionary *dic = _addArray[indexPath.row];
            NSArray *tmp = [dic allKeys];
            NSString *tmpstr = [tmp lastObject];
            NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
            [_addArray replaceObjectAtIndex:indexPath.row withObject:tmpdic];
            break;
        }
        case 2:
        {
            NSDictionary *dic = _contactArray[indexPath.row];
            NSArray *tmp = [dic allKeys];
            NSString *tmpstr = [tmp lastObject];
            NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
            [_contactArray replaceObjectAtIndex:indexPath.row withObject:tmpdic];
            break;
        }
        case 3:
        {
            NSDictionary *dic = _dateArray[indexPath.row];
            NSArray *tmp = [dic allKeys];
            NSString *tmpstr = [tmp lastObject];
            if ([tmpstr isEqualToString:FSLocalizedForKey(@"kProfileDate")]) break;
            NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
            [_dateArray replaceObjectAtIndex:indexPath.row withObject:tmpdic];
            break;
        }
        case 4:
        {
            NSString *tmpstr = [cell.keyTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (tmpstr.length < 1) break;
            NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
            if (indexPath.row < _customArray.count) {
                [_customArray replaceObjectAtIndex:indexPath.row withObject:tmpdic];
            }
            
            break;
        }
        default:
            break;
    }
}

- (void)saveData2Mode
{
    for (int i = 0; i < _nameArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil) continue;
        NSDictionary *dic = _nameArray[i];
        NSArray *tmp = [dic allKeys];
        NSString *tmpstr = [tmp lastObject];
        NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
        [_nameArray replaceObjectAtIndex:i withObject:tmpdic];
    }
    
    for (int i = 0; i < _addArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil) continue;
        NSDictionary *dic = _addArray[i];
        NSArray *tmp = [dic allKeys];
        NSString *tmpstr = [tmp lastObject];
        NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
        [_addArray replaceObjectAtIndex:i withObject:tmpdic];
    }
    
    for (int i = 0; i < _contactArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:2];
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil) continue;
        NSDictionary *dic = _contactArray[i];
        NSArray *tmp = [dic allKeys];
        NSString *tmpstr = [tmp lastObject];
        NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
        [_contactArray replaceObjectAtIndex:i withObject:tmpdic];
    }
    
    for (int i = 0; i < _dateArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:3];
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil) continue;
        NSDictionary *dic = _dateArray[i];
        NSArray *tmp = [dic allKeys];
        NSString *tmpstr = [tmp lastObject];
        if ([tmpstr isEqualToString:FSLocalizedForKey(@"kProfileDate")]) continue;
        NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
        [_dateArray replaceObjectAtIndex:i withObject:tmpdic];
    }
    
    for (int i = 0; i < _customArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:4];
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil) continue;
        NSString *tmpstr = [cell.keyTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSDictionary *tmpdic = [NSDictionary dictionaryWithObjectsAndKeys:utlStr, tmpstr,nil];
        [_customArray replaceObjectAtIndex:i withObject:tmpdic];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _textTips = @[@[@"kProfileAddFullName",@"kProfileAddFirstName",@"kProfileAddMiddleName",@"kProfileAddLastName"],@[@"kProfileAddStreet1",@"kProfileAddStreet2",@"kProfileAddCity",@"kProfileAddState",@"kProfileAddZip",@"kProfileAddCountry"],@[@"kProfileAddEmail",@"kProfileAddTel"],@[@"kProfileAddBirthDate"]];
    [self loadDateFromProfileFile];
    [self initNavbar];
    _isModified = NO;

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 49) style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    
    self.bottomToolbar = [[UIView alloc] init];
    self.bottomToolbar.backgroundColor = NormalBarBackgroundColor;
    [self.view addSubview:self.bottomToolbar];
    
    UIButton *btn = [[UIButton alloc] init];
    [btn addTarget:self action:@selector(addCustomFieldClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomToolbar addSubview:btn];
    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bottomToolbar.mas_left);
        make.right.mas_equalTo(self.bottomToolbar.mas_right);
        make.top.mas_equalTo(self.bottomToolbar.mas_top);
        make.height.mas_equalTo(49);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage fs_ImageNamed:@"fill_sign_profile_customfield"];
    UILabel *customLabel = [[UILabel alloc] init];
    customLabel.text = FSLocalizedForKey(@"kProfileAddCustom");
    customLabel.font = [UIFont systemFontOfSize:15];
    customLabel.textColor = BlackThemeTextColor;
    [btn addSubview:imageView];
    [btn addSubview:customLabel];
    [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.centerY.mas_equalTo(btn.mas_centerY);
        make.left.mas_equalTo(btn.mas_left).offset(4);
    }];
    
    [customLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(4);
        make.centerY.mas_equalTo(btn.mas_centerY);
    }];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.separatorColor = DividingLineColor;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 44;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.headerTitles = @[ FSLocalizedForKey(@"kName"), FSLocalizedForKey(@"kProfileAddress"), FSLocalizedForKey(@"kProfileContactInfo"), FSLocalizedForKey(@"kProfileDates"), FSLocalizedForKey(@"kProfileCustomfield")];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

- (void)addCustomFieldClick:(UIButton *)btn
{
    if (self.navigationItem.rightBarButtonItem != self.doneItem) {
        self.title = FSLocalizedForKey(@"kProfileAddCustomTitle");
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = self.doneItem;
    }
    if (_customArray.count > 0) {
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_customArray.count - 1 inSection:4]];
        if (cell) {
            if (cell.keyTextField.text.length < 1) {
                [cell.keyTextField becomeFirstResponder];
                return;
            }
            
//            if (cell.valueTextField.text.length < 1) {
//                [cell.valueTextField becomeFirstResponder];
//                return;
//            }
        }
    }
    [self saveData2Mode];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
    [_customArray addObject:dic];
    [self.tableView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self->_customArray.count - 1 inSection:4] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self->_customArray.count - 1 inSection:4]];
        [cell.keyTextField becomeFirstResponder];
    });
}

- (void)textFieldDidChange:(NSNotification *)info
{
    UITextField *textField = [info object];
    if (textField != nil && textField == self.editingField) {
        _isModified = YES;
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.bottomToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        if (@available(iOS 11.0, *)) {
            make.height.mas_equalTo(TAB_BAR_HEIGHT_NORMAL + self.view.safeAreaInsets.bottom);
        } else {
            make.height.mas_equalTo(TAB_BAR_HEIGHT_NORMAL);
        }
    }];
    
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.top.mas_equalTo(self.view.mas_top);
        make.bottom.mas_equalTo(self.bottomToolbar.mas_top);
    }];
}

- (void)initNavbar
{
    self.closeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage fs_ImageNamed:@"webview_close"] style:UIBarButtonItemStylePlain target:self action:@selector(closeClick:)];
    self.doneItem = [[UIBarButtonItem alloc] initWithTitle:FSLocalizedForKey(@"kDone") style:UIBarButtonItemStylePlain target:self action:@selector(doneClick:)];
    self.editItem = [[UIBarButtonItem alloc] initWithTitle:FSLocalizedForKey(@"kEdit") style:UIBarButtonItemStylePlain target:self action:@selector(editClick:)];
    
    self.title = FSLocalizedForKey(@"kProfiletitle");
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:BlackThemeTextColor,NSForegroundColorAttributeName, [UIFont systemFontOfSize:18],NSFontAttributeName,nil];
    [self.navigationController.navigationBar setTitleTextAttributes:attributes];
    
    [self.navigationController.navigationBar setTintColor:BlackThemeTextColor];
    self.navigationController.navigationBar.barTintColor = NormalBarBackgroundColor;
    self.navigationItem.leftBarButtonItem = self.closeItem;
    _contentArray = [self loadPredefineValue];
    if (_contentArray.count > 0) {
        self.navigationItem.rightBarButtonItem = self.editItem;
    }else{
        self.navigationItem.rightBarButtonItem = self.doneItem;
    }
}

- (void)closeClick:(UIBarButtonItem *)item
{
    if (self.selectCall) {
        self.selectCall(self, @"");
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)doneClick:(UIBarButtonItem *)item
{
    self.title = FSLocalizedForKey(@"kProfiletitle");
    self.navigationItem.leftBarButtonItem = self.closeItem;
    self.navigationItem.rightBarButtonItem = self.editItem;
    if (self.editingField && self.editingField.isFirstResponder) {
        [self.editingField resignFirstResponder];
        _editingField = nil;
    }
    [self saveData2Mode];
    NSMutableArray *deleteArray = [NSMutableArray array];
    for (int i = 0; i < _customArray.count; i++) {
        NSDictionary *tmpDic = [_customArray objectAtIndex:i];
        NSArray *tmpKeys = [tmpDic allKeys];
        NSString *tmpKeystr = [tmpKeys lastObject];

        if (tmpKeystr.length < 1 ){//|| tmpValuestr.length < 1) {
//            [_customArray removeObjectAtIndex:i];
            [deleteArray addObject:tmpDic];
        }
    }
    for (NSDictionary *dic in deleteArray) {
        [_customArray removeObject:dic];
    }
    
    [self.tableView reloadData];
}

- (void)editClick:(UIBarButtonItem *)item
{
    self.title = FSLocalizedForKey(@"kProfileAddCustomTitle");
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = self.doneItem;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4 + (_customArray.count > 0 ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return _nameArray.count;
    }else if (section == 1){
        return _addArray.count;
    }else if (section == 2){
        return _contactArray.count;
    }else if (section == 3){
        return _dateArray.count;
    }else{
        return _customArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FillSignProfileCell *cell = [[FillSignProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifierProfileCell" isCustomFieldCell:(indexPath.section == 4)];
    cell.indexPath = indexPath;
    switch (indexPath.section) {
        case 0:
        {
            NSDictionary *dic = _nameArray[indexPath.row];
            NSArray *tmpKeys = [dic allKeys];
            NSString *tmpKeystr = [tmpKeys lastObject];
            if (tmpKeystr.length > 0) cell.keyLabel.text = FSLocalizedForKey(tmpKeystr);
            
            NSArray *tmpValues = [dic allValues];
            NSString *tmpValuestr = [tmpValues lastObject];
            if (tmpValuestr.length > 0){
                cell.valueTextField.text = tmpValuestr;
                if (self.navigationItem.rightBarButtonItem == self.editItem) {
                    cell.valueTextField.userInteractionEnabled = NO;
                }
                else
                {
                    cell.valueTextField.userInteractionEnabled = YES;
                }
            }else{
                NSString *tipstr = _textTips[indexPath.section][indexPath.row];
                cell.valueTextField.placeholder = FSLocalizedForKey(tipstr);
                cell.valueTextField.userInteractionEnabled = YES;
            }
            cell.valueTextField.delegate = self;
            break;
        }
        case 1:
        {
            NSDictionary *dic = _addArray[indexPath.row];
            NSArray *tmpKeys = [dic allKeys];
            NSString *tmpKeystr = [tmpKeys lastObject];
            if (tmpKeystr.length > 0) cell.keyLabel.text = FSLocalizedForKey(tmpKeystr);
            
            NSArray *tmpValues = [dic allValues];
            NSString *tmpValuestr = [tmpValues lastObject];
            if (tmpValuestr.length > 0){
                cell.valueTextField.text = tmpValuestr;
                cell.valueTextField.userInteractionEnabled = self.navigationItem.rightBarButtonItem == self.editItem ? NO : YES;
            }else{
                NSString *tipstr = _textTips[indexPath.section][indexPath.row];
                cell.valueTextField.placeholder = FSLocalizedForKey(tipstr);
                cell.valueTextField.userInteractionEnabled = YES;
            }
            cell.valueTextField.delegate = self;
            break;
        }
        case 2:
        {
            NSDictionary *dic = _contactArray[indexPath.row];
            NSArray *tmpKeys = [dic allKeys];
            NSString *tmpKeystr = [tmpKeys lastObject];
            if (tmpKeystr.length > 0) cell.keyLabel.text = FSLocalizedForKey(tmpKeystr);
            
            NSArray *tmpValues = [dic allValues];
            NSString *tmpValuestr = [tmpValues lastObject];
            if (tmpValuestr.length > 0){
                cell.valueTextField.text = tmpValuestr;
                cell.valueTextField.userInteractionEnabled = self.navigationItem.rightBarButtonItem == self.editItem ? NO : YES;
            }else{
                NSString *tipstr = _textTips[indexPath.section][indexPath.row];
                cell.valueTextField.placeholder = FSLocalizedForKey(tipstr);
                cell.valueTextField.userInteractionEnabled = YES;
            }
            cell.valueTextField.delegate = self;
            break;
        }
        case 3:
        {
            NSDictionary *dic = _dateArray[indexPath.row];
            NSArray *tmpKeys = [dic allKeys];
            NSString *tmpKeystr = [tmpKeys lastObject];
            if (tmpKeystr.length > 0) cell.keyLabel.text = FSLocalizedForKey(tmpKeystr);
            
            NSArray *tmpValues = [dic allValues];
            NSString *tmpValuestr = [tmpValues lastObject];
            if (tmpValuestr.length > 0){
                cell.valueTextField.text = tmpValuestr;
                cell.valueTextField.userInteractionEnabled = self.navigationItem.rightBarButtonItem == self.editItem ? NO : YES;
            }else{
                if (indexPath.row == 1) {
                    NSString *tipstr = [_textTips[indexPath.section] lastObject];
                    cell.valueTextField.placeholder = FSLocalizedForKey(tipstr);
                    cell.valueTextField.userInteractionEnabled = YES;
                }
            }
            
            if (indexPath.row == 0) {
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                df.dateFormat = @"yyyy/MM/dd";
                NSString *dateString = [df stringFromDate:[NSDate date]];
                cell.valueTextField.text = dateString;
                cell.valueTextField.userInteractionEnabled = NO;
            }
            cell.valueTextField.delegate = self;
            break;
        }
        case 4:
        {
            NSDictionary *dic = _customArray[indexPath.row];
            NSArray *tmpKeys = [dic allKeys];
            NSString *tmpKeystr = [tmpKeys lastObject];
            if (tmpKeystr.length > 0){
                cell.keyTextField.text = tmpKeystr;
            }else{
                cell.keyTextField.placeholder = FSLocalizedForKey(@"kProfileAddCustomKey");
            }
            cell.keyTextField.delegate = self;
            [cell.keyTextField addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
            
            NSArray *tmpValues = [dic allValues];
            NSString *tmpValuestr = [tmpValues lastObject];
            if (tmpValuestr.length > 0){
                cell.valueTextField.text = tmpValuestr;
                cell.valueTextField.userInteractionEnabled = self.navigationItem.rightBarButtonItem == self.editItem ? NO : YES;
            }else{
                cell.valueTextField.placeholder = FSLocalizedForKey(@"kProfileAddCustomValue");
                cell.valueTextField.userInteractionEnabled = YES;
            }
            cell.valueTextField.delegate = self;
            break;
        }
        default:
            break;
    }
    
    return cell;
}

#pragma mark -  table view delegate handler
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 26;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FillSignHeaderView *headerView = [[FillSignHeaderView alloc] initWithTitle:self.headerTitles[section]];
    headerView.backgroundColor = NormalBarBackgroundColor;
    return headerView;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    //[self saveData2ModebyIndexPath:indexPath];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.navigationItem.rightBarButtonItem == self.doneItem && indexPath.section == 4) {
        if (self.editingField && self.editingField.isFirstResponder) {
            return NO;
        }
        return YES;
    }else{
        return NO;
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [_customArray removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FSLocalizedForKey(@"kDelete");
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FillSignProfileCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if ((cell.keyLabel.text.length > 0 || cell.keyTextField.text.length > 0) && cell.valueTextField.text.length > 0) {
        
        if ((indexPath.section == 3 && indexPath.row == 0 && ![IQKeyboardManager sharedManager].isKeyboardShowing) || self.navigationItem.rightBarButtonItem == self.editItem) {
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.selectCall) {
                    NSString *utlStr = [cell.valueTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    if (utlStr.length)  self.selectCall(self, utlStr);
                }
            }];
        }
    }
}

#pragma mark -  textfieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return ((textField.text.length < 1 && self.navigationItem.rightBarButtonItem == self.editItem) || self.navigationItem.rightBarButtonItem != self.editItem);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _editingField = textField;
    //if (self.navigationItem.rightBarButtonItem == self.editItem) {
        self.title = FSLocalizedForKey(@"kProfileAddCustomTitle");
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = self.doneItem;
    //}
    FillSignProfileCell *cell = [self getCellBytextField:textField];
    if (cell.keyTextField == textField) {
        cell.valueTextField.userInteractionEnabled = NO;
    }
}

-(void)changedTextField:(id)textField
{
    UITextField *keyField = (UITextField*)textField;
    FillSignProfileCell *cell = [self getCellBytextField:keyField];
    if (cell.keyTextField == textField) {
        if (keyField.text.length > 0) {
            cell.valueTextField.userInteractionEnabled = YES;
        }
        else
        {
            cell.valueTextField.userInteractionEnabled = NO;
        }
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_editingField == textField) {
        _editingField = nil;
    }
    FillSignProfileCell *cell = [self getCellBytextField:textField];
    if (cell.keyTextField == textField) {
        if (textField.text.length > 0) {
            cell.valueTextField.userInteractionEnabled = YES;
        }
        else
        {
            cell.valueTextField.userInteractionEnabled = NO;
        }
    }
    NSIndexPath *indexPath = cell.indexPath;
    [self saveData2ModebyIndexPath:indexPath cell:cell];
}


- (FillSignProfileCell *)getCellBytextField:(UITextField *)textField
{
    UIView *superView = textField.superview;
    while (![superView isKindOfClass:[FillSignProfileCell class]] && superView != nil) {
        superView = superView.superview;
    }
    return (FillSignProfileCell *)superView;
}


- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if (_isModified) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:FILLSIGNPROFILE]) {
            [[NSFileManager defaultManager] removeItemAtPath:FILLSIGNPROFILE error:nil];
        }
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self->_modeDatas writeToFile:FILLSIGNPROFILE atomically:YES];
        });
        
        if (self.updateCall) {
            NSArray *contentArray = [self loadPredefineValue];
            self.updateCall(self, contentArray);
        }
    }
    [super dismissViewControllerAnimated:flag completion:completion];
}

@end
