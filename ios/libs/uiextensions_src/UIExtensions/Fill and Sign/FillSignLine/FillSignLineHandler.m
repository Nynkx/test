/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignLineHandler.h"

#define MINWIDTH 45
#define MINHEIGH 15
#define RATEWIDTH 1.1

@interface FillSignLineHandler ()
@property (nonatomic) CGPoint lastPt;
@property (nonatomic) BOOL isOnCtrolPt;
@property (nonatomic) BOOL isLeftCtrolPt;
@end

@implementation FillSignLineHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex{
    self = [super initWithUIExtensionsManager:extensionsManager pageIndex:pageIndex];
    if (self) {
        self.type = FSFillSignTypeLine;
    }
    return self;
}

- (CGSize)getFormItemSize:(CGFloat)xoffset position:(FillSignXPosition)position
{
    CGSize itemSize = [super getFormItemSize:xoffset position:position];
    if (CGSizeEqualToSize(itemSize, CGSizeZero)) {
        return CGSizeMake(MINWIDTH, MINHEIGH);
    }
    return CGSizeMake(itemSize.width, MINHEIGH);
}

- (void)smallerCall
{
    CGSize size = self.PDFRect.size;
    CGSize newSize = size;
    if (self.rotation == 1 || self.rotation == 3) {
        newSize.height = size.height / 1.1;
    }else{
        newSize.width = size.width / 1.1;
    }
    
    CGSize newViewSize = CGSizeApplyAffineTransform(newSize, self.pdf2Pagetransform);
    newViewSize.width = ABS(newViewSize.width);
    newViewSize.height = ABS(newViewSize.height);
    CGPoint center = self.center;
    center.x -= (newViewSize.width - self.bounds.size.width) * 0.5;
    self.center = center;
    self.bounds = CGRectMake(0, 0, newViewSize.width, self.bounds.size.height);
    
    [self updatePDFRect];
    [self updateFillSignObjPosition];
    [super smallerCall];
    self.extensionsManager.isDocModified = YES;
}

- (void)biggerCall
{
    CGSize size = self.PDFRect.size;
    CGSize newSize = size;
    if (self.rotation == 1 || self.rotation == 3) {
        newSize.height = size.height * 1.1;
    }else{
        newSize.width = size.width * 1.1;
    }
    
    CGSize newViewSize = CGSizeApplyAffineTransform(newSize, self.pdf2Pagetransform);
    newViewSize.width = ABS(newViewSize.width);
    newViewSize.height = ABS(newViewSize.height);
    CGPoint center = self.center;
    center.x -= (newViewSize.width - self.bounds.size.width) * 0.5;
    self.center = center;
    self.bounds = CGRectMake(0, 0, newViewSize.width, self.bounds.size.height);
    
    [self updatePDFRect];
    [self updateFillSignObjPosition];
    [super biggerCall];
    [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
    self.extensionsManager.isDocModified = YES;
}

- (CGSize)minSizeForResizing {
    return CGSizeMake(MINWIDTH, MINHEIGH);
}

- (void)onPanGesture:(UIPanGestureRecognizer *)guest endGesture:(BOOL)end
{
    UIView *pageView = [self.pdfViewCtrl getPageView:self.pageIndex];
    CGPoint point = [guest locationInView:pageView];
    FillSignMenu *fillSignMenu = [FillSignMenu sharedMenuController];
    
    if (guest.state == UIGestureRecognizerStateBegan) {
        _lastPt = point;
        _isOnCtrolPt = YES;
        _isLeftCtrolPt = YES;
        CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[[self getPDFFillSignObj] getRect] pageIndex:self.pageIndex];
        CGPoint pvStartPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMidY(rect));
        CGPoint pvEndPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMidY(rect));
        UIImage *dragDot = self.dragDot;
        double newStartX = pvStartPoint.x;
        double newStartY = pvStartPoint.y;
        double newEndX = pvEndPoint.x;
        double newEndY = pvEndPoint.y;
        double LINE_DOT_MARGIN = dragDot.size.width / 2;
        if (pvStartPoint.x == pvEndPoint.x) {
            if (pvStartPoint.y < pvEndPoint.y) {
                newStartY -= LINE_DOT_MARGIN;
                newEndY += LINE_DOT_MARGIN;
            } else if (pvStartPoint.y > pvEndPoint.y) {
                newStartY += LINE_DOT_MARGIN;
                newEndY -= LINE_DOT_MARGIN;
            }
        } else if (pvStartPoint.x < pvEndPoint.x) {
            double angle = atan((pvEndPoint.y - pvStartPoint.y) / (pvEndPoint.x - pvStartPoint.x));
            newStartX = pvStartPoint.x - LINE_DOT_MARGIN * cos(angle);
            newStartY = pvStartPoint.y - LINE_DOT_MARGIN * sin(angle);
            newEndX = pvEndPoint.x + LINE_DOT_MARGIN * cos(angle);
            newEndY = pvEndPoint.y + LINE_DOT_MARGIN * sin(angle);
        } else if (pvStartPoint.x > pvEndPoint.x) {
            double angle = atan((pvEndPoint.y - pvStartPoint.y) / (pvStartPoint.x - pvEndPoint.x));
            newStartX = pvStartPoint.x + LINE_DOT_MARGIN * cos(angle);
            newStartY = pvStartPoint.y - LINE_DOT_MARGIN * sin(angle);
            newEndX = pvEndPoint.x - LINE_DOT_MARGIN * cos(angle);
            newEndY = pvEndPoint.y + LINE_DOT_MARGIN * sin(angle);
        }

        CGRect startPointRect = CGRectMake(newStartX - dragDot.size.width / 2, newStartY - dragDot.size.width / 2, dragDot.size.width * 2, dragDot.size.width);
        startPointRect = CGRectInset(startPointRect, -10, -10);
        CGRect endPointRect = CGRectMake(newEndX - dragDot.size.width / 2, newEndY - dragDot.size.width / 2, dragDot.size.width * 2, dragDot.size.width);
        endPointRect = CGRectInset(endPointRect, -10, -10);
        if (CGRectContainsPoint(startPointRect, point)) {
            _isLeftCtrolPt = YES;
        } else if (CGRectContainsPoint(endPointRect, point)) {
            _isLeftCtrolPt = NO;
        } else {
            _isOnCtrolPt = NO;
        }
        self.isMove = !_isOnCtrolPt;
        if (_isOnCtrolPt) {
            self.isOnchange = YES;
            [fillSignMenu setMenuVisible:NO animated:NO];
            [self showMagnifier:self.pageIndex point:point];
            self.isMove = YES;
        }else{
            [super onPanGesture:guest endGesture:end];
        }
    }else if (guest.state == UIGestureRecognizerStateChanged) {
        if (_isOnCtrolPt) {
            [fillSignMenu setMenuVisible:NO animated:NO];
            if (!self.isOnchange)
            {
                [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
                return;
            }
            CGPoint offset = CGPointMake(point.x - _lastPt.x, point.y - _lastPt.y);
            CGRect widthRect = CGRectApplyAffineTransform(CGRectMake(0, 0, MINWIDTH, MINWIDTH), self.pdf2Pagetransform);
            float minwidth = ABS(widthRect.size.width);
            CGFloat xoffset = offset.x;
            if (_isLeftCtrolPt) {
                if ((CGRectGetMinX(self.frame) + offset.x) > self.edgeInsets.left) {
                    CGSize size = self.bounds.size;
                    if (size.width - xoffset> minwidth) {
                        self.center = CGPointMake(self.center.x + offset.x, self.center.y);
                        size.width -= xoffset;
                        self.bounds = CGRectMake(0, 0, size.width, size.height);
                        [self updatePDFRect];
                    }
                }
            }else{
                if ((CGRectGetMaxX(self.frame) + offset.x) < (pageView.frame.size.width - self.edgeInsets.right)) {
                    CGSize size = self.bounds.size;
                    if (size.width + xoffset> minwidth) {
                        size.width += xoffset;
                        self.bounds = CGRectMake(0, 0, size.width, size.height);
                        [self updatePDFRect];
                    }
                }
            }
            _lastPt = point;
            [self moveMagnifier:self.pageIndex point:point];
        }else{
            [super onPanGesture:guest endGesture:end];
        }
    }else if (guest.state == UIGestureRecognizerStateEnded){
        if (_isOnCtrolPt) {
            if (!self.isOnchange)
            {
                [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
                return;
            }
            self.isOnchange = NO;
            [self updatePDFRect];
            [self updateFillSignObjPosition];
            
            CGRect rect = self.frame;
            rect = CGRectInset(rect, -15, -15);
            FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:self.type];
            [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
            [fillSignMenu setMenuVisible:YES animated:NO];
            
            self.extensionsManager.isDocModified = YES;
            _lastPt = CGPointZero;
            [self closeMagnifier];
            self.isMove = NO;
        }else{
            [super onPanGesture:guest endGesture:end];
        }
    }
    [self.pdfViewCtrl refresh:self.pageIndex needRender:YES];
    if (end) {
        if (_isOnCtrolPt) {
            self.isOnchange = NO;
            [self updatePDFRect];
            [self updateFillSignObjPosition];
            
            CGRect rect = self.frame;
            rect = CGRectInset(rect, -15, -15);
            FSFillSignItemtype itemtype = [self getFillSignItemTypeByFSFillSignType:self.type];
            [fillSignMenu setTargetRect:rect fillSignType:itemtype inView:pageView];
            [fillSignMenu setMenuVisible:YES animated:NO];
            
            self.extensionsManager.isDocModified = YES;
            _lastPt = CGPointZero;
            [self closeMagnifier];
        }
    }
}

@end
