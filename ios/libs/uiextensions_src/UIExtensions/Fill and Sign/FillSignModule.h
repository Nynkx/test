/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <Foundation/Foundation.h>
#import "FillSignToolHandler.h"

@interface FillSignModule : NSObject <IDocEventListener, IToolEventListener, IStateChangeListener, IModule>

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager;
@property (nonatomic, strong) FillSignToolHandler *fillSignToolHandler;
- (NSString *)getPreDefineStr;
- (NSArray *)getPreDefineContentArray;
- (void)cancelSelectItem;
- (void)updateItem;
@end
