/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FillSignMenu.h"
#import "FSWeakPointerArray.h"

#define ICONWIDTH 40
#define ARROWHEIGH 10
#define ARROWWIDTH 20

@interface FillSignMenu()
@property (nonatomic,weak) UIView* targetView;
@property (nonatomic,assign) CGRect targetRect;

@property (nonatomic, strong) UIView *menuView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *arrowViewUp;
@property (nonatomic, strong) UIView *arrowViewDown;
@property (nonatomic, assign) BOOL isMoreMenus;

@property(nonatomic,strong) NSMutableArray<UIButton *> *menuItems;
@property(nonatomic,strong) NSArray *toolImageNameArr;
@property (nonatomic, strong) FSWeakPointerArray *layoutListeners;
@property (nonatomic, strong) FSWeakPointerArray *eventListeners;
@end



@implementation FillSignMenu
static FillSignMenu *staticFillSignMenuInstance = nil;


+ (instancetype)sharedMenuController{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticFillSignMenuInstance = [[self alloc] init];
        staticFillSignMenuInstance.arrowDirection = FillSignMenuArrowDirection_Down;
        staticFillSignMenuInstance.menuItems = [NSMutableArray arrayWithCapacity:7];
        staticFillSignMenuInstance.toolImageNameArr = @[@"fill_sign_menu_check",@"fill_sign_menu_X",@"fill_sign_menu_dot",@"fill_sign_menu_line",@"fill_sign_menu_roundrect",@"fill_sign_menu_text",@"fill_sign_menu_segtext"];
        staticFillSignMenuInstance.layoutListeners = [FSWeakPointerArray array];
        staticFillSignMenuInstance.eventListeners = [FSWeakPointerArray array];
    });
    return staticFillSignMenuInstance;
}

- (void)registerFillSignMenuLayout:(id<FillSignMenuLayout>)listener{
    [_layoutListeners addObject:listener];
}

- (void)unregisterFillSignMenuLayout:(id<FillSignMenuLayout>)listener{
    [_layoutListeners removeObject:listener];
}

- (void)registerFillSignMenuEvent:(id<FillSignMenuEvent>)listener{
    [_eventListeners addObject:listener];
}

- (void)unregisterFillSignMenuEvent:(id<FillSignMenuEvent>)listener{
    [_eventListeners removeObject:listener];
}

- (void)setMenuVisible:(BOOL)menuVisible animated:(BOOL)animated
{
    if ([UIMenuController sharedMenuController].isMenuVisible && menuVisible) {
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
        return;
    }
    _menuVisible = menuVisible;
    if (menuVisible) {
        UIView *menuView = self.menuView;
        [self initMenuContent];
        UIWindow *keywindow = [NSObject fs_getForegroundActiveWindow];
        [keywindow addSubview:menuView];
        
        if (!_isMoreMenus && _type != FSFillSignItemtypeNone) {
            [_layoutListeners enumerateObjectsUsingBlock:^(id<FillSignMenuLayout>listener, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([listener respondsToSelector:@selector(canBiggerEnable:inView:)]) {
                    BOOL canBigger = [listener canBiggerEnable:self inView:self.targetView];
                    UIButton *bigBtn = [self getItemByTag:FSFillSignItemtypeBigger];
                    bigBtn.enabled = canBigger;
                }

                if ([listener respondsToSelector:@selector(canSmallerEnable:inView:)]) {
                    BOOL canSmaller = [listener canSmallerEnable:self inView:self.targetView];
                    UIButton *smallBtn = [self getItemByTag:FSFillSignItemtypeSmaller];
                    smallBtn.enabled = canSmaller;
                }
            }];
        }
    }else{
        [_menuView removeFromSuperview];
        _menuView = nil;
    }
}

- (void)setTargetRect:(CGRect)targetRect fillSignType:(FSFillSignItemtype)type inView:(UIView *)targetView
{
    _targetRect = targetRect;
    if (_type != type) {
        _isMoreMenus = NO;
        [_menuView removeFromSuperview];
        _menuView = nil;
    }
    _type = type;
    _targetView = targetView;
    _isMoreMenus = NO;
}

- (void)updateTargetRect:(CGRect)targetRect inView:(nonnull UIView *)targetView
{
    _targetRect = targetRect;
    _targetView = targetView;
    [self caculateShowPositionAndFrame];
    if (!_isMoreMenus && _type != FSFillSignItemtypeNone) {
        [_layoutListeners enumerateObjectsUsingBlock:^(id<FillSignMenuLayout>listener, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([listener respondsToSelector:@selector(canBiggerEnable:inView:)]) {
                BOOL canBigger = [listener canBiggerEnable:self inView:self.targetView];
                UIButton *bigBtn = [self getItemByTag:FSFillSignItemtypeBigger];
                bigBtn.enabled = canBigger;
            }

            if ([listener respondsToSelector:@selector(canSmallerEnable:inView:)]) {
                BOOL canSmaller = [listener canSmallerEnable:self inView:self.targetView];
                UIButton *smallBtn = [self getItemByTag:FSFillSignItemtypeSmaller];
                smallBtn.enabled = canSmaller;
            }
        }];

    }
}

- (void)updateFrame
{
    [self caculateShowPositionAndFrame];
    if (!_isMoreMenus  && _type != FSFillSignItemtypeNone) {
        [_layoutListeners enumerateObjectsUsingBlock:^(id<FillSignMenuLayout>listener, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([listener respondsToSelector:@selector(canBiggerEnable:inView:)]) {
                BOOL canBigger = [listener canBiggerEnable:self inView:self.targetView];
                UIButton *bigBtn = [self getItemByTag:FSFillSignItemtypeBigger];
                bigBtn.enabled = canBigger;
            }
            
            if ([listener respondsToSelector:@selector(canSmallerEnable:inView:)]) {
                BOOL canSmaller = [listener canSmallerEnable:self inView:self.targetView];
                UIButton *smallBtn = [self getItemByTag:FSFillSignItemtypeSmaller];
                smallBtn.enabled = canSmaller;
            }
        }];
    }
}

- (UIView *)menuView
{
    if (_menuView == nil) {
        _menuView = [[UIView alloc] init];
        _contentView = [[UIView alloc] init];
        _contentView.layer.cornerRadius = 8.0;
        _contentView.layer.masksToBounds = YES;
        _contentView.backgroundColor = [UIColor blackColor];
        _arrowViewUp = [[UIView alloc] init];
        _arrowViewDown = [[UIView alloc] init];
        //_arrowView.backgroundColor = [UIColor blackColor];
        CAShapeLayer *upLayer = [[CAShapeLayer alloc] init];
        UIBezierPath *path = [[UIBezierPath alloc] init];
        [path moveToPoint:CGPointMake(0, 0)];
        [path addLineToPoint:CGPointMake(ARROWWIDTH * 0.5, ARROWHEIGH)];
        [path addLineToPoint:CGPointMake(ARROWWIDTH, 0)];
        [path closePath];
        upLayer.path = path.CGPath;

        [_arrowViewUp.layer addSublayer:upLayer];
        
        CAShapeLayer *downLayer = [[CAShapeLayer alloc] init];
        UIBezierPath *path1 = [[UIBezierPath alloc] init];
        [path1 moveToPoint:CGPointMake(0, ARROWHEIGH)];
        [path1 addLineToPoint:CGPointMake(ARROWWIDTH, ARROWHEIGH)];
        [path1 addLineToPoint:CGPointMake(ARROWWIDTH * 0.5, 0)];
        [path1 closePath];
        downLayer.path = path1.CGPath;

        [_arrowViewDown.layer addSublayer:downLayer];
        
        [_menuView addSubview:_contentView];
        [_menuView addSubview:_arrowViewUp];
        
        if (_type == FSFillSignItemtypeNone) {
            NSInteger tag = FSFillSignItemtypeCheck;
            if (_menuItems.count > 0){
                for (UIButton *btn in _menuItems) {
                    [btn removeFromSuperview];
                }
                [_menuItems removeAllObjects];
            }
            NSUInteger count = _toolImageNameArr.count;
            for (int i = 0; i < count; i++) {
                if (tag != _type) {
                    UIButton *btn = [[UIButton alloc] init];
                    [btn setImage:[UIImage fs_ImageNamed:_toolImageNameArr[i]] forState:UIControlStateNormal];
                    [btn addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
                    btn.frame = CGRectMake( _menuItems.count * ICONWIDTH, 0, ICONWIDTH, ICONWIDTH);
                    btn.tag = tag;
                    [_contentView addSubview:btn];
                    [_menuItems addObject:btn];
                }
                tag = (tag << 1);
            }
        }else{
            if (_isMoreMenus) {
                NSInteger tag = FSFillSignItemtypeCheck;
                if (_menuItems.count > 0){
                    for (UIButton *btn in _menuItems) {
                        [btn removeFromSuperview];
                    }
                    [_menuItems removeAllObjects];
                }
                NSArray *tmpArr = _toolImageNameArr.copy;
                if (_type == FSFillSignItemtypeCheck ||
                    _type == FSFillSignItemtypeX ||
                    _type == FSFillSignItemtypeDot ||
                    _type == FSFillSignItemtypeLine ||
                    _type == FSFillSignItemtypeRoundrect) {
                    NSMutableArray *tmpMuArr = tmpArr.mutableCopy;
                    while (tmpMuArr.count > 5) {
                        [tmpMuArr removeLastObject];
                    }
                    tmpArr = tmpMuArr.copy;
                }
                for (int i = 0; i < tmpArr.count + 1; i++) {
                    if (tag != _type) {
                        UIButton *btn = [[UIButton alloc] init];
                        [btn addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
                        btn.frame = CGRectMake( _menuItems.count * ICONWIDTH, 0, ICONWIDTH, ICONWIDTH);
                        [_contentView addSubview:btn];
                        [_menuItems addObject:btn];
                        if (i < tmpArr.count) {
                            [btn setImage:[UIImage fs_ImageNamed:tmpArr[i]] forState:UIControlStateNormal];
                            btn.tag = tag;
                        }else{
                            [btn setImage:[UIImage fs_ImageNamed:@"fill_sign_menu_more"] forState:UIControlStateNormal];
                            btn.tag = FSFillSignItemtypeMore;
                        }
                    }
                    tag = (tag << 1);
                }
            }else{
                NSInteger tag = FSFillSignItemtypeSmaller;
                if (_menuItems.count > 0){
                    for (UIButton *btn in _menuItems) {
                        [btn removeFromSuperview];
                    }
                    [_menuItems removeAllObjects];
                }
                for (int i = 0; i < 3; i++) {
                    if (tag == FSFillSignItemtypeDelete) {
                        UIButton *btn = [[UIButton alloc] init];
                        [btn addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
                        btn.frame = CGRectMake((_menuItems.count) * ICONWIDTH, 0, ICONWIDTH, ICONWIDTH);
                        btn.tag = FSFillSignItemtypeMore;
                        [btn setImage:[UIImage fs_ImageNamed:@"fill_sign_menu_more"] forState:UIControlStateNormal];
                        [_contentView addSubview:btn];
                        [_menuItems addObject:btn];
                    }
                    UIButton *btn = [[UIButton alloc] init];
                    [btn addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
                    btn.frame = CGRectMake( _menuItems.count * ICONWIDTH, 0, ICONWIDTH, ICONWIDTH);
                    btn.tag = tag;
                    if (btn.tag == FSFillSignItemtypeSmaller) [btn setImage:ImageNamed(@"fill_sign_menu_smaller") forState:UIControlStateNormal];
                    if (btn.tag == FSFillSignItemtypeBigger) [btn setImage:[UIImage fs_ImageNamed:@"fill_sign_menu_bigger"] forState:UIControlStateNormal];
                    if (btn.tag == FSFillSignItemtypeDelete) [btn setImage:[UIImage fs_ImageNamed:@"fill_sign_menu_delete"] forState:UIControlStateNormal];
                    [_contentView addSubview:btn];
                    [_menuItems addObject:btn];
                    tag = (tag << 1);
                }
                
            }
        }
    }
    return _menuView;
}

- (void)caculateShowPositionAndFrame
{
    CGFloat menuContentWidth = self.menuItems.count * ICONWIDTH;
    CGSize menuContentSize = CGSizeMake(menuContentWidth, ICONWIDTH);
    
    UIWindow *keywindow = [NSObject fs_getForegroundActiveWindow];
    CGRect targetOnWindow = [_targetView convertRect:_targetRect toView:keywindow];
    CGRect targetViewOnWindow = [_targetView convertRect:_targetView.frame toView:keywindow];
    if (CGRectContainsRect(targetViewOnWindow, targetOnWindow)) targetViewOnWindow = targetOnWindow;
    if (!_targetView || targetOnWindow.origin.x + targetOnWindow.size.width < 0 || targetOnWindow.origin.y + targetOnWindow.size.height < 0 || targetOnWindow.origin.x > keywindow.frame.size.width || targetOnWindow.origin.y > keywindow.frame.size.height) {
        _menuView.hidden = YES;
        return;
    }
    _menuView.hidden = NO;
    
    CGFloat arrowx = targetOnWindow.origin.x + targetOnWindow.size.width * 0.5;
    if (arrowx - menuContentWidth * 0.5 >= 0 && arrowx + menuContentWidth * 0.5 <= keywindow.frame.size.width) {
        if (CGRectGetMinY(targetOnWindow) - ABS(CGRectGetMinY(targetOnWindow) - CGRectGetMinY(targetViewOnWindow)) > ARROWHEIGH + ICONWIDTH) {
            CGRect menuFrame = CGRectMake(CGRectGetMidX(targetOnWindow) - menuContentWidth * 0.5, CGRectGetMinY(targetOnWindow) - ARROWHEIGH - ICONWIDTH, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, 0, menuContentSize.width, menuContentSize.height);
            if (_arrowViewDown.superview) {
                [_arrowViewDown removeFromSuperview];
                [_menuView addSubview:_arrowViewUp];
            }
            _arrowViewUp.frame = CGRectMake(menuContentSize.width * 0.5 - ARROWWIDTH * 0.5, ICONWIDTH - 1, ARROWWIDTH, ARROWHEIGH);
            
        }else if (keywindow.frame.size.height - CGRectGetMaxY(targetOnWindow) > ARROWHEIGH + ICONWIDTH){
            CGRect menuFrame = CGRectMake(targetOnWindow.origin.x + targetOnWindow.size.width * 0.5 - menuContentWidth * 0.5, targetOnWindow.origin.y + targetOnWindow.size.height, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, ARROWHEIGH, menuContentSize.width, menuContentSize.height);
            //_arrowView.frame = CGRectMake(menuContentSize.width * 0.5 - ARROWWIDTH * 0.5, 0, ARROWWIDTH, ARROWHEIGH);
            if (_arrowViewUp.superview) {
                [_arrowViewUp removeFromSuperview];
                [_menuView addSubview:_arrowViewDown];
            }
            _arrowViewDown.frame = CGRectMake(menuContentSize.width * 0.5 - ARROWWIDTH * 0.5, 1, ARROWWIDTH, ARROWHEIGH);
        }else{
            CGRect menuFrame = CGRectMake(targetOnWindow.origin.x + targetOnWindow.size.width * 0.5 - menuContentWidth * 0.5, targetOnWindow.size.height * 0.5 - ARROWHEIGH - ICONWIDTH, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, 0, menuContentSize.width, menuContentSize.height);
            
            if (_arrowViewDown.superview) {
                [_arrowViewDown removeFromSuperview];
                [_menuView addSubview:_arrowViewUp];
            }
            _arrowViewUp.frame = CGRectMake(menuContentSize.width * 0.5 - ARROWWIDTH * 0.5, ICONWIDTH - 1, ARROWWIDTH, ARROWHEIGH);
        }
        return;
    }
    
    if (arrowx - menuContentWidth * 0.5 < 0 && arrowx + menuContentWidth * 0.5< keywindow.frame.size.width) {
        if (CGRectGetMinY(targetOnWindow) - ABS(CGRectGetMinY(targetOnWindow) - CGRectGetMinY(targetViewOnWindow)) > ARROWHEIGH + ICONWIDTH) {
            CGRect menuFrame = CGRectMake(0, targetOnWindow.origin.y - ARROWHEIGH - ICONWIDTH, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, 0, menuContentSize.width, menuContentSize.height);
            if (_arrowViewDown.superview) {
                [_arrowViewDown removeFromSuperview];
                [_menuView addSubview:_arrowViewUp];
            }
            _arrowViewUp.frame = CGRectMake(arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5 < 8 ? 8 : arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5, ICONWIDTH - 1, ARROWWIDTH, ARROWHEIGH);
        }else if (keywindow.frame.size.height - CGRectGetMaxY(targetOnWindow) > ARROWHEIGH + ICONWIDTH){
            CGRect menuFrame = CGRectMake(0, targetOnWindow.origin.y + targetOnWindow.size.height, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, ARROWHEIGH, menuContentSize.width, menuContentSize.height);
           
            if (_arrowViewUp.superview) {
                [_arrowViewUp removeFromSuperview];
                [_menuView addSubview:_arrowViewDown];
            }
             _arrowViewDown.frame = CGRectMake(arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5 < 8 ? 8 : arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5, 1, ARROWWIDTH, ARROWHEIGH);
        }else{
            CGRect menuFrame = CGRectMake(0, targetOnWindow.size.height * 0.5 - ARROWHEIGH - ICONWIDTH, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, 0, menuContentSize.width, menuContentSize.height);
            
            if (_arrowViewDown.superview) {
                [_arrowViewDown removeFromSuperview];
                [_menuView addSubview:_arrowViewUp];
            }
            _arrowViewUp.frame = CGRectMake(arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5 < 8 ? 8 : arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5, ICONWIDTH - 1, ARROWWIDTH, ARROWHEIGH);
        }
        return;
    }
    
    if (arrowx - menuContentWidth * 0.5 > 0 && arrowx + menuContentWidth * 0.5 > keywindow.frame.size.width) {
        if (CGRectGetMinY(targetOnWindow) - ABS(CGRectGetMinY(targetOnWindow) - CGRectGetMinY(targetViewOnWindow)) > ARROWHEIGH + ICONWIDTH) {
            CGRect menuFrame = CGRectMake(keywindow.frame.size.width - menuContentWidth, targetOnWindow.origin.y - ARROWHEIGH - ICONWIDTH, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, 0, menuContentSize.width, menuContentSize.height);
           
            if (_arrowViewDown.superview) {
                [_arrowViewDown removeFromSuperview];
                [_menuView addSubview:_arrowViewUp];
            }
            _arrowViewUp.frame = CGRectMake(arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5 > menuFrame.size.width - 28 ? menuFrame.size.width - 28 : arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5, ICONWIDTH - 1, ARROWWIDTH, ARROWHEIGH);
        }else if (keywindow.frame.size.height - CGRectGetMaxY(targetOnWindow) > ARROWHEIGH + ICONWIDTH){
            CGRect menuFrame = CGRectMake(keywindow.frame.size.width - menuContentWidth, targetOnWindow.origin.y + targetOnWindow.size.height, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, ARROWHEIGH, menuContentSize.width, menuContentSize.height);
            
            if (_arrowViewUp.superview) {
                [_arrowViewUp removeFromSuperview];
                [_menuView addSubview:_arrowViewDown];
            }
            _arrowViewDown.frame = CGRectMake(arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5 > menuFrame.size.width - 28 ? menuFrame.size.width - 28 : arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5, 1, ARROWWIDTH, ARROWHEIGH);
        }else{
            CGRect menuFrame = CGRectMake(keywindow.frame.size.width - menuContentWidth, targetOnWindow.size.height * 0.5 - ARROWHEIGH - ICONWIDTH, menuContentWidth, ARROWHEIGH + ICONWIDTH);
            _menuView.frame = menuFrame;
            _contentView.frame = CGRectMake(0, 0, menuContentSize.width, menuContentSize.height);
            
            if (_arrowViewDown.superview) {
                [_arrowViewDown removeFromSuperview];
                [_menuView addSubview:_arrowViewUp];
            }
            _arrowViewUp.frame = CGRectMake(arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5 > menuFrame.size.width - 28 ? menuFrame.size.width - 28 : arrowx - menuFrame.origin.x - ARROWWIDTH * 0.5, ICONWIDTH - 1, ARROWWIDTH, ARROWHEIGH);
        }
        return;
    }
}

- (void)initMenuContent
{
    [self caculateShowPositionAndFrame];
}


- (void)itemClick:(UIButton *)item
{
    if (_type == FSFillSignItemtypeNone) {
        _type = item.tag;
        [self setMenuVisible:NO animated:NO];
        [self setMenuVisible:YES animated:NO];
    }else{
        if (item.tag == FSFillSignItemtypeMore) {
            _isMoreMenus = !_isMoreMenus;
            [self setMenuVisible:NO animated:NO];
            [self setMenuVisible:YES animated:NO];
        }
    }
    
    if (item.tag > FSFillSignItemtypeNone && item.tag < FSFillSignItemtypeMore && _eventListeners.count) {
        [_eventListeners enumerateObjectsUsingBlock:^(id<FillSignMenuEvent>listener, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([listener respondsToSelector:@selector(didSelectedMenu:type:)]) {
                [listener didSelectedMenu:self type:item.tag];
            }
        }];
        
        [_layoutListeners enumerateObjectsUsingBlock:^(id<FillSignMenuLayout>listener, NSUInteger idx, BOOL * _Nonnull stop) {
            if (item.tag == FSFillSignItemtypeBigger) {
                if ([listener respondsToSelector:@selector(canBiggerEnable:inView:)]) {
                    BOOL canBigger = [listener canBiggerEnable:self inView:self.targetView];
                    UIButton *bigBtn = [self getItemByTag:FSFillSignItemtypeBigger];
                    bigBtn.enabled = canBigger;
                    
                    UIButton *smallBtn = [self getItemByTag:FSFillSignItemtypeSmaller];
                    smallBtn.enabled = YES;
                }
            }
            
            if (item.tag == FSFillSignItemtypeSmaller) {
                if ([listener respondsToSelector:@selector(canSmallerEnable:inView:)]) {
                    BOOL canSmaller = [listener canSmallerEnable:self inView:self.targetView];
                    UIButton *smallBtn = [self getItemByTag:FSFillSignItemtypeSmaller];
                    smallBtn.enabled = canSmaller;
                    
                    UIButton *bigBtn = [self getItemByTag:FSFillSignItemtypeBigger];
                    bigBtn.enabled = YES;
                }
            }
        }];
    }
}

- (UIButton *)getItemByTag:(FSFillSignItemtype)type
{
    if (type == FSFillSignItemtypeNone) {
        return nil;
    }
    for (int i = 0; i < _menuItems.count; i++) {
        UIButton *btn = [_menuItems objectAtIndex:i];
        if (btn.tag == type) {
            return btn;
        }
    }
    return nil;
}
@end
