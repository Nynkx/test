/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "NoteModule.h"
#import <FoxitRDK/FSPDFViewControl.h>

#import "NoteAnnotHandler.h"
#import "NoteToolHandler.h"
#import "Utility.h"

@interface NoteModule () {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

@property (nonatomic, weak) TbBaseItem *propertyItem;

@end

@implementation NoteModule

- (NSString *)getName {
    return @"Note";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
        
        NoteToolHandler* toolHanlder = [[NoteToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_pdfViewCtrl registerDocEventListener:toolHanlder];
        [_extensionsManager registerToolHandler:toolHanlder];
        
        NoteAnnotHandler* annotHandler = [[NoteAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];
        //        [_extensionsManager.propertyBar registerPropertyBarListener:annotHandler];
    }
    return self;
}

- (void)loadModule {
    _extensionsManager.annotationToolsBar.noteClicked = ^() {
        [self annotItemClicked];
    };
}

- (void)annotItemClicked {
    [_extensionsManager setCurrentToolHandler:[_extensionsManager getToolHandlerByName:Tool_Note]];
    [_extensionsManager changeState:STATE_ANNOTTOOL];

    [_extensionsManager.toolSetBar removeAllItems];

        TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    [_extensionsManager registerAnnotPropertyListener:self];
    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    self.propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:FSAnnotNote]];
    [_extensionsManager.toolSetBar addItem:self.propertyItem displayPosition:Position_CENTER];
    self.propertyItem.onTapClick = ^(TbBaseItem *item) {
        CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_extensionsManager.pdfViewCtrl];
        if (DEVICE_iPHONE) {
            [self->_extensionsManager showProperty:FSAnnotNote rect:rect inView:self->_extensionsManager.pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:FSAnnotNote rect:item.contentView.bounds inView:item.contentView];
        }
    };

    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_continue")];
    } else {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_single")];
    }
    continueItem.tag = 3;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_extensionsManager.pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            item.imageNormal = ImageNamed(@"annot_continue");
            item.imageSelected = ImageNamed(@"annot_continue");
        } else {
            item.imageNormal = ImageNamed(@"annot_single");
            item.imageSelected = ImageNamed(@"annot_single");
        }

        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };

    [Utility showAnnotationType:FSLocalizedForKey(@"kNote") type:FSAnnotNote pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.propertyItem.contentView.superview);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];

    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.left.equalTo(self.propertyItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];

    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.right.equalTo(self.propertyItem.contentView.mas_left).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_extensionsManager.pdfViewCtrl];
}

#pragma mark - IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotNote) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

@end
