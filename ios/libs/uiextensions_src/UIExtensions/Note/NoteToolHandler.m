/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "NoteToolHandler.h"
#import "NoteDialog.h"

@interface NoteToolHandler () <IDocEventListener>

@end

@implementation NoteToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotNote;
    }
    return self;
}

- (NSString *)getName {
    return Tool_Note;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    CGRect rect1 = [pageView frame];
    CGSize size = rect1.size;
    if (point.x > size.width || point.y > size.height || point.x < 0 || point.y < 0)
        return NO;
    float scale = [_pdfViewCtrl getPageViewWidth:pageIndex] / 1000.0;
    
    FSRectF *iconfsrect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:NOTE_ANNOTATION_WIDTH top1:NOTE_ANNOTATION_WIDTH];
    CGRect iconrect = [_pdfViewCtrl convertPdfRectToPageViewRect:iconfsrect pageIndex:pageIndex];
    float iconwidth = iconrect.size.width;
    CGRect rect = CGRectMake(point.x - NOTE_ANNOTATION_WIDTH * scale / 2, point.y - NOTE_ANNOTATION_WIDTH * scale / 2, iconwidth, iconwidth);
    
    FSRectF *dibRect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:pageIndex];
    
    FSPointF *tempPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSRectF *annotTempRect = [[FSRectF alloc] init];
    annotTempRect.left = tempPoint.x;
    annotTempRect.bottom = tempPoint.y - NOTE_ANNOTATION_WIDTH;
    annotTempRect.right = annotTempRect.left + NOTE_ANNOTATION_WIDTH;
    annotTempRect.top = annotTempRect.bottom + NOTE_ANNOTATION_WIDTH;
    dibRect = annotTempRect;

    NoteDialog *noteDialog = [[NoteDialog alloc] init];
    [noteDialog show:nil replyAnnots:nil title:nil];
    self.currentVC = noteDialog;
    typeof(self) __weak weakSelf = self;
    noteDialog.noteEditDone = ^(NoteDialog *dialog) {
        typeof(weakSelf) __strong strongSelf = weakSelf;
        assert(strongSelf);
        strongSelf.currentVC = nil;
        FSPDFPage *page = [strongSelf->_pdfViewCtrl.currentDoc getPage:pageIndex];
        if (!page || [page isEmpty])
            return;
        FSNote *annot = [[FSNote alloc] initWithAnnot:[page addAnnot:FSAnnotNote rect:dibRect]];
        annot.icon = strongSelf->_extensionsManager.noteIcon;
        annot.color = [strongSelf->_extensionsManager getPropertyBarSettingColor:self.type];
        annot.opacity = [strongSelf->_extensionsManager getAnnotOpacity:self.type] / 100.0f;
        annot.contents =[Utility encodeEmojiString:[dialog getContent]];
        annot.NM = [Utility getUUID];
        annot.author = _extensionsManager.annotAuthor;
        annot.subject = @"Note";
        annot.flags = FSAnnotFlagPrint | FSAnnotFlagNoZoom | FSAnnotFlagNoRotate;
        FSDateTime *now = [Utility convert2FSDateTime:[NSDate date]];
        [annot setCreationDateTime:now];
        [annot setModifiedDateTime:now];
        id<IAnnotHandler> annotHandler = [strongSelf->_extensionsManager getAnnotHandlerByAnnot:annot];
        [annotHandler addAnnot:annot];
    };

    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (self != _extensionsManager.currentToolHandler) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
}

#pragma mark IDocEventListener

- (void)onDocWillClose:(FSPDFDoc *)document {
    if (self.currentVC) {
        [self.currentVC dismissViewControllerAnimated:NO completion:nil];
    }
}

@end
