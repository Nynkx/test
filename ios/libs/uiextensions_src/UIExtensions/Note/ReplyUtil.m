/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ReplyUtil.h"

@implementation ReplyUtil

+ (void)getGroupReplysInDocument:(FSPDFDoc *)document markupArray:(FSMarkupArray *)markupArray replys:(NSMutableArray *)replys{
    if (![markupArray getSize])
        return;
    for (int i = 0; i < markupArray.getSize; i++ ) {
        FSMarkup *markup = [markupArray getAt:i];
        NSMutableArray *tmp = [NSMutableArray array];
        [self getReplysInDocument:document annot:markup replys:tmp];
        [replys addObjectsFromArray:tmp];
    }
}

+ (void)getReplysInDocument:(FSPDFDoc *)document annot:(FSAnnot *)rootAnnot replys:(NSMutableArray *)replys {
    if (![rootAnnot isMarkup])
        return;
    FSMarkup *rootMarkup = [[FSMarkup alloc] initWithAnnot:rootAnnot];
    int countOfReplies = [rootMarkup getReplyCount];
    for (int i = 0; i < countOfReplies; i++) {
        FSNote *reply = [rootMarkup getReply:i];
        for (FSNote *note in replys) {
            //Loop detect!
            if ([note.NM isEqualToString:reply.NM])
                return;
        }
        [replys addObject:reply];
        [ReplyUtil getReplysInDocument:document annot:reply replys:replys];
    }
}

@end
