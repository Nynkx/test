/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "NoteAnnotHandler.h"

@implementation NoteAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.pageEdgeInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
        self.annotBorderEdgeInsets = UIEdgeInsetsMake(-2, -2, -2, -2);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotNote;
}

- (BOOL)canResizeAnnot:(FSAnnot *)annot {
    return NO;
}

- (void)showStyle {
    [self.extensionsManager.propertyBar setColors:@[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff]];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_ICONTYPE frame:CGRectZero];
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_ICONTYPE lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_ICONTYPE intValue:annot.icon];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:[NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]]];
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:annot.pageIndex]];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:annot.pageIndex];
    if ([Utility isAnnot:self.extensionsManager.currentAnnot uniqueToAnnot:annot]) {
        if (!(pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint])) {
            [self.extensionsManager setCurrentAnnot:nil];
        } else {
            if (self.extensionsManager.shouldShowMenu && !self.extensionsManager.shouldShowPropertyBar) {
                [self showMenu];
            }
        }
    } else {
        self.shouldShowMenuOnSelection = NO;
        [self.extensionsManager setCurrentAnnot:annot];
        self.shouldShowMenuOnSelection = YES;
        [self comment];
    }
    return YES;
}

@end
