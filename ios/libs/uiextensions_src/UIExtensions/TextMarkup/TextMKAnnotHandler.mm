/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "TextMKAnnotHandler.h"
#import "UIExtensionsConfig+private.h"

@interface MKAnnotHandler ()

@property (nonatomic, assign) int startReadPos;
@property (nonatomic, assign) int endReadPos;
@end



@implementation MKAnnotHandler {
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super initWithUIExtensionsManager:extensionsManager];
    if (self) {
        _taskServer = extensionsManager.taskServer;
        self.annotBorderEdgeInsets = UIEdgeInsetsMake(-5, -5, -5, -5);
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotHighlight;
}

- (BOOL)shouldDrawAnnot:(FSAnnot *)annot inPDFViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    return YES;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
   
    if (annot.canCopyText && [Utility canCopyText:self.extensionsManager.pdfViewCtrl] && self.extensionsManager.config.copyText) {
        options |= FSMenuOptionCopyText;
        options |= FSMenuOptionSpeech;
    }
    return options;
}

- (BOOL)canMoveAnnot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)canResizeAnnot:(FSAnnot *)annot {
    return NO;
}

- (void)showStyle {
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    NSArray *colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.extensionsManager.propertyBar setColors:colors];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY frame:CGRectZero];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];

    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
}

- (void)speech{
    
    SpeechModule *speech = self.extensionsManager.speechModule;
    [speech speakFromTextMarkup:[[FSTextMarkup alloc] initWithAnnot:self.annot]];
}


- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    if (annot.type == FSAnnotStrikeOut && [Utility isReplaceText:[[FSStrikeOut alloc] initWithAnnot:annot]]) {
        // this is a |replace| annot
        return NO;
    }
    return [super isHitAnnot:annot point:point];
}

@end
