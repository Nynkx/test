/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MarkupModule.h"
#import "TextMKAnnotHandler.h"
#import "TextMKToolHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>
#import "UIExtensionsConfig.h"

@interface MarkupModule () {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
    FSAnnotType _annotType;
}
@property (nonatomic, weak) TbBaseItem *propertyItem;
@end

@implementation MarkupModule

- (NSString *)getName {
    return @"Markup";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
        MKToolHandler* toolHandler = [[MKToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        MKAnnotHandler* annotHandler = [[MKAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];
    }
    return self;
}

- (void)loadModule {

    _extensionsManager.annotationToolsBar.highLightClicked = ^() {
        self->_annotType = FSAnnotHighlight;
        [self annotItemClicked];
    };
    _extensionsManager.annotationToolsBar.strikeOutClicked = ^() {
        self->_annotType = FSAnnotStrikeOut;
        [self annotItemClicked];
    };

    _extensionsManager.annotationToolsBar.underLineClicked = ^() {
        self->_annotType = FSAnnotUnderline;
        [self annotItemClicked];
    };

    _extensionsManager.annotationToolsBar.breakLineClicked = ^() {
        self->_annotType = FSAnnotSquiggly;
        [self annotItemClicked];
    };
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    id<IToolHandler> toolHandler = [_extensionsManager getToolHandlerByName:Tool_Markup];
    switch (_annotType) {
    case FSAnnotHighlight:
    case FSAnnotSquiggly:
    case FSAnnotStrikeOut:
    case FSAnnotUnderline:
        toolHandler.type = _annotType;
        [_extensionsManager setCurrentToolHandler:toolHandler];
        break;

    default:
        break;
    }

    [_extensionsManager.toolSetBar removeAllItems];

    UIImage *itemImg = ImageNamed(@"annot_done");
    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:itemImg];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    [_extensionsManager registerAnnotPropertyListener:self];
    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    self.propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:_annotType]];
    [_extensionsManager.toolSetBar addItem:self.propertyItem displayPosition:Position_CENTER];
    self.propertyItem.onTapClick = ^(TbBaseItem *item) {
        if (DEVICE_iPHONE) {
            CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_extensionsManager.pdfViewCtrl];
            [self->_extensionsManager showProperty:self->_annotType rect:rect inView:self->_extensionsManager.pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:self->_annotType rect:item.contentView.bounds inView:item.contentView];
        }
    };

    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        itemImg = ImageNamed(@"annot_continue");
        continueItem = [TbBaseItem createItemWithImage:itemImg];
    } else {
        itemImg = ImageNamed(@"annot_single");
        continueItem = [TbBaseItem createItemWithImage:itemImg];
    }
    continueItem.tag = 3;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_extensionsManager.pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            UIImage *itemImg = ImageNamed(@"annot_continue");
            item.imageNormal = itemImg;
            item.imageSelected = itemImg;
        } else {
            UIImage *itemImg = ImageNamed(@"annot_single");
            item.imageNormal = itemImg;
            item.imageSelected = itemImg;
        }

        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };

    if (_annotType == FSAnnotHighlight) {
        [Utility showAnnotationType:FSLocalizedForKey(@"kHighlight") type:FSAnnotHighlight pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
    } else if (_annotType == FSAnnotSquiggly) {
        [Utility showAnnotationType:FSLocalizedForKey(@"kSquiggly") type:FSAnnotSquiggly pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
    } else if (_annotType == FSAnnotStrikeOut) {
        [Utility showAnnotationType:FSLocalizedForKey(@"kStrikeout") type:FSAnnotStrikeOut pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
    } else if (_annotType == FSAnnotUnderline) {
        [Utility showAnnotationType:FSLocalizedForKey(@"kUnderline") type:FSAnnotUnderline pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
    }
    
    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.propertyItem.contentView.superview);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];
    
    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.left.equalTo(self.propertyItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];
    
    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.right.equalTo(self.propertyItem.contentView.mas_left).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_pdfViewCtrl];
}

#pragma mark - IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == _annotType) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

@end
