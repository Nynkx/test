/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol IModule;
@interface SpeechModule : NSObject<IModule>

@property (nonatomic, assign) int readingIndex;
@property (nonatomic, strong, nullable) FSRectF *readingRect;
@property (nonatomic, assign) int readingDirection;

- (instancetype)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager;

- (void)speakText:(NSString *)text;
- (void)speakFromPageIndex:(int)pageIndex;
- (void)speakFromPageIndex:(int)pageIndex startCharPos:(int)startPos;
- (void)speakSelectPageIndex:(int)pageIndex startCharPos:(int)startPos endCharPos:(int)pos;
- (void)speakFromTextMarkup:(FSTextMarkup *)textMarkup;
@end

NS_ASSUME_NONNULL_END
