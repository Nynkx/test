/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "SpeechModule.h"
#import "AlertView.h"
#import <AVFoundation/AVFoundation.h>
#import "StringDrawUtil.h"
#import <MediaPlayer/MediaPlayer.h>

@interface RateItemCell : UITableViewCell
@property (nonatomic, assign) BOOL itemOnselected;
@property (nonatomic, strong) UIImageView *selectImageItem;
@end

@implementation RateItemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier isSelect:(BOOL)isSelect
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.itemOnselected = isSelect;
        self.textLabel.font = [UIFont systemFontOfSize:15];
        self.textLabel.textColor = BlackThemeTextColor;
        
        self.selectImageItem = [[UIImageView alloc] init];
        self.selectImageItem.bounds = CGRectMake(0, 0, 40, 40);
        self.selectImageItem.image = ImageNamed(@"common_blue_short_select");
        self.selectImageItem.layer.anchorPoint = CGPointMake(1.0, 0.5);
        [self.contentView addSubview:self.selectImageItem];
        self.selectImageItem.hidden = YES;
        self.selectImageItem.contentMode = UIViewContentModeScaleAspectFill;
        
        self.imageView.layer.anchorPoint = CGPointMake(0, 0.5);
        self.textLabel.layer.anchorPoint = CGPointMake(0, 0.5);
    }
    return self;
}

- (void)setItemOnselected:(BOOL)itemOnselected
{
    _itemOnselected = itemOnselected;
    self.selectImageItem.hidden = !itemOnselected;
    if (itemOnselected) {
        self.textLabel.textColor =  BlueThemeTextColor;
    }else{
        self.textLabel.textColor = BlackThemeTextColor;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.textLabel.center = CGPointMake( 14, self.frame.size.height * 0.5);
    self.selectImageItem.center = CGPointMake( self.contentView.frame.size.width, self.frame.size.height * 0.5);
}

@end

typedef enum : NSUInteger {
    RateItemType0,
    RateItemType1,
    RateItemType2,
    RateItemType3,
    RateItemType4,
    RateItemType5,
} RateItemType;

@interface RateItemCtr : UITableViewController
@property (nonatomic, copy) void (^ctrHidden)();
@property (nonatomic, copy) void (^onRateItemClick)(RateItemType type);
@property (nonatomic, assign) BOOL isPhoneMode;
@property (nonatomic, assign) BOOL isFistTime;
@property (nonatomic, assign) RateItemType curRateType;
@end

@implementation RateItemCtr

- (instancetype)init
{
    if (self = [super init]) {
        _isPhoneMode = NO;
        _isFistTime = YES;
        _curRateType = RateItemType2;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = DividingLineColor;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.rowHeight = 44;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (_isFistTime) {
        self.tableView.contentOffset = CGPointZero;
        _isFistTime = NO;
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ThemeViewBackgroundColor;
    
    UIView *divideLine = [[UIView alloc] init];
    [view addSubview:divideLine];
    divideLine.backgroundColor = DividingLineColor;
    [divideLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(divideLine.superview.mas_left);
        make.right.mas_equalTo(divideLine.superview.mas_right);
        make.bottom.mas_equalTo(divideLine.superview.mas_bottom);
        make.height.mas_equalTo([Utility realPX:1.0]);
    }];
    
    UILabel *title = [[UILabel alloc] init];
    title.textColor = BlackThemeTextColor;
    title.font = [UIFont systemFontOfSize:18];
    title.text = FSLocalizedForKey(@"kSpeechRateTitle");
    //title.bounds = CGRectMake(0, 0, title.intrinsicContentSize.width, title.intrinsicContentSize.height);
    if (_isPhoneMode) {
        view.bounds = CGRectMake(0, 0, tableView.frame.size.width, 44);
        
        [view addSubview:title];
        
        [title mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(title.superview.mas_centerX);
            make.centerY.mas_equalTo(title.superview.mas_centerY);
        }];
        return view;
    }else{
        view.bounds = CGRectMake(0, 0, tableView.frame.size.width, 44);
        //title.center = CGPointMake(view.center.x, 22);
        [view addSubview:title];
        [title mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(title.superview.mas_centerX);
            make.centerY.mas_equalTo(title.superview.mas_centerY);
        }];
        return view;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}


- (RateItemCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    RateItemCell *cell = [[RateItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier isSelect:NO];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *title = @"";
    RateItemType type = (RateItemType)indexPath.row;
    switch (type) {
        case RateItemType0:
            title = @"0.5X";
            break;
        case RateItemType1:
            title = @"0.75X";
            break;
        case RateItemType2:
            title = @"1X";
            break;
        case RateItemType3:
            title = @"1.25X";
            break;
        case RateItemType4:
            title = @"1.5X";
            break;
        case RateItemType5:
            title = @"2X";
            break;
        default:
            title = @"1X";
            break;
    }
    cell.textLabel.text = title;
    if (type == _curRateType) {
        cell.itemOnselected = YES;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RateItemType type = (RateItemType)indexPath.row;
    if (self.onRateItemClick) {
        _curRateType = type;
        self.onRateItemClick(type);
        [tableView reloadData];
    }
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if (self.ctrHidden) {
        self.ctrHidden();
    }
    self.transitioningDelegate = nil;
    [super dismissViewControllerAnimated:flag completion:completion];
}

@end

typedef enum
{
	READING_STATUS_NONE = 0,
	READING_STATUS_PAGE,
	READING_STATUS_SELECTED,
    READING_STATUS_TEXT
} READING_STATUS;

@interface SpeechModule () <AVSpeechSynthesizerDelegate, IDocEventListener, IStateChangeListener, IDrawEventListener, UIPopoverPresentationControllerDelegate>

#define MIN_READING_SPEED 0.05
#define MAX_READING_SPEED 1.0
#define DEFAULT_READING_SPEED 0.40
#define CHANGE_READING_SPEED 0.05

@property (strong, nonatomic) AVSpeechSynthesizer *speechSynthesizer;
@property (assign, nonatomic) float readingSpeed;
@property (copy, nonatomic) NSString *readingLanguage;
@property (copy, nonatomic) NSString *readingContent;
@property (copy, nonatomic) NSString *mainLanguage;
@property (copy, nonatomic) NSArray *arrayBounds;
@property (assign, nonatomic) int boundIndex;
@property (assign, nonatomic) READING_STATUS currentStatus;
@property (assign, nonatomic) int currentPageindex;
@property (assign, nonatomic) int currentReadingIndex;
@property (assign, nonatomic) int readingOffset;
@property (assign, nonatomic) int readStartCharPos;
@property (assign, nonatomic) int readEndCharPos;

@property (strong, nonatomic)  FSToolbar *toolbarBottom;

@property (strong, nonatomic)  UIButton *buttonPreviousPage;
@property (strong, nonatomic)  UIButton *buttonPlay;
@property (strong, nonatomic)  UIButton *buttonStop;
@property (strong, nonatomic)  UIButton *buttonNextPage;
@property (strong, nonatomic)  UIButton *buttonCycle;
@property (strong, nonatomic)  UIButton *buttonRate;

@property (strong, nonatomic) FSToolbar *topToolBar;

@property (nonatomic, assign) BOOL isInitedData;
@property (nonatomic, assign) BOOL isInitedBarViewData;

@property (nonatomic, strong) RateItemCtr *curRateItemCtr;
@property (nonatomic, assign) BOOL autoNextPage;
@property (nonatomic, assign) int oldState;
@property (nonatomic, assign) BOOL isSpeechState;

@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@end
@implementation SpeechModule

static NSString *speechSpeed = @"speechSpeed";
static NSString *speechAutoNextPage = @"speechAutoNextPage";

- (instancetype)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _extensionsManager = extensionsManager;
        _isSpeechState = NO;
        _oldState = [_extensionsManager getState];
        [self loadModule];
    }
    return self;
}

- (FSToolbar *)topToolBar{
    if (!_topToolBar) {
        _topToolBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
        _topToolBar.hidden = YES;
        [_pdfViewCtrl addSubview:_topToolBar];
        
        [_topToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self.pdfViewCtrl);
        }];
        
        UIView *viewTopSpeech = [UIView new];
        [_topToolBar addSubview:viewTopSpeech];
        [viewTopSpeech mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(viewTopSpeech.superview);
        }];
        
        UILabel  *labelSpeech = [[UILabel alloc] init];
        labelSpeech.text = FSLocalizedForKey(@"kSpeak");
        labelSpeech.font = [UIFont systemFontOfSize:18];
        labelSpeech.textColor = BarLikeBlackTextColor;
        [viewTopSpeech addSubview:labelSpeech];
        [labelSpeech mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(labelSpeech.superview);
        }];
        
        UIButton *buttonCancel = [[UIButton alloc] init];
        [buttonCancel setImage:ImageNamed(@"common_back_black") forState:UIControlStateNormal];
        [buttonCancel setImage:[Utility unableImage:ImageNamed(@"common_back_black")] forState:UIControlStateHighlighted];
        [buttonCancel addTarget:self action:@selector(buttonCancelClick) forControlEvents:UIControlEventTouchUpInside];

        [viewTopSpeech addSubview:buttonCancel];
        [buttonCancel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(buttonCancel.superview.mas_left).offset(4);
            make.centerY.mas_equalTo(labelSpeech);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(40);
        }];
    }
    return _topToolBar;
}


- (FSToolbar *)toolbarBottom{
    if (!_toolbarBottom) {
        _toolbarBottom = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];
        _toolbarBottom.hidden = YES;
        [_pdfViewCtrl addSubview:_toolbarBottom];
        
        [_toolbarBottom mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(self.pdfViewCtrl);
        }];
        
        
        UIView *toolbarBottomSpeech = [UIView new];
        [_toolbarBottom addSubview:toolbarBottomSpeech];
        [toolbarBottomSpeech mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.centerX.mas_equalTo(toolbarBottomSpeech.superview);
        }];
        
        self.buttonPreviousPage = [[UIButton alloc] init];
        [toolbarBottomSpeech addSubview:_buttonPreviousPage];
        [_buttonPreviousPage setImage:ImageNamed(@"speech_previous") forState:UIControlStateNormal];
        [_buttonPreviousPage setImage:[Utility unableImage:ImageNamed(@"speech_previous")] forState:UIControlStateHighlighted];
        [_buttonPreviousPage addTarget:self action:@selector(barButtonPreviousPageClicked:) forControlEvents:UIControlEventTouchUpInside];

        self.buttonNextPage = [[UIButton alloc] init];
        [toolbarBottomSpeech addSubview:_buttonNextPage];
        [_buttonNextPage setImage:ImageNamed(@"speech_next") forState:UIControlStateNormal];
        [_buttonNextPage setImage:[Utility unableImage:ImageNamed(@"speech_next")] forState:UIControlStateHighlighted];
        [_buttonNextPage addTarget:self action:@selector(barButtonNextPageClicked:) forControlEvents:UIControlEventTouchUpInside];

        self.buttonPlay = [[UIButton alloc] init];
        [toolbarBottomSpeech addSubview:_buttonPlay];
        [_buttonPlay setImage:ImageNamed(@"speech_pause") forState:UIControlStateNormal];
        [_buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_pause")] forState:UIControlStateHighlighted];
        [_buttonPlay addTarget:self action:@selector(barButtonPlayClicked:) forControlEvents:UIControlEventTouchUpInside];

        self.buttonStop = [[UIButton alloc] init];
        [toolbarBottomSpeech addSubview:_buttonStop];
        [_buttonStop setImage:ImageNamed(@"speech_stop") forState:UIControlStateNormal];
        [_buttonStop setImage:[Utility unableImage:ImageNamed(@"speech_stop")] forState:UIControlStateHighlighted];
        [_buttonStop addTarget:self action:@selector(barButtonStopClicked:) forControlEvents:UIControlEventTouchUpInside];

        self.buttonCycle = [[UIButton alloc] init];
        [toolbarBottomSpeech addSubview:_buttonCycle];
        [self.buttonCycle setImage:ImageNamed(@"speech_circulation") forState:UIControlStateNormal];
        [self.buttonCycle setBackgroundImage:[Utility unableImage:[UIImage fs_imageWithColor:[UIColor grayColor] size:CGSizeMake(18, 18)]]  forState:UIControlStateSelected];
        [_buttonCycle addTarget:self action:@selector(buttonCycleClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        self.buttonRate = [[UIButton alloc] init];
        [toolbarBottomSpeech addSubview:_buttonRate];
        self.buttonRate.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Light" size:17];
        NSString *speedStr = [self translaterRateItemTypeToStr:[self translaterFloatSpeedToRateType:_readingSpeed]];
        [self.buttonRate setTitle:speedStr forState:UIControlStateNormal];
        [self.buttonRate setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];

        [_buttonRate addTarget:self action:@selector(buttonRateClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [_buttonPreviousPage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_buttonPreviousPage.superview);
            make.centerY.mas_equalTo(toolbarBottomSpeech);
            make.width.height.mas_equalTo(40);
        }];
        
        [_buttonPlay mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_buttonPreviousPage.mas_right).offset(ITEM_MARGIN_NORMAL);
            make.centerY.mas_equalTo(toolbarBottomSpeech);
            make.width.height.mas_equalTo(40);
        }];
        
        [_buttonStop mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_buttonPlay.mas_right).offset(ITEM_MARGIN_NORMAL);
            make.centerY.mas_equalTo(toolbarBottomSpeech);
            make.width.height.mas_equalTo(40);
        }];
        
        [_buttonNextPage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_buttonStop.mas_right).offset(ITEM_MARGIN_NORMAL);
            make.centerY.mas_equalTo(toolbarBottomSpeech);
            make.width.height.mas_equalTo(40);
        }];
        [_buttonCycle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_buttonNextPage.mas_right).offset(ITEM_MARGIN_NORMAL);
            make.centerY.mas_equalTo(toolbarBottomSpeech);
            make.width.height.mas_equalTo(40);
        }];
        
        [_buttonRate mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_buttonCycle.mas_right).offset(ITEM_MARGIN_NORMAL);
            make.right.mas_equalTo(_buttonRate.superview);
            make.centerY.mas_equalTo(toolbarBottomSpeech);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(45);
        }];
    }
    return _toolbarBottom;
}


- (void)speak
{  
    [self showSpeechBar:YES animation:YES];
    [self refreshInterface];
    
    if (UIAccessibilityIsVoiceOverRunning()) {
        [self barButtonPlayClicked:self.buttonPlay];
    }
}

- (void)showSpeechBar:(BOOL)isShow animation:(BOOL)animation{
	self.toolbarBottom.hidden = !isShow;
	self.topToolBar.hidden = !isShow;
    
	if (animation)
	{
		[Utility addAnimation:self.topToolBar.layer type:self.topToolBar.hidden?kCATransitionReveal:kCATransitionMoveIn subType:self.topToolBar.hidden?kCATransitionFromTop: kCATransitionFromBottom timeFunction:kCAMediaTimingFunctionEaseInEaseOut duration:0.3];
		[Utility addAnimation:self.toolbarBottom.layer type:self.toolbarBottom.hidden?kCATransitionReveal:kCATransitionMoveIn subType:self.toolbarBottom.hidden?kCATransitionFromBottom: kCATransitionFromTop timeFunction:kCAMediaTimingFunctionEaseInEaseOut duration:0.3];
	}
	
}

	
- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
	[[UIApplication sharedApplication] endReceivingRemoteControlEvents];
}

#pragma mark buttonClick methods
- (void)buttonCancelClick {
    [_extensionsManager changeState:_oldState];
}

- (void)barButtonPreviousPageClicked:(id)sender {
	if (_currentStatus == READING_STATUS_PAGE && (_currentPageindex-1) >= 0)
    {
        [self stopOnly];
        _currentPageindex--;
        [_pdfViewCtrl gotoPage:_currentPageindex animated:YES];
        self.currentStatus = READING_STATUS_PAGE;
        if (![self speakContentWithIndex:_currentPageindex]) {
            [self barButtonStopClicked:self.buttonStop];
            return;
        }
    }
}

- (void)barButtonPlayClicked:(id)sender {
	if (_currentStatus == READING_STATUS_NONE)
	{
//		Reading current page
		[self refreshInterface];
        self.currentStatus = READING_STATUS_PAGE;
		[self speakContentWithIndex:_currentPageindex];
        [_pdfViewCtrl gotoPage:_currentPageindex animated:YES];
	}
	else if (_currentStatus == READING_STATUS_PAGE || _currentStatus == READING_STATUS_SELECTED)
	{
		if (self.speechSynthesizer.paused)
		{
			[self.speechSynthesizer continueSpeaking];
		}
		else
		{
			[self.speechSynthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
		}
	}
}

- (void)barButtonStopClicked:(id)sender {
	[self stop];
}

- (void)barButtonNextPageClicked:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.currentStatus == READING_STATUS_PAGE &&  (self.currentPageindex+1) < [[self.pdfViewCtrl currentDoc] getPageCount]){
            
            [self stopOnly];
            self.currentPageindex++;
            [self.pdfViewCtrl gotoPage:self.currentPageindex animated:YES];
            self.currentStatus = READING_STATUS_PAGE;
            if (![self speakContentWithIndex:self.currentPageindex]) {
                [self barButtonStopClicked:self.buttonStop];
                return;
            }
        }
    });
}

- (void)buttonCycleClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    _autoNextPage = !sender.selected;
}

- (void)buttonRateClicked:(id)sender {
    [self presentRateItemCtr];
}


- (void)presentRateItemCtr
{
    if (DEVICE_iPHONE ) {
        CGFloat height = 308;
        RateItemCtr *ctr = [[RateItemCtr alloc] init];
        ctr.curRateType = [self translaterFloatSpeedToRateType:_readingSpeed];
        ctr.isPhoneMode = YES;
        self.curRateItemCtr = ctr;
        ctr.onRateItemClick = ^(RateItemType type) {
            switch (type) {
                case RateItemType0:
                {
                    self.readingSpeed = 0.2;
                }
                    break;
                case RateItemType1:
                {
                    self.readingSpeed = 0.3;
                }
                    break;
                case RateItemType2:
                {
                    self.readingSpeed = 0.4;
                }
                    break;
                case RateItemType3:
                {
                    self.readingSpeed = 0.5;
                }
                    break;
                case RateItemType4:
                {
                    self.readingSpeed = 0.6;
                }
                    break;
                case RateItemType5:
                {
                    self.readingSpeed = 0.8;
                }
                    break;
                default:
                    break;
            }
            NSString *speedStr = [self translaterRateItemTypeToStr:type];
            [self.buttonRate setTitle:speedStr forState:UIControlStateNormal];
            if (self.currentStatus != READING_STATUS_NONE) {
                [self speakAfterSpeedChange];
            }
        };
        ctr.ctrHidden = ^{
            self.curRateItemCtr = nil;
        };
        [ctr fs_makeTransitionAnimator:^(FSPresentedVCAnimator * _Nonnull animator) {
            animator.size = CGSizeMake(self.pdfViewCtrl.fs_width, height);
            animator.presentationStyle = FSModalPresentationBottom;
            animator.maxVerticalHeigh = height;
        }];
        [self.pdfViewCtrl.fs_viewController presentViewController:ctr animated:YES completion:nil];
    }else{
        RateItemCtr *ctr = [[RateItemCtr alloc] init];
        ctr.curRateType = [self translaterFloatSpeedToRateType:_readingSpeed];
        ctr.isPhoneMode = NO;
        self.curRateItemCtr = ctr;
        ctr.onRateItemClick = ^(RateItemType type) {
            switch (type) {
                case RateItemType0:
                {
                    self.readingSpeed = 0.2;
                }
                    break;
                case RateItemType1:
                {
                    self.readingSpeed = 0.3;
                }
                    break;
                case RateItemType2:
                {
                    self.readingSpeed = 0.4;
                }
                    break;
                case RateItemType3:
                {
                    self.readingSpeed = 0.5;
                }
                    break;
                case RateItemType4:
                {
                    self.readingSpeed = 0.6;
                }
                    break;
                case RateItemType5:
                {
                    self.readingSpeed = 0.8;
                }
                    break;
                default:
                    break;
            }
            NSString *speedStr = [self translaterRateItemTypeToStr:type];
            [self.buttonRate setTitle:speedStr forState:UIControlStateNormal];
            if (self.currentStatus != READING_STATUS_NONE) {
                [self speakAfterSpeedChange];
            }
        };
        ctr.ctrHidden = ^{
            self.curRateItemCtr = nil;
        };
        ctr.modalPresentationStyle = UIModalPresentationPopover;
        ctr.preferredContentSize = CGSizeMake(320, 308);
        UIPopoverPresentationController * popover = [ctr popoverPresentationController];
        popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
        popover.backgroundColor = ThemeViewBackgroundColor;
        popover.delegate = self;
        popover.sourceView = self.buttonRate;
        popover.sourceRect = self.buttonRate.bounds;
        [self.pdfViewCtrl.fs_viewController presentViewController:ctr animated:YES completion:nil];
    }
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    if (popoverPresentationController.sourceView == self.buttonRate) {
        self.curRateItemCtr = nil;
    }
}

- (RateItemType)translaterFloatSpeedToRateType:(float)readSpeed
{

    if (readSpeed >= 0.25 && readSpeed < 0.35) {
        return RateItemType1;
    }
    
    if (readSpeed >= 0.35 && readSpeed < 0.45) {
        return RateItemType2;
    }
    
    if (readSpeed >= 0.45 && readSpeed < 0.55) {
        return RateItemType3;
    }
    
    if (readSpeed >= 0.55 && readSpeed < 0.7) {
        return RateItemType4;
    }
    
    if (readSpeed >= 0.7) {
        return RateItemType5;
    }
    return RateItemType0;
}

- (NSString *)translaterRateItemTypeToStr:(RateItemType)type
{
    NSString *speedStr = @"1X";
    switch (type) {
        case RateItemType0:
        {
            speedStr = @"0.5X";
        }
            break;
        case RateItemType1:
        {
            speedStr = @"0.75X";
        }
            break;
        case RateItemType2:
        {
            speedStr = @"1X";
        }
            break;
        case RateItemType3:
        {
            speedStr = @"1.25X";
        }
            break;
        case RateItemType4:
        {
            speedStr = @"1.5X";
        }
            break;
        case RateItemType5:
        {
            speedStr = @"2X";
        }
            break;
        default:
        {
            speedStr = @"1X";
        }
            break;
    }
    return speedStr;
}

#pragma mark - AVSpeechSynthesizerDelegate

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didCancelSpeechUtterance:(AVSpeechUtterance *)utterance
{
	[self stop];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.boundIndex < (self.arrayBounds.count -1) ) {
            self.boundIndex++;
            NSString *text = self.arrayBounds[self.boundIndex][2];
            [self updateReadingLanguage:text];
            if (self.currentStatus != READING_STATUS_SELECTED) [self resetOverlayView];
            self.readingContent = text;
            [self speak:text];
            return;
        }else if (self.currentStatus == READING_STATUS_PAGE)
        {
            if (!self.autoNextPage) {

                [self stopOnly];
                self.currentStatus = READING_STATUS_NONE;
                
                [self refreshInterface];
                [self speakContentWithIndex:self.currentPageindex];
                self.currentStatus = READING_STATUS_PAGE;
                return;
            }else if (self.autoNextPage && (self.currentPageindex+1) < [[self.pdfViewCtrl currentDoc] getPageCount])
            {
                [self resetOverlayView];
                
                int currentPageindex = [self->_pdfViewCtrl getCurrentPage];
                BOOL turnPage = NO;
                if (currentPageindex == self.currentPageindex) {
                    turnPage = YES;
                }
                self.currentPageindex++;
                int pageCount = [self.pdfViewCtrl.currentDoc getPageCount];
                
                while (self.currentPageindex<pageCount) {
                    FSTextPage *textPage = [Utility getTextSelect:self.pdfViewCtrl.currentDoc pageIndex:self.currentPageindex];
                    if (!textPage.getCharCount) {
                        self.currentPageindex++;
                    }else{
                        break;
                    }
                }
                if (self.currentPageindex>=pageCount) {
                    [self stop];
                    return;
                }
                [self speakContentWithIndex:self.currentPageindex];
                [self refreshInterface];
                if (turnPage) {
                    [self.pdfViewCtrl gotoPage:self.currentPageindex animated:YES];
                }
                return;
            }
            

        }else  if (self.currentStatus == READING_STATUS_SELECTED){
            if (self.buttonCycle.selected) {
                [self speakSelectPageIndex:self.currentPageindex startCharPos:self.readStartCharPos endCharPos:self.readEndCharPos];
                return;
            }
        }else  if (self.currentStatus == READING_STATUS_TEXT){
            if (self.buttonCycle.selected) {
                [self speakText:self.readingContent];
                return;
            }
        }
        [self stop];
    });
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance
{
    if (self.currentStatus != READING_STATUS_SELECTED || (self.currentStatus == READING_STATUS_SELECTED && _boundIndex == 0)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self refreshOverlayView];
            [self refreshInterface];
        });
    }
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didPauseSpeechUtterance:(AVSpeechUtterance *)utterance
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self resetOverlayView];
        [self refreshInterface];
    });
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didContinueSpeechUtterance:(AVSpeechUtterance *)utterance
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self refreshOverlayView];
        [self refreshInterface];
    });
}

#pragma mark - Public method

- (void)speakFromTextMarkup:(FSTextMarkup *)textMarkup {
    FSTextPage *textPage = [Utility getTextSelect:self.pdfViewCtrl.currentDoc pageIndex:textMarkup.pageIndex];
    NSMutableString *readingText = [NSMutableString string];
    for (int i = 0; i < textMarkup.quadPoints.getSize; i++) {
        FSQuadPoints *quadPoints = [textMarkup.quadPoints getAt:i];
        
        NSString *text = nil;
        NSMutableArray *tmp = @[ [[FSRectF alloc] initWithLeft1:quadPoints.third.x bottom1:quadPoints.third.y right1:quadPoints.second.x top1:quadPoints.second.y], [[FSRectF alloc] initWithLeft1:quadPoints.first.x bottom1:quadPoints.first.y right1:quadPoints.fourth.x top1:quadPoints.fourth.y], [[FSRectF alloc] initWithLeft1:quadPoints.second.x bottom1:quadPoints.second.y right1:quadPoints.third.x top1:quadPoints.third.y] ].mutableCopy;
        while (tmp.count) {
            FSRectF *dibRect = tmp.firstObject;
            @try {
                text = [textPage getTextInRect:dibRect];
                break;
            } @catch (NSException *exception) {
                [tmp removeObject:dibRect];
            }
        }
        [readingText appendString:[NSString stringWithFormat:@"%@ ",text]];
    }
    [self speakText:readingText];
}


- (void)speakText:(NSString *)text{
    if (!_isInitedBarViewData) {
        _isInitedBarViewData = YES;
    }
    
    [self stopOnly];
    [self refreshInterface];
    self.currentStatus = READING_STATUS_TEXT;
    if ([_extensionsManager getState] != STATE_SPEECH) {
        [_extensionsManager changeState:STATE_SPEECH];
        [self showSpeechBar:YES animation:YES];
    }
    _boundIndex = -1;
    self.mainLanguage = [StringDrawUtil languageForString:text];
    [self speakContentWithText:text];
}

- (void)speakFromPageIndex:(int)pageIndex{
    if (self.currentStatus == READING_STATUS_TEXT || self.currentStatus == READING_STATUS_SELECTED) return;
    [self speakFromPageIndex:pageIndex startCharPos:0];
}

- (void)speakFromPageIndex:(int)pageIndex startCharPos:(int)startPos{
    
    if (!_isInitedBarViewData) {
        _isInitedBarViewData = YES;
    }
    [self stopOnly];
    [self refreshInterface];
    self.currentStatus = READING_STATUS_PAGE;
    if ([_extensionsManager getState] != STATE_SPEECH) {
        [_extensionsManager changeState:STATE_SPEECH];
        [self showSpeechBar:YES animation:YES];
    }
    _currentPageindex = pageIndex;
    [self speakContentWithIndex:pageIndex startCharPos:startPos endCharPos:0];
}

- (void)speakSelectPageIndex:(int)pageIndex startCharPos:(int)startPos endCharPos:(int)pos{
    self.readStartCharPos = startPos;
    self.readEndCharPos = pos;
    if (!_isInitedBarViewData) {
        _isInitedBarViewData = YES;
    }
    
    [self stopOnly];
    [self refreshInterface];
    self.currentStatus = READING_STATUS_SELECTED;
    if ([_extensionsManager getState] != STATE_SPEECH) {
        [_extensionsManager changeState:STATE_SPEECH];
        [self showSpeechBar:YES animation:YES];
    }
    _currentPageindex = pageIndex;
    [self speakContentWithIndex:pageIndex startCharPos:startPos endCharPos:pos];
}


- (void)speakContentWithIndex:(int)pageIndex startCharPos:(int)startPos endCharPos:(int)pos;
{
    if (self.pdfViewCtrl.isDynamicXFA) return;

    NSMutableString *readingText = [NSMutableString string];
    
    FSTextPage *textPage = [Utility getTextSelect:self.pdfViewCtrl.currentDoc pageIndex:pageIndex];
    
    int count = (pos == 0 ? [textPage getCharCount] - startPos  : (pos - startPos) +1);
    
    NSMutableArray *arrayBounds =  [Utility getTextRects:textPage start:startPos count:count].mutableCopy;
    _boundIndex = -1;
    [arrayBounds enumerateObjectsUsingBlock:^(NSArray *arr, NSUInteger idx, BOOL *stop) {
        NSMutableArray *arrayOne = arr.mutableCopy;
        [arrayOne addObject:[NSNumber numberWithInt:(int)readingText.length]];
        NSString *text = arrayOne[2];
        [readingText appendString:[NSString stringWithFormat:@"%@ ",text]];
        [arrayBounds replaceObjectAtIndex:idx withObject:arrayOne.copy];
    }];
    self.arrayBounds = arrayBounds.copy;
    self.mainLanguage = [StringDrawUtil languageForString:readingText];
    if (arrayBounds.count > 0) {
        _boundIndex = 0;
        NSString *text = self.arrayBounds[_boundIndex][2];
        [self speakContentWithText:text];
    }else{
        [self speakContentWithText:readingText];
    }
}

- (void)stop
{
	[self stopOnly];
	self.currentStatus = READING_STATUS_NONE;
}

- (void)refreshInterface
{
    int titleIndex = _currentPageindex;
    NSString *content = @"";
    
    if (_currentStatus == READING_STATUS_NONE)
    {
        titleIndex = [_pdfViewCtrl getCurrentPage];
        _currentPageindex = titleIndex;
    
        self.buttonPreviousPage.enabled = NO;
        self.buttonNextPage.enabled = NO;
        [self.buttonPlay setImage:ImageNamed(@"speech_start") forState:UIControlStateNormal];
        [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_start")] forState:UIControlStateHighlighted];
        self.buttonStop.enabled = NO;
    }
    else if (_currentStatus == READING_STATUS_PAGE)
    {
        if (self.arrayBounds.count > 0 && _boundIndex >= 0 &&  _boundIndex < self.arrayBounds.count)
        {
            content = self.arrayBounds[_boundIndex][2];
        }
        
        if ((_currentPageindex-1) < 0)
        {
            self.buttonPreviousPage.enabled = NO;
        }
        else
        {
            self.buttonPreviousPage.enabled = YES;
        }
        if ((_currentPageindex+1) >= [[_pdfViewCtrl currentDoc] getPageCount])
        {
            self.buttonNextPage.enabled = NO;
        }
        else
        {
            self.buttonNextPage.enabled = YES;
        }
        if (self.speechSynthesizer.paused)
        {
            [self.buttonPlay setImage:ImageNamed(@"speech_start") forState:UIControlStateNormal];
            [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_start")] forState:UIControlStateHighlighted];
        }
        else
        {
            [self.buttonPlay setImage:ImageNamed(@"speech_pause") forState:UIControlStateNormal];
            [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_pause")] forState:UIControlStateHighlighted];
        }
        self.buttonStop.enabled = YES;
        
    }
    else if (_currentStatus == READING_STATUS_SELECTED)
    {
        content = self.readingContent;
        
        self.buttonPreviousPage.enabled = NO;
        self.buttonNextPage.enabled = NO;
        if (self.speechSynthesizer.paused)
        {
            [self.buttonPlay setImage:ImageNamed(@"speech_start") forState:UIControlStateNormal];
            [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_start")] forState:UIControlStateHighlighted];
        }
        else
        {
            [self.buttonPlay setImage:ImageNamed(@"speech_pause") forState:UIControlStateNormal];
            [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_pause")] forState:UIControlStateHighlighted];
            self.buttonPlay.accessibilityLabel = NSLocalizedString(@"kPlayBtnPause", nil);
        }
        self.buttonStop.enabled = YES;
        
    }
    else if (_currentStatus == READING_STATUS_TEXT)
    {
        content = self.readingContent;
        
        self.buttonPreviousPage.enabled = NO;
        self.buttonNextPage.enabled = NO;
        if (self.speechSynthesizer.paused)
        {
            [self.buttonPlay setImage:ImageNamed(@"speech_start") forState:UIControlStateNormal];
            [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_start")] forState:UIControlStateHighlighted];
        }
        else
        {
            [self.buttonPlay setImage:ImageNamed(@"speech_pause") forState:UIControlStateNormal];
            [self.buttonPlay setImage:[Utility unableImage:ImageNamed(@"speech_pause")] forState:UIControlStateHighlighted];
            self.buttonPlay.accessibilityLabel = NSLocalizedString(@"kPlayBtnPause", nil);
        }
        self.buttonStop.enabled = YES;
        
    }
}

#pragma IGestureEventListener

- (BOOL)onTap:(UITapGestureRecognizer *)recognizer {
    if ([_extensionsManager getState] == STATE_SPEECH) {
        [self showSpeechBar:self.topToolBar.hidden animation:YES];
        [_extensionsManager setFullScreen:!self.topToolBar.hidden];
        return YES;
    }
    return NO;
}

- (BOOL)onLongPress:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

#pragma IStateChangeListener
-(void)onStateChanged:(int)state
{
    if (state != STATE_SPEECH && _isSpeechState) {
        if (state != STATE_PAGENAVIGATE) _oldState = state;
        _isSpeechState = NO;
        if (_currentStatus != READING_STATUS_NONE)
        {
            if (!self.speechSynthesizer.paused)
            {
                [self barButtonPlayClicked:self.buttonPlay];
            }
            _currentStatus = READING_STATUS_NONE;
        }
        [self showSpeechBar:NO animation:YES];
        [self resetOverlayView];
        [self stop];
    }
    if (state == STATE_SPEECH) {
        _isSpeechState = YES;
        [_pdfViewCtrl gotoPage:_currentPageindex animated:YES];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *setCategoryError = nil;
        [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
        
        if (!_isInitedBarViewData) {
            _isInitedBarViewData = YES;
        }
        [self speak];
    }
}

- (void)appEnterBackground
{
    if (_currentStatus == READING_STATUS_PAGE || _currentStatus == READING_STATUS_SELECTED)
    {
        if (self.speechSynthesizer.isSpeaking) {
            [self.speechSynthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        }
    }
}

- (void)appEnterForeground
{
    [self.speechSynthesizer continueSpeaking];
}


- (void)resetOverlayView
{
	_readingIndex = -1;
    dispatch_main_async_safe(^{
        [self.pdfViewCtrl refresh:self.currentPageindex];
    });
    
}

- (void)refreshOverlayView
{
    if (self.arrayBounds.count > 0 && (self.currentStatus == READING_STATUS_PAGE) &&  _boundIndex < self.arrayBounds.count)
    {
        _readingRect = [Utility CGRect2FSRectF:[self.arrayBounds[_boundIndex][0] CGRectValue]];
        _readingDirection = [self.arrayBounds[_boundIndex][1] intValue];
    }
    _readingIndex = _currentPageindex;
    [_pdfViewCtrl refresh:_currentPageindex];
}

- (void)stopOnly
{
	self.speechSynthesizer.delegate = nil;
	[self.speechSynthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
	self.speechSynthesizer = nil;
	self.readingLanguage = nil;
	self.readingContent = nil;
	self.arrayBounds = nil;
    self.readingRect = nil;
	_readingOffset = 0;
	[self resetOverlayView];
}

- (void)setCurrentStatus:(READING_STATUS)currentStatus
{
	_currentStatus = currentStatus;
	[self refreshInterface];
}

- (BOOL)speakContentWithIndex:(int)pageIndex
{
	NSMutableString *readingText = [NSMutableString string];
    FSTextPage *textPage = [Utility getTextSelect:self.pdfViewCtrl.currentDoc pageIndex:pageIndex];
    
    NSMutableArray *arrayBounds =  [Utility getTextRects:textPage start:0 count:[textPage getCharCount]].mutableCopy;
	_boundIndex = -1;
	[arrayBounds enumerateObjectsUsingBlock:^(NSArray *arr, NSUInteger idx, BOOL *stop) {
		NSMutableArray *arrayOne = arr.mutableCopy;
		[arrayOne addObject:[NSNumber numberWithInt:(int)readingText.length]];
		NSString *text = arrayOne[2];
		[readingText appendString:[NSString stringWithFormat:@"%@ ",text]];
        [arrayBounds replaceObjectAtIndex:idx withObject:arrayOne.copy];
	}];
    self.arrayBounds = arrayBounds.copy;
    self.mainLanguage = [StringDrawUtil languageForString:readingText];
    if (arrayBounds.count > 0) {
        _boundIndex = 0;
        NSString *text = self.arrayBounds[_boundIndex][2];
        [self speakContentWithText:text];
    }else{
        [self speakContentWithText:readingText];
    }
    return [textPage getCharCount];
}

- (void)speakContentWithText:(NSString*)content
{
	_readingOffset = 0;
	self.readingContent = content;
	[self updateReadingLanguage:content];
	[self speak:self.readingContent];
}

- (void)speakAfterSpeedChange
{
	_readingOffset = _currentReadingIndex;
	if (self.readingContent && self.readingContent.length > 0 && _currentReadingIndex >=0 && _currentReadingIndex < self.readingContent.length)
	{
		NSString *text = [self.readingContent substringFromIndex:_currentReadingIndex];
		[self speak:text];
	}
	[self refreshInterface];
}

- (void)updateReadingLanguage:(NSString*)text
{
    NSString *mainLanguage = self.mainLanguage;
    if (_MAC_CATALYST_) {
        if ([mainLanguage hasPrefix:@"zh"]) {
            mainLanguage = @"en";
        }
        if (text.length && text.intValue) {
            self.readingLanguage =  @"en-US";
            return;
        }
    }
	NSString *lang = [StringDrawUtil languageForString:text];
    if (!lang.length)
    {
        lang = mainLanguage;
    }
	if (!lang || lang.length == 0)
	{
		lang = @"zh";
	}
	else
	{
		NSRange range = [lang rangeOfString:@"-"];
		if (range.length > 0)
		{
			lang = [lang substringToIndex:range.location];
		}
	}
	
	NSString *countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
	NSString *newLang = [NSString stringWithFormat:@"%@-%@", lang, countryCode];
	
	NSArray *allVoices = [AVSpeechSynthesisVoice speechVoices];
	__block BOOL find = NO;
	__block NSString *bestLang = nil;
	[allVoices enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		NSString *voiceLang = ((AVSpeechSynthesisVoice*)obj).language;
		if (!bestLang)
		{
			NSArray *array = [voiceLang componentsSeparatedByString:@"-"];
			if (array.count > 0)
			{
				if ([lang isEqualToString:array[0]])
				{
					bestLang = voiceLang;
				}
			}
		}
		
		if ([[newLang lowercaseString] isEqualToString:[voiceLang lowercaseString]])
		{
			find = YES;
			*stop = YES;
		}
	}];
	
	if (!find)
	{
		if ([[lang lowercaseString] isEqualToString:@"zh"])
		{
			newLang = @"zh-CN";
		}
		else if ([[lang lowercaseString] isEqualToString:@"en"])
		{
			newLang = @"en-US";
		}
		else
		{
			newLang = bestLang;
		}
	}
	
	if (!newLang)
	{
		newLang = [AVSpeechSynthesisVoice currentLanguageCode];
	}
	
	self.readingLanguage = newLang;
}

- (void)speak:(NSString*)text
{
	[self.speechSynthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
	self.speechSynthesizer.delegate = nil;
	self.speechSynthesizer = nil;
	
	AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:text];
	[utterance setRate:_readingSpeed];
	[utterance setVoice:[AVSpeechSynthesisVoice voiceWithLanguage:self.readingLanguage]];
	self.speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
	self.speechSynthesizer.delegate = self;
	[self.speechSynthesizer speakUtterance:utterance];
}

#pragma IDocEventListener methods

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error{
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
}

- (void)onDocWillClose:(FSPDFDoc *)document{
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    if (_isSpeechState) {
         [_extensionsManager changeState:_oldState];
    }
}


#pragma IDvDrawEventListener

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context{
    if ([_extensionsManager getState] == STATE_SPEECH && self.readingIndex == pageIndex) {
        if (self.currentStatus == READING_STATUS_PAGE) {
            if (![self.readingRect isEmpty] && self.readingRect) {
                int readIndex = self.readingIndex;

                FSRectF *readingRect = self.readingRect;
                
                CGRect pageRect = [_pdfViewCtrl convertPdfRectToPageViewRect:readingRect pageIndex:readIndex];
                CGRect parentRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pageRect pageIndex:readIndex];

                CGRect selfRect = [[_pdfViewCtrl getPageView:readIndex] convertRect:parentRect fromView:[_pdfViewCtrl getDisplayView]];

                CGContextSaveGState(context);


                CGContextSetRGBFillColor(context, 0, 0, 1, 0.3);
                CGContextFillRect(context, selfRect);
                CGContextRestoreGState(context);
            }
        }else if (self.currentStatus == READING_STATUS_SELECTED) {
            [self.arrayBounds enumerateObjectsUsingBlock:^(NSArray  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                int readIndex = self.readingIndex;

                FSRectF *readingRect = [Utility CGRect2FSRectF:[obj[0] CGRectValue]];;
                
                CGRect pageRect = [_pdfViewCtrl convertPdfRectToPageViewRect:readingRect pageIndex:readIndex];
                CGRect parentRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pageRect pageIndex:readIndex];

                CGRect selfRect = [[_pdfViewCtrl getPageView:readIndex] convertRect:parentRect fromView:[_pdfViewCtrl getDisplayView]];

                CGContextSaveGState(context);


                CGContextSetRGBFillColor(context, 0, 0, 1, 0.3);
                CGContextFillRect(context, selfRect);
                CGContextRestoreGState(context);
            }];
        }
    }
}


#pragma IModule methods
-(NSString*)getName
{
	return @"Speech";
}
-(void)loadModule
{
    _isInitedData = YES;
    _isInitedBarViewData = NO;
    _autoNextPage = YES;
    _readingSpeed = DEFAULT_READING_SPEED;
    
    [_extensionsManager registerStateChangeListener:self];
	[_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerDrawEventListener:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

@end
