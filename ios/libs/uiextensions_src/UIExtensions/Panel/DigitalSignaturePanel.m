/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "DigitalSignaturePanel.h"
#import "AnnotationListViewController.h"
#import "AnnotationStruct.h"
#import "PanelController+private.h"
#import "PanelHost.h"
#import "DigitalSignatureAnnotHandler.h"
#import "DynamicXFAHandler.h"

#define ENLARGE_EDGE 3

@interface DigitalSignaturePanel () {
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak UIExtensionsManager *_extensionsManager;
}

@property (nonatomic, strong) UIView *toolbar;
@property (nonatomic, strong) UIView *contentView;
//@property (nonatomic, strong) Popover *popover;
@property (nonatomic, strong) NSMutableDictionary *digitalsigDict;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *moreMenuArr;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (nonatomic, strong) UIExtensionsManager *signDocExtensionsMgr;

@end

@implementation DigitalSignaturePanel

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager panelController:(FSPanelController *)panelController {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _panelController = panelController;
        self.contentView = [[UIView alloc] init];
        self.contentView.backgroundColor = ThemeViewBackgroundColor;
        
        [self layoutTopbar];
        
        self.digitalsigDict = @{}.mutableCopy;
        self.moreMenuArr = @[].mutableCopy;
        self.tapGesture = nil;
        
        _extensionsManager.isViewSignedDocument = NO;
        [self addtableView];
    }
    return self;
}

- (void)load {
    [_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_panelController.panel insertSpec:self atIndex:4];
    [_extensionsManager registerAnnotEventListener:self];
    [_panelController registerPanelChangedListener:self];
    //    _panelController.panel.currentSpec = self;
}

- (void)unload {
    [_pdfViewCtrl unregisterDocEventListener:self];
    [_pdfViewCtrl unregisterPageEventListener:self];
    [_panelController.panel removeSpec:self];
    [_extensionsManager unregisterAnnotEventListener:self];
    [_panelController unregisterPanelChangedListener:self];
}

-(void)layoutTopbar{
    self.toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _pdfViewCtrl.bounds.size.width, 64)];
    self.toolbar.backgroundColor = PanelTopBarColor;
    
    CGSize titleSize = [Utility getTextSize:FSLocalizedForKey(@"kDigitalSignatureTitle") fontSize:17.0f maxSize:CGSizeMake(self.toolbar.frame.size.width - 60, 100)];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, titleSize.width, 25)];
    title.backgroundColor = [UIColor clearColor];
    title.textAlignment = NSTextAlignmentCenter;
    title.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    title.text = FSLocalizedForKey(@"kDigitalSignatureTitle");
    title.textColor = BlackThemeTextColor;
    title.center = CGPointMake(self.toolbar.bounds.size.width / 2, title.center.y);
    [self.toolbar addSubview:title];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    UITapGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelAction)];
    backgroundView.userInteractionEnabled = YES;
    [backgroundView addGestureRecognizer:tapG];
    [self.toolbar addSubview:backgroundView];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 32, 12, 12)];
    [cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    UIImage *bcakgrdImg = [UIImage imageNamed:@"panel_cancel.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    [cancelButton setBackgroundImage:bcakgrdImg forState:UIControlStateNormal];
    
    if (DEVICE_iPHONE) {
        [backgroundView addSubview:cancelButton];
    }
}

-(void)addtableView {
    CGRect frame = CGRectMake(0, 10, _pdfViewCtrl.bounds.size.width, _pdfViewCtrl.bounds.size.height - 107);
    self.tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    self.tableView.cellLayoutMarginsFollowReadableWidth = false;
    
    [self.contentView addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(self.contentView);
    }];
    
    [self refreshInterface];
}

- (void)refreshInterface {
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.separatorColor = DividingLineColor;
    self.tableView.backgroundView = [[UIView alloc] init];
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ThemeViewBackgroundColor;
    [self.tableView setTableFooterView:view];
}

- (void)cancelAction {
    _panelController.isHidden = YES;
}

#pragma mark - UITableViewDelegate dataSource & delegate
- (NSInteger)numberOfSectionsIntableView:(UITableView *)tableView{
    [tableView fs_tableViewDisplayWitMsg:FSLocalizedForKey(@"kDigitalSignatureTableEmptyData") ifNecessaryForRowCount:[self.digitalsigDict count]];
    if ([self.digitalsigDict count]) tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    return [self.digitalsigDict count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([self.digitalsigDict count] == 0) {
        UIView *view = [[UIView alloc] init];
        return view;
    }
    
    NSArray *keys = [self.digitalsigDict allKeys];
    
    if ([keys containsObject:@(-1)]) {
        UIView *view = [[UIView alloc] init];
        return view;
    }
    
    UIView *subView = [[UIView alloc] init];
    subView.backgroundColor = TabViewSectionColor;
    UILabel *labelSection = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tableView.bounds.size.width - 20, 25)];
    labelSection.font = [UIFont systemFontOfSize:13];
    labelSection.backgroundColor = [UIColor clearColor];
    labelSection.textColor = BlackThemeTextColor;
    
    NSString *sectionTitle = nil;
    sectionTitle = [NSString stringWithFormat:@"%@ %d", FSLocalizedForKey(@"kPage"), [[keys objectAtIndex:section] intValue] + 1];
    labelSection.text = sectionTitle;
    
    UILabel *labelTotal = [[UILabel alloc] initWithFrame:CGRectMake(tableView.bounds.size.width - 120, 0, 100, 25)];
    labelTotal.font = [UIFont systemFontOfSize:13];
    labelTotal.textAlignment = NSTextAlignmentRight;
    labelTotal.backgroundColor = [UIColor clearColor];
    labelTotal.textColor = TabViewSectionTextColor;
    
    labelTotal.text = [NSString stringWithFormat:@"%lu", (unsigned long) [[self.digitalsigDict objectForKey:@(section)] count]];
    
    [subView addSubview:labelSection];
    [subView addSubview:labelTotal];
    
    [labelSection mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.left.equalTo(subView.mas_safeAreaLayoutGuideLeft).offset(5);
        } else {
            make.left.equalTo(subView.mas_left).offset(5);
        }
        make.width.mas_equalTo(labelSection.frame.size.width);
        make.top.equalTo(subView.mas_top);
        make.height.mas_equalTo(labelSection.frame.size.height);
    }];
    [labelTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.right.equalTo(subView.mas_safeAreaLayoutGuideRight).offset(-5);
        } else {
            make.right.equalTo(subView.mas_right).offset(-5);
        }
        make.width.mas_equalTo(labelTotal.frame.size.width);
        make.top.equalTo(subView.mas_top);
        make.height.mas_equalTo(labelTotal.frame.size.height);
    }];
    
    return subView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keys = [self.digitalsigDict allKeys];
    int pageIndex = [keys containsObject:@(-1)] ? -1 : (int)section;
    return [[self.digitalsigDict objectForKey:@(pageIndex)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellID = @"cellID";
    DigitalSignaturePanelCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    NSArray *keys = [self.digitalsigDict allKeys];
    int pageIndex = [keys containsObject:@(-1)] ? -1 : (int)indexPath.section;
    
    NSArray *rowsData = [self.digitalsigDict objectForKey:@(pageIndex)];
    NSMutableDictionary *cellData = rowsData[indexPath.row];
    
    if (cell == nil) {
        cell = [[DigitalSignaturePanelCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
        cell.frame = CGRectMake(0,0,cell.frame.size.width,cell.frame.size.height);
    }
    cell.backgroundColor = ThemeViewBackgroundColor;
    [cell reloadData:cellData];
    
    __weak typeof(self) weakSelf = self;
    cell.editHandle = ^(UIButton *button) {
        __strong typeof(self) strongSelf = weakSelf;
        
        if (!strongSelf.tapGesture) {
            strongSelf.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:strongSelf action:@selector(handleTap:)];
            [strongSelf.tableView addGestureRecognizer:strongSelf.tapGesture];
        }
        strongSelf.tapGesture.enabled = YES;
        
        if(strongSelf.moreMenuArr.count > 0){
            NSIndexPath *showMenuAtIndexPath = [strongSelf.moreMenuArr objectAtIndex: 0 ];
            
            if( !(showMenuAtIndexPath.section == indexPath.section && showMenuAtIndexPath.row == indexPath.row) ){
                DigitalSignaturePanelCell *cell = [tableView cellForRowAtIndexPath:showMenuAtIndexPath];
                [cell setMoreBtnViewHidden:YES animated:YES];
                
                [strongSelf.moreMenuArr replaceObjectAtIndex:0 withObject:indexPath];
            }
        }else{
            [strongSelf.moreMenuArr addObject:indexPath];
        }
        
    };
    
    cell.viewHandle = ^(UIButton *button) {
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf->_pdfViewCtrl lockRefresh];
        [strongSelf viewSignature:[[cellData objectForKey:@"pageIndex"] intValue] signatureIndex:(int)indexPath.row uuid:[cellData objectForKey:@"uuid"]];
        [strongSelf->_pdfViewCtrl unlockRefresh];
    };
    
    cell.verifyHandle = ^(UIButton *button) {
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf verifySignature:[[cellData objectForKey:@"pageIndex"] intValue] signatureIndex:(int)indexPath.row  uuid:[cellData objectForKey:@"uuid"]];
    };
    cell.signHandle = ^(UIButton *button) {
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf signSignature:[[cellData objectForKey:@"pageIndex"] intValue] signatureIndex:(int)indexPath.row  uuid:[cellData objectForKey:@"uuid"]];
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    if(self.moreMenuArr.count > 0){
        NSIndexPath *showMenuAtIndexPath = [self.moreMenuArr objectAtIndex: 0 ];

        if( !(showMenuAtIndexPath.section == indexPath.section && showMenuAtIndexPath.row == indexPath.row) ){
            DigitalSignaturePanelCell *cell = [tableView cellForRowAtIndexPath:showMenuAtIndexPath];
            [cell setMoreBtnViewHidden:YES animated:YES];

            [self.moreMenuArr replaceObjectAtIndex:0 withObject:indexPath];
        }
    }else{
        [self.moreMenuArr addObject:indexPath];
    }
    
    NSArray *keys = [self.digitalsigDict allKeys];
    int pageIndex = [keys containsObject:@(-1)] ? -1 : (int)indexPath.section;
    
    NSArray *rowsData = [self.digitalsigDict objectForKey:@(pageIndex)];
    NSMutableDictionary *cellData = rowsData[indexPath.row];
    
    [self gotoPage:[[cellData objectForKey:@"pageIndex"] intValue] signatureIndex:(int)indexPath.row uuid:[cellData objectForKey:@"uuid"] isShowSignList:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark IPanelSpec
- (int)getType {
    return FSPanelTypeDigitalSignature;
}

- (UIView *)getTopToolbar {
    return self.toolbar;
}

- (UIView *)getContentView {
    return self.contentView;
}

- (SegmentItem*)getSegmentItem {
    SegmentItem *digitalSignatrue = [[SegmentItem alloc] init];
    digitalSignatrue.tag = FSPanelTypeDigitalSignature;
    UIImage* normalImg = [UIImage imageNamed:@"panel_top_digitalsig_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIImage* selImg = [UIImage imageNamed:@"panel_top_digitalsig_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    digitalSignatrue.image = normalImg;
    digitalSignatrue.selectImage = selImg;
    return digitalSignatrue;
}

- (void)onActivated {
    [self reloadData];
}

- (void)onDeactivated {
    _extensionsManager.isViewSignedDocument = NO;
    [self hideCellEditView];
}

#pragma mark - IPanelChangedListener
- (void)onPanelChanged:(BOOL)isHidden {
    if (isHidden) {
        [self hideCellEditView];
    }
    
}

#pragma mark - IAnnotEventListener

- (void)onAnnotAdded:(FSPDFPage *)page annot:(FSAnnot *)annot {
    [self reloadData];
}

-(void)onAnnotDeleted:(FSPDFPage *)page annot:(FSAnnot *)annot {
    [self reloadData];
}

- (void)onAnnotModified:(FSPDFPage *)page annot:(FSAnnot *)annot {
    [self reloadData];
}

- (void)onCurrentAnnotChanged:(FSAnnot *)lastAnnot currentAnnot:(FSAnnot *)currentAnnot {
}

#pragma mark IDocEventListener
- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    
    if (_extensionsManager.isViewSignedDocument) {
        return ;
    }
    if (self.signDocExtensionsMgr.isViewSignedDocument) {
        return ;
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self reloadData];
    });
}

- (void)onDocWillClose:(FSPDFDoc *)document{
    if ([document isEqual:[self.signDocExtensionsMgr.pdfViewCtrl currentDoc]]) {
        self.signDocExtensionsMgr.isViewSignedDocument = NO;
        _extensionsManager.isViewSignedDocument = NO;
    }
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

#pragma mark IPageEventListener
- (void)onPagesWillRemove:(NSArray<NSNumber *> *)indexes {
}

- (void)onPagesWillMove:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
}

- (void)onPagesWillRotate:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    [self reloadData];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    [self reloadData];
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    [self reloadData];
}

#pragma mark private
- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
    [self hideCellEditView];
}

- (void)hideCellEditView {
    if(self.moreMenuArr.count > 0){
        NSIndexPath *showMenuAtIndexPath = [self.moreMenuArr objectAtIndex: 0 ];
        
        DigitalSignaturePanelCell *cell = [self.tableView cellForRowAtIndexPath:showMenuAtIndexPath];
        [cell setMoreBtnViewHidden:YES animated:YES];
    }
    
    self.tapGesture.enabled = NO;
}


-(void)signSignature:(int)pageIndex signatureIndex:(int)signatureIndex uuid:(NSString *)uuid{
    @try {
        if ([_pdfViewCtrl isDynamicXFA]) {
            FSXFADoc *xfaDoc = [_pdfViewCtrl getXFADoc];
            FSXFAPage *page = [xfaDoc getPage:pageIndex];
            FSXFAWidget *xfaWidget = [page getWidgetByFullName:uuid];
            
            if (![xfaWidget isEmpty] && [xfaWidget getType] == FSXFAWidgetWidgetTypeSignature) {
                
                DynamicXFAHandler *annotHandler = nil;
                annotHandler = (DynamicXFAHandler *)[_extensionsManager getDynamicAnnotHandler];
                annotHandler.handlerCurrentWidget = nil;
                [annotHandler.formNaviBar.contentView setHidden:YES];
                
                _panelController.isHidden = YES;
                [_pdfViewCtrl gotoPage:pageIndex animated:YES];
                
                BOOL isShowSignList = YES;
                if (isShowSignList) {
                    [self performSelector:@selector(setCurrentXFAWidgetItem:) withObject:xfaWidget afterDelay:0.5];
                }
                
                return ;
            }
            return;
        }
        
        FSSignature *signature = [_pdfViewCtrl.currentDoc getSignature:signatureIndex];
        if ([signature isEmpty]) {
            return ;
        }
        FSWidget *widget = [[signature getControl:0] getWidget];
        
        @try {
            int pageIndex = [[widget getPage] getIndex];
            _panelController.isHidden = YES;
            [_pdfViewCtrl gotoPage:pageIndex animated:YES];
            [self performSelector:@selector(setCurrentAnnotionItem:) withObject:widget afterDelay:0.5];
            
        } @catch (NSException *exception) {
        }
        
    } @catch (NSException *exception) {
    }
}

-(void)verifySignature:(int)pageIndex signatureIndex:(int)signatureIndex uuid:(NSString *)uuid{
    @try {
        if ([_pdfViewCtrl isDynamicXFA]) {
            FSXFADoc *xfaDoc = [_pdfViewCtrl getXFADoc];
            FSXFAPage *page = [xfaDoc getPage:pageIndex];
            FSXFAWidget *xfaWidget = [page getWidgetByFullName:uuid];
            
            if (![xfaWidget isEmpty] && [xfaWidget getType] == FSXFAWidgetWidgetTypeSignature) {
                
                FSSignature *signature = [xfaWidget getSignature];
                if ([signature isEmpty]) return ;
                
                CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:[xfaWidget getRect] pageIndex:pageIndex];
                CGRect resultRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:pageIndex];
                
                DynamicXFASignatureAnnotHandler *signatureAnnotHandler = [[DynamicXFASignatureAnnotHandler alloc] initWithUIExtensionsManager:_extensionsManager];
                [_pdfViewCtrl registerScrollViewEventListener:signatureAnnotHandler];
                [_extensionsManager registerRotateChangedListener:signatureAnnotHandler];
                [_extensionsManager registerAnnotHandler:signatureAnnotHandler];
                signatureAnnotHandler.isDynamicXFAFile = YES;
                signatureAnnotHandler.menuRect = resultRect;
                signatureAnnotHandler.editXFAWidget = xfaWidget;
                [signatureAnnotHandler dynamicXFASignatureSelected: signature shouldShowMenu:NO];
                [signatureAnnotHandler verify];
                
                return ;
            }
            return;
        }
        
        FSSignature *signature = [_pdfViewCtrl.currentDoc getSignature:signatureIndex];
        if ([signature isEmpty]) {
            return ;
        }
        FSWidget *widget = [[signature getControl:0] getWidget];
        
        DigitalSignatureAnnotHandler *annotHandler = nil;
        annotHandler = (DigitalSignatureAnnotHandler *)[_extensionsManager getAnnotHandlerByAnnot:widget];
        annotHandler.currentSelectSign = signature;
        [annotHandler verify];
        
    } @catch (NSException *exception) {
    }
}

-(void)viewSignature:(int)pageIndex signatureIndex:(int)signatureIndex uuid:(NSString *)uuid{
    @try {
        if ([_pdfViewCtrl isDynamicXFA]) {
            FSXFADoc *xfaDoc = [_pdfViewCtrl getXFADoc];
            FSXFAPage *page = [xfaDoc getPage:pageIndex];
            FSXFAWidget *xfaWidget = [page getWidgetByFullName:uuid];
            
            if (![xfaWidget isEmpty] && [xfaWidget getType] == FSXFAWidgetWidgetTypeSignature) {

                unsigned long fileLength = [_pdfViewCtrl.currentDoc getFileSize];
                FSInt32Array *byteRanges = [[xfaWidget getSignature] getByteRangeArray];
                unsigned int r1 = 0;
                unsigned int r2 = 0;
                if ([byteRanges getSize] == 4) {
                    r1 = [byteRanges getAt:2];
                    r2 = [byteRanges getAt:3];
                }
                
                if (fileLength < (r1 + r2)) {
                    [self.contentView fs_showHUDMessage:FSLocalizedForKey(@"kDigitalSignatureFaildView")];
                }else if (fileLength == (r1 + r2)){
                    [self.contentView fs_showHUDMessage:FSLocalizedForKey(@"kDigitalSignatureAllReadyView")];
                }else{
                    FSPDFDoc *doc = [[xfaWidget getSignature] getSignedVersionDocument:_pdfViewCtrl.filePath];
                    if ([doc isEmpty]) return;
                    if ([doc load:nil] == FSErrSuccess) {
                        BOOL isUseTabToView = NO;
                        if (_extensionsManager.isMultiFileMode && isUseTabToView) {
                            NSString *tempDirectory = NSTemporaryDirectory();
                            NSString *tmpPath = [tempDirectory stringByAppendingString:[NSString stringWithFormat:@"tmpdigs_%@.pdf",[[xfaWidget getSignature] getName]]];
                            [doc saveAs:tmpPath save_flags:FSPDFDocSaveFlagNormal];
                            
                            NSString *path = tmpPath;
                            [_extensionsManager.delegate uiextensionsManager:_extensionsManager openNewDocAtPath:path shouldCloseCurrentDoc:NO];
                            
                        } else{
                            [self previewSignedVersionDocument:doc];
                        }
                        
                        
                    }else {
                        [self.contentView fs_showHUDMessage:FSLocalizedForKey(@"kFileErrorNotLoaded")];
                        
                        _extensionsManager.isViewSignedDocument = NO;
                    }
                    
                }
            }
            return;
        }
        
        FSSignature *signature = [_pdfViewCtrl.currentDoc getSignature:signatureIndex];
        if ([signature isEmpty]) {
            return ;
        }
//        FSWidget *widget = [[signature getControl:0] getWidget];
        
        unsigned long fileLength = [_pdfViewCtrl.currentDoc getFileSize];
        FSInt32Array *byteRanges = [signature getByteRangeArray];
        unsigned int r1 = 0;
        unsigned int r2 = 0;
        if ([byteRanges getSize] == 4) {
            r1 = [byteRanges getAt:2];
            r2 = [byteRanges getAt:3];
        }
        
        if (fileLength < (r1 + r2)) {
            [self.contentView fs_showHUDMessage:FSLocalizedForKey(@"kDigitalSignatureFaildView")];
        }else if (fileLength == (r1 + r2)){
            [self.contentView fs_showHUDMessage:FSLocalizedForKey(@"kDigitalSignatureAllReadyView")];
        }else{
            
            FSPDFDoc *doc = [signature getSignedVersionDocument:_pdfViewCtrl.filePath];
            if ([doc isEmpty]) return;
            if ([doc load:nil] == FSErrSuccess) {
                BOOL isUseTabToView = NO;
                if (_extensionsManager.isMultiFileMode && isUseTabToView) {
                    NSString *tempDirectory = NSTemporaryDirectory();
                    NSString *tmpPath = [tempDirectory stringByAppendingString:[NSString stringWithFormat:@"tmpdigs_%@.pdf",[signature getName]]];
                    [doc saveAs:tmpPath save_flags:FSPDFDocSaveFlagNormal];
                    
                    NSString *path = tmpPath;
                    [_extensionsManager.delegate uiextensionsManager:_extensionsManager openNewDocAtPath:path shouldCloseCurrentDoc:NO];
                    
                } else{
                    [self previewSignedVersionDocument:doc];
                }
            }else {
                [self.contentView fs_showHUDMessage:FSLocalizedForKey(@"kFileErrorNotLoaded")];
                _extensionsManager.isViewSignedDocument = NO;
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}

-(void)previewSignedVersionDocument:(FSPDFDoc *)doc {
    FSPDFViewCtrl *pdfviewctrl = [[FSPDFViewCtrl alloc] initWithFrame:_pdfViewCtrl.bounds];
    [pdfviewctrl setRMSAppClientId:@"972b6681-fa03-4b6b-817b-c8c10d38bd20" redirectURI:@"com.foxitsoftware.com.mobilepdf-for-ios://authorize"];
    [pdfviewctrl registerDocEventListener:self];
    
    self.signDocExtensionsMgr = [[UIExtensionsManager alloc] initWithPDFViewControl:pdfviewctrl configurationObject:self->_extensionsManager.config];
    
    pdfviewctrl.extensionsManager = self.signDocExtensionsMgr;
    [pdfviewctrl setViewSignedDocument:YES];
    [pdfviewctrl setDoc:doc];
    [pdfviewctrl setValue:_pdfViewCtrl.filePath forKey:@"filePath"];
    
    _extensionsManager.isViewSignedDocument = YES;
    self.signDocExtensionsMgr.isViewSignedDocument = YES;
    
    UIViewController *pdfViewController = [[UIViewController alloc] init];
    [pdfViewController.view addSubview: pdfviewctrl];
    [pdfviewctrl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(pdfviewctrl.superview);
    }];
    
    [pdfViewController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_pdfViewCtrl.fs_viewController presentViewController:pdfViewController animated:YES completion:nil];
    });

    __weak typeof(self) weakSelf = self;
    self.signDocExtensionsMgr.goBack = ^() {
        __strong typeof(self) strongSelf = weakSelf;

        if (pdfViewController != nil || strongSelf.signDocExtensionsMgr.isViewSignedDocument) {
            [pdfViewController dismissViewControllerAnimated:YES completion:nil];
        }
    };
}

-(void)gotoPage:(int)pageIndex signatureIndex:(int)signatureIndex uuid:(NSString *)uuid isShowSignList:(BOOL)isShowSignList {
    @try {
        if ([_pdfViewCtrl.currentDoc isXFA] && [_pdfViewCtrl isDynamicXFA] ) {
            FSXFADoc *xfaDoc = [_pdfViewCtrl getXFADoc];
            FSXFAPage *page = [xfaDoc getPage:pageIndex];
            FSXFAWidget *xfaWidget = [page getWidgetByFullName:uuid];
            
            if (![xfaWidget isEmpty] && [xfaWidget getType] == FSXFAWidgetWidgetTypeSignature) {
                
                _panelController.isHidden = YES;
                [_pdfViewCtrl gotoPage:pageIndex animated:YES];
                
                BOOL isShowSignList = YES;
                if (isShowSignList) {
                    [self performSelector:@selector(setCurrentXFAWidgetItem:) withObject:xfaWidget afterDelay:0.5];
                }
                
                return ;
            }
            return;
        }
        
        FSSignature *signature = [_pdfViewCtrl.currentDoc getSignature:signatureIndex];
        if ([signature isEmpty]) {
            return ;
        }
        FSWidget *widget = [[signature getControl:0] getWidget];
        
        @try {
            int pageIndex = [[widget getPage] getIndex];
            _panelController.isHidden = YES;
            [_pdfViewCtrl gotoPage:pageIndex animated:YES];
            
            if (isShowSignList) {
                [self performSelector:@selector(setCurrentAnnotionItem:) withObject:widget afterDelay:0.5];
            }
            
        } @catch (NSException *exception) {
        }
        
    } @catch (NSException *exception) {
    }
}

- (void)setCurrentAnnotionItem:(FSWidget *)widget {
    if ([_pdfViewCtrl getPageLayoutMode] != PDF_LAYOUT_MODE_REFLOW) {
        [_extensionsManager setCurrentAnnot:widget];
    }
}

- (void)setCurrentXFAWidgetItem:(FSXFAWidget *)widget {
    if ([_pdfViewCtrl getPageLayoutMode] != PDF_LAYOUT_MODE_REFLOW) {
        [_extensionsManager setCurrentWidget:widget];
        
        int pageIndex = [[widget getXFAPage] getIndex];
        FSSignature *signature = [widget getSignature];
        if ([signature isEmpty]) return ;
        
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:[widget getRect] pageIndex:pageIndex];
        CGRect resultRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:pageIndex];
        
        DynamicXFASignatureAnnotHandler *signatureAnnotHandler = [[DynamicXFASignatureAnnotHandler alloc] initWithUIExtensionsManager:_extensionsManager];
        [_pdfViewCtrl registerScrollViewEventListener:signatureAnnotHandler];
        [_extensionsManager registerRotateChangedListener:signatureAnnotHandler];
        [_extensionsManager registerAnnotHandler:signatureAnnotHandler];
        signatureAnnotHandler.isDynamicXFAFile = YES;
        signatureAnnotHandler.menuRect = resultRect;
        signatureAnnotHandler.editXFAWidget = widget;
        [signatureAnnotHandler dynamicXFASignatureSelected:signature shouldShowMenu:YES];
    }
}

-(void)getSignaturesInXFADoc {
    @try {
        FSXFADoc *xfaDoc = [_pdfViewCtrl getXFADoc];
        int pageCount = [xfaDoc getPageCount];
        
        NSMutableArray *array = @[].mutableCopy;
        for (int i = 0 ; i < pageCount ; i++) {
            FSXFAPage *page = [xfaDoc getPage:i];
            int widgetCount = [page getWidgetCount];
            
            int signIndex = 1;
            for (int m = 0 ; m < widgetCount; m++) {
                FSXFAWidget *xfaWidget = [page getWidget:m];
                if (![xfaWidget isEmpty] && [xfaWidget getType] == FSXFAWidgetWidgetTypeSignature) {
                    FSSignature *signature = [xfaWidget getSignature];
                    if ([signature isEmpty]) {
                        continue;
                    }
                    
                    if ([signature isEmpty]) {
                        continue;
                    }
                    NSMutableDictionary *itemData = @{}.mutableCopy;
                    [itemData setObject:@([signature isSigned]) forKey:@"isSigned"];
                    if ([signature isSigned]) {
                        NSString *signer = nil;
                        
                        NSString *certSubject = [signature getCertificateInfo:@"Subject"];
                        NSRange r = [certSubject rangeOfString:@"CN="];
                        if (0 < r.length) {
                            certSubject = [certSubject substringFromIndex:r.location + 3];
                            r = [certSubject rangeOfString:@","];
                            if (0 < r.length) {
                                certSubject = [certSubject substringToIndex:r.location];
                            }
                        }
                        signer = certSubject;
                        
                        if (signer == nil || signer.length == 0) {
                            signer = [signature getKeyValue:FSSignatureKeyNameSigner];
                        }
                        
                        if (signer == nil || signer.length == 0) {
                            signer = FSLocalizedForKey(@"kDigitalSignatureListSignerUnknown");
                        }

                        [itemData setObject:signer forKey:@"signer"];
                        
                        [itemData setObject:[signature getSignTime] forKey:@"signdate"];
                        [itemData setObject:@(signIndex++) forKey:@"signIndex"];
                    }else{
                        [itemData setObject:[signature getName] forKey:@"signName"];
                    }
                    
                    [itemData setObject:@(NO) forKey:@"readOnly"];
                    if ([signature getFlags] & FSFieldFlagReadOnly) {
                        [itemData setObject:@(YES) forKey:@"readOnly"];
                    }
                    
                    [itemData setObject:@(i) forKey:@"pageIndex"];
                    
                    [itemData setObject:[xfaWidget getName:FSXFAWidgetWidgetNameTypeFullName] forKey:@"uuid"];
                    
                    [array addObject:itemData];
                }
            }
        }
        
        if (array.count > 0) {
            [self.digitalsigDict setObject:array forKey:@(-1)];
        }
    } @catch (NSException *exception) {
        
    }
}

-(void)getSignatureListData:(FSPDFDoc *)document {
    @try {
        if (!document || [document isEmpty]) return;
        if ([document isXFA] && [_pdfViewCtrl isDynamicXFA] ) {
            [_pdfViewCtrl lockRefresh];
            [self getSignaturesInXFADoc];
            [_pdfViewCtrl unlockRefresh];
            return ;
        }
    } @catch (NSException *exception) {
        return;
    }
    
    @try {
        int signIndex = 1;
        int signatureCount = [document getSignatureCount];
        
        NSMutableArray *array = @[].mutableCopy;
        for (int i = 0 ; i < signatureCount; i ++) {
            FSSignature *signature = [document getSignature:i];

            if ([signature isEmpty]) {
                continue;
            }
            NSMutableDictionary *itemData = @{}.mutableCopy;
            [itemData setObject:@([signature isSigned]) forKey:@"isSigned"];
            if ([signature isSigned]) {
                NSString *signer = [signature getKeyValue:FSSignatureKeyNameSigner];
                
                 if (!signer.length) {
                     NSString *certSubject = [signature getCertificateInfo:@"Subject"];
                     NSRange r = [certSubject rangeOfString:@"CN="];
                     if (0 < r.length) {
                         certSubject = [certSubject substringFromIndex:r.location + 3];
                         r = [certSubject rangeOfString:@","];
                         if (0 < r.length) {
                             certSubject = [certSubject substringToIndex:r.location];
                         }
                     }
               
                    signer = certSubject;
                }
                
                if (!signer.length) {
                    NSMutableArray *arr =  [FSPDFCertUtil getCertFromSignatureContent:signature];
                    if (arr != nil) {
                        NSDictionary *dic = [FSPDFCertUtil getTrustCertificateInformation:[arr lastObject]];
                        signer = dic[@"name"];
                    }
                }
                
                if (signer == nil || signer.length == 0) {
                    signer = FSLocalizedForKey(@"kDigitalSignatureListSignerUnknown");
                }
                
                [itemData setObject:signer forKey:@"signer"];
                
                [itemData setObject:[signature getSignTime] forKey:@"signdate"];
                [itemData setObject:@(signIndex++) forKey:@"signIndex"];
            }else{
                [itemData setObject:[signature getName] forKey:@"signName"];
            }
            
            [itemData setObject:@(-1) forKey:@"pageIndex"];
            
            [itemData setObject:@(NO) forKey:@"readOnly"];
            if ([signature getFlags] & FSFieldFlagReadOnly) {
                [itemData setObject:@(YES) forKey:@"readOnly"];
            }
            
            FSWidget *widget = [[signature getControl:0] getWidget];
            @try {
                if ([widget isEmpty]) continue;
                [itemData setObject:[widget NM] forKey:@"uuid"];
                
                [array addObject:itemData];
            }@catch (NSException *exception){
                continue;
            }
        }
        if (array.count > 0) {
            [self.digitalsigDict setObject:array forKey:@(-1)];
        }

    }@catch (NSException *exception) {
    }
}

- (void)reloadData {
    [self.digitalsigDict removeAllObjects];
    [self getSignatureListData:_pdfViewCtrl.currentDoc];
    [self.tableView reloadData];
}

@end
