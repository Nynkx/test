/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "OutlinePanel.h"
#import "OutlineViewController.h"
#import "PanelController+private.h"
#import "PanelHost.h"

@interface OutlinePanel () {
    __weak FSPanelController *_panelController;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak UIExtensionsManager *_extensionsManager;
}

@property (nonatomic, strong) UIView *toolbar;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) OutlineViewController *outlineCtrl;
@property (nonatomic, strong) FSNavigationController *navCtrl;

@end

@implementation OutlinePanel

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager panelController:(FSPanelController *)panelController {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _panelController = panelController;
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 100, 25)];
        title.backgroundColor = [UIColor clearColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        title.text = FSLocalizedForKey(@"kOutline");
        title.textColor = BlackThemeTextColor;
        self.toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _pdfViewCtrl.bounds.size.width, 64)];
        self.toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.toolbar.backgroundColor = PanelTopBarColor;
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 107, _pdfViewCtrl.bounds.size.width, _pdfViewCtrl.bounds.size.height - 107)];
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.contentView.backgroundColor = ThemeViewBackgroundColor;

        _outlineCtrl = [[OutlineViewController alloc] initWithStyle:UITableViewStylePlain pdfViewCtrl:_pdfViewCtrl panelController:_panelController];
        _outlineCtrl.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _outlineCtrl.view.backgroundColor = [UIColor clearColor];
        _outlineCtrl.hasParentBookmark = NO;
        _outlineCtrl.bookmarkGotoPageHandler = ^(int page) {
        };

        self.navCtrl = [[FSNavigationController alloc] initWithRootViewController:_outlineCtrl];
        self.navCtrl.view.frame = self.contentView.bounds;
        self.navCtrl.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        self.navCtrl.navigationBarHidden = YES;
        [self.contentView addSubview:self.navCtrl.view];
        title.center = CGPointMake(self.toolbar.bounds.size.width / 2, title.center.y);

        UIView *divideView = [[UIView alloc] initWithFrame:CGRectMake(0, 106, _pdfViewCtrl.bounds.size.width, [Utility realPX:1.0f])];
        divideView.backgroundColor = DividingLineColor;
        divideView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
        [self.toolbar addSubview:divideView];
        [self.toolbar addSubview:title];
        if (DEVICE_iPHONE) {
            UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 32, 12, 12)];
            [cancelButton addTarget:self action:@selector(cancelBookmark) forControlEvents:UIControlEventTouchUpInside];
            cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
            [cancelButton setBackgroundImage:[UIImage imageNamed:@"panel_cancel" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [cancelButton setEnlargedEdge:10];
            [self.toolbar addSubview:cancelButton];
        }
    }
    return self;
}

- (void)load {
    [_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_panelController.panel insertSpec:self atIndex:1];
    _panelController.panel.currentSpec = self;
}

- (void)unload {
    [_pdfViewCtrl unregisterDocEventListener:self];
    [_pdfViewCtrl unregisterPageEventListener:self];
    [_panelController.panel removeSpec:self];
}

- (void)cancelBookmark {
    _panelController.isHidden = YES;
}

- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error == FSErrSuccess) {
        [_outlineCtrl loadData:nil];
    }
}

- (void)onDocWillClose:(FSPDFDoc *)document {
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
    [_outlineCtrl clearData];
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

- (int)getType {
    return FSPanelTypeOutline;
}

- (SegmentItem*)getSegmentItem {
    SegmentItem *outline = [[SegmentItem alloc] init];
    outline.tag = FSPanelTypeOutline;
    UIImage* normalImg = [UIImage imageNamed:@"panel_top_outline_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIImage* selImg = [UIImage imageNamed:@"panel_top_outline_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    outline.image = normalImg;
    outline.selectImage = selImg;
    return outline;
}

- (UIView *)getTopToolbar {
    return self.toolbar;
}

- (UIView *)getContentView {
    return self.contentView;
}

- (void)onActivated {
}

- (void)onDeactivated {
}

#pragma mark IPageEventListener
- (void)onPagesWillRemove:(NSArray<NSNumber *> *)indexes {
}

- (void)onPagesWillMove:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
}

- (void)onPagesWillRotate:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    [_outlineCtrl clearData];
    [_outlineCtrl loadData:nil];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    [_outlineCtrl clearData];
    [_outlineCtrl loadData:nil];
}

- (void)onPagesRotated:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

@end
