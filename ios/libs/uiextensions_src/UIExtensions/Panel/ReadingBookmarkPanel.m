/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ReadingBookmarkPanel.h"
#import "PanelController+private.h"
#import "PanelHost.h"
#import "ReadingBookmarkModule.h"
#import "ReadingBookmarkViewController.h"
#import "UniversalEditViewController.h"

@interface ReadingBookmarkPanel () {
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPanelController *_panelController;
}

@property (nonatomic, strong) UIView *toolbar;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) ReadingBookmarkViewController *bookmarkCtrl;
@end

@implementation ReadingBookmarkPanel

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager panelController:(FSPanelController *)panelController {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _panelController = panelController;
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 180, 25)];
        title.backgroundColor = [UIColor clearColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        title.text = FSLocalizedForKey(@"kBookmark");
        title.textColor = BlackThemeTextColor;
        self.toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _pdfViewCtrl.bounds.size.width, 64)];
        self.toolbar.backgroundColor = PanelTopBarColor;
        self.toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 107, DEVICE_iPHONE ? CGRectGetWidth(_pdfViewCtrl.bounds) : 300, _pdfViewCtrl.bounds.size.height - 107)];
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.contentView.backgroundColor = ThemeViewBackgroundColor;

        self.bookmarkCtrl = [[ReadingBookmarkViewController alloc] initWithStyle:UITableViewStylePlain pdfViewCtrl:_pdfViewCtrl panelController:_panelController];
        //        _bookmarkCtrl.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _bookmarkCtrl.view.backgroundColor = [UIColor clearColor];
        _bookmarkCtrl.bookmarkGotoPageHandler = ^(int page) {

        };
        _bookmarkCtrl.bookmarkSelectionHandler = ^() {

        };
        
        __weak typeof(_extensionsManager) weakManager = _extensionsManager;
        _bookmarkCtrl.bookmarkDeleteHandler = ^() {
            __strong UIExtensionsManager *strongManager = weakManager;
            [[NSNotificationCenter defaultCenter] postNotificationName:UPDATEBOOKMARK object:nil];
            strongManager.isDocModified = YES;
        };
        self.editButton = [[UIButton alloc] initWithFrame:CGRectMake(self.toolbar.frame.size.width - 65, 20, 55, 35)];
        [_editButton addTarget:self action:@selector(clearBookmark:) forControlEvents:UIControlEventTouchUpInside];
        [_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
        [_editButton setTitle:FSLocalizedForKey(@"kClear") forState:UIControlStateNormal];
        _editButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [_editButton setEnlargedEdge:ENLARGE_EDGE];

        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        UITapGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelBookmark)];
        backgroundView.userInteractionEnabled = YES;
        [backgroundView addGestureRecognizer:tapG];
        [self.toolbar addSubview:backgroundView];

        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 32, 12, 12)];
        [cancelButton addTarget:self action:@selector(cancelBookmark) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        [cancelButton setBackgroundImage:[UIImage imageNamed:@"panel_cancel.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];

        _bookmarkCtrl.view.frame = self.contentView.bounds;
        [self.contentView addSubview:_bookmarkCtrl.view];
        [_bookmarkCtrl.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left);
            make.right.equalTo(self.contentView.mas_right);
            make.top.equalTo(self.contentView.mas_top);
            make.bottom.equalTo(self.contentView.mas_bottom);
        }];

        title.center = CGPointMake(self.toolbar.bounds.size.width / 2, title.center.y);

        UIView *divideView = [[UIView alloc] initWithFrame:CGRectMake(0, 106, _pdfViewCtrl.bounds.size.width, [Utility realPX:1.0f])];
        divideView.backgroundColor = DividingLineColor;
        divideView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
        [self.toolbar addSubview:divideView];

        [self.toolbar addSubview:title];
        [self.toolbar addSubview:_editButton];
        if (DEVICE_iPHONE) {
            [backgroundView addSubview:cancelButton];
        }
    }
    return self;
}

- (void)cancelBookmark {
    _panelController.isHidden = YES;
}

- (void)clearBookmark:(id)sender {
    if ([_bookmarkCtrl getBookmarkCount] > 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kClearBookmark") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 [self->_bookmarkCtrl clearData:YES];
                                                                 [[NSNotificationCenter defaultCenter] postNotificationName:UPDATEBOOKMARK object:nil];
                                                                 self->_extensionsManager.isDocModified = YES;
                                                             }];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancelAction];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)load {
    [_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_panelController.panel insertSpec:self atIndex:0];
    _panelController.panel.currentSpec = self;
}

- (void)unload {
    [_pdfViewCtrl unregisterDocEventListener:self];
    [_pdfViewCtrl unregisterPageEventListener:self];
    [_panelController.panel removeSpec:self];
    _panelController.panel.currentSpec = nil;
}

- (void)editBookMark {
    _bookmarkCtrl.isContentEditing = !_bookmarkCtrl.isContentEditing;
}

- (void)reloadData {
    [_bookmarkCtrl loadData];
}

#pragma mark--- IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    //    [_bookmarkCtrl clearData:NO];
    [_bookmarkCtrl loadData];
    if (![Utility canAssemble:_pdfViewCtrl]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.editButton.enabled = NO;
            self.editButton.hidden = YES;
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.editButton.enabled = YES;
            self.editButton.hidden = NO;
            [self->_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
        });
    }
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    [_bookmarkCtrl clearData:NO];
    if (self.bookmarkCtrl.currentVC) {
        if ([self.bookmarkCtrl.currentVC isKindOfClass:[UINavigationController class]]) {
            [(UINavigationController *) self.bookmarkCtrl.currentVC dismissViewControllerAnimated:NO completion:nil];
        }

        else if ([self.bookmarkCtrl.currentVC isKindOfClass:[UniversalEditViewController class]]) {
            [(UniversalEditViewController *) self.bookmarkCtrl.currentVC dismissViewControllerAnimated:NO completion:nil];
        }

        else if ([self.bookmarkCtrl.currentVC isKindOfClass:[UIAlertController class]]) {
            [(UIAlertController *) self.bookmarkCtrl.currentVC dismissViewControllerAnimated:NO completion:nil];
        } else if ([self.bookmarkCtrl.currentVC isKindOfClass:[TSAlertView class]]) {
            [(TSAlertView *) self.bookmarkCtrl.currentVC dismissWithClickedButtonIndex:0 animated:NO];
        }
    }
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.editButton.enabled = YES;
        [self->_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
    });
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

#pragma mark IPageEventListener
- (void)onPagesWillRemove:(NSArray<NSNumber *> *)indexes {
}

- (void)onPagesWillMove:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
}

- (void)onPagesWillRotate:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    NSMutableArray *readingbookMarksArray =[NSMutableArray array];
    
    FSPDFDoc *doc = _pdfViewCtrl.currentDoc;
    int count = [doc getReadingBookmarkCount];
    for (int i = count - 1; i > -1; i--) {
        FSReadingBookmark *rb = [doc getReadingBookmark:i];
        int index = [rb getPageIndex];
        [readingbookMarksArray addObject:@(index)];
        if (-1 == index) {
            [doc removeReadingBookmark:rb];
        }
    }
    
    NSMutableArray *oldPageArray =[NSMutableArray array];
    int oldpageCount = [_pdfViewCtrl getPageCount] + (int)[indexes count];
    for (int i = 0; i < oldpageCount; i++) {
        [oldPageArray addObject:@(i)];
    }
    
    for (int i = 0; i < indexes.count; i++) {
        [oldPageArray removeObject: [indexes objectAtIndex:i]];
    }

    if (![readingbookMarksArray containsObject:@(-1)]) {
        int count = (int)[readingbookMarksArray count];
        for (int i = count - 1; i > -1; i--) {
            FSReadingBookmark *rb = [doc getReadingBookmark:i];
            int index = [rb getPageIndex];
            [readingbookMarksArray addObject:@(index)];
            if ([indexes containsObject:@(index)]) {
                [doc removeReadingBookmark:rb];
            }
            if ([oldPageArray containsObject:@(index)] && rb.pageIndex != [oldPageArray indexOfObject:@(index)]) {
                int newPageIndex = (int)[oldPageArray indexOfObject:@(index)];
                rb.pageIndex = newPageIndex;
            }
        }
    }

    [_bookmarkCtrl clearData:NO];
    [_bookmarkCtrl loadData];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.editButton.enabled = YES;
        [self->_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
    });
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    [_bookmarkCtrl clearData:NO];
    [_bookmarkCtrl loadData];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.editButton.enabled = YES;
        [self->_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
    });
}

- (void)onPagesRotated:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (int)getType {
    return FSPanelTypeReadingBookmark;
}

- (UIView *)getTopToolbar {
    return self.toolbar;
}

- (UIView *)getContentView {
    return self.contentView;
}

- (SegmentItem*)getSegmentItem{
    SegmentItem *bookmark = [[SegmentItem alloc] init];
    bookmark.tag = FSPanelTypeReadingBookmark;
    bookmark.image = [UIImage imageNamed:@"panel_top_bookmak_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    bookmark.selectImage = [UIImage imageNamed:@"panel_top_bookmak_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    return bookmark;
}

- (void)onActivated {
}

- (void)onDeactivated {
    [self.bookmarkCtrl hideCellEditView];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return NO;
}

@end
