/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AttachmentViewController.h"
#import "AlertView.h"
#import "AnnotationItem.h"
#import "AnnotationListCell.h"
#import "AnnotationListMore.h"
#import "AnnotationStruct.h"
#import "AttachmentController.h"
#import "AttachmentPanel.h"
#import "FSAnnotExtent.h"
#import "FileManageListViewController.h"
#import "FileSelectDestinationViewController.h"
#import "PanelController.h"
#import "PanelHost.h"
#import "UIExtensionsConfig.h"

@interface AttachmentViewController () <IPanelChangedListener, AnnotationListCellDelegate, IAnnotEventListener>

@property (nonatomic, assign) BOOL isKeyboardShow;
@property (nonatomic, retain) UIDocumentInteractionController *documentPopoverController;
@property (nonatomic, strong) NSOperationQueue *loadAttachmentQueue;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (nonatomic, strong) UIViewController *originalPresentedViewController;

@end

@implementation AttachmentViewController {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
    FSPanelController *__weak _panelController;
    AttachmentPanel *__weak _attachmentPanel;
}

- (id)initWithStyle:(UITableViewStyle)style extensionsManager:(UIExtensionsManager *)extensionsManager module:(AttachmentPanel *)attachmentPanel {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _panelController = attachmentPanel.panelController;
        _attachmentPanel = attachmentPanel;
        _allAttachmentsSections = [NSMutableArray array];
        [_panelController registerPanelChangedListener:self];
        [_extensionsManager registerAnnotEventListener:self];
        [_pdfViewCtrl registerDocEventListener:self];
        [_pdfViewCtrl registerPageEventListener:self];
        self.moreIndexPath = nil;
        self.originalPresentedViewController = nil;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundView = [[UIView alloc] init];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    ((UIScrollView *) self.tableView).delegate = self;
    self.tableView.clipsToBounds = YES;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ThemeViewBackgroundColor;
    [self.tableView setTableFooterView:view];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataList:) name:NSNotificationNameAttachmentLsitUpdate object:nil];
    
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.separatorColor = DividingLineColor;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
//    //    [NSObject cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
//    [self.tableView reloadData];
//    //    double delayInSeconds = 1.2;
//    //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
//    //        [self.tableView reloadData];
//    //    });
//}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)deviceOrientationChange {
    [self.tableView reloadData];

    [[NSNotificationCenter defaultCenter] postNotificationName:ORIENTATIONCHANGED object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (UIDeviceOrientationIsValidInterfaceOrientation(interfaceOrientation));
}
*/

#pragma mark - IAnnotEventListener

- (void)onAnnotAdded:(FSPDFPage *)page annot:(FSAnnot *)annot {
    if (annot.type == FSAnnotFileAttachment) {
        [self updateAllAttachments:[AttachmentItem itemWithAttachmentAnnotation:[[FSFileAttachment alloc] initWithAnnot:annot]] operation:AnnotationOperation_Add];
    }
}

- (void)onAnnotWillDelete:(FSPDFPage *)page annot:(FSAnnot *)annot {
    if (annot.type == FSAnnotFileAttachment) {
        [self updateAllAttachments:[AttachmentItem itemWithAttachmentAnnotation:[[FSFileAttachment alloc] initWithAnnot:annot]] operation:AnnotationOperation_Delete];
    }
}

- (void)onAnnotModified:(FSPDFPage *)page annot:(FSAnnot *)annot {
    if (annot.type == FSAnnotFileAttachment) {
        [self updateAllAttachments:[AttachmentItem itemWithAttachmentAnnotation:[[FSFileAttachment alloc] initWithAnnot:annot]] operation:AnnotationOperation_Modify];
    }
}

- (void)onDocumentAttachmentAdded:(AttachmentItem *)attachmentItem {
    [self updateAllAttachments:attachmentItem operation:AnnotationOperation_Add];
}

- (void)onDocumentAttachmentDeleted:(AttachmentItem *)attachmentItem {
    [self updateAllAttachments:attachmentItem operation:AnnotationOperation_Delete];
}

- (void)onDocumentAttachmentModified:(AttachmentItem *)attachmentItem {
    [self updateAllAttachments:attachmentItem operation:AnnotationOperation_Modify];
}

#pragma mark - IPanelChangedListener

- (void)onPanelChanged:(BOOL)isHidden {
    if (isHidden) {
        [self hideCellEditView];
    }

    if (isHidden) {
        AnnotationListCell *cell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:self.indexPath];
        UITextView *edittextview = (UITextView *) [cell.contentView viewWithTag:107];
        [edittextview resignFirstResponder];
    }

    if (self.editAnnoItem) {
        AnnotationListCell *cell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:self.indexPath];
        UITextView *edittextview = (UITextView *) [cell.contentView viewWithTag:107];
        UILabel *labelContents = (UILabel *) [cell.contentView viewWithTag:104];
        [edittextview resignFirstResponder];
        // when text view resign first responder, textViewDidEndEditing will be called
        // to rename attachment, so following code is commented
        //        NSDate *now = [NSDate date];
        //        if (self.editAnnoItem) {
        //            self.editAnnoItem.annot.contents = edittextview.text;
        //            self.editAnnoItem.annot.modifiedDate = now;
        //            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:self.editAnnoItem.annot];
        //            [annotHandler modifyAnnot:self.editAnnoItem.annot];
        //            self.editAnnoItem = nil;
        //        }
        edittextview.hidden = YES;
        labelContents.hidden = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        cell.isInputText = NO;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.allAttachmentsSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *array = [self.allAttachmentsSections objectAtIndex:section];
    return array.count;
}

#pragma mark -  UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.allAttachmentsSections.count == 0) {
        UIView *view = [[UIView alloc] init];
        return view;
    }
    NSArray *array = [self.allAttachmentsSections copy];

    AttachmentItem *attachmentItem = nil;
    if (section < [array count]) {
        attachmentItem = [[array objectAtIndex:section] objectAtIndex:0];
    }

    UIView *subView = [[UIView alloc] init];
    subView.backgroundColor = TabViewSectionColor;
    UILabel *labelSection = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tableView.bounds.size.width - 20, 25)];
    labelSection.font = [UIFont systemFontOfSize:13];
    labelSection.backgroundColor = [UIColor clearColor];
    labelSection.textColor = BlackThemeTextColor;
    NSString *sectionTitle = nil;
    if (attachmentItem && attachmentItem.isDocumentAttachment) {
        sectionTitle = FSLocalizedForKey(@"kDocumentAttachmentTab");
    } else {
        sectionTitle = [NSString stringWithFormat:@"%@ %d", FSLocalizedForKey(@"kPage"), attachmentItem.pageIndex + 1];
    }

    labelSection.text = sectionTitle;

    UILabel *labelTotal = [[UILabel alloc] initWithFrame:CGRectMake(tableView.bounds.size.width - 120, 0, 100, 25)];
    labelTotal.font = [UIFont systemFontOfSize:13];
    labelTotal.textAlignment = NSTextAlignmentRight;
    labelTotal.backgroundColor = [UIColor clearColor];
    labelTotal.textColor = TabViewSectionTextColor;

    if (section < [array count]) {
        labelTotal.text = [NSString stringWithFormat:@"%lu", (unsigned long) [[array objectAtIndex:section] count]];
    }

    [subView addSubview:labelSection];
    [subView addSubview:labelTotal];

    [labelSection mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.left.equalTo(subView.mas_safeAreaLayoutGuideLeft).offset(5);
        } else {
            make.left.equalTo(subView.mas_left).offset(5);
        }
        make.width.mas_equalTo(labelSection.frame.size.width);
        make.top.equalTo(subView.mas_top);
        make.height.mas_equalTo(labelSection.frame.size.height);
    }];
    [labelTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.right.equalTo(subView.mas_safeAreaLayoutGuideRight).offset(-5);
        } else {
            make.right.equalTo(subView.mas_right).offset(-5);
        }
        make.width.mas_equalTo(labelTotal.frame.size.width);
        make.top.equalTo(subView.mas_top);
        make.height.mas_equalTo(labelTotal.frame.size.height);
    }];

    return subView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.allAttachmentsSections.count == 0) {
        return 0;
    }

    NSArray *sectionAttachments = [NSArray array];
    if (indexPath.section < [self.allAttachmentsSections count]) {
        sectionAttachments = [self.allAttachmentsSections objectAtIndex:[indexPath section]];
    }

    AttachmentItem *attachmentItem = nil;
    if (indexPath.row < [sectionAttachments count]) {
        attachmentItem = [sectionAttachments objectAtIndex:[indexPath row]];
    }

    float cellHeight = 68;
    CGSize contentSize = CGSizeMake(0, 0);
    NSString *contents = nil;
    if (attachmentItem && [attachmentItem.description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length != 0) {
        contents = [attachmentItem.description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        contents = [Utility decodeEmojiString:contents];
    }

    if (contents == nil || contents.length == 0) {
        if (self.indexPath && self.indexPath.section == indexPath.section && self.indexPath.row == indexPath.row) {
            contentSize.height = 25;
        }
    } else {
        contentSize = [Utility getTextSize:contents fontSize:13.0 maxSize:CGSizeMake(DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact ? CGRectGetWidth(_pdfViewCtrl.bounds) - 40 : 300 - 40, 2000)];
        if (contentSize.height < 25)
            contentSize.height = 25;
        else
            contentSize.height += 5;
    }
    return cellHeight + contentSize.height;
}

- (AttachmentItem *)getAnnotationItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!indexPath) {
        return nil;
    }
    if (indexPath.section >= self.allAttachmentsSections.count) {
        return nil;
    }
    NSArray *sectionAttachments = [self.allAttachmentsSections objectAtIndex:[indexPath section]];
    if (indexPath.item >= sectionAttachments.count) {
        return nil;
    }
    return [sectionAttachments objectAtIndex:indexPath.item];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"annotationCellIdentifier";

    if (self.allAttachmentsSections.count == 0) {
        AnnotationListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[AnnotationListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier isMenu:NO];
        }
        return cell;
    }

    AttachmentItem *attachmentItem = [self getAnnotationItemAtIndexPath:indexPath];
    AnnotationListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[AnnotationListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier isMenu:NO];
    }
    cell.cellDelegate = self;

    UIImageView *annoimageView = (UIImageView *) [cell.contentView viewWithTag:99];
    annoimageView.frame = CGRectMake(8, 15, 40, 40);
    UIImageView *buttonViewLevel = (UIImageView *) [cell.contentView viewWithTag:100];
    buttonViewLevel.hidden = YES;
    UILabel *labelAuthor = (UILabel *) [cell.contentView viewWithTag:102];
    UILabel *labelDate = (UILabel *) [cell.contentView viewWithTag:103];
    UILabel *labelSize = (UILabel *) [cell.contentView viewWithTag:110];
    UILabel *labelContents = (UILabel *) [cell.contentView viewWithTag:104];
    labelContents.hidden = NO;
    labelSize.hidden = NO;
    labelContents.text = attachmentItem.description;
    UITextView *edittextview = (UITextView *) [cell.contentView viewWithTag:107];
    edittextview.returnKeyType = UIReturnKeyDone;
    UIImageView *annoupdatetip = (UIImageView *) [cell.contentView viewWithTag:108];
    annoupdatetip.hidden = YES;
    UIImageView *annouprepltip = (UIImageView *) [cell.contentView viewWithTag:109];
    //    cell.indexPath = indexPath;
    cell.item = attachmentItem;
    annoimageView.image = [UIImage imageNamed:[Utility getThumbnailName:attachmentItem.filePath] inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    attachmentItem.annosection = indexPath.section;
    attachmentItem.annorow = indexPath.row;

    labelAuthor.text = attachmentItem.fileName;
    labelAuthor.textColor = BlackThemeTextColor;
    [labelAuthor setFont:[UIFont systemFontOfSize:18]];

    labelDate.text = [Utility displayDateInYMDHM:attachmentItem.modifyDate];
    labelSize.text = [Utility displayFileSize:attachmentItem.fileSize];
    
    

    NSString *contents = [attachmentItem.description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    contents = [Utility decodeEmojiString:contents];
    if (contents == nil || contents.length == 0) {
        labelContents.hidden = YES;

    } else {
        labelContents.hidden = NO;
        labelContents.text = contents;
        CGSize contentSize = CGSizeZero;
        labelContents.numberOfLines = 0;

        contentSize = [Utility getTextSize:contents fontSize:13.0 maxSize:CGSizeMake(DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact ? CGRectGetWidth(_pdfViewCtrl.bounds) - 40 : 300 - 40, 2000)];

        [labelContents mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(labelContents.superview.mas_top).offset(69);
            make.left.equalTo(labelContents.superview.mas_left).offset(20);
            make.right.equalTo(labelContents.superview.mas_right).offset(-20);
        }];
        [edittextview mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(labelContents.superview.mas_top).offset(69);
            make.left.equalTo(labelContents.superview.mas_left).offset(20);
            make.right.equalTo(labelContents.superview.mas_right).offset(-20);
            make.height.mas_equalTo(contentSize.height);
        }];
    }

    annoimageView.hidden = NO;
    [labelAuthor setTextAlignment:NSTextAlignmentLeft];
    annouprepltip.hidden = YES;

    [labelContents setTextColor:[UIColor darkGrayColor]];
    if (cell.isInputText) {
        edittextview.hidden = NO;
        labelContents.hidden = YES;
    } else {
        edittextview.hidden = YES;
        if (contents == nil || contents.length == 0) {
            labelContents.hidden = YES;
        } else {
            labelContents.hidden = NO;
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    NSArray *sectionAttachments = [self.allAttachmentsSections objectAtIndex:[indexPath section]];
    AttachmentItem *attachmentItem = [sectionAttachments objectAtIndex:[indexPath row]];

    [self openAttachment:attachmentItem];
}

- (void)updateAllAttachments:(AttachmentItem *)attachmentItem operation:(int)operation {
    @synchronized(self) {
        NSMutableArray *attachments = nil;
        for (NSMutableArray *array in self.allAttachmentsSections) {
            AttachmentItem *item = [array objectAtIndex:0];
            if (attachmentItem.pageIndex == item.pageIndex) {
                attachments = array;
                break;
            }
        }

        if (operation == AnnotationOperation_Add) {
            if (!attachments) {
                attachments = [NSMutableArray array];
                [self.allAttachmentsSections addObject:attachments];
            }
            [attachments addObject:attachmentItem];
        } else if (operation == AnnotationOperation_Modify) {
            int index = -1;
            for (AttachmentItem *tmpItem in attachments) {
                if (attachmentItem.annot) {
                    if ([tmpItem.annot.NM isEqualToString:attachmentItem.annot.NM]) {
                        index = (int) [attachments indexOfObject:tmpItem];
                        break;
                    }
                } else {
                    if ([tmpItem.fileName isEqualToString:attachmentItem.fileName]) {
                        index = (int) [attachments indexOfObject:tmpItem];
                        break;
                    }
                }
            }
            if (index != -1) {
                [attachments replaceObjectAtIndex:index withObject:attachmentItem];
            }
        } else if (operation == AnnotationOperation_Delete) {
            for (AttachmentItem *tmpItem in attachments) {
                if (attachmentItem.annot) {
                    if ([tmpItem.annot.NM isEqualToString:attachmentItem.annot.NM]) {
                        [attachments removeObject:tmpItem];
                        if (attachments.count == 0) {
                            [self.allAttachmentsSections removeObject:attachments];
                        }
                        break;
                    }
                } else {
                    if ([tmpItem.keyName isEqualToString:attachmentItem.keyName]) {
                        [attachments removeObject:tmpItem];
                        if (attachments.count == 0) {
                            [self.allAttachmentsSections removeObject:attachments];
                        }
                        break;
                    }
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self resortAllAttachments];
            [self.tableView reloadData];
        });
    }
}

- (void)openAttachment:(AttachmentItem *)attachmentItem {
    if (!attachmentItem.fileName) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailedOpenAttachmentBadFileName") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *_Nonnull action) {
                                                           [self popoverDidCancel];
                                                       }];
        [alertController addAction:action];
        [self presentAlertController:alertController];
        return;
    }

    if (!attachmentItem.fileSpec) {
        return;
    }
    if ([Utility isSupportFormat:attachmentItem.filePath]) {
        AttachmentController *attachmentCtr = [[AttachmentController alloc] init];
        [attachmentCtr fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];

        [[Utility getTopMostViewController] presentViewController:attachmentCtr
                                         animated:YES
                                       completion:^{
                                           //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                if (![Utility loadFileSpec:attachmentItem.fileSpec toPath:attachmentItem.filePath]) return;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [attachmentCtr openDocument:attachmentItem];
                });
            });
                                       }];
    } else {
        if (![Utility loadFileSpec:attachmentItem.fileSpec toPath:attachmentItem.filePath]) {
            return;
        }
        BOOL isPresent = NO;
        #if !_MAC_CATALYST_
        NSURL *urlFile = [NSURL fileURLWithPath:attachmentItem.filePath isDirectory:NO];
        self.documentPopoverController = [UIDocumentInteractionController interactionControllerWithURL:urlFile];
        self.documentPopoverController.delegate = self;
        if (DEVICE_iPHONE) {
            isPresent = [self.documentPopoverController presentOpenInMenuFromRect:_pdfViewCtrl.frame inView:_pdfViewCtrl animated:YES];
        } else {
            CGRect dvRect = CGRectMake(CGRectGetWidth(_pdfViewCtrl.bounds) / 2, CGRectGetHeight(_pdfViewCtrl.bounds) / 2, 20, 20);
            isPresent = [self.documentPopoverController presentOpenInMenuFromRect:dvRect inView:_pdfViewCtrl animated:YES];
        }
        #endif
        if (!isPresent) {
            NSString *fileName = [attachmentItem.fileSpec getFileName];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:FSLocalizedForKey(@"kFailedOpenAttachment"), fileName] message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)setIsShowMore:(BOOL)isShowMore {
    _isShowMore = isShowMore;
}

- (void)loadAllAnnotAttachments:(NSArray *)annotAttachments {
    NSMutableArray *attachmentItems = [NSMutableArray arrayWithCapacity:annotAttachments.count];
    for (FSFileAttachment *annot in annotAttachments) {
        AttachmentItem *attachmentItem = [AttachmentItem itemWithAttachmentAnnotation:annot];
        attachmentItem.currentlevel = 1;
        attachmentItem.isSecondLevel = YES;
        [attachmentItems addObject:attachmentItem];
    }
    if (attachmentItems.count > 0) {
        [self.allAttachmentsSections addObject:attachmentItems];
    }
}

- (void)loadAllDocumentAttachments:(FSPDFDoc *)document {
    if (![self documentHasNameTree:document]) return;
    NSMutableArray<AttachmentItem *> *attachmentItems = [NSMutableArray<AttachmentItem *> array];
    FSPDFNameTree *nameTree = [[FSPDFNameTree alloc] initWithDocument:document type:FSPDFNameTreeEmbeddedFiles];
    for (int i = 0; i < [nameTree getCount]; i++) {
        NSString *name = [nameTree getName:i];
        FSPDFObject *dict = [nameTree getObj:name];
        if (dict && dict.getType == FSPDFObjectDictionary) {
            FSFileSpec *file = [[FSFileSpec alloc] initWithDocument:document pdf_object:dict];
            if (file) {
                AttachmentItem *attachmentItem = [AttachmentItem itemWithDocumentAttachment:name file:file PDFPath:_pdfViewCtrl.filePath];
                attachmentItem.currentlevel = 1;
                attachmentItem.isSecondLevel = YES;
                [attachmentItems addObject:attachmentItem];
            }
        }
    }
    if (attachmentItems.count > 0) {
        [self.allAttachmentsSections addObject:attachmentItems];
    }
}

- (BOOL)documentHasNameTree:(FSPDFDoc *)document{
    @try {
        FSPDFDictionary *catalog = document.getCatalog;
        return [catalog hasKey:@"Names"];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    return NO;
}

- (void)resortAllAttachments {
    if (self.allAttachmentsSections.count > 1) {
        [self.allAttachmentsSections sortUsingComparator:^NSComparisonResult(NSArray *obj1, NSArray *obj2) {
            AttachmentItem *attachmentItem1 = [obj1 objectAtIndex:0];
            AttachmentItem *attachmentItem2 = [obj2 objectAtIndex:0];
            if (attachmentItem2.isDocumentAttachment) {
                return NSOrderedAscending;
            } else if (attachmentItem1.isDocumentAttachment) {
                return NSOrderedDescending;
            } else {
                return attachmentItem1.pageIndex > attachmentItem2.pageIndex;
            }
        }];
    }
}

#pragma mark <AnnotationListCellDelegate>

- (void)annotationListCellWillShowEditView:(AnnotationListCell *)cell {
    [self hideCellEditView];
}

- (void)annotationListCellDidShowEditView:(AnnotationListCell *)cell {
    self.isShowMore = YES;
    self.moreIndexPath = [self.tableView indexPathForCell:cell];
    if (!self.tapGesture) {
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self.view addGestureRecognizer:self.tapGesture];
    }
    self.tapGesture.enabled = YES;
}


- (BOOL)annotationListCellCanDescript:(AnnotationListCell *)cell {
    AnnotationItem *item = cell.item; // ?: [self getAnnotationItemAtIndexPath:[self.tableView indexPathForCell:cell]];
    return item.annot ? item.annot.canEdit : [Utility canModifyContents:_pdfViewCtrl];
}

- (BOOL)annotationListCellCanDelete:(AnnotationListCell *)cell {
    if ([_extensionsManager.config.tools containsObject:Tool_Multiple_Selection] && cell.item.annot.canGetGroupHeader) return NO;
    AnnotationItem *item = cell.item; // ?: [self getAnnotationItemAtIndexPath:[self.tableView indexPathForCell:cell]];
    assert(item);
    return item.annot ? item.annot.canEdit: [Utility canModifyContents:_pdfViewCtrl];
}

- (BOOL)annotationListCellCanSave:(AnnotationListCell *)cell {
    AttachmentItem *attachmentItem = (AttachmentItem *)cell.item;
    if (attachmentItem.isDocumentAttachment) {
        return [Utility canExtractContents:_pdfViewCtrl];
    }
//    return [Utility canExtractContents:_pdfViewCtrl];
    return [Utility canCopyForAssess:_pdfViewCtrl];
}

- (BOOL)annotationListCellCanFlatten:(AnnotationListCell *)cell {
    if ([_extensionsManager.config.tools containsObject:Tool_Multiple_Selection] && cell.item.annot.canGetGroupHeader) return NO;
    AnnotationItem *item = cell.item; // ?: [self getAnnotationItemAtIndexPath:[self.tableView indexPathForCell:cell]];
    if ([item isKindOfClass:[AttachmentItem class]]) {
        if (![Utility canAddAnnot:_pdfViewCtrl]) return NO;
        return !((AttachmentItem *)item).isDocumentAttachment;
    }
    return NO;
}


- (void)annotationListCellDescript:(AnnotationListCell *)cell {
    [self hideCellEditView];
    [self addNoteToAnnotation:cell.item withIndexPath:[self.tableView indexPathForCell:cell]];
}

- (void)annotationListCellDelete:(AnnotationListCell *)cell {
    [self hideCellEditView];
    [self deleteAnnotation:cell.item];
}

- (void)annotationListCellSave:(AnnotationListCell *)cell {
    [self hideCellEditView];
    [self saveAttachment:cell.item];
}

- (void)annotationListCellFlatten:(AnnotationListCell *)cell {
    [self hideCellEditView];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFlatten") message:FSLocalizedForKey(@"kFlattenTip") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *flattenAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFlatten") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([cell.item.annot isEmpty]) {
            return;
        }
        FSPDFPage *page = [cell.item.annot getPage];
        FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:cell.item.annot];
        
        if (self->_extensionsManager.currentAnnot) {
            [self->_extensionsManager setCurrentAnnot:nil];
        }
        @try {
            BOOL flag = [page flattenAnnot:cell.item.annot];
            if (flag) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAnnotLsitUpdate object:nil];
                [self reloadDataList:nil];
                int pageIndex = [page getIndex];
                [self->_pdfViewCtrl refresh:pageIndex needRender:YES];
                self->_extensionsManager.isDocModified = YES;
                NSArray *undoItems = [UndoItem itemForUndoItemsWithAttributes:attributes extensionsManager:self->_extensionsManager replyAttributes:nil];
                for (UndoItem *item in undoItems) {
                    [self->_extensionsManager removeUndoItem:item];
                }
            }
        } @catch (NSException *exception) {
        }
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:flattenAction];
    [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
}

- (void)reloadDataList:(NSNotification *)not{
    [self clearData];
    [self loadData];
}

#pragma mark

- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
    assert(self.isShowMore);
    assert(self.moreIndexPath);
    [self hideCellEditView];
}

- (void)hideCellEditView {
    if (self.isShowMore) {
        assert(self.moreIndexPath);
        AnnotationListCell *cell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:self.moreIndexPath];
        [cell setEditViewHidden:YES];
        self.isShowMore = NO;
        self.moreIndexPath = nil;
        self.tapGesture.enabled = NO;
    }
}

#pragma mark - methods

- (void)clearData {
    if (self.loadAttachmentQueue) {
        [self.loadAttachmentQueue cancelAllOperations];
        [self.loadAttachmentQueue waitUntilAllOperationsAreFinished];
    }
    [self.allAttachmentsSections removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    self.indexPath = nil;
}

- (void)loadData {
    if ([_pdfViewCtrl isDynamicXFA]) {
        return;
    }
    
    if (!self.loadAttachmentQueue) {
        self.loadAttachmentQueue = [[NSOperationQueue alloc] init];
        self.loadAttachmentQueue.qualityOfService = QOS_CLASS_UTILITY;

    }
    FSPDFDoc *doc = _pdfViewCtrl.currentDoc;
    typeof(self) __weak weakSelf = self;

    //load attachment annotations
    int pageCount = 0;
    @try {
        pageCount = [doc getPageCount];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return;
    }
    NSBlockOperation *op = [[NSBlockOperation alloc] init];
    typeof(op) __weak weakOp = op;
    if (_extensionsManager.config.loadAttachment) {
        const int pagesPerExecution = 100;
        for (int pageStart = 0; pageStart < pageCount; pageStart += pagesPerExecution) {
            [op addExecutionBlock:^{
                @autoreleasepool {
                    for (int pageIndex = pageStart; pageIndex < pageStart + pagesPerExecution && pageIndex < pageCount; pageIndex++) {
                        if (weakOp.isCancelled) {
                            return;
                        }
                        FSPDFPage *page = nil;
                        @try {
                            page = [doc getPage:pageIndex];
                        } @catch (NSException *exception) {
                            continue;
                        }
                        @try {
                            NSArray *annots = [Utility getAnnotationsOfType:FSAnnotFileAttachment inPage:page];
                            if (annots.count > 0) {
                                [self loadAllAnnotAttachments:annots];
                            }
                        } @catch (NSException *exception) {
                        }
                    }
                }
            }];
        }
    }

    op.completionBlock = ^{
        if (weakOp.isCancelled) {
            return;
        }
        //load document attachments
        @try {
            [weakSelf loadAllDocumentAttachments:doc];
        } @catch (NSException *exception) {
            return;
        }
        [weakSelf resortAllAttachments];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    };
    [self.loadAttachmentQueue addOperation:op];
}

// attachment could be a attachment annotation or a document attachment
- (void)deleteAnnotation:(AttachmentItem *)item {
    if (self.editAnnoItem) {
        AnnotationListCell *cell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:self.indexPath];
        UITextView *edittextview = (UITextView *) [cell.contentView viewWithTag:107];
        UILabel *labelContents = (UILabel *) [cell.contentView viewWithTag:104];
        [self tryRenameAttachment:self.editAnnoItem newName:edittextview.text addUndo:!self.editAnnoItem.isDocumentAttachment];
        self.editAnnoItem = nil;

        edittextview.hidden = YES;
        labelContents.hidden = NO;
        [edittextview resignFirstResponder];
        cell.isInputText = NO;
    }

    if (item.isDocumentAttachment) {
        [self _deleteDocumentAttachment:item addUndo:NO];
    } else {
        assert(item.annot);
        id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:item.annot];
        [annotHandler removeAnnot:item.annot];
        _extensionsManager.currentAnnot = nil;
    }
}

- (void)_deleteDocumentAttachment:(AttachmentItem *)attachmentItem addUndo:(BOOL)addUndo {
    FSPDFNameTree *nameTree = [[FSPDFNameTree alloc] initWithDocument:_pdfViewCtrl.currentDoc type:FSPDFNameTreeEmbeddedFiles];
    NSString *name = attachmentItem.keyName;
    if ([nameTree hasName:name]) {
        [nameTree removeObj:name];
        _extensionsManager.isDocModified = YES;
    } else {
        // should never reach here
        assert(NO);
        return;
    }
    [self onDocumentAttachmentDeleted:attachmentItem];

    if (addUndo) {
        [_extensionsManager addUndoItem:[UndoItem itemWithUndo:^(UndoItem *item) {
                                if (![nameTree hasName:name]) {
                                    FSFileSpec *fileSpec = [[FSFileSpec alloc] initWithDocument:self->_extensionsManager.pdfViewCtrl.currentDoc];
                                    [fileSpec setFileName:attachmentItem.fileName];
                                    assert(attachmentItem.filePath);
                                    [fileSpec embed:attachmentItem.filePath];
                                    if ([nameTree add:name pdf_object:[fileSpec getDict]]) {
                                        [self onDocumentAttachmentAdded:attachmentItem];
                                    }
                                }
                            }
                                            redo:^(UndoItem *item) {
                                                [self _deleteDocumentAttachment:attachmentItem addUndo:NO];
                                            }
                                            pageIndex:-1]];
    }
}

- (void)saveAttachment:(AttachmentItem *)item {
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_SaveTo;
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:^{
            self->_moveFileAlertCheckType = MoveFileAlertCheckType_Ask;
            [self copyFile:item toFolder:[destinationFolder objectAtIndex:0]];
        }];
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        self->_moveFileAlertCheckType = MoveFileAlertCheckType_Cancel;
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    selectDestinationNavController.delegate = selectDestination;
    [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    [[Utility getTopMostViewController] presentViewController:selectDestinationNavController animated:YES completion:nil];
}

- (void)copyFile:(AttachmentItem *)item toFolder:(NSString *)toFolder {
    if (!item.fileName || [item.fileName isEqualToString:@""]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailedSaveAttachmentBadFileName") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
        [alertController addAction:action];
        [self presentAlertController:alertController];
        return;
    }
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *srcFilePath = item.filePath;
    BOOL isDir = YES;
    if (![fileManager fileExistsAtPath:toFolder isDirectory:&isDir] || !isDir) {
        return;
    }
    NSString *destFilePath = [toFolder stringByAppendingPathComponent:item.fileName];
    if ([fileManager fileExistsAtPath:destFilePath]) {
        NSString *msg = [NSString stringWithFormat:FSLocalizedForKey(@"kSureToReplaceFile"), item.fileName];

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cacelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                              style:UIAlertActionStyleCancel
                                                            handler:^(UIAlertAction *_Nonnull action) {
                                                                self->_moveFileAlertCheckType = MoveFileAlertCheckType_Cancel;
                                                                self->_alertViewFinished = YES;
                                                                //                                                                [self popoverDidCancel];
                                                            }];
        UIAlertAction *replaceAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *_Nonnull action) {
                                                                  self->_moveFileAlertCheckType = MoveFileAlertCheckType_Replace;
                                                                  self->_alertViewFinished = YES;
                                                              }];
        [alertController addAction:cacelAction];
        [alertController addAction:replaceAction];
        [self presentAlertController:alertController];

        if (_moveFileAlertCheckType == MoveFileAlertCheckType_Cancel) {
            return;
        }
    }

    [fileManager removeItemAtPath:destFilePath error:nil];

    NSError *error = nil;
    BOOL isOK = YES;
    if ([fileManager fileExistsAtPath:srcFilePath isDirectory:nil]) {
        isOK = [fileManager copyItemAtPath:srcFilePath toPath:destFilePath error:&error];
    } else {
        isOK = [Utility loadFileSpec:item.fileSpec toPath:destFilePath];
    }
    if (!isOK) {
        NSString *msg = [NSString stringWithFormat:FSLocalizedForKey(@"kCopyFileFailed"), item.fileName, error.localizedDescription];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(msg) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *_Nonnull action) {
                                                           [self popoverDidCancel];
                                                       }];
        [alertController addAction:action];
        [self presentAlertController:alertController];
    }
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (alertView.tag == 201) {
//        if (buttonIndex == 0) //cancel
//        {
//            _moveFileAlertCheckType = MoveFileAlertCheckType_Cancel;
//        } else if (buttonIndex == 1) //replace
//        {
//            _moveFileAlertCheckType = MoveFileAlertCheckType_Replace;
//        }
//        _alertViewFinished = YES;
//    }
//}

- (void)addNoteToAnnotation:(AttachmentItem *)item withIndexPath:(NSIndexPath *)indexPath {
    _extensionsManager.isDocModified = YES;
    if (self.editAnnoItem) {
        AnnotationListCell *cell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:self.indexPath];
        UITextView *edittextview = (UITextView *) [cell.contentView viewWithTag:107];
        UILabel *labelContents = (UILabel *) [cell.contentView viewWithTag:104];
        [self tryRenameAttachment:self.editAnnoItem newName:edittextview.text addUndo:!self.editAnnoItem.isDocumentAttachment];
        self.editAnnoItem = nil;
        edittextview.hidden = YES;
        labelContents.hidden = NO;
        [edittextview resignFirstResponder];
        cell.isInputText = NO;
    }

    self.editAnnoItem = item;
    self.indexPath = indexPath;
    [self.tableView reloadData];
    AnnotationListCell *selectcell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    selectcell.isInputText = YES;
    UITextView *edittextview = (UITextView *) [selectcell.contentView viewWithTag:107];
    edittextview.delegate = self;
    edittextview.hidden = NO;

    UILabel *labelContents = (UILabel *) [selectcell.contentView viewWithTag:104];
    labelContents.hidden = YES;

    edittextview.text = labelContents.text;
    [edittextview scrollsToTop];
    [edittextview performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.3];
}

#pragma mark--- TextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        if (self.editAnnoItem) {
            [self tryRenameAttachment:self.editAnnoItem newName:textView.text addUndo:!self.editAnnoItem.isDocumentAttachment];
            self.editAnnoItem = nil;
        }

        UITableViewCell *selectcell = [self.tableView cellForRowAtIndexPath:self.indexPath];
        UITextView *edittextview = (UITextView *) [selectcell.contentView viewWithTag:107];
        edittextview.hidden = YES;
        UILabel *labelContents = (UILabel *) [selectcell.contentView viewWithTag:104];
        labelContents.hidden = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.editAnnoItem) {
        [self tryRenameAttachment:self.editAnnoItem newName:textView.text addUndo:!self.editAnnoItem.isDocumentAttachment];
        self.editAnnoItem = nil;
    }

    AnnotationListCell *selectcell = (AnnotationListCell *) [self.tableView cellForRowAtIndexPath:self.indexPath];
    UITextView *edittextview = (UITextView *) [selectcell.contentView viewWithTag:107];
    edittextview.hidden = YES;
    UILabel *labelContents = (UILabel *) [selectcell.contentView viewWithTag:104];
    labelContents.hidden = NO;
    self.indexPath = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    [textView resignFirstResponder];
    selectcell.isInputText = NO;
}

- (void)tryRenameAttachment:(AttachmentItem *)attachmentItem newName:(NSString *)newName addUndo:(BOOL)addUndo {
    if (attachmentItem.annot) {
        FSFileAttachment *annot = [[FSFileAttachment alloc] initWithAnnot:attachmentItem.annot];
        NSString *oldName = annot.contents;
        if (![newName isEqualToString:oldName]) {
            attachmentItem.annot.contents = [Utility encodeEmojiString:newName];
            NSDate *oldModifiedDate = annot.modifiedDate;
            NSDate *newModifiedDate = [NSDate date];
            annot.modifiedDate = newModifiedDate;
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
            [annotHandler modifyAnnot:annot addUndo:NO];

            //add undo item
            if (addUndo) {
                FSPDFPage *page = [annot getPage];
                NSString *NM = annot.NM;
                [_extensionsManager addUndoItem:[UndoItem itemWithUndo:^(UndoItem *item) {
                                        FSFileAttachment *annot = [[FSFileAttachment alloc] initWithAnnot:[Utility getAnnotByNM:NM inPage:page]];
                                        if (annot && ![annot isEmpty]) {
                                            annot.contents = [Utility encodeEmojiString:oldName];
                                            
                                            annot.modifiedDate = oldModifiedDate;
                                            [annotHandler modifyAnnot:annot addUndo:NO];
                                        }
                                    }
                                                    redo:^(UndoItem *item) {
                                                        FSFileAttachment *annot = [[FSFileAttachment alloc] initWithAnnot:[Utility getAnnotByNM:NM inPage:page]];
                                                        if (annot && ![annot isEmpty]) {
                                                            annot.contents = [Utility encodeEmojiString:newName];
                                                            annot.modifiedDate = newModifiedDate;
                                                            [annotHandler modifyAnnot:annot addUndo:NO];
                                                        }
                                                    }
                                                    pageIndex:annot.pageIndex]];
            }
        }
    } else {
        NSString *oldDescription = attachmentItem.description;
        if (newName && [newName length] && ![newName isEqualToString:oldDescription]) {
            NSDate *oldModifiedDate = attachmentItem.modifyDate;
            NSDate *newModifiedDate = [NSDate date];
            @try {
                [attachmentItem.fileSpec setDescription:[Utility encodeEmojiString:newName]];
            } @catch (NSException *exception) {
                NSString *msg = [NSString stringWithFormat:FSLocalizedForKey(@"kFailedChangeDocumentAttachmentDescription"), exception.description];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(msg) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction *_Nonnull action) {
                                                                   [self popoverDidCancel];
                                                               }];
                [alertController addAction:action];
                [self presentAlertController:alertController];
                return;
            }
            attachmentItem.description = [Utility encodeEmojiString:newName];
            [attachmentItem.fileSpec setModifiedDateTime:[Utility convert2FSDateTime:newModifiedDate]];
            [self onDocumentAttachmentModified:attachmentItem];

            if (addUndo) {
                [_extensionsManager addUndoItem:[UndoItem itemWithUndo:^(UndoItem *item) {
                                        attachmentItem.description = oldDescription;
                                        [attachmentItem.fileSpec setDescription:oldDescription];
                                        [attachmentItem.fileSpec setModifiedDateTime:[Utility convert2FSDateTime:oldModifiedDate]];
                                        [self onDocumentAttachmentModified:attachmentItem];
                                    }
                                                    redo:^(UndoItem *item) {
                                                        attachmentItem.description = newName;
                                                        [attachmentItem.fileSpec setDescription:newName];
                                                        [attachmentItem.fileSpec setModifiedDateTime:[Utility convert2FSDateTime:newModifiedDate]];
                                                        [self onDocumentAttachmentModified:attachmentItem];
                                                    }
                                                    pageIndex:-1]];
            }
        }
    }
}

#pragma mark - Private methods

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark-- keyboard
- (void)keyboardDidShow:(NSNotification *)note {
    [self.tableView scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    self.indexPath = nil;
    if (error == FSErrSuccess && document && ![document isWrapper]) {
        [self loadData];
    }
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    [self clearData];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.tableView.frame = CGRectMake(0, 0, self.tableView.superview.frame.size.width, self.tableView.superview.frame.size.height);
    });
    
}

#pragma mark IPageEventListener

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    [self clearData];
    [self loadData];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    [self clearData];
    [self loadData];
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    if (self.loadAttachmentQueue) {
        [self.loadAttachmentQueue cancelAllOperations];
        [self.loadAttachmentQueue waitUntilAllOperationsAreFinished];
    }
    [self.allAttachmentsSections removeAllObjects];

    self.indexPath = nil;

    [self loadData];
}

#pragma mark popover

- (void)presentAlertController:(UIAlertController *)alertController {
    UIViewController *rootVC = self;
    self.originalPresentedViewController = rootVC.presentedViewController;
    if (self.originalPresentedViewController) {
        [self.originalPresentedViewController dismissViewControllerAnimated:true
                                                                 completion:^{
                                                                     [rootVC presentViewController:alertController animated:YES completion:nil];
                                                                 }];
    } else {
        [rootVC presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)popoverDidCancel {
    if (self.originalPresentedViewController) {
        UIViewController *rootVC = self;
        [rootVC presentViewController:self.originalPresentedViewController animated:true completion:nil];
        self.originalPresentedViewController = nil;
    }
}
@end
