/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <FoxitRDK/FSPDFViewControl.h>

typedef void (^DigitalSignatureEditHandle)(UIButton *button);
typedef void (^DigitalSignatureViewHandle)(UIButton *button);
typedef void (^DigitalSignatureVerifyHandle)(UIButton *button);
typedef void (^DigitalSignatureSignHandle)(UIButton *button);

/**@brief A cell on the DigitalSignature list. */
@interface DigitalSignaturePanelCell : UITableViewCell

@property (copy, nonatomic) DigitalSignatureEditHandle editHandle;
@property (copy, nonatomic) DigitalSignatureViewHandle viewHandle;
@property (copy, nonatomic) DigitalSignatureVerifyHandle verifyHandle;
@property (copy, nonatomic) DigitalSignatureSignHandle signHandle;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
-(void)reloadData:(NSMutableDictionary *)cellData;
-(void)setMoreBtnViewHidden:(BOOL)isHidden animated:(BOOL)animated;
@end
