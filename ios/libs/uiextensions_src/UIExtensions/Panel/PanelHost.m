/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PanelHost.h"
#import "PanelController.h"


@implementation PanelHost

- (instancetype)initWithFrame:(CGSize)size {
    self = [super init];
    if (self) {
        self.specs = [[NSMutableArray alloc] init];
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        self.contentView.autoresizesSubviews = YES;
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self loadSegmentView];
    }
    return self;
}

- (void)loadSegmentView {
    if (self.segmentView) {
        [self.segmentView removeFromSuperview];
    }
    self.segmentView = [[SegmentView alloc] initWithFrame:CGRectMake(10, 60, (self.contentView.bounds.size.width - 20), 40)];
    self.segmentView.delegate = self;
    self.segmentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.contentView addSubview:self.segmentView];
    
    NSMutableArray<SegmentItem*>* items =  [[NSMutableArray<SegmentItem*> alloc] init];
    for(id<IPanelSpec> spec in self.specs) {
        SegmentItem* item = [spec getSegmentItem];
        [items addObject:item];
    }
    [self.segmentView loadWithItems:items];
    if (items.count > 0) {
        [self.segmentView setSelectItem:[items objectAtIndex:0]];
    }
}

- (void)reloadSegmentView {
    [self loadSegmentView];
    if (self.specs.count >0) {
        UIView *topToolbar = [[self.specs firstObject] getTopToolbar];
        topToolbar.backgroundColor = UIColor_DarkMode(topToolbar.backgroundColor, ThemeBarColor_Dark);
        
        [self.segmentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(10);
            make.right.equalTo(self.contentView.mas_right).offset(-10);
            make.bottom.equalTo(topToolbar.mas_bottom).offset(0);
            make.height.mas_equalTo(40);
        }];
    }else{
        self.segmentView.hidden = YES;
    }
}

#pragma mark <SegmentDelegate>

- (void)itemClickWithItem:(SegmentItem *)item
{
    assert(item.tag != 0);
    for (id<IPanelSpec> spec in self.specs) {
        if ([spec getType] == item.tag) {
            [self setCurrentSpec:spec];
            break;
        }
    }
}

- (void)updateSpec:(id<IPanelSpec>)spec  {
    UIView *topToolbar = [spec getTopToolbar];
    
    topToolbar.frame = CGRectMake(topToolbar.frame.origin.x, topToolbar.frame.origin.y, DEVICE_iPHONE ? ([spec getContentView].bounds.size.width) : 300, topToolbar.frame.size.height);
    [spec getContentView].frame = CGRectMake([spec getContentView].frame.origin.x, [spec getContentView].frame.origin.y, DEVICE_iPHONE ? ([spec getContentView].bounds.size.width) : 300, [spec getContentView].frame.size.height);
    
    [self.contentView insertSubview:topToolbar belowSubview:self.segmentView];
    //    [self.contentView insertSubview:[spec getContentView] belowSubview:self.segmentView];
    [topToolbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(0);
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.contentView.mas_safeAreaLayoutGuideTop).offset(-24);
        } else {
            make.top.equalTo(self.contentView.mas_top).offset(0);
        }
        
        make.height.mas_equalTo(@107);
    }];
    
    //    [[spec getContentView] mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.equalTo(self.contentView.mas_left).offset(0);
    //        make.right.equalTo(self.contentView.mas_right).offset(0);
    //        make.top.equalTo(topToolbar.mas_bottom).offset(0);
    //        make.bottom.equalTo(self.contentView.mas_bottom).offset(0);
    //    }];
    
    [topToolbar setHidden:YES];
    [[spec getContentView] setHidden:YES];
    
    [self.segmentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(topToolbar.mas_bottom).offset(0);
        make.height.mas_equalTo(40);
    }];
}

- (void)addSpec:(id<IPanelSpec>)spec {
    [self.specs addObject:spec];
    [self updateSpec:spec];
}

- (void)insertSpec:(id<IPanelSpec>)spec atIndex:(int)index {
    if(index > self.specs.count)
        [self.specs addObject:spec];
    else
        [self.specs insertObject:spec atIndex:index];
    [self updateSpec:spec];
}

- (void)removeSpec:(id<IPanelSpec>)spec {
    if (spec == self.currentSpec) {
        self.currentSpec = ({
            id<IPanelSpec> firstOtherPanel = nil;
            for (id<IPanelSpec> panel in self.specs) {
                if (panel != spec) {
                    firstOtherPanel = panel;
                    break;
                }
            }
            firstOtherPanel;
        });
    }
    [[spec getTopToolbar] removeFromSuperview];
    [[spec getContentView] removeFromSuperview];
    [self.specs removeObject:spec];
}

- (void)setCurrentSpec:(id<IPanelSpec>)currentSpec {
    //todel
    //    if (currentSpec == self.currentSpec) {
    //        return;
    //    }
    if ([self.specs containsObject:_currentSpec]) {
        [[_currentSpec getTopToolbar] setHidden:YES];
        [[_currentSpec getContentView] setHidden:YES];
        [[_currentSpec getContentView] removeFromSuperview];
        [self.currentSpec onDeactivated];
    }
    _currentSpec = currentSpec;
    [[_currentSpec getTopToolbar] setHidden:NO];
    [[_currentSpec getContentView] setHidden:NO];
    [self.contentView insertSubview:[_currentSpec getContentView] belowSubview:self.segmentView];
    [_currentSpec onActivated];

    [[_currentSpec getContentView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(0);
        make.top.equalTo([self->_currentSpec getTopToolbar].mas_bottom).offset(0);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(0);
    }];
}

- (UIView *)contentView {
    return _contentView;
}

@end
