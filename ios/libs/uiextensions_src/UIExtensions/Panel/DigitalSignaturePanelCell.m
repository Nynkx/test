/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
#import "DigitalSignaturePanelCell.h"
#import "AnnotationListMore.h"


@interface DigitalSignaturePanelCell ()

@property (nonatomic, strong) UIButton *moreBtn;
@property (nonatomic, strong) UIButton *viewBtn;
@property (nonatomic, strong) UIButton *verifyBtn;
@property (nonatomic, strong) UIButton *signBtn;

@property (nonatomic, strong) UIView *moreView;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) NSMutableDictionary *cellData;
@property (nonatomic, assign) float moreEditViewWidth;

@end

@implementation DigitalSignaturePanelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        NSString *fieldTitle = @"";
        NSString *fieldDetail = @"";
        
        self.textLabel.text = fieldTitle;
        self.textLabel.font = [UIFont systemFontOfSize:13.0f];
        
        self.detailTextLabel.text = fieldDetail;
        self.detailTextLabel.font = [UIFont systemFontOfSize:8.0f];
        [self.detailTextLabel setTextColor:[UIColor darkGrayColor]];
        
        UIImage *cellImg = [UIImage imageNamed:@"panel_digitalsig_logo" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.imageView.image = cellImg;
        
        self.moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreBtn setImage:[UIImage imageNamed:@"document_cellmore_more" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.contentView addSubview:_moreBtn];
        
        [_moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];

        __weak typeof(self) weakSelf = self;
        [_moreBtn setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.editHandle(sender);
            
            [strongSelf setMoreBtnViewHidden:NO animated:YES];
            
            if (!strongSelf.tapGesture) {
                strongSelf.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:strongSelf action:@selector(handleTap:)];
                [strongSelf.contentView addGestureRecognizer:strongSelf.tapGesture];
            }
            strongSelf.tapGesture.enabled = YES;
        }];
        
        self.moreEditViewWidth = 0;
    }
    return self;
}

-(void)reloadData:(NSMutableDictionary *)cellData {
    _cellData = cellData;

    NSString *fieldTitle = @"";
    NSString *fieldDetail = @"";
    
    if ([[cellData objectForKey:@"isSigned"] boolValue]) {
        NSString *version = @"";//[NSString stringWithFormat:FSLocalizedForKey(@"kDigitalSignatureSignedTitleVersion"), [[cellData objectForKey:@"signIndex"] intValue]];
        NSString *signer = [NSString stringWithFormat:FSLocalizedForKey(@"kDigitalSignatureSignedTitleSigner"), [cellData objectForKey:@"signer"]];
        fieldTitle = [NSString stringWithFormat:@"%@%@",version,signer];
        
        FSDateTime *dateTime = [cellData objectForKey:@"signdate"];
        NSDate *signdate = [Utility convertFSDateTime2NSDate:dateTime];
        NSString *signdateString = [Utility displayDateInYMDHM:signdate];
        
        fieldDetail = signdateString;
        
        UIImage *cellImg = [UIImage imageNamed:@"panel_digitalsig_logo_signed" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.imageView.image = cellImg;
    }else{
        fieldTitle = FSLocalizedForKey(@"kDigitalSignatureUnsigned");
        fieldDetail = [cellData objectForKey:@"signName"];
        
        UIImage *cellImg = [UIImage imageNamed:@"panel_digitalsig_logo_unsigned" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.imageView.image = cellImg;
    }
    self.textLabel.text = fieldTitle;
    self.detailTextLabel.text = fieldDetail;
    
    [self updateMoreView];
//    [self setMoreBtnViewHidden:YES animated:NO];
}

- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
    self.tapGesture.enabled = NO;
    [self setMoreBtnViewHidden:YES animated:YES];
}

- (void)updateMoreView{
    if (self.moreView) {
        [self.moreView removeFromSuperview];
        self.moreView = nil;
    }
    
    self.moreView = [[UIView alloc] initWithFrame:CGRectZero];
    self.moreView.backgroundColor = OptionMoreColor;
    [self.contentView addSubview:self.moreView];
    
    [self.moreView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSMutableArray<UIButton *> *buttons = [NSMutableArray<UIButton *> array];
    
    BOOL canView = YES;
    BOOL canVerify = YES;
    BOOL canSign = YES;

    if ([[_cellData objectForKey:@"isSigned"] boolValue]) {
        canView = YES;
        canVerify = YES;
        canSign = NO;
    }else{
        canView = NO;
        canVerify = NO;
        canSign = YES;
    }
    
    if (canView) {
        self.viewBtn = [Utility createButtonWithImageAndTitle:FSLocalizedForKey(@"kDigitalSignatureBtnView")
                                                  imageNormal:[UIImage imageNamed:@"panel_digitalsig_view" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                imageSelected:[UIImage imageNamed:@"panel_digitalsig_view" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                 imageDisable:[UIImage imageNamed:@"panel_digitalsig_view" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        
        [buttons addObject:self.viewBtn];
    }
    
    if (canVerify) {
        self.verifyBtn = [Utility createButtonWithImageAndTitle:FSLocalizedForKey(@"kDigitalSignatureBtnVerify")
                                                    imageNormal:[UIImage imageNamed:@"panel_digitalsig_verify" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                  imageSelected:[UIImage imageNamed:@"panel_digitalsig_verify" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                   imageDisable:[UIImage imageNamed:@"panel_digitalsig_verify" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        [buttons addObject:self.verifyBtn];
    }
    
    if (canSign) {
        self.signBtn = [Utility createButtonWithImageAndTitle:FSLocalizedForKey(@"kSignAction")
                                                  imageNormal:[UIImage imageNamed:@"panel_digitalsig_sign" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                imageSelected:[UIImage imageNamed:@"panel_digitalsig_sign" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                                 imageDisable:[UIImage imageNamed:@"panel_digitalsig_sign" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        [buttons addObject:self.signBtn];
        
        if ([[_cellData objectForKey:@"readOnly"] boolValue]) {
            self.signBtn.enabled = NO;
            [self.signBtn setTitleColor:[UIColor colorWithRGB:0x5c5c5c] forState:UIControlStateNormal];
        }else{
            self.signBtn.enabled = YES;
            [self.signBtn setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
        }
    }
    
    const CGFloat minButtonWidth = 50.0f;
    
    if (buttons.count > 1) {
        [buttons enumerateObjectsUsingBlock:^(UIButton *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            [self.moreView addSubview:obj];
            [obj addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        }];
        
        [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10.0f leadSpacing:10.0f tailSpacing:10.0f];
        
        [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.moreView.mas_centerY);
        }];
    }else{
        [buttons enumerateObjectsUsingBlock:^(UIButton *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            [self.moreView addSubview:obj];
            [obj mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(obj.superview).offset(10);
                make.right.mas_equalTo(obj.superview).offset(-10);
                make.top.bottom.mas_equalTo(obj.superview);
            }];
            [obj addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        }];
    }
    
    self.moreEditViewWidth = buttons.count * minButtonWidth + 10 * (buttons.count - 1);
    
    [self.moreView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.contentView.mas_right).offset(50);
        make.right.mas_equalTo(self.contentView).offset(self.contentView.frame.size.width);
        make.top.bottom.mas_equalTo(self.contentView);
        make.width.mas_equalTo(self.moreEditViewWidth);
    }];

    [buttons removeAllObjects];
    buttons = nil;
//    self.moreView.hidden = YES;
}

- (void)handleClick:(UIButton *)button {
    self.tapGesture.enabled = NO;
    [self setMoreBtnViewHidden:YES animated:YES];
    if (button == self.viewBtn) {
        self.viewHandle(button);
    } else if (button == self.verifyBtn) {
        self.verifyHandle(button);
    } else if (button == self.signBtn) {
        self.signHandle(button);
    }
}

-(void)setMoreBtnViewHidden:(BOOL)isHidden animated:(BOOL)animated {
    [self.contentView setNeedsUpdateConstraints];
    [self.contentView updateConstraintsIfNeeded];
    
    if (isHidden) {
        if (animated) {
            [UIView animateWithDuration:.3 animations:^{
                [self.moreView mas_remakeConstraints:^(MASConstraintMaker *make) {
//                    make.left.mas_equalTo(self.contentView.mas_right);
                    make.right.mas_equalTo(self.contentView).offset(self.contentView.frame.size.width);
                    make.top.bottom.mas_equalTo(self.contentView);
                    make.width.mas_equalTo(self.moreEditViewWidth);
                }];
                [self.contentView layoutIfNeeded];
            }];
        }else{
            [self.moreView mas_remakeConstraints:^(MASConstraintMaker *make) {
                //                    make.left.mas_equalTo(self.contentView.mas_right);
                make.right.mas_equalTo(self.contentView).offset(self.contentView.frame.size.width);
                make.top.bottom.mas_equalTo(self.contentView);
                make.width.mas_equalTo(self.moreEditViewWidth);
            }];
        }
    }
    else {
        if (animated) {
            [UIView animateWithDuration:0.3 animations:^{
                [self.moreView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(self.contentView);
                    make.top.bottom.mas_equalTo(self.contentView);
                    make.width.mas_equalTo(self.moreEditViewWidth);
                }];
                [self.contentView layoutIfNeeded];
            }];
        }else{
            [self.moreView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.contentView);
                make.top.bottom.mas_equalTo(self.contentView);
                make.width.mas_equalTo(self.moreEditViewWidth);
            }];
            [self.contentView layoutIfNeeded];
        }
    }
}

@end
