/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PanelHost.h"
#import "PanelController.h"
#import "AnnotationPanel.h"
#import "AttachmentPanel.h"
#import "OutlinePanel.h"
#import "PanelController+private.h"
#import "PanelHost.h"
#import "ReadingBookmarkPanel.h"
#import "DigitalSignaturePanel.h"

@interface FSPanelController ()

@property (nonatomic, strong) UIButton *cancelButton;

@end

@implementation FSPanelController {
    UIControl *_maskView;
    __weak UIView *_superView;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak UIExtensionsManager *_extensionsManager;
    AnnotationPanel *annotationPanel;
    OutlinePanel *outlinePanel;
    ReadingBookmarkPanel *bookmarkPanel;
    AttachmentPanel *attachmentPanel;
    DigitalSignaturePanel *digitalSignaturePanel;
}

- (instancetype)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _superView = extensionsManager.pdfViewCtrl;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _extensionsManager = extensionsManager;
        _isHidden = YES;

        CGSize size = CGSizeMake(DEVICE_iPHONE ? _pdfViewCtrl.bounds.size.width : 300, _pdfViewCtrl.bounds.size.height);
        self.panel = [[PanelHost alloc] initWithFrame:size];
        self.panel.contentView.backgroundColor = ThemeViewBackgroundColor;
        CGRect screenFrame = _pdfViewCtrl.bounds;
        if (DEVICE_iPHONE) {
            self.panel.contentView.frame = CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height);
        } else {
            self.panel.contentView.frame = CGRectMake(0, 0, 300, screenFrame.size.height);
        }

        self.panelListeners = [[NSMutableArray alloc] init];

        // mask view
        _maskView = [[UIControl alloc] initWithFrame:_pdfViewCtrl.bounds];
        _maskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;

        //        [_superView addSubview:self.panel.contentView];
        //        self.panel.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        //        [self setIsHidden:YES animated:NO];

        NSArray<NSNumber *> *panelTypes = ({
            UIExtensionsConfig *config = _extensionsManager.config;
            NSMutableArray<NSNumber *> *panelTypes = @[].mutableCopy;
            if (config.loadReadingBookmark) {
                [panelTypes addObject:@(FSPanelTypeReadingBookmark)];
            }
            if (config.loadOutline) {
                [panelTypes addObject:@(FSPanelTypeOutline)];
            }
            if ([config.tools fs_containsAnyObjectNotInArray:@[ Tool_Select, Tool_Signature ]]) {
                [panelTypes addObject:@(FSPanelTypeAnnotation)];
            }
            if (config.loadAttachment) {
                [panelTypes addObject:@(FSPanelTypeAttachment)];
            }
            if (config.loadSignature) {
                [panelTypes addObject:@(FSPanelTypeDigitalSignature)];
            }

            panelTypes;
        });
        
        //load bookmark panel
        if ([panelTypes containsObject:@(FSPanelTypeReadingBookmark)]) {
            bookmarkPanel = [[ReadingBookmarkPanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
            [bookmarkPanel load];
        }
        //load outline panel
        if ([panelTypes containsObject:@(FSPanelTypeOutline)]) {
            outlinePanel = [[OutlinePanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
            [outlinePanel load];
        }
        //Load annotation panel
        if ([panelTypes containsObject:@(FSPanelTypeAnnotation)]) {
            annotationPanel = [[AnnotationPanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
            [annotationPanel load];
        }
        //load attachment panel
        if ([panelTypes containsObject:@(FSPanelTypeAttachment)]) {
            attachmentPanel = [[AttachmentPanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
            [attachmentPanel load];
        }
        //Load digital signature panel
        if ([panelTypes containsObject:@(FSPanelTypeDigitalSignature)]) {
            digitalSignaturePanel = [[DigitalSignaturePanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
            [digitalSignaturePanel load];
        }
        
        [self.panel reloadSegmentView];
    }
    return self;
}

#pragma mark - get panels show/hide status
-(NSMutableDictionary *)getItemHiddenStatus {
    NSMutableDictionary *panelInfo = [@{
                                        @"ReadingBookmark":@"YES",
                                        @"Outline":@"YES",
                                        @"Annotation":@"YES",
                                        @"Attachment":@"YES",
                                        @"DigitalSignature":@"YES",
                                        } mutableCopy];
    
    NSMutableDictionary *panelsTags = [@{
                                         [NSNumber numberWithInt:FSPanelTypeReadingBookmark]:@"ReadingBookmark",
                                         [NSNumber numberWithInt:FSPanelTypeOutline]:@"Outline",
                                         [NSNumber numberWithInt:FSPanelTypeAnnotation]:@"Annotation",
                                         [NSNumber numberWithInt:FSPanelTypeAttachment]:@"Attachment",
                                         [NSNumber numberWithInt:FSPanelTypeDigitalSignature]:@"DigitalSignature",
                                         } mutableCopy];
    
    NSArray *tempArr = nil;
    
    for ( id<IPanelSpec> thePanel in self.panel.specs) {
        int tag = [thePanel getType];
        tempArr = [panelsTags allKeys];
        
        if ([tempArr containsObject: [NSNumber numberWithInt:tag]]) {
            [panelInfo setObject:@"NO" forKey:[panelsTags objectForKey:[NSNumber numberWithInt:tag]]];
        }
    }
    
    return panelInfo;
}

- (id<IPanelSpec>)getPanel:(FSPanelType) type {
    for ( id<IPanelSpec> thePanel in self.panel.specs) {
        int getType = [thePanel getType];
        if(getType == type)
            return thePanel;
    }
    return nil;
}

- (BOOL)isPanelHidden:(FSPanelType) type {
    for ( id<IPanelSpec> thePanel in self.panel.specs) {
        int getType = [thePanel getType];
        if(getType == type)
            return NO;
    }
    return YES;
}

- (void)setPanelHidden:(BOOL)isHidden type:(FSPanelType)type {
    if (isHidden) {
        [self removePanelOfType:type];
    } else {
        [self addPanelOfType:type];
    }
    if (self.panel.specs.count > 0) {
        if (_cancelButton) {
            [_cancelButton removeFromSuperview];
        }
    } else {
        if (DEVICE_iPHONE) {
            [self.panel.contentView addSubview:self.cancelButton];
        }
    }
}

- (void)removePanelOfType:(FSPanelType)type {
    id<IPanelSpec> thePanel = nil;
    switch (type) {
        case FSPanelTypeOutline:
            thePanel = outlinePanel;
            [outlinePanel unload];
            outlinePanel = nil;
            break;
        case FSPanelTypeAnnotation:
            thePanel = annotationPanel;
            [annotationPanel unload];
            annotationPanel = nil;
            break;
        case FSPanelTypeAttachment:
            thePanel = attachmentPanel;
            [attachmentPanel unload];
            attachmentPanel = nil;
            break;
        case FSPanelTypeReadingBookmark:
            thePanel = bookmarkPanel;
            [bookmarkPanel unload];
            bookmarkPanel = nil;
            break;
        case FSPanelTypeDigitalSignature:
            thePanel = digitalSignaturePanel;
            [digitalSignaturePanel unload];
            digitalSignaturePanel = nil;
            break;
        default:
            break;
    }
    if(!thePanel) return;
    [self.panel reloadSegmentView];
}

- (void)addPanelOfType:(FSPanelType)type {
    switch (type) {
        case FSPanelTypeOutline:
            if (_extensionsManager.config.loadOutline && outlinePanel == nil) {
                outlinePanel = [[OutlinePanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
                [outlinePanel load];
            }
            break;
        case FSPanelTypeAnnotation:
            if (_extensionsManager.config.tools.count > 0 && annotationPanel == nil) {
                annotationPanel = [[AnnotationPanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
                [annotationPanel load];
            }
            break;
        case FSPanelTypeAttachment:
            if (_extensionsManager.config.loadAttachment && attachmentPanel == nil) {
                attachmentPanel = [[AttachmentPanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
                [attachmentPanel load];
            }
            break;
        case FSPanelTypeReadingBookmark:
            if (_extensionsManager.config.loadReadingBookmark && bookmarkPanel == nil) {
                bookmarkPanel = [[ReadingBookmarkPanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
                [bookmarkPanel load];
            }
        break;
        case FSPanelTypeDigitalSignature:
            if (_extensionsManager.config.loadSignature && digitalSignaturePanel == nil) {
                digitalSignaturePanel = [[DigitalSignaturePanel alloc] initWithUIExtensionsManager:_extensionsManager panelController:self];
                [digitalSignaturePanel load];
            }
            break;
        default:
            break;
    }
    [self.panel reloadSegmentView];
}

- (void)setIsHidden:(BOOL)isHidden {
    [self setIsHidden:isHidden animated:YES];
}

- (void)setIsHidden:(BOOL)isHidden animated:(BOOL)animated {
    if (_isHidden == isHidden) {
        return;
    }
    _isHidden = isHidden;
    if (_isHidden) {
        if (self.panel.contentView.superview) {
            [self.panel.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self->_superView).priorityLow();
                make.right.equalTo(self->_superView.mas_left);
                if (DEVICE_iPHONE) {
                    make.right.equalTo(self->_superView).priorityLow();
                }
            }];
            [_superView setNeedsUpdateConstraints];
            [_superView updateConstraintsIfNeeded];
        }
        if (animated) {
            [UIView animateWithDuration:0.4
                animations:^{
                    self->_maskView.alpha = 0.1f;
                    [self.panel.contentView layoutIfNeeded];
                    [self->_superView layoutIfNeeded];
                }
                completion:^(BOOL finished) {
                    [self->_maskView removeFromSuperview];
                    self.panel.contentView.hidden = self->_isHidden;
                }];
        } else {
            [_maskView removeFromSuperview];
            self.panel.contentView.hidden = _isHidden;
        }
    } else {
        if (![_superView.subviews containsObject:self.panel.contentView]) {
            [_superView addSubview:self.panel.contentView];
        }
        
        [self.panel.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self->_superView);
            make.right.equalTo(self->_superView.mas_left).priorityMedium();
            make.left.equalTo(self->_superView).priorityLow();
            if (DEVICE_iPHONE) {
                make.width.equalTo(self->_superView);
            } else {
                make.width.mas_equalTo(300);
            }
        }];
        [_superView setNeedsUpdateConstraints];
        [_superView updateConstraintsIfNeeded];
        [_superView layoutIfNeeded];

        _maskView.frame = _pdfViewCtrl.bounds;
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.3f;
        [_maskView addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
        [_superView insertSubview:_maskView belowSubview:self.panel.contentView];

        [_maskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->_maskView.superview.mas_left).offset(0);
            make.right.equalTo(self->_maskView.superview.mas_right).offset(0);
            make.top.equalTo(self->_maskView.superview.mas_top).offset(0);
            make.bottom.equalTo(self->_maskView.superview.mas_bottom).offset(0);
        }];

        self.panel.contentView.hidden = _isHidden;
        
        [self.panel.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->_superView).priorityHigh();
            make.right.equalTo(self->_superView.mas_left).priorityLow();
            if (DEVICE_iPHONE) {
                make.right.equalTo(self->_superView).priorityHigh();
            }
        }];
        
        [_superView setNeedsUpdateConstraints];
        [_superView updateConstraintsIfNeeded];
        if (animated) {
            [UIView animateWithDuration:0.4
                             animations:^{
                                 [self->_superView layoutIfNeeded];
                             }
                             completion:^(BOOL finished){
                             }];
        }
    }
    for (id<IPanelChangedListener> listener in self.panelListeners) {
        if ([listener respondsToSelector:@selector(onPanelChanged:)]) {
            [listener onPanelChanged:_isHidden];
        }
    }
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 32, 12, 12)];
        [_cancelButton addTarget:self action:@selector(canelPanel:) forControlEvents:UIControlEventTouchUpInside];
        _cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        [_cancelButton setBackgroundImage:[UIImage imageNamed:@"panel_cancel" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [_cancelButton setEnlargedEdge:10];
        [self.panel.contentView addSubview:_cancelButton];
    }
    return _cancelButton;
}

- (void)canelPanel:(UIButton *)button {
    self.isHidden = YES;
}

- (void)dismiss:(id)sender {
    self.isHidden = YES;
}

- (void)registerPanelChangedListener:(id<IPanelChangedListener>)listener {
    if (self.panelListeners) {
        [self.panelListeners addObject:listener];
    }
}

- (void)unregisterPanelChangedListener:(id<IPanelChangedListener>)listener {
    if ([self.panelListeners containsObject:listener]) {
        [self.panelListeners removeObject:listener];
    }
}

- (void)reloadReadingBookmarkPanel {
    [bookmarkPanel reloadData];
}

@end
