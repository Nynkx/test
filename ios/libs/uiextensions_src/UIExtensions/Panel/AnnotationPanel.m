/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AnnotationPanel.h"
#import "AnnotationListViewController.h"
#import "AnnotationStruct.h"
#import "PanelController+private.h"
#import "PanelHost.h"

#define ENLARGE_EDGE 3

@interface AnnotationPanel () <IPageEventListener> {
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak UIExtensionsManager *_extensionsManager;
}

@property (nonatomic, strong) UIView *toolbar;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) Popover *popover;

@end

@implementation AnnotationPanel

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager panelController:(FSPanelController *)panelController {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _panelController = panelController;

        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 100, 25)];
        title.backgroundColor = [UIColor clearColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        title.text = FSLocalizedForKey(@"kAnnotation");
        title.textColor = BlackThemeTextColor;

        self.toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _pdfViewCtrl.bounds.size.width, 64)];
        self.toolbar.backgroundColor = PanelTopBarColor;
        self.contentView = [[UIView alloc] init];
        self.contentView.backgroundColor = ThemeViewBackgroundColor;

        self.annotationCtrl = [[AnnotationListViewController alloc] initWithStyle:UITableViewStylePlain extensionsManager:extensionsManager module:self];
        [panelController registerPanelChangedListener:self.annotationCtrl];
        _annotationCtrl.annotationGotoPageHandler = ^(int page, NSString *annotuuid) {

        };
        _annotationCtrl.annotationSelectionHandler = ^() {

        };

        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 32, 12, 12)];
        [cancelButton addTarget:self action:@selector(cancelBookmark) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        UIImage *bcakgrdImg = [UIImage imageNamed:@"panel_cancel.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        [cancelButton setBackgroundImage:bcakgrdImg forState:UIControlStateNormal];

        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        UITapGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelBookmark)];
        backgroundView.userInteractionEnabled = YES;
        [backgroundView addGestureRecognizer:tapG];
        [self.toolbar addSubview:backgroundView];

        self.editButton = [[UIButton alloc] initWithFrame:CGRectMake(self.toolbar.frame.size.width - 65, 20, 55, 35)];
        [_editButton addTarget:self action:@selector(didClickedMore) forControlEvents:UIControlEventTouchUpInside];
        [_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
        [_editButton setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
        UIImage *moreImg = [UIImage imageNamed:@"annotion_list_more" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        [_editButton setImage:moreImg forState:UIControlStateNormal];
        _editButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [_editButton setEnlargedEdge:ENLARGE_EDGE];
        [self.contentView addSubview:_annotationCtrl.view];
        [_annotationCtrl.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(0);
            make.right.equalTo(self.contentView.mas_right).offset(0);
            make.top.equalTo(self.contentView.mas_top).offset(0);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(0);
        }];
        title.center = CGPointMake(self.toolbar.bounds.size.width / 2, title.center.y);
        [self.toolbar addSubview:title];
        [self.toolbar addSubview:_editButton];
        if (DEVICE_iPHONE) {
            [backgroundView addSubview:cancelButton];
        }
    }
    return self;
}

- (void)load {
    [_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_panelController.panel insertSpec:self atIndex:2];
    _panelController.panel.currentSpec = self;
}

- (void)unload {
    [_pdfViewCtrl unregisterDocEventListener:self];
    [_pdfViewCtrl unregisterPageEventListener:self];
    [_panelController.panel removeSpec:self];
    _panelController.panel.currentSpec = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:_annotationCtrl name:ANNOLIST_UPDATETOTAL object:nil];
}

- (void)cancelBookmark {
    _panelController.isHidden = YES;
}

- (void)didClickedMore{
    UIView *menu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 100)];
    menu.backgroundColor = UIColor_DarkMode(menu.backgroundColor, ThemePopViewColor_Dark);
    UIButton *clear = [[UIButton alloc] initWithFrame:CGRectMake(10, 15, 130, 20)];
    [clear setTitle:FSLocalizedForKey(@"kClear") forState:UIControlStateNormal];
    [clear setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [clear setTitleColor:[UIColor colorWithRed:111 / 255.f green:113 / 255.f blue:121 / 255.f alpha:1] forState:UIControlStateDisabled];
    [clear setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    clear.titleLabel.font = [UIFont systemFontOfSize:15];
    [clear addTarget:self action:@selector(clearAnnotations) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:clear];
    UIButton *applyAll = [[UIButton alloc] initWithFrame:CGRectMake(10, 65, 130, 20)];
    [applyAll setTitle:FSLocalizedForKey(@"kApplyAllRedaction") forState:UIControlStateNormal];
    [applyAll setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [applyAll setTitleColor:[UIColor colorWithRed:111 / 255.f green:113 / 255.f blue:121 / 255.f alpha:1] forState:UIControlStateDisabled];
    [applyAll setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    applyAll.titleLabel.font = [UIFont systemFontOfSize:15];
    [applyAll addTarget:self action:@selector(applyAllRedaction) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:applyAll];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, 50, 130, 1)];
    line.backgroundColor = ThemePopViewDividingLineColor;
    [menu addSubview:line];
    
    clear.enabled = [_annotationCtrl getAnnotationsCount];
    applyAll.enabled = [_annotationCtrl getRedactAnnotsCount] && _extensionsManager.isSupportRedaction && [Utility canApplyRedaction:[_extensionsManager.pdfViewCtrl currentDoc]];

    self.popover = [Popover popover];
    
    void (^block)(void) = ^{
        [self _popoverSideEdgeConfig];
        CGPoint point = CGPointMake(CGRectGetMidX(self.editButton.frame), CGRectGetMaxY(self.editButton.frame));
        CGPoint startPoint = [self.editButton.superview convertPoint:point toView:self->_pdfViewCtrl];
        [self.popover showAtPoint:startPoint popoverPosition:DXPopoverPositionDown withContentView:menu inView:self->_pdfViewCtrl];
    };
    
    block();
    
    self.popover.didDismissHandler = ^{
    };
    self.popover.didRotatedHandler = ^{
        block();
    };
}

- (void) _popoverSideEdgeConfig{
    if (IS_IPHONE_OVER_TEN) {
        NSInteger orientation =  [UIDevice currentDevice].orientation;
        if (orientation == 1 || orientation == 4) {
            self.popover.sideEdge = 4.f;
        }else {
            self.popover.sideEdge = 44.f;
        }
    }
}

- (void)clearAnnotations {
    [self.popover dismiss];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kClearAnnotations") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *_Nonnull action) {
                                                             [self->_annotationCtrl clearAnnotations];
                                                         }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancelAction];
    [alertController addAction:action];
    [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)applyAllRedaction{
    [self.popover dismiss];
    UIAlertController *alert = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kRedaction") message:FSLocalizedForKey(@"kRedactTip") actions:@[FSLocalizedForKey(@"kCancel"),FSLocalizedForKey(@"kOK")] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
        if (index == 1) {
           [self->_annotationCtrl applyRedactAnnotations];
        }
    }];
    [_pdfViewCtrl.fs_viewController presentViewController:alert animated:YES completion:nil];
}

- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_annotationCtrl viewWillAppear:YES];
    });

    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self->_annotationCtrl selector:@selector(UpdateAnnotationsTotal:) name:ANNOLIST_UPDATETOTAL object:nil];
        self->_annotationCtrl.allCanModify = YES;
        self->_annotationCtrl.indexPath = nil;
        [self->_annotationCtrl clearData];
        [self->_annotationCtrl loadData:YES];

        //update clear button state
        if (![Utility canAddAnnot:self->_pdfViewCtrl]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self->_editButton.enabled = NO;
                [self->_editButton setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self->_editButton.enabled = YES;
                [self->_editButton setTitleColor:[UIColor colorWithRed:0 / 255.f green:150.f / 255.f blue:212.f / 255.f alpha:1] forState:UIControlStateNormal];
            });
        }
    });
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
    _annotationCtrl.allCanModify = YES;
    _annotationCtrl.annoupdatetipLB.hidden = YES;
    _annotationCtrl.tableView.frame = CGRectMake(0, 0, _annotationCtrl.tableView.superview.frame.size.width, _annotationCtrl.tableView.superview.frame.size.height);

    [[NSNotificationCenter defaultCenter] removeObserver:_annotationCtrl name:ANNOLIST_UPDATETOTAL object:nil];
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

- (int)getType {
    return FSPanelTypeAnnotation;
}

- (UIView *)getTopToolbar {
    return self.toolbar;
}

- (UIView *)getContentView {
    return self.contentView;
}

- (SegmentItem*)getSegmentItem {
    SegmentItem *annotation = [[SegmentItem alloc] init];
    annotation.tag = FSPanelTypeAnnotation;
    UIImage* normalImg = [UIImage imageNamed:@"panel_top_annot_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIImage* selImg = [UIImage imageNamed:@"panel_top_annot_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    annotation.image = normalImg;
    annotation.selectImage = selImg;
    return annotation;
}

- (void)onActivated {
}

- (void)onDeactivated {
    [self.annotationCtrl hideCellEditView];
}

#pragma mark IPageEventListener
- (void)onPagesWillRemove:(NSArray<NSNumber *> *)indexes {
}

- (void)onPagesWillMove:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
}

- (void)onPagesWillRotate:(NSArray<NSNumber *> *)indexes rotation:(int)rotation {
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    [self reloadData];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    [self reloadData];
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    [self reloadData];
}

#pragma mark private

- (void)reloadData {
    _annotationCtrl.allCanModify = YES;
    _annotationCtrl.indexPath = nil;
    [_annotationCtrl loadData:YES];
}

@end
