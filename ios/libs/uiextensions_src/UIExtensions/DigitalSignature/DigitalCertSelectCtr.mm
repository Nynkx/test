/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "DigitalCertSelectCtr.h"

#import "CerInfoViewController.h"

@interface DigitalCertCell : UITableViewCell
@property (nonatomic, strong) UIImageView *selectIcon;
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation DigitalCertCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disSelectTrack" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        self.nameLabel = [[UILabel alloc] init];
        [self addSubview:self.selectIcon];
        [self addSubview:self.nameLabel];
        self.accessoryType = UITableViewCellAccessoryDetailButton;

        [self.selectIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *)) {
                make.left.mas_equalTo(self.mas_safeAreaLayoutGuideLeft);
            } else {
                make.left.mas_equalTo(self.mas_left).offset(10);
            }
            make.centerY.mas_equalTo(self.mas_centerY);
            make.width.mas_equalTo(26);
            make.height.mas_equalTo(26);
        }];

        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.selectIcon.mas_right).offset(20);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.right.mas_equalTo(self.mas_right).offset(-50);
            make.height.mas_equalTo(30);
        }];

        UIView *divideView = [[UIView alloc] init];
        divideView.backgroundColor = DividingLineColor;
        [self addSubview:divideView];
        [divideView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.selectIcon.mas_left);
            make.height.mas_equalTo(DIVIDE_VIEW_THICKNES);
            make.bottom.mas_equalTo(divideView.superview.mas_bottom);
            if (@available(iOS 11.0, *)) {
                make.right.mas_equalTo(self.mas_safeAreaLayoutGuideRight).offset(-2);
            } else {
                make.right.mas_equalTo(self.mas_right).offset(-2);
            }
        }];
    };
    return self;
}

@end

@interface DigitalCertSelectCtr () <UITableViewDelegate, UITableViewDataSource, TSAlertViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger currentSelectIndexPathRow;
@property (nonatomic, assign) NSInteger showPromptTimes;
@property (nonatomic, strong) TSAlertView *currentAlertView;
@property (nonatomic, strong) NSMutableDictionary *currentCertPasswordDic;
@property (nonatomic, copy) void (^finishInputPassword)(NSString *);

@property (nonatomic) UIStatusBarStyle statusBarStyle;

@property (nonatomic, strong) UIViewController *originalPresentedViewController;
@property (nonatomic, strong) CERT_INFO *cerInfo;

@end

@implementation DigitalCertSelectCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    self.currentCertPasswordDic = [[NSMutableDictionary alloc] init];
    self.currentSelectIndexPathRow = -1;
    [self loadCertInfo];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:FSLocalizedForKey(@"kCancel") style:UIBarButtonItemStylePlain target:self action:@selector(didCancel)];
    self.navigationItem.leftBarButtonItem = leftButton;
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:FSLocalizedForKey(@"kOK") style:UIBarButtonItemStylePlain target:self action:@selector(didOk)];
    self.navigationItem.rightBarButtonItem = rightButton;
    rightButton.enabled = NO;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRGB:0x179cd8];
    self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
        
    _statusBarStyle = UIStatusBarStyleDefault;
    self.originalPresentedViewController = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    if (self.dataArray.count < 1) {
        UILabel *nonCertLabel = [[UILabel alloc] init];
        nonCertLabel.text = FSLocalizedForKey(@"kNoCert");
        nonCertLabel.font = [UIFont systemFontOfSize:20];
        [self.view addSubview:nonCertLabel];
        [nonCertLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(nonCertLabel.superview.mas_centerX);
            make.centerY.mas_equalTo(nonCertLabel.superview.mas_centerY);
        }];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.statusBarStyle;
}

- (void)setStatusBarStyle:(UIStatusBarStyle)statusBarStyle {
    _statusBarStyle = statusBarStyle;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)loadCertInfo {
    NSString *path = DOCUMENT_PATH;
    NSFileManager *myFileManager = [NSFileManager defaultManager];

    NSDirectoryEnumerator *myDirectoryEnumerator;

    myDirectoryEnumerator = [myFileManager enumeratorAtPath:path];

    while ((path = [myDirectoryEnumerator nextObject]) != nil) {
        if ([[[path pathExtension] lowercaseString] isEqualToString:@"p12"] || [[[path pathExtension] lowercaseString] isEqualToString:@"pfx"]) {
            [self.dataArray addObject:path.copy];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *certCellIndentifier = @"certCellIndentifier";
    DigitalCertCell *cell = [[DigitalCertCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:certCellIndentifier];
    cell.nameLabel.text = [(NSString *) [self.dataArray objectAtIndex:indexPath.row] lastPathComponent];
    cell.nameLabel.font = [UIFont systemFontOfSize:16];
    if (self.currentSelectIndexPathRow == indexPath.row) {
        cell.selectIcon.image = [UIImage imageNamed:@"selectTrack" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        cell.selectIcon.image = [UIImage imageNamed:@"disSelectTrack" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CERT_INFO *info = [[CERT_INFO alloc] init];
    self.cerInfo = info;
    NSString *path = [DOCUMENT_PATH stringByAppendingPathComponent:self.dataArray[indexPath.row]];
    NSString *md5 = [NSString fs_getFileMD5WithPath:path];
    if ([self.currentCertPasswordDic objectForKey:md5]) {
        self.currentSelectIndexPathRow = indexPath.row;
        [self.tableView reloadData];
    } else {
        __weak typeof(self) weakSelf = self;
        [self promptForPassword:^(NSString *password) {
            if (password == nil) {
                weakSelf.showPromptTimes = 0;
                return;
            }

            PDF_CERT_ERROR_CODE result =[FSPDFCertUtil getCertInfo:path password:password certInfo:info];
            if (result == PDF_CERT_ERROR_FILE) {
                return;
            } else if (result == PDF_CERT_ERROR_PASSWORD) {
                [weakSelf tableView:tableView didSelectRowAtIndexPath:indexPath];
                return;
            }

            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *endDate = [dateFormatter dateFromString:info.certEndDate];

            weakSelf.showPromptTimes = 0;
            if (endDate.timeIntervalSince1970 - [NSDate date].timeIntervalSince1970 < 0) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kCertExpired") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction *_Nonnull action) {
                                                                   [self popoverDidCancel];
                                                               }];
                [alertController addAction:action];
                [self presentAlertController:alertController];
            } else {
                [weakSelf.currentCertPasswordDic setObject:password forKey:md5];
                weakSelf.currentSelectIndexPathRow = indexPath.row;
            }
            [weakSelf.tableView reloadData];
        }];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(nonnull NSIndexPath *)indexPath {
    CERT_INFO *info = [[CERT_INFO alloc] init];
    NSString *path = [DOCUMENT_PATH stringByAppendingPathComponent:self.dataArray[indexPath.row]];
    NSString *md5 = [NSString fs_getFileMD5WithPath:path];
    if ([self.currentCertPasswordDic objectForKey:md5]) {
        PDF_CERT_ERROR_CODE result =[FSPDFCertUtil getCertInfo:path password:[self.currentCertPasswordDic objectForKey:md5] certInfo:info];
        if (result == PDF_CERT_ERROR_FILE) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kCertFileError") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction *_Nonnull action) {
                                                               [self popoverDidCancel];
                                                           }];
            [alertController addAction:action];
            [self presentAlertController:alertController];
            return;
        } else if (result == PDF_CERT_ERROR_PASSWORD) {
            [self.currentCertPasswordDic removeObjectForKey:md5];
            [self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
            return;
        }
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
        CerInfoViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"CerInfoViewController"];
        [VC setCerInfoDic:info.cerInfoDic];
        [self.navigationController pushViewController:VC animated:YES];
    } else {
        [self promptForPassword:^(NSString *password) {
            if (password == nil) {
                self.showPromptTimes = 0;
                return;
            }

            PDF_CERT_ERROR_CODE result = [FSPDFCertUtil getCertInfo:path password:password certInfo:info];
            if (result == PDF_CERT_ERROR_FILE) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kCertFileError") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction *_Nonnull action) {
                                                                   [self popoverDidCancel];
                                                               }];
                [alertController addAction:action];
                [self presentAlertController:alertController];
                return;
            } else if (result == PDF_CERT_ERROR_PASSWORD) {
                [self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
                return;
            }

            self.showPromptTimes = 0;
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *endDate = [dateFormatter dateFromString:info.certEndDate];

            if (endDate.timeIntervalSince1970 - [NSDate date].timeIntervalSince1970 < 0) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kCertExpired") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction *_Nonnull action) {
                                                                   [self popoverDidCancel];
                                                               }];
                [alertController addAction:action];
                [self presentAlertController:alertController];
            } else {
                [self.currentCertPasswordDic setObject:password forKey:md5];
                self.currentSelectIndexPathRow = indexPath.row;
            }

            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
            CerInfoViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"CerInfoViewController"];
            [VC setCerInfoDic:info.cerInfoDic];
            [self.navigationController pushViewController:VC animated:YES];
        }];
    }
}

- (void)didOk {
    void (^doneOperator)() = ^{
        if (self.currentSelectIndexPathRow != -1 && self.currentSelectIndexPathRow < self.dataArray.count) {
            NSString *path = [DOCUMENT_PATH stringByAppendingPathComponent:self.dataArray[self.currentSelectIndexPathRow]];
            NSString *md5 = [NSString fs_getFileMD5WithPath:path];
            if ([self.currentCertPasswordDic objectForKey:md5]) {
                if (self.doneOperator) {
                    self.doneOperator(path, [self.currentCertPasswordDic objectForKey:md5], md5);
                }
            }
        }
    };
    if (self.selectOperator == FSCertSelectOperatorSignature) {
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     doneOperator();
                                 }];
    }else{
        [NSUserDefaults fs_setTrustedCerInfo:self.cerInfo.cerInfoDic];
        [self.navigationController popViewControllerAnimated:YES];
        doneOperator();
    }
}

- (void)didCancel {
    void (^cancelOperator)() = ^{
        self.statusBarStyle = UIStatusBarStyleLightContent;
        //                                 [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        if (self.cancelOperator) {
            self.cancelOperator(nil, nil, nil);
        }
    };
    if (self.selectOperator == FSCertSelectOperatorSignature) {
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     cancelOperator();
                                 }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        cancelOperator();
    }
    
}

- (void)promptForPassword:(void (^)(NSString *))complete {
    self.finishInputPassword = complete;
    TSAlertView *alertView = [[TSAlertView alloc] init];
    if (self.showPromptTimes == 0) {
        alertView.title = FSLocalizedForKey(@"kOfflineCopyAlterTitle");
    } else {
        alertView.title = FSLocalizedForKey(@"kPassWordErrorAlterTitle");
    }
    self.currentAlertView = alertView;
    [alertView addButtonWithTitle:FSLocalizedForKey(@"kCancel")];
    [alertView addButtonWithTitle:FSLocalizedForKey(@"kOK")];
    alertView.style = TSAlertViewStyleInputText;
    alertView.buttonLayout = TSAlertViewButtonLayoutNormal;
    alertView.usesMessageTextView = NO;
    alertView.inputTextField.secureTextEntry = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inputTextFieldChange:) name:UITextFieldTextDidChangeNotification object:alertView.inputTextField];
    alertView.delegate = self;
    UIButton *sureBtn = alertView.buttons.lastObject;
    sureBtn.enabled = NO;
    [sureBtn setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
    [alertView show];
    self.showPromptTimes++;
}

- (void)inputTextFieldChange:(NSNotification *)aNotification {
    if ([self.currentAlertView.inputTextField isEqual:aNotification.object]) {
        UIButton *sureBtn = self.currentAlertView.buttons.lastObject;
        if (((UITextField *) aNotification.object).text.length != 0) {
            sureBtn.enabled = YES;
            [sureBtn setTitleColor:[UIColor colorWithRed:0 / 255.0 green:122.0 / 255.0 blue:255.0 / 255.0 alpha:1] forState:UIControlStateNormal];
        } else {
            sureBtn.enabled = NO;
            [sureBtn setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
        }
    }
}

#pragma mark - TSAlertViewDelegate

- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    TSAlertView *tsAlertView = (TSAlertView *) alertView;
    double delayInSeconds = .1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

    if (buttonIndex == 1) {
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            NSString *password = tsAlertView.inputTextField.text;
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            self.currentAlertView = nil;
            self.finishInputPassword(password);
        });
    } else {
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            self.currentAlertView = nil;
            self.finishInputPassword(nil);
        });
    }
}

#pragma mark pop over

- (void)presentAlertController:(UIAlertController *)alertController {
    self.originalPresentedViewController = self.presentedViewController;
    if (self.originalPresentedViewController) {
        [self.originalPresentedViewController dismissViewControllerAnimated:true
                                                                 completion:^{
                                                                     [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
                                                                 }];
    } else {
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)popoverDidCancel {
    if (self.originalPresentedViewController) {
        [[Utility getTopMostViewController] presentViewController:self.originalPresentedViewController animated:true completion:nil];
        self.originalPresentedViewController = nil;
    }
}

@end
