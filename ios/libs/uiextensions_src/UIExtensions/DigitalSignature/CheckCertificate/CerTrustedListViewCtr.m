/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CerTrustedListViewCtr.h"
#import "DigitalCertSelectCtr.h"
#import "FSOptionsMoreButton.h"
#import "CerInfoViewController.h"
#import "FSBaseViews.h"

@interface CerTrustedTableViewCell : FSTableViewCell
@property (nonatomic, weak) IBOutlet UILabel *cerSubjectLab;
@property (nonatomic, weak) IBOutlet UILabel *cerEndDateLab;
@property (nonatomic, weak) IBOutlet FSOptionsMoreButton *more;
@end

static NSString *const CerTrustedInfos = @"CerTrustedInfos";
static NSString *const CerTrustedTableViewCellID = @"CerTrustedTableViewCell";

@interface CerTrustedListViewCtr ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UIBarButtonItem *rightItem;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, copy) NSArray *dataSource;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@end

@implementation CerTrustedListViewCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kTrustedCert") ;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.rowHeight = PREFERENCE_CELL_ROW_HEIGHT;
    self.tableView.separatorColor = DividingLineColor;
    
    self.dataSource = [NSUserDefaults fs_getTrustedCerInfos];
    
    self.rightItem.title = FSLocalizedForKey(@"kAdd");
}

- (IBAction)touchedBackItem:(UIBarButtonItem *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (IBAction)touchedTrustCerItem:(UIBarButtonItem *)sender {
    DigitalCertSelectCtr *digitalCertSelectCtr = [[DigitalCertSelectCtr alloc] init];
    digitalCertSelectCtr.selectOperator = FSCertSelectOperatorAddTrustCer;
    digitalCertSelectCtr.title = FSLocalizedForKey(@"kSelectAddCert");
    [self.navigationController pushViewController:digitalCertSelectCtr animated:YES];
    
    digitalCertSelectCtr.doneOperator = ^(NSString *path, NSString *passwd, NSString *md5) {
        self.dataSource = [NSUserDefaults fs_getTrustedCerInfos];
        [self.tableView reloadData];
    };
}

- (void)setUIExtensionsManager:(UIExtensionsManager *)extensionsManager{
    _extensionsManager = extensionsManager;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CerTrustedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CerTrustedTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *cerInfo = self.dataSource[indexPath.row];
    cell.cerSubjectLab.text = cerInfo[@"subject"];
    cell.cerEndDateLab.text = cerInfo[@"endDateStr"];
    @weakify(self)
    [cell.more.optionItems enumerateObjectsUsingBlock:^(FSOptionsMoreItem * _Nonnull item1, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self)
        item1.touchedItem = ^(FSOptionsMoreItem *item2) {
            if (idx == 0){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
                CerInfoViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"CerInfoViewController"];
                [VC setCerInfoDic:cerInfo];
                [self.navigationController pushViewController:VC animated:YES];
            }else{
                UIAlertController *alertView = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kDeleteTrustedCer") actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kOK") ] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                    if (index == 1) {
                        [NSUserDefaults fs_removeTrustedCerInfo:cerInfo];
                        self.dataSource = [NSUserDefaults fs_getTrustedCerInfos];
                        [self.tableView reloadData];
                    }
                }];
                [[Utility getTopMostViewController] presentViewController:alertView animated:YES completion:nil];
            }
        };
    }];
    return cell;
}

@end

@implementation CerTrustedTableViewCell

- (void)awakeFromNib{
    [super awakeFromNib];
    FSOptionsMoreItem *moreInfoItem = [FSOptionsMoreItem createItemWithImageAndTitle:FSLocalizedForKey(@"kInfo")
                                                                         imageNormal:ImageNamed(@"check_cer_info") imageSelected:ImageNamed(@"check_cer_info")];
    FSOptionsMoreItem *deleteItem = [FSOptionsMoreItem createItemWithImageAndTitle:FSLocalizedForKey(@"kDelete")
                                                                          imageNormal:ImageNamed(@"trust_cer_delete")
                                                                        imageSelected:ImageNamed(@"trust_cer_delete")];
    self.more.optionItems = @[ moreInfoItem, deleteItem ];
    [self.more setup];
}

@end
