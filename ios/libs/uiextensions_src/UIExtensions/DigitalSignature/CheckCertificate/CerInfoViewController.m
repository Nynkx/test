/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CerInfoViewController.h"
#import "FSBaseViews.h"

@interface InfoTableViewCell : FSTableViewCell
@property (nonatomic, weak) IBOutlet UILabel *titleLab;
@property (nonatomic, weak) IBOutlet UILabel *infoLab;
@end

static NSString *const InfoTableViewCellID = @"InfoTableViewCell";

@interface CerInfoViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, copy) NSArray *dataSource;
@end

@implementation CerInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kCertInfo");
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    
}
- (IBAction)touchedBackItem:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setCerInfoDic:(NSDictionary *)cerInfoDic{
    NSString *title = @"titile";
    NSString *info = @"info";
    
    NSString * (^keyForValue) (NSString *) = ^(NSString *key){
        NSString *value = cerInfoDic[key];
        return value ? value : @"";
    };
    NSArray *tmpKeyArr = @[ FSLocalizedForKey(@"kCertSerialNum"), FSLocalizedForKey(@"kTrustedCertName"), FSLocalizedForKey(@"kCertIssuer"), FSLocalizedForKey(@"kUsage") , FSLocalizedForKey(@"kTrustedCertValidFrom"), FSLocalizedForKey(@"kTrustedCertValidTo"), FSLocalizedForKey(@"kCertEmail")];
    NSArray *tmpValueArr = @[ keyForValue(@"serialNumber"), keyForValue(@"name"), keyForValue(@"issuerInfo"), keyForValue(@"origUsage"), keyForValue(@"startDateStr") , keyForValue(@"endDateStr"), keyForValue(@"email"),];
    NSMutableArray *tmpMuArr = [NSMutableArray array];
    for (int i = 0; i < tmpKeyArr.count; i ++ ) {
        NSString *tmpTitle = tmpKeyArr[i];
        tmpTitle = [tmpTitle stringByReplacingOccurrencesOfString:@":" withString:@""];
        tmpTitle = [tmpTitle stringByReplacingOccurrencesOfString:@"：" withString:@""];
        NSDictionary *dic = @{ title : tmpTitle, info : tmpValueArr[i] };
        if ([tmpValueArr[i] isEqualToString:keyForValue(@"origUsage")]) {
            NSArray *locs = [keyForValue(@"origUsage") componentsSeparatedByString:@","];
            NSMutableString *locStrs = [NSMutableString string];
            NSString *locValue = nil;
            if (locs.count > 0) {
                locValue = FSLocalizedForKey(locs[0]);
                locStrs = [NSMutableString stringWithString:locValue];
            }
            
            for (int i = 1; i < locs.count; i++) {
                locValue = FSLocalizedForKey(locs[i]);
                [locStrs appendString:[NSString stringWithFormat:@", %@",locValue]];
            }
            dic = @{ title : tmpTitle, info : locStrs };
        }
        
        [tmpMuArr addObject:dic];
    }
    self.dataSource = tmpMuArr.copy;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return FSLocalizedForKey(@"kCertInfo");
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:InfoTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = self.dataSource[indexPath.row];
    NSString *title = @"titile";
    NSString *info = @"info";
    cell.titleLab.text = dic[title];
    cell.infoLab.text = [dic[info] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

@implementation InfoTableViewCell


@end
