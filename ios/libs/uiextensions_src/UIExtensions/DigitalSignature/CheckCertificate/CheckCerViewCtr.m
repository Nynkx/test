/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "CheckCerViewCtr.h"
#import "FSOptionsMoreButton.h"
#import "CerInfoViewController.h"
#import "FSBaseViews.h"

@class MultistageView;
@interface MultistageObj : NSObject
@property (nonatomic, assign) BOOL isFold;
@property (nonatomic, assign) NSInteger residueLowerLevel;
@property (nonatomic, assign) CGFloat increaseltHeight;
@property (nonatomic, assign) CGFloat increaseltWidth;
@property (nonatomic, assign) CGFloat increaseltLayoutWidth;
@property (nonatomic, assign) CGFloat layoutHeight;
@property (nonatomic, weak) MultistageObj *superiorMultistageObj;
@property (nonatomic, strong) MultistageObj *lowerLevelMultistageObj;
@property (nonatomic, weak) MultistageView *multistageView;
@property (nonatomic, copy) NSDictionary *cerInfo;
@end

typedef NS_ENUM(NSInteger, MultistageViewStyle) {
    MultistageViewStyleCerCheck = 0
};

@interface MultistageView : FSView
@property (nonatomic, assign) MultistageViewStyle style;
@property (nonatomic, strong) UIButton *foldBtn;
@property (nonatomic, strong) UILabel *nameLab;
@property (nonatomic, strong) UILabel *detailsLab;
@property (nonatomic, strong) FSOptionsMoreButton *moreBtn;
@property (nonatomic, weak) MultistageView *lowerLevelMultistageView;
@property (nonatomic, weak) MultistageObj *multistageObj;
@property (nonatomic, assign) CGFloat containerHeight;
@property (nonatomic, assign) CGFloat layoutHeight;
@end

@interface MultistageTableViewCell : FSTableViewCell
- (void)setMultistageObj:(MultistageObj *)multistageObj;
@end

static NSString *const MultistageTableViewCellID = @"MultistageTableViewCell";
@interface CheckCerViewCtr ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrView;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (nonatomic, weak) FSSignature *currentSign;
@property (nonatomic, copy) NSArray *dataSource;
@end

@implementation CheckCerViewCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kViewCert");
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //If it's not autolayout computational cell, use the tableView:heightForRowAtIndexPath:
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200.f;
    
    [self.scrView layoutIfNeeded];
    [self creatDataSource];
    
}

- (IBAction)touchedBackItem:(UIBarButtonItem *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)setCurrentSelectSign:(FSSignature *)currentSign{
    _currentSign = currentSign;
}

- (void)setUIExtensionsManager:(UIExtensionsManager *)extensionsManager{
    _extensionsManager = extensionsManager;
}

- (void)creatDataSource{
    NSMutableArray *cersArr = [FSPDFCertUtil getCertFromSignatureContent:self.currentSign];
    if (!cersArr.count) return;
    MultistageObj *lasObj = nil;
    NSInteger count = cersArr.count;
    
    NSMutableArray *cersInfoArr = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        NSDictionary *cerInfo = [FSPDFCertUtil getTrustCertificateInformation:cersArr[i]];
        [cersInfoArr addObject:cerInfo];
    }

    cersInfoArr = [self sortCerInfoWithCerInfos:cersInfoArr];

    count = cersInfoArr.count;
    for (int i = 0; i < count; i++) {
        NSDictionary *cerInfo = cersInfoArr[i];
        MultistageObj *obj = [MultistageObj new];
        obj.cerInfo = cerInfo.copy;
        obj.residueLowerLevel = count - 1 - i;
        obj.superiorMultistageObj = lasObj;
        obj.increaseltLayoutWidth = obj.increaseltWidth * i;
        lasObj.lowerLevelMultistageObj = obj;
        lasObj = obj;
        if (!self.dataSource) {
            self.dataSource = @[lasObj];
        }
    }
}

- (NSMutableArray *)sortCerInfoWithCerInfos:(NSMutableArray *)cerInfos{
    if (cerInfos.count == 1) {
        return cerInfos;
    }
    NSInteger count = cerInfos.count;
    NSMutableArray *headCerInfos = @[].mutableCopy;
    NSMutableArray *tailCerInfos = @[].mutableCopy;
    NSMutableArray *middleCerInfos = @[].mutableCopy;
    for (int i = 0; i < count; i ++ ) {
        BOOL isExistSup = NO;
        BOOL isExistSub = NO;
        NSDictionary *cerInfo_i = cerInfos[i];
        for (int j = 0; j < count; j++) {
            if (j == i) {
                continue;
            }
            NSDictionary *cerInfo_j = cerInfos[j];
            if ([cerInfo_i[@"subject"] isEqualToString:cerInfo_j[@"issuerInfo"]]) {
                isExistSub = YES;
            }
            if ([cerInfo_i[@"issuerInfo"] isEqualToString:cerInfo_j[@"subject"]]) {
                isExistSup = YES;
            }
        }
        if (!isExistSup && isExistSub) {
            [headCerInfos addObject:cerInfo_i];
        }else if (isExistSup && !isExistSub) {
            [tailCerInfos addObject:cerInfo_i];
        }else {
            [middleCerInfos addObject:cerInfo_i];
        }
    }
    
    while (middleCerInfos.count) {
        count = middleCerInfos.count;
        for (int i = 0; i < count; i++) {
            NSDictionary *cerInfo = middleCerInfos[i];
            if ([cerInfo[@"subject"] isEqualToString:[tailCerInfos firstObject][@"issuerInfo"]]) {
                [tailCerInfos insertObject:cerInfo atIndex:0];
                [middleCerInfos removeObject:cerInfo];
            }
            if ([cerInfo[@"issuerInfo"] isEqualToString:[tailCerInfos firstObject][@"subject"]]) {
                [headCerInfos addObject:cerInfo ];
                [middleCerInfos removeObject:cerInfo];
            }
        }
    }
    [headCerInfos addObjectsFromArray:tailCerInfos];
    
    return headCerInfos;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MultistageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MultistageTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setMultistageObj:self.dataSource[indexPath.row]];
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    MultistageObj *obj = self.dataSource[indexPath.row];
//    return obj.layoutHeight;
//}

@end

@implementation MultistageObj

- (instancetype)init{
    self = [super init];
    if (self) {
        _increaseltHeight = PREFERENCE_CELL_ROW_HEIGHT;
        _isFold = YES;
        _layoutHeight = _increaseltHeight;
        _increaseltWidth = 35.f;
    }
    return self;
}

- (void)setLayoutHeight:(CGFloat)layoutHeight{
    if (layoutHeight ==  _layoutHeight) return;
    _layoutHeight = layoutHeight;
    self.multistageView.layoutHeight = layoutHeight;
    MultistageObj *superiorMultistageObj = self.superiorMultistageObj;
    if (superiorMultistageObj) {
        superiorMultistageObj.layoutHeight = superiorMultistageObj.increaseltHeight + layoutHeight;
        superiorMultistageObj.multistageView.layoutHeight = superiorMultistageObj.layoutHeight;
    }
}

@end

@implementation MultistageView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (instancetype)initWithMultistageObj:(MultistageObj *)multistageObj style:(MultistageViewStyle)style{
    self = [super init];
    if (self) {
        _style = style;
        _containerHeight = multistageObj.increaseltHeight;
        _multistageObj = multistageObj;
        multistageObj.multistageView = self;
        if (multistageObj.lowerLevelMultistageObj) {
            MultistageView *lowerLevelMultistageView = [[MultistageView alloc] initWithMultistageObj:multistageObj.lowerLevelMultistageObj style:style];
            self.lowerLevelMultistageView = lowerLevelMultistageView;
            [self addSubview:lowerLevelMultistageView];
            [lowerLevelMultistageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self).offset(multistageObj.lowerLevelMultistageObj.increaseltWidth);
                make.right.mas_equalTo(self);
                make.bottom.mas_equalTo(self).priorityMedium();
                make.top.mas_equalTo(self).offset(self.containerHeight);
            }];
        }
        [self initCommon];
    }
    return self;
}

- (void)initCommon{
    UIView *containerView = [UIView new];
    containerView.backgroundColor = [UIColor clearColor];
    [self addSubview:containerView];
    [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(self.containerHeight);
    }];
    CGFloat horSpacing = ITEM_MARGIN_NORMAL;
    CGFloat viewMargin = ITEM_MARGIN_LITTLE_NORMAL;
    if (self.lowerLevelMultistageView) {
        self.foldBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.foldBtn setImage:ImageNamed(@"panel_annotation_close") forState:UIControlStateSelected];
        [self.foldBtn setImage:ImageNamed(@"panel_annotation_open") forState:UIControlStateNormal];
        [self.foldBtn addTarget:self action:@selector(clickedFlodBtn:) forControlEvents:UIControlEventTouchUpInside];
        [containerView addSubview:self.foldBtn];
        [self.foldBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.mas_equalTo(self.foldBtn.superview);
            make.width.mas_equalTo(horSpacing*3);
        }];
    }
    
    if (self.style == MultistageViewStyleCerCheck) {
        self.nameLab = [[UILabel alloc] init];
        self.nameLab.textAlignment = NSTextAlignmentLeft;
        self.nameLab.text = self.multistageObj.cerInfo[@"subject"];
        [self.nameLab setContentHuggingPriority:249 forAxis:UILayoutConstraintAxisHorizontal];
        [containerView addSubview:self.nameLab];

        self.detailsLab = [[UILabel alloc] init];
        self.detailsLab.textAlignment = NSTextAlignmentLeft;
        self.detailsLab.font = [UIFont systemFontOfSize:13.f];
        self.detailsLab.textColor = SubjectLabTextColor;
        self.detailsLab.text = self.multistageObj.cerInfo[@"endDateStr"];
        [self.detailsLab setContentHuggingPriority:249 forAxis:UILayoutConstraintAxisHorizontal];
        [containerView addSubview:self.detailsLab];
        
        @weakify(self)
        FSOptionsMoreItem *moreInfoItem = [FSOptionsMoreItem createItemWithImageAndTitle:FSLocalizedForKey(@"kInfo")
                                                                                imageNormal:ImageNamed(@"check_cer_info") imageSelected:ImageNamed(@"check_cer_info")];
        moreInfoItem.touchedItem = ^(FSOptionsMoreItem *item) {
            @strongify(self)
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
            CerInfoViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"CerInfoViewController"];
            [VC setCerInfoDic:self.multistageObj.cerInfo];
            [self.fs_viewController.navigationController pushViewController:VC animated:YES];
        };
        FSOptionsMoreItem *moreTrustItem = [FSOptionsMoreItem createItemWithImageAndTitle:FSLocalizedForKey(@"kTrust")
                                                                               imageNormal:ImageNamed(@"trust_cer")
                                                                            imageSelected:ImageNamed(@"trust_cer")];
        moreTrustItem.enabled = ![NSUserDefaults fs_isTrustedCerInfo:self.multistageObj.cerInfo];
        moreTrustItem.touchedItem = ^(FSOptionsMoreItem *item) {
            @strongify(self)
            [self.fs_viewController
             presentViewController:[UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kWarning")
                                                                                    message:FSLocalizedForKey(@"kTrustedCertTip")
                                                                                    actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kOK") ]
                                                                                 actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                                                                                     if (index == 1) {
                                                                                         [NSUserDefaults fs_setTrustedCerInfo:self.multistageObj.cerInfo];
                                                                                         item.enabled = NO;
                                                                                     }
                                                                                 }]
             animated:YES completion:nil];
           
        };
        self.moreBtn = [[FSOptionsMoreButton alloc]
                        initWithOptionPosition:FSOptionPositionRightToLeft
                        optionItems:@[ moreInfoItem, moreTrustItem ]];
        [self.moreBtn setup];
        self.moreBtn.enabled = NO;
        [self.moreBtn setImage:ImageNamed(@"document_cellmore_more") forState:UIControlStateNormal];
        [containerView addSubview:self.moreBtn];
        
        [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.foldBtn ? self.foldBtn.mas_right : self.mas_left).offset(self.foldBtn ? 0 : horSpacing);
            make.right.mas_equalTo(self.moreBtn.mas_left).offset(-viewMargin);
            make.bottom.mas_equalTo(self.nameLab.superview.mas_centerY).offset(-ITEM_MARGIN_SMALL);
        }];
        
        [self.detailsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.nameLab);
            make.top.mas_equalTo(self.detailsLab.superview.mas_centerY).offset(ITEM_MARGIN_SMALL);
        }];
        
        [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.mas_equalTo(self.moreBtn.superview);
            make.width.mas_equalTo(50.f);
        }];
    }
    UIView *divideView = [[UIView alloc] init];
    divideView.backgroundColor = DividingLineColor;
    [containerView addSubview:divideView];
    [divideView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(divideView.superview);
        make.height.mas_equalTo(DIVIDE_VIEW_THICKNES);
    }];
}

- (void)setLayoutHeight:(CGFloat)layoutHeight{
    _layoutHeight = layoutHeight;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(layoutHeight);
    }];
}

- (void)clickedFlodBtn:(UIButton *)btn{
    btn.selected = !btn.selected;
    self.multistageObj.isFold = !btn.selected;
    CGFloat oldLayoutHeight = self.multistageObj.layoutHeight;
    [self layoutHeightWithMultistageObj:self.multistageObj];
    CGFloat currentLayoutHeight = self.multistageObj.layoutHeight;
    [self lowerLevelMultistageView:self enable:!(oldLayoutHeight > currentLayoutHeight)];
    [self.fs_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        CGFloat offset = self.multistageObj.increaseltLayoutWidth;
        if (!self.multistageObj.isFold) {
            offset = self.multistageObj.lowerLevelMultistageObj.increaseltLayoutWidth;
        }
        make.width.mas_equalTo(self.fs_tableView.superview.fs_mas_width).offset(offset);
    }];
    [self.fs_tableView reloadData];
}

- (void)lowerLevelMultistageView:(MultistageView *)view enable:(BOOL)enable  {
    if (view.lowerLevelMultistageView) {
        view.lowerLevelMultistageView.moreBtn.enabled = enable;
        if (enable) {
            if (!view.multistageObj.lowerLevelMultistageObj.isFold) {
                [self lowerLevelMultistageView:view.lowerLevelMultistageView enable:enable];
            }
            return;
        }
        [self lowerLevelMultistageView:view.lowerLevelMultistageView enable:enable];
    }
}

- (CGFloat)layoutHeightWithMultistageObj:(MultistageObj *)multistageObj{
    CGFloat layoutHeight = multistageObj.increaseltHeight;
    if (!multistageObj.isFold) {
        if (multistageObj.lowerLevelMultistageObj) {
            layoutHeight += [self layoutHeightWithMultistageObj:multistageObj.lowerLevelMultistageObj];
        }
    }
    multistageObj.layoutHeight = layoutHeight;
    return layoutHeight;
}

@end

@interface MultistageTableViewCell ()

@property (nonatomic, weak) MultistageView *multistageView;
@end

@implementation MultistageTableViewCell

- (void)setMultistageObj:(MultistageObj *)multistageObj{
    if (multistageObj) {
//        if (self.multistageView) { //Remove multistageView if cell reuse is required
//            [self.multistageView removeFromSuperview];
//        }
        //If you need to reuse the cell, delete the multistageView, but this is not currently necessary
        if (!self.multistageView) {
            MultistageView *multistageView = [[MultistageView alloc] initWithMultistageObj:multistageObj style:MultistageViewStyleCerCheck];
            self.multistageView = multistageView;
            self.multistageView.moreBtn.enabled = YES;
            [self.contentView addSubview:multistageView];
            [multistageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.mas_equalTo(self.contentView);
                make.bottom.mas_equalTo(self.contentView).priorityMedium();
                make.height.mas_equalTo(multistageObj.layoutHeight);
            }];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


