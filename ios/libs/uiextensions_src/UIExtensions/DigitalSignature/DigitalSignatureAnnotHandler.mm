/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "DigitalSignatureAnnotHandler.h"
#import "AlertView.h"
#import "AnnotationSignature.h"

#import "CustomIOSAlertView.h"
#import "FileSelectDestinationViewController.h"
#import "MenuControl.h"
#import "MenuItem.h"
#import "SignToolHandler.h"
#import "SignatureListViewController.h"
#import "SignatureViewController.h"
#import "FormAnnotHandler.h"
#import "DigitalSignatureUtility.h"
#import <sys/stat.h>
#import "CheckCerViewCtr.h"
#import "NSUserDefaults+Extensions.h"

@interface DigitalSignatureAnnotHandler () <UIPopoverPresentationControllerDelegate, SignatureListDelegate> {
}
@property (nonatomic, assign) BOOL shouldShowMenu;
@property (nonatomic, strong) FSAnnot *editAnnot;
@property (nonatomic, strong) NSMutableDictionary *digitalSignDic;
@property (nonatomic, strong) SignatureListViewController *signatureListCtr;
@property (nonatomic, assign) BOOL isAdded;
@property (nonatomic, strong) UIImage *annotImage;
@property (nonatomic, strong) AnnotationSignature *signature;
@property (nonatomic, strong) SignatureViewController *signViewCtr;
@property (nonatomic, assign) BOOL isShowList;
@property (nonatomic, assign) UIUserInterfaceSizeClass currentSizeclass;
@property (nonatomic, assign) BOOL isReview;
@property (nonatomic, assign) BOOL isShowVerifyInfo;
@property (nonatomic, strong) CustomIOSAlertView *customAlertView;
@property (nonatomic, strong) SignToolHandler *signToolHandler;
@property (nonatomic, weak) FormAnnotHandler *formAnnotHandler;
@property (nonatomic, assign) BOOL shouldShowMenuOnSelection;
@property (nonatomic, assign) BOOL isUseLTVVerify;
@property (nonatomic, assign) unsigned int currentSigState;
//ltv
@property (nonatomic, assign) BOOL isVerifySig;
@property (nonatomic, assign) BOOL isUseExpire;
@property (nonatomic, assign) BOOL isIgnoreDocInfo;
@property (nonatomic, assign) FSLTVVerifierTimeType sigTimeType;
@property (nonatomic, assign) FSLTVVerifierVerifyMode verifyMode;
@property (nonatomic, strong) FSSignatureVerifyResult *ltvVerifyResult;
@property (nonatomic, assign) FSSignatureVerifyResultLTVState ltvState;
@property (nonatomic, assign) BOOL isFileChanged;

@property (nonatomic, assign) int certIndex;
@property (nonatomic, assign) int storeCertIndex;
@end
static unsigned long get_file_size(const char *path);
@implementation DigitalSignatureAnnotHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _extensionsManager = extensionsManager;
        self.signToolHandler = (SignToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Signature];

        self.shouldShowMenu = NO;
        self.editAnnot = nil;
        self.digitalSignDic = [[NSMutableDictionary alloc] init];
        
        self.isUseLTVVerify = !FSPDFViewCtrl.fipsMode;
        
        // FSLTVVerifierVerfiryModeAcrobat
        self.isVerifySig = YES;
        self.isUseExpire = YES;
        self.isIgnoreDocInfo = NO;
        self.sigTimeType = FSLTVVerifierSignatureCreationTime;
        self.verifyMode = FSLTVVerifierVerifyModeAcrobat;
        
        // FSLTVVerifierVerifyModeETSI
//        self.isVerifySig = YES;
//        self.isUseExpire = NO;
//        self.isIgnoreDocInfo = NO;
//        self.sigTimeType = FSLTVVerifierSignatureTSTTime;
//        self.verifyMode = FSLTVVerifierVerifyModeETSI;
        
        self.ltvVerifyResult = nil;
        self.ltvState = FSSignatureVerifyResultLTVStateInactive;
        self.isFileChanged = NO;
        self.currentSigState = 0;
        
        self.certIndex = -1;
        self.storeCertIndex = -1;
        
    }
    return self;
}

- (SignatureListViewController *)signatureListCtr {
    if (!_signatureListCtr) {
        _signatureListCtr = [[SignatureListViewController alloc] init];
        _signatureListCtr.isFieldSigList = YES;
    }
    return _signatureListCtr;
}

- (FormAnnotHandler *)formAnnotHandler{
    if (!_extensionsManager.config.loadForm) {
        return nil;
    }
    if (!_formAnnotHandler) {
        for (id<IAnnotHandler> annotHandler in _extensionsManager.annotHandlers) {
            if ([annotHandler isKindOfClass:[FormAnnotHandler class]]) {
                _formAnnotHandler = (FormAnnotHandler *)annotHandler;
                break;
            }
        }
    }
    return _formAnnotHandler;
}

- (FSAnnotType)getType {
    return FSAnnotWidget;
}

- (BOOL)annotCanAnswer:(FSAnnot *)annot {
    return YES;
}

- (FSRectF *)getAnnotBBox:(FSAnnot *)annot {
    return [annot getRect];
}

- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    if (self.shouldShowMenuOnSelection) {
        return [self.formAnnotHandler isHitAnnot:annot point:point];
    }
    CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    pvRect = CGRectInset(pvRect, -30, -30);
    CGPoint pvPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:point pageIndex:annot.pageIndex];
    if (CGRectContainsPoint(pvRect, pvPoint)) {
        return YES;
    }

    return NO;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    if (self.shouldShowMenuOnSelection) {
        [self.formAnnotHandler onAnnotSelected:annot];
        return;
    }
    self.editAnnot = annot;
    FSPDFPage *page = [self.editAnnot getPage];
    float x = self.editAnnot.fsrect.left + (self.editAnnot.fsrect.right - self.editAnnot.fsrect.left) * 0.5;
    float y = self.editAnnot.fsrect.bottom + (self.editAnnot.fsrect.top - self.editAnnot.fsrect.bottom) * 0.5;

    FSPointF *pt = [[FSPointF alloc] init];
    [pt set:x y:y];
    FSAnnot *annotTemp = nil;
    @try {
        annotTemp = [page getAnnotAtPoint:pt tolerance:0];
    } @catch (NSException *exception) {
    }
    if (annotTemp == nil || [annotTemp getType] != FSAnnotWidget) {
        return;
    }
    FSField *field = [[[FSWidget alloc] initWithAnnot:annotTemp] getField];
    if ([field getType] != FSFieldTypeSignature) {
        return;
    }
    FSSignature *sig = [[FSSignature alloc] initWithField:field];
    self.currentSelectSign = sig;

    BOOL isSigned = [sig isSigned];

    NSMutableArray *array = [NSMutableArray array];

    self.shouldShowMenu = YES;
    self.currentSizeclass = SIZECLASS;
    if (isSigned) {
        MenuItem *vaildDigitalSign = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kVerifySign") object:self action:@selector(vaildDigitalSign)];
        MenuItem *cancel = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kCancel") object:self action:@selector(cancel)];
        [array addObject:vaildDigitalSign];
        [array addObject:cancel];
        _extensionsManager.menuControl.menuItems = array;
        [self showAnnotMenu];
    } else {
        if ([_pdfViewCtrl isRMSProtected]) {
            return;
        }
        
        BOOL canFillForm = [Utility canFillForm:_pdfViewCtrl];
        if (!canFillForm) {
            return;
        }

        FSFieldFlags flags = (FSFieldFlags)[field getFlags];
        if (flags == FSFieldFlagReadOnly) {
            [_extensionsManager setCurrentAnnot:nil];
            return;
        }

        MenuItem *addSign = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kAddSign") object:self action:@selector(sign)];
        MenuItem *signList = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSignList") object:self action:@selector(signList)];
        MenuItem *deleteSign = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kDeleteSign") object:self action:@selector(deleteSign)];
        [array addObject:addSign];
        [array addObject:signList];
        [array addObject:deleteSign];
        _extensionsManager.menuControl.menuItems = array;

        //Fill the necessary field of signature for signning if there is no such one.
        FSPDFDictionary *sigDict = [sig getSignatureDict];
        if (![sigDict hasKey:@"Filter"]) {
            FSPDFObject *value = [FSPDFObject createFromName:@"Adobe.PPKLite"];
            [sigDict setAt:@"Filter" pdf_object:value];
        }
        if (![sigDict hasKey:@"SubFilter"]) {
            FSPDFObject *value = [FSPDFObject createFromName:@"adbe.pkcs7.detached"];
            [sigDict setAt:@"SubFilter" pdf_object:value];
        }

        [self addFieldSign];
    }
}

- (BOOL)addAnnot:(FSAnnot *)annot {
    return [self addAnnot:annot addUndo:YES];
}

- (BOOL)addAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    return NO;
}

- (BOOL)modifyAnnot:(FSAnnot *)annot {
    return [self modifyAnnot:annot addUndo:YES];
}

- (BOOL)modifyAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    return NO;
}

- (BOOL)removeAnnot:(FSAnnot *)annot {
    return [self removeAnnot:annot addUndo:YES];
}

- (BOOL)removeAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    return NO;
}

- (void)onReviewStateChanged:(BOOL)start {
    if (start) {
        _isReview = YES;
    } else {
        _isReview = NO;
    }
}

- (void)addFieldSign {
    NSString *selectSig = [AnnotationSignature getSignatureSelected];
    AnnotationSignature *signnature = [AnnotationSignature getSignature:selectSig];
    if (signnature.certMD5 && signnature.certPasswd && signnature.certFileName) {
        _isAdded = YES;
        self.signature = signnature;
        self.annotImage = [AnnotationSignature getSignatureImage:signnature.name];

        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.editAnnot.fsrect pageIndex:self.editAnnot.pageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.editAnnot.pageIndex needRender:NO];

        [self showAnnotMenu];
    } else {
        if ([AnnotationSignature getCertSignatureList].count <= 0) {
            SignatureViewController *signatureCtr = [[SignatureViewController alloc] initWithUIExtensionsManager:_extensionsManager];
            self.signViewCtr = signatureCtr;
            [signatureCtr fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
            signatureCtr.isFieldSig = YES;
            signatureCtr.currentSignature = nil;
            DigitalSignatureAnnotHandler *__weak weakSelf = self;
            signatureCtr.saveHandler = ^{
                NSString *selectSig = [AnnotationSignature getSignatureSelected];
                AnnotationSignature *signnature = [AnnotationSignature getSignature:selectSig];
                DigitalSignatureAnnotHandler *strongSelf = weakSelf;
                assert(strongSelf);
                strongSelf->_isAdded = YES;
                strongSelf.signature = signnature;
                strongSelf.annotImage = [AnnotationSignature getSignatureImage:signnature.name];
                int pageIndex = strongSelf.editAnnot.pageIndex;
                CGRect newRect = [strongSelf->_pdfViewCtrl convertPdfRectToPageViewRect:strongSelf.editAnnot.fsrect pageIndex:pageIndex];
                newRect = CGRectInset(newRect, -30, -30);
                [strongSelf->_pdfViewCtrl refresh:newRect pageIndex:pageIndex needRender:NO];
                weakSelf.shouldShowMenu = YES;
                [strongSelf showAnnotMenu];
            };
            signatureCtr.cancelHandler = ^{
                DigitalSignatureAnnotHandler *strongSelf = weakSelf;
                assert(strongSelf);
                [strongSelf->_extensionsManager setCurrentAnnot:nil];
            };
            weakSelf.shouldShowMenu = NO;
            UIViewController *rootViewController = self->_pdfViewCtrl.fs_viewController;
            [rootViewController presentViewController:signatureCtr
                                             animated:NO
                                           completion:^{

                                           }];
        } else {
            [self signList];
        }
    }
}

- (void)cancel {
    [_extensionsManager setCurrentAnnot:nil];
}

#pragma mark do ltv verify
- (UIView *)createCertContentView:(NSMutableDictionary *)infos{
    int contentViewHeight = 0;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 290)];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 7;
    contentView.layer.masksToBounds = YES;
    
    UILabel *titleLable = [[UILabel alloc] init];
    titleLable.text = FSLocalizedForKey(@"kAddTrustedCertTitle");
    titleLable.font = [UIFont boldSystemFontOfSize:16];
    [contentView addSubview:titleLable];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size = [titleLable.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:16], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLable.superview.mas_top).offset(10);
        make.centerX.mas_equalTo(titleLable.superview.mas_centerX);
        make.width.mas_equalTo(size.width + 1);
        make.height.mas_equalTo(size.height);
    }];
    contentViewHeight += 10 + size.height +15 ;
    
    UILabel *signatureViald = [[UILabel alloc] init];
    signatureViald.text = FSLocalizedForKey(@"kAddTrustedCertDesciption");
    
    signatureViald.font = [UIFont systemFontOfSize:15];
    signatureViald.numberOfLines = 0;
    [contentView addSubview:signatureViald];
    size = [signatureViald.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    [signatureViald mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLable.mas_bottom).offset(15);
        make.left.mas_equalTo(signatureViald.superview.mas_left).offset(10);
        make.width.mas_equalTo(size.width + 1);
    }];
    contentViewHeight += size.height +15;
    
    NSMutableArray *keyArr = @[
                               @"kTrustedCertName",
                               @"kTrustedCertIssuer",
                               @"kTrustedCertValidFrom",
                               @"kTrustedCertValidTo",
                               @"kTrustedCertKeyUsage",
                               ].mutableCopy;
    
    NSMutableArray *valueArr = @[
                                 @"name",
                                 @"issuer",
                                 @"certStartDate",
                                 @"certEndDate",
                                 @"keyUsage",
                                 ].mutableCopy;
    
    UILabel *lastLabel = nil;
    
    for (int i = 0 ; i < keyArr.count; i++) {
        float maxRowHeight = 0;
        
        NSString *keyFlag = [keyArr objectAtIndex:i];
        NSString *valueFlag = [valueArr objectAtIndex:i];
        
        NSString *keyText = FSLocalizedForKey(keyFlag);
        NSString *valueText = [infos objectForKey:valueFlag];
        
        CGSize keySize = [keyText boundingRectWithSize:CGSizeMake(contentView.frame.size.width -20 , 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
        
        CGSize valueSize = [valueText boundingRectWithSize:CGSizeMake(contentView.frame.size.width /2 -10 , 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
        
        maxRowHeight = MAX(keySize.height, valueSize.height);
        
        UILabel *keyLabel = [[UILabel alloc] init];
        keyLabel.text = keyText;
        keyLabel.font = [UIFont systemFontOfSize:14];
        keyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
        [contentView addSubview:keyLabel];
        
        [keyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.top.mas_equalTo(contentViewHeight);
            }else{
                make.top.mas_equalTo(lastLabel.mas_bottom).offset(5);
            }
            
            make.left.mas_equalTo(keyLabel.superview.mas_left).offset(10);
            make.width.mas_equalTo(keySize.width +1);
            make.height.mas_equalTo(maxRowHeight);
        }];
        
        UILabel *valueLabel = [[UILabel alloc] init];
        valueLabel.text = valueText;
        valueLabel.font = [UIFont systemFontOfSize:14];
        valueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
        valueLabel.numberOfLines = 0;
        [contentView addSubview:valueLabel];
        
        [valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(contentView.frame.size.width /2 );
            make.right.mas_equalTo(valueLabel.superview.mas_right).offset(-10);
            make.top.mas_equalTo(keyLabel.mas_top);
        }];
        
        lastLabel = keyLabel;
        
        contentViewHeight += 5 + maxRowHeight;
    }
    
    contentViewHeight += 10;
    
    CGRect frame = contentView.frame;
    frame.size.height = contentViewHeight;
    contentView.frame = frame;
    
    return contentView;
}

- (UIView *)createAlertContentView:(NSMutableDictionary *)infos{
    int contentViewHeight = 0;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 290)];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 7;
    contentView.layer.masksToBounds = YES;
    
    UILabel *titleLable = [[UILabel alloc] init];
    titleLable.text = FSLocalizedForKey(@"kVerifyTitle");
    titleLable.font = [UIFont boldSystemFontOfSize:16];
    titleLable.textColor = BlackThemeTextColor;
    [contentView addSubview:titleLable];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size = [titleLable.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:16], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLable.superview.mas_top).offset(10);
        make.centerX.mas_equalTo(titleLable.superview.mas_centerX);
        make.width.mas_equalTo(size.width + 1);
        make.height.mas_equalTo(size.height);
    }];
    contentViewHeight += 10 + size.height +15 ;
    
    FSSignatureStates status = (FSSignatureStates)self.currentSigState;
    BOOL isFileChange = [[infos objectForKey:@"isFileChange"] boolValue];
    
    UILabel *signatureViald = [[UILabel alloc] init];
    if (status & FSSignatureStateVerifyValid || status == FSSignatureStateVerifyNoChange) {
        if (isFileChange) {
            signatureViald.text = FSLocalizedForKey(@"kVerifyVaildModifyResult");
        } else {
            signatureViald.text = FSLocalizedForKey(@"kVerifyVaildResult");
        }
        
    } else if (status & FSSignatureStateVerifyInvalid) {
        signatureViald.text = FSLocalizedForKey(@"kVerifyInvaildResult");
    }
    else if (status & FSSignatureStateVerifyErrorByteRange){
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultErrorByteRange");
    } else if (status & FSSignatureStateVerifyIssueUnknown){
        signatureViald.text = FSLocalizedForKey(@"kVerifyUnknownResult");
    } else if (status & FSSignatureStateVerifyIssueExpire){
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultIssueExpire");
    } else if (status & FSSignatureStateVerifyErrorData){
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultErrorData");
    } else {
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultInvaildAndOther");
    }
    
    signatureViald.font = [UIFont systemFontOfSize:15];
    signatureViald.numberOfLines = 0;
    [contentView addSubview:signatureViald];
    size = [signatureViald.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    [signatureViald mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLable.mas_bottom).offset(15);
        make.left.mas_equalTo(signatureViald.superview.mas_left).offset(10);
        make.width.mas_equalTo(size.width + 1);
    }];
    contentViewHeight += size.height +15;
    
    if (self.ltvState == FSSignatureVerifyResultLTVStateEnable) {
        UILabel *ltvStateLabel = [[UILabel alloc] init];
        ltvStateLabel.text = FSLocalizedForKey(@"kLTVStateEnable");
        ltvStateLabel.font = [UIFont systemFontOfSize:15];
        ltvStateLabel.numberOfLines = 0;
        [contentView addSubview:ltvStateLabel];
        CGSize ltvsize = [ltvStateLabel.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
        [ltvStateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(signatureViald.mas_bottom).offset(15);
            make.left.mas_equalTo(ltvStateLabel.superview.mas_left).offset(10);
            make.width.mas_equalTo(ltvsize.width + 1);
        }];
        contentViewHeight += ltvsize.height +15;
    }
    
    NSMutableArray *keyArr = @[
                               @"kCertIssuer",
                               @"kCertSerialNum",
                               @"kCertEmail",
                               @"kCertStartTime",
                               @"kCertEndTime",
                               @"kSignDate",
                               ].mutableCopy;
    NSMutableArray *valueArr = @[
                                 @"certPublisher",
                                 @"certSerialNum",
                                 @"certEmailInfo",
                                 @"certStartDate",
                                 @"certEndDate",
                                 @"signDate",
                                 ].mutableCopy;
    
    NSDateFormatter *signdateFormatter = [[NSDateFormatter alloc] init];
    [signdateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [signdateFormatter stringFromDate:[Utility convertFSDateTime2NSDate: [infos objectForKey:@"signDate"]]];
    if (dateStr == nil || dateStr.length == 0) {
        dateStr = @"";
    }
    [infos setObject:dateStr forKey:@"signDate"];
    
    UILabel *lastLabel = nil;
    
    for (int i = 0 ; i < keyArr.count; i++) {
        float maxRowHeight = 0;
        
        NSString *keyFlag = [keyArr objectAtIndex:i];
        NSString *valueFlag = [valueArr objectAtIndex:i];
        
        NSString *keyText = FSLocalizedForKey(keyFlag);
        NSString *valueText = [infos objectForKey:valueFlag];
        
        CGSize keySize = [keyText boundingRectWithSize:CGSizeMake(contentView.frame.size.width -20 , 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
        
        CGSize valueSize = [valueText boundingRectWithSize:CGSizeMake(contentView.frame.size.width /2 -10 , 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
        
        maxRowHeight = MAX(keySize.height, valueSize.height);
        
        UILabel *keyLabel = [[UILabel alloc] init];
        keyLabel.text = keyText;
        keyLabel.font = [UIFont systemFontOfSize:14];
        keyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
        [contentView addSubview:keyLabel];
        
        [keyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.top.mas_equalTo(contentViewHeight);
            }else{
                make.top.mas_equalTo(lastLabel.mas_bottom).offset(5);
            }
            
            make.left.mas_equalTo(keyLabel.superview.mas_left).offset(10);
            make.width.mas_equalTo(keySize.width +1);
            make.height.mas_equalTo(maxRowHeight);
        }];
        
        UILabel *valueLabel = [[UILabel alloc] init];
        valueLabel.text = valueText;
        valueLabel.font = [UIFont systemFontOfSize:14];
        valueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
        valueLabel.numberOfLines = 0;
        [contentView addSubview:valueLabel];
        
        [valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(contentView.frame.size.width /2 );
            make.right.mas_equalTo(valueLabel.superview.mas_right).offset(-10);
            make.top.mas_equalTo(keyLabel.mas_top);
        }];
        
        lastLabel = keyLabel;
        
        contentViewHeight += 5 + maxRowHeight;
    }

    contentViewHeight += 10;
    
    CGRect frame = contentView.frame;
    frame.size.height = contentViewHeight;
    contentView.frame = frame;
    
    return contentView;
}

-(BOOL)hasModifiedDocument{
    BOOL isFileChanged = NO;
    NSString *fileName = @"";
    if (self.signToolHandler.getDocPath) {
        fileName = self.signToolHandler.getDocPath();
    }
    unsigned long fileLength = get_file_size([fileName UTF8String]);
    @try {
        FSInt32Array *byteRanges = [self.currentSelectSign getByteRangeArray];
        if ([byteRanges getSize] == 4) {
            unsigned int r1 = [byteRanges getAt:2];
            unsigned int r2 = [byteRanges getAt:3];
            if (fileLength != (r1 + r2)) {
                isFileChanged = YES;
                return isFileChanged;
            }
        }
    }
    @catch(NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    return NO;
}

-(void)doLTVVerify {
    @try {
        [_pdfViewCtrl fs_showHUDLoading:FSLocalizedForKey(@"kVerifyingDigital") process:^{
            if (self.currentSelectSign == nil) return;
            FSLTVVerifier *ltvVerifier = [[FSLTVVerifier alloc] initWithDocument:self->_pdfViewCtrl.currentDoc is_verify_signature:self.isVerifySig use_expired_tst:self.isUseExpire ignore_doc_info:self.isIgnoreDocInfo time_type:self.sigTimeType];
            [ltvVerifier setVerifyMode:self.verifyMode];
            
            SignTrustedCertStoreCallback *signTrustedCertStoreCallback = [[SignTrustedCertStoreCallback alloc] init];
            [ltvVerifier setTrustedCertStoreCallback:signTrustedCertStoreCallback];
            
            FSSignatureVerifyResultArray *resultArr = [ltvVerifier verifySignature:self.currentSelectSign];
            
            if ([resultArr getSize] == 0) {
                NSLog(@"error");
            }
            
            self.ltvVerifyResult = [resultArr getAt:0];
            self.currentSigState = [self.ltvVerifyResult getSignatureState];
            self.ltvState = [self.ltvVerifyResult getLTVState];
            self.isFileChanged = [self hasModifiedDocument];
            
            dispatch_main_async_safe(^{
                [self showVerifyResult];
            });
        }];

    } @catch (NSException *exception) {
        NSLog(@"error");
    }
    
}

-(NSData *)getCertFromSignatureContent:(FSSignature *)signature {
    NSData *result = nil;
    NSMutableArray *certArr = [FSPDFCertUtil getCertFromSignatureContent:signature];
    
    if (self.certIndex == -1) {
        self.certIndex = 0;
    }
    else{
        if (self.certIndex >= certArr.count -1) {
            self.certIndex = 0;
        }else{
            self.certIndex++;
        }
    }
    
    result = [certArr objectAtIndex:certArr.count -1];
    return result;
}

-(void)showVerifyResult{
    FSSignature *sig = self.currentSelectSign;
    
    int status = self.currentSigState;
    
    NSMutableDictionary *signatureVerifyInfo = @{
                                                 @"signDate":[sig getSignTime],
                                                 @"certSerialNum":[sig getCertificateInfo:@"SerialNumber"],
                                                 @"certStartDate":[sig getCertificateInfo:@"ValidPeriodFrom"],
                                                 @"certEndDate":[sig getCertificateInfo:@"ValidPeriodTo"],
                                                 @"certPublisher":@"",
                                                 @"certEmailInfo":@"",
                                                 @"status":@(status),
                                                 @"ltvState":@(self.ltvState),
                                                 }.mutableCopy;
    

    NSString *certIssuer = [sig getCertificateInfo:@"Issuer"];
    NSRange r = [certIssuer rangeOfString:@"CN="];
    if (0 < r.length) {
        certIssuer = [certIssuer substringFromIndex:r.location + 3];
        r = [certIssuer rangeOfString:@","];
        if (0 < r.length) {
            [signatureVerifyInfo setObject:[certIssuer substringToIndex:r.location] forKey:@"certPublisher"];
        }
    }
    
    NSString *certSubject = [sig getCertificateInfo:@"Subject"];
    r = [certSubject rangeOfString:@"E="];
    if (0 < r.length) {
        certSubject = [certSubject substringFromIndex:r.location + 2];
        r = [certSubject rangeOfString:@","];
        if (0 < r.length) {
            [signatureVerifyInfo setObject:[certSubject substringToIndex:r.location] forKey:@"certEmailInfo"];
        }
    }
    
    BOOL isFileChanged = NO;
    NSString *fileName = @"";
    if (self.signToolHandler.getDocPath) {
        fileName = self.signToolHandler.getDocPath();
    }
    unsigned long fileLength = get_file_size([fileName UTF8String]);
    FSInt32Array *byteRanges = [sig getByteRangeArray];
    if ([byteRanges getSize] == 4) {
        unsigned int r1 = [byteRanges getAt:2];
        unsigned int r2 = [byteRanges getAt:3];
        if (fileLength != (r1 + r2)) {
            isFileChanged = YES;
        }
    }
    
    [signatureVerifyInfo setObject:@(isFileChanged) forKey:@"isFileChanged"];
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    self.customAlertView = alertView;
    [alertView setContainerView:[self createAlertContentView:signatureVerifyInfo]];
    
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kViewCert"), nil]];
    
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        [alertView close];
        if (buttonIndex == 1) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CerOperationView" bundle:[NSBundle bundleForClass:[self class]]];
            UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"CheckCerViewCtrNavC"];
            CheckCerViewCtr *VC = (CheckCerViewCtr *)nav.topViewController;
            [VC setUIExtensionsManager:self->_extensionsManager];
            [VC setCurrentSelectSign:sig];
            [nav fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
            [self->_pdfViewCtrl.fs_viewController presentViewController:nav animated:YES completion:nil];
            return ;
        }
        self->_isShowVerifyInfo = NO;
        [self->_extensionsManager setCurrentAnnot:nil];
        self.certIndex = -1;
    }];
    
    [alertView setUseMotionEffects:true];
    
    [alertView show];
   
    _isShowVerifyInfo = YES;
}

- (void)verify {
    [self vaildDigitalSign];
}

- (void)vaildDigitalSign {
    if (self.isUseLTVVerify) {
        [self doLTVVerify];
        return ;
    }
    
    FSSignature *sig = self.currentSelectSign;
    CERT_INFO *info = [[CERT_INFO alloc] init];

    FSSignatureStates status = [self.signToolHandler verifyDigitalSignature:nil signature:sig];
    info.signDate = [sig getSignTime];
    info.certSerialNum = [sig getCertificateInfo:@"SerialNumber"];
    info.certStartDate = [sig getCertificateInfo:@"ValidPeriodFrom"];
    info.certEndDate = [sig getCertificateInfo:@"ValidPeriodTo"];
    NSString *certIssuer = [sig getCertificateInfo:@"Issuer"];
    NSRange r = [certIssuer rangeOfString:@"CN="];
    if (0 < r.length) {
        certIssuer = [certIssuer substringFromIndex:r.location + 3];
        r = [certIssuer rangeOfString:@","];
        if (0 < r.length) {
            info.certPublisher = [certIssuer substringToIndex:r.location];
        }
    }

    NSString *certSubject = [sig getCertificateInfo:@"Subject"];
    r = [certSubject rangeOfString:@"E="];
    if (0 < r.length) {
        certSubject = [certSubject substringFromIndex:r.location + 2];
        r = [certSubject rangeOfString:@","];
        if (0 < r.length) {
            info.certEmailInfo = [certSubject substringToIndex:r.location];
        }
    }

    BOOL isFileChanged = NO;
    NSString *fileName = @"";
    if (self.signToolHandler.getDocPath) {
        fileName = self.signToolHandler.getDocPath();
    }
    unsigned long fileLength = get_file_size([fileName UTF8String]);
    FSInt32Array *byteRanges = [sig getByteRangeArray];
    if ([byteRanges getSize] == 4) {
        unsigned int r1 = [byteRanges getAt:2];
        unsigned int r2 = [byteRanges getAt:3];
        if (fileLength != (r1 + r2)) {
            isFileChanged = YES;
        }
    }

    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    self.customAlertView = alertView;
    [alertView setContainerView:[self createAlertContentView:status certInfo:info isFileChange:isFileChanged]];

    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:FSLocalizedForKey(@"kOK"), nil]];

    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        [alertView close];
        self->_isShowVerifyInfo = NO;
    }];

    [alertView setUseMotionEffects:true];

    [alertView show];
    _isShowVerifyInfo = YES;
    [_extensionsManager setCurrentAnnot:nil];
}

static unsigned long get_file_size(const char *path) {
    unsigned long filesize = -1;
    struct stat statbuff;
    if (stat(path, &statbuff) < 0) {
        return filesize;
    } else {
        filesize = (unsigned long) statbuff.st_size;
    }
    return filesize;
}

static const char* statusMsg[] = {
    "Unknonw", //0x00000000
    "Unsigned signature.",//0x00000001
    "Signed signature.",//0x00000002
    "Verification state of a signature is valid.",//0x00000004
    "Verification state of a signature is invalid.",//0x00000008
    "Signature data is corrupted (that means the signature data cannot be parsed properly).", //0x00000010
    "Unsupported signature.",//0x00000020
    "Non expected byte range.",//0x00000040
    "The document has changed within the scope of the signature.",//0x00000080
    "Signature cannot be trusted (containing aggression). ",//0x00000100
    "Signature does not have any data for signing. ",//0x00000200
    "",
    "",
    "Verification state of the issuer is valid.",//0x00001000
    "Verification state of the issuer is unknown.",//0x00002000
    "Certificate for verifying issuer is revoked.",//0x00004000
    "Certificate for verifying issuer is expired.",//0x00008000
    "Not check the issuer.",//0x00010000
    "The verified issue is current issuer.",//0x00020000
    "No timestamp or not check timestamp.", //0x00040000
    "The signature is a timestamp signature.",//0x00080000
    "Verification state of the timestamp is valid.",  //0x00100000
    "Verification state of the timestamp is invalid.",//0x00200000
    "Verification state of the timestamp is expired.",//0x00400000
    "Verification state of the timestamp issuer is unknown.",//0x00800000
    "Verification state of the timestamp issuer is valid.",//0x01000000
    "Verification state of the timestamp time is valid, since the times is before the expiration date."//0x02000000
};
- (NSString*) getStatusErrorString:(FSSignatureStates) status
{
    NSString* result = nil;
    int index = 0;
    while (index < 32)
    {
        int length = sizeof(statusMsg)/sizeof(const char*);
        if(((status >> index)&0x1) && (index > 3 && index < length-1))
        {
            NSString* status = [[NSString alloc] initWithUTF8String:statusMsg[index+1]];
            if(!result)
                result = status;
            else
                result = [result stringByAppendingString:status];
        }
        index++;
    }
    return result;
}

- (UIView *)createAlertContentView:(FSSignatureStates)status certInfo:(CERT_INFO *)certInfo isFileChange:(BOOL)isFileChange {
    int contentViewHeight = 0;
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 290)];
    contentView.backgroundColor = [UIColor whiteColor];
    UILabel *titleLable = [[UILabel alloc] init];
    titleLable.text = FSLocalizedForKey(@"kVerifyTitle");
    titleLable.font = [UIFont boldSystemFontOfSize:16];
    titleLable.textColor = BlackThemeTextColor;
    [contentView addSubview:titleLable];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size = [titleLable.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:16], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLable.superview.mas_top).offset(10);
        make.centerX.mas_equalTo(titleLable.superview.mas_centerX);
        make.width.mas_equalTo(size.width + 1);
        make.height.mas_equalTo(size.height);
    }];
    contentViewHeight += size.height +10;

    UILabel *signatureViald = [[UILabel alloc] init];
    if (status & FSSignatureStateVerifyValid || status == FSSignatureStateVerifyNoChange) {
        if (isFileChange) {
            signatureViald.text = FSLocalizedForKey(@"kVerifyVaildModifyResult");
        } else {
            signatureViald.text = FSLocalizedForKey(@"kVerifyVaildResult");
        }
        
    } else if (status & FSSignatureStateVerifyInvalid) {
        signatureViald.text = FSLocalizedForKey(@"kVerifyInvaildResult");
    }
    else if (status & FSSignatureStateVerifyErrorByteRange){
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultErrorByteRange");
    } else if (status & FSSignatureStateVerifyIssueUnknown){
        signatureViald.text = FSLocalizedForKey(@"kVerifyUnknownResult");
    } else if (status & FSSignatureStateVerifyIssueExpire){
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultIssueExpire");
    } else if (status & FSSignatureStateVerifyErrorData){
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultErrorData");
    } else {
        signatureViald.text = FSLocalizedForKey(@"kVerifyResultInvaildAndOther");
    }
    signatureViald.font = [UIFont systemFontOfSize:15];
    signatureViald.numberOfLines = 0;
    [contentView addSubview:signatureViald];
    size = [signatureViald.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    [signatureViald mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLable.mas_bottom).offset(15);
        make.left.mas_equalTo(signatureViald.superview.mas_left).offset(10);
        make.width.mas_equalTo(size.width + 1);
    }];
    contentViewHeight += size.height +15;
    
//    NSString* detail = [self getStatusErrorString:status];
//    UILabel *signatureDetail = [[UILabel alloc] init];
//    signatureDetail.text = detail;
//    signatureDetail.font = [UIFont systemFontOfSize:15];
//    signatureDetail.numberOfLines = 0;
//    [contentView addSubview:signatureDetail];
//    size = [signatureDetail.text boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
//    [signatureDetail mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(signatureViald.mas_bottom).offset(15);
//        make.left.mas_equalTo(signatureDetail.superview.mas_left).offset(10);
//        make.width.mas_equalTo(size.width + 1);
//    }];
//    if (detail.length >0) {
//        contentViewHeight += size.height + 15;
//    }else{
//        contentViewHeight += 15;
//    }
    
    float maxWidth = 0;
    CGSize issuerSize = [FSLocalizedForKey(@"kCertIssuer") boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    if (maxWidth < issuerSize.width) {
        maxWidth = issuerSize.width;
    }
    CGSize serialNumSize = [FSLocalizedForKey(@"kCertSerialNum") boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    if (maxWidth < serialNumSize.width) {
        maxWidth = serialNumSize.width;
    }
    CGSize emailSize = [FSLocalizedForKey(@"kCertEmail") boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    if (maxWidth < emailSize.width) {
        maxWidth = emailSize.width;
    }
    CGSize certDateSSize = [FSLocalizedForKey(@"kCertStartTime") boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    if (maxWidth < certDateSSize.width) {
        maxWidth = certDateSSize.width;
    }
    CGSize certDateESize = [FSLocalizedForKey(@"kCertEndTime") boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    if (maxWidth < certDateESize.width) {
        maxWidth = certDateESize.width;
    }
    CGSize signDateSize = [FSLocalizedForKey(@"kSignDate") boundingRectWithSize:CGSizeMake(300, 100) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    if (maxWidth < signDateSize.width) {
        maxWidth = signDateSize.width;
    }

    UILabel *issuerKeyLabel = [[UILabel alloc] init];
    issuerKeyLabel.text = FSLocalizedForKey(@"kCertIssuer");
    issuerKeyLabel.font = [UIFont systemFontOfSize:14];
    issuerKeyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:issuerKeyLabel];
    [issuerKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(signatureDetail.mas_bottom).offset(15);
        make.top.mas_equalTo(signatureViald.mas_bottom).offset(15);
        make.left.mas_equalTo(issuerKeyLabel.superview.mas_left).offset(10);
        make.width.mas_equalTo(issuerSize.width + 1);
        make.height.mas_equalTo(issuerSize.height);
    }];
    
    UILabel *issuerValueLabel = [[UILabel alloc] init];
    issuerValueLabel.text = certInfo.certPublisher;
    issuerValueLabel.font = [UIFont systemFontOfSize:14];
    issuerValueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    issuerValueLabel.numberOfLines = 0;
    [contentView addSubview:issuerValueLabel];
    [issuerValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(maxWidth + 10 + 2);
        make.top.mas_equalTo(issuerKeyLabel.mas_top);
        make.right.mas_equalTo(issuerValueLabel.superview.mas_right).offset(-10);
    }];
    contentViewHeight += issuerSize.height + 15;
    
    UILabel *serialNumKeyLabel = [[UILabel alloc] init];
    serialNumKeyLabel.text = FSLocalizedForKey(@"kCertSerialNum");
    serialNumKeyLabel.font = [UIFont systemFontOfSize:14];
    serialNumKeyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:serialNumKeyLabel];
    [serialNumKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        if (issuerValueLabel.text.length < 1) {
            make.top.mas_equalTo(issuerKeyLabel.mas_bottom).offset(5);
        } else {
            make.top.mas_equalTo(issuerValueLabel.mas_bottom).offset(5);
        }
        make.left.mas_equalTo(serialNumKeyLabel.superview.mas_left).offset(10);
        make.width.mas_equalTo(serialNumSize.width + 1);
        make.height.mas_equalTo(serialNumSize.height);
    }];

    UILabel *serialNumValueLabel = [[UILabel alloc] init];
    serialNumValueLabel.text = certInfo.certSerialNum;
    serialNumValueLabel.font = [UIFont systemFontOfSize:14];
    serialNumValueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    serialNumValueLabel.numberOfLines = 0;
    [contentView addSubview:serialNumValueLabel];
    [serialNumValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(maxWidth + 10 + 2);
        make.top.mas_equalTo(serialNumKeyLabel.mas_top);
        make.right.mas_equalTo(serialNumValueLabel.superview.mas_right).offset(-10);
    }];
    contentViewHeight += serialNumSize.height + 5;

    UILabel *eamilKeyLabel = [[UILabel alloc] init];
    eamilKeyLabel.text = FSLocalizedForKey(@"kCertEmail");
    eamilKeyLabel.font = [UIFont systemFontOfSize:14];
    eamilKeyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:eamilKeyLabel];
    [eamilKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        if (serialNumValueLabel.text.length < 1) {
            make.top.mas_equalTo(serialNumKeyLabel.mas_bottom).offset(5);
        } else {
            make.top.mas_equalTo(serialNumValueLabel.mas_bottom).offset(5);
        }
        make.left.mas_equalTo(eamilKeyLabel.superview.mas_left).offset(10);
        make.width.mas_equalTo(emailSize.width + 1);
        make.height.mas_equalTo(emailSize.height);
    }];

    UILabel *emailValueLabel = [[UILabel alloc] init];
    emailValueLabel.text = certInfo.certEmailInfo;
    emailValueLabel.font = [UIFont systemFontOfSize:14];
    emailValueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    emailValueLabel.numberOfLines = 0;
    [contentView addSubview:emailValueLabel];
    [emailValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(maxWidth + 10 + 2);
        make.top.mas_equalTo(eamilKeyLabel.mas_top);
        make.right.mas_equalTo(emailValueLabel.superview.mas_right).offset(-10);
    }];
    contentViewHeight += emailSize.height + 5;
    
    UILabel *certVaildDateKeyLabel = [[UILabel alloc] init];
    certVaildDateKeyLabel.text = FSLocalizedForKey(@"kCertStartTime");
    certVaildDateKeyLabel.font = [UIFont systemFontOfSize:14];
    certVaildDateKeyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:certVaildDateKeyLabel];
    [certVaildDateKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        if (emailValueLabel.text.length < 1) {
            make.top.mas_equalTo(eamilKeyLabel.mas_bottom).offset(5);
        } else {
            make.top.mas_equalTo(emailValueLabel.mas_bottom).offset(5);
        }
        make.left.mas_equalTo(certVaildDateKeyLabel.superview.mas_left).offset(10);
        make.width.mas_equalTo(certDateSSize.width + 1);
        make.height.mas_equalTo(certDateSSize.height);
    }];

    UILabel *vaildDateValueLabel = [[UILabel alloc] init];
    vaildDateValueLabel.text = certInfo.certStartDate;
    vaildDateValueLabel.font = [UIFont systemFontOfSize:14];
    vaildDateValueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:vaildDateValueLabel];
    [vaildDateValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(maxWidth + 10 + 2);
        make.top.mas_equalTo(certVaildDateKeyLabel.mas_top);
        make.bottom.mas_equalTo(certVaildDateKeyLabel.mas_bottom);
        make.right.mas_equalTo(vaildDateValueLabel.superview.mas_right).offset(-10);
    }];
    contentViewHeight += certDateSSize.height + 5;
    
    UILabel *certInvaildDateKeyLabel = [[UILabel alloc] init];
    certInvaildDateKeyLabel.text = FSLocalizedForKey(@"kCertEndTime");
    certInvaildDateKeyLabel.font = [UIFont systemFontOfSize:14];
    certInvaildDateKeyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:certInvaildDateKeyLabel];
    [certInvaildDateKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(certVaildDateKeyLabel.mas_bottom).offset(5);
        make.left.mas_equalTo(certInvaildDateKeyLabel.superview.mas_left).offset(10);
        make.width.mas_equalTo(certDateESize.width + 1);
        make.height.mas_equalTo(certDateESize.height);
    }];

    UILabel *invaildDateValueLabel = [[UILabel alloc] init];
    invaildDateValueLabel.text = certInfo.certEndDate;
    invaildDateValueLabel.font = [UIFont systemFontOfSize:14];
    invaildDateValueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:invaildDateValueLabel];
    [invaildDateValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(maxWidth + 10 + 2);
        make.top.mas_equalTo(certInvaildDateKeyLabel.mas_top);
        make.bottom.mas_equalTo(certInvaildDateKeyLabel.mas_bottom);
        make.right.mas_equalTo(invaildDateValueLabel.superview.mas_right).offset(-10);
    }];
    contentViewHeight += certDateESize.height + 5;

    UILabel *signDateKeyLabel = [[UILabel alloc] init];
    signDateKeyLabel.text = FSLocalizedForKey(@"kSignDate");
    signDateKeyLabel.font = [UIFont systemFontOfSize:14];
    signDateKeyLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:signDateKeyLabel];
    [signDateKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(certInvaildDateKeyLabel.mas_bottom).offset(5);
        make.left.mas_equalTo(signDateKeyLabel.superview.mas_left).offset(10);
        make.width.mas_equalTo(signDateSize.width + 1);
        make.height.mas_equalTo(signDateSize.height);
    }];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    UILabel *signDateValueLabel = [[UILabel alloc] init];
    signDateValueLabel.text = [dateFormatter stringFromDate:[Utility convertFSDateTime2NSDate:certInfo.signDate]];
    signDateValueLabel.font = [UIFont systemFontOfSize:14];
    signDateValueLabel.textColor = [UIColor colorWithRGB:0x8A8A8A];
    [contentView addSubview:signDateValueLabel];
    [signDateValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(maxWidth + 10 + 2);
        make.top.mas_equalTo(signDateKeyLabel.mas_top);
        make.bottom.mas_equalTo(signDateKeyLabel.mas_bottom);
        make.right.mas_equalTo(signDateValueLabel.superview.mas_right).offset(-10);
    }];
    contentViewHeight += signDateSize.height + 5 + 15;
    
    CGRect frame = contentView.frame;
    frame.size.height = contentViewHeight;
    contentView.frame = frame;

    return contentView;
}

- (void)signDigitalSignature:(FSSignature *)sig signArea:(FSRectF *)rect signImagePath:(NSString *)imagePath savePath: (NSString*) pdfFilePath {
    if (pdfFilePath == nil) return;
    NSString *selectSig = [AnnotationSignature getSignatureSelected];
    AnnotationSignature *signature = [AnnotationSignature getSignature:selectSig];

    DIGITALSIGNATURE_PARAM *param = [[DIGITALSIGNATURE_PARAM alloc] init];
    param.certFile = [SIGNATURE_PATH stringByAppendingPathComponent:signature.certMD5];
    param.certPwd = signature.certPasswd;
    param.subfilter = @"adbe.pkcs7.detached";
    param.imagePath = imagePath;
    param.rect = rect;
    param.sigName = signature.name;
    param.signFilePath = pdfFilePath;
    [self.signToolHandler initSignature:sig withParam:param];
    [[NSFileManager defaultManager] removeItemAtPath:param.imagePath error:nil];
    BOOL isSuccess = [self.signToolHandler signSignature:sig withParam:param];
    if (isSuccess) {
        double delayInSeconds = 0.4;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kSaveSignedDocSuccess") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               double delayInSeconds = 0.4;
                                                               dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                                               dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                                                                   if (self.signToolHandler.docChanging) {
                                                                       self->_extensionsManager.isDocModified = NO;
                                                                       self.signToolHandler.docChanging(pdfFilePath);
                                                                   }
                                                               });
                                                               return;
                                                           }];
            [alertController addAction:action];
            [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        });
    } else {
        double delayInSeconds = 0.4;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kError") message:FSLocalizedForKey(@"kSaveSignedDocFailure") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        });
    }

};

- (void)addDigitalSign:(FSSignature *)sig signArea:(FSRectF *)rect signImagePath:(NSString *)imagePath {
    BOOL isAutoSaveSignedDoc = self->_extensionsManager.isAutoSaveSignedDoc;
    if (isAutoSaveSignedDoc) {
        NSString* userSavePath = self->_extensionsManager.signedDocSavePath;
        if (userSavePath == nil) {
            userSavePath = self->_pdfViewCtrl.filePath;
            if (userSavePath == nil) return;
            userSavePath = [userSavePath.stringByDeletingPathExtension stringByAppendingString:@"-signed.pdf"];
        }
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:userSavePath]) {
            [fileManager removeItemAtPath:userSavePath error:nil];
        }
        [self signDigitalSignature:sig signArea:rect signImagePath:imagePath savePath:userSavePath];
        return;
    }
    
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Select;
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:nil];
        typedef void (^DumbBlock)(void);
        DumbBlock __block inputFileName;
        __weak DumbBlock weakInputFileName = nil;
        weakInputFileName = inputFileName = ^() {
            InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kInputNewFileName")
                                                                           message:nil
                                                                buttonClickHandler:^(UIView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    return;
                }
                InputAlertView *inputAlert = (InputAlertView *) alertView;
                NSString *fileName = inputAlert.inputTextField.text;
                
                if ([fileName rangeOfString:@"/"].location != NSNotFound) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kIllegalNameWarning") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                         style:UIAlertActionStyleCancel
                                                                       handler:^(UIAlertAction *action) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //                                                                                                                                   weakInputFileName();
                            });
                            return;
                        }];
                        [alertController addAction:action];
                        [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    });
                    return;
                } else if (fileName.length == 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //                                                                            weakInputFileName();
                    });
                    return;
                }
                
                
                
                NSString *pdfFilePath = [destinationFolder[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", fileName]];
                NSFileManager *fileManager = [[NSFileManager alloc] init];
                if ([fileManager fileExistsAtPath:pdfFilePath]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                               style:UIAlertActionStyleCancel
                                                                             handler:^(UIAlertAction *action) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //                                                                                                                                         weakInputFileName();
                            });
                        }];
                        UIAlertAction *replaceAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kReplace")
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction *action) {
                            [fileManager removeItemAtPath:pdfFilePath error:nil];
                            [self signDigitalSignature:sig signArea:rect signImagePath:imagePath savePath:pdfFilePath];
                            inputFileName = nil;
                        }];
                        [alertController addAction:cancelAction];
                        [alertController addAction:replaceAction];
                        [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    });
                } else {
                    [self signDigitalSignature:sig signArea:rect signImagePath:imagePath savePath:pdfFilePath];
                    inputFileName = nil;
                }
            }
                                                                 cancelButtonTitle:FSLocalizedForKey(@"kCancel")
                                                                 otherButtonTitles:FSLocalizedForKey(@"kOK"), nil];
            inputAlertView.style = TSAlertViewStyleInputText;
            inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
            inputAlertView.usesMessageTextView = NO;
            [inputAlertView show];
        };
        
        inputFileName();
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    UIViewController *rootViewController = self->_pdfViewCtrl.fs_viewController;
    [rootViewController presentViewController:selectDestinationNavController
                                     animated:YES
                                   completion:nil];
}

- (void)sign {
    if (!_isAdded) {
        self.currentSelectSign = nil;
        _annotImage = nil;
        _isAdded = NO;

        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.signature.rect pageIndex:self.signature.pageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.signature.pageIndex needRender:YES];
        return;
    }

    BOOL isSaveTip = [Preference getBoolValue:Module_Signature type:@"SignSaveTip" delaultValue:NO];
    if (!isSaveTip) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kConfirmSign") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 [self deleteSign];
                                                             }];
        UIAlertAction *signAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [self addSign];
                                                               [Preference setBoolValue:Module_Signature type:@"SignSaveTip" value:YES];
                                                           }];
        [alertController addAction:cancelAction];
        [alertController addAction:signAction];
        [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    } else {
        [self addSign];
    }
}

- (void)addSign {
    FSSignature *sig = self.currentSelectSign;
    NSString *selectSig = [AnnotationSignature getSignatureSelected];
    AnnotationSignature *signnature = [AnnotationSignature getSignature:selectSig];
    if (signnature.certMD5 && signnature.certPasswd && signnature.certFileName) {
        NSString *imagePath = [SIGNATURE_PATH stringByAppendingPathComponent:[selectSig stringByAppendingString:@"_i"]];
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.editAnnot.fsrect pageIndex:self.editAnnot.pageIndex];
        UIImage *tmpImage = [Utility scaleToSize:[UIImage imageWithContentsOfFile:imagePath] size:rect.size];
        NSString *tmpPath = [imagePath stringByAppendingString:@"_tmp"];
        [UIImagePNGRepresentation(tmpImage) writeToFile:tmpPath atomically:YES];

        [self addDigitalSign:sig signArea:self.editAnnot.fsrect signImagePath:tmpPath];
    }
    self.annotImage = nil;
    [_extensionsManager setCurrentAnnot:nil];
}

- (void)signList {
    self.shouldShowMenu = NO;
    UIViewController *rootViewController = self->_pdfViewCtrl.fs_viewController;
    if (DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) {
        [self.signatureListCtr fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];
        [rootViewController presentViewController:self.signatureListCtr
                                         animated:YES
                                       completion:^{
                                           self.isShowList = YES;
                                       }];
    } else {
        self.signatureListCtr.modalPresentationStyle = UIModalPresentationPopover;
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.editAnnot.fsrect pageIndex:self.editAnnot.pageIndex];
        self.signatureListCtr.popoverPresentationController.sourceRect = rect;
        self.signatureListCtr.popoverPresentationController.sourceView = rootViewController.view;
        self.signatureListCtr.popoverPresentationController.delegate = self;
        self.signatureListCtr.preferredContentSize = CGSizeMake(300, 420);
        [rootViewController presentViewController:self.signatureListCtr
                                         animated:YES
                                       completion:^{
                                           self.isShowList = YES;
                                       }];
    }

    self.signatureListCtr.delegate = self;
}

#pragma mark <UIPopoverPresentationControllerDelegate>

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    self.isShowList = NO;
    _signatureListCtr = nil;
    if (self.signature) {
        self.shouldShowMenu = YES;
        [self showAnnotMenu];
    } else {
        [_extensionsManager setCurrentAnnot:nil];
    }
}

- (void)deleteSign {
    _isAdded = NO;
    self.annotImage = nil;
    self.signature = nil;

    CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.editAnnot.fsrect pageIndex:self.editAnnot.pageIndex];
    newRect = CGRectInset(newRect, -30, -30);
    [_pdfViewCtrl refresh:newRect pageIndex:self.editAnnot.pageIndex needRender:NO];
    [_extensionsManager setCurrentAnnot:nil];
}

- (void)onAnnotDeselected:(FSAnnot *)annot {
    if (self.shouldShowMenuOnSelection) {
        [self.formAnnotHandler onAnnotDeselected:annot];
        return;
    }
    self.currentSelectSign = nil;
    _annotImage = nil;
    if (_isAdded) {
        [self deleteSign];
        _isAdded = NO;
    }
    MenuControl *annotMenu = _extensionsManager.menuControl;
    if (annotMenu.isMenuVisible) {
        [annotMenu setMenuVisible:NO animated:YES];
    }
}

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if (self.formAnnotHandler && annot.canDelete) {
        self.shouldShowMenuOnSelection = YES;
    }
    return [self.formAnnotHandler onPageViewLongPress:pageIndex recognizer:recognizer annot:annot];
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if (self.shouldShowMenuOnSelection) {
         [_extensionsManager setCurrentAnnot:nil];
         self.shouldShowMenuOnSelection = NO;
        CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        if (pageIndex != annot.pageIndex || ![self isHitAnnot:annot point:pdfPoint]) {
            return YES;
        }
    }
    if (_extensionsManager.currentAnnot == annot) {
        CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        if (pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint]) {
            if (self.shouldShowMenu)
                [self showAnnotMenu];
            return YES;
        } else {
            if (_isAdded) {
                [self deleteSign];
            } else {
                _annotImage = nil;
                _isAdded = NO;
                [_extensionsManager setCurrentAnnot:nil];
            }
            return YES;
        }
    } else {
        [_extensionsManager setCurrentAnnot:annot];
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if (self.shouldShowMenuOnSelection) {
        return [self.formAnnotHandler onPageViewPan:pageIndex recognizer:recognizer annot:annot];
    }
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer annot:(FSAnnot *)annot {
    if ([_extensionsManager getAnnotHandlerByAnnot:annot] == self) {
        CGPoint point = [gestureRecognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        if (pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    if (self.shouldShowMenuOnSelection) {
        [self.formAnnotHandler onDraw:pageIndex inContext:context annot:annot];
        return;
    }
    
    if (_extensionsManager.currentAnnot == annot && pageIndex == annot.pageIndex && _isAdded) {
        if (self.annotImage) {
            CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
            UIImage *image = [Utility scaleToSize:self.annotImage size:rect.size];
            CGContextSaveGState(context);

            CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
            CGContextTranslateCTM(context, 0, rect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
            CGContextDrawImage(context, rect, [image CGImage]);

            CGContextRestoreGState(context);
        }
    }
}

- (void)onDocWillOpen {
    _signViewCtr = nil;
    self.signature = nil;
    _isShowList = NO;
    [self.digitalSignDic removeAllObjects];
}

#pragma mark IRotateChangedListener

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self dismissAnnotMenu];
    if (_isShowList) {
        [self.signatureListCtr dismissViewControllerAnimated:NO
                                                  completion:^{
                                                      self->_signatureListCtr = nil;
                                                  }];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self showAnnotMenu];
    if (_isShowList) {
        [self signList];
    }
}

#pragma mark IScrollViewEventListener

- (void)onScrollViewWillBeginDragging:(UIScrollView *)dviewer {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndDragging:(UIScrollView *)dviewer willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self showAnnotMenu];
    }
}

- (void)onScrollViewWillBeginDecelerating:(UIScrollView *)dviewer {
}

- (void)onScrollViewDidEndDecelerating:(UIScrollView *)dviewer {
    [self showAnnotMenu];
}

- (void)onScrollViewWillBeginZooming:(UIScrollView *)dviewer {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndZooming:(UIScrollView *)dviewer {
    double delayInSeconds = .2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [self showAnnotMenu];
    });
}

- (void)showAnnotMenu {
    if (self.shouldShowMenu) {
        if (_extensionsManager.currentAnnot == self.editAnnot && [_extensionsManager getAnnotHandlerByAnnot:_extensionsManager.currentAnnot] == self) {
            if (![_pdfViewCtrl isPageVisible:self.editAnnot.pageIndex]) return;
            CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.editAnnot.fsrect pageIndex:self.editAnnot.pageIndex];
            CGRect showRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:self.editAnnot.pageIndex];
            MenuControl *annotMenu = _extensionsManager.menuControl;
            [annotMenu setRect:showRect];
            [annotMenu showMenu];
        }
    }
}

- (void)dismissAnnotMenu {
    if (_extensionsManager.currentAnnot == self.editAnnot && [_extensionsManager getAnnotHandlerByAnnot:_extensionsManager.currentAnnot] == self) {
        MenuControl *annotMenu = _extensionsManager.menuControl;
        if (annotMenu.isMenuVisible) {
            [annotMenu setMenuVisible:NO animated:YES];
        }
    }
}

#pragma mark SignatureListDelegate

- (void)signatureListViewController:(SignatureListViewController *)signatureListViewController openSignature:(AnnotationSignature *)signature {
    self.isShowList = NO;
    void (^completion)() = ^{
        self->_signatureListCtr = nil;
        self.isShowList = NO;
        
        SignatureViewController *signatureCtr = [[SignatureViewController alloc] initWithUIExtensionsManager:self->_extensionsManager];
        self.signViewCtr = signatureCtr;
        [signatureCtr fs_setCustomTransitionAnimator:_extensionsManager.presentedVCAnimator];
        signatureCtr.currentSignature = signature;
        signatureCtr.isFieldSig = YES;
        DigitalSignatureAnnotHandler *__weak weakSelf = self;
        signatureCtr.saveHandler = ^{
            NSString *selectSig = [AnnotationSignature getSignatureSelected];
            AnnotationSignature *signnature = [AnnotationSignature getSignature:selectSig];
            DigitalSignatureAnnotHandler *strongSelf = weakSelf;
            assert(strongSelf);
            strongSelf->_isAdded = YES;
            strongSelf.signature = signnature;
            strongSelf.annotImage = [AnnotationSignature getSignatureImage:signnature.name];
            
            CGRect newRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:strongSelf.editAnnot.fsrect pageIndex:strongSelf.editAnnot.pageIndex];
            newRect = CGRectInset(newRect, -30, -30);
            [strongSelf->_pdfViewCtrl refresh:newRect pageIndex:self.editAnnot.pageIndex needRender:YES];
            strongSelf.shouldShowMenu = YES;
            [strongSelf showAnnotMenu];
        };
        signatureCtr.cancelHandler = ^{
            DigitalSignatureAnnotHandler *strongSelf = weakSelf;
            assert(strongSelf);
            if (strongSelf.signature) {
                strongSelf.shouldShowMenu = YES;
                [strongSelf showAnnotMenu];
            } else {
                [strongSelf->_extensionsManager setCurrentAnnot:nil];
            }
        };
        UIViewController *rootViewController = self->_pdfViewCtrl.fs_viewController;
        [rootViewController presentViewController:signatureCtr
                                         animated:NO
                                       completion:^{
                                           
                                       }];
        
    };
    if (signatureListViewController.presentingViewController) {
        [signatureListViewController dismissViewControllerAnimated:YES
                                                        completion:completion];
    }else{
        completion();
    }
}

- (void)signatureListViewController:(SignatureListViewController *)signatureListViewController deleteSignature:(AnnotationSignature *)signature {
    if ([self.signature.name isEqualToString:signature.name]) {
        [self deleteSign];
        if ([AnnotationSignature getCertSignatureList].count < 1) {
            [signatureListViewController dismissViewControllerAnimated:YES
                                                            completion:^{
                                                                self->_signatureListCtr = nil;
                                                                self.isShowList = NO;
                                                            }];
        }
    }
}

- (void)signatureListViewController:(SignatureListViewController *)signatureListViewController selectSignature:(AnnotationSignature *)signature {
    NSString *selectSig = [AnnotationSignature getSignatureSelected];
    AnnotationSignature *signnature = [AnnotationSignature getSignature:selectSig];
    if (signnature.certMD5 && signnature.certPasswd && signnature.certFileName) {
        _isAdded = YES;
        self.signature = signnature;
        self.annotImage = [AnnotationSignature getSignatureImage:signnature.name];

        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.editAnnot.fsrect pageIndex:self.editAnnot.pageIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [_pdfViewCtrl refresh:newRect pageIndex:self.editAnnot.pageIndex needRender:NO];
    }
    self.isShowList = NO;
    self.shouldShowMenu = YES;
    [self showAnnotMenu];
}

- (void)cancelSignature {
    _signatureListCtr = nil;
    if (self.signature) {
        self.shouldShowMenu = YES;
        [self showAnnotMenu];
    } else {
        [_extensionsManager setCurrentAnnot:nil];
    }
    self.isShowList = NO;
}

@end


