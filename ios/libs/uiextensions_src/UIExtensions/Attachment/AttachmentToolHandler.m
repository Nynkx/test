/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AttachmentToolHandler.h"
#import "AttachmentAnnotHandler.h"

@interface AttachmentToolHandler ()

@end

@implementation AttachmentToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotFileAttachment;
    }
    return self;
}

- (NSString *)getName {
    return Tool_Attachment;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

#define ATTACHMENT_WIDTH 20
#define ATTACHMENT_HEIGHT 24

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    float scale = [_pdfViewCtrl getPageViewWidth:pageIndex] / 1000.0;
    
    FSRectF *iconfsrect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:NOTE_ANNOTATION_WIDTH top1:NOTE_ANNOTATION_WIDTH];
    CGRect iconrect = [_pdfViewCtrl convertPdfRectToPageViewRect:iconfsrect pageIndex:pageIndex];
    float iconwidth = iconrect.size.width;
    CGRect rect = CGRectMake(point.x - ATTACHMENT_WIDTH * scale / 2, point.y - ATTACHMENT_HEIGHT * scale / 2,  iconwidth, iconwidth);

    FSRectF *dibRect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:pageIndex];
    
    FSPointF *tempPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSRectF *annotTempRect = [[FSRectF alloc] init];
    annotTempRect.left = tempPoint.x;
    annotTempRect.bottom = tempPoint.y - NOTE_ANNOTATION_WIDTH;
    annotTempRect.right = annotTempRect.left + NOTE_ANNOTATION_WIDTH;
    annotTempRect.top = annotTempRect.bottom + NOTE_ANNOTATION_WIDTH;
    dibRect = annotTempRect;

    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Import;
    selectDestination.expectFileType = [[NSArray alloc] initWithObjects:@"*", nil];
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:^{
            if (destinationFolder.count > 0) {
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSDictionary *fileAttribute = [fileManager attributesOfItemAtPath:destinationFolder[0] error:nil];
                long long fileSize = [fileAttribute fileSize];
                if (fileSize > 50 * 1024 * 1024) { // 50MB
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:[NSString stringWithFormat:FSLocalizedForKey(@"kAttachmentMaxSize"), 50] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:action];
                    [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    return;
                }
                
                FSPDFPage *page = [self->_pdfViewCtrl.currentDoc getPage:pageIndex];
                if (!page || [page isEmpty])
                    return;
                
                FSFileAttachment *annot = [[FSFileAttachment alloc] initWithAnnot:[page addAnnot:self.type rect:dibRect]];
                annot.NM = [Utility getUUID];
                annot.author = _extensionsManager.annotAuthor;
                annot.icon = self->_extensionsManager.attachmentIcon;
                annot.color = [self->_extensionsManager getPropertyBarSettingColor:self.type];
                annot.opacity = [self->_extensionsManager getAnnotOpacity:self.type] / 100.0f;
                annot.subject = @"File Attachment";
                
                annot.flags = FSAnnotFlagPrint | FSAnnotFlagNoZoom | FSAnnotFlagNoRotate;
                
                FSFileSpec *attachFile = [[FSFileSpec alloc] initWithDocument:self->_pdfViewCtrl.currentDoc];
                if (attachFile && [attachFile embed:destinationFolder[0]]) {
                    NSString *fileName = [destinationFolder[0] lastPathComponent];
                    [attachFile setFileName:fileName];
                    [attachFile setCreationDateTime:[Utility convert2FSDateTime:[fileAttribute fileCreationDate]]];
                    [attachFile setModifiedDateTime:[Utility convert2FSDateTime:[fileAttribute fileModificationDate]]];
                    [annot setFileSpec:attachFile];
                    
                    annot.content = fileName;
                }
                
                FSDateTime *now = [Utility convert2FSDateTime:[NSDate date]];
                [annot setCreationDateTime:now];
                [annot setModifiedDateTime:now];
                
                id<IAnnotHandler> annotHandler = [self->_extensionsManager getAnnotHandlerByAnnot:annot];
                [annotHandler addAnnot:annot];
            }
        }];
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    [_pdfViewCtrl.fs_viewController presentViewController:selectDestinationNavController animated:YES completion:nil];
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (self != _extensionsManager.currentToolHandler) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
}

@end
