/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AttachmentAnnotHandler.h"
#import "AttachmentController.h"

@interface AttachmentAnnotHandler () <UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) UIDocumentInteractionController *documentPopoverController;

@end

@implementation AttachmentAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
        self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-30, -30, -30, -30);
        self.annotBorderEdgeInsets = UIEdgeInsetsMake(-2, -2, -2, -2);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotFileAttachment;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    return options;
}

- (void)comment {
    [self openAttachment:[[FSFileAttachment alloc] initWithAnnot:self.extensionsManager.currentAnnot]];
    [self.extensionsManager setCurrentAnnot:nil];
}

- (void)showStyle {
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    [self.extensionsManager.propertyBar setColors:@[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff]];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_ATTACHMENT_ICONTYPE frame:CGRectZero];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_ATTACHMENT_ICONTYPE lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_ATTACHMENT_ICONTYPE intValue:annot.icon];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager]; //todel
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:[NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]]];
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    if (self.extensionsManager.currentAnnot == annot) {
        if (pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint]) {
            return YES;
        } else {
            [self.extensionsManager setCurrentAnnot:nil];
            return YES;
        }
    } else {
        [self openAttachment:[[FSFileAttachment alloc] initWithAnnot:annot]];
        return YES;
    }
    return NO;
}

- (BOOL)canResizeAnnot:(FSAnnot *)annot {
    return NO;
}

#pragma mark private

- (void)openAttachment:(FSFileAttachment *)annot {
    if (![annot getFileSpec]) {
        return;
    }
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSString *attachmentPath = [Utility getAttachmentTempFilePath:annot];
    if ([Utility isSupportFormat:attachmentPath]) {
        AttachmentController *attachmentCtr = [[AttachmentController alloc] init];
        [attachmentCtr fs_setCustomTransitionAnimator:self.extensionsManager.presentedVCAnimator];
        
        UIViewController *rootViewController = self.pdfViewCtrl.fs_viewController;
        [rootViewController presentViewController:attachmentCtr
                                         animated:YES
                                       completion:^{
                                           //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
                                           dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
                                           dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                                               
                                               if (![defaultManager fileExistsAtPath:attachmentPath]) {
                                                   [Utility loadAttachment:annot toPath:attachmentPath];
                                               }
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   AttachmentItem *attachmentItem = [AttachmentItem itemWithAttachmentAnnotation:annot];
                                                   attachmentItem.currentlevel = 1;
                                                   attachmentItem.isSecondLevel = YES;
                                                   
                                                   [attachmentCtr openDocument:attachmentItem];
                                               });
                                           });

                                       }];
    } else {
        if (![defaultManager fileExistsAtPath:attachmentPath]) {
            if (![Utility loadAttachment:annot toPath:attachmentPath]) {
                return;
            }
        }
        BOOL isPresent = NO;
        #if !_MAC_CATALYST_
        NSURL *urlFile = [NSURL fileURLWithPath:attachmentPath isDirectory:NO];
        self.documentPopoverController = [UIDocumentInteractionController interactionControllerWithURL:urlFile];
        self.documentPopoverController.delegate = self;
        if (DEVICE_iPHONE) {
            isPresent = [self.documentPopoverController presentOpenInMenuFromRect:self.pdfViewCtrl.frame inView:self.pdfViewCtrl animated:YES];
        } else {
            int pageIndex = annot.pageIndex;
            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
            CGRect dvRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
            isPresent = [self.documentPopoverController presentOpenInMenuFromRect:dvRect inView:self.pdfViewCtrl animated:YES];
        }
        #endif
        if (!isPresent) {
            NSString *fileName = [[annot getFileSpec] getFileName];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:FSLocalizedForKey(@"kFailedOpenAttachment"), fileName] message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancelAction];
            [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        }
    }
}


@end
