/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AttachmentModule.h"
#import "AttachmentAnnotHandler.h"
#import "AttachmentToolHandler.h"
#import "Utility.h"

@interface AttachmentModule ()

@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, weak) TbBaseItem *propertyItem;

@end

@implementation AttachmentModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Attachment";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        self.colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
        [self loadModule];
        AttachmentAnnotHandler* annotHandler = [[AttachmentAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];
        
        AttachmentToolHandler* toolHandler = [[AttachmentToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
    }
    return self;
}

- (void)loadModule {
    _extensionsManager.annotationToolsBar.attachmentClicked = ^() {
        [self annotItemClicked];
    };

    [_extensionsManager registerAnnotPropertyListener:self];
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    id<IToolHandler> toolHandler = [_extensionsManager getToolHandlerByName:Tool_Attachment];
    [_extensionsManager setCurrentToolHandler:toolHandler];

    [_extensionsManager.toolSetBar removeAllItems];

        TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    self.propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:FSAnnotFileAttachment]];
    [_extensionsManager.toolSetBar addItem:self.propertyItem displayPosition:Position_CENTER];

    self.propertyItem.onTapClick = ^(TbBaseItem *item) {
        CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_pdfViewCtrl];
        if (DEVICE_iPHONE) {
            [self->_extensionsManager showProperty:FSAnnotFileAttachment rect:rect inView:self->_pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:FSAnnotFileAttachment rect:item.contentView.bounds inView:item.contentView];
        }
    };

    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_continue")];
    } else {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_single")];
    }
    continueItem.tag = 3;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            item.imageNormal = ImageNamed(@"annot_continue");
            item.imageSelected = ImageNamed(@"annot_continue");
        } else {
            item.imageNormal = ImageNamed(@"annot_single");
            item.imageSelected = ImageNamed(@"annot_single");
        }

        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };

    [Utility showAnnotationType:FSLocalizedForKey(@"kAttachment") type:FSAnnotFileAttachment pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    //autolayout can avoid that the layout of button will be wrong after rotating over horizontal screen
    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.propertyItem.contentView.superview);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];

    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.left.equalTo(self.propertyItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];

    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.right.equalTo(self.propertyItem.contentView.mas_left).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_pdfViewCtrl];
}

#pragma mark - IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotFileAttachment) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

@end
