/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AttachmentController.h"
#import "TbBaseBar.h"
#import <WebKit/WebKit.h> //There are some bugs in ios13.2 https://bugs.webkit.org/show_bug.cgi?id=202173

#define UX_BG_COLOR_TOOLBAR_LIGHT 0xF5F5F5

@interface AttachmentController ()
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) FSToolbar *topToolbar;
@property (nonatomic, strong) TbBaseBar *topToolbarContent;
@property (nonatomic, strong) TbBaseItem *backItem;
@property (nonatomic, strong) TbBaseItem *titleItem;

@end

@implementation AttachmentController

- (instancetype)init {
    self = [super init];
    if (self) {
        
        self.topToolbar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];

        // init top toolbar
        self.topToolbarContent = [[TbBaseBar alloc] init];
        self.topToolbarContent.top = NO;
        self.topToolbarContent.hasDivide = NO;
        self.topToolbarContent.contentView.frame = CGRectMake(0, 0, VIEW_TEMPORARY_SIZE_VALUE, NAVIGATION_BAR_HEIGHT_NORMAL);

        self.backItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        [self.topToolbarContent addItem:self.backItem displayPosition:Position_LT];

        self.titleItem = [TbBaseItem createItemWithTitle:@"-"];
        self.titleItem.textColor = BarLikeBlackTextColor;
        self.titleItem.enable = NO;
        if (!DEVICE_iPHONE) {
            [self.topToolbarContent addItem:self.titleItem displayPosition:Position_CENTER];
        }

        [self.topToolbar addSubview:self.topToolbarContent.contentView];
        [self.topToolbarContent.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(self.topToolbarContent.contentView.superview);
        }];
        // add view to view controller
        [self.view addSubview:self.topToolbar];
        [self.topToolbar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.view);
        }];

        // set button callback
        __weak typeof(self) weakSelf = self;
        self.backItem.onTapClick = ^(TbBaseItem *item) {
            
            [weakSelf dismissViewControllerAnimated:YES
                                         completion:^{
                                             weakSelf.isShowing = NO;
                                             [weakSelf.webView removeFromSuperview];
                                             weakSelf.webView = nil;
                                         }];
        };

        self.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        self.automaticallyAdjustsScrollViewInsets = NO;

        if (self.webView == nil) {
            self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
//            self.webView.scalesPageToFit = YES;
            //            self.webView.delegate = self;
            self.webView.allowsLinkPreview = YES;
            [self.view insertSubview:self.webView atIndex:0];
            [self.webView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view.mas_left).offset(0);
                make.top.equalTo(self.topToolbar.mas_bottom);
                make.right.equalTo(self.view.mas_right).offset(0);
                make.bottom.equalTo(self.view.mas_bottom).offset(0);
            }];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.webView.backgroundColor = ThemeViewBackgroundColor;
    self.isShowing = YES;
}

- (BOOL)openDocument:(AttachmentItem *)attachmentItem {
    NSString *filePath = attachmentItem.filePath;
    
    [_topToolbarContent removeItem:_titleItem];
    
    NSString *title = attachmentItem.fileName;
    NSUInteger prefixLocation = [title rangeOfString:@"."].location;
    title = [title substringToIndex:prefixLocation];
    
    _titleItem.text = title;

    if (_titleItem.text.length > 20) {
        _titleItem.text = [NSString stringWithFormat:@"%@..%@", [_titleItem.text substringToIndex:10], [_titleItem.text substringFromIndex:_titleItem.text.length - 10]];
    }
    [_topToolbarContent addItem:_titleItem displayPosition:Position_CENTER];

    BOOL willLoadRequest = YES;
    NSURL *url = [NSURL fileURLWithPath:filePath isDirectory:NO];
    if ([Utility isGivenPath:filePath type:@"pdf"]) {
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:request];
        willLoadRequest = NO;
    } else if ([Utility isGivenPath:filePath type:@"txt"] || [Utility isGivenPath:filePath type:@"htm"] || [Utility isGivenPath:filePath type:@"html"]) {
        NSStringEncoding encoding;
        NSError *error = nil;
        NSString *temp = [NSString stringWithContentsOfURL:url usedEncoding:&encoding error:&error];
        if (!error) {
            if (encoding != NSUTF8StringEncoding) {
                willLoadRequest = NO;
                [self.webView loadHTMLString:temp baseURL:[url URLByDeletingLastPathComponent]];
            }
        } else {
            willLoadRequest = NO;
            NSData *data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
            [self.webView loadData:data MIMEType:@"text/html" characterEncodingName:@"GBK" baseURL:[url URLByDeletingLastPathComponent]];
        }
    } else if ([Utility isGivenPath:filePath type:@"jpg"] || [Utility isGivenPath:filePath type:@"jpeg"] || [Utility isGivenPath:filePath type:@"png"] || [Utility isGivenPath:filePath type:@"bmp"] || [Utility isGivenPath:filePath type:@"gif"] || [Utility isGivenPath:filePath type:@"tiff"] || [Utility isGivenPath:filePath type:@"tif"]) {
        willLoadRequest = NO;
        
        NSURL *baseURL = [[NSBundle bundleForClass:[self class]] bundleURL];//adapter WKWebView
        if ([filePath rangeOfString:@"file://"].location != NSNotFound) {
            //iCloud
            NSString *htmString = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><img src=\"%@\" width=\"100%%\" height=\"100%%\"/></body></html>", filePath];
            [self.webView loadHTMLString:htmString baseURL:baseURL];

        } else {
            //Document
            NSString *htmString = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><img src=\"%@\" width=\"100%%\" height=\"100%%\"/></body></html>", [url absoluteString]];
            [self.webView loadHTMLString:htmString baseURL:baseURL];
        }
    }

    if (willLoadRequest) {
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
    }

    return 0;
}
@end
