/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <MobileCoreServices/MobileCoreServices.h>
#import "AudioVideoToolHandler.h"
#import "AudioVideoAnnotHandler.h"
#import "FSFileAndImagePicker.h"

@interface AudioVideoToolHandler ()<FSFileAndImagePickerDelegate>

@property (nonatomic) CGPoint point;
@property (nonatomic) int pageIndex;
// 0.0~1.0
@property (nonatomic) CGFloat minPlayerLayerWidthInPage;
@property (nonatomic) CGFloat maxPlayerLayerWidthInPage;
@property (nonatomic) CGFloat minPlayerLayerHeightInPage;
@property (nonatomic) CGFloat maxPlayerLayerHeightInPage;

@end

@implementation AudioVideoToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}
- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager{
    return [[AudioVideoToolHandler alloc] initWithUIExtensionsManager:extensionsManager annotType:FSAnnotScreen];
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager annotType:(FSAnnotType)annotType {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _annotType = annotType;
        _minPlayerLayerWidthInPage = 0.3;
        _maxPlayerLayerWidthInPage = 0.5;
        _minPlayerLayerHeightInPage = 0.3;
        _maxPlayerLayerHeightInPage = 0.5;
        _pageIndex = -1;
        _point = CGPointZero;
    }
    return self;
}

- (NSString *)getName {
    if (self.annotType == FSAnnotScreen) {
        return Tool_Video;
    }
    return Tool_Audio;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
}


// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return [self handleLongPressAndPan:pageIndex gestureRecognizer:recognizer];
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return [self handleLongPressAndPan:pageIndex gestureRecognizer:recognizer];
}

- (BOOL)handleLongPressAndPan:(int)pageIndex gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    _point = point;
    _pageIndex = pageIndex;
    
    FSFileAndImagePicker *picker = [[FSFileAndImagePicker alloc] init];
    picker.extensionsManager = _extensionsManager;
    picker.fileType = self.fileType;
    picker.delegate = self;
    UIViewController *rootViewController = _pdfViewCtrl.fs_viewController;
    [picker presentInRootViewController:rootViewController];
    
    return YES;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (CGRect)annotRectWithCenter:(CGPoint)center pageView:(UIView *)pageView {
    float pageWidth = CGRectGetWidth(pageView.frame);
    float pageHeight = CGRectGetHeight(pageView.frame);
    if (!CGRectContainsPoint(pageView.bounds, center)) {
        return CGRectZero;
    }

    if (self.fileType  == FSMediaFileTypeAudio) {
        UIImage *image = [UIImage imageNamed:@"add_audio_h" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        CGFloat audio_icon_width = image.size.width;
        CGFloat audio_icon_height = image.size.height;
        CGRect frame = CGRectMake(center.x - audio_icon_width / 2, center.y - audio_icon_height / 2, audio_icon_width, audio_icon_height);
        return [Utility boundedRectForRect:frame containerRect:pageView.bounds];
    }

    CGSize videoSize = [self getThumbailImageRequestAtTimeSecond:0]?[self getThumbailImageRequestAtTimeSecond:0].size:CGSizeMake(240.f, 240.f);
    CGFloat originWidth = videoSize.width;
    CGFloat originHeight = videoSize.height;
    
    CGFloat width = MIN(MAX(originWidth, self.minPlayerLayerWidthInPage * pageWidth), self.maxPlayerLayerWidthInPage * pageWidth);
    CGFloat height = originHeight / originWidth * width;
    height = MIN(MAX(height, self.minPlayerLayerHeightInPage * pageHeight), self.maxPlayerLayerHeightInPage * pageHeight);
    width = originWidth / originHeight * height;
    
    CGRect frame = CGRectMake(center.x - width / 2, center.y - height / 2, width, height);
    return [Utility boundedRectForRect:frame containerRect:pageView.bounds];
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
}

+ (CGSize)sizeAspectRatioFitSize:(CGSize)originSize maxWidth:(CGFloat)maxWidth maxHeight:(CGFloat)maxHeight {
    CGSize size = originSize;
    CGFloat aspectRatio = originSize.height / originSize.width;
    if (size.width > maxWidth) {
        size.width = maxWidth;
        size.height = size.width * aspectRatio;
    }
    if (size.height > maxHeight) {
        size.height = maxHeight;
        size.width = size.height / aspectRatio;
    }
    return size;
}

+ (CGPoint)centerOfRect:(CGRect)rect {
    return CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height / 2);
}

- (void)addAnnotWithRect:(CGRect)rect pageIndex:(int)pageIndex {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    FSRectF *fsrect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:pageIndex];
    FSScreen *annot = [[FSScreen alloc] initWithAnnot:[page addAnnot:FSAnnotScreen rect:fsrect]];
    if (![annot getCptr]) {
        return;
    }
    
    int pvcRotation = [_pdfViewCtrl getViewRotation];
    FSRotation rotation = (FSRotation)(pvcRotation) % 4;
    rotation = ([page getRotation] + rotation) % 4;
    if (rotation != 0) {
        rotation = 4 - rotation;
    }
    [annot setRotation:rotation];
    
    FSFileSpec *fileSpec = [[FSFileSpec alloc] initWithDocument:self->_pdfViewCtrl.currentDoc];
    if (fileSpec && [fileSpec embed:self.filePath]) {
        annot.NM = [Utility getUUID];
        annot.author = _extensionsManager.annotAuthor;
        annot.createDate = [NSDate date];
        annot.modifiedDate = [NSDate date];
        annot.flags = FSAnnotFlagPrint;
        NSString *mineType = @"";
        annot.intent = mineType.copy;
        annot.opacity = [_extensionsManager getAnnotOpacity:self.annotType] / 100.0f;
        annot.borderInfo = annot.borderInfo;
        FSImage *fsimage = nil;
        if (self.fileType == FSMediaFileTypeAudio) {
            fsimage = [Utility uiimageToFSImage:[UIImage imageNamed:@"add_audio_h" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        }
        if (self.fileType == FSMediaFileTypeVideo) {
            fsimage = [Utility uiimageToFSImage:[self getThumbailImageRequestAtTimeSecond:0]];
        }
        if (fsimage) {
            [annot setImage:fsimage frame_index:0 compress:0];
        }
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSDictionary *fileAttribute = [fileManager attributesOfItemAtPath:self.filePath error:nil];
        
        FSAction *action = [FSAction create:_extensionsManager.pdfViewCtrl.currentDoc action_type:FSActionTypeRendition];
        FSRenditionAction *renditionAction = [[FSRenditionAction alloc] initWithAction:action];
        renditionAction.operationType = FSRenditionActionOpTypeAssociate;
        renditionAction.screenAnnot = annot;
        
        FSRendition *rendition = [[FSRendition alloc] initWithPdf_doc:_extensionsManager.pdfViewCtrl.currentDoc rendition_dict:[FSPDFDictionary create]];
        
        NSString *fileName = [self.filePath lastPathComponent];
        [fileSpec setFileName:fileName];
        [fileSpec setCreationDateTime:[Utility convert2FSDateTime:[fileAttribute fileCreationDate]]];
        [fileSpec setModifiedDateTime:[Utility convert2FSDateTime:[fileAttribute fileModificationDate]]];
        [rendition setMediaClipFile:fileSpec];
        rendition.renditionName = fileName;
        rendition.mediaClipName = fileName;
        rendition.permission = FSRenditionMediaPermTempAccess;
        rendition.mediaClipContentType = [self mimeTypeForFileAtPath:self.filePath];
        [renditionAction insertRendition:rendition index:-1];
        annot.action = renditionAction;
        annot.contents = fileSpec.fileName;
        
        [_pdfViewCtrl lockRefresh];
        [annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        [_pdfViewCtrl refresh:CGRectInset(rect, -5, -5) pageIndex:pageIndex needRender:YES];
        
        for (id<IAnnotHandler> annotHandler in _extensionsManager.annotHandlers) {
            if ([annotHandler isKindOfClass:[AudioVideoAnnotHandler class]]) {
                [annotHandler addAnnot:annot addUndo:YES];
                break;
            }
        }
    }

    self.filePath = nil;
}

- (NSString *)mimeTypeForFileAtPath:(NSString *)path
{
    if (![[[NSFileManager alloc] init] fileExistsAtPath:path]) {
        return nil;
    }
    
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!MIMEType) {
        NSString *mineType = @"";
        if (self.fileType == FSMediaFileTypeAudio) {
            mineType = @"audio";
        }
        else if (self.fileType == FSMediaFileTypeVideo) {
            mineType = @"video";
        }
        NSURL *fileURL = [NSURL fileURLWithPath:path];
        return  [NSString stringWithFormat:@"%@/%@",mineType,fileURL.pathExtension];
    }
    return (__bridge NSString *)(MIMEType);
}

- (UIImage *)getThumbailImageRequestAtTimeSecond:(CGFloat)timeBySecond {
    
    NSURL *url = [NSURL fileURLWithPath:self.filePath];
    AVURLAsset *urlAsset = [AVURLAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:urlAsset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMake(timeBySecond, 10);
    CMTime actualTime;
    NSError *error = nil;
    CGImageRef cgImage = [imageGenerator copyCGImageAtTime:time
                                                actualTime:&actualTime
                                                     error:&error];
    UIImage *icon_img = [UIImage imageNamed:@"add_veido_kaishi" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    if (error) {
        NSLog(@"getThumbailImageRequestAtTimeSecond：%@",error.localizedDescription);
        return nil;
    }
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    
    CGFloat tW = CGImageGetWidth(image.CGImage);
    CGFloat tH = CGImageGetHeight(image.CGImage);
    
    CGFloat w = tW/3;
    CGFloat h = tH/3;
    if(tW < tH){
        h = w;
    }else{
        w = h;
    }
    UIGraphicsBeginImageContext(CGSizeMake(tW, tH));
    [image drawInRect:CGRectMake(0, 0, tW, tH)];
    [icon_img drawInRect:CGRectMake((tW - w) /2, (tH - h)/2, w, h)];
    UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return resultImg;
}

#pragma mark <FSFileAndImagePickerDelegate>

- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didPickFileAtPath:(NSString *)filePath{
    
    if (!filePath) {
        return;
    }
    
    self.filePath = filePath;
    UIView *pageView = [_pdfViewCtrl getPageView:_pageIndex];
    CGRect frame = [self annotRectWithCenter:_point pageView:pageView];
    if (CGRectIsEmpty(frame)) {
        return;
    }
    [self addAnnotWithRect:frame pageIndex:_pageIndex];
    
    [self completed];
    if (_extensionsManager.continueAddAnnot) {
        return;
    }
    [_extensionsManager changeState:STATE_EDIT];
}

- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didPickImage:(UIImage *)image imagePath:(NSString *)imagePath {
    
}
- (void)fileAndImagePickerDidCancel:(FSFileAndImagePicker *)fileAndImagePicker {
    [self completed];
}

- (void)completed{
    self.filePath = nil;
    _point = CGPointZero;
    _pageIndex = -1;
}


@synthesize type;

@end
