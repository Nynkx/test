/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AudioVideoModule.h"
#import "FSFileAndImagePicker.h"
#import "AudioVideoAnnotHandler.h"
#import "AudioVideoToolHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>
#import "FSMediaFileUtil.h"

@interface AudioVideoModule ()

@property (nonatomic, weak) TbBaseItem *doneItem;

@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, assign) FSMediaFileType mediaFileType;
@end

@implementation AudioVideoModule{
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"AudioVideo";
}

- (TbBaseItem *)doneItem{
    if (!_doneItem) {
        TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
        doneItem.tag = 0;
        [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
        doneItem.onTapClick = ^(TbBaseItem *item) {
            if (self->_extensionsManager.currentToolHandler) {
                [self->_extensionsManager setCurrentToolHandler:nil];
            }
            [self->_extensionsManager changeState:STATE_EDIT];
        };
        _doneItem = doneItem;
    }
    return _doneItem;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        AudioVideoToolHandler *toolHandler = [[AudioVideoToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        AudioVideoAnnotHandler *annotHandler = [[AudioVideoAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];
        
        annotHandler.minPlayerLayerWidthInPage = annotHandler.minPlayerLayerHeightInPage = 0.1;
        annotHandler.maxPlayerLayerWidthInPage = annotHandler.maxPlayerLayerHeightInPage = 0.7;
        
        [self loadModule];
    }
    return self;
}

- (void)loadModule {
    _extensionsManager.annotationToolsBar.audioClicked = ^() {
        self.mediaFileType = FSMediaFileTypeAudio;
        [self annotItemClicked];
    };
    _extensionsManager.annotationToolsBar.videoClicked = ^() {
        self.mediaFileType = FSMediaFileTypeVideo;
        [self annotItemClicked];
    };
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];

    AudioVideoToolHandler *toolHanlder = (AudioVideoToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Video];
    toolHanlder.fileType = self.mediaFileType;
    [_extensionsManager setCurrentToolHandler:toolHanlder];
    
    
    switch (self.mediaFileType) {
        case FSMediaFileTypeAudio:
            [Utility showAnnotationType:FSLocalizedForKey(@"kAudio") type:FSAnnotScreen pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
            break;
        case FSMediaFileTypeVideo:
            [Utility showAnnotationType:FSLocalizedForKey(@"kVideo") type:FSAnnotScreen pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
            break;
        default:
            break;
    }
    
    [_extensionsManager.toolSetBar removeAllItems];
    
    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_continue")];
    } else {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_single")];
    }
    continueItem.tag = 1;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_extensionsManager.pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            item.imageNormal = ImageNamed(@"annot_continue");
            item.imageSelected = ImageNamed(@"annot_continue");
        } else {
            item.imageNormal = ImageNamed(@"annot_single");
            item.imageSelected = ImageNamed(@"annot_single");
        }
        
        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_extensionsManager.pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };
    
    [self.doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.doneItem.contentView.superview);
        make.centerX.mas_equalTo(self.doneItem.contentView.superview.mas_centerX).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(self.doneItem.contentView.fs_size);
    }];
    
    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(continueItem.contentView.superview);
        make.centerX.equalTo(continueItem.contentView.superview.mas_centerX).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];
    
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_pdfViewCtrl];
}
@end
