/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AudioPlayerControl.h"
#import <AVKit/AVKit.h>
#if !_MAC_CATALYST_
#import <CallKit/CallKit.h>
@interface AudioPlayerControl () <AVAudioPlayerDelegate, CXCallObserverDelegate>
#else
@interface AudioPlayerControl () <AVAudioPlayerDelegate>
#endif

{
    CADisplayLink *_link;
}
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, strong) UILabel *currentTime;
@property (nonatomic, strong) UILabel *totalTime;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIButton *pauseOrPlay;
#if !_MAC_CATALYST_
@property (nonatomic, strong) CXCallObserver *callObserver;
#endif
@property (nonatomic, assign) BOOL isPlay;
@end

@implementation AudioPlayerControl

#pragma mark - lifecycle

static AudioPlayerControl *playerControl = nil;
+ (AudioPlayerControl *)sharedControl{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        playerControl = [[self alloc] init];
    });
    return playerControl;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    
    _isPlay = NO;
    #if !_MAC_CATALYST_
    _callObserver = [[CXCallObserver alloc] init];
    [_callObserver setDelegate:self queue:nil];
    #endif
    
    UIView *containerView = [[UIView alloc] init];
    containerView.layer.masksToBounds = YES;
    containerView.layer.cornerRadius = 6.f;
    containerView.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.68];
    self.containerView = containerView;
    
    UILabel *currentTime = [[UILabel alloc] init];
    currentTime.text = @"00:00";
    currentTime.textAlignment = NSTextAlignmentLeft;
    currentTime.textColor = WhiteThemeTextColor;
    currentTime.font = [UIFont systemFontOfSize:10.f];
    self.currentTime = currentTime;
    
    UILabel *totalTime = [[UILabel alloc] init];
    totalTime.text = @"00:00";
    currentTime.textAlignment = NSTextAlignmentRight;
    totalTime.textColor = currentTime.textColor;
    totalTime.font = currentTime.font;
    self.totalTime = totalTime;
    
    UISlider *silder = [[UISlider alloc] init];
    silder.continuous = YES;
    silder.minimumTrackTintColor = UIColorHex(0x48baee);
    silder.maximumTrackTintColor = UIColorHex(0xa7a7a7);
    [silder setThumbImage:ImageNamed(@"audio_slider_thumb") forState:UIControlStateNormal];
    [silder addTarget:self action:@selector(changeCurrentProgress:)
     forControlEvents:UIControlEventTouchUpInside];
    self.slider = silder;
    
    UIButton *backward = [[UIButton alloc] init];
    [backward setImage:ImageNamed(@"audio_backward") forState:UIControlStateNormal];
    [backward addTarget:self action:@selector(backward:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *speed = [[UIButton alloc] init];
    [speed setImage:ImageNamed(@"audio_speed") forState:UIControlStateNormal];
    [speed addTarget:self action:@selector(speed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *pauseOrPlay = [[UIButton alloc] init];
    [pauseOrPlay setImage:ImageNamed(@"audio_pause") forState:UIControlStateSelected];
    [pauseOrPlay setImage:ImageNamed(@"audio_continue") forState:UIControlStateNormal];
    [pauseOrPlay addTarget:self action:@selector(pauseOrPlay:) forControlEvents:UIControlEventTouchUpInside];
    self.pauseOrPlay = pauseOrPlay;
    
    UIButton *stop = [[UIButton alloc] init];
    [stop setImage:ImageNamed(@"audio_stop") forState:UIControlStateNormal];
    [stop addTarget:self action:@selector(stop:) forControlEvents:UIControlEventTouchUpInside];
    
    [containerView addSubview:currentTime];
    [containerView addSubview:totalTime];
    [containerView addSubview:silder];
    [containerView addSubview:backward];
    [containerView addSubview:speed];
    [containerView addSubview:pauseOrPlay];
    [containerView addSubview:stop];
    
    [currentTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView).offset(15);
        make.top.equalTo(containerView).offset(18);
    }];
    
    [totalTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(containerView).offset(-15);
        make.centerY.equalTo(currentTime);
    }];
    
    [silder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(currentTime.mas_right).offset(10);
        make.right.equalTo(totalTime.mas_left).offset(-10);
        make.centerY.equalTo(currentTime);
        make.height.offset(30);
    }];
    
    [pauseOrPlay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(containerView.mas_centerX).offset(-5);
        make.bottom.offset(-10);
    }];
    
    [stop mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView.mas_centerX).offset(5);
        make.centerY.equalTo(pauseOrPlay);
    }];
    
    [backward mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(pauseOrPlay.mas_left).offset(-5);
        make.centerY.equalTo(pauseOrPlay);
    }];
    
    [speed mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(stop.mas_right).offset(5);
        make.centerY.equalTo(pauseOrPlay);
    }];
}

#pragma mark - custom accessors

- (AVAudioPlayer *)audioPlayer{
    if (!_audioPlayer) {
        NSError *error = nil;
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_filePath error:&error];
        _audioPlayer.numberOfLoops = 0;
        _audioPlayer.delegate = self;
        [_audioPlayer prepareToPlay];
        if (error) {
            return nil;
        }
        _slider.minimumValue = 0;
        _slider.maximumValue = _audioPlayer.duration;
        _totalTime.text = [NSString stringWithFormat:@"%02d:%02d",(int)_audioPlayer.duration / 60, (int)_audioPlayer.duration % 60];
        _link = [CADisplayLink displayLinkWithTarget:self selector:@selector(tick)];
        [_link addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    }
    return _audioPlayer;
}

- (void)setFilePath:(NSURL *)filePath{
    self.audioPlayer = nil;
    _filePath = filePath;
}

#pragma mark - events
static NSTimeInterval skipInterval = 10;

- (void)backward:(UIButton *)btn{
    [self setAudioPlayTime:(self.audioPlayer.currentTime - skipInterval)?:0];
}
- (void)speed:(UIButton *)btn{
    
    [self setAudioPlayTime:self.audioPlayer.currentTime + skipInterval];
}

- (void)pauseOrPlay:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        [self play];
    }else{
        [self pause];
    }
}

- (void)stop:(UIButton *)btn{
    [self.audioPlayer stop];
    [self dismiss];
}

-(void)changeCurrentProgress:(UISlider *)slider{
    NSTimeInterval difference = self.audioPlayer.currentTime - self.slider.value;
    if (difference > -1 && difference < 1) {
        return;
    }
    [self setAudioPlayTime:slider.value];
}

#pragma mark- public
- (void)addSuperview:(UIView *)view{
    [self dismiss];
    [view insertSubview:self.containerView atIndex:1];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view);
        make.centerY.equalTo(view).multipliedBy(1.5);
        make.width.offset(240);
        make.height.offset(90);
    }];
}
- (void)play{
    _isPlay = YES;
    if (![self.audioPlayer isPlaying]) {
        [self.audioPlayer play];
    }
    self.pauseOrPlay.selected = YES;
}

- (void)pause{
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer pause];
    }
    self.pauseOrPlay.selected = NO;
}

#pragma mark- private
- (void)dismiss{
    if (!_isPlay) return;
    _isPlay = NO;
    [_link invalidate];
    [self.audioPlayer stop];
    [self.containerView removeFromSuperview];
}

- (void)setAudioPlayTime:(NSTimeInterval)time{
    BOOL isPlaying = [self.audioPlayer isPlaying];
    [self.audioPlayer pause];
    self.audioPlayer.currentTime = time;
    if (isPlaying) {
        [self.audioPlayer play];
    }else{
        self.currentTime.text = [NSString stringWithFormat:@"%02d:%02d",(int)self.audioPlayer.currentTime / 60, (int)self.audioPlayer.currentTime % 60];
    }
}

- (void)tick{
    if ([self.audioPlayer isPlaying]) {
        self.currentTime.text = [NSString stringWithFormat:@"%02d:%02d",(int)self.audioPlayer.currentTime / 60, (int)self.audioPlayer.currentTime % 60];
        if (self.slider.state == UIControlStateHighlighted) {
            return;
        }
        self.slider.value = self.audioPlayer.currentTime;
    }
}
#pragma mark- <CXCallObserverDelegate>
#if !_MAC_CATALYST_
- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
    if (!playerControl.audioPlayer.isPlaying) {
        return;
    }
    [playerControl pause];
    if(call.hasEnded){
        [playerControl play];
    }
}
#endif

#pragma mark- <AVAudioPlayerDelegate>
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (flag) {
//        [self dismiss];
        if (![self.audioPlayer isPlaying]) {
             self.pauseOrPlay.selected = NO;
        }
    }
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError * __nullable)error{
    if (error) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kMediaFileValid") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismiss];
        }];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];

    }
}

@end
