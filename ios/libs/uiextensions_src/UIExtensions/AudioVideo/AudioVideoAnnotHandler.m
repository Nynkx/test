/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AudioVideoAnnotHandler.h"

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "FSAnnot+Extensions.h"
#import "AudioPlayerControl.h"
#import "FSMediaFileUtil.h"

@interface  VideoPlayerControler : AVPlayerViewController;
@end

@interface AudioVideoAnnotHandler ()<UIDocumentInteractionControllerDelegate>
@property (nonatomic, strong) AudioPlayerControl *audioPlay;
@property (nonatomic, assign) FSMediaFileType mediaFileType;
@property (nonatomic, strong) UIDocumentInteractionController *documentPopoverController;
@end

@implementation AudioVideoAnnotHandler{
    __weak UIExtensionsManager *_extensionsManager;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager annotType:(FSAnnotType)annotType{
    self = [super initWithUIExtensionsManager:extensionsManager];
    if (self) {
        _extensionsManager = extensionsManager;
        _annotType = annotType;
        _minPlayerLayerWidthInPage = 0.1;
        _maxPlayerLayerWidthInPage = 0.5;
        _minPlayerLayerHeightInPage = 0.1;
        _maxPlayerLayerHeightInPage = 0.5;
        if (annotType == FSAnnotSound) {
            self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-25, -25, -25, -25);
        }
    }
    return self;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    return [[AudioVideoAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager annotType:FSAnnotScreen];
}

- (FSAnnotType)getType {
    return self.annotType;
}

- (FSMediaFileType)mediaFileType{
    FSScreen *screen = [[FSScreen alloc] initWithAnnot:self.annot];
    FSAction *action = screen.action;
    if ([action isEmpty]) {
        return FSMediaFileTypeUndefine;
    }
    FSRenditionAction *renditionAction = [[FSRenditionAction alloc] initWithAction:action];
    if (([renditionAction isEmpty] || renditionAction.getRenditionCount == 0)) {
        return FSMediaFileTypeUndefine;
    }
    FSRendition *rendition = [renditionAction getRendition:0];
    NSString *mineType = [rendition getMediaClipContentType];
    if ([mineType containsString:@"audio"]) {
        return FSMediaFileTypeAudio;
    }
    return FSMediaFileTypeVideo;
}

- (BOOL)keepAspectRatioForResizingAnnot:(FSAnnot *)annot {
    return YES;
}

- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot {
    UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
    CGFloat pageWidth = CGRectGetWidth(pageView.bounds);
    CGFloat pageHeight = CGRectGetHeight(pageView.bounds);
    CGFloat minW = self.minPlayerLayerWidthInPage * pageWidth;
    CGFloat minH = self.minPlayerLayerHeightInPage * pageHeight;
    return CGSizeMake(minW, minH);
}

- (CGSize)maxSizeForResizingAnnot:(FSAnnot *)annot {
    UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
    CGFloat pageWidth = CGRectGetWidth(pageView.bounds);
    CGFloat pageHeight = CGRectGetHeight(pageView.bounds);
    CGFloat maxW = self.maxPlayerLayerWidthInPage * pageWidth;
    CGFloat maxH = self.maxPlayerLayerHeightInPage * pageHeight;
    return CGSizeMake(maxW, maxH);
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:annot.pageIndex]];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:annot.pageIndex];
    if ([Utility isAnnot:self.extensionsManager.currentAnnot uniqueToAnnot:annot]) {
        if (!(pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint])) {
            [self.extensionsManager setCurrentAnnot:nil];
        } else if (self.extensionsManager.shouldShowMenu && !self.extensionsManager.shouldShowPropertyBar) {
            [self showMenu];
        }
    } else {
        [self play:annot];
    }
    return YES;
}

- (void) delete {
    if (self.audioPlay) {
        NSString *medioFilePath = [Utility getDocumentRenditionTempFilePath:self.annot.ha_fileSpec PDFPath:self.extensionsManager.pdfViewCtrl.filePath];
        NSString *playFilePath = self.audioPlay.filePath.path;
        if ([medioFilePath isEqualToString:playFilePath]) {
            [self.audioPlay dismiss];
        }
    }
    [super delete];
}

- (void)play{
    [self play:self.annot];
    [_extensionsManager setCurrentAnnot:nil];
}

- (void)play:(FSAnnot *)annot{
    if (self.audioPlay) [self.audioPlay dismiss];
    NSURL * (^renditionResult)(FSFileSpec *) = ^(FSFileSpec *fileSpec){
        NSURL *fileURL = nil;
        NSString *medioFilePath = [Utility getDocumentRenditionTempFilePath:fileSpec PDFPath:self.extensionsManager.pdfViewCtrl.filePath];
        if (!medioFilePath) return fileURL;
        BOOL result = [Utility loadFileSpec:fileSpec toPath:medioFilePath];
        if (!result) fileURL = nil;
        fileURL = [NSURL fileURLWithPath:medioFilePath];
        return fileURL;
    };
    
    if (self.annotType == FSAnnotScreen && annot.getType == FSAnnotScreen) {
        FSScreen *screen = [[FSScreen alloc] initWithAnnot:annot];
        FSAction *action = screen.action;
        FSRenditionAction *renditionAction = [[FSRenditionAction alloc] initWithAction:action];
        FSRendition *rendition = [renditionAction getRendition:0];
        FSFileSpec *fileSpec = rendition.mediaClipFile;
        NSLog(@"%@",fileSpec);
        NSURL *fileURL = renditionResult(fileSpec);
        if (!fileURL) return;
        if (![self isPlayableFileURLForAlertView:fileURL]) return;
        if (screen.screenActiontype == FSScreenActionTypeAudio) {
            [self audioPlayWithFile:fileURL];
        }
        else if (screen.screenActiontype == FSScreenActionTypeVideo){
            AVURLAsset *asset = [AVURLAsset assetWithURL:fileURL];
            AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:asset];
            AVPlayer *player = [AVPlayer playerWithPlayerItem:playerItem];
            VideoPlayerControler *VC = [[VideoPlayerControler alloc] init];
            VC.player = player;
            [self.pdfViewCtrl.fs_viewController presentViewController:VC animated:YES completion:nil];
        }
    }else if (self.annotType == FSAnnotSound && annot.getType == FSAnnotSound){
        [self.pdfViewCtrl fs_showHUDLoading:FSLocalizedForKey(@"kLoading") process:^{
            FSSound *sound = [[FSSound alloc] initWithAnnot:annot];
            FSFileSpec *fileSpec = sound.getFileSpec;
            NSURL *fileURL = renditionResult(fileSpec);
            if (!fileURL){
                NSString *medioFilePath = [Utility getDocumentSoundTempFilePath:sound PDFPath:self.extensionsManager.pdfViewCtrl.filePath];
                if (!medioFilePath) return;
                fileURL = [NSURL fileURLWithPath:medioFilePath];
            }
            dispatch_main_async_safe(^{
                [self audioPlayWithFile:fileURL];
            });
        }];
    }
}

- (BOOL)isPlayableFileURLForAlertView:(NSURL *)fileURL{
    AVURLAsset *asset = [AVURLAsset assetWithURL:fileURL];
    NSArray *tracks = asset.tracks;
    
    __block BOOL isPlayable = NO;
    [tracks enumerateObjectsUsingBlock:^(AVAssetTrack *track, NSUInteger idx, BOOL * _Nonnull stop) {
        if (track.isPlayable) {
            isPlayable = YES;
            *stop = YES;
        }
    }];
    if (!isPlayable) {
        BOOL canOpen = NO;
        #if !_MAC_CATALYST_
        CGRect dvRect = CGRectMake(SCREENWIDTH/2, SCREENHEIGHT/2, 20, 20);
        self.documentPopoverController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        self.documentPopoverController.delegate = self;
        UIView *inView = self.extensionsManager.pdfViewCtrl;
        if (DEVICE_iPHONE){
            canOpen = [self.documentPopoverController presentOpenInMenuFromRect:inView.frame inView:inView animated:YES];
        }
        else{
            canOpen = [self.documentPopoverController presentOpenInMenuFromRect:dvRect inView:inView animated:YES];
        }
        #endif
        if (!canOpen) {
            NSLog(@"can't open");
        }
    }
    return isPlayable;
}

- (void)audioPlayWithFile:(NSURL *)fileURL{
    self.audioPlay = [AudioPlayerControl sharedControl];
    self.audioPlay.filePath = fileURL;
    [self.audioPlay addSuperview:self.extensionsManager.pdfViewCtrl];
    [self.audioPlay play];
}

- (UIColor *)borderColorForSelectedAnnot:(FSAnnot *)annot {
    return [UIColor colorWithRGB:0x179cd8];
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionNone;
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionPlay;
        if (annot.canDelete && self.annotType == FSAnnotScreen) {
            options |= FSMenuOptionDelete;
        }
    }else{
        options = FSMenuOptionPlay;
    }
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    return options;
}

#pragma mark <UIDocumentInteractionControllerDelegate>
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller{
    
}

@end

@interface VideoPlayerControler ()<AVPlayerViewControllerDelegate>
@end
@implementation VideoPlayerControler

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.delegate = self;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(playerItemDidReachEnd:)
     name:AVPlayerItemDidPlayToEndTimeNotification
     object:nil];
    [self.player play];
}

- (void)playerItemDidReachEnd:(NSNotification *)not{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
