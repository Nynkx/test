/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PrintRenderer.h"
#import "Utility.h"

@implementation PrintRenderer

- (id)initWithDocument:(FSPDFDoc *)document {
    if (self = [super init]) {
        _document = document;
    }
    return self;
}

- (NSInteger)numberOfPages {
    return [self.document getPageCount];
}

- (void)drawPageAtIndex:(NSInteger)pageIndex inRect:(CGRect)printableRect {
    FSPDFPage *page = nil;
    @try {
        page = [self.document getPage:(int) pageIndex];
    } @catch (NSException *exception) {
        return;
    }
    if (page == nil) {
        return;
    }
    CGRect contentRect = self.paperRect;
    int height = contentRect.size.height;
    CGSize size = CGSizeMake([page getWidth], [page getHeight]);
    int width = height * size.width / size.height;
    if (width > contentRect.size.width) {
        width = contentRect.size.width;
        height = width * size.height / size.width;
    }

    @autoreleasepool {
        CGContextRef context = UIGraphicsGetCurrentContext();
        [Utility printPage:page inContext:context inRect:CGRectMake(0, 0, width, height) shouldDrawAnnotation:YES];
    }
}

@end
