/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <execinfo.h>
#import "Utility.h"
#import <ifaddrs.h>
#import <netinet/in.h>
#import <sys/socket.h>

#import <CommonCrypto/CommonDigest.h>
#if defined(HAS_FSPDFOBJC)
#import "PrintRenderer.h"
#import "TaskServer.h"
#import "PrintXFARenderer.h"

#define DOCUMENT_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define IOS11_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 11.0)
#define IOS13_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 13.0)

void FoxitLog(NSString *format, ...) {
#if DEBUG
    if (FOXIT_LOG_ON) {
        va_list args;
        va_start(args, format);
        NSString *msg = [[NSString alloc] initWithFormat:format arguments:args];
        NSLog(@"%@", msg);
        va_end(args);
    }
#endif
}

typedef void (^PauseUpdateBlock)(void);
typedef BOOL (^NeedPauseBlock)(void);
typedef void (^ProgressiveBlock)(UIImage *image, BOOL finish);

@interface FSPause : NSObject <FSPauseCallback>
@property (nonatomic, copy) PauseUpdateBlock updateData;
@property (nonatomic, copy) NeedPauseBlock needPause;
@property (nonatomic, assign) int pauseCallBackCount;
@end

@implementation FSPause

+ (FSPause *)pauseWithBlock:(NeedPauseBlock)needPause {
    FSPause *pause = [[FSPause alloc] init];
    pause.needPause = needPause;
    return pause;
}

- (BOOL)needPauseNow {
    BOOL needPauseNow = NO;
    if (self.needPause) {
        needPauseNow = self.needPause();
    }
    if (!needPauseNow && self.updateData) {
        if (_pauseCallBackCount % 100 == 0 ) {
            self.updateData();
        }
        _pauseCallBackCount ++ ;
    }
    return needPauseNow;
}

@end
#endif

@implementation Utility

#if defined(HAS_FSPDFOBJC)

//get the xib name according to iPhone or iPad
+ (NSString *)getXibName:(NSString *)baseXibName {
    NSString *xibName;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if ([baseXibName isEqualToString:@"FileManageListViewController"]) {
            xibName = @"FileManageViewController_iPhone";
        } else {
            xibName = [NSString stringWithFormat:@"%@_%@", baseXibName, @"iPhone"];
        }
    } else {
        if ([baseXibName isEqualToString:@"PasswordInputViewController"]) {
            xibName = @"PasswordInputViewController";
        } else if ([baseXibName isEqualToString:@"SettingViewController"]) {
            xibName = @"SettingViewController";
        } else if ([baseXibName isEqualToString:@"ContentViewController"]) {
            xibName = @"ContentViewController";
        } else if ([baseXibName isEqualToString:@"WifiSettingViewController"]) {
            xibName = @"WifiSettingViewController";
        } else {
            xibName = [NSString stringWithFormat:@"%@_%@", baseXibName, @"iPad"];
        }
    }
    return xibName;
}

//display date in yyyy-MM-dd HH:mm formate
+ (NSString *)displayDateInYMDHM:(NSDate *)date {
    return [Utility displayDateInYMDHM:date hasSymbol:YES];
}

+ (NSString *)displayDateInYMDHM:(NSDate *)date hasSymbol:(BOOL)hasSymbol {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:hasSymbol ? @"yyyy-MM-dd HH:mm" : @"yyyyMMddHHmm"];
    return [dateFormatter stringFromDate:date];
}

//Verify if point in polygon
+ (BOOL)isPointInPolygon:(CGPoint)p polygonPoints:(NSArray *)polygonPoints {
    CGMutablePathRef path = CGPathCreateMutable();
    [polygonPoints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGPoint p = [obj CGPointValue];
        if (idx == 0) {
            CGPathMoveToPoint(path, NULL, p.x, p.y);
        } else {
            CGPathAddLineToPoint(path, NULL, p.x, p.y);
        }
    }];
    CGPathCloseSubpath(path);
    BOOL ret = CGPathContainsPoint(path, NULL, p, false);
    CGPathRelease(path);

    return ret;
}

+ (CGRect)convertToCGRect:(CGPoint)p1 p2:(CGPoint)p2 {
    return CGRectMake(MIN(p1.x, p2.x),
                      MIN(p1.y, p2.y),
                      fabs(p1.x - p2.x),
                      fabs(p1.y - p2.y));
}

+ (UIEdgeInsets)convertCGRect2Insets:(CGRect)rect size:(CGSize)size {
    return UIEdgeInsetsMake(rect.origin.y,
                            rect.origin.x,
                            size.height - rect.origin.y - rect.size.height,
                            size.width - rect.origin.x - rect.size.width);
}

+ (CGRect)convertCGRectWithMargin:(CGRect)rect size:(CGSize)size margin:(int)margin {
    CGRect newRect = rect;
    newRect.origin.x = MAX(0, rect.origin.x - margin);
    newRect.origin.y = MAX(0, rect.origin.y - margin);
    newRect.size.width = MIN(rect.origin.x + rect.size.width + margin, size.width) - newRect.origin.x;
    newRect.size.height = MIN(rect.origin.y + rect.size.height + margin, size.height) - newRect.origin.y;
    return newRect;
}

//Get Rect by two points
+ (FSRectF *)convertToFSRect:(FSPointF *)p1 p2:(FSPointF *)p2 {
    FSRectF *rect = [[FSRectF alloc] init];
    rect.left = MIN([p1 getX], [p2 getX]);
    rect.right = MAX([p1 getX], [p2 getX]);
    rect.top = MAX([p1 getY], [p2 getY]);
    rect.bottom = MIN([p1 getY], [p2 getY]);
    return rect;
}

+ (FSRectF *)inflateFSRect:(FSRectF *)rect width:(float)width height:(float)height
{
    if(!rect) return nil;
    FSRectF *innerRect = [[FSRectF alloc] init];
    innerRect.left = rect.left - width; innerRect.right = rect.right + width;
    innerRect.bottom = rect.bottom - height; innerRect.top = rect.top + height;
    return innerRect;
}

+ (BOOL)isPointInFSRect:(FSRectF *)rect point:(FSPointF*)point
{
    return point.x <= rect.right && point.x >= rect.left && point.y <= rect.top && point.y >= rect.bottom;
}

//Standard Rect
+ (CGRect)getStandardRect:(CGRect)rect {
    rect.origin.x = (int) rect.origin.x;
    rect.origin.y = (int) rect.origin.y;
    rect.size.width = (int) (rect.size.width + 0.5);
    rect.size.height = (int) (rect.size.height + 0.5);
    return rect;
}

//Get UUID
+ (NSString *)getUUID {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef strUUID = CFUUIDCreateString(NULL, uuid);
    NSString *ret = [(__bridge NSString *) strUUID lowercaseString];
    CFRelease(strUUID);
    CFRelease(uuid);
    return ret;
}

+ (NSArray<FSAnnot *> *)getAnnotsInPage:(FSPDFPage *)page predicateBlock:(BOOL (^)(FSAnnot *_Nonnull))predicateBlock {
    NSMutableArray<FSAnnot *> *array = [NSMutableArray<FSAnnot *> array];
    if ([page isEmpty]) {
        return nil;
    }
    int count = [page getAnnotCount];
    for (int i = 0; i < count; i++) {
        FSAnnot *fsannot = [page getAnnot:i];
        if (predicateBlock && !predicateBlock(fsannot)) {
            continue;
        }
        [array addObject:fsannot];
    }
    return array;
}

+ (NSArray<FSAnnot *> *)getAnnotsInAnnots:(NSArray *)annots predicateBlock:(BOOL (^)(FSAnnot *_Nonnull))predicateBlock {
    NSMutableArray<FSAnnot *> *array = [NSMutableArray<FSAnnot *> array];
    int count = annots.count;
    if (!count) {
        return nil;
    }
    for (int i = 0; i < count; i++) {
        FSAnnot *fsannot = annots[i];
        if (predicateBlock && !predicateBlock(fsannot)) {
            continue;
        }
        [array addObject:fsannot];
    }
    return array;
}

+ (NSArray<FSAnnot *> *)getAnnotationsOfType:(FSAnnotType)type inPage:(FSPDFPage *)page {
    if ([page isEmpty]) {
        return nil;
    }
    int count = [page getAnnotCount];
    NSMutableArray<FSAnnot *> *array = [NSMutableArray<FSAnnot *> arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        FSAnnot *annot = [page getAnnot:i];
        if (annot && ![annot isEmpty] && annot.type == type) {
            [array addObject:annot];
        }
    }
    return array.copy;
}

+ (BOOL)isReplaceText:(FSMarkup *)annot {
    if (![annot isGrouped]) {
        return NO;
    }
    if (annot.type == FSAnnotStrikeOut) {
        FSMarkupArray *groupAnnots = [annot getGroupElements];
        for (int i = 0; i < [groupAnnots getSize]; i++) {
            FSAnnot *groupAnnot = [groupAnnots getAt:i];
            if (groupAnnot.type == FSAnnotCaret) {
                return YES;
            }
        }
    } else if (annot.type == FSAnnotCaret) {
        FSMarkupArray *groupAnnots = [annot getGroupElements];
        for (int i = 0; i < [groupAnnots getSize]; i++) {
            FSAnnot *groupAnnot = [groupAnnots getAt:i];
            if (groupAnnot.type == FSAnnotStrikeOut) {
                return YES;
            }
        }
    }
    return NO;
}

+ (FSRectF *)getCaretAnnotRect:(FSMarkup *)markup {
    if (![markup isGrouped]) {
        return markup.fsrect;
    }
    CGRect unionRect = CGRectZero;
    FSMarkupArray *groupAnnots = [markup getGroupElements];
    for (int i = 0; i < [groupAnnots getSize]; i++) {
        FSAnnot *annot = [groupAnnots getAt:i];
        if (i == 0) {
            unionRect = [self FSRectF2CGRect:annot.fsrect];
        } else {
            unionRect = CGRectUnion(unionRect, [self FSRectF2CGRect:annot.fsrect]);
        }
    }
    return [self CGRect2FSRectF:unionRect];
}

+ (FSTextPage *)getTextSelect:(FSPDFDoc *)doc pageIndex:(int)index;
{
    FSTextPage *textPage = nil;
    FSPDFPage *page = [doc getPage:index];
    if (page && ![page isEmpty]){
        if (!page.isParsed) [page startParse:FSPDFPageParsePageNormal pause:nil is_reparse:NO];
        textPage = [[FSTextPage alloc] initWithPage:page flags:FSTextPageParseTextNormal];
    }
    return textPage;
}

//get word range of string, including space
+ (NSArray *)_getUnitWordBoundary:(NSString *)str {
    NSMutableArray *array = [NSMutableArray array];
    CFStringTokenizerRef tokenizer = CFStringTokenizerCreate(kCFAllocatorDefault,
                                                             (CFStringRef) str,
                                                             CFRangeMake(0, [str length]),
                                                             kCFStringTokenizerUnitWordBoundary,
                                                             NULL);
    CFStringTokenizerTokenType tokenType = kCFStringTokenizerTokenNone;
    while ((tokenType = CFStringTokenizerAdvanceToNextToken(tokenizer)) != kCFStringTokenizerTokenNone) {
        CFRange tokenRange = CFStringTokenizerGetCurrentTokenRange(tokenizer);
        NSRange range = NSMakeRange(tokenRange.location, tokenRange.length);
        [array addObject:[NSValue valueWithRange:range]];
    }
    if (tokenizer) {
        CFRelease(tokenizer);
    }
    return array;
}

+ (NSRange)getWordByTextIndex:(int)index textPage:(FSTextPage *)fstextPage {
    __block NSRange retRange = NSMakeRange(index, 1);

    int pageTotalCharCount = 0;

    if (fstextPage != nil) {
        pageTotalCharCount = [fstextPage getCharCount];
    }

    int startIndex = MAX(0, index - 25);
    int endIndex = MIN(pageTotalCharCount - 1, index + 25);
    index -= startIndex;

    NSString *str = [fstextPage getChars:MIN(startIndex, endIndex) count:ABS(endIndex - startIndex) + 1];
    NSArray *array = [self _getUnitWordBoundary:str];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSRange range = [obj rangeValue];
        if (NSLocationInRange(index, range)) {
            NSString *tmp = [str substringWithRange:range];
            if ([tmp isEqualToString:@" "]) {
                NSUInteger nextIndex = idx + 1;
                if (nextIndex < array.count) {
                    range = [[array objectAtIndex:nextIndex] rangeValue];
                }
            }
            retRange = NSMakeRange(startIndex + range.location, range.length);
            *stop = YES;
        }
    }];
    return retRange;
}

+ (NSArray *)getTextRects:(FSTextPage *)fstextPage start:(int)start count:(int)count {
    NSMutableArray *ret = [NSMutableArray array];

    if (fstextPage != nil) {
        int rectCount = [fstextPage getTextRectCount:start count:count];
        for (int i = 0; i < rectCount; i++) {
            FSRectF *dibRect = [fstextPage getTextRect:i];
            if (dibRect.getLeft == dibRect.getRight || dibRect.getTop == dibRect.getBottom) {
                continue;
            }

            FSRotation direction = [fstextPage getBaselineRotation:i];
            NSString *text = [fstextPage getTextInRect:dibRect];
            NSArray *array = [NSArray arrayWithObjects:[NSValue valueWithCGRect:[Utility FSRectF2CGRect:dibRect]], @(direction), text, nil];

            [ret addObject:array];
        }

        //merge rects if possible
        if (ret.count > 1) {
            int i = 0;
            while (i < ret.count - 1) {
                int j = i + 1;
                while (j < ret.count) {
                    FSRectF *rect1 = [Utility CGRect2FSRectF:[[[ret objectAtIndex:i] objectAtIndex:0] CGRectValue]];
                    FSRectF *rect2 = [Utility CGRect2FSRectF:[[[ret objectAtIndex:j] objectAtIndex:0] CGRectValue]];

                    int direction1 = [[[ret objectAtIndex:i] objectAtIndex:1] intValue];
                    int direction2 = [[[ret objectAtIndex:j] objectAtIndex:1] intValue];
                    BOOL adjcent = NO;
                    if (direction1 == direction2) {
                        adjcent = NO;
                    }
                    if (adjcent) {
                        FSRectF *rectResult = [[FSRectF alloc] init];
                        [rectResult setLeft:MIN([rect1 getLeft], [rect2 getLeft])];
                        [rectResult setBottom:MAX([rect1 getTop], [rect2 getTop])];
                        [rectResult setRight:MAX([rect1 getRight], [rect2 getRight])];
                        [rectResult setTop:MIN([rect1 getBottom], [rect2 getBottom])];
                        NSArray *array = [NSArray arrayWithObjects:[NSValue valueWithCGRect:[Utility FSRectF2CGRect:rectResult]], @(direction1), nil];
                        [ret replaceObjectAtIndex:i withObject:array];
                        [ret removeObjectAtIndex:j];
                    } else {
                        j++;
                    }
                }
                i++;
            }
        }
    }

    return ret;
}

+ (BOOL)isGivenPath:(NSString *)path type:(NSString *)type {
    if ([type isEqualToString:@"*"]) {
        return YES;
    }
    NSString *dotType = [NSString stringWithFormat:@".%@", type];
    if ([path.pathExtension.lowercaseString isEqualToString:type.lowercaseString]) {
        return YES;
    } else if ([path.lowercaseString isEqualToString:dotType]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isGivenExtension:(NSString *)extension type:(NSString *)type {
    if ([type isEqualToString:@"*"]) {
        return YES;
    }
    return [extension.lowercaseString isEqualToString:type.lowercaseString];
}
+ (FSRectF *)getPageRealRect:(FSPDFPage *)page{
    FSRectF *realRect = [page getBox:FSPDFPageCropBox];
    if ([realRect isEmpty]) realRect = [page getBox:FSPDFPageMediaBox];
    if ([realRect isEmpty]) realRect = [page getBox:FSPDFPageTrimBox];
    if ([realRect isEmpty]) realRect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:[page getWidth] top1:[page getHeight]];
    return realRect;
}

#pragma mark - methods from DmUtil
#pragma mark Static method

static void _CGDataProviderReleaseDataCallback(void *info, const void *data, size_t size) {
    free((void *) data);
}

+ (UIImage *)dib2img:(void *)pBuf size:(int)size dibWidth:(int)dibWidth dibHeight:(int)dibHeight withAlpha:(BOOL)withAlpha {
    int bit = 3;
    if (withAlpha) {
        bit = 4;
    }
    unsigned char *buf = (unsigned char *) pBuf;
    int stride32 = dibWidth * bit;
    dispatch_apply(dibHeight, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(size_t ri) {
        dispatch_apply(dibWidth, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(size_t j) {
            long i = dibHeight - 1 - ri;
            unsigned char tmp = buf[i * stride32 + j * bit];
            buf[i * stride32 + j * bit] = buf[i * stride32 + j * bit + 2];
            buf[i * stride32 + j * bit + 2] = tmp;
        });
    });

    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, pBuf, size, _CGDataProviderReleaseDataCallback);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    if (withAlpha) {
        bitmapInfo = bitmapInfo | kCGImageAlphaLast;
    }
    CGImageRef image = CGImageCreate(dibWidth, dibHeight, 8, withAlpha ? 32 : 24, dibWidth * (withAlpha ? 4 : 3),
                                     colorSpace, bitmapInfo,
                                     provider, NULL, YES, kCGRenderingIntentDefault);
    UIImage *img = [UIImage imageWithCGImage:image scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    CGImageRelease(image);
    CGColorSpaceRelease(colorSpace);
    CGDataProviderRelease(provider);
    return img;
}

+ (UIImage *)rgbDib2img:(const void *)pBuf size:(int)size dibWidth:(int)dibWidth dibHeight:(int)dibHeight withAlpha:(BOOL)withAlpha freeWhenDone:(BOOL)b {
    @autoreleasepool{
        CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, pBuf, size, b ? _CGDataProviderReleaseDataCallback : nil);
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
        if (withAlpha) {
            bitmapInfo = bitmapInfo | kCGImageAlphaLast;
        }
        CGImageRef image = CGImageCreate(dibWidth, dibHeight, 8, withAlpha ? 32 : 24, dibWidth * (withAlpha ? 4 : 3),
                                         colorSpace, bitmapInfo,
                                         provider, NULL, YES, kCGRenderingIntentDefault);
        UIImage *img = [UIImage imageWithCGImage:image
                                           scale:
                        //                    1.0f
                        [UIScreen mainScreen].scale
                                     orientation:UIImageOrientationUp];
        CGImageRelease(image);
        CGColorSpaceRelease(colorSpace);
        CGDataProviderRelease(provider);
        return img;
    }
}

+ (BOOL)rectEqualToRect:(FSRectF *)rect rect:(FSRectF *)rect1 {
    if ([rect getLeft] == [rect1 getLeft] &&
        [rect getRight] == [rect1 getRight] &&
        [rect getTop] == [rect getTop] &&
        [rect getBottom] == [rect1 getBottom]) {
        return YES;
    }
    return NO;
}

+ (BOOL)quadsEqualToQuads:(FSQuadPoints *)quads1 quads:(FSQuadPoints *)quads2 {
    return [Utility pointEqualToPoint:[quads1 getFirst] point:[quads2 getFirst]] &&
           [Utility pointEqualToPoint:[quads1 getSecond]
                                point:[quads2 getSecond]] &&
           [Utility pointEqualToPoint:[quads1 getThird]
                                point:[quads2 getThird]] &&
           [Utility pointEqualToPoint:[quads1 getFourth]
                                point:[quads2 getFourth]];
}

+ (BOOL)pointEqualToPoint:(FSPointF *)point1 point:(FSPointF *)point2 {
    return fabsf(point1.x - point2.x) < 1e-4 &&
           fabsf(point1.y - point2.y) < 1e-4;
}

+ (BOOL)inkListEqualToInkList:(FSPath *)inkList1 inkList:(FSPath *)inkList2 {
    int count = [inkList1 getPointCount];
    if (count != [inkList2 getPointCount]) {
        return NO;
    }
    for (int i = 0; i < count; i++) {
        if (![Utility pointEqualToPoint:[inkList2 getPoint:i] point:[inkList2 getPoint:i]]) {
            return NO;
        }
        if ([inkList1 getPointType:i] != [inkList2 getPointType:i]) {
            return NO;
        }
    }
    return YES;
}

+ (CGRect)FSRectF2CGRect:(FSRectF *)fsrect {
    if (fsrect == nil) {
        return CGRectZero;
    }
    return CGRectMake(fsrect.getLeft, fsrect.getTop, (fsrect.getRight - fsrect.getLeft), (fsrect.getBottom - fsrect.getTop));
}

+ (FSRectF *)CGRect2FSRectF:(CGRect)rect {
    FSRectF *fsrect = [[FSRectF alloc] init];
    [fsrect setLeft:rect.origin.x];
    [fsrect setBottom:rect.origin.y + rect.size.height];
    [fsrect setRight:rect.origin.x + rect.size.width];
    [fsrect setTop:rect.origin.y];
    return fsrect;
}

+ (NSDate *)convertFSDateTime2NSDate:(FSDateTime *)time {
    if ([time getYear] > 10000 || [time getYear] == 0 ||
        [time getMonth] > 12 || [time getMonth] == 0 ||
        [time getDay] > 31 || [time getDay] == 0 ||
        [time getHour] > 24 ||
        [time getMinute] > 60 ||
        [time getSecond] > 60) {
        return nil;
    }

    unsigned short hour = [time getHour];
    unsigned short minute = [time getMinute];

    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:[time getYear]];
    [comps setMonth:[time getMonth]];
    [comps setDay:[time getDay]];
    [comps setHour:hour];
    [comps setMinute:minute];
    [comps setSecond:[time getSecond]];
    [comps setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:[time getUtc_hour_offset] * 3600 + [time getUtc_minute_offset] * 60]];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [gregorian dateFromComponents:comps];
//    date = [date dateByAddingTimeInterval: [time getUtc_hour_offset] * 3600 + [time getUtc_minute_offset] * 60];
    return date;
}

+ (FSDateTime *)convert2FSDateTime:(NSDate *)date {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:date];
    FSDateTime *time = [[FSDateTime alloc] init];
    time.year = [comps year];
    time.month = [comps month];
    time.day = [comps day];
    time.hour = [comps hour];
    time.minute = [comps minute];
    time.second = [comps second];
    
    NSTimeZone* localTimeZone = [NSTimeZone localTimeZone];
    NSInteger gmtOffset = [localTimeZone secondsFromGMTForDate:[NSDate date]];
    [time setUtc_hour_offset:gmtOffset/3600];
    [time setUtc_minute_offset:(gmtOffset % 3600) / 60];
    return time;
}

CGPDFDocumentRef GetPDFDocumentRef(const char *filename) {
    CFStringRef path;
    CFURLRef url;
    CGPDFDocumentRef document;

    path = CFStringCreateWithCString(NULL, filename, kCFStringEncodingUTF8);

    url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, 0);

    CFRelease(path);

    document = CGPDFDocumentCreateWithURL(url);

    if (document) {
        if (CGPDFDocumentGetNumberOfPages(document) == 0) {
            CGPDFDocumentRelease(document);
            document = nil;
            printf("`%s' needs at least onepage!\n", filename);
        }
    } else {
        printf("`%s' is corrupted.\n", filename);
    }

    CFRelease(url);
    return document;
}

+ (CGSize)getPDFPageSizeWithIndex:(NSUInteger)index pdfPath:(NSString *)path {
    @autoreleasepool {
        @synchronized(self) {
            @try {
                FSPDFDoc *fspdfdoc = [[FSPDFDoc alloc] initWithPath:path];
                FSErrorCode ret = [fspdfdoc load:nil];
                if(!ret)
                {
                    FSPDFPage* page = [fspdfdoc getPage:0];
                    if ([page isEmpty]) {
                        return CGSizeZero;
                    }
                    return CGSizeMake([page getWidth], [page getHeight]);
                }
                return CGSizeMake(0, 0);
            }
            @catch(NSException* e)
            {
                return CGSizeMake(0, 0);
            }
        }
    }
}

+ (UIImage *)drawXFAPage:(FSXFAPage *)page dibWidth:(int)dibWidth dibHeight:(int)dibHeight shouldDrawAnnotation:(BOOL)shouldDrawAnnotation needPause:(BOOL (^__nullable)(void))needPause rotation:(FSRotation)rotation{
    UIImage *img = nil;
    
    @autoreleasepool {
        CGFloat scale = [UIScreen mainScreen].scale;
#ifdef CONTEXT_DRAW
        scale = 1;
#endif
        int newDibWidth = dibWidth * scale;
        int newDibHeight = dibHeight * scale;
        int newPdfX = 0;
        int newPdfY = 0;
        int newPdfWidth = dibWidth * scale;
        int newPdfHeight = dibHeight * scale;
        
        {
            if (!page) {
                return img;
            }
            
            //create a 24bit bitmap
            int size = newDibWidth * newDibHeight * 3;
            void *pBuf = malloc(size);
            FSBitmap *fsbitmap = [[FSBitmap alloc] initWithWidth:newDibWidth height:newDibHeight format:FSBitmapDIBRgb buffer:[NSData dataWithBytesNoCopy:(unsigned char *) pBuf length:size freeWhenDone:NO] pitch:newDibWidth * 3];
            
            //render page, must have
            FSRenderer *fsrenderer = [[FSRenderer alloc] initWithBitmap:fsbitmap is_rgb_order:YES];
            
            FSMatrix2D *xfamatrix = [page getDisplayMatrix:newPdfX top:newPdfY width:newPdfWidth height:newPdfHeight rotate:rotation];
            
            memset(pBuf, 0xff, size);
            
            void (^releaseRender)(BOOL freepBuf) = ^(BOOL freepBuf) {
                if (freepBuf) {
                    free(pBuf);
                }
            };
            int contextFlag = shouldDrawAnnotation ? (FSRendererRenderAnnot | FSRendererRenderPage) : FSRendererRenderPage;
            [fsrenderer setRenderContentFlags:contextFlag];
            [fsrenderer enableForPrint:NO];
            
            FSPause *pause = [FSPause pauseWithBlock:needPause];
            FSProgressive *ret = [fsrenderer startRenderXFAPage:page matrix:xfamatrix is_highlight:NO pause:pause];
            
            if (ret != nil) {
                FSProgressiveState state = [ret resume];
                while (FSProgressiveToBeContinued == state) {
                    state = [ret resume];
                }
                if (FSProgressiveFinished != state) {
                    releaseRender(YES);
                    return img;
                }
            }
            
            img = [Utility rgbDib2img:pBuf size:size dibWidth:newDibWidth dibHeight:newDibHeight withAlpha:NO freeWhenDone:YES];
            if (img) {
                fsrenderer = nil;
                fsbitmap = nil;
                xfamatrix = nil;
            }
            
            releaseRender(img == nil);
        }
    }
    
    return img;
}

+ (UIImage *)drawPageThumbnailWithPDFPath:(NSString *)pdfPath pageIndex:(int)pageIndex pageSize:(CGSize)size rotation:(FSRotation)rotation {
    @autoreleasepool {
        @synchronized(self) {
            @try {
                FSPDFDoc *fspdfdoc = [[FSPDFDoc alloc] initWithPath:pdfPath];
                FSErrorCode ret = [fspdfdoc load:nil];
                
                if (!ret) {
                    FSPDFPage* page = [fspdfdoc getPage:pageIndex];
                    if ([page isEmpty]) {
                        return nil;
                    }
                    return [Utility drawPage:page targetSize:size shouldDrawAnnotation:YES needPause:nil rotation:rotation enableForPrint:NO];
                }
                
                return nil;
            }
            @catch(NSException* e)
            {
                return nil;
            }
        }
    }
}

+ (FSPDFDocEncryptType)getDocumentSecurityType:(NSString *)filePath taskServer:(TaskServer *_Nullable)taskServer {
    __block FSErrorCode ret = FSErrUnknown;
    Task *task = [[Task alloc] init];
    task.run = ^() {
        //        FSLibraryModuleRight right = [FSLibrary getModuleRight:FSLibraryModuleNameStandard];
        //        if (right == FSLibraryModuleRightNone || right == FSLibraryModuleRightUnknown)
        //            return;
        @autoreleasepool {
            FSPDFDoc *fspdfdoc = [[FSPDFDoc alloc] initWithPath:filePath];
            ret = [fspdfdoc load:nil];

        }
    };
    if (!taskServer)
        taskServer = [[TaskServer alloc] init];
    [taskServer executeSync:task];

    if (ret == FSErrSuccess) {
        return FSPDFDocEncryptNone;
    } else if (ret == FSErrPassword) {
        return FSPDFDocEncryptPassword;
    } else if (ret == FSErrHandle) {
        return FSPDFDocEncryptRMS;
    } else {
        return FSPDFDocEncryptCustom;
    }
}

+ (NSString *)convert2SysFontString:(NSString *)str {
    NSString *ret = str;
    if ([str isEqualToString:@"Times-Roman"]) {
        ret = @"TimesNewRomanPSMT";
    } else if ([str isEqualToString:@"Times-Bold"]) {
        ret = @"TimesNewRomanPS-BoldMT";
    } else if ([str isEqualToString:@"Times-Italic"]) {
        ret = @"TimesNewRomanPS-ItalicMT";
    } else if ([str isEqualToString:@"Times-BoldItalic"]) {
        ret = @"TimesNewRomanPS-BoldItalicMT";
    }
    return ret;
}

//Get test size by font
+ (CGSize)getTestSize:(UIFont *)font {
    return [@"WM" sizeWithAttributes:@{NSFontAttributeName : font}];
}

+ (float)getAnnotMinXMarginInPDF:(FSPDFViewCtrl *)pdfViewCtrl pageIndex:(int)pageIndex {
    CGRect pvRect = CGRectMake(0, 0, 2, 2);
    FSRectF *pdfRect = [pdfViewCtrl convertPageViewRectToPdfRect:pvRect pageIndex:pageIndex];
    return pdfRect.right - pdfRect.left;
}

+ (float)getAnnotMinYMarginInPDF:(FSPDFViewCtrl *)pdfViewCtrl pageIndex:(int)pageIndex {
    CGRect pvRect = CGRectMake(0, 0, 2, 2);
    FSRectF *pdfRect = [pdfViewCtrl convertPageViewRectToPdfRect:pvRect pageIndex:pageIndex];
    return pdfRect.top - pdfRect.bottom;
}

+ (float)convertWidth:(float)width fromPageViewToPDF:(FSPDFViewCtrl *)pdfViewCtrl pageIndex:(int)pageIndex {
    FSRectF *fsRect = [[FSRectF alloc] init];
    [fsRect setLeft:0];
    [fsRect setBottom:width];
    [fsRect setRight:width];
    [fsRect setTop:0];
    CGRect pvRect = [pdfViewCtrl convertPdfRectToPageViewRect:fsRect pageIndex:pageIndex];
    return pvRect.size.width;
}

+ (CGRect)getAnnotRect:(FSAnnot *)annot pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    FSPDFPage *page = [pdfViewCtrl.currentDoc getPage:annot.pageIndex];
    if (page && ![page isEmpty]) {
        CGRect retRect = CGRectZero;
        FSMatrix2D *fsmatrix = [pdfViewCtrl getDisplayMatrix:annot.pageIndex];
//        BOOL shouldTransformIcon = (annot.type != FSAnnotNote && annot.type != FSAnnotFileAttachment);
        FSRectI *annotRect = [annot getDeviceRect:fsmatrix];
        retRect.origin.x = [annotRect getLeft];
        retRect.origin.y = [annotRect getTop];
        retRect.size.width = [annotRect getRight] - [annotRect getLeft];
        retRect.size.height = [annotRect getBottom] - [annotRect getTop];
        return retRect;
    }
    return CGRectZero;
}

+ (UIImage *)getAnnotImage:(FSAnnot *)annot pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    int pageIndex = annot.pageIndex;
    CGRect rect = [self getAnnotRect:annot pdfViewCtrl:pdfViewCtrl];
    if (annot.type == FSAnnotFreeText) {
        if (annot.intent.length == 0 || [annot.intent isEqualToString:@"FreeTextCallout"]) {
            rect = CGRectInset(rect, -1, -1);
        }
    }
    if (rect.size.width == 0 || rect.size.height == 0) {
        return nil;
    }
#if false // draw in bitmap
    int dibWidth = rect.size.width;
    int dibHeight = rect.size.height;
    CGFloat scale = [UIScreen mainScreen].scale;
    int newDibWidth = dibWidth * scale;
    int newDibHeight = dibHeight * scale;
    int size = newDibWidth * newDibHeight * 4;
    void *pBuf = malloc(size);
    memset(pBuf, 0x00, size);
    FSBitmap *fsbitmap = [[FSBitmap alloc] initWithWidth:newDibWidth height:newDibHeight format:FSBitmapDIBArgb buffer:[NSData dataWithBytesNoCopy:(unsigned char *) pBuf length:size freeWhenDone:NO] pitch:newDibWidth * 4];
    FSRenderer *fsrenderer = [[FSRenderer alloc] initWithBitmap:fsbitmap is_rgb_order:YES];
    [fsrenderer setTransformAnnotIcon:NO];
    FSMatrix2D *fsmatrix = [pdfViewCtrl getDisplayMatrix:pageIndex fromOrigin:CGPointMake(-rect.origin.x, -rect.origin.y)];
//    NSLog(@"matrix: %.1f, %.1f, %.1f, %.1f, %.1f, %.1f", [fsmatrix getA], [fsmatrix getB], [fsmatrix getC], [fsmatrix getD], [fsmatrix getE], [fsmatrix getF]);
    
    int contextFlag = FSRendererRenderAnnot;
    [fsrenderer setRenderContentFlags:contextFlag];
    
    BOOL isOK = [fsrenderer renderAnnot:annot matrix:fsmatrix];
    assert(isOK);
    UIImage *img = [Utility rgbDib2img:pBuf size:size dibWidth:newDibWidth dibHeight:newDibHeight withAlpha:YES freeWhenDone:YES];
    if (img) {
        [UIImagePNGRepresentation(img) writeToFile:@"/Users/lzw/Desktop/annotImage.png" atomically:YES];
        return img;
    } else {
        free(pBuf);
        return nil;
    }
#else     // draw in context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();

    FSMatrix2D *fsmatrix = [pdfViewCtrl getDisplayMatrix:pageIndex fromOrigin:CGPointMake(-rect.origin.x, -rect.origin.y)];
    FSRenderer *fsrenderer = nil;
    @try {
        fsrenderer = [[FSRenderer alloc] initWithContext:context device_type:FSRendererDeviceDisplay];
    } @catch (NSException *exception) {
        UIGraphicsEndImageContext();
        return nil;
    }

    if (pdfViewCtrl.colorMode != FSRendererColorModeNormal) {
        [fsrenderer setColorMode:pdfViewCtrl.colorMode];
        [fsrenderer setMappingModeColors:pdfViewCtrl.mappingModeBackgroundColor.fs_argbValue foreground_color:pdfViewCtrl.mappingModeForegroundColor.fs_argbValue];
    }
    [fsrenderer renderAnnot:annot matrix:fsmatrix];

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
#endif
}

static const char *s_StandardFontNames[] = {
    "Courier",
    "Courier-Bold",
    "Courier-BoldOblique",
    "Courier-Oblique",
    "Helvetica",
    "Helvetica-Bold",
    "Helvetica-BoldOblique",
    "Helvetica-Oblique",
    "Times-Roman",
    "Times-Bold",
    "Times-BoldItalic",
    "Times-Italic",
    "Symbol",
    "ZapfDingbats"};

+ (int)toStandardFontID:(NSString *)fontName {
    for (int i = 0; i < sizeof(s_StandardFontNames) / sizeof(const char *); i++) {
        NSString *stdFontName = [NSString stringWithUTF8String:s_StandardFontNames[i]];
        if ([fontName isEqualToString:stdFontName]) {
            return i;
        }
    }
    return -1;
}

+ (FSRectF *)normalizeFSRect:(FSRectF *)dibRect {
    if (dibRect.left > dibRect.right) {
        float tmp = dibRect.left;
        dibRect.left = dibRect.right;
        dibRect.right = tmp;
    }
    if (dibRect.bottom > dibRect.top) {
        float tmp = dibRect.top;
        dibRect.top = dibRect.bottom;
        dibRect.bottom = tmp;
    }
    return dibRect;
}

+ (CGRect)normalizeCGRect:(CGRect)rect {
    if (rect.size.width < 0) {
        rect.origin.x += rect.size.width;
        rect.size.width = -rect.size.width;
    }
    if (rect.size.height < 0) {
        rect.origin.y += rect.size.height;
        rect.size.height = -rect.size.height;
    }
    return rect;
}

+ (FSRectF *)makeFSRectWithLeft:(float)left top:(float)top right:(float)right bottom:(float)bottom {
    FSRectF *rect = [[FSRectF alloc] init];
    [rect setLeft:left];
    [rect setBottom:bottom];
    [rect setRight:right];
    [rect setTop:top];
    return rect;
}

+ (FSPointF *)makeFSPointWithX:(float)x y:(float)y {
    FSPointF *point = [[FSPointF alloc] init];
    [point set:x y:y];
    return point;
}

+ (NSString *)getErrorCodeDescription:(FSErrorCode)error {
    switch (error) {
    case FSErrSecurityHandler:
        return FSLocalizedForKey(@"kInvalidSecurityHandler");
    case FSErrFile:
        return FSLocalizedForKey(@"kUnfoundOrCannotOpen");
    case FSErrFormat:
        return FSLocalizedForKey(@"kInvalidFormat");
    case FSErrPassword:
        return FSLocalizedForKey(@"kDocPasswordError");
    case FSErrHandle:
        return FSLocalizedForKey(@"kHandlerError");
    case FSErrCertificate:
        return FSLocalizedForKey(@"kWrongCertificate");
    case FSErrUnknown:
        return FSLocalizedForKey(@"kUnknownError");
    case FSErrInvalidLicense:
        return FSLocalizedForKey(@"kInvalidLibraryLicense");
    case FSErrParam:
        return FSLocalizedForKey(@"kInvalidParameter");
    case FSErrUnsupported:
        return FSLocalizedForKey(@"kUnsupportedType");
    case FSErrOutOfMemory:
        return FSLocalizedForKey(@"kOutOfMemory");
    default:
        return @"";
    }
}

+ (FSAnnot *)getAnnotByNM:(NSString *)nm inPage:(FSPDFPage *)page {
    for (int i = 0; i < [page getAnnotCount]; i++) {
        FSAnnot *annot = [page getAnnot:i];
        if ([annot.NM isEqualToString:nm]) {
            return annot;
        }
    }
    return nil;
}

+ (FSRectF *)cloneRect:(FSRectF *)rect {
    FSRectF *clone = [[FSRectF alloc] init];
    [clone setLeft:rect.left];
    [clone setBottom:rect.bottom];
    [clone setRight:rect.right];
    [clone setTop:rect.top];
    return clone;
}

+ (FSPointF *)clonePoint:(FSPointF *)point {
    FSPointF *clone = [[FSPointF alloc] init];
    [clone set:point.x y:point.y];
    return clone;
}

+ (FSPath *)cloneInkList:(FSPath *)inkList {
    if (!inkList || [inkList isEmpty]) {
        return nil;
    }
    int count = [inkList getPointCount];
    FSPath *clone = [[FSPath alloc] init];
    if (count > 0) {
        [clone moveTo:[inkList getPoint:0]];
    }
    FSPointF *pointZero = [Utility makeFSPointWithX:0 y:0];
    for (int i = 1; i < count; i++) {
        [clone lineTo:pointZero];
    }
    for (int i = 0; i < count; i++) {
        [clone setPoint:i point:[inkList getPoint:i] type:[inkList getPointType:i]];
    }
    return clone;
}

+ (UIImage *)scaleToSize:(UIImage *)oriImage size:(CGSize)size {
    CGFloat width = CGImageGetWidth(oriImage.CGImage);
    CGFloat height = CGImageGetHeight(oriImage.CGImage);

    float verticalRadio = size.height * 1.0 / height;
    float horizontalRadio = size.width * 1.0 / width;

    float radio = 1;
    if (verticalRadio > 1 && horizontalRadio > 1) {
        radio = verticalRadio > horizontalRadio ? horizontalRadio : verticalRadio;
    } else {
        radio = verticalRadio < horizontalRadio ? verticalRadio : horizontalRadio;
    }

    width = width * radio;
    height = height * radio;

    int xPos = (size.width - width) / 2;
    int yPos = (size.height - height) / 2;

    UIGraphicsBeginImageContext(size);

    [oriImage drawInRect:CGRectMake(xPos, yPos, width, height)];

    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return scaledImage;
}

//Get file type thumbnail name
+ (NSString *)getThumbnailName:(NSString *)path {
    NSString *ret = DEVICE_iPHONE ? @"thumbnail_none_iphone" : @"thumbnail_none_ipad";
    if ([self isPDFPath:path]) {
        ret = DEVICE_iPHONE ? @"thumbnail_pdf_iphone" : @"thumbnail_pdf_ipad";
    }
    return ret;
}

#pragma mark file related

//Verify file type
+ (BOOL)isPDFPath:(NSString *)path {
    if ([path.pathExtension.lowercaseString isEqualToString:@"pdf"]) {
        return YES;
    } else if ([path.lowercaseString isEqualToString:@".pdf"]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isPDFExtension:(NSString *)extension {
    return [extension.lowercaseString isEqualToString:@"pdf"];
}

+ (BOOL)isSupportFormat:(NSString *)path {
    if ([Utility isPDFPath:path] ||
        [Utility isGivenPath:path
                        type:@"txt"] ||
        [Utility isGivenPath:path
                        type:@"doc"] ||
        [Utility isGivenPath:path
                        type:@"docx"] ||
        [Utility isGivenPath:path
                        type:@"xls"] ||
        [Utility isGivenPath:path
                        type:@"xlsx"] ||
        [Utility isGivenPath:path
                        type:@"ppt"] ||
        [Utility isGivenPath:path
                        type:@"pptx"] ||
        [Utility isGivenPath:path
                        type:@"png"] ||
        [Utility isGivenPath:path
                        type:@"jpg"] ||
        [Utility isGivenPath:path
                        type:@"jpeg"] ||
        [Utility isGivenPath:path
                        type:@"zip"] ||
        [Utility isGivenPath:path
                        type:@"xml"] ||
        [Utility isGivenPath:path
                        type:@"html"] ||
        [Utility isGivenPath:path
                        type:@"htm"] ||
        [Utility isGivenPath:path
                        type:@"bmp"] ||
        [Utility isGivenPath:path
                        type:@"tif"] ||
        [Utility isGivenPath:path
                        type:@"tiff"] ||
        [Utility isGivenPath:path
                        type:@"gif"]) {
        return YES;
    }
    return NO;
}

//Get filetype icon name
+ (NSString *)getIconName:(NSString *)path {
    NSString *ret = @"list_none";
    if ([Utility isPDFPath:path]) {
        ret = @"list_pdf";
    }
    return ret;
}

//display the file size string
+ (NSString *)displayFileSize:(unsigned long long)byte {
    if (byte < 1024) {
        return [NSString stringWithFormat:@"%lld B", byte];
    } else if (byte < 1024 * 1024) {
        return [NSString stringWithFormat:@"%.2f KB", byte / 1024.0];
    } else if (byte < 1024 * 1024 * 1024) {
        return [NSString stringWithFormat:@"%.2f MB", byte / (1024 * 1024.0)];
    } else {
        return [NSString stringWithFormat:@"%.2f GB", byte / (1024 * 1024 * 1024.0)];
    }
}

+ (ScreenSizeMode)getScreenSizeMode {
    const NSInteger screenWidth = SCREENWIDTH;
    const NSInteger screenHeight = SCREENHEIGHT;
    if (screenWidth == 480 || screenHeight == 480) {
        return ScreenSizeMode_35;
    } else if (screenWidth == 568 || screenHeight == 568) {
        return ScreenSizeMode_40;
    } else if (screenWidth == 667 || screenHeight == 667) {
        return ScreenSizeMode_47;
    } else if (screenWidth == 736 || screenHeight == 736) {
        return ScreenSizeMode_55;
    } else if (screenWidth == 1024 || screenHeight == 1024) {
        return ScreenSizeMode_97;
    }
    return ScreenSizeMode_35;
}

#define LIBRARY_PATH [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define DATA_FOLDER_NAME @"Data"
#define INTERNAL_FOLDER_NAME @"Internal"
#define ATTACHMENT_FOLDER_NAME @"Attachment"
#define RENDITION_FOLDER_NAME @"Rendition"
#define DATA_PATH [LIBRARY_PATH stringByAppendingPathComponent:DATA_FOLDER_NAME]
#define INTERNAL_PATH [DATA_PATH stringByAppendingPathComponent:INTERNAL_FOLDER_NAME]
#define ATTACHMENT_PATH [INTERNAL_PATH stringByAppendingPathComponent:ATTACHMENT_FOLDER_NAME]
#define RENDITION_PATH [INTERNAL_PATH stringByAppendingPathComponent:RENDITION_FOLDER_NAME]

+ (NSString *)getAttachmentTempFilePath:(FSFileAttachment *)attachment {
    NSString *attachmentFileName = [[attachment getFileSpec] getFileName];
    return [ATTACHMENT_PATH stringByAppendingString:[NSString stringWithFormat:@"/%@_%i.%@", attachment.NM, attachment.pageIndex, [attachmentFileName pathExtension]]];
}
+ (NSString *)getDocumentAttachmentTempFilePath:(FSFileSpec *)attachmentFile PDFPath:(NSString *)PDFPath {
    NSString *attachmentFileName = [attachmentFile getFileName];
    return [ATTACHMENT_PATH stringByAppendingString:[NSString stringWithFormat:@"/%@_%@.%@", [Utility getStringMD5:PDFPath], [Utility getStringMD5:attachmentFileName], [attachmentFileName pathExtension]]];
}
+ (NSString *)getDocumentRenditionTempFilePath:(FSFileSpec *)renditionFile PDFPath:(NSString *)PDFPath {
    if ([renditionFile isEmpty]) {
        NSLog(@"%@ is isEmpty",renditionFile);
        return nil;
    }
    NSString *renditionFileName = [renditionFile getFileName];
    return [RENDITION_PATH stringByAppendingString:[NSString stringWithFormat:@"/%@_%@.%@", [Utility getStringMD5:PDFPath], [Utility getStringMD5:renditionFileName], [renditionFileName pathExtension]]];
}

+ (NSString *)getDocumentSoundTempFilePath:(FSSound *)sound PDFPath:(NSString *)PDFPath {
    @try {
        FSPDFStream *soundStream = [sound getSoundStream];
        NSMutableData *data = [NSMutableData dataWithData:[soundStream getData:NO]];
        NSString *savePath = [RENDITION_PATH stringByAppendingString:[NSString stringWithFormat:@"/%@_%@.%@", [Utility getStringMD5:PDFPath], [Utility getDataMD5:data], @"wav"]];
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        if ([defaultManager fileExistsAtPath:savePath]) return savePath;
        
        if (![defaultManager fileExistsAtPath:[savePath stringByDeletingLastPathComponent]]) {
            [defaultManager createDirectoryAtPath:[savePath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        NSMutableData *randomAccessFile = [NSMutableData data];
        uint64_t streamSize = [soundStream getDataSize:NO];
        
        short bit = (short) sound.getBits;

        uint64_t riffSize = streamSize + 36;
        
        NSUInteger lInt = 4;
        NSUInteger sInt = 2;
        
        [randomAccessFile appendData:[@"RIFF" dataUsingEncoding:NSUTF8StringEncoding]];
        [randomAccessFile appendBytes:&riffSize length:lInt];
        [randomAccessFile appendData:[@"WAVEfmt " dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSUInteger chunkSize = 16;
        [randomAccessFile appendBytes:&chunkSize length:lInt];
        
        short format = 1;
        [randomAccessFile appendBytes:&format length:sInt];
        
        short channelCount = sound.getChannelCount;
        [randomAccessFile appendBytes:&channelCount length:sInt];
        
        int64_t rate = (int64_t)sound.getSamplingRate;
        [randomAccessFile appendBytes:&rate length:lInt];
        
        int64_t bytePerSec = rate * channelCount * bit / 8;
        [randomAccessFile appendBytes:&bytePerSec length:lInt];
        
        short blockAlign = (short) (bit * channelCount / 8);
        [randomAccessFile appendBytes:&blockAlign length:sInt];
        [randomAccessFile appendBytes:&bit length:sInt];
        [randomAccessFile appendData:[@"data" dataUsingEncoding:NSUTF8StringEncoding]];
        
        BOOL ret = NO;
        NSUInteger encodingFormat = sound.getSampleEncodingFormat;
        switch (encodingFormat) {
            case FSSoundSampleEncodingFormatALaw:
                break;
            case FSSoundSampleEncodingFormatMuLaw:
                break;
            case FSSoundSampleEncodingFormatSigned:
            {
                NSMutableData *buffer = [NSMutableData data];
                int j = 0, k = 0;
                for (int i = 0; i < streamSize; i += 2) {
                    NSData *low = [data subdataWithRange:NSMakeRange(j++, 1)];
                    NSData *high = [data subdataWithRange:NSMakeRange(j++, 1)];
                    [buffer replaceBytesInRange:NSMakeRange(k++, 1) withBytes:high.bytes];
                    [buffer replaceBytesInRange:NSMakeRange(k++, 1) withBytes:low.bytes];
                }
                [randomAccessFile appendData:buffer];
                ret = YES;
            }
                break;
            case FSSoundSampleEncodingFormatRaw:
            default:
            {
                [randomAccessFile appendData:data];
                ret = YES;
            }
                break;
        }
       
         return [randomAccessFile writeToFile:savePath atomically:YES] ? savePath : nil;;
    } @catch (NSException *exception) {
        return nil;
    }
}

// get 32 bytes md5 hash string
+ (NSString *)getStringMD5:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [self getDataMD5:data];
}

+ (NSString *)getDataMD5:(NSData *)data {
    uint8_t md5[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(data.bytes, (CC_LONG) data.length, md5);
    
    NSMutableString *hashString = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [hashString appendFormat:@"%02x", md5[i]];
    }
    return hashString;
}

+ (BOOL)loadAttachment:(FSFileAttachment *)annot toPath:(NSString *)attachmentPath {
    return [Utility loadFileSpec:[annot getFileSpec] toPath:attachmentPath];
}

+ (BOOL)loadFileSpec:(FSFileSpec *)fileSpec toPath:(NSString *)path {
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    if ([defaultManager fileExistsAtPath:path]) {
        return YES;
    }
    if (!fileSpec) {
        return NO;
    }
    if (![defaultManager createDirectoryAtPath:[path stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil]) {
        return NO;
    }
    id<FSFileReaderCallback> fileRead = [fileSpec getFileData];
    unsigned long long fileSize = [fileRead getSize];
    @autoreleasepool {
        NSMutableData *data = [NSMutableData data];
        unsigned long long offset = 0;
        unsigned long long bufferSize = 2048000;
        while (1) {
            @autoreleasepool {
                NSData *dataBlock = [fileRead readBlock:offset size:MIN(bufferSize, fileSize - offset)];
                if (dataBlock.length > 0) {
                    offset += dataBlock.length;
                    [data appendData:dataBlock];
                } else {
                    break;
                }
            }
        }
        return [data writeToFile:path atomically:YES];
    } // autorelease pool
}

//+ (FSBitmap *)uiimageToFSBitmap:(UIImage *)image {
//    NSData *data = UIImagePNGRepresentation(image);
//    if (data) {
//        FSBitmap *bitmap = [[FSBitmap alloc] initWithWidth:image.size.width height:image.size.height format:FSBitmapDIBArgb buffer:data.copy pitch:image.size.width * 4];
//        return bitmap;
//    }
//    return nil;
//}

+ (UIImage *)bitmapToUIImage:(FSBitmap *)bitmap {
    //    FSBitmapDIBFormat format = [bitmap getFormat];
    //    [bitmap convertFormat:FSBitmapDIBArgb icc_transform:nil];
    NSMutableData *data = [[bitmap getBuffer] mutableCopy];
    void *bytes = data.mutableBytes;
    if (bytes == NULL) {
        return nil;
    } else {
        return [Utility dib2img:(void *_Nonnull) bytes size:(int) data.length dibWidth:[bitmap getWidth] dibHeight:[bitmap getHeight] withAlpha:YES];
    }
}

+ (FSBitmap *)uiimageToFSBitmap:(UIImage *)image {
    CGImageRef cgImage = [image CGImage];
    int width = (int) CGImageGetWidth(cgImage);
    int height = (int) CGImageGetHeight(cgImage);
    int bytesPerPixel = 4;
    int bytesPerRow = bytesPerPixel * width;

    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    NSUInteger bitsPerComponent = 8;

    //    FSBitmap *bitmap = [[FSBitmap alloc] initWithWidth:width height:height format:FSBitmapDIBArgb];

    size_t sz = bytesPerRow * height;
    void *buf = malloc(sz);
    if (!buf) {
        return nil;
    }
    memset(buf, 0x0, sz);
    unsigned char *rawData = (unsigned char *) buf; //([bitmap getBuffer].bytes);
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), cgImage);
    CGContextRelease(context);

    // Now your rawData contains the image data in the RGBA8888 pixel format.
    // swap red and blue channel
    for (int row = 0; row < height; row++) {
        unsigned char *p = rawData + bytesPerRow * row;
        for (int col = 0; col < width; col++) {
            unsigned char tmp = p[0];
            p[0] = p[2];
            p[2] = tmp;
            p += bytesPerPixel;
        }
    }
    return [[FSBitmap alloc] initWithWidth:width height:height format:FSBitmapDIBArgb buffer:[[NSData alloc] initWithBytesNoCopy:buf length:sz freeWhenDone:NO] pitch:bytesPerRow];
    //
    //    return bitmap;
}

+ (FSBitmap *)imgDataToBitmap:(NSData *)imgData {
#if true
    UIImage *image = [UIImage imageWithData:imgData];
    if (image) {
        return [Utility uiimageToFSBitmap:image];
    } else {
        return nil;
    }
#else
    UIImage *image = [UIImage imageWithData:imgData];
    CGImageRef cgImg = [image CGImage];
    CGDataProviderRef provider = CGImageGetDataProvider(cgImg);
    CFDataRef data = CGDataProviderCopyData(provider);
    int width = (int) CGImageGetWidth(cgImg);
    int height = (int) CGImageGetHeight(cgImg);
    size_t bPP = CGImageGetBitsPerPixel(cgImg);
    if (bPP != 32) {
        return nil;
    }
    //Reverse bgr format to rgb
    const unsigned char *buffer = CFDataGetBytePtr(data);
    unsigned char *buf = (unsigned char *) buffer;
    int Bpp = (int) bPP / 8;
    int stride32 = width * Bpp;
    dispatch_apply(height, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(size_t ri) {
        dispatch_apply(width, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(size_t j) {
            long i = height - 1 - ri;
            unsigned char tmp = buf[i * stride32 + j * Bpp];
            buf[i * stride32 + j * Bpp] = buf[i * stride32 + j * Bpp + 2];
            buf[i * stride32 + j * Bpp + 2] = tmp;
        });
    });

    NSUInteger size = CFDataGetLength(data);
    FSBitmap *bitmap = [[FSBitmap alloc] initWithWidth:width height:height format:FSBitmapDIBArgb buffer:[NSData dataWithBytesNoCopy:(unsigned char *) buf length:size freeWhenDone:NO] pitch:0];
    return bitmap;
#endif
}

//+ (NSDictionary<NSString *, FSPDFObject *> *)getNSDictionaryFromPDFDictionary:(FSPDFDictionary *)pdfDict {
//    NSMutableDictionary<NSString *, FSPDFObject *> *dict = [NSMutableDictionary<NSString *, FSPDFObject *> dictionary];
//    void *pos = nil;
//    while (1) {
//        pos = [pdfDict moveNext:pos];
//        NSString *key = [pdfDict getKey:pos];
//        if (key.length == 0) {
//            break;
//        }
//        FSPDFObject *obj = [pdfDict getValue:pos];
//        if (!obj) {
//            continue;
//        }
//        [dict setObject:obj forKey:key];
//    }
//    return dict;
//}

#pragma mark icon string name to int

static NSDictionary<NSString *, NSNumber *> *g_noteIconNameToType = nil;
static NSDictionary<NSString *, NSNumber *> *g_attachmentIconNameToType = nil;
static NSDictionary<NSString *, NSNumber *> *g_stampIconNameToType = nil;

/**
 * @name    Macro Definitions for Annotation Icon Name
 */
/**@{*/

/** @brief    Note icon name: Check. */
#define FS_ANNOT_ICONNAME_TEXT_CHECK "Check"
/** @brief    Note icon name: Circle. */
#define FS_ANNOT_ICONNAME_TEXT_CIRCLE "Circle"
/** @brief    Note icon name: Comment. */
#define FS_ANNOT_ICONNAME_TEXT_COMMENT "Comment"
/** @brief    Note icon name: Cross. */
#define FS_ANNOT_ICONNAME_TEXT_CROSS "Cross"
/** @brief    Note icon name: Help. */
#define FS_ANNOT_ICONNAME_TEXT_HELP "Help"
/** @brief    Note icon name: Insert. */
#define FS_ANNOT_ICONNAME_TEXT_INSERT "Insert"
/** @brief    Note icon name: Key. */
#define FS_ANNOT_ICONNAME_TEXT_KEY "Key"
/** @brief    Note icon name: New Paragraph. */
#define FS_ANNOT_ICONNAME_TEXT_NEWPARAGRAPH "NewParagraph"
/** @brief    Note icon name: Note. */
#define FS_ANNOT_ICONNAME_TEXT_NOTE "Note"
/** @brief    Note icon name: Paragraph. */
#define FS_ANNOT_ICONNAME_TEXT_PARAGRAPH "Paragraph"
/** @brief    Note icon name: Right Arrow. */
#define FS_ANNOT_ICONNAME_TEXT_RIGHTARROW "RightArrow"
/** @brief    Note icon name: Right Pointer. */
#define FS_ANNOT_ICONNAME_TEXT_RIGHTPOINTER "RightPointer"
/** @brief    Note icon name: Star. */
#define FS_ANNOT_ICONNAME_TEXT_STAR "Star"
/** @brief    Note icon name: Up Arrow. */
#define FS_ANNOT_ICONNAME_TEXT_UPARROW "UpArrow"
/** @brief    Note icon name: Up-left Arrow. */
#define FS_ANNOT_ICONNAME_TEXT_UPLEFTARROW "UpLeftArrow"

/** @brief    File attachment icon type: Graph. */
#define FS_ANNOT_ICONNAME_FILEATTACH_GRAPH "Graph"
/** @brief    File attachment icon type: PaperClip. */
#define FS_ANNOT_ICONNAME_FILEATTACH_PAPERCLIP "Paperclip"
/** @brief    File attachment icon type: PushPin. */
#define FS_ANNOT_ICONNAME_FILEATTACH_PUSHPIN "PushPin"
/** @brief    File attachment icon type: Tag. */
#define FS_ANNOT_ICONNAME_FILEATTACH_TAG "Tag"

#define FS_ANNOT_ICONNAME_SIGNATURE_FOXIEFILAG "FoxitFlag"

/**@}*/

+ (void)setupIconNameAndTypes {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // note
        g_noteIconNameToType = @{
            @FS_ANNOT_ICONNAME_TEXT_CHECK : @FPDF_ICONTYPE_NOTE_CHECK,
            @FS_ANNOT_ICONNAME_TEXT_CIRCLE : @FPDF_ICONTYPE_NOTE_CIRCLE,
            @FS_ANNOT_ICONNAME_TEXT_COMMENT : @FPDF_ICONTYPE_NOTE_COMMENT,
            @FS_ANNOT_ICONNAME_TEXT_CROSS : @FPDF_ICONTYPE_NOTE_CROSS,
            @FS_ANNOT_ICONNAME_TEXT_HELP : @FPDF_ICONTYPE_NOTE_HELP,
            @FS_ANNOT_ICONNAME_TEXT_INSERT : @FPDF_ICONTYPE_NOTE_INSERT,
            @FS_ANNOT_ICONNAME_TEXT_KEY : @FPDF_ICONTYPE_NOTE_KEY,
            @FS_ANNOT_ICONNAME_TEXT_NEWPARAGRAPH : @FPDF_ICONTYPE_NOTE_NEWPARAGRAPH,
            @FS_ANNOT_ICONNAME_TEXT_NOTE : @FPDF_ICONTYPE_NOTE_NOTE,
            @FS_ANNOT_ICONNAME_TEXT_PARAGRAPH : @FPDF_ICONTYPE_NOTE_PARAGRAPH,
            @FS_ANNOT_ICONNAME_TEXT_RIGHTARROW : @FPDF_ICONTYPE_NOTE_RIGHTARROW,
            @FS_ANNOT_ICONNAME_TEXT_RIGHTPOINTER : @FPDF_ICONTYPE_NOTE_RIGHTPOINTER,
            @FS_ANNOT_ICONNAME_TEXT_STAR : @FPDF_ICONTYPE_NOTE_STAR,
            @FS_ANNOT_ICONNAME_TEXT_UPARROW : @FPDF_ICONTYPE_NOTE_UPARROW,
            @FS_ANNOT_ICONNAME_TEXT_UPLEFTARROW : @FPDF_ICONTYPE_NOTE_UPLEFTARROW,
        };
        // attachment
        g_attachmentIconNameToType = @{
            @FS_ANNOT_ICONNAME_FILEATTACH_GRAPH : @FPDF_ICONTYPE_FILEATTACH_GRAPH,
            @FS_ANNOT_ICONNAME_FILEATTACH_PAPERCLIP : @FPDF_ICONTYPE_FILEATTACH_PAPERCLIP,
            @FS_ANNOT_ICONNAME_FILEATTACH_PUSHPIN : @FPDF_ICONTYPE_FILEATTACH_PUSHPIN,
            @FS_ANNOT_ICONNAME_FILEATTACH_TAG : @FPDF_ICONTYPE_FILEATTACH_TAG,
        };
        // stamp
        g_stampIconNameToType = @{
            @"Approved" : @0,
            @"Completed" : @1,
            @"Confidential" : @2,
            @"Draft" : @3,
            @"Emergency" : @4,
            @"Expired" : @5,
            @"Final" : @6,
            @"Received" : @7,
            @"Reviewed" : @8,
            @"Revised" : @9,
            @"Verified" : @10,
            @"Void" : @11,
            @"Accepted" : @12,
            @"Initial" : @13,
            @"Rejected" : @14,
            @"Sign Here" : @15,
            @"Witness" : @16,
            @"DynaApproved" : @17,
            @"DynaConfidential" : @18,
            @"DynaReceived" : @19,
            @"DynaReviewed" : @20,
            @"DynaRevised" : @21,
        };
    });
}

+ (int)getIconTypeWithIconName:(NSString *)iconName annotType:(FSAnnotType)annotType {
    [Utility setupIconNameAndTypes];
    NSDictionary<NSString *, NSNumber *> *nameToType = nil;
    switch (annotType) {
    case FSAnnotNote:
        nameToType = g_noteIconNameToType;
        break;
    case FSAnnotFileAttachment:
        nameToType = g_attachmentIconNameToType;
        break;
    case FSAnnotStamp:
        nameToType = g_stampIconNameToType;
        break;
    default:
        break;
    }
    __block int iconType = FPDF_ICONTYPE_UNKNOWN;
    [nameToType enumerateKeysAndObjectsUsingBlock:^(NSString *_iconName, NSNumber *_iconType, BOOL *stop) {
        if ([_iconName caseInsensitiveCompare:iconName] == NSOrderedSame) {
            iconType = [_iconType intValue];
            *stop = YES;
        }
    }];
    return iconType;
}

+ (NSString *)getIconNameWithIconType:(int)iconType annotType:(FSAnnotType)annotType {
    [Utility setupIconNameAndTypes];
    NSDictionary<NSString *, NSNumber *> *nameToType = nil;
    switch (annotType) {
    case FSAnnotNote:
        nameToType = g_noteIconNameToType;
        break;
    case FSAnnotFileAttachment:
        nameToType = g_attachmentIconNameToType;
        break;
    case FSAnnotStamp:
        nameToType = g_stampIconNameToType;
        break;
    default:
        break;
    }

    __block NSString *iconName = nil;
    [nameToType enumerateKeysAndObjectsUsingBlock:^(NSString *_Nonnull _iconName, NSNumber *_Nonnull _iconType, BOOL *_Nonnull stop) {
        if ([_iconType intValue] == iconType) {
            iconName = _iconName;
            *stop = YES;
        }
    }];
    return iconName;
}

+ (BOOL)isValidIconName:(NSString *)iconName annotType:(FSAnnotType)annotType {
    return [Utility getIconTypeWithIconName:iconName annotType:annotType] != FPDF_ICONTYPE_UNKNOWN;
}

+ (NSArray<NSString *> *)getAllIconLowercaseNames {
    [Utility setupIconNameAndTypes];
    NSMutableArray<NSString *> *iconNames = [NSMutableArray<NSString *> array];
    for (NSDictionary *dict in @[ g_noteIconNameToType, g_stampIconNameToType, g_attachmentIconNameToType ]) {
        for (NSString *key in dict.allKeys) {
            [iconNames addObject:key.lowercaseString];
        }
    }
    return iconNames;
}


+ (BOOL)isSignatureWithAnnot:(FSAnnot *)annot{
    @try {
        if (annot && ![annot isEmpty] && annot.type == FSAnnotWidget) {
            FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
            if ([widget isEmpty]) return NO;
            FSControl *control = [widget  getControl];
            if (control && ![control isEmpty]) {
                FSField *field = [control getField];
                FSFieldType type = [field getType];
                if (type == FSFieldTypeSignature) {
                    return YES;
                }
            }
        }
        return NO;
    } @catch (NSException *exception) {
        return NO;
    }
}

+ (BOOL)simpleCheckPDFA:(FSPDFDoc *)document{
    if (document.isEmpty) {
        return false;
    }
    @try {
        FSPDFDictionary *rootDict = document.getCatalog;
        if (!rootDict) {
            return false;
        }
        FSPDFObject *metaDataObj = [rootDict getElement:@"Metadata"];
        if (!metaDataObj) {
            return false;
        }

        FSPDFStream *metaDataStream = metaDataObj.getStream;
        if (!metaDataStream) {
            return false;
        }
        NSData *data = [metaDataStream getData:false];
        NSString *strStream = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] stringByTrim];
        return [strStream rangeOfString:@"pdfaid:conformance"].location != NSNotFound;
    } @catch (NSException *e) {
        NSLog(@"%@",e);
    }
    return false;
}

+ (BOOL)hasSignatureInDocument:(FSPDFDoc *)document {
    FSPDFDictionary* root = [document getCatalog];
    if(!root) return NO;
    FSPDFObject* acroForm = [root getElement:@"AcroForm"];
    if(!acroForm) return NO;
    if ([acroForm getType] == FSPDFObjectReference)
        acroForm = [acroForm getDirectObject];
    if (!acroForm || [acroForm getType] != FSPDFObjectDictionary)
        return NO;
    FSPDFDictionary *dict = [acroForm getDict];
    FSPDFObject *flag = [dict getElement:@"SigFlags"];
    int sigFlags = flag ? [flag getInteger] : 0;
    if(sigFlags & 0x01)
        return YES;
    return NO;
}

+ (BOOL)isOwnerOfDoucment:(FSPDFDoc *)document {
    int mdpPermission = [Utility getMDPDigitalSignPermissionInDocument:document];
    if (mdpPermission != 0) {
        return NO;
    }
    FSPDFDocPasswordType passwordType = [document getPasswordType];
    return (passwordType == FSPDFDocPwdNoPassword || passwordType == FSPDFDocPwdOwner);
}

+ (BOOL)isDocumentSigned:(FSPDFDoc *)document {
    if(![Utility hasSignatureInDocument:document]) return NO;
    int count = [document getSignatureCount];
    for (int i = 0; i < count; i++) {
        if ([[document getSignature:i] isSigned])
            return YES;
    }
    return NO;
}

+ (BOOL)canAddAnnot:(FSPDFViewCtrl *)viewctrl {
    return ([viewctrl getDocPermissions] & FSPDFDocPermAnnotForm) > 0;
}

+ (BOOL)canAddAnnotToDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;
    int mdpPermission = [Utility getMDPDigitalSignPermissionInDocument:document];
    if (mdpPermission == 1 || mdpPermission == 2) {
        return NO;
    }
    return ([document getUserPermissions] & FSPDFDocPermAnnotForm) > 0;
}

+ (BOOL)canCopyText:(FSPDFViewCtrl *)viewctrl {
    return ([viewctrl getDocPermissions] & FSPDFDocPermExtract) > 0;
}

+ (BOOL)canCopyTextInDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;
    return ([document getUserPermissions] & FSPDFDocPermExtract) > 0;
}

+ (BOOL)canFillForm:(FSPDFViewCtrl *)viewctrl {
    return ([viewctrl getDocPermissions] & FSPDFDocPermFillForm) > 0;
}

+ (BOOL)canFillFormInDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;
    int mdpPermission = [Utility getMDPDigitalSignPermissionInDocument:document];
    if (mdpPermission == 1) {
        return NO;
    }
    return ([document getUserPermissions] & FSPDFDocPermFillForm) > 0;
}

+ (BOOL)canAddSign:(FSPDFViewCtrl *)viewctrl {
    return [self canAddSignToDocument:[viewctrl currentDoc]];
}

+ (BOOL)canAddSignToDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;
    int mdpPermission = [Utility getMDPDigitalSignPermissionInDocument:document];
    if (mdpPermission != 0) {
        return NO;
    }
    // FSPDFDocPermFillForm means you may sign the existing signature, but not create one.
    unsigned int perm = [document getUserPermissions];
    return ((perm & FSPDFDocPermAnnotForm) != 0u && (perm & FSPDFDocPermModify) != 0u);
}

+ (BOOL)canAssemble:(FSPDFViewCtrl *)viewctrl {
    if ([Utility isDocumentSigned:viewctrl.currentDoc])
        return NO;
    return (([viewctrl getDocPermissions] & FSPDFDocPermAssemble) != 0u);
}

+ (BOOL)canAssembleDocument:(FSPDFDoc *)document {
    if ([Utility isDocumentSigned:document])
        return NO;
    unsigned int perm = [document getUserPermissions];
    return [Utility isOwnerOfDoucment:document] || ((perm & FSPDFDocPermAssemble) != 0u);
}

+ (BOOL)canCopyForAssess:(FSPDFViewCtrl *)viewctrl {
    FSPDFDocUserPermissions perm = [viewctrl getDocPermissions];
    return (perm & FSPDFDocPermExtractAccess) != 0u || (perm & FSPDFDocPermExtract) != 0u;
}

+ (BOOL)canCopyForAssessInDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;

    unsigned long allPermission = [document getUserPermissions];
    return (allPermission & FSPDFDocPermExtractAccess) != 0u || (allPermission & FSPDFDocPermExtract) != 0u;
}

+ (BOOL)canModifyContents:(FSPDFViewCtrl *)viewctrl {
    if ([Utility isDocumentSigned:viewctrl.currentDoc])
        return NO;
    return ([viewctrl getDocPermissions] & FSPDFDocPermModify) != 0u;
}

+ (BOOL)canModifyContentsInDocument:(FSPDFDoc *)document {
    if ([Utility isDocumentSigned:document])
        return NO;

    if ([Utility isOwnerOfDoucment:document])
        return YES;

    unsigned long allPermission = [document getUserPermissions];
    return (allPermission & FSPDFDocPermModify) != 0u;
}

+ (BOOL)canExtractContents:(FSPDFViewCtrl *)viewctrl {
    return ([viewctrl getDocPermissions] & FSPDFDocPermExtract) != 0u;
}

+ (BOOL)canExtractContentsInDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;
    unsigned long allPermission = [document getUserPermissions];
    return (allPermission & FSPDFDocPermExtract) != 0u;
}

+ (BOOL)canPrint:(FSPDFViewCtrl *)viewctrl {
    return ([viewctrl getDocPermissions] & FSPDFDocPermPrint) != 0u;
}

+ (BOOL)canPrintDocument:(FSPDFDoc *)document {
    if ([Utility isOwnerOfDoucment:document])
        return YES;
    unsigned long allPermission = [document getUserPermissions];
    return (allPermission & FSPDFDocPermPrint) != 0u;
}

+ (BOOL)canApplyRedaction:(FSPDFDoc *)document{
    return [self canAddAnnotToDocument:document] && [self canAssembleDocument:document] && !document.isSigned;
}

+ (int)getMDPDigitalSignPermissionInDocument:(FSPDFDoc *)document {
    FSPDFDictionary *catalog = [document getCatalog];
    FSPDFDictionary *perms = [[[catalog getElement:@"Perms"] getDirectObject] getDict];
    if (!perms || ![perms isKindOfClass:[FSPDFDictionary class]]) {
        return 0;
    }
    FSPDFDictionary *docMDP = [[[perms getElement:@"DocMDP"] getDirectObject] getDict];
    if (!docMDP || ![docMDP isKindOfClass:[FSPDFDictionary class]]) {
        return 0;
    }
    FSPDFArray *reference = [[[docMDP getElement:@"Reference"] getDirectObject] getArray];
    if (!reference || ![reference isKindOfClass:[FSPDFArray class]]) {
        return 0;
    }
    for (int i = 0; i < reference.getElementCount; i++) {
        FSPDFDictionary *tmpDict = [[[reference getElement:i] getDirectObject] getDict];
        if (!tmpDict || ![tmpDict isKindOfClass:[FSPDFDictionary class]]) {
            return 0;
        }
        NSString *transformMethod = [[[tmpDict getElement:@"TransformMethod"] getDirectObject] getWideString];
        if (![transformMethod isEqualToString:@"DocMDP"]) {
            continue;
        }
        FSPDFDictionary *transformParams = [[[tmpDict getElement:@"TransformParams"] getDirectObject] getDict];
        if (!transformParams || ![transformParams isKindOfClass:[FSPDFDictionary class]] || [transformParams getCptr] == [tmpDict getCptr]) {
            return 0;
        }
        int permisson = [[[transformParams getElement:@"P"] getDirectObject] getInteger];
        return permisson;
    }
    return 0;
}

+ (void)assignImage:(UIImageView *)imageView rawFrame:(CGRect)frame image:(UIImage *)image {
    if (image.size.width / image.size.height == frame.size.width / frame.size.height) {
        imageView.frame = [Utility getStandardRect:frame];
    } else if (image.size.width / image.size.height < frame.size.width / frame.size.height) {
        float realHeight = frame.size.height;
        float realWidth = image.size.width / image.size.height * realHeight;
        imageView.frame = [Utility getStandardRect:CGRectMake(frame.origin.x + (frame.size.width - realWidth) / 2, frame.origin.y, realWidth, realHeight)];
    } else {
        float realWidth = frame.size.width;
        float realHeight = image.size.height / image.size.width * realWidth;
        imageView.frame = [Utility getStandardRect:CGRectMake(frame.origin.x, frame.origin.y + (frame.size.height - realHeight) / 2, realWidth, realHeight)];
    }
    imageView.image = image;
}

+ (NSArray *)searchFilesWithFolder:(NSString *)folder recursive:(BOOL)recursive {
    NSMutableArray *fileList = [NSMutableArray array];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *fileAndFolderList = [fileManager contentsOfDirectoryAtPath:folder error:nil];
    for (NSString *file in fileAndFolderList) {
        if ([file.lowercaseString isEqualToString:@".DS_Store".lowercaseString]) {
            continue;
        }
        NSString *thisFile = [folder stringByAppendingPathComponent:file];
        BOOL isDir = NO;
        if ([fileManager fileExistsAtPath:thisFile isDirectory:&isDir] && isDir) {
            if (recursive) {
                [fileList addObjectsFromArray:[[self class] searchFilesWithFolder:thisFile recursive:recursive]];
            }
        } else {
            [fileList addObject:thisFile];
        }
    }
    return (NSArray *) fileList;
}

+ (FSRectF*)getPageBoundary:(FSPDFPage*)page
{
    FSRectF *pageBox = [page getBox:FSPDFPageCropBox];
    if (!pageBox || [pageBox isEmpty])
        pageBox = [page getBox:FSPDFPageMediaBox];
    if([pageBox isEmpty])
    {
        pageBox = [[FSRectF alloc] init];
        [pageBox setLeft:0];
        [pageBox setBottom:0];
        [pageBox setRight:612];
        [pageBox setTop:792];
    }
    return pageBox;
}

+ (BOOL)parsePage:(FSPDFPage *)page flag:(unsigned int)flag pause:(id<FSPauseCallback> _Nullable)pause {
    FSProgressive *progress = [page startParse:flag pause:pause is_reparse:NO];
    if (!progress) {
        return YES;
    }
    while (YES) {
        int rate = [progress getRateOfProgress];
        if (rate < 0) {
            return NO;
        } else if (rate == 100) {
            return YES;
        }
        if ([pause needPauseNow]) {
            return NO;
        }
        [progress resume];
    }
}

+ (BOOL)parsePage:(FSPDFPage *)page {
    return [Utility parsePage:page flag:FSPDFPageParsePageNormal pause:nil];
}

+ (UIImage *)drawPage:(FSPDFPage *)page dibWidth:(int)dibWidth dibHeight:(int)dibHeight shouldDrawAnnotation:(BOOL)shouldDrawAnnotation progressive:(ProgressiveBlock)progressive needPause:(BOOL (^__nullable)(void))needPause rotation:(FSRotation)rotation enableForPrint:(BOOL)enableForPrint{
    UIImage *img = nil;

    @autoreleasepool {
        CGFloat scale = [UIScreen mainScreen].scale;
#ifdef CONTEXT_DRAW
        scale = 1;
#endif
        int newDibWidth = dibWidth * scale;
        int newDibHeight = dibHeight * scale;
        int newPdfX = 0;
        int newPdfY = 0;
        int newPdfWidth = dibWidth * scale;
        int newPdfHeight = dibHeight * scale;
        
        {
            if (!page) {
                return img;
            }
            
#ifdef CONTEXT_DRAW
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDibWidth, newDibHeight), YES, [[UIScreen mainScreen] scale]);
            CGContextRef context = UIGraphicsGetCurrentContext();
#else
            //create a 24bit bitmap
            int size = newDibWidth * newDibHeight * 3;
            void *pBuf = malloc(size);
            FSBitmap *fsbitmap = [[FSBitmap alloc] initWithWidth:newDibWidth height:newDibHeight format:FSBitmapDIBRgb buffer:[NSData dataWithBytesNoCopy:(unsigned char *) pBuf length:size freeWhenDone:NO] pitch:newDibWidth * 3];
#endif
            
            //render page, must have
#ifdef CONTEXT_DRAW
            FSRenderer *fsrenderer = [[FSRenderer alloc] initWithContext:context device_type:FSRendererDeviceDisplay];
#else
            FSRenderer *fsrenderer = [[FSRenderer alloc] initWithBitmap:fsbitmap is_rgb_order:YES];
#endif

            FSMatrix2D *fsmatrix = [page getDisplayMatrix:newPdfX top:newPdfY width:newPdfWidth height:newPdfHeight rotate:rotation];
            //        if (isNightMode) {
            //#ifndef CONTEXT_DRAW
            //            //set background color of bitmap to black
            //            memset(pBuf, 0x00, size);
            //#endif
            //            [fsrenderer setColorMode:FSRendererColorModeMapping];
            //            [fsrenderer setMappingModeColors:pdfViewCtrl.mappingModeBackgroundColor.rgbaValue foreground_color:pdfViewCtrl.mappingModeForegroundColor.rgbaValue];
            //        } else
            {
#ifdef CONTEXT_DRAW
                CGContextSetRGBFillColor(context, 1, 1, 1, 1);
                CGContextFillRect(context, CGRectMake(0, 0, newDibWidth, newDibHeight));
#else
                //set background color of bitmap to white
                memset(pBuf, 0xff, size);
#endif
            }
            
            void (^releaseRender)(BOOL freepBuf) = ^(BOOL freepBuf) {
#ifdef CONTEXT_DRAW
                UIGraphicsEndImageContext();
#else
                if (freepBuf) {
                    free(pBuf);
                }
#endif
            };
            int contextFlag = shouldDrawAnnotation ? (FSRendererRenderAnnot | FSRendererRenderPage) : FSRendererRenderPage;
            [fsrenderer setRenderContentFlags:contextFlag];
            [fsrenderer enableForPrint:enableForPrint];
            
            FSPause *pause = [FSPause pauseWithBlock:needPause];
            
            [pause setUpdateData:^{
                if (progressive) {
                    progressive([Utility rgbDib2img:pBuf size:size dibWidth:newDibWidth dibHeight:newDibHeight withAlpha:NO freeWhenDone:NO], NO);
                }
            }];
            FSProgressive *ret = [fsrenderer startRender:page matrix:fsmatrix pause:pause];
            if (ret != nil && ![pause needPauseNow]) {
                FSProgressiveState state;
                while (true) {
                    state = [ret resume];
                    if (state != FSProgressiveToBeContinued) {
                        break;
                    }
                }
                if (FSProgressiveFinished != state) {
                    releaseRender(YES);
                    return nil;
                }
            }
            
#ifdef CONTEXT_DRAW
            img = UIGraphicsGetImageFromCurrentImageContext();
#else
            img = [Utility rgbDib2img:pBuf size:size dibWidth:newDibWidth dibHeight:newDibHeight withAlpha:NO freeWhenDone:YES];
#endif
            releaseRender(img == nil);
        }
    }
    if (progressive) {
        progressive(img, YES);
    }
    return img;
}

+ (UIImage *)drawPage:(FSPDFPage *)page targetSize:(CGSize)targetSize shouldDrawAnnotation:(BOOL)shouldDrawAnnotation needPause:(BOOL (^__nullable)(void))needPause rotation:(FSRotation)rotation enableForPrint:(BOOL)enableForPrint{
    return [Utility drawPage:page targetSize:targetSize shouldDrawAnnotation:shouldDrawAnnotation progressive:nil needPause:needPause rotation:rotation enableForPrint:enableForPrint];
}

+ (UIImage *)drawPage:(FSPDFPage *)page targetSize:(CGSize)targetSize shouldDrawAnnotation:(BOOL)shouldDrawAnnotation progressive:(void (^__nullable)(UIImage * image, BOOL))progressive needPause:(BOOL (^ __nullable)(void))needPause rotation:(FSRotation)rotation enableForPrint:(BOOL)enableForPrint{
    BOOL isOK = [Utility parsePage:page];
    if (!isOK || ![page isParsed]) {
        return nil;
    }
    return [Utility drawPage:page dibWidth:targetSize.width dibHeight:targetSize.height shouldDrawAnnotation:shouldDrawAnnotation progressive:progressive needPause:needPause rotation:rotation enableForPrint:enableForPrint];
#if 0
    UIGraphicsBeginImageContextWithOptions(targetSize, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    FSRenderer* fsrenderer = [[FSRenderer alloc] initWithContext:context device_type:FSRendererDeviceDisplay];
    if (isNightMode) {
        [fsrenderer setColorMode:FSRendererColorModeMapping];
        [fsrenderer setMappingModeColors:pdfViewCtrl.mappingModeBackgroundColor.rgbaValue foreground_color:pdfViewCtrl.mappingModeForegroundColor.rgbaValue];
    } else {
        CGContextSetRGBFillColor(context, 1, 1, 1, 1);
        CGContextFillRect(context, CGRectMake(0, 0, targetSize.width, targetSize.height));
    }
    [fsrenderer setTransformAnnotIcon:NO];
    [fsrenderer setRenderContentFlags:shouldDrawAnnotation ? (FSRendererRenderAnnot|FSRendererRenderPage) : FSRendererRenderPage];
    UIImage* image = nil;
    @try {
        FSMatrix2D* fsmatrix = [page getDisplayMatrix:0 top:0 width:ceilf(targetSize.width) height:ceilf(targetSize.height) rotate:FSRotation0];
        FSProgressiveState state = [fsrenderer startRender:page matrix:fsmatrix pause:nil];
        while (FSProgressiveToBeContinued == state) {
            state = [fsrenderer continueRender];
        }
        if (FSProgressiveFinished == state) {
            image = UIGraphicsGetImageFromCurrentImageContext();
        }
    } @catch (NSException *exception) {
        UIGraphicsEndImageContext();
        return nil;
    }
    
    UIGraphicsEndImageContext();
    return image;
#endif
}

+ (void)printPage:(FSPDFPage *)page inContext:(CGContextRef)context inRect:(CGRect)rect shouldDrawAnnotation:(BOOL)shouldDrawAnnotation {
#if 0
    BOOL parseSuccess = [Utility parsePage:page flag:FSPDFPageParsePageNormal pause:nil];
    if (!parseSuccess) {
        return;
    }
    FSRenderer *fsrenderer = [[FSRenderer alloc] initWithContext:context device_type:FSRendererDevicePrinter];
    FSMatrix2D *fsmatrix = [page getDisplayMatrix:rect.origin.x top:rect.origin.y width:rect.size.width height:rect.size.height rotate:FSRotation0];
    [fsrenderer setRenderContentFlags:(shouldDrawAnnotation ? (FSRendererRenderPage | FSRendererRenderAnnot) : FSRendererRenderPage)];
    FSProgressive *progressive = [fsrenderer startRender:page matrix:fsmatrix pause:nil];
    if (progressive != nil) {
        while (true) {
            if ([progressive resume] != FSProgressiveToBeContinued) {
                break;
            }
        }
    }
#else
    UIImage *image = [Utility drawPage:page targetSize:rect.size shouldDrawAnnotation:YES needPause:nil rotation:FSRotation0 enableForPrint:YES];
    if (image) {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, 2 * rect.origin.y + rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawImage(context, rect, image.CGImage);
        CGContextRestoreGState(context);
    }
#endif
}

+ (void)tryLoadDocument:(FSPDFDoc *)document withPassword:(NSString *)password success:(void (^)(NSString *password))success error:(void (^_Nullable)(NSString *description))error abort:(void (^_Nullable)(void))abort {
    FSErrorCode errorCode = [document load:password];
    switch (errorCode) {
    case FSErrSuccess: {
        if (success) {
            success(password);
        }
    } break;
    case FSErrPassword: {
        NSString *title = password.length > 0 ? @"kDocPasswordError" : @"kDocNeedPassword";
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(title) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *_Nonnull textField) {
            textField.text = @"";
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 if (abort) {
                                                                     abort();
                                                                 }
                                                             }];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           UITextField *textField = alertController.textFields[0];
                                                           NSString *guessPassword = textField.text;
                                                           [Utility tryLoadDocument:document withPassword:guessPassword success:success error:error abort:abort];
                                                       }];
        [alertController addAction:cancelAction];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
    } break;
    default: {
        if (error) {
            error([Utility getErrorCodeDescription:errorCode]);
        }
    } break;
    }
}

//File/Folder existance
+ (BOOL)isFileOrFolderExistAtPath:(NSString *)path fileOrFolderName:(NSString *)fileOrFolderName {
    BOOL isAlreadyFileOrFolderExist = NO;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *subFolder = [fileManager contentsOfDirectoryAtPath:path error:nil];
    for (NSString *thisFolder in subFolder) {
        if ([thisFolder caseInsensitiveCompare:fileOrFolderName] == NSOrderedSame) {
            isAlreadyFileOrFolderExist = YES;
            break;
        }
    }
    return isAlreadyFileOrFolderExist;
}

+ (BOOL)showAnnotationContinue:(BOOL)isContinue pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl siblingSubview:(UIView *)siblingSubview {
    [self dismissAnnotationContinue:pdfViewCtrl];
    NSString *textString = nil;
    if (isContinue) {
        textString = FSLocalizedForKey(@"kAnnotContinue");
    } else {
        textString = FSLocalizedForKey(@"kAnnotSingle");
    }

    CGSize titleSize = [Utility getTextSize:textString fontSize:15.0f maxSize:CGSizeMake(300, 100)];

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(pdfViewCtrl.bounds) / 2, CGRectGetHeight(pdfViewCtrl.bounds) - 120, titleSize.width + 10, 30)];
    view.center = CGPointMake(CGRectGetWidth(pdfViewCtrl.bounds) / 2, CGRectGetHeight(pdfViewCtrl.bounds) - 105);
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.8;
    view.layer.cornerRadius = 10.0f;
    view.tag = 2112;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleSize.width, 30)];
    label.center = CGPointMake(view.frame.size.width / 2, view.frame.size.height / 2);
    label.backgroundColor = [UIColor clearColor];
    label.text = textString;

    label.textColor = WhiteThemeTextColor;
    label.font = [UIFont systemFontOfSize:15];
    [view addSubview:label];
    [pdfViewCtrl insertSubview:view belowSubview:siblingSubview];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view.superview.mas_centerX).offset(0);
        make.top.equalTo(view.superview.mas_bottom).offset(-120);
        make.width.mas_equalTo(titleSize.width + 10);
        make.height.mas_equalTo(@30);
    }];
    return YES;
}

+ (void)dismissAnnotationContinue:(UIView *)superView {
    for (UIView *view in superView.subviews) {
        if (view.tag == 2112) {
            [view removeFromSuperview];
        }
    }
}

+ (BOOL)showAnnotationType:(NSString *)annotType type:(FSAnnotType)type pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl belowSubview:(UIView *)siblingSubview {
    for (UIView *view in pdfViewCtrl.subviews) {
        if (view.tag == 2113) {
            [view removeFromSuperview];
        }
    }

    CGSize titleSize = [Utility getTextSize:annotType fontSize:13.0f maxSize:CGSizeMake(130, 100)];

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(pdfViewCtrl.bounds) / 2, CGRectGetHeight(pdfViewCtrl.bounds) - 80, titleSize.width + 20 + 10, 20)];
    view.center = CGPointMake(CGRectGetWidth(pdfViewCtrl.bounds) / 2, CGRectGetHeight(pdfViewCtrl.bounds) - 70);
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.8;
    view.layer.cornerRadius = 5.0f;
    view.tag = 2113;

    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(5, 2, 16, 16);
    switch (type) {
    case FSAnnotHighlight:
        imageView.image = [UIImage imageNamed:@"property_type_highlight" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotLink:
        break;
    case FSAnnotNote:
        imageView.image = [UIImage imageNamed:@"property_type_note" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotStrikeOut:
        imageView.image = [UIImage imageNamed:@"property_type_strikeout" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotUnderline:
        imageView.image = [UIImage imageNamed:@"property_type_underline" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotSquiggly:
        imageView.image = [UIImage imageNamed:@"property_type_squiggly" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotSquare:
        imageView.image = [UIImage imageNamed:@"property_type_rectangle" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotCircle:
        imageView.image = [UIImage imageNamed:@"property_type_circle" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotLine:
        if ([annotType isEqualToString:FSLocalizedForKey(@"kArrow")]) {
            imageView.image = [UIImage imageNamed:@"property_type_arrowline" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        }
        else if ([annotType isEqualToString:FSLocalizedForKey(@"kDistance")]) {
            imageView.image = [UIImage imageNamed:@"property_type_distance" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        }
        else {
            imageView.image = [UIImage imageNamed:@"property_type_line" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        }
        break;
    case FSAnnotFreeText:
        if ([annotType isEqualToString:FSLocalizedForKey(@"kTextbox")]) {
            imageView.image = [UIImage imageNamed:@"property_type_textbox" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        } else if ([annotType isEqualToString:FSLocalizedForKey(@"kCallout")]) {
            imageView.image = [UIImage imageNamed:@"property_type_callout" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        } else {
            imageView.image = [UIImage imageNamed:@"property_type_freetext" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        }
        break;
    case FSAnnotInk:
        if ([annotType isEqualToString:FSLocalizedForKey(@"kEraser")]) {
            imageView.image = [UIImage imageNamed:@"property_type_erase" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        } else {
            imageView.image = [UIImage imageNamed:@"property_type_pencil" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        }
        break;
    case FSAnnotStamp:
        imageView.image = [UIImage imageNamed:@"property_type_stamp" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotCaret:
        if ([annotType isEqualToString:FSLocalizedForKey(@"kReplaceText")]) {
            imageView.image = [UIImage imageNamed:@"property_type_replace" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        } else
            imageView.image = [UIImage imageNamed:@"property_type_caret" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotScreen:
            if ([annotType isEqualToString:FSLocalizedForKey(@"kPropertyImage")]) {
                imageView.image = [UIImage imageNamed:@"property_type_image" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
            }else if ([annotType isEqualToString:FSLocalizedForKey(@"kAudio")]){
                imageView.image = [UIImage imageNamed:@"property_type_audio" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
            }else{
                imageView.image = [UIImage imageNamed:@"property_type_video" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
            }
        break;
    case FSAnnotFileAttachment:
        imageView.image = [UIImage imageNamed:@"property_type_attachment" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotPolygon:
        if ([annotType isEqualToString:FSLocalizedForKey(@"kCloud")]) {
            imageView.image = [UIImage imageNamed:@"property_type_cloud" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        } else {
            imageView.image = [UIImage imageNamed:@"property_type_polygon" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        }
        break;
    case FSAnnotPolyLine:
        imageView.image = [UIImage imageNamed:@"property_type_polyline" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    case FSAnnotRedact:
        imageView.image = [UIImage imageNamed:@"property_type_polyline" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        break;
    default:
        break;
    }

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleSize.width, 20)];
    label.center = CGPointMake(view.frame.size.width / 2 + 10, view.frame.size.height / 2);
    label.backgroundColor = [UIColor clearColor];
    label.text = annotType;
    label.textColor = WhiteThemeTextColor;
    label.font = [UIFont systemFontOfSize:12];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(view).offset(-5);
        make.centerY.mas_equalTo(view);
    }];
    if (imageView.image){
        [view addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(view).offset(5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(imageView.frame.size);
        }];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageView.mas_right).offset(5);
        }];
    }else{
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(view).offset(5);
        }];
    }
    [pdfViewCtrl insertSubview:view belowSubview:siblingSubview];

    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view.superview.mas_centerX).offset(0);
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(pdfViewCtrl.mas_safeAreaLayoutGuideBottom).offset(-80);
        } else {
            make.top.equalTo(view.superview.mas_bottom).offset(-80);
        }
        make.height.mas_equalTo(@20);
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:3
                         animations:^{
                             view.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             [view removeFromSuperview];
                         }];
    });
    return YES;
}

+ (void)showTips:(NSString *)tipsContent pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl belowSubview:(UIView *)siblingSubview {
    for (UIView *view in pdfViewCtrl.subviews) {
        if (view.tag == 2113) {
            [view removeFromSuperview];
        }
    }
    
    CGSize titleSize = [Utility getTextSize:tipsContent fontSize:13.0f maxSize:CGSizeMake(100, 100)];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(pdfViewCtrl.bounds) / 2, CGRectGetHeight(pdfViewCtrl.bounds) - 80, titleSize.width + 20 + 10, 20)];
    view.center = CGPointMake(CGRectGetWidth(pdfViewCtrl.bounds) / 2, CGRectGetHeight(pdfViewCtrl.bounds) - 70);
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.8;
    view.layer.cornerRadius = 5.0f;
    view.tag = 2113;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(5, 2, 16, 16);
    
    imageView.image = [UIImage imageNamed:@"property_type_multiple_select" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleSize.width, 20)];
    label.center = CGPointMake(view.frame.size.width / 2 + 10, view.frame.size.height / 2);
    label.backgroundColor = [UIColor clearColor];
    label.text = tipsContent;
    label.textColor = WhiteThemeTextColor;
    label.font = [UIFont systemFontOfSize:12];
    [view addSubview:imageView];
    [view addSubview:label];
    [pdfViewCtrl insertSubview:view belowSubview:siblingSubview];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view.superview.mas_centerX).offset(0);
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(pdfViewCtrl.mas_safeAreaLayoutGuideBottom).offset(-80);
        } else {
            make.top.equalTo(view.superview.mas_bottom).offset(-80);
        }
        make.width.mas_equalTo(titleSize.width + 20 + 10);
        make.height.mas_equalTo(@20);
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:3
                         animations:^{
                             view.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             [view removeFromSuperview];
                         }];
    });
}

//Add animation
+ (void)addAnimation:(CALayer *)layer type:(NSString *)type subType:(NSString *)subType timeFunction:(NSString *)timeFunction duration:(float)duration {
    CATransition *animation = [CATransition animation];
    [animation setType:type];
    [animation setSubtype:subType];
    [animation setDuration:duration];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:timeFunction]];
    [layer addAnimation:animation forKey:nil];
}

+ (FSReadingBookmark *)getReadingBookMarkAtPage:(FSPDFDoc *)doc page:(int)page {
    int count;
    @try {
        count = [doc getReadingBookmarkCount];
    } @catch (NSException *exception) {
        return nil;
    }
    for (int i = 0; i < count; i++) {
        FSReadingBookmark *bookmark = [doc getReadingBookmark:i];
        if ([bookmark getPageIndex] == page)
            return bookmark;
    }
    return nil;
}

+ (UIButton *)createButtonWithImage:(UIImage *)image {
    UIButton *button = [[UIButton alloc] initWithFrame:(CGRect){CGPointZero, image.size}];
    [button setEnlargedEdge:3.f];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;

    [button setImage:image forState:UIControlStateNormal];
    UIImage *translucentImage = [Utility imageByApplyingAlpha:image alpha:0.5];
    [button setImage:translucentImage forState:UIControlStateHighlighted];
    [button setImage:translucentImage forState:UIControlStateDisabled];

    return button;
}

+ (UIButton *)createButtonWithTitle:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    CGSize titleSize = [Utility getTextSize:title fontSize:18.0f maxSize:CGSizeMake(400, 100)];
    button.frame = (CGRect){CGPointZero, titleSize};
    button.contentMode = UIViewContentModeScaleToFill;
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [button setEnlargedEdge:3];
    return button;
}

+ (UIButton *)createButtonWithImageAndTitle:(NSString *)title
                                imageNormal:(UIImage *)imageNormal
                              imageSelected:(UIImage *)imageSelected
                               imageDisable:(UIImage *)imageDisabled {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGSize titleSize = [Utility getTextSize:title fontSize:9.0f maxSize:CGSizeMake(200, 100)];
    
    float width = imageNormal.size.width;
    float height = imageNormal.size.height;
    button.contentMode = UIViewContentModeScaleToFill;
    [button setImage:imageNormal forState:UIControlStateNormal];
    [button setImage:[Utility imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateHighlighted];
    [button setImage:[Utility imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateSelected];
    
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRGB:0x5c5c5c] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithRGB:0x5c5c5c] forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont systemFontOfSize:9];
    
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height * 1.5, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    button.frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width + 2 : width, titleSize.height + height);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    return button;
}

+ (CGFloat)getUIToolbarPaddingX {
    if (IOS13_OR_LATER) return 2.f;
    return (IOS11_OR_LATER) ? 16.f : 20.f;
}

+ (BOOL)isSinceiOS:(const NSString *)requiredVersion {
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    return ([currSysVer compare:(NSString *) requiredVersion options:NSNumericSearch] != NSOrderedAscending);
}

#pragma mark print methods

+ (UIPrintInteractionController *)createPrintInteractionControolerForDoc:(FSPDFDoc *)doc jobName:(nullable NSString *)jobName {
    if (![Utility isSinceiOS:@"4.2"]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kAirPrintVersion") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        return nil;
    }

    if (![UIPrintInteractionController isPrintingAvailable]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kAirPrintNotAvailable") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        return nil;
    }

    if (![Utility canPrintDocument:doc]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSNoAccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        return nil;
    }

    //save editing first? todo

    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    printController.showsPageRange = YES;

    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = jobName;
    if ([doc getPageCount] > 0) {
        FSPDFPage *page = [doc getPage:0];
        if ([page getWidth] > [page getHeight]) {
            printInfo.orientation = UIPrintInfoOrientationLandscape;
        }
    }
    printController.printInfo = printInfo;
    printController.printPageRenderer = [[PrintRenderer alloc] initWithDocument:doc];
    return printController;
}

+ (void)printDoc:(FSPDFDoc *)doc animated:(BOOL)animated jobName:(nullable NSString *)jobName delegate:(nullable id<UIPrintInteractionControllerDelegate>)delegate completionHandler:(nullable UIPrintInteractionCompletionHandler)completion {
    UIPrintInteractionController *printController = [self.class createPrintInteractionControolerForDoc:doc jobName:jobName];
    printController.delegate = delegate;
    [printController presentAnimated:animated completionHandler:completion];
}

+ (void)printDoc:(FSPDFDoc *)doc fromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated jobName:(nullable NSString *)jobName delegate:(nullable id<UIPrintInteractionControllerDelegate>)delegate completionHandler:(nullable UIPrintInteractionCompletionHandler)completion {
    UIPrintInteractionController *printController = [self.class createPrintInteractionControolerForDoc:doc jobName:jobName];
    printController.delegate = delegate;
    [printController presentFromRect:rect inView:view animated:animated completionHandler:completion];
}

////Take screen shot
+ (UIImage *)screenShot:(UIView *)view {
    // Create a graphics context with the target size
    CGSize imageSize = view.bounds.size;
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, [UIScreen mainScreen].scale);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    // Retrieve the screenshot image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

//remove all gesture
+ (void)removeAllGestureRecognizer:(UIView *)view {
    for (UIGestureRecognizer *ges in view.gestureRecognizers) {
        [view removeGestureRecognizer:ges];
    }
}

//Crop image
+ (UIImage*)cropImage:(UIImage*)img rect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [img scale]);
    [img drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    UIImage *cropped_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cropped_image;
}

+ (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation{
    long double rotate = 0.0;
    CGRect rect;
    float translateX = 0;
    float translateY = 0;
    float scaleX = 1.0;
    float scaleY = 1.0;
    
    switch (orientation) {
        case UIImageOrientationLeft:
            rotate = M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = 0;
            translateY = -rect.size.width;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationRight:
            rotate = 3 * M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = -rect.size.height;
            translateY = 0;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationDown:
            rotate = M_PI;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = -rect.size.width;
            translateY = -rect.size.height;
            break;
        default:
            rotate = 0.0;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = 0;
            translateY = 0;
            break;
    }
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0.0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextRotateCTM(context, rotate);
    CGContextTranslateCTM(context, translateX, translateY);
    
    CGContextScaleCTM(context, scaleX, scaleY);
    CGContextDrawImage(context, CGRectMake(0, 0, rect.size.width, rect.size.height), image.CGImage);
    
    UIImage *newPic = UIGraphicsGetImageFromCurrentImageContext();
    
    return newPic;
}

#pragma mark

+ (NSArray *)getTextRects:(FSTextPage *)fstextPage startCharIndex:(int)startCharIndex endCharIndex:(int)endCharIndex {
    NSMutableArray *ret = [NSMutableArray array];
    if (fstextPage == nil) {
        return ret;
    }
    int count = ABS(endCharIndex - startCharIndex) + 1;
    startCharIndex = MIN(startCharIndex, endCharIndex);
    int rectCount = [fstextPage getTextRectCount:startCharIndex count:count];
    for (int i = 0; i < rectCount; i++) {
        FSRectF *dibRect = [fstextPage getTextRect:i];
        if (dibRect.getLeft == dibRect.getRight || dibRect.getTop == dibRect.getBottom) {
            continue;
        }

        FSRotation direction = [fstextPage getBaselineRotation:i];
        NSArray *array = [NSArray arrayWithObjects:[NSValue valueWithCGRect:[Utility FSRectF2CGRect:dibRect]], @(direction), nil];

        [ret addObject:array];
    }

    //merge rects if possible
    if (ret.count > 1) {
        int i = 0;
        while (i < ret.count - 1) {
            int j = i + 1;
            while (j < ret.count) {
                FSRectF *rect1 = [Utility CGRect2FSRectF:[[[ret objectAtIndex:i] objectAtIndex:0] CGRectValue]];
                FSRectF *rect2 = [Utility CGRect2FSRectF:[[[ret objectAtIndex:j] objectAtIndex:0] CGRectValue]];

                int direction1 = [[[ret objectAtIndex:i] objectAtIndex:1] intValue];
                int direction2 = [[[ret objectAtIndex:j] objectAtIndex:1] intValue];
                BOOL adjcent = NO;
                if (direction1 == direction2) {
                    adjcent = NO;
                }
                if (adjcent) {
                    FSRectF *rectResult = [[FSRectF alloc] init];
                    [rectResult setLeft:MIN([rect1 getLeft], [rect2 getLeft])];
                    [rectResult setBottom:MAX([rect1 getTop], [rect2 getTop])];
                    [rectResult setRight:MAX([rect1 getRight], [rect2 getRight])];
                    [rectResult setTop:MIN([rect1 getBottom], [rect2 getBottom])];
                    NSArray *array = [NSArray arrayWithObjects:[NSValue valueWithCGRect:[Utility FSRectF2CGRect:rectResult]], @(direction1), nil];
                    [ret replaceObjectAtIndex:i withObject:array];
                    [ret removeObjectAtIndex:j];
                } else {
                    j++;
                }
            }
            i++;
        }
    }
    return ret;
}

// get distance unit info
+ (NSArray *)getDistanceUnitInfo:(NSString *)measureRatio {
    measureRatio = [measureRatio stringByReplacingOccurrencesOfString:@"= "withString:@""];
    NSArray *result = [measureRatio componentsSeparatedByString:@" "];
    return  result;
}

// get distance between to cgpoint
+ (float)getDistanceFromX:(FSPointF *)start toY:(FSPointF *)end{
    float distance;
    CGFloat xDist = (end.x - start.x);
    CGFloat yDist = (end.y - start.y);
    distance = sqrt((xDist * xDist) + (yDist * yDist));
    return distance;
}

// get distance between to cgpoint with unit
+ (float)getDistanceFromX:(FSPointF *)start toY:(FSPointF *)end withUnit:(NSString *)measureRatio{
    NSArray *distancInfo = [Utility getDistanceUnitInfo:measureRatio];
    
    NSString *distanceUnit = [distancInfo objectAtIndex:1];
    float scale = 0.0;
    if ([[distancInfo objectAtIndex:0] floatValue] != 0) {
        scale = [[distancInfo objectAtIndex:2] floatValue]/[[distancInfo objectAtIndex:0] floatValue];
    }
    
    float distance = [Utility getDistanceFromX:start toY:end]; // pt
    NSMutableDictionary *unitDict = @{
                                      @"pt":[NSNumber numberWithFloat:1.0 ],
                                      @"inch":[NSNumber numberWithFloat:1.0/72 ],
                                      @"ft":[NSNumber numberWithFloat:1.0/(72 *12) ],
                                      @"yd":[NSNumber numberWithFloat:1.0/(72 *36) ],
                                      @"p":[NSNumber numberWithFloat:1.0/(12) ],
                                      @"mm":[NSNumber numberWithFloat:25.4/72 ],
                                      @"cm":[NSNumber numberWithFloat:2.54/72 ],
                                      @"m":[NSNumber numberWithFloat:0.0254/72 ],
                                      }.mutableCopy;
    
    float transParams = [[unitDict objectForKey:distanceUnit] floatValue];
    return distance * transParams * scale;
}

+ (FSImage *)uiimageToFSImage:(UIImage *)image {
    NSString *tmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:@"_uiimageToFSImage_tmp_.png"];
    if ([UIImagePNGRepresentation(image) writeToFile:tmpFile atomically:YES]) {
        return [[FSImage alloc] initWithPath:tmpFile];
    } else {
        return nil;
    }
    //    FSBitmap *bitmap = [Utility uiimageToFSBitmap:image];
    //    if (bitmap) {
    //        FSImage *fsImage = [[FSImage alloc] init];
    //        [fsImage addFrame:bitmap];
    //        return fsImage;
    //    }
    //    return nil;
}

+ (CGRect)boundedRectForRect:(CGRect)rect containerRect:(CGRect)containerRect {
    rect.origin.x = MAX(CGRectGetMinX(rect), CGRectGetMinX(containerRect));
    rect.origin.x = MIN(CGRectGetMinX(rect), CGRectGetMaxX(containerRect) - CGRectGetWidth(rect));
    rect.origin.y = MAX(CGRectGetMinY(rect), CGRectGetMinY(containerRect));
    rect.origin.y = MIN(CGRectGetMinY(rect), CGRectGetMaxY(containerRect) - CGRectGetHeight(rect));
    return rect;
}

+ (FSRotation)rotationForValue:(NSValue *)value {
    switch ([(NSNumber *) value intValue]) {
    case 0:
        return FSRotation0;
    case 90:
        return FSRotation90;
    case 180:
        return FSRotation180;
    case 270:
        return FSRotation270;
    default:
        return FSRotation0;
    }
}

+ (int)valueForRotation:(FSRotation)rotation {
    switch (rotation) {
    case FSRotation0:
        return 0;
    case FSRotation90:
        return 90;
    case FSRotation180:
        return 180;
    case FSRotation270:
        return 270;
    default:
        return 0;
    }
}

+ (UIImageOrientation)imageOrientationForRotation:(FSRotation)rotation {
    switch (rotation) {
    case FSRotation0:
        return UIImageOrientationUp;
    case FSRotation90:
        return UIImageOrientationLeft;
    case FSRotation180:
        return UIImageOrientationDown;
    case FSRotation270:
        return UIImageOrientationRight;
    default:
        return UIImageOrientationUp;
    }
}

//+ (NSArray<FSPointF *> *)getPolygonVertexes:(FSPolygon *)polygon {
//    NSMutableArray<FSPointF *> *vertexes = @[].mutableCopy;
//    FSPointFArray *points = [polygon getVertexes];
//    for (int i = 0; i < [points getSize]; i++) {
//        FSPointF *vertex = [points getAt:i];
//        if (vertex) {
//            [vertexes addObject:vertex];
//        }
//    }
//    return vertexes;
//}
//
//+ (NSArray<FSPointF *> *)getPolyLineVertexes:(FSPolyLine *)polyline {
//    NSMutableArray<FSPointF *> *vertexes = @[].mutableCopy;
//    FSPointFArray *points = [polyline getVertexes];
//    for (int i = 0; i < [points getSize]; i++) {
//        FSPointF *vertex = [points getAt:i];
//        if (vertex) {
//            [vertexes addObject:vertex];
//        }
//    }
//    return vertexes;
//}

+ (BOOL)isAnnot:(FSAnnot *)annot1 uniqueToAnnot:(FSAnnot *)annot2 {
    return (annot1 == nil && annot2 == nil) || [annot1 isEqualToAnnot:annot2];
}

+(NSDictionary *)getACLInfo:(NSString *)aclstr {
    NSData *jsonData = [aclstr dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                    options:NSJSONReadingMutableContainers
                                                      error:&err];
    if(err) {
        return nil;
    }
    
    if (jsonObject != nil && err == nil){
        if ([jsonObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *deserializedDictionary = (NSDictionary *)jsonObject;
            return deserializedDictionary;
        }
        else if ([jsonObject isKindOfClass:[NSArray class]]){
            NSArray *deserializedArray = (NSArray *)jsonObject;
            return [deserializedArray lastObject];
        } else {
            return nil;
        }
    } else if ([jsonData length] == 0 && err == nil) {
        return nil;
    } else {
        return nil;
    }
}

+ (UIImage *)drawCDRMNonAccessImage:(int)dibWidth dibHeight:(int)dibHeight{
    UIImage *img = nil;
    
    CGFloat scale = [UIScreen mainScreen].scale;
    int newDibWidth = dibWidth * scale;
    int newDibHeight = dibHeight * scale;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDibWidth, newDibHeight), YES, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 1, 1, 1, 1);
    CGContextFillRect(context, CGRectMake(0, 0, newDibWidth, newDibHeight));
    
    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont systemFontOfSize:14.f], NSForegroundColorAttributeName:[UIColor blackColor]};
    NSString *text = FSLocalizedForKey(@"kInvalidPermissionTip");
    CGRect rect = [text boundingRectWithSize:CGSizeMake(0,14) options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:attributes context:nil];
    
    [text drawAtPoint:CGPointMake((newDibWidth-rect.size.width)/2,(newDibHeight - 14)/2) withAttributes:attributes];
    img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (NSString *)encodeEmojiString:(NSString *)encodeString{
    if ([self stringContainsEmoji:encodeString]) {
        NSString *utf8Str = [NSString stringWithUTF8String:[encodeString UTF8String]];
        NSData *tempData = [utf8Str dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *result = [[NSString alloc] initWithData:tempData encoding:NSUTF8StringEncoding];
        return result;
    }else{
        return encodeString;
    }
}

+ (NSString *)decodeEmojiString:(NSString *)decodeString{
    if (!decodeString) return nil;
    const char *cString = [decodeString UTF8String];
    NSData *tempData = [NSData dataWithBytes:cString length:strlen(cString)];
    NSString *result = [[NSString alloc] initWithData:tempData encoding:NSNonLossyASCIIStringEncoding];
    
    if (![self stringContainsEmoji:result]) {
        return decodeString;
    }
    
    return result;
}

+ (NSString *)filterStrEmoji:(NSString *)emojiStr
{
    NSString *tempStr = [[NSString alloc]init];
    NSMutableString *kksstr = [[NSMutableString alloc]init];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:10];
    NSMutableString *strMu = [[NSMutableString alloc]init];
    for(int i =0; i < [emojiStr length]; i++)
    {
        tempStr = [emojiStr substringWithRange:NSMakeRange(i, 1)];
        [strMu appendString:tempStr];
        if ([self stringContainsEmoji:strMu]) {
            strMu = [[strMu substringToIndex:([strMu length]-2)] mutableCopy];
            [array removeLastObject];
            continue;
        }else
            [array addObject:tempStr];
    }
    for (NSString *strs in array) {
        [kksstr appendString:strs];
    }
    return kksstr;
}

+ (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}

#pragma mark xfa print
+ (void)printXFADoc:(FSXFADoc *)doc animated:(BOOL)animated jobName:(nullable NSString *)jobName delegate:(nullable id<UIPrintInteractionControllerDelegate>)delegate completionHandler:(nullable UIPrintInteractionCompletionHandler)completion {
    UIPrintInteractionController *printController = [self.class createPrintInteractionControolerForXFADoc:doc jobName:jobName];
    printController.delegate = delegate;
    [printController presentAnimated:animated completionHandler:completion];
}

+ (void)printXFADoc:(FSXFADoc *)doc fromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated jobName:(nullable NSString *)jobName delegate:(nullable id<UIPrintInteractionControllerDelegate>)delegate completionHandler:(nullable UIPrintInteractionCompletionHandler)completion {
    UIPrintInteractionController *printController = [self.class createPrintInteractionControolerForXFADoc:doc jobName:jobName];
    printController.delegate = delegate;
    [printController presentFromRect:rect inView:view animated:animated completionHandler:completion];
}

+ (UIPrintInteractionController *)createPrintInteractionControolerForXFADoc:(FSXFADoc *)doc jobName:(nullable NSString *)jobName {
    if (![Utility isSinceiOS:@"4.2"]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kAirPrintVersion") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
        return nil;
    }
    
    if (![UIPrintInteractionController isPrintingAvailable]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kAirPrintNotAvailable") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
        return nil;
    }
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    printController.showsPageRange = YES;
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = jobName;
    if ([doc getPageCount] > 0) {
        FSXFAPage *page = [doc getPage:0];
        if ([page getWidth] > [page getHeight]) {
            printInfo.orientation = UIPrintInfoOrientationLandscape;
        }
    }
    printController.printInfo = printInfo;
    printController.printPageRenderer = [[PrintXFARenderer alloc] initWithXFADoc:doc];
    return printController;
}

+ (void)printXFAPage:(FSXFAPage *)page inContext:(CGContextRef)context inRect:(CGRect)rect shouldDrawAnnotation:(BOOL)shouldDrawAnnotation{
    UIImage *image = [Utility drawXFAPage:page dibWidth:rect.size.width dibHeight:rect.size.height shouldDrawAnnotation:shouldDrawAnnotation needPause:nil rotation:FSRotation0];
    if (image) {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, 2 * rect.origin.y + rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawImage(context, rect, image.CGImage);
        CGContextRestoreGState(context);
    }
}

+ (void)flattenAnnot:(FSAnnot *)annot completed:(void (^)(NSArray<FSAnnotAttributes *> *replyAttributes))completed{
    if (!annot || [annot isEmpty]) {
        return;
    }
    FSPDFPage *page = [annot getPage];
    FSAnnotType type = [annot type];
    @try {
        NSArray *replyAnnots = annot.replyAnnots;
        BOOL flag = NO;
        if (type == FSAnnotCaret) {
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            if ([markup isGrouped]) {
                FSMarkupArray *groupAnnots = [markup getGroupElements];
                for (int i = 0; i < [groupAnnots getSize]; i++) {
                    FSAnnot *groupAnnot = [groupAnnots getAt:i];
                    flag = [page flattenAnnot:groupAnnot];
                }
            }else{
                goto dealFlattenAnnot;
            }
        }else{
            dealFlattenAnnot:{flag = [page flattenAnnot:annot];}
        }
        if (flag) {
            NSMutableArray *replyAttributes = @[].mutableCopy;
            if (replyAnnots.count) {
                for (FSNote *note in replyAnnots) {
                    [replyAttributes addObjectsFromArray:[self removeAnnotWithReplies:note tempAttributesArr:@[].mutableCopy]];
                }
            }
            completed(replyAttributes.copy);
        }
    } @catch (NSException *exception) {
    }
}

+ (NSArray *)removeAnnotWithReplies:(FSAnnot *)annot tempAttributesArr:(NSMutableArray *)tempAttributesArr {
    FSPDFPage *page = [annot getPage];
    NSArray *replyAnnots = annot.replyAnnots;
    for (FSNote *note in replyAnnots) {
        NSArray *noteReplyAnnots = note.replyAnnots;
        if (noteReplyAnnots) {
            [self removeAnnotWithReplies:note tempAttributesArr:tempAttributesArr];
        }else{
            [tempAttributesArr addObject:[FSAnnotAttributes attributesWithAnnot:note]];
            [page removeAnnot:note];
        }
    }
    [tempAttributesArr addObject:[FSAnnotAttributes attributesWithAnnot:annot]];
    [page removeAnnot:annot];
    return tempAttributesArr.copy;
}

+ (void)applyRedactAnnot:(FSRedact *)annot completed:(void (^)(NSArray<FSAnnotAttributes *> *replyAttributes))completed{
    if (!annot || [annot isEmpty]) {
        return;
    }
    @try {
        NSArray *replyAnnots = annot.replyAnnots;
        BOOL flag = [annot apply];
        if (flag) {
            NSMutableArray *replyAttributes = @[].mutableCopy;
            for (FSNote *note in replyAnnots) {
                [replyAttributes addObjectsFromArray:[self removeAnnotWithReplies:note tempAttributesArr:@[].mutableCopy]];
            }
            completed(replyAttributes.copy);
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
        return;
    }
}

+ (BOOL)isVertexValid:(CGPoint)vertex pageView:(UIView *)pageView{
    CGRect validRect = CGRectInset(pageView.bounds, 5, 5);
    if (!CGRectContainsPoint(validRect, vertex)) {
        return NO;
    }
    return YES;
}

+ (BOOL)canGetAnnot:(FSAnnot *)annot withUIConfig:(UIExtensionsConfig *)config{
    if (![config canInteractWithAnnot:annot]) {
                                          return NO;
                                      }
                                      FSAnnotType type = [annot getType];
                                      if (type == FSAnnotWidget || type == FSAnnotMovie) {
                                          return NO;
                                      }
                                      // 'replace text' consist of caret and strikeout annotation, add former only
                                      if ((type == FSAnnotStrikeOut && [Utility isReplaceText:[[FSStrikeOut alloc] initWithAnnot:annot]])) {
                                          return NO;
                                      }
                                      //State annot is not supported.
                                      if (type == FSAnnotNote && [[[FSNote alloc] initWithAnnot:annot] getState] != 0)
                                          return NO;
                                      if ((annot.flags & FSAnnotFlagHidden) != 0)
                                          return NO;
                                      return YES;
}

+ (NSArray<FSAnnot *> *)getAnnotationsForPageAtIndex:(FSPDFPage *)page withUIConfig:(UIExtensionsConfig *)config{
    NSArray *array = [Utility getAnnotsInPage:page
                               predicateBlock:^BOOL(FSAnnot *_Nonnull annot) {return [self canGetAnnot:annot withUIConfig:config];}];
    return array;
}

+ (NSArray<FSAnnot *> *)getAnnotationsFromAnnots:(NSArray *)annots withUIConfig:(UIExtensionsConfig *)config{
    NSArray *array = [Utility getAnnotsInAnnots:annots
                                 predicateBlock:^BOOL(FSAnnot *_Nonnull annot) {return [self canGetAnnot:annot withUIConfig:config];}];
    return array;
}

+(FSRectF *)convertPageViewRectToPdfRect:(FSRectF *)pageRect pageDisplayMatrix:(FSMatrix2D *)pageDisplayMatrix annot:(FSAnnot *)annot oldPdfRect:(FSRectF *)oldPdfRect rotation:(int)rotation{
    FSMatrix2D *page_matrix = pageDisplayMatrix;
    FSMatrix2D *annot_matrix = [annot getDisplayMatrix:page_matrix];
    FSMatrix2D *transformMatrix = [[FSMatrix2D alloc] init];
    
    [transformMatrix setReverse:annot_matrix];
    
    FSRectF *fsRect = [[FSRectF alloc] initWithLeft1:pageRect.left bottom1:pageRect.bottom right1:pageRect.right top1:pageRect.top];
    
    [transformMatrix transformRect:fsRect];
    
    if([annot getFlags] & FSAnnotFlagNoRotate || [annot getFlags] & FSAnnotFlagNoZoom || [annot getType] == FSAnnotNote)
    {
        int nRotation = rotation;
        FSRotation nPageRotate = 0;
        nPageRotate = [[annot getPage] getRotation];
        nRotation = (nRotation + nPageRotate) % 4;
        
        float height = fsRect.height;
        float width = fsRect.width;
        switch (nRotation) {
            case 0:
                fsRect.top = oldPdfRect.top;
                fsRect.left = oldPdfRect.left;
                fsRect.bottom = fsRect.top - height;
                fsRect.right = fsRect.left + width;
                break;
            case 1:
                fsRect.top = oldPdfRect.bottom;
                fsRect.left = oldPdfRect.left;
                fsRect.bottom = fsRect.top - height;
                fsRect.right = fsRect.left + width;
                break;
            case 2:
                fsRect.top = oldPdfRect.bottom;
                fsRect.left = oldPdfRect.right;
                fsRect.bottom = fsRect.top - height;
                fsRect.right = fsRect.left + width;
                break;
            case 3:
                fsRect.top = oldPdfRect.top;
                fsRect.left = oldPdfRect.right;
                fsRect.bottom = fsRect.top - height;
                fsRect.right = fsRect.left + width;
                break;
            default:
                break;
        }
        
    }
    
    return fsRect;
}

+ (void)tryLoadDoc:(NSString *)filePath withPassword:(NSString *)password success:(void (^)(NSString *password))success error:(void (^_Nullable)(NSString *description))error abort:(void (^_Nullable)(void))abort {
    FSPDFDoc *doc = [[FSPDFDoc alloc] initWithPath:filePath];
    NSString *title = [filePath lastPathComponent];
    FSErrorCode errorCode = [doc load:password];
    switch (errorCode) {
        case FSErrSuccess: {
            if (success) {
                success(password);
            }
        } break;
        case FSErrPassword: {
            NSString *message = password.length > 0 ? @"kDocPasswordError" : @"kDocNeedPassword";
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:FSLocalizedForKey(message) preferredStyle:UIAlertControllerStyleAlert];
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *_Nonnull textField) {
                textField.text = @"";
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction *action) {
                                                                     if (abort) {
                                                                         abort();
                                                                     }
                                                                 }];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               UITextField *textField = alertController.textFields[0];
                                                               NSString *guessPassword = textField.text;
                                                               [Utility tryLoadDoc:filePath withPassword:guessPassword success:success error:error abort:abort];
                                                           }];
            [alertController addAction:cancelAction];
            [alertController addAction:action];
            [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        } break;
        default: {
            if (error) {
                error([Utility getErrorCodeDescription:errorCode]);
            }
        } break;
    }
}

+(BOOL)checkFileisRMS:(NSString *)filePath password:(NSString *)password {
    NSString *MSIRM = @"MicrosoftIRMServices";
    NSString *FXRMS = @"FoxitRMS";
    
    FSPDFDoc *fspdfdoc = [[FSPDFDoc alloc] initWithPath:filePath];
    FSErrorCode ret = (FSErrorCode) [fspdfdoc load:password];
    
    BOOL isRMSFile = NO;
    FSPDFDictionary *encryDic = [fspdfdoc getEncryptDict];
    NSString *filterStr;
    if ([encryDic hasKey:@"Filter"]) {
        FSPDFObject *filterObj = [encryDic getElement:@"Filter"];
        filterStr = [filterObj getWideString];
    }
    if ([filterStr isEqualToString:MSIRM] || [filterStr isEqualToString:FXRMS]) {
        int irmVersion = 0;
        if ([encryDic hasKey:@"MicrosoftIRMVersion"]) {
            FSPDFObject *irmVersionOBJ = [encryDic getElement:@"MicrosoftIRMVersion"];
            irmVersion = [irmVersionOBJ getInteger];
            if (irmVersion == 2) {
                isRMSFile = YES;
            }
        }
    }
    
    FSPDFDocWrapperType pdfWrapperType = [fspdfdoc getWrapperType];
    if (pdfWrapperType == FSPDFDocWrapperPDFV2) {
        isRMSFile = YES;
    }
    
    if (pdfWrapperType == FSPDFDocWrapperFoxit) {
        FSWrapperData *wrapperData = [fspdfdoc getWrapperData];
        NSString *wrapperType = [wrapperData getType];
        if ([wrapperType isEqualToString:@"MicrosoftIRMServices"] || [wrapperType isEqualToString:@"FoxitRMS"]) {
            isRMSFile = YES;
        }
    }
    
    if (ret == FSErrSecurityHandler) {
        FSPDFDictionary *encryDic = [fspdfdoc getEncryptDict];
        NSString *filterStr;
        if ([encryDic hasKey:@"Filter"]) {
            FSPDFObject *filterObj = [encryDic getElement:@"Filter"];
            filterStr = [filterObj getWideString];
        }
        if ([filterStr isEqualToString:MSIRM]) {
            isRMSFile = YES;
        }
    }
    return isRMSFile;
}

+(CGFloat)GetStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

+ (FSRectF *)normalizePDFRect:(FSRectF *)rectF {
    if (rectF.left > rectF.right) {
        float tmp = rectF.left;
        rectF.left = rectF.right;
        rectF.right = tmp;
    }
    
    if (rectF.top < rectF.bottom) {
        float tmp = rectF.top;
        rectF.top = rectF.bottom;
        rectF.bottom = tmp;
    }
    
    if (rectF.left == rectF.right) rectF.right += 1;
    if (rectF.top == rectF.bottom) rectF.top += 1;
    return rectF;
}

#endif
+ (CGFloat)realPX:(CGFloat)wantPX {
    if ([UIScreen mainScreen].scale == 0) {
        return wantPX;
    }
    return wantPX / [UIScreen mainScreen].scale;
}

+ (CGSize)getTextSize:(NSString *)text fontSize:(float)fontSize maxSize:(CGSize)maxSize {
    if (nil == text)
        text = @""; //for getting correct text size as following.
    NSDictionary *attrs = @{NSFontAttributeName : [UIFont systemFontOfSize:fontSize]};
    CGSize textSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    textSize.width += 2;
    return textSize;
}

// calculate Attributed String size
+ (CGSize)getAttributedTextSize:(NSAttributedString *)attributedString maxSize:(CGSize)maxSize {
    CGRect stringRect = [attributedString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    stringRect.size.width += 4;
    return stringRect.size;
}

+ (UIImage *)unableImage:(UIImage*)originImage{
    return [self imageByApplyingAlpha:originImage alpha:0.5];
}

+ (UIImage *)imageByApplyingAlpha:(UIImage *)image alpha:(CGFloat)alpha {
    if (!image) return nil;
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    if (!ctx)
        return nil;
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);

    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);

    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);

    CGContextSetAlpha(ctx, alpha);

    CGContextDrawImage(ctx, area, image.CGImage);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}

+ (UIViewController*) getTopMostViewController {
    UIViewController *presentingViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    while (presentingViewController.presentedViewController != nil) {
        presentingViewController = presentingViewController.presentedViewController;
    }
    return presentingViewController;
}

+ (NSString *)localizedString:(NSString *)translation_key {
    return [self localizedString:translation_key language:[FSLocalization languageAbbr]];
}

+ (NSString *)localizedString:(NSString *)translation_key language:(NSString *)language{
    if (!translation_key) return nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:path];
    NSString *localizableStr = [languageBundle localizedStringForKey:translation_key value:@"" table:[FSLocalization tableName]];
    if ([localizableStr isEqualToString:translation_key] || !languageBundle) {
        if([localizableStr isEqualToString:translation_key]){
            path = [[NSBundle bundleForClass:self] pathForResource:language ofType:@"lproj"];
            languageBundle = [NSBundle bundleWithPath:path];
            localizableStr = [languageBundle localizedStringForKey:translation_key value:@"" table:@"FoxitLocalizable"];
        }
        if (!languageBundle) {
            path = [[NSBundle bundleForClass:self] pathForResource:@"en" ofType:@"lproj"];
            languageBundle = [NSBundle bundleWithPath:path];
            localizableStr = [languageBundle localizedStringForKey:translation_key value:@"" table:@"FoxitLocalizable"];
        }
    }
    return localizableStr;
}

+ (void)alert_OK_WithTitle:(NSString *)title message:(NSString *)message actionBack:(AlertActionCallBack)actionBack{
    [self alert_OK_WithTitle:title message:message actionBack:actionBack onCtrl:[self getTopMostViewController]];
}

+ (void)alert_OK_WithTitle:(NSString *)title message:(NSString *)message actionBack:(AlertActionCallBack)actionBack onCtrl:(UIViewController *)ctrl{
    if ([title hasPrefix:@"k"]) title = FSLocalizedForKey(title);
    if ([message hasPrefix:@"k"]) message = FSLocalizedForKey(message);
    if (!ctrl.fs_isCurrentViewControllerVisible) {
        [ctrl.view fs_showHUDMessage:message];
        return;
    }
    UIAlertController *alertCtrl = [UIAlertController fs_alertWithTitle:title message:message actions:@[ FSLocalizedForKey(@"kOK")] actionBack:actionBack];
    [ctrl presentViewController:alertCtrl animated:YES completion:nil];
}

@end
