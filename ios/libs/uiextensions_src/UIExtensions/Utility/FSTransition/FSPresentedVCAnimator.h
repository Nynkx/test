/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <UIKit/UIKit.h>

@class FSPDFViewCtrl;
NS_ASSUME_NONNULL_BEGIN

@interface FSPresentedVCAnimator : NSObject<UIViewControllerTransitioningDelegate>
@property (nonatomic, assign) FSModalPresentationStyle presentationStyle;
@property (nonatomic, assign) CGFloat maxVerticalHeigh;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, copy) dispatch_block_t dismissViewControllerCompletion;
- (instancetype)initWithPdfViewCtrl:(nullable FSPDFViewCtrl *)pdfViewCtrl ;
- (instancetype)initWithVerticalHeigh:(CGFloat)verticalHeigh presentationStyle:(FSModalPresentationStyle)modalPresentationStyle;
- (instancetype)initWithSize:(CGSize)size presentationStyle:(FSModalPresentationStyle)presentationStyle;

@end

NS_ASSUME_NONNULL_END
