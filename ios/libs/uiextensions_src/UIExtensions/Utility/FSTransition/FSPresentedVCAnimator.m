/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSPresentedVCAnimator.h"
#import <FoxitRDK/FSPDFViewControl.h>

static NSString *kTransitionPanGestureKey = @"kTransitionPanGestureKey";

@interface FSPresentationController : UIPresentationController <UIGestureRecognizerDelegate>
@property (nonatomic, assign) FSModalPresentationStyle modalPresentationStyle;
@property (nonatomic, assign) CGFloat maxVerticalHeigh;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIViewController *sourceViewController;
@property (nonatomic, assign) BOOL shouldAutorotate;
@property (nonatomic, assign) double currentRotation;
@property (nonatomic, assign) UIInterfaceOrientation lastPreferredInterfaceOrientation;

@property (nonatomic, strong) UIView *dimmingView;
@property (nonatomic, strong) UIView *safeFillView;
@property (nonatomic, assign) CGSize srcSize;
@property (nonatomic, assign) CGPoint srcPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, assign) BOOL isChangeSize;
@property (nonatomic, assign) BOOL isStopPanUp;
@property (nonatomic, assign) BOOL isStopPanDown;
@property (nonatomic, copy) dispatch_block_t dismissViewControllerCompletion;

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;
- (UIInterfaceOrientation)currentPreferredInterfaceOrientation;
@end

@implementation FSPresentationController

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController{
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    if (self) {
        self.shouldAutorotate = [self.presentedViewController shouldAutorotate];
        self.currentRotation = 0;
    }
    return self;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return [self.presentedViewController preferredInterfaceOrientationForPresentation];
}

- (UIInterfaceOrientation)currentPreferredInterfaceOrientation{
    return [UIApplication sharedApplication].statusBarOrientation;
}

- (BOOL)shouldPresentInFullscreen{
    if (self.modalPresentationStyle == FSModalPresentationFullPDFViewCtrl) {return CGRectEqualToRect([UIScreen mainScreen].bounds, self.pdfViewCtrl.bounds); }
    return YES;
}

- (void)presentationTransitionWillBegin{
    [super presentationTransitionWillBegin];
    if (self.modalPresentationStyle == FSModalPresentationFullPDFViewCtrl){
        self.presentedViewController.view.hidden = NO;
        self.presentedView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        UIInterfaceOrientation preferredInterfaceOrientationForPresentation = [self preferredInterfaceOrientationForPresentation];
        UIInterfaceOrientation currentPreferredInterfaceOrientation = [self currentPreferredInterfaceOrientation];
        if (self.preferredInterfaceOrientationForPresentation != currentPreferredInterfaceOrientation) {
            int rotation = ([self convertInterfaceOrientation:preferredInterfaceOrientationForPresentation] + [self convertInterfaceOrientation:currentPreferredInterfaceOrientation]) % 4;
            if (rotation != 0) {
                self.currentRotation = M_PI * rotation * 0.5;
                self.presentedView.transform = CGAffineTransformMakeRotation(self.currentRotation);
            }
        }
        self.lastPreferredInterfaceOrientation = currentPreferredInterfaceOrientation;
    }else{
        [self.containerView addSubview:self.dimmingView];
        self.dimmingView.backgroundColor =[UIColor colorWithWhite:0.0 alpha:0.3];
        self.dimmingView.center = self.containerView.center;
        self.dimmingView.bounds = self.containerView.bounds;
        
        if ( @available(iOS 11.0, *) ){
            [self.containerView addSubview:self.safeFillView];
            self.safeFillView.backgroundColor = self.presentedView.backgroundColor;
        }
    }
}

- (void)presentationTransitionDidEnd:(BOOL)completed
{
    [super presentationTransitionDidEnd:completed];
    if (self.modalPresentationStyle == FSModalPresentationBottom) {
        if ([self.presentedView isKindOfClass:[UIScrollView class]]) {
            [((UIScrollView *)self.presentedView) addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
            for (int i = 0; i < self.presentedView.gestureRecognizers.count; i++) {
                UIGestureRecognizer *gestureRecognizer = self.presentedView.gestureRecognizers[i];
                if ([gestureRecognizer  isKindOfClass:NSClassFromString(@"UIScrollViewPanGestureRecognizer")])
                {
                    [gestureRecognizer addTarget:self action:@selector(handleGesture:)];
                }
            }
        }else if (self.modalPresentationStyle == FSModalPresentationCenter){
            UIPanGestureRecognizer *gesture = [self.presentedView fs_getAssociatedForKey:kTransitionPanGestureKey];
            if (gesture) {
                [self.presentedView removeGestureRecognizer:gesture];
            }
            gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
            gesture.delegate = self;
            [self.presentedView addGestureRecognizer:gesture];
            [self.presentedView fs_addAssociatedObject:gesture forKey:kTransitionPanGestureKey];
        }
    }

}

- (CGRect)frameOfPresentedViewInContainerView{
    if (self.modalPresentationStyle == FSModalPresentationFullPDFViewCtrl) return [super frameOfPresentedViewInContainerView];
    CGRect rect = self.presentedView.bounds;
    return rect;
}

- (void)dismissalTransitionWillBegin{
    [super dismissalTransitionWillBegin];
    if (self.modalPresentationStyle == FSModalPresentationFullPDFViewCtrl){
        self.presentedViewController.view.hidden = YES;
        if (self.currentRotation != 0) {
            self.presentedView.transform = CGAffineTransformIdentity;
        }
    }else{
        if (self.modalPresentationStyle == FSModalPresentationBottom){
            if ([self.presentedView isKindOfClass:[UIScrollView class]]) {
                [self.presentedView removeObserver:self forKeyPath:@"contentOffset"];
                for (int i = 0; i < self.presentedView.gestureRecognizers.count; i++) {
                    UIGestureRecognizer *gestureRecognizer = self.presentedView.gestureRecognizers[i];
                    if ([gestureRecognizer  isKindOfClass:NSClassFromString(@"UIScrollViewPanGestureRecognizer")])
                    {
                        [gestureRecognizer removeTarget:self action:@selector(handleGesture:)];
                    }
                }
            }else{
                UIPanGestureRecognizer *gesture = [self.presentedView fs_getAssociatedForKey:kTransitionPanGestureKey];
                [gesture removeTarget:self action:@selector(handleGesture:)];
                if (gesture) {
                    [self.presentedView removeGestureRecognizer:gesture];
                }
            }
        }
        [self.presentedViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
            self.dimmingView.alpha = 0.0;
            if ( @available(iOS 11.0, *) ) self.safeFillView.hidden = YES;
        } completion:nil];
    }
}

- (void)containerViewWillLayoutSubviews{
    [super containerViewWillLayoutSubviews];
    if (self.modalPresentationStyle == FSModalPresentationFullPDFViewCtrl){
        UIInterfaceOrientation preferredInterfaceOrientationForPresentation = [self preferredInterfaceOrientationForPresentation];
        UIInterfaceOrientation currentPreferredInterfaceOrientation = [self currentPreferredInterfaceOrientation];
        if (self.lastPreferredInterfaceOrientation != preferredInterfaceOrientationForPresentation) {
            if (self.preferredInterfaceOrientationForPresentation != currentPreferredInterfaceOrientation) {
                if (self.currentRotation != 0) {
                    self.presentedView.transform = CGAffineTransformIdentity;
                }
                int rotation = ([self convertInterfaceOrientation:preferredInterfaceOrientationForPresentation] + [self convertInterfaceOrientation:currentPreferredInterfaceOrientation]) % 4;
                if (rotation != 0) {
                    self.currentRotation = M_PI * rotation * 0.5;
                    self.presentedView.transform = CGAffineTransformMakeRotation(self.currentRotation);
                }
            }
        }
        self.lastPreferredInterfaceOrientation = currentPreferredInterfaceOrientation;
    }
}

- (void)containerViewDidLayoutSubviews{
    [super containerViewDidLayoutSubviews];
    if (self.modalPresentationStyle == FSModalPresentationFullPDFViewCtrl){
        CGRect presentedContainerViewFrame = [_pdfViewCtrl convertRect:_pdfViewCtrl.bounds toView:self.containerView.superview];
        self.containerView.frame = presentedContainerViewFrame;
        self.presentedView.frame = self.containerView.bounds;
    }else{
        if (_isChangeSize) return;
        self.dimmingView.center = self.containerView.center;
        self.dimmingView.bounds = self.containerView.bounds;
        if (self.modalPresentationStyle == FSModalPresentationBottom) {
            if ([self.presentedView isKindOfClass:[UIScrollView class]]) {
                ((UIScrollView *)self.presentedView).contentOffset = CGPointZero;
            }
        }
        
        if ( @available(iOS 11.0, *) ){
            [_safeFillView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(self.presentedView.fs_size).priorityMedium();
                if (self.modalPresentationStyle == FSModalPresentationCenter){
                    make.center.mas_equalTo(self.presentedView).priorityHigh();
                }else{
                    make.top.mas_equalTo(self.presentedView).priorityHigh();
                    make.bottom.mas_equalTo(self.safeFillView.superview).priorityHigh();
                    make.left.right.mas_equalTo(self.safeFillView.superview);
                }
            }];
        }
        
        if (self.presentedView.superview == self.containerView) {
            if (self.modalPresentationStyle == FSModalPresentationCenter) {
                [self.presentedView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.center.mas_equalTo(self.presentedView.superview);
                    make.size.mas_equalTo(self.presentedView.fs_size);
                }];
            }else{
                [self.presentedView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(self.presentedView.superview);
                    make.bottom.mas_equalTo(self.presentedView.superview.fs_mas_bottom).priorityHigh();
                    make.size.mas_equalTo(self.presentedView.fs_size).priorityHigh();
                    make.left.mas_equalTo(self.presentedView.superview.fs_mas_left);
                    make.right.mas_equalTo(self.presentedView.superview.fs_mas_right);
                }];
            }
        }else{
            if (self.modalPresentationStyle == FSModalPresentationCenter) {
                self.presentedView.center = self.containerView.center;
            }else{
                if (_maxVerticalHeigh < self.presentedView.bounds.size.height) {
                    _maxVerticalHeigh = self.presentedView.bounds.size.height;
                }
                CGFloat width = MIN(self.containerView.frame.size.height, self.containerView.frame.size.width);
                CGFloat heigh = (self.presentedView.bounds.size.height > (self.containerView.bounds.size.height - 20) ? (self.containerView.bounds.size.height - 20) : self.presentedView.bounds.size.height);
                if ( @available(iOS 11.0, *) ) {
                    heigh = (self.presentedView.bounds.size.height > (self.containerView.bounds.size.height - 20 - self.containerView.safeAreaInsets.top) ? (self.containerView.bounds.size.height - 20 - self.containerView.safeAreaInsets.top) : self.presentedView.bounds.size.height);
                }
                self.presentedView.bounds = CGRectMake(0, 0, width, heigh);
                self.presentedView.center = CGPointMake(self.containerView.center.x, self.containerView.frame.size.height - heigh * 0.5);
            }
        }
    }
}

- (void)callDismissViewControllerCompletion{
    !self.dismissViewControllerCompletion ? : self.dismissViewControllerCompletion();
}

- (void)handleBackgroundTap:(UIGestureRecognizer *)gestureRecognizer
{
    self.presentedViewController.transitioningDelegate = nil;
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        [self callDismissViewControllerCompletion];
    }];
}

- (void)handleGesture:(UIPanGestureRecognizer*)gestureRecognizer {
    CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view.superview];
    //NSLog(@"%s",__func__);
    if ([self.presentedView isKindOfClass:[UIScrollView class]]) {
        UIScrollView *touchView = ((UIScrollView *)self.presentedView);
        CGFloat maxHeigh = (_maxVerticalHeigh  > (self.containerView.bounds.size.height - 20) ? (self.containerView.bounds.size.height - 20) : _maxVerticalHeigh);
        if (self.presentedView.superview != self.containerView) {
            if ( @available(iOS 11.0, *) ) {
                maxHeigh = (_maxVerticalHeigh  > (self.containerView.bounds.size.height - 20 - self.containerView.safeAreaInsets.top) ? (self.containerView.bounds.size.height - 20 - self.containerView.safeAreaInsets.top) : _maxVerticalHeigh);
            }
        }
        
        maxHeigh = (maxHeigh > touchView.contentSize.height ? touchView.contentSize.height : maxHeigh);
        
        switch (gestureRecognizer.state) {
            case UIGestureRecognizerStateBegan: {
                //[touchView performSelector:@selector(handlePan:) withObject:gestureRecognizer]
                self.srcSize = self.presentedView.bounds.size;
                self.srcPoint = self.presentedView.center;
                _isChangeSize = YES;
                _isStopPanUp = NO;
                _isStopPanDown = NO;
                self.lastPoint = translation;
                break;
            }
            case UIGestureRecognizerStateChanged: {
                CGFloat dy = translation.y - self.lastPoint.y;
                if (self.presentedView.bounds.size.height >= maxHeigh) {
                    if (touchView.contentOffset.y <= 0 && dy < 0) {
                        _isChangeSize = NO;
                        break;
                    }
                    
                    if (touchView.contentOffset.y > 0) {
                        _isChangeSize = NO;
                        _isStopPanDown = YES;
                        break;
                    }
                }
                
                CGRect bounds = self.presentedView.bounds;
                if (bounds.size.height < maxHeigh && bounds.size.height - dy >= maxHeigh) {
                    bounds.size.height -= dy;
                    bounds.size.height = (bounds.size.height > maxHeigh ? maxHeigh : bounds.size.height);
                    if (self.presentedView.superview == self.containerView) {
                        [self.presentedView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.size.mas_equalTo(bounds.size).priorityHigh();
                        }];
                    }else{
                        self.presentedView.bounds = bounds;
                        self.presentedView.center = CGPointMake(self.presentedView.center.x, self.containerView.frame.size.height - bounds.size.height * 0.5);
                    }
                    
                    self.lastPoint = translation;
                    _isStopPanUp = YES;
                }
                if (_isStopPanDown || _isStopPanUp) {
                    break;
                }
                bounds.size.height -= dy;
                bounds.size.height = (bounds.size.height > maxHeigh ? maxHeigh : bounds.size.height);
                if (self.presentedView.superview == self.containerView) {
                    [self.presentedView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.size.mas_equalTo(bounds.size).priorityHigh();
                    }];
                }else{
                    self.presentedView.bounds = bounds;
                    self.presentedView.center = CGPointMake(self.presentedView.center.x, self.containerView.frame.size.height - bounds.size.height * 0.5);
                }
                self.lastPoint = translation;
                touchView.contentOffset = CGPointZero;
                //[self.presentedView.superview layoutSubviews];
                break;
            }
            case UIGestureRecognizerStateEnded:
            case UIGestureRecognizerStateCancelled:
            {
                _isChangeSize = NO;
                CGRect bounds = self.presentedView.bounds;
                if (bounds.size.height > self.srcSize.height) {
                    _isStopPanUp = YES;
                    if (maxHeigh - self.srcSize.height > 0) {
                        if ((bounds.size.height - self.srcSize.height) / (maxHeigh - self.srcSize.height) >= 0.5) {
                            bounds.size.height = maxHeigh;
                            [UIView animateWithDuration:0.3 animations:^{
                                if (self.presentedView.superview == self.containerView) {
                                    [self.presentedView mas_updateConstraints:^(MASConstraintMaker *make) {
                                        make.size.mas_equalTo(bounds.size).priorityHigh();
                                    }];
                                }else{
                                    self.presentedView.bounds = bounds;
                                    self.presentedView.center = CGPointMake(self.presentedView.center.x, self.containerView.frame.size.height - bounds.size.height * 0.5);
                                }

                            }];
                        }else{
                            [UIView animateWithDuration:0.3 animations:^{
                                if (self.presentedView.superview == self.containerView) {
                                    [self.presentedView mas_updateConstraints:^(MASConstraintMaker *make) {
                                        make.size.mas_equalTo(self.srcSize).priorityHigh();
                                    }];
                                }else{
                                    self.presentedView.bounds = CGRectMake(0, 0, self.srcSize.width, self.srcSize.height);
                                    self.presentedView.center = self.srcPoint;
                                }
                            }];
                        }
                    }
                }
                
                if (bounds.size.height < self.srcSize.height) {
                    if (self.srcSize.height > 0) {
                        if ((self.srcSize.height - bounds.size.height) / self.srcSize.height >= 0.5) {
                            self.presentedViewController.transitioningDelegate = nil;
                            [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
                                [self callDismissViewControllerCompletion];
                            }];
                        }else{
                            [UIView animateWithDuration:0.3 animations:^{
                                if (self.presentedView.superview == self.containerView) {
                                    [self.presentedView mas_updateConstraints:^(MASConstraintMaker *make) {
                                        make.size.mas_equalTo(self.srcSize).priorityHigh();
                                    }];
                                }else{
                                    self.presentedView.bounds = CGRectMake(0, 0, self.srcSize.width, self.srcSize.height);
                                    self.presentedView.center = self.srcPoint;
                                }
                            }];
                        }
                    }
                }
                //[self.presentedView.superview layoutSubviews];
                break;
            }
            default:
                _isChangeSize = NO;
                break;
        }
    }else{
        
        CGFloat maxHeigh = (_maxVerticalHeigh  > (self.containerView.bounds.size.height - 20) ? (self.containerView.bounds.size.height - 20) : _maxVerticalHeigh);
        if ( @available(iOS 11.0, *) ) {
            maxHeigh = (_maxVerticalHeigh  > (self.containerView.bounds.size.height - 20 - self.containerView.safeAreaInsets.top) ? (self.containerView.bounds.size.height - 20 - self.containerView.safeAreaInsets.top) : _maxVerticalHeigh);
        }
        
        switch (gestureRecognizer.state) {
            case UIGestureRecognizerStateBegan: {
                //[touchView performSelector:@selector(handlePan:) withObject:gestureRecognizer]
                self.srcPoint = self.presentedView.center;
                _isChangeSize = YES;
                self.lastPoint = translation;
                break;
            }
            case UIGestureRecognizerStateChanged: {
                CGFloat dy = translation.y - self.lastPoint.y;
                if (self.presentedView.center.y + dy < self.srcPoint.y) {
                    break;
                }
                self.presentedView.center = CGPointMake(self.srcPoint.x, self.presentedView.center.y + dy);
                self.lastPoint = translation;
                //[self.presentedView.superview layoutSubviews];
                break;
            }
            case UIGestureRecognizerStateEnded:
            case UIGestureRecognizerStateCancelled:
            {
                _isChangeSize = NO;
                CGPoint curPoint = self.presentedView.center;
                if (curPoint.y - self.srcPoint.y > maxHeigh * 0.5) {
                    self.presentedViewController.transitioningDelegate = nil;
                    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
                        [self callDismissViewControllerCompletion];
                    }];
                }else{
                    [UIView animateWithDuration:0.3 animations:^{
                        self.presentedView.center = self.srcPoint;
                    }];
                }
                //[self.presentedView.superview layoutSubviews];
                break;
            }
            default:
                _isChangeSize = NO;
                break;
        }
    }
}

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<NSString*, id> *)change context:(nullable void *)context{

    if([keyPath isEqualToString:@"contentOffset"])
    {
        CGPoint offset = [[change objectForKey:@"new"] CGPointValue];
        if (offset.y > 0 && _isStopPanUp) {
            ((UIScrollView *)(self.presentedView)).contentOffset = CGPointZero;
        }
        
        if (offset.y < 0) {
            ((UIScrollView *)(self.presentedView)).contentOffset = CGPointZero;
        }
    }
}

- (UIView *)dimmingView{
    if (!_dimmingView) {
        _dimmingView = [[UIView alloc] init];
        UITapGestureRecognizer *backgroundTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBackgroundTap:)];
        [_dimmingView addGestureRecognizer:backgroundTapRecognizer];
        
        if ( @available(iOS 11.0, *) ) _safeFillView = [[UIView alloc] init];
    }
    return _dimmingView;
}

- (int)convertInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
            return 0;
        case UIInterfaceOrientationLandscapeRight:
            return 1;
        case UIInterfaceOrientationPortraitUpsideDown:
            return 2;
        case UIInterfaceOrientationLandscapeLeft:
            return 3;
        default:
            return 0;
    }
}

@end

@interface FSPresentedVCAnimator ()
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@end
@implementation FSPresentedVCAnimator

- (instancetype)init{
    return [[FSPresentedVCAnimator alloc] initWithPdfViewCtrl:nil];
}

- (instancetype)initWithPdfViewCtrl:(nullable FSPDFViewCtrl *)pdfViewCtrl{
    self = [super init];
    if (self) {
        _presentationStyle = FSModalPresentationFullScreen;
        if (pdfViewCtrl) {
            _presentationStyle = FSModalPresentationFullPDFViewCtrl;
            _pdfViewCtrl = pdfViewCtrl;
        }
    }
    return self;
}

- (instancetype)initWithVerticalHeigh:(CGFloat)verticalHeigh presentationStyle:(FSModalPresentationStyle)modalPresentationStyle{
    self = [super init];
    if (self) {
        _presentationStyle = modalPresentationStyle;
        _maxVerticalHeigh = verticalHeigh;
    }
    return self;
}
- (instancetype)initWithSize:(CGSize)size presentationStyle:(FSModalPresentationStyle)presentationStyle{
    self = [super init];
    if (self) {
        _presentationStyle = presentationStyle;
        _size = size;
    }
    return self;
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    FSPresentationController *presentation = [[FSPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    presentation.dismissViewControllerCompletion = [self.dismissViewControllerCompletion copy];
    presented.view.bounds = CGRectMake(0, 0, _size.width, _size.height);
    switch (self.presentationStyle) {
        case FSModalPresentationFullPDFViewCtrl:
            presented.view.bounds = CGRectMake(0, 0, self.pdfViewCtrl.fs_width, self.pdfViewCtrl.fs_height);
            presentation.pdfViewCtrl = self.pdfViewCtrl;
            break;
        case FSModalPresentationCenter:
            presentation.size = self.size;
            break;
        case FSModalPresentationBottom:
            presentation.maxVerticalHeigh = self.maxVerticalHeigh;
            break;
        default:
            return nil;
            break;
    }
    presentation.modalPresentationStyle = self.presentationStyle;
    return presentation;
}

@end

