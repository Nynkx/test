/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <objc/runtime.h>
@interface UIViewController ()
@property (nonatomic, strong) FSPresentedVCAnimator *fs_animator;
@end
@implementation UIViewController (FSTransition)

YYSYNTH_DYNAMIC_PROPERTY_OBJECT(fs_animator, setFs_animator, RETAIN, FSPresentedVCAnimator *)

- (void)fs_makeTransitionAnimator:(void(^)(FSPresentedVCAnimator *animator))block{
    FSPresentedVCAnimator *animator = [[FSPresentedVCAnimator alloc] init];
    block(animator);
    [self fs_setCustomTransitionAnimator:animator];
    self.fs_animator = animator;
}

- (void)fs_setCustomTransitionAnimator:(FSPresentedVCAnimator *)animator{
    if (!animator) return;
    // use UIModalPresentationFullScreen
    if (animator.presentationStyle == FSModalPresentationFullScreen || ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && animator.presentationStyle == FSModalPresentationFullPDFViewCtrl)) {
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        return;
    }
    self.modalTransitionStyle = 0;
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = animator;
    self.modalPresentationCapturesStatusBarAppearance = YES;
}

@end
