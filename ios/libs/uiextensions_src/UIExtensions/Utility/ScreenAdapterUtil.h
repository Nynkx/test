/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <CoreGraphics/CGBase.h>

#if defined(__cplusplus)
#define SCREEN_EXTERN extern "C"
#else
#define SCREEN_EXTERN extern
#endif

typedef CGFLOAT_TYPE SCREEN_TYPE;

#ifndef iPhone4
#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#endif

#ifndef iPhone5
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#endif

#ifndef iPhone6 //equal to iPhone7,8
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size)) : NO)
#endif

#ifndef iPhone6plus //equal to iPhone7,8plus
#define iPhone6plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)
#endif

#ifndef iPhone6plusLarge
#define iPhone6plusLarge ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) : NO)
#endif

#ifndef iPhoneX //equal to iPhoneXS
#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#endif

#ifndef iPhoneXR
#define iPhoneXR ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)
#endif

#ifndef iPhoneXMax
#define iPhoneXMax ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)
#endif

SCREEN_EXTERN const SCREEN_TYPE PREFERENCE_CELL_ROW_HEIGHT;
SCREEN_EXTERN const SCREEN_TYPE IPAD_PREFERENCE_BAR_WIDTH;
SCREEN_EXTERN const SCREEN_TYPE STATUS_BAR_HEIGHT_NORMAL;
SCREEN_EXTERN const SCREEN_TYPE NAVIGATION_BAR_HEIGHT_NORMAL;
SCREEN_EXTERN const SCREEN_TYPE NAVIGATION_HEIGHT_NORMAL;
SCREEN_EXTERN const SCREEN_TYPE TAB_BAR_HEIGHT_NORMAL;
SCREEN_EXTERN const SCREEN_TYPE ITEM_MARGIN_SMALL;
SCREEN_EXTERN const SCREEN_TYPE ITEM_MARGIN_LITTLE_NORMAL;;
SCREEN_EXTERN const SCREEN_TYPE ITEM_MARGIN_NORMAL;
SCREEN_EXTERN const SCREEN_TYPE ITEM_MARGIN_MEDIUM;
SCREEN_EXTERN const SCREEN_TYPE DIVIDE_TOOL_BAR_THICKNES;
SCREEN_EXTERN const SCREEN_TYPE DIVIDE_VIEW_THICKNES;
SCREEN_EXTERN const SCREEN_TYPE VIEW_TEMPORARY_SIZE_VALUE;
SCREEN_EXTERN const SCREEN_TYPE TOOLBAR_ITEM_INVALID_MARGIN_VALUE;

#ifndef isiPhoneXSet
#define isiPhoneXSet (iPhoneX == YES || iPhoneXR == YES || iPhoneXMax == YES)
#endif

#ifndef STATUS_BAR_HEIGHT //44.f or 20.f
#define STATUS_BAR_HEIGHT (isiPhoneXSet ? 44.f : STATUS_BAR_HEIGHT_NORMAL)
#endif

#ifndef NAVGATION_BAR_HEIGHT //44.f or 44.f
#define NAVGATION_BAR_HEIGHT (isiPhoneXSet ? 44.f : NAVIGATION_BAR_HEIGHT_NORMAL)
#endif

#ifndef TAB_BAR_SAFE_BOTTOM_MARGIN //
#define TAB_BAR_SAFE_BOTTOM_MARGIN (isiPhoneXSet ? 34.f  : 0)
#endif

#ifndef NAVIGATION_HEIGHT //88.f or 64.f
#define NAVIGATION_HEIGHT (STATUS_BAR_HEIGHT + NAVGATION_BAR_HEIGHT)
#endif

#ifndef TAB_BAR_HEIGHT //83.f or 49.f
#define TAB_BAR_HEIGHT (TAB_BAR_SAFE_BOTTOM_MARGIN + TAB_BAR_HEIGHT_NORMAL)
#endif


NS_ASSUME_NONNULL_BEGIN

@interface UIView (MasonrySafeAreaLayout)

@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_leading;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_trailing;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_left;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_right;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_top;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_bottom;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_width ;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_height;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_centerX;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_centerY ;

@end

NS_ASSUME_NONNULL_END
