/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSWeakPointerArray.h"

@interface FSWeakPointerArray ()
@property (nonatomic, strong) NSPointerArray *weakPointer;
@end
@implementation FSWeakPointerArray

+ (FSWeakPointerArray *)array{
    return [[FSWeakPointerArray alloc] init];
}

- (FSWeakPointerArray *)init{
    self = [super init];
    if (self) {
        _weakPointer = [NSPointerArray weakObjectsPointerArray];
    }
    return self;
}

- (void)addObject:(id)anObject{
    [self.weakPointer compact];
    [self.weakPointer addPointer:(__bridge void *)(anObject)];
}

- (void)removeObjectAtIndex:(NSUInteger)index{
    [self.weakPointer compact];
    [self.weakPointer removePointerAtIndex:index];
}

- (void)removeObject:(id)anObject{
    NSUInteger count = self.weakPointer.count;
    for(int i = 0; i < count; i++) {
        if((__bridge void *)(anObject) == [self.weakPointer pointerAtIndex:i]) {
            [self removeObjectAtIndex:i];
            break;
        }
    }
}

- (void)removeAllObjects{
    NSUInteger count = self.weakPointer.count;
    for (int i = 0; i < count; i++) {
        [self removeObjectAtIndex:0];
    }
}

- (NSUInteger)count{
    return self.weakPointer.count;
}

- (id)objectAtIndex:(NSUInteger)index{
    return [self.weakPointer pointerAtIndex:index];
}

- (BOOL)containsObject:(id)anObject{
    return [[self.weakPointer allObjects] containsObject:anObject];
}

- (void)enumerateObjectsUsingBlock:(void (NS_NOESCAPE ^)(id obj, NSUInteger idx, BOOL *stop))block {
    [[self.weakPointer allObjects] enumerateObjectsUsingBlock:block];
}

#pragma mark - NSFastEnumeration
- (NSUInteger)countByEnumeratingWithState:(nonnull NSFastEnumerationState *)state objects:(id  _Nullable __unsafe_unretained * _Nonnull)buffer count:(NSUInteger)len {
    return [[self.weakPointer allObjects] countByEnumeratingWithState:state objects:buffer count:len];
}

@end
