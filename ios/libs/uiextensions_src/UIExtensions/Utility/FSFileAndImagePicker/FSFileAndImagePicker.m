/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSFileAndImagePicker.h"
#import "Const.h"
#import "PrivateDefine.h"
#import "FileSelectDestinationViewController.h"
#import "PhotoToPDF/PhotoToPDFViewController.h"
#import "FSMediaFileToPDFViewController.h"
#import "FSAudioRecorderController.h"

#import "AudioPlayerControl.h"
#import "InsertPageConfigVCtrl.h"

@interface FSFileAndImagePicker () <PhotoToPDFViewControllerDelegate, FSMediaFileToPDFViewControllerDelegate, FSAVAudioRecorderViewControllerDelegate>

@property (nonatomic, weak) UIViewController *rootViewController;
@property (nonatomic, copy) NSArray<NSString *> *imageTypes;
@property (nonatomic, copy) NSArray<NSString *> *audioTypes;
@property (nonatomic, copy) NSArray<NSString *> *videoTypes;
@end

@implementation FSFileAndImagePicker

- (id)init {
    if (self = [super init]) {
        _imageTypes = @[ @"jbig2", @"jpx", @"tif", @"gif", @"png", @"jpg", @"bmp", @"jpeg" ]; // image types by default
        _audioTypes = @[ @"mp3", @"caf", @"aac", @"wav", @"wma", @"cda", @"flac", @"m4a", @"mid", @"mka", @"mp2", @"mpa", @"mpc", @"ape", @"ofr", @"ogg", @"ra", @"wv", @"tta", @"ac3", @"dts" ]; // audio
        _videoTypes = @[ @"avi", @"ivf", @"wmp", @"wm", @"wvx", @"wmx", @"asf", @"asx", @"mp2", @"swf", @"mpeg", @"mov", @"mp4" ]; // video
        self.fileType = FSMediaFileTypeImage;
    }
    return self;
}

- (void)setFileType:(FSMediaFileType)fileType{
    _fileType = fileType;
    switch (fileType) {
        case FSMediaFileTypeAudio:
            _expectedFileTypes = _audioTypes;
            break;
            
        case FSMediaFileTypeVideo:
            _expectedFileTypes = _videoTypes;
            break;
        case FSMediaFileTypeImage:
            _expectedFileTypes = _imageTypes;
            break;
        default:
            _expectedFileTypes = nil;
            break;
    }
}

- (void)presentInRootViewController:(UIViewController *)rootViewController fromView:(UIView *_Nullable)view {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:DEVICE_iPHONE ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             [self.delegate fileAndImagePickerDidCancel:self];
                                                         }];
    
    UIAlertAction *pageAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kBlankPage")
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"InsertPage" bundle:[NSBundle bundleForClass:[self class]]];
                                                            UINavigationController *nav = [storyboard instantiateInitialViewController];
                                                            nav.modalPresentationStyle = UIModalPresentationFullScreen;
                                                            InsertPageConfigVCtrl *VC = (InsertPageConfigVCtrl *)nav.topViewController;
                                                            [VC setUIExtensionsManager:self.extensionsManager];
                                                            VC.delegate = self.insertPageDelegate;
                                                            [rootViewController presentViewController:nav animated:YES completion:nil];
                                                        }];
    
    
    UIAlertAction *fileAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromDocument")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:NO completion:nil];
                                                           
                                                           FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
                                                           selectDestination.isRootFileDirectory = YES;
                                                           selectDestination.fileOperatingMode = FileListMode_Import;
                                                           selectDestination.expectFileType = self.expectedFileTypes;
                                                           [selectDestination loadFilesWithPath:DOCUMENT_PATH];
                                                           selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
                                                               [controller dismissViewControllerAnimated:YES completion:^{
                                                                   if (destinationFolder.count > 0) {
                                                                       NSString *srcPath = destinationFolder[0];
                                                                       if ([self.imageTypes containsObject:srcPath.pathExtension.lowercaseString]) {
                                                                           UIImage *image = [UIImage imageWithContentsOfFile:srcPath];
                                                                           if (image) {
                                                                               [self.delegate fileAndImagePicker:self didPickImage:image imagePath:srcPath];
                                                                           }
                                                                       } else {
                                                                           [self.delegate fileAndImagePicker:self didPickFileAtPath:srcPath];
                                                                       }
                                                                   }
                                                               }];
                                                           };
                                                           selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
                                                               [controller dismissViewControllerAnimated:YES completion:^{
                                                                   [self.delegate fileAndImagePickerDidCancel:self];
                                                               }];
                                                           };
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
                                                               [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
                                                               [rootViewController presentViewController:selectDestinationNavController animated:YES completion:nil];
                                                           });
                                                       }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromAlbum")
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            PhotoToPDFViewController *photoController = [[PhotoToPDFViewController alloc] initWithButton:(UIButton*)view];
                                                            photoController.delegate = self;
                                                            [rootViewController presentViewController:photoController animated:NO completion:nil];
                                                            [photoController openAlbum];
                                                        }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromCamera")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             PhotoToPDFViewController *photoController = [[PhotoToPDFViewController alloc] initWithButton:(UIButton*)view];
                                                             photoController.delegate = self;
                                                             [rootViewController presentViewController:photoController animated:NO completion:nil];
                                                             [photoController openCamera];
                                                         }];
    
    [alert addAction:pageAction];
    [alert addAction:cancelAction];
    [alert addAction:fileAction];
    [alert addAction:photoAction];
    [alert addAction:cameraAction];
    
    //    if (view) {
    //        alert.popoverPresentationController.sourceView = view;
    //        alert.popoverPresentationController.sourceRect = view.bounds;
    //    }
    [rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)presentInRootViewController:(UIViewController *)rootViewController{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:DEVICE_iPHONE ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             [self.delegate fileAndImagePickerDidCancel:self];
                                                         }];
    UIAlertAction *fileAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromDocument")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:NO completion:nil];
                                                           
                                                           FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
                                                           selectDestination.isRootFileDirectory = YES;
                                                           selectDestination.fileOperatingMode = FileListMode_Import;
                                                           selectDestination.expectFileType = self.expectedFileTypes;
                                                           [selectDestination loadFilesWithPath:DOCUMENT_PATH];
                                                           selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
                                                               [controller dismissViewControllerAnimated:YES completion:^{
                                                                   if (destinationFolder.count > 0) {
                                                                       NSString *srcPath = destinationFolder[0];
                                                                       if ([self.imageTypes containsObject:srcPath.pathExtension.lowercaseString]) {
                                                                           UIImage *image = [UIImage imageWithContentsOfFile:srcPath];
                                                                           if (image) {
                                                                               [self.delegate fileAndImagePicker:self didPickImage:image imagePath:srcPath];
                                                                           }
                                                                       } else {
                                                                           [self.delegate fileAndImagePicker:self didPickFileAtPath:srcPath];
                                                                       }
                                                                   }
                                                               }];
                                                           };
                                                           selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
                                                               [controller dismissViewControllerAnimated:YES completion:^{
                                                                   [self.delegate fileAndImagePickerDidCancel:self];
                                                               }];
                                                           };
                                                           FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
                                                           [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
                                                           [rootViewController presentViewController:selectDestinationNavController animated:YES completion:nil];
                                                       }];
    
    [alert addAction:cancelAction];
    [alert addAction:fileAction];
    
    _rootViewController = rootViewController;
    if (self.fileType == FSMediaFileTypeAudio) {
        [self audioFileAddWithAlert:alert];
    }else if (self.fileType == FSMediaFileTypeVideo) {
        [self videoFileAddWithAlert:alert];
    }else if (self.fileType == FSMediaFileTypeImage){
        [self imageFileAddWithAlert:alert];
    }
    else{
        [self defaultFileAddWithAlert:alert];
    }
}

- (void)audioFileAddWithAlert:(UIAlertController *)alert{
    
    UIAlertAction *recordAudioAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kRecordAudio")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             [[AudioPlayerControl sharedControl] dismiss];
                                                             self.rootViewController.definesPresentationContext = YES;
                                                             FSAudioRecorderController *presentViewController = [[FSAudioRecorderController alloc] init];
                                                             presentViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                                                             presentViewController.view.backgroundColor = [UIColor clearColor];
                                                             presentViewController.delegate = self;
                                                             [self.rootViewController presentViewController:presentViewController animated:NO completion:nil];
                                                         }];
    
    [alert addAction:recordAudioAction];

    [_rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)videoFileAddWithAlert:(UIAlertController *)alert{
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromAlbum")
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            FSMediaFileToPDFViewController *mediaFileToPDFViewController = self.mediaFileToPDFViewController;
                                                            [self.rootViewController presentViewController:mediaFileToPDFViewController animated:NO completion:nil];
                                                            [mediaFileToPDFViewController openAlbum];
                                                        }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kRecordVideo")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             FSMediaFileToPDFViewController *mediaFileToPDFViewController = self.mediaFileToPDFViewController;
                                                             [self.rootViewController presentViewController:mediaFileToPDFViewController animated:NO completion:nil];
                                                             [mediaFileToPDFViewController openCamera];
                                                         }];
    
    [alert addAction:photoAction];
    [alert addAction:cameraAction];
    
    [_rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)imageFileAddWithAlert:(UIAlertController *)alert{
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromAlbum")
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            PhotoToPDFViewController *photoController = [[PhotoToPDFViewController alloc] initWithButton:nil];
                                                            photoController.delegate = self;
                                                            [self.rootViewController presentViewController:photoController animated:NO completion:nil];
                                                            [photoController openAlbum];
                                                        }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromCamera")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             PhotoToPDFViewController *photoController = [[PhotoToPDFViewController alloc] initWithButton:nil];
                                                             photoController.delegate = self;
                                                             [self.rootViewController presentViewController:photoController animated:NO completion:nil];
                                                             [photoController openCamera];
                                                         }];
    [alert addAction:photoAction];
    [alert addAction:cameraAction];
    
    [_rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)defaultFileAddWithAlert:(UIAlertController *)alert{
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromAlbum")
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            FSMediaFileToPDFViewController *mediaFileToPDFViewController = self.mediaFileToPDFViewController;
                                                            [self.rootViewController presentViewController:mediaFileToPDFViewController animated:NO completion:nil];
                                                            [mediaFileToPDFViewController openAlbum];
                                                        }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFromCamera")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             FSMediaFileToPDFViewController *mediaFileToPDFViewController = self.mediaFileToPDFViewController;
                                                             [self.rootViewController presentViewController:mediaFileToPDFViewController animated:NO completion:nil];
                                                             [mediaFileToPDFViewController openCamera];
                                                         }];
    
    [alert addAction:photoAction];
    [alert addAction:cameraAction];
    
    //    if (view) {
    //        alert.popoverPresentationController.sourceView = view;
    //        alert.popoverPresentationController.sourceRect = view.bounds;
    //    }
    [_rootViewController presentViewController:alert animated:YES completion:nil];
}

- (FSMediaFileToPDFViewController *)mediaFileToPDFViewController{
    [[AudioPlayerControl sharedControl] dismiss];
    FSMediaFileToPDFViewController *mediaFileToPDFViewController = [[FSMediaFileToPDFViewController alloc] init];
    mediaFileToPDFViewController.fileType = self.fileType;
    mediaFileToPDFViewController.delegate = self;
    return mediaFileToPDFViewController;
}

#pragma mark <PhotoToPDFViewControllerDelegate>

- (void)photoToPDFViewController:(PhotoToPDFViewController *)photoToPDFViewController didSelectImage:(UIImage *)image {
    [self.delegate fileAndImagePicker:self didPickImage:image imagePath:nil];
}

- (void)photoToPDFViewControllerDidCancel:(PhotoToPDFViewController *)photoToPDFViewController {
    [self.delegate fileAndImagePickerDidCancel:self];
}

#pragma mark <FSMediaFileToPDFViewControllerDelegate>

- (void)mediaFileToPDFViewController:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController didPickFileAtPath:(NSString *)filePath{
    if (self.delegate) {
        [self.delegate fileAndImagePicker:self didPickFileAtPath:filePath];
    }
}

- (void)mediaFileToPDFViewController:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController didSelectVideo:(AVPlayer *)video{
    if (self.delegate) {
        [self.delegate fileAndImagePicker:self didSelectVideo:video];
    }
}

- (void)mediaFileToPDFViewController:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController didSelectImage:(UIImage *)image{
     [self.delegate fileAndImagePicker:self didPickImage:image imagePath:nil];
}
- (void)mediaFileToPDFViewControllerDidCancel:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController{
     [self.delegate fileAndImagePickerDidCancel:self];
}
#pragma mark <FSAVAudioRecorderViewControllerDelegate>

- (void)audioRecorderViewController:(FSAudioRecorderController *)audioRecorderViewController didRecordFileAtPath:(NSString *)filePath{
    if (self.delegate) {
        [self.delegate fileAndImagePicker:self didPickFileAtPath:filePath];
    }
}
- (void)audioRecorderViewControllerDidCancel:(FSAudioRecorderController *)audioRecorderViewController{
    [self.delegate fileAndImagePickerDidCancel:self];
}

@end
