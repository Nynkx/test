/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "FSAudioRecorderController.h"
#import "PrivateDefine.h"
#import <AVFoundation/AVFoundation.h>
#import <pthread.h>

@interface _FS_Timer : NSObject

+ (_FS_Timer *)timerWithTimeInterval:(NSTimeInterval)interval
                            target:(id)target
                          selector:(SEL)selector
                           repeats:(BOOL)repeats;
- (instancetype)initWithFireTime:(NSTimeInterval)start
                        interval:(NSTimeInterval)interval
                          target:(id)target
                        selector:(SEL)selector
                         repeats:(BOOL)repeats NS_DESIGNATED_INITIALIZER;
@property (readonly) BOOL repeats;
@property (readonly) NSTimeInterval timeInterval;
@property (readonly, getter=isValid) BOOL valid;
- (void)invalidate;
- (void)fire;
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;
@end

@interface FSAudioRecorderController ()<AVAudioRecorderDelegate>
@property (nonatomic, strong) AVAudioRecorder *audioRecorder;
@property (nonatomic, assign) BOOL iscCancelRecord;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *textLab;
@property (nonatomic, assign) int second;
@property (nonatomic, assign) BOOL isRecording;
@property (nonatomic, strong) _FS_Timer *timer;
@end

@implementation FSAudioRecorderController

- (AVAudioRecorder *)audioRecorder{
    if (!_audioRecorder) {
        NSURL *url = [self getSavePath];
        NSError *error = nil;
        NSDictionary *setting = [self getAudioSetting];
        _audioRecorder = [[AVAudioRecorder alloc]initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate = self;
        if (error) {
            NSLog(@"init audio_recorder error，error info：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioRecorder;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.iscCancelRecord = NO;
    self.second = 0;
    
    [self setSession];

    UIButton *recordContainerView = [[UIButton alloc] init];
    recordContainerView.layer.masksToBounds = YES;
    recordContainerView.layer.cornerRadius = 6.f;
    recordContainerView.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.68];
    [recordContainerView addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *textLab = [[UILabel alloc] init];
    textLab.font = [UIFont systemFontOfSize:(self.recordContainerViewWidth/6.f)];
    textLab.textColor = WhiteThemeTextColor;
    textLab.text = FSLocalizedForKey(@"KStart");
    self.textLab = textLab;
    
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = ImageNamed(@"add_audio_t");
    self.imageView = imageView;
    
    [recordContainerView addSubview:textLab];
    [recordContainerView addSubview:imageView];
    [self.view addSubview:recordContainerView];
    
    [recordContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.view).offset(-20);
        make.centerX.equalTo(self.view);
        make.width.height.offset(self.recordContainerViewWidth);
    }];
     
     [textLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerX.equalTo(recordContainerView);
         make.top.offset(self.recordContainerViewWidth/9.f);
         make.height.offset(2 * self.recordContainerViewWidth/9.f);
     }];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(recordContainerView);
        make.height.offset(2 * self.recordContainerViewWidth/5.f);
        make.bottom.offset(- self.recordContainerViewWidth/9.f);
    }];
}

- (void)setSession{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [audioSession setActive:YES error:nil];
}

- (void)startRecord:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        self.imageView.image = ImageNamed(@"tingzhi");
        self.isRecording = YES;
        _FS_Timer *timer = [[_FS_Timer alloc] initWithFireTime:1 interval:1 target:self selector:@selector(timeInterval:) repeats:YES];
        self.timer = timer;
        [self.timer fire];
        [self.audioRecorder record];
    }else{
        [self.audioRecorder stop];
        self.isRecording = NO;
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)timeInterval:(_FS_Timer *)timer{
    int seconds = self.second % 60;
    int minutes = (self.second / 60) % 60;
    int hours = self.second / 3600;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textLab.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    });
    if (!self.isRecording) {
        [timer invalidate];
        return;
    }
    self.second ++;
}

- (NSURL *)getSavePath{
    NSString *dateStr = ({
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter stringFromDate:date];
    });
    NSString *urlStr = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    urlStr = [urlStr stringByAppendingPathComponent:[NSString stringWithFormat:@"my_record_%@.wav",dateStr]];
    NSURL *url = [NSURL fileURLWithPath:urlStr];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL blHave = [fileManager fileExistsAtPath:urlStr];
    if (blHave) {
        NSLog(@"file already existed");
        BOOL result = [fileManager removeItemAtURL:url error:nil];
        if (result) {
            NSLog(@"file delete succeeded");
        }
    }
    return url;
}

- (NSDictionary *)getAudioSetting{
    NSMutableDictionary* recordSetting = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithFloat:16000], AVSampleRateKey,
                                          [NSNumber numberWithInt:kAudioFormatLinearPCM],AVFormatIDKey,
                                          [NSNumber numberWithInt:1], AVNumberOfChannelsKey,
                                          [NSNumber numberWithInt:16], AVLinearPCMBitDepthKey,
                                          [NSNumber numberWithBool:NO],AVLinearPCMIsBigEndianKey,
                                          [NSNumber numberWithBool:NO],AVLinearPCMIsFloatKey,
                                          nil];
    return recordSetting;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    if (!self.isRecording) {
        self.view.hidden = YES;
        self.iscCancelRecord = YES;
        [self.audioRecorder stop];
        if (self.delegate) {
            [self.delegate audioRecorderViewControllerDidCancel:self];
        }
        NSFileManager* fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtURL:self.getSavePath error:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (CGFloat)recordContainerViewWidth{
    if (DEVICE_iPHONE) {
        CGFloat W = self.view.frame.size.width/4;
        CGFloat H = self.view.frame.size.height/4;
        if (W > H) {
            return H;
        }
        return W;
    }
    return 110.f;
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
    if (flag) {
        if (self.delegate && !self.iscCancelRecord) {
            [self.delegate audioRecorderViewController:self didRecordFileAtPath:recorder.url.path];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSFileManager *fileManager = [NSFileManager defaultManager];
                BOOL blHave = [fileManager fileExistsAtPath:self.getSavePath.path];
                if (blHave) {
                    NSLog(@"file already existed");
                    BOOL result = [fileManager removeItemAtURL:self.getSavePath error:nil];
                    if (result) {
                        NSLog(@"file delete succeeded");
                    }
                }
            });
        }
    }
}

@end

#define LOCK(...) dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER); \
__VA_ARGS__; \
dispatch_semaphore_signal(_lock);


@implementation _FS_Timer {
    BOOL _valid;
    NSTimeInterval _timeInterval;
    BOOL _repeats;
    __weak id _target;
    SEL _selector;
    dispatch_source_t _source;
    dispatch_semaphore_t _lock;
}

+ (_FS_Timer *)timerWithTimeInterval:(NSTimeInterval)interval
                            target:(id)target
                          selector:(SEL)selector
                           repeats:(BOOL)repeats {
    return [[self alloc] initWithFireTime:interval interval:interval target:target selector:selector repeats:repeats];
}

- (instancetype)initWithFireTime:(NSTimeInterval)start
                        interval:(NSTimeInterval)interval
                          target:(id)target
                        selector:(SEL)selector
                         repeats:(BOOL)repeats {
    self = [super init];
    _repeats = repeats;
    _timeInterval = interval;
    _valid = YES;
    _target = target;
    _selector = selector;
    
    __weak typeof(self) _self = self;
    _lock = dispatch_semaphore_create(1);
    _source = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(_source, dispatch_time(DISPATCH_TIME_NOW, (start * NSEC_PER_SEC)), (interval * NSEC_PER_SEC), 0);
    dispatch_source_set_event_handler(_source, ^{[_self fire];});
    dispatch_resume(_source);
    return self;
}

- (void)invalidate {
    dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER);
    if (_valid) {
        dispatch_source_cancel(_source);
        _source = NULL;
        _target = nil;
        _valid = NO;
    }
    dispatch_semaphore_signal(_lock);
}

- (void)fire {
    if (!_valid) return;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER);
    id target = _target;
    if (!target) {
        dispatch_semaphore_signal(_lock);
        [self invalidate];
    } else {
        dispatch_semaphore_signal(_lock);
        [target performSelector:_selector withObject:self];
        if (!_repeats) {
            [self invalidate];
        }
    }
#pragma clang diagnostic pop
}

- (BOOL)repeats {
    LOCK(BOOL repeat = _repeats); return repeat;
}

- (NSTimeInterval)timeInterval {
    LOCK(NSTimeInterval t = _timeInterval) return t;
}

- (BOOL)isValid {
    LOCK(BOOL valid = _valid) return valid;
}

- (void)dealloc {
    [self invalidate];
}

@end
