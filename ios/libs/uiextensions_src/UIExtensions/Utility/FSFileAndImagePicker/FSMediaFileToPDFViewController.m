/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "FSMediaFileToPDFViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Utility.h"

@interface FSMediaFileToPDFViewController ()

@property (nonatomic, strong) UIImagePickerController *mediaController;
@property (nonatomic) BOOL canPick;

@end

@implementation FSMediaFileToPDFViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        self.mediaController = [[UIImagePickerController alloc] init];
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        self.mediaController.delegate = self;
    }
    return self;
}

- (void)openAlbum {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        self.mediaController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        if (self.fileType == FSMediaFileTypeVideo) {
            self.mediaController.mediaTypes=@[(NSString *)kUTTypeMovie];
        }

        [self.view addSubview:self.mediaController.view];
        [self addChildViewController:self.mediaController];
        [self.mediaController didMoveToParentViewController:self];
        
        self.canPick = true;
    } else {
        [[UIApplication sharedApplication].keyWindow.rootViewController dismissViewControllerAnimated:NO completion:nil];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFailed") message:FSLocalizedForKey(@"kFailedOpenAlbum") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        [alertController addAction:cancelAction];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)openCamera {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.mediaController.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.mediaController.showsCameraControls = YES;
        if (self.fileType == FSMediaFileTypeVideo) {
            self.mediaController.mediaTypes=@[(NSString *)kUTTypeMovie];
            self.mediaController.videoQuality=UIImagePickerControllerQualityTypeIFrame1280x720;
            self.mediaController.cameraCaptureMode=UIImagePickerControllerCameraCaptureModeVideo;
        }
        [self.view addSubview:self.mediaController.view];
        [self addChildViewController:self.mediaController];
        [self.mediaController didMoveToParentViewController:self];
        
        self.canPick = true;
    } else {
        [[UIApplication sharedApplication].keyWindow.rootViewController dismissViewControllerAnimated:NO completion:nil];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFailed") message:FSLocalizedForKey(@"kFailedOpenCamera") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        [alertController addAction:cancelAction];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    BOOL canPick = self.canPick;
    self.canPick = false;
    if (canPick) {
        if (self.delegate) {
            if (self.fileType == FSMediaFileTypeVideo) {
                NSURL *url = (NSURL *) [info objectForKey:UIImagePickerControllerMediaURL];;
                NSString *urlStr = [url path];
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(urlStr)) {
                    if (self.delegate){
                        [self.delegate mediaFileToPDFViewController:self didPickFileAtPath:url.path];
                    }
                }
            }else{
                UIImage *uiImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
                [self.delegate mediaFileToPDFViewController:self didSelectImage:uiImage];
            }
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[UIApplication sharedApplication].keyWindow.rootViewController dismissViewControllerAnimated:NO completion:nil];
    self.canPick = false;
    [self.delegate mediaFileToPDFViewControllerDidCancel:self];
}
@end
