/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <UIKit/UIKit.h>
#import "FSMediaFileUtil.h"

@class FSFileAndImagePicker;
@protocol InsertPageConfigVCtrlDelegate;

NS_ASSUME_NONNULL_BEGIN

@protocol FSFileAndImagePickerDelegate

@optional
- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didSelectVideo:(AVPlayer *)video;
- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didPickFileAtPath:(NSString *)filePath;
- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didPickImage:(UIImage *)image imagePath:(nullable NSString *)imagePath;
- (void)fileAndImagePickerDidCancel:(FSFileAndImagePicker *)fileAndImagePicker;

@end

@class UIExtensionsManager;
@interface FSFileAndImagePicker : NSObject

@property (nonatomic, weak) id<InsertPageConfigVCtrlDelegate> insertPageDelegate;
@property (nonatomic, weak) id<FSFileAndImagePickerDelegate> delegate;
@property (nonatomic, strong) NSArray<NSString *> *expectedFileTypes;
@property (nonatomic, assign) FSMediaFileType fileType;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;


- (void)presentInRootViewController:(UIViewController *)rootViewController;
- (void)presentInRootViewController:(UIViewController *)rootViewController fromView:(UIView *_Nullable)view;

@end

NS_ASSUME_NONNULL_END
