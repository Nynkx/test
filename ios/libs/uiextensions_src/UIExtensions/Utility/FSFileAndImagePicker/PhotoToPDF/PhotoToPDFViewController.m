/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Utility.h"

#import "PhotoToPDFViewController.h"

@interface PhotoToPDFViewController ()

@property (nonatomic, strong) UIImagePickerController *mediaController;
@property (nonatomic) BOOL canPick;

@end

@implementation PhotoToPDFViewController

- (id)initWithButton:(UIButton *)button {
    self = [super init];
    if (self) {
        
        self.mediaController = [[UIImagePickerController alloc] init];
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        self.mediaController.delegate = self;
        if (button) {
            self.mediaController.popoverPresentationController.sourceRect = button.bounds;
            self.mediaController.popoverPresentationController.sourceView = button;
        }
    }
    return self;
}

- (void)openAlbum {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        self.mediaController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

        [self.view addSubview:self.mediaController.view];
        [self addChildViewController:self.mediaController];
        [self.mediaController didMoveToParentViewController:self];
        
        self.canPick = true;
    } else {
        [self dismissViewControllerAnimated:NO completion:^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFailed") message:FSLocalizedForKey(@"kFailedOpenAlbum") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        }];
    }
}

- (void)openCamera {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.mediaController.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.mediaController.showsCameraControls = YES;

        [self.view addSubview:self.mediaController.view];
        [self addChildViewController:self.mediaController];
        [self.mediaController didMoveToParentViewController:self];
        
        self.canPick = true;
    } else {
        [self dismissViewControllerAnimated:NO completion:^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFailed") message:FSLocalizedForKey(@"kFailedOpenCamera") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        }];

        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        BOOL canPick = self.canPick;
        self.canPick = false;
        if (canPick) {
            if (self.delegate) {
                UIImage *uiImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
                [self.delegate photoToPDFViewController:self didSelectImage:uiImage];
            }
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
        self.canPick = false;
        [self.delegate photoToPDFViewControllerDidCancel:self];
    }];
    
}

@end
