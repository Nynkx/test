/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import <UIKit/UIKit.h>
#import "FSMediaFileUtil.h"

@class FSMediaFileToPDFViewController;

@protocol FSMediaFileToPDFViewControllerDelegate
- (void)mediaFileToPDFViewController:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController didPickFileAtPath:(NSString *)filePath;
- (void)mediaFileToPDFViewController:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController didSelectImage:(UIImage *)image;
- (void)mediaFileToPDFViewController:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController didSelectVideo:(AVPlayer *)video;
- (void)mediaFileToPDFViewControllerDidCancel:(FSMediaFileToPDFViewController *)mediaFileToPDFViewController;

@end

@interface FSMediaFileToPDFViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, assign) FSMediaFileType fileType;
@property (nonatomic, weak) id<FSMediaFileToPDFViewControllerDelegate> delegate;

- (void)openAlbum;
- (void)openCamera;

@end
