/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ScreenAdapterUtil.h"

const SCREEN_TYPE PREFERENCE_CELL_ROW_HEIGHT = 72.f;
const SCREEN_TYPE IPAD_PREFERENCE_BAR_WIDTH = 401.f;
const SCREEN_TYPE STATUS_BAR_HEIGHT_NORMAL = 20.f;
const SCREEN_TYPE NAVIGATION_BAR_HEIGHT_NORMAL = 44.f;
const SCREEN_TYPE NAVIGATION_HEIGHT_NORMAL = 64.f;
const SCREEN_TYPE TAB_BAR_HEIGHT_NORMAL = 49.f;
const SCREEN_TYPE ITEM_MARGIN_SMALL = 5.f;
const SCREEN_TYPE ITEM_MARGIN_LITTLE_NORMAL = 10.f;
const SCREEN_TYPE ITEM_MARGIN_NORMAL = 15.f;
const SCREEN_TYPE ITEM_MARGIN_MEDIUM = 30.f;
const SCREEN_TYPE DIVIDE_TOOL_BAR_THICKNES = 0.5f;
const SCREEN_TYPE DIVIDE_VIEW_THICKNES = 1.f;
const SCREEN_TYPE VIEW_TEMPORARY_SIZE_VALUE = 1024.f;
const SCREEN_TYPE TOOLBAR_ITEM_INVALID_MARGIN_VALUE = -1;

@implementation UIView (MasonrySafeAreaLayout)

- (MASViewAttribute *)fs_mas_leading {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideLeading;
    return self.mas_leading;
}

- (MASViewAttribute *)fs_mas_trailing {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideTrailing;
    return self.mas_trailing;
}

- (MASViewAttribute *)fs_mas_left {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideLeft;
    return self.mas_left;
}

- (MASViewAttribute *)fs_mas_right {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideRight;
    return self.mas_right;
}

- (MASViewAttribute *)fs_mas_top {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideTop;
    return self.mas_top;
}

- (MASViewAttribute *)fs_mas_bottom {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideBottom;
    return self.mas_bottom;
}

- (MASViewAttribute *)fs_mas_width {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideWidth;
    return self.mas_width;
}

- (MASViewAttribute *)fs_mas_height {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideHeight;
    return self.mas_height;
}

- (MASViewAttribute *)fs_mas_centerX {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideCenterX;
    return self.mas_centerX;
}

- (MASViewAttribute *)fs_mas_centerY {
    if (@available(iOS 11.0, *))return self.mas_safeAreaLayoutGuideCenterY;
    return self.mas_centerY;
}

@end



