/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

typedef NS_ENUM(NSInteger, FSScreenActionType) {
    FSScreenActionTypeUnScreen = 0,
    FSScreenActionTypeUnknow = 1,
    FSScreenActionTypeImage = 2,
    FSScreenActionTypeAudio = 3,
    FSScreenActionTypeVideo = 4
};

@interface FSAnnot (Extensions)
- (FSScreenActionType)screenActiontype;
- (FSFileSpec *)ha_fileSpec;
@end

