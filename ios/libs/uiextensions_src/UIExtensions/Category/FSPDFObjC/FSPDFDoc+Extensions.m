/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSPDFDoc+Extensions.h"

@implementation FSPDFDoc (Extensions)

- (BOOL)isSigned{
    int signatureCount = [self getSignatureCount];
    for (int i = 0; i < signatureCount; i++){
        FSSignature *signature = [self getSignature:i];
        if ([signature isSigned]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isProtected{
    if (self.isSigned) {
        return YES;
    }
    FSSecurityHandler* securityHandler = [self getSecurityHandler];
    if ([securityHandler getSecurityType] == FSPDFDocEncryptRMS) {
        return YES;
    }
    return NO;
}

@end
