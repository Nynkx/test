/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSAnnotExtent.h"
#import "FSAnnotAttributes.h"
#import "Const.h"

FSRectF *convertToFSRect(FSPointF *p1, FSPointF *p2) {
    FSRectF *rect = [[FSRectF alloc] init];
    rect.left = MIN([p1 getX], [p2 getX]);
    rect.right = MAX([p1 getX], [p2 getX]);
    rect.top = MAX([p1 getY], [p2 getY]);
    rect.bottom = MIN([p1 getY], [p2 getY]);
    return rect;
}

@implementation FSAnnot (useProperties)

- (FSPDFDoc *)document {
    return [[self getPage] getDocument];
}

- (int)pageIndex {
    return [[self getPage] getIndex];
}

- (FSAnnotType)type {
    return [self getType];
}

- (FSRectF *)fsrect {
    return [self getRect];
}

- (void)setFsrect:(FSRectF *)fsrect {
    //Check if rect is valid.
    if(fsrect.right - fsrect.left <= 1E-5 || fsrect.top-fsrect.bottom <= 1E-5)
        return;
    [self move:fsrect];
}

- (unsigned int)color {
    if (self.type == FSAnnotFreeText) {
        FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:self];
        FSDefaultAppearance *ap = [annot getDefaultAppearance];
        return ap.text_color;
    }else if (self.type == FSAnnotWidget) {
        FSWidget *annot = [[FSWidget alloc] initWithAnnot:self];
        FSControl *control = [annot getControl];
        FSDefaultAppearance *ap = [control getDefaultAppearance];
        return ap.text_color;
    } else {
        return [self getBorderColor];
    }
}

- (void)setColor:(unsigned int)color {
    if (self.type == FSAnnotFreeText) {
        FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:self];
        FSDefaultAppearance *ap = [annot getDefaultAppearance];
        ap.text_color = color;
        [annot setDefaultAppearance:ap];
    }else if (self.type == FSAnnotWidget) {
        FSWidget *annot = [[FSWidget alloc] initWithAnnot:self];
        FSControl *control = [annot getControl];
        FSDefaultAppearance *ap = [control getDefaultAppearance];
        ap.text_color = color;
        [control setDefaultAppearance:ap];
    }
    else {
        [self setBorderColor:color];
    }
}

- (float)lineWidth {
    return [[self getBorderInfo] getWidth];
}
- (void)setLineWidth:(float)lineWidth {
    FSBorderInfo *borderInfo = [self getBorderInfo];
    [borderInfo setWidth:lineWidth];
    [self setBorderInfo:borderInfo];
}
- (unsigned int)flags {
    return [self getFlags];
}

- (NSString *)subject {
    if (![self isMarkup])
        return nil;
    return [[[FSMarkup alloc] initWithAnnot:self] getSubject];
}

- (void)setSubject:(NSString *)subject {
    if (![self isMarkup])
        return;
    return [[[FSMarkup alloc] initWithAnnot:self] setSubject:subject];
}

- (NSString *)NM {
    NSString *NM = nil;
    @try {
        NM = [self getUniqueID];
    } @catch (NSException *exception) {
    }
    if (!NM.length) {
        NM = [NSString stringWithFormat:@"%d_%d", [[self getPage] getIndex],[self.getDict getObjNum]];
    }
    return NM;
}

- (void)setNM:(NSString *)NM {
    BOOL canDo = ![Utility isDocumentSigned:self.document];
    if (canDo && [Utility canAddAnnotToDocument:self.document]) canDo = self.canEdit;
    if (self.type == FSAnnotWidget && !canDo)  return;
    @try {
        [self setUniqueID:NM];
    } @catch (NSException *exception) {
        NSLog(@"failed setting annot NM: %@", exception.description);
    }
}

- (NSString *)uuidWithPageIndex {
    NSString *uniqueID = nil;
    @try {
        uniqueID = [self getUniqueID];
    } @catch (NSException *exception) {
        uniqueID = nil;
    }
    if (!uniqueID.length) {
        return self.NM;
    }
   return [[NSString stringWithFormat:@"%d_", [[self getPage] getIndex]] stringByAppendingString:uniqueID];
}

- (NSString *)replyTo {
    if (FSAnnotNote != self.type) {
        return nil;
    }
    FSMarkup *markup = [[[FSNote alloc] initWithAnnot:self] getReplyTo];
    if (!markup || [markup isEmpty])
        return nil;
    return markup.NM;
}

- (NSArray *)replyAnnots{
    if ([self isMarkup])
    {
        FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:self];
        NSMutableArray *replyAnnots = @[].mutableCopy;
        for (int i = 0; i < markup.getReplyCount; i++) {
            [replyAnnots addObject:[markup getReply:i]];
        }
        return replyAnnots.copy;
    }
    return nil;
}

- (NSString *)author {
    if ([self getType] == FSAnnotScreen) {
        return [[[FSScreen alloc] initWithAnnot:self] getTitle];
    }
    if (![self isMarkup])
        return nil;
    FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
    return [mk getTitle];
}
- (void)setAuthor:(NSString *)author {
    if ([self getType] == FSAnnotScreen) {
        [[[FSScreen alloc] initWithAnnot:self] setTitle:author];
    }else if ([self isMarkup]) {
        [[[FSMarkup alloc] initWithAnnot:self] setTitle:author];
    }
}
- (NSString *)contents {
    return [self getContent];
}

- (void)setContents:(NSString *)contents {
    [self setContent:contents];
}

- (NSDate *)modifiedDate {
    @try {
    FSDateTime *dt = [self getModifiedDateTime];
    return dt ? [Utility convertFSDateTime2NSDate:dt] : nil;
    }
    @catch(NSException* e)
    {
        return nil;
    }
}
- (void)setModifiedDate:(NSDate *)modifiedDate {
    if (!modifiedDate) {
        return;
    }
    BOOL canDo = ![Utility isDocumentSigned:self.document];
    if (canDo && [Utility canAddAnnotToDocument:self.document]) canDo = self.canEdit;
    if (self.type == FSAnnotWidget && !canDo)  return;
    FSDateTime *dt = [Utility convert2FSDateTime:modifiedDate];
    [self setModifiedDateTime:dt];
}
- (NSDate *)createDate {
    if (![self isMarkup])
        return nil;
    FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
    @try {
    FSDateTime *dt = [mk getCreationDateTime];
    return dt ? [Utility convertFSDateTime2NSDate:dt] : nil;
    }
    @catch(NSException* e)
    {
        return nil;
    }
}
- (void)setCreateDate:(NSDate *)createDate {
    if (!createDate || ![self isMarkup])
        return;
    FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
    FSDateTime *dt = [Utility convert2FSDateTime:createDate];
    [mk setCreationDateTime:dt];
}
- (NSString *)intent {
    if (![self isMarkup])
        return nil;
    FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
    return [mk getIntent];
}
- (void)setIntent:(NSString *)intent {
    if (![self isMarkup])
        return;
    FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
    return [mk setIntent:intent];
}
- (NSString *)selectedText {
    FSPDFPage *_fspage = [self getPage];
    FSTextPage *textPage = nil;
    BOOL parseSuccess = [Utility parsePage:_fspage flag:FSPDFPageParsePageTextOnly pause:nil];
    if (parseSuccess) {
        textPage = [[FSTextPage alloc] initWithPage:_fspage flags:FSTextPageParseTextNormal];
    }

    NSString *selectedText = @"";
    if (textPage) {
        NSArray *array = self.quads;
        for (int i = 0; i < array.count; i++) {
            FSQuadPoints *arrayQuad = [array objectAtIndex:i];
            FSRectF *rect = convertToFSRect(arrayQuad.getFirst, arrayQuad.getFourth);
            NSString *tmp = [textPage getTextInRect:rect];
            if (tmp) {
                selectedText = [selectedText stringByAppendingString:tmp];
            }
        }
    }
    return selectedText;
}
- (NSArray *)quads {
    FSQuadPointsArray *quads = nil;
    FSAnnotType type = [self getType];
    if (type == FSAnnotHighlight || type == FSAnnotUnderline || type == FSAnnotStrikeOut || type == FSAnnotSquiggly || type == FSAnnotRedact) {
        FSTextMarkup *markup = [[FSTextMarkup alloc] initWithAnnot:self];
        quads = [markup getQuadPoints];
    } else if (type == FSAnnotLink) {
        FSLink *link = [[FSLink alloc] initWithAnnot:self];
        quads = [link getQuadPoints];
    } else {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < [quads getSize]; i++) {
        FSQuadPoints *quadPoints = [quads getAt:i];
        FSQuadPoints *clone = [[FSQuadPoints alloc] initWithQuad_points:quadPoints];
        [array addObject:clone];
    }
    return array;
}
- (void)setQuads:(NSArray *)quads {
    if (quads.count == 0)
        return;

    FSAnnotType type = [self getType];
    if (FSAnnotHighlight != type && FSAnnotUnderline != type && FSAnnotStrikeOut != type && FSAnnotSquiggly != type && FSAnnotLink != type && FSAnnotRedact != type)
        return;
    FSQuadPointsArray *array = [[FSQuadPointsArray alloc] init];
    for (FSQuadPoints *quad in quads) {
        [array add:quad];
    }
    if (FSAnnotLink == type) {
        [[FSLink alloc] initWithAnnot:self].quadPoints = array;
    }else if (FSAnnotRedact == type) {
        [[FSRedact alloc] initWithAnnot:self].quadPoints = array;
    }else {
        [[FSTextMarkup alloc] initWithAnnot:self].quadPoints = array;
    }
}

- (FSMarkup *)canGetGroupHeader {
    if (self.isMarkup) {
        FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:self];
        if (markup.isGrouped){
            if (markup.getGroupHeader && ![markup.getGroupHeader isEmpty]) {
                return markup.getGroupHeader;
            }else{
                if ([markup getGroupElements].getSize > 1) {
                    [markup.getPage setAnnotGroup:[markup getGroupElements] header_index:0];
                    return [[markup getGroupElements] getAt:0];
                }else{
                    [markup ungroup];
                }
            }
        }
    }
    return nil;
}

- (FSMarkupArray *)canGetGroupElements {
    FSMarkup *groupHeader = self.canGetGroupHeader;
    if (groupHeader && ![groupHeader isEmpty]) {
        return [groupHeader getGroupElements];
    }
    return nil;
}

- (BOOL)canReplyGroup{
    if (self.canReply) {
        FSMarkupArray *groupElements = self.canGetGroupElements;
        if (groupElements) {
            for (int i = 0; i < groupElements.getSize; i++) {
                FSMarkup *markup = [groupElements getAt:i];
                if (!markup.canReply) return NO;
            }
            return YES;
        }
        return YES;
    }
    return NO;
}

- (BOOL)canGroup{
    return !(self.flags & FSAnnotFlagLocked) && self.canModify;
}

- (BOOL)canReply {
    if (![self isMarkup] ||
        self.type == FSAnnotFreeText ||
        self.type == FSAnnotFileAttachment ||
        self.type == FSAnnotSound ||
        self.flags & FSAnnotFlagReadOnly ||
        ![self canModify]) {
        return NO;
    }
    return YES;
}

- (BOOL)canModify {
    return [Utility canAddAnnotToDocument:self.document];
}

- (BOOL)canCopyText {
    return [Utility canCopyTextInDocument:self.document];
}

- (BOOL)canApplyRedaction {
    return self.type == FSAnnotRedact && [Utility canApplyRedaction:self.document];
}

- (BOOL)canDisplay {
    return YES;
}

- (BOOL)canPrintable {
    return YES;
}

- (BOOL)canDoPreference{
    return !(self.flags & FSAnnotFlagLocked || self.flags & FSAnnotFlagReadOnly || self.type == FSAnnotSound);
}

- (BOOL)canMove {
    return self.canDoPreference;
}

- (BOOL)canResize {
    return self.canDoPreference;
}

- (BOOL)canRotate {
    return self.type == FSAnnotStamp && self.canDoPreference;
}

- (BOOL)canDelete {
    return self.canModify && self.canDoPreference;
}

- (BOOL)canEdit {
    return self.canModify && self.canDoPreference;
}

- (BOOL)canModifyAppearance {
    return self.canModify && self.canDoPreference;
}

- (BOOL)canView {
    return YES;
}

- (float)opacity {
    if ([self isMarkup]) {
        FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
        return [mk getOpacity];
    } else if ([self getType] == FSAnnotScreen) {
        FSScreen *screen = [[FSScreen alloc] initWithAnnot:self];
        return screen.opacity;
    } else {
        return 1.0f;
    }
}

- (void)setOpacity:(float)opacity {
    if ([self isMarkup]) {
        FSMarkup *mk = [[FSMarkup alloc] initWithAnnot:self];
        [mk setOpacity:opacity];
    } else if ([self getType] == FSAnnotScreen) {
        FSScreen *screen = [[FSScreen alloc] initWithAnnot:self];
        [screen setOpacity:opacity];
    }
}

- (void)setIcon:(int)icon {
    FSAnnotType type = self.type;
    NSString *iconName = [Utility getIconNameWithIconType:icon annotType:type];
    if (iconName) {
        if (type == FSAnnotNote) {
            [[FSNote alloc] initWithAnnot:self].iconName = iconName;
        } else if (type == FSAnnotStamp) {
            [[FSStamp alloc] initWithAnnot:self].iconName = iconName;
        } else if (type == FSAnnotFileAttachment) {
            [[FSFileAttachment alloc] initWithAnnot:self].iconName = iconName;
        }
    }
}

- (int)icon {
    FSAnnotType type = [self getType];
    NSString *iconName = nil;
    if (type == FSAnnotNote) {
        iconName = [[FSNote alloc] initWithAnnot:self].iconName;
    } else if (type == FSAnnotStamp) {
        iconName = [[FSStamp alloc] initWithAnnot:self].iconName;
    } else if (type == FSAnnotFileAttachment) {
        iconName = [[FSFileAttachment alloc] initWithAnnot:self].iconName;
    }
    if (iconName) {
        return [Utility getIconTypeWithIconName:iconName annotType:type];
    }
    return FPDF_ICONTYPE_UNKNOWN;
}

- (void)applyAttributes:(FSAnnotAttributes *)attributes {
    [attributes resetAnnot:self];
}

- (BOOL)isEqualToAnnot:(FSAnnot *)annot {
    if (self == annot) {
        return YES;
    }
    if ([self isEmpty] || !annot || [annot isEmpty]  ) {
        return NO;
    }
    
    @try {
        if (self.type != annot.type) {
            return NO;
        }
    } @catch (NSException *exception) {
        return NO;
    }
    //Duplicate pages have the same uuid, using PageIndex as the identifier
    return [self.uuidWithPageIndex isEqualToString:annot.uuidWithPageIndex];
}

- (BOOL)isReplyToAnnot:(FSAnnot *)annot {
    if (!annot || self.replyTo.length == 0) {
        return NO;
    }
    return [self.replyTo isEqualToString:[annot NM]] && [[self getPage] getIndex] == [[annot getPage] getIndex];
}

@end

@implementation FSPointFArray (debug)

- (NSString *)description {
    NSMutableString *ret = @"".mutableCopy;
    int sz = [self getSize];
    [ret appendFormat:@"%d points:", sz];
    for (int i = 0; i < sz; i++) {
        FSPointF *pt = [self getAt:i];
        [ret appendFormat:@" (%.1f, %.1f)", pt.x, pt.y];
    }
    return ret;
}

@end
