/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
#import "FSAnnot+Extensions.h"

@implementation FSAnnot (Extensions)

- (FSScreenActionType)screenActiontype{
    FSAnnotType type =  [self getType];
    if (type != FSAnnotScreen) return  FSScreenActionTypeUnScreen;
    FSScreen *screen = [[FSScreen alloc] initWithAnnot:self];
    FSAction *action = screen.action;
    if ([action isEmpty]) return FSScreenActionTypeImage;
    FSActionType actionType = [action getType];
    if (actionType != FSActionTypeRendition) return FSScreenActionTypeUnknow;
    FSRenditionAction *renditionAction = [[FSRenditionAction alloc] initWithAction:action];
    if (([renditionAction isEmpty] || renditionAction.getRenditionCount == 0)) return FSScreenActionTypeImage;
    FSRendition *rendition = [renditionAction getRendition:0];
    NSString *mineType = [rendition getMediaClipContentType];
    if ([mineType containsString:@"audio"])  return FSScreenActionTypeAudio;
    return FSScreenActionTypeVideo;
}


- (FSFileSpec *)ha_fileSpec{
    FSAnnotType type =  [self getType];
    if (type == FSAnnotFileAttachment) {
        FSFileAttachment *attachmentAnnot = [[FSFileAttachment alloc] initWithAnnot:self];
        return [attachmentAnnot getFileSpec];
    }
    if (self.screenActiontype == FSScreenActionTypeUnknow || self.screenActiontype == FSScreenActionTypeImage || self.screenActiontype == FSScreenActionTypeUnScreen) return nil;
    FSScreen *screen = [[FSScreen alloc] initWithAnnot:self];
    FSAction *action = screen.action;
    FSRenditionAction *renditionAction = [[FSRenditionAction alloc] initWithAction:action];
    FSRendition *rendition = [renditionAction getRendition:0];
    FSFileSpec *fileSpec = rendition.mediaClipFile;
    return fileSpec;
}

@end

