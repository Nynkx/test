/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "UITextView+Extensions.h"
#import  <objc/runtime.h>
#import "NSObject+Extensions.h"
#import "YYKitMacro.h"

#define LOCK(...) dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER); \
__VA_ARGS__; \
dispatch_semaphore_signal(_lock);
#define kiOS9_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 9.0)
#define kiOS10_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 10.0)
#define kiOS11_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 11.0)

static dispatch_semaphore_t _lock;

@interface UITextView ()

@property (nonatomic, copy) NSString *inputText;
@property (nonatomic, assign) NSUInteger inputLength;
@property (nonatomic, assign) BOOL shouldChangeText;
@property (nonatomic, assign) BOOL isClickKeyboardCandidateBarCell;
@end

@implementation UITextView (CompatibleiOS9)

#pragma mark -

+ (void) load{
    [super load];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!kiOS10_OR_LATER) {
            [self fs_exchangeImpWithOriginalSel:@selector(setDelegate:) swizzledSel:@selector(fs_swizzled_setDelegate:)];
            _lock = dispatch_semaphore_create(1);
        }
    });
}

YYSYNTH_DYNAMIC_PROPERTY_OBJECT(inputText, setInputText, COPY, NSString *)
YYSYNTH_DYNAMIC_PROPERTY_CTYPE(inputLength, setInputLength, NSUInteger)
YYSYNTH_DYNAMIC_PROPERTY_CTYPE(shouldChangeText, setShouldChangeText, BOOL)
YYSYNTH_DYNAMIC_PROPERTY_CTYPE(isClickKeyboardCandidateBarCell, setIsClickKeyboardCandidateBarCell, BOOL)

- (NSRange)textRangeTransformRange:(UITextRange *)range{
    const NSInteger location = [self offsetFromPosition:self.beginningOfDocument toPosition:range.start];
    const NSInteger length = [self offsetFromPosition:range.start toPosition:range.end];
    return NSMakeRange(location, length);
}

#pragma mark - swizzled method

- (void)fs_swizzled_setDelegate:(id<UITextViewDelegate>)delegate{
    [self fs_swizzled_setDelegate:delegate];
    
    SEL selectors[] = {
        @selector(textView:shouldChangeTextInRange:replacementText:),
        @selector(textViewDidChange:),
        @selector(textViewDidChangeSelection:)
    };
    
    for (NSUInteger index = 0; index < sizeof(selectors) / sizeof(SEL); ++index) {
        SEL originalSelector = selectors[index];
        SEL swizzledSelector = NSSelectorFromString([@"fs_swizzled_" stringByAppendingString:NSStringFromSelector(originalSelector)]);
        SEL tempSelector = NSSelectorFromString([@"tmp_" stringByAppendingString:NSStringFromSelector(originalSelector)]);
        [UITextView fs_exchangeImpWithOriginalClass:[delegate class] swizzledClass:[self class] originalSel:originalSelector swizzledSel:swizzledSelector tmpSel:tempSelector];
    }
}

- (BOOL)fs_swizzled_textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return [self fs_swizzled_textView:textView shouldChangeTextInRange:range replacementText:text];
}

- (void)fs_swizzled_textViewDidChange:(UITextView *)textView{
    [self fs_swizzled_textViewDidChange:textView];
}

- (void)fs_swizzled_textViewDidChangeSelection:(UITextView *)textView{
    NSRange range =  [textView textRangeTransformRange:[textView markedTextRange]];
    if (NSEqualRanges (range, NSMakeRange(0, 0)) || !textView.isClickKeyboardCandidateBarCell) {
        [self fs_swizzled_textViewDidChangeSelection:textView];
        return;
    }
    LOCK({
        NSString *text = [textView.text copy];
        NSString *inputText = [textView.inputText copy];
        if (text) {
            NSRange newRange = NSMakeRange(range.location, textView.inputLength);
            textView.shouldChangeText = [self fs_swizzled_textView:textView shouldChangeTextInRange:newRange replacementText:inputText];
            if (!textView.shouldChangeText) {
                textView.text = [text stringByReplacingCharactersInRange:range withString:@""];
            }
            textView.isClickKeyboardCandidateBarCell = NO;
        }
        [self fs_swizzled_textViewDidChangeSelection:textView];
    });
}

#pragma mark - tmp method
- (BOOL)tmp_textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {return YES;}
- (void)tmp_textViewDidChange:(UITextView *)textView {}
- (void)tmp_textViewDidChangeSelection:(UITextView *)textView {}
@end

@interface CompatibleKeyBoardCellDelegateTargetiOS9 : NSObject <UIGestureRecognizerDelegate>
- (void)didClicked:(UITapGestureRecognizer *)tap;
@end

@interface UICollectionViewCell (CompatibleKeyBoardiOS9)
@property (nonatomic, weak) CompatibleKeyBoardCellDelegateTargetiOS9 *compatibleiOS9;
@end

@implementation UICollectionViewCell (CompatibleKeyBoardiOS9)

+ (void)load{
    [super load];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!kiOS10_OR_LATER) {
            Class cls = NSClassFromString(@"UIKeyboardCandidateBarCell");
            SEL selectors[] = {
                @selector(allocWithZone:),
                @selector(initWithFrame:)
            };
            
            for (NSUInteger index = 0; index < sizeof(selectors) / sizeof(SEL); ++index) {
                SEL originalSelector = selectors[index];
                SEL swizzledSelector = NSSelectorFromString([@"fs_swizzled_" stringByAppendingString:NSStringFromSelector(originalSelector)]);
                [cls fs_exchangeImpWithOriginalSel:originalSelector swizzledSel:swizzledSelector];
            }
        }
    });
}

YYSYNTH_DYNAMIC_PROPERTY_OBJECT(compatibleiOS9, setCompatibleiOS9, RETAIN, CompatibleKeyBoardCellDelegateTargetiOS9 *)

#pragma mark - swizzled method
+ (instancetype)fs_swizzled_allocWithZone:(struct _NSZone *)zone{
    UICollectionViewCell *cell =  [self fs_swizzled_allocWithZone:zone];
    return cell;
}

- (instancetype)fs_swizzled_initWithFrame:(CGRect)frame{
    UICollectionViewCell *cell =  [self fs_swizzled_initWithFrame:frame];
    CompatibleKeyBoardCellDelegateTargetiOS9  *compatibleiOS9 = [[CompatibleKeyBoardCellDelegateTargetiOS9 alloc] init];
    cell.compatibleiOS9 = compatibleiOS9;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:compatibleiOS9 action:@selector(didClicked:)];
    tap.delegate = compatibleiOS9;
    [cell addGestureRecognizer:tap];
    return cell;
}
@end

@implementation CompatibleKeyBoardCellDelegateTargetiOS9

- (void)didClicked:(UITapGestureRecognizer *)tap{}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    BOOL flag = NO;
    UITextView *textView = [self firstResponder];
    if ([textView isKindOfClass:[UITextView class]]) {
        NSRange range =  [textView textRangeTransformRange:[textView markedTextRange]];
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
         UITextInputMode *inputMode = [UITextInputMode currentInputMode];
        #pragma clang diagnostic pop
        if (![[inputMode class] isKindOfClass:NSClassFromString(@"UIKeyboardExtensionInputMode")] && [[inputMode primaryLanguage] isEqualToString:@"zh-Hans"]) {
            flag = YES;
        }
        textView.inputLength = range.length;
    }
    if (flag){
        if (!_lock) _lock = dispatch_semaphore_create(1);
        LOCK([gestureRecognizer.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop1) {
            if ([view isKindOfClass:[UIView class]]) {
                [view.subviews enumerateObjectsUsingBlock:^( UILabel * _Nonnull lab, NSUInteger idx, BOOL * _Nonnull stop2) {
                    if ([lab isKindOfClass:[UILabel class]]) {
                        textView.isClickKeyboardCandidateBarCell = YES;
                        textView.inputText = lab.text;
                        *stop1 = YES;
                        *stop2 = YES;
                    }
                }];
            }
        }];)
    }
    return NO;
}

- (__kindof UIView *)firstResponder {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    return firstResponder;
}
@end


@implementation UITextView (FixTextLayoutNotFoundiOS13)

+ (void)load{
    [super load];
    if (@available(iOS 13.2, *)) return;
    const char *className = "_UITextLayoutView";
    Class cls = objc_getClass(className);
    if (cls == nil) {
        cls = objc_allocateClassPair([UIView class], className, 0);
        objc_registerClassPair(cls);
    #if DEBUG
        printf("added %s dynamically\n", className);
    #endif
    }
}
@end
