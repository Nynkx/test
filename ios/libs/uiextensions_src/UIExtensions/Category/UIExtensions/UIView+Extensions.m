/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "UIView+Extensions.h"
#import "MBProgressHUD.h"
#import <objc/runtime.h>

const NSUInteger kMBProgressHUDTag = 8686;

@implementation UIView (Extensions)

- (CGFloat)fs_left {
    return self.frame.origin.x;
}

- (void)fs_setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)fs_top {
    return self.frame.origin.y;
}

- (void)fs_setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)fs_right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)fs_setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)fs_bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)fs_setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

- (CGFloat)fs_width {
    return self.frame.size.width;
}

- (void)fs_setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)fs_height {
    return self.frame.size.height;
}

- (void)fs_setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)fs_centerX {
    return self.center.x;
}

- (void)fs_setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}

- (CGFloat)fs_centerY {
    return self.center.y;
}

- (void)fs_setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}

- (CGPoint)fs_origin {
    return self.frame.origin;
}

- (void)fs_setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)fs_size {
    return self.frame.size;
}

- (void)fs_setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (UITableView *)fs_tableView {
    for (UIView *view = self; view; view = view.superview) {
        UIResponder *nextResponder = [view nextResponder];
        if ([nextResponder isKindOfClass:[UITableView class]]) {
            return (UITableView *)nextResponder;
        }
    }
    return nil;
}

- (UIViewController *)fs_viewController {
    for (UIView *view = self; view; view = view.superview) {
        UIResponder *nextResponder = [view nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

- (UIView *)fs_MBProgressHUD{
    return [self viewWithTag:kMBProgressHUDTag];
}

- (void)fs_showHUDCustomView:(UIView * _Nullable)customView{
    [self fs_showHUDCustomView:customView msg:nil];
}

- (void)fs_showHUDCustomView:(UIView * _Nullable)customView msg:(NSString * _Nullable)msg{
    [self fs_showHUDCustomView:customView msg:msg process:nil];
}

- (void)fs_showHUDCustomView:(UIView * _Nullable)customView msg:(NSString * _Nullable)msg process:(_Nullable dispatch_block_t)process{
    [self fs_hideHUDLoading];
    dispatch_main_async_safe(^{
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self animated:YES];
        progressHUD.tag = kMBProgressHUDTag;
        progressHUD.customView = customView;
        progressHUD.mode = MBProgressHUDModeCustomView;
        progressHUD.detailsLabelText = msg;
        progressHUD.removeFromSuperViewOnHide = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHUD)];
        [progressHUD addGestureRecognizer:tap];
    });
}

- (void)hideHUD{
    [self fs_hideHUDLoading];
}

- (void)fs_showHUDLoading:(NSString *)msg process:(dispatch_block_t)process {
    [self fs_showHUDLoading:msg process:process synch:NO];
}

- (void)fs_showHUDLoading:(NSString *)msg process:(dispatch_block_t)process synch:(BOOL)synch{
    [self fs_showHUDLoading:msg process:process synch:synch onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
}

- (void)fs_showHUDLoading:(NSString *)msg process:(dispatch_block_t)process synch:(BOOL)synch onQueue:(dispatch_queue_t)queue {
    dispatch_semaphore_t wait;
    if (synch) wait = dispatch_semaphore_create(0);
    @try {
        [self fs_showHUDLoading:msg];
        dispatch_async(queue, ^{
            process();
            if (synch) dispatch_semaphore_signal(wait);
            [self fs_hideHUDLoading];
        });
        if (synch) dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
    } @catch (NSException *exception) {
        if (synch) dispatch_semaphore_signal(wait);
        [self fs_hideHUDLoading];
        @throw exception;
    }
}


- (void)fs_showHUDLoading:(NSString *)msg {
    [self fs_hideHUDLoading];
    dispatch_main_async_safe(^{
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self animated:YES];
        progressHUD.tag = kMBProgressHUDTag;
        progressHUD.mode = MBProgressHUDModeIndeterminate;
        progressHUD.detailsLabelText = msg;
        progressHUD.removeFromSuperViewOnHide = YES;
    });
}

- (void)fs_hideHUDLoading {
    dispatch_main_async_safe(^{
        MBProgressHUD *hud = [self viewWithTag:kMBProgressHUDTag];
        if (hud) {
            [MBProgressHUD hideHUDForView:self animated:YES];
            [hud removeFromSuperview];
        }
    });
}

- (void)fs_showHUDMessage:(NSString *)msg{
    [self fs_showHUDMessage:msg afterHideDelay:2];
}

- (void)fs_showHUDMessage:(NSString *)msg afterHideDelay:(NSTimeInterval)delay{
    [MBProgressHUD hideHUDForView:self animated:YES];
    dispatch_main_async_safe(^{
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self animated:YES];
        progressHUD.mode = MBProgressHUDModeText;
        progressHUD.detailsLabelText = msg;
        progressHUD.removeFromSuperViewOnHide = YES;
        [progressHUD hide:YES afterDelay:delay];
        
    });
}

@end

static char topEdgeKey;
static char leftEdgeKey;
static char bottomEdgeKey;
static char rightEdgeKey;

@implementation UIView (UIViewEnlargeEdge)

- (void)setEnlargedEdge:(CGFloat)enlargedEdge {
    [self setEnlargedEdgeWithTop:enlargedEdge left:enlargedEdge bottom:enlargedEdge right:enlargedEdge];
}

- (CGFloat)enlargedEdge {
    return [(NSNumber *) objc_getAssociatedObject(self, &topEdgeKey) floatValue];
}

- (void)setEnlargedEdgeWithTop:(CGFloat)top left:(CGFloat)left bottom:(CGFloat)bottom right:(CGFloat)right {
    objc_setAssociatedObject(self, &topEdgeKey, [NSNumber numberWithFloat:top], OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &leftEdgeKey, [NSNumber numberWithFloat:left], OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &bottomEdgeKey, [NSNumber numberWithFloat:bottom], OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &rightEdgeKey, [NSNumber numberWithFloat:right], OBJC_ASSOCIATION_RETAIN);
}

- (CGRect)enlargeRect {
    NSNumber *topEdge = objc_getAssociatedObject(self, &topEdgeKey);
    NSNumber *leftEdge = objc_getAssociatedObject(self, &leftEdgeKey);
    NSNumber *bottomEdge = objc_getAssociatedObject(self, &bottomEdgeKey);
    NSNumber *rightEdge = objc_getAssociatedObject(self, &rightEdgeKey);
    if (topEdge && rightEdge && bottomEdge && leftEdge) {
        return CGRectMake(self.bounds.origin.x - leftEdge.floatValue,
                          self.bounds.origin.y - topEdge.floatValue,
                          self.bounds.size.width + leftEdge.floatValue + rightEdge.floatValue,
                          self.bounds.size.height + topEdge.floatValue + bottomEdge.floatValue);
    } else {
        return self.bounds;
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect newBounds = [self enlargeRect];
    return CGRectContainsPoint(newBounds, point) ? YES : NO;
}

@end
