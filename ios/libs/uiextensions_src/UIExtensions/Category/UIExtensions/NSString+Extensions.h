/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


NS_ASSUME_NONNULL_BEGIN

@interface NSString (Extensions)

- (CGSize)fs_adaptTextWithFontSize:(float)fontSize maxSize:(CGSize)maxSize;

@end

@interface NSString (GetFileMD5)
+ (NSString *)fs_getFileMD5WithPath:(NSString *)path;
@end

NS_ASSUME_NONNULL_END
