/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import <UIKit/UIKit.h>
#import "UIColor+YYAdd.h"

NS_ASSUME_NONNULL_BEGIN
struct RGB {
    float red;
    float green;
    float blue;
    float alpha;
};

struct HSV {
    float hue;
    float saturation;
    float brightness;
    float alpha;
};

typedef struct RGB RGB;
typedef struct HSV HSV;

RGB hsv2rgb(HSV hsv);

HSV rgb2hsv(RGB rgb);

@interface UIColor (Extensions)

+ (UIColor *)fs_blackColor;
+ (UIColor *)fs_darkGrayColor;
+ (UIColor *)fs_lightGrayColor;
+ (UIColor *)fs_whiteColor;
+ (UIColor *)fs_grayColor;
+ (UIColor *)fs_redColor;
+ (UIColor *)fs_greenColor;
+ (UIColor *)fs_blueColor;
+ (UIColor *)fs_cyanColor;
+ (UIColor *)fs_yellowColor;
+ (UIColor *)fs_magentaColor;
+ (UIColor *)fs_orangeColor;
+ (UIColor *)fs_purpleColor;
+ (UIColor *)fs_brownColor;
+ (UIColor *)fs_clearColor;


+ (UIColor *)fs_colorWithARGBHexString:(NSString *)hexStr;
- (uint32_t)fs_argbValue;
@end


@interface UIColor (DarkMode_iOS13)

/**
Creates and returns a color object for dark mode.

@discussion:
Valid format: #RGB #RGBA #RRGGBB #RRGGBBAA 0xRGB ...
The `#` or "0x" sign is not required.
The alpha will be set to 1.0 if there is no alpha component.
It will return nil when an error occurs in parsing.

Example: @"0xF0F", @"66ccff", @"#66CCFF88"
 */
+ (UIColor *)fs_colorWithLightHex:(NSString *)lightHex darkHex:(NSString *)darkHex;
+ (UIColor *)fs_colorWithLight:(UIColor *)light dark:(UIColor *)dark;
+ (UIColor *)fs_colorWithOwner:(nullable id<UITraitEnvironment>)owner light:(UIColor *)light dark:(UIColor *)dark;
+ (UIColor *)fs_colorWithDynamicProvider:(UIColor * (^)(UITraitCollection *traitCollection))dynamicProvider;
- (UIColor *)fs_initWithDynamicProvider:(UIColor * (^)(UITraitCollection *traitCollection))dynamicProvider;

@end


NS_ASSUME_NONNULL_END
