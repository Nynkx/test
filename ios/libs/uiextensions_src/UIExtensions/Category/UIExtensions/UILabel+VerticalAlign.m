/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UILabel+VerticalAlign.h"

@interface UILabel (IBLocalized)
@end

@implementation UILabel (IBLocalized)

- (void)setIBLocalizedText:(NSString *)text{
    self.text = FSLocalizedForKey(text);
}
@end

// -- file: UILabel+VerticalAlign.m
@implementation UILabel (VerticalAlign)
- (void)alignTop {
    CGSize fontSize = [self.text sizeWithAttributes:@{NSFontAttributeName : self.font}];
    double finalHeight = fontSize.height * self.numberOfLines;
    double finalWidth = self.frame.size.width; //expected width of label
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = self.lineBreakMode;
    CGSize theStringSize = [self.text boundingRectWithSize:CGSizeMake(finalWidth, finalHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.font, NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
    for (int i = 0; i < newLinesToPad; i++)
        self.text = [self.text stringByAppendingString:@"\n "];
}

@end
