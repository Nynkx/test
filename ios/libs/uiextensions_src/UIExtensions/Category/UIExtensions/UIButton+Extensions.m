/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIButton+Extensions.h"
#import "NSString+Extensions.h"
#import "UIImage+Extensions.h"
#import "PrivateDefine.h"
#import <objc/runtime.h>

@implementation UIButton (Extensions)

+ (UIButton *)createButtonWithImageAndTitle:(nullable NSString *)title
                                imageNormal:(nullable UIImage *)imageNormal
                              imageSelected:(nullable UIImage *)imageSelected
                               imageDisable:(nullable UIImage *)imageDisabled {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGSize titleSize = [title fs_adaptTextWithFontSize:9.0f maxSize:CGSizeMake(200, 100)];
    
    float width = imageNormal.size.width;
    float height = imageNormal.size.height;
    button.contentMode = UIViewContentModeScaleToFill;
    [button setImage:imageNormal forState:UIControlStateNormal];
    [button setImage:[imageNormal fs_applyingAlpha:0.5] forState:UIControlStateHighlighted];
    [button setImage:[imageNormal fs_applyingAlpha:0.5] forState:UIControlStateSelected];
    
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:UIColorHex(0x5c5c5c) forState:UIControlStateHighlighted];
    [button setTitleColor:UIColorHex(0x5c5c5c) forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont systemFontOfSize:9];
    
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height * 1.5, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    button.frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width + 2 : width, titleSize.height + height);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    return button;
}

@end

static char topEdgeKey;
static char leftEdgeKey;
static char bottomEdgeKey;
static char rightEdgeKey;

@implementation UIButton (EnlargeEdge)

- (void)setEnlargedEdge:(CGFloat)enlargedEdge {
    [self setEnlargedEdgeWithTop:enlargedEdge left:enlargedEdge bottom:enlargedEdge right:enlargedEdge];
}

- (CGFloat)enlargedEdge {
    return [(NSNumber *) objc_getAssociatedObject(self, &topEdgeKey) floatValue];
}

- (void)setEnlargedEdgeWithTop:(CGFloat)top left:(CGFloat)left bottom:(CGFloat)bottom right:(CGFloat)right {
    objc_setAssociatedObject(self, &topEdgeKey, [NSNumber numberWithFloat:top], OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &leftEdgeKey, [NSNumber numberWithFloat:left], OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &bottomEdgeKey, [NSNumber numberWithFloat:bottom], OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &rightEdgeKey, [NSNumber numberWithFloat:right], OBJC_ASSOCIATION_RETAIN);
}

- (CGRect)enlargeRect {
    NSNumber *topEdge = objc_getAssociatedObject(self, &topEdgeKey);
    NSNumber *leftEdge = objc_getAssociatedObject(self, &leftEdgeKey);
    NSNumber *bottomEdge = objc_getAssociatedObject(self, &bottomEdgeKey);
    NSNumber *rightEdge = objc_getAssociatedObject(self, &rightEdgeKey);
    if (topEdge && rightEdge && bottomEdge && leftEdge) {
        return CGRectMake(self.bounds.origin.x - leftEdge.floatValue,
                          self.bounds.origin.y - topEdge.floatValue,
                          self.bounds.size.width + leftEdge.floatValue + rightEdge.floatValue,
                          self.bounds.size.height + topEdge.floatValue + bottomEdge.floatValue);
    } else {
        return self.bounds;
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect newBounds = [self enlargeRect];
    if (CGRectEqualToRect(newBounds, self.bounds)) {
        return [super pointInside:point withEvent:event];
    }
    return CGRectContainsPoint(newBounds, point) ? YES : NO;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect newBounds = [self enlargeRect];
    if (CGRectEqualToRect(newBounds, self.bounds)) {
        return [super hitTest:point withEvent:event];
    }
    return CGRectContainsPoint(newBounds, point) ? self : nil;
}
@end

