/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "UIColor+Extensions.h"
#import "PrivateDefine.h"

RGB hsv2rgb(HSV hsv) {
    RGB rgb = {0.0, 0.0, 0.0, 0.0};
    float r, g, b;

    int i = (int)(hsv.hue * 6);
    float f = hsv.hue * 6 - (float)(i);
    float p = hsv.brightness * (1 - hsv.saturation);
    float q = hsv.brightness * (1 - f * hsv.saturation);
    float t = hsv.brightness * (1 - (1 - f) * hsv.saturation);
    
    switch (i % 6) {
        case 0: r = hsv.brightness; g = t; b = p; break;
            
        case 1: r = q; g = hsv.brightness; b = p; break;
            
        case 2: r = p; g = hsv.brightness; b = t; break;
            
        case 3: r = p; g = q; b = hsv.brightness; break;
            
        case 4: r = t; g = p; b = hsv.brightness; break;
            
        case 5: r = hsv.brightness; g = p; b = q; break;
            
        default: r = hsv.brightness; g = t; b = p;
    }
    rgb.red = r;
    rgb.green = g;
    rgb.blue = b;
    rgb.alpha = hsv.alpha;
    return rgb;
}

HSV rgb2hsv(RGB rgb) {
    HSV hsb = {0.0, 0.0, 0.0, 0.0};
    float rd = rgb.red, gd = rgb.green, bd = rgb.blue;
    
    float maxV = MAX(rd, MAX(gd, bd));
    float minV = MIN(rd, MIN(gd, bd));
    float h = 0, s = 0, b = maxV;
    
    float d = maxV - minV;
    
    s = maxV == 0 ? 0 : d / minV;
    
    if (maxV == minV) {
        h = 0;
    } else {
        if (maxV == rd) {
            h = (gd - bd) / d + (gd < bd ? 6 : 0);
        } else if (maxV == gd) {
            h = (bd - rd) / d + 2;
        } else if (maxV == bd) {
            h = (rd - gd) / d + 4;
        }
        h /= 6;
    }
    hsb.hue = h;
    hsb.saturation = s;
    hsb.brightness = b;
    hsb.alpha = rgb.alpha;
    return hsb;
}

@implementation UIColor (Extensions)

+ (UIColor *)fs_blackColor{ return [self blackColor]; }
+ (UIColor *)fs_darkGrayColor { return [self darkGrayColor]; }
+ (UIColor *)fs_lightGrayColor { return [self lightGrayColor]; }
+ (UIColor *)fs_whiteColor { return [self whiteColor]; }
+ (UIColor *)fs_grayColor { return [self grayColor]; }
+ (UIColor *)fs_redColor { return [self redColor]; }
+ (UIColor *)fs_greenColor { return [self greenColor]; }
+ (UIColor *)fs_blueColor { return [self blueColor]; }
+ (UIColor *)fs_cyanColor { return [self cyanColor]; }
+ (UIColor *)fs_yellowColor { return [self yellowColor]; }
+ (UIColor *)fs_magentaColor { return [self magentaColor]; }
+ (UIColor *)fs_orangeColor { return [self orangeColor]; }
+ (UIColor *)fs_purpleColor { return [self purpleColor]; }
+ (UIColor *)fs_brownColor { return [self brownColor]; }
+ (UIColor *)fs_clearColor { return [self clearColor]; }

+ (UIColor *)fs_colorWithARGBHexString:(NSString *)hexStr {
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    unsigned hexNum;
    if (![scanner scanHexInt:&hexNum]) return nil;
    if (hexStr.length == 8) {
        uint32_t result = 0;
        sscanf([[hexStr substringWithRange:NSMakeRange(0, 2)] UTF8String], "%X", &result);
        float a = result/255.0f;
        return [[UIColor colorWithRGB:hexNum] colorWithAlphaComponent:a];
    }
    return [UIColor colorWithRGB:hexNum];
}

- (uint32_t)fs_argbValue {
    CGFloat r,g,b,a;
    if (![self getRed:&r green:&g blue:&b alpha:&a]) return 0;
    
    a = MIN(MAX(self.alpha, 0.0f), 1.0f);
    r = MIN(MAX(self.red, 0.0f), 1.0f);
    g = MIN(MAX(self.green, 0.0f), 1.0f);
    b = MIN(MAX(self.blue, 0.0f), 1.0f);
    
    return (((int)roundf(a * 255)) << 24)
    | (((int)roundf(r * 255)) << 16)
    | (((int)roundf(g * 255)) << 8)
    | (((int)roundf(b * 255)));
}

@end

@implementation UIColor (DarkMode_iOS13)

+ (UIColor *)fs_colorWithLightHex:(NSString *)lightHex darkHex:(NSString *)darkHex{
    return [self fs_colorWithLight:[UIColor yy_colorWithHexString:lightHex] dark:[UIColor yy_colorWithHexString:darkHex]];
}

+ (UIColor *)fs_colorWithLight:(UIColor *)light dark:(UIColor *)dark{
   return [self fs_colorWithOwner:nil light:light dark:dark];
}

+ (UIColor *)fs_colorWithOwner:(nullable id<UITraitEnvironment>)owner light:(UIColor *)light dark:(UIColor *)dark {
    if (!light && !dark) return nil;
    if (!light) light = [self fs_whiteColor];
    if (!dark) return light;
   return [UIColor fs_colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
       #if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_12_0
       if (@available(iOS 12.0, *)) {
           if (owner) {
               traitCollection = owner.traitCollection;
           }
           if (traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
               return dark;
           }
       }
       #endif
       return light;
    }];
}

+ (UIColor *)fs_colorWithDynamicProvider:(UIColor * (^)(UITraitCollection *traitCollection))dynamicProvider{
    return [[self alloc] fs_initWithDynamicProvider:dynamicProvider];
}

- (UIColor *)fs_initWithDynamicProvider:(UIColor * (^)(UITraitCollection *traitCollection))dynamicProvider{
    
    #if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_13_0
        if (@available(iOS 13.0, *)) {
            return [[UIColor alloc] initWithDynamicProvider:dynamicProvider];
        }
    #elif __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_12_0
        if (@available(iOS 12.0, *)) {
            return dynamicProvider([UITraitCollection traitCollectionWithUserInterfaceStyle:UIUserInterfaceStyleLight]);
        }
    #endif
    return dynamicProvider([UITraitCollection new]);
}


@end
