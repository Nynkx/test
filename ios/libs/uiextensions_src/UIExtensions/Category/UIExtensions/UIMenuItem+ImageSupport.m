/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <objc/runtime.h>
#import "YYKitMacro.h"
#import "UIMenuItem+ImageSupport.h"
#import "NSObject+Extensions.h"

#define INVISIBLE_IDENTIFIER @"\uFEFF\u200B"

static NSMutableDictionary<NSString *, UIImage*> *titleImagePairs;

@interface UIMenuItem ()
@property (nonatomic, copy) NSString *uuidTitle;

@end

@implementation UIMenuItem (ImageSupport)

+ (void)load{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        titleImagePairs = [NSMutableDictionary dictionary];
    });
}

+ (void)dealloc{
    titleImagePairs = nil;
}

- (instancetype)initWithTitle:(NSString *)title image:(nullable UIImage *)image action:(SEL)action {
    if (!title.length) title = @"";
    #if !_MAC_CATALYST_
    if (image) {
        self.uuidTitle = [NSString stringWithFormat:@"%@%@%@",INVISIBLE_IDENTIFIER,[self createUuidTitle],INVISIBLE_IDENTIFIER];
        [titleImagePairs setValue:image forKey:self.uuidTitle];
        title = self.uuidTitle;
    }
    #endif
    return [self initWithTitle:title action:action];
}

- (void)dealloc{
    if (self.uuidTitle) {
        [titleImagePairs removeObjectForKey:self.uuidTitle];
    }
}

YYSYNTH_DYNAMIC_PROPERTY_OBJECT(uuidTitle, setUuidTitle, COPY, NSString *)

- (NSString *)createUuidTitle{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef strUUID = CFUUIDCreateString(NULL, uuid);
    NSString *ret = [(__bridge NSString *) strUUID lowercaseString];
    CFRelease(strUUID);
    CFRelease(uuid);
    return ret;
}

@end


@interface NSString (MenuImageSupport) @end

@implementation NSString (MenuImageSupport)

+ (void)load{
    [super load];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        SEL selectors[] = {
            @selector(copyWithZone:),
            @selector(sizeWithAttributes:)
        };
        
        for (NSUInteger index = 0; index < sizeof(selectors) / sizeof(SEL); ++index) {
            SEL originalSel = selectors[index];
            SEL swizzledSel = NSSelectorFromString([@"fs_swizzled_" stringByAppendingString:NSStringFromSelector(originalSel)]);
            
            [self fs_exchangeImpWithOriginalSel:originalSel swizzledSel:swizzledSel];
        }
    });
}

- (CGSize)fs_swizzled_sizeWithAttributes:(NSDictionary *)attributes{
    if ([self doesWrapInvisibleIdentifiers]) {
        CGSize size = [titleImagePairs[self] size];
        if (size.width > 36.f) {
            size = CGSizeMake(36., 36.f);
        }
        return size;
    }
    return [self fs_swizzled_sizeWithAttributes:attributes];
}

- (BOOL)doesWrapInvisibleIdentifiers{
    BOOL doesStartMatch = [self rangeOfString:INVISIBLE_IDENTIFIER options:NSAnchoredSearch].location != NSNotFound;
    if (!doesStartMatch){
        return NO;
    }
    BOOL doesEndMatch = [self rangeOfString:INVISIBLE_IDENTIFIER options:NSAnchoredSearch | NSBackwardsSearch].location != NSNotFound;
    return doesEndMatch;
}

@end

@interface UILabel (MenuImageSupport) @end

@implementation UILabel (MenuImageSupport)


+ (void)load{
    [super load];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        SEL selectors[] = {
            @selector(drawTextInRect:),
            @selector(setFrame:)
        };
        
        for (NSUInteger index = 0; index < sizeof(selectors) / sizeof(SEL); ++index) {
            SEL originalSel = selectors[index];
            SEL swizzledSel = NSSelectorFromString([@"fs_swizzled_" stringByAppendingString:NSStringFromSelector(originalSel)]);
            [self fs_exchangeImpWithOriginalSel:originalSel swizzledSel:swizzledSel];
        }
    });
}

- (void)fs_swizzled_drawTextInRect:(CGRect)rect{
    
    if (![self.text doesWrapInvisibleIdentifiers] ||
        !titleImagePairs[self.text]){
        [self fs_swizzled_drawTextInRect:rect];
        return;
    }
    
    UIImage *img = titleImagePairs[self.text];
    CGSize size = img.size;
    CGPoint point = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    point.x = ceilf(point.x - size.width/2);
    point.y = ceilf(point.y - size.height/2);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, img.scale);
    [[UIColor whiteColor] setFill];
    CGRect bounds = CGRectMake(0, 0, size.width, size.height);
    UIRectFill(bounds);
    [img drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [tintedImage drawAtPoint:point];
}

- (void)fs_swizzled_setFrame:(CGRect)frame{
   
    if ([self.text doesWrapInvisibleIdentifiers] &&
        titleImagePairs[self.text]){
        frame = self.superview.bounds;
    }
     [self fs_swizzled_setFrame:frame];
}

@end

