/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <UIKit/UIKit.h>
#import "MASViewAttribute.h"

NS_CLASS_AVAILABLE_IOS(9_0)
@interface UILayoutGuide (FSMASAdditions)

@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_left;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_top;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_right;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_bottom;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_leading;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_trailing;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_width;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_height;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_centerX;
@property (nonatomic, strong, readonly) MASViewAttribute *fs_mas_centerY;

@end
