/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (Extensions)

+ (UIButton *)createButtonWithImageAndTitle:(nullable NSString *)title
                                imageNormal:(nullable UIImage *)imageNormal
                              imageSelected:(nullable UIImage *)imageSelected
                               imageDisable:(nullable UIImage *)imageDisabled;

@end

@interface UIButton (EnlargeEdge)

- (void)setEnlargedEdge:(CGFloat)enlargedEdge;

@end

NS_ASSUME_NONNULL_END
