/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIAlertController+Extensions.h"

@implementation UIAlertController (Extensions)

+ (UIAlertController *)fs_alertWithTitle:(NSString *)title message:(NSString *)message actions:(NSArray <NSString *>*)actions actionBack:(AlertActionCallBack)actionBack {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (int i = 0; i < actions.count; i ++) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:actions[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            !actionBack?:actionBack(i, action);
        }];
        [alert addAction:action];
    }
    return alert;
}

+ (void)fs_alertWithTitle:(NSString *)title message:(NSString *)message actions:(NSArray <NSString *>*)actions actionBack:(AlertActionCallBack)actionBack onCtrl:(UIViewController *)ctrl {
    [ctrl presentViewController:[self fs_alertWithTitle:title message:message actions:actions actionBack:actionBack] animated:YES completion:nil];
}


@end
