/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Extensions)
+ (UIWindow *)fs_getForegroundActiveWindow;
- (void)fs_addWeakAssociatedObject:(id)object forKey:(NSString *)key;
- (id)fs_getWeakAssociatedForKey:(NSString *)key;
- (void)fs_addAssociatedObject:(id)object forKey:(NSString *)key;
- (id)fs_getAssociatedForKey:(NSString *)key;
- (__kindof UIViewController *)fs_storyBoardViewControllerWithName:(NSString *)name identifier:(NSString *)identifier;
@end

@interface NSObject (MethodSwizzling)
+ (void)fs_exchangeImpWithOriginalSel:(SEL)originalSel swizzledSel:(SEL)swizzledSel;
+ (void)fs_exchangeImpWithClass:(Class)cls originalSel:(SEL)originalSel swizzledSel:(SEL)swizzledSel;
+ (void)fs_exchangeImpWithOriginalClass:(Class)oriCls swizzledClass:(Class)swiCls originalSel:(SEL)oriSel swizzledSel:(SEL)swiSel tmpSel:(SEL)tmpSel;
@end

NS_ASSUME_NONNULL_END
