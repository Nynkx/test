/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIViewController+Extensions.h"

@implementation UIViewController (Extensions)

- (void)fs_showHUDLoading:(NSString *_Nullable)msg{
    dispatch_main_async_safe(^{
        [self.view fs_showHUDLoading:msg];
    });
}

- (BOOL)fs_isCurrentViewControllerVisible{
    if ([NSThread isMainThread]) {
        return (self.isViewLoaded && self.view.window);
    }
    __block BOOL ret = NO;
    dispatch_semaphore_t wait = dispatch_semaphore_create(0);
    dispatch_async(dispatch_get_main_queue(), ^{
        ret = (self.isViewLoaded && self.view.window);
        dispatch_semaphore_signal(wait);
    });
    dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
    return ret;
}

@end

