/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIImage+Extensions.h"
#import <objc/runtime.h>
#import "YYKitMacro.h"

@implementation UIImage (Extensions)

+ (UIImage *)fs_ImageNamed:(NSString *)name{
    return [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:NSClassFromString(@"UIExtensionsManager")] compatibleWithTraitCollection:nil];
}

+ (UIImage *)fs_imageWithColor:(UIColor *)color size:(CGSize)size{
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)fs_applyingAlpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    if (!ctx)
        return nil;
    
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, self.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end

@implementation UIImage (DarkMode)

YYSYNTH_DYNAMIC_PROPERTY_OBJECT(fs_darkImage, setFs_darkImage, RETAIN, UIImage *);

+ (UIImage *)fs_ImageNamedForDark:(NSString *)name{
    UIImage *image = [self fs_ImageNamed:name];
    NSString *darkImageName = [NSString stringWithFormat:@"%@_dark",name];
    UIImage *darkImage = [self fs_ImageNamed:darkImageName];
    image.fs_darkImage = darkImage;
    return image;
}

+ (UIImage *)fs_imageWithLightName:(NSString *)lightName darkName:(NSString *)darkName{
    return [self fs_imageWithLight:[self fs_ImageNamed:lightName] dark:[self fs_ImageNamed:darkName]];
}

+ (UIImage *)fs_imageWithLight:(UIImage *)light dark:(UIImage *)dark{
    light.fs_darkImage = dark;
    return light;
}

@end


