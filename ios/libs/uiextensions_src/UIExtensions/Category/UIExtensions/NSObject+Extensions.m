/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "NSObject+Extensions.h"
#import <objc/runtime.h>

@implementation NSObject (Extensions)

+ (UIWindow *)fs_getForegroundActiveWindow{
    #if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_13_0
    if (@available(iOS 13.0, *)) {
        UIWindowScene *unattachedScene = nil;
        for (UIWindowScene *wScene in [UIApplication sharedApplication].connectedScenes){
            if (wScene.activationState == UISceneActivationStateUnattached){
                unattachedScene = wScene;
            }
            if (wScene.activationState == UISceneActivationStateForegroundActive){
                return wScene.windows.firstObject;
            }
        }
        if (![[[UIApplication sharedApplication] delegate] respondsToSelector:@selector(window)]) return unattachedScene.windows.firstObject;
    }
    #endif
    return [[[UIApplication sharedApplication] delegate] window];
}

- (void)fs_addWeakAssociatedObject:(id)object forKey:(NSString *)key{
    __weak id weakObject = object;
    objc_setAssociatedObject(self, (const void *)key.hash, ^{
        return weakObject;
    }, OBJC_ASSOCIATION_COPY);
}

- (id)fs_getWeakAssociatedForKey:(NSString *)key{
    id (^getter)(void) = objc_getAssociatedObject(self, (const void *)key.hash);
    if (getter) return getter();
    return nil;
}

- (void)fs_addAssociatedObject:(id)object forKey:(NSString *)key{
    objc_setAssociatedObject(self, (const void *)key.hash, object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)fs_getAssociatedForKey:(NSString *)key{
    return objc_getAssociatedObject(self, (const void *)key.hash);
}

- (__kindof UIViewController *)fs_storyBoardViewControllerWithName:(NSString *)name identifier:(NSString *)identifier{
    if ([NSStringFromClass([self class]) isEqualToString:@"NSObject"]) {
        @throw [NSException exceptionWithName:@"Foxit Exception" reason:@"The class must not be NSObject" userInfo:nil];
    }
    return [[UIStoryboard storyboardWithName:name bundle:[NSBundle bundleForClass:[self class]]] instantiateViewControllerWithIdentifier:identifier];
}
@end


@implementation NSObject(MethodSwizzling)

+ (void)fs_exchangeImpWithOriginalSel:(SEL)originalSel swizzledSel:(SEL)swizzledSel{
    [self fs_exchangeImpWithClass:[self class] originalSel:originalSel swizzledSel:swizzledSel];
}

+ (void)fs_exchangeImpWithClass:(Class)cls originalSel:(SEL)originalSel swizzledSel:(SEL)swizzledSel
{
    Method originalMethod = class_getInstanceMethod(cls, originalSel);
    Method swizzledMethod = class_getInstanceMethod(cls, swizzledSel);
    
    BOOL didAddMethod = class_addMethod(cls,originalSel,
                                        method_getImplementation(swizzledMethod),
                                        method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {
        class_replaceMethod(cls,swizzledSel,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
    
}

+ (void)fs_exchangeImpWithOriginalClass:(Class)oriCls swizzledClass:(Class)swiCls originalSel:(SEL)oriSel swizzledSel:(SEL)swiSel tmpSel:(SEL)tmpSel{
    
    Span_Class_ExchangeImp(oriCls, oriSel, swiCls, swiSel, tmpSel);
}

static void Span_Class_ExchangeImp(Class originalClass, SEL originalSel, Class swizzledClass, SEL swizzledSel, SEL noneSel){
    Method originalMethod = class_getInstanceMethod(originalClass, originalSel);
    Method swizzledMethod = class_getInstanceMethod(swizzledClass, swizzledSel);
    if (!originalMethod) {
        Method noneMethod = class_getInstanceMethod(swizzledClass, noneSel);
        class_addMethod(originalClass, originalSel, method_getImplementation(noneMethod), method_getTypeEncoding(noneMethod));
        originalMethod = class_getInstanceMethod(originalClass, originalSel);
    }
    BOOL didAddMethod = class_addMethod(originalClass, swizzledSel, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {
        Method newMethod = class_getInstanceMethod(originalClass, swizzledSel);
        method_exchangeImplementations(originalMethod, newMethod);
    }
}

@end

@interface NSObject (FixAppleBug)
@end

@implementation NSObject(FixAppleBug)

+ (void)load{
    if (@available(iOS 13.2, *)){
        #if DEBUG
        const char *className = "_UIScrollerImpContainerView";
        Class cls = objc_getClass(className);
        if (cls == nil) {
            cls = objc_allocateClassPair([UIView class], className, 0);
            objc_registerClassPair(cls);
            printf("added %s dynamically\n", className);
        }
        #endif
    }
}

@end
