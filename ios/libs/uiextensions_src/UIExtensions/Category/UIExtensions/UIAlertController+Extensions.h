/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <UIKit/UIKit.h>

typedef void (^AlertActionCallBack)(NSUInteger index, UIAlertAction * _Nonnull action);

NS_ASSUME_NONNULL_BEGIN

@interface UIAlertController (Extensions)

+ (void)fs_alertWithTitle:(NSString *)title message:(NSString *)message actions:(NSArray <NSString *>*)actions actionBack:(AlertActionCallBack)actionBack onCtrl:(UIViewController *)ctrl;

+ (UIAlertController *)fs_alertWithTitle:(NSString *)title message:(NSString *)message actions:(NSArray <NSString *>*)actions actionBack:(nullable AlertActionCallBack)actionBack;

@end

NS_ASSUME_NONNULL_END
