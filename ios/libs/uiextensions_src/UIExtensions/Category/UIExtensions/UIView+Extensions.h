/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import <UIKit/UIKit.h>


#ifndef dispatch_main_async_safe
#define dispatch_main_async_safe(block)\
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {\
        block();\
    } else {\
        dispatch_async(dispatch_get_main_queue(), block);\
    }
#endif

#ifndef dispatch_async_global
#define dispatch_async_global(block)\
    dispatch_async(dispatch_get_global_queue(0, 0), block);
#endif

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Extensions)

@property (nonatomic, setter=fs_setLeft:)    CGFloat fs_left;
@property (nonatomic, setter=fs_setTop:)     CGFloat fs_top;
@property (nonatomic, setter=fs_setRight:)   CGFloat fs_right;
@property (nonatomic, setter=fs_setBottom:)  CGFloat fs_bottom;
@property (nonatomic, setter=fs_setWidth:)   CGFloat fs_width;
@property (nonatomic, setter=fs_setHeight:)  CGFloat fs_height;
@property (nonatomic, setter=fs_setCenterX:) CGFloat fs_centerX;
@property (nonatomic, setter=fs_setCenterY:) CGFloat fs_centerY;
@property (nonatomic, setter=fs_setOrigin:)  CGPoint fs_origin;
@property (nonatomic, setter=fs_setSize:)    CGSize  fs_size;

- (UITableView *)fs_tableView;

- (UIViewController *)fs_viewController;

- (UIView *)fs_MBProgressHUD;

- (void)fs_showHUDCustomView:(UIView * _Nullable)customView;

- (void)fs_showHUDCustomView:(UIView * _Nullable)customView msg:(NSString * _Nullable)msg;

//queue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
- (void)fs_showHUDLoading:(NSString *_Nullable)msg process:(_Nullable dispatch_block_t)process;

- (void)fs_showHUDLoading:(NSString *)msg process:(dispatch_block_t)process synch:(BOOL)synch;

- (void)fs_showHUDLoading:(NSString *)msg process:(dispatch_block_t)process synch:(BOOL)synch onQueue:(dispatch_queue_t)queue;

- (void)fs_showHUDLoading:(NSString *_Nullable)msg;

- (void)fs_hideHUDLoading;

- (void)fs_showHUDMessage:(NSString *)msg;

- (void)fs_showHUDMessage:(NSString *)msg afterHideDelay:(NSTimeInterval)delay;
@end

@interface UIView (UIViewEnlargeEdge)

- (void)setEnlargedEdge:(CGFloat)enlargedEdge;

@end

NS_ASSUME_NONNULL_END
