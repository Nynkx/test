/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "UILayoutGuide+FSMASAdditions.h"

@implementation UILayoutGuide (FSMASAdditions)

#pragma mark - NSLayoutAttribute properties

- (MASViewAttribute *)fs_mas_left {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeLeft];
}

- (MASViewAttribute *)fs_mas_top {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeTop];
}

- (MASViewAttribute *)fs_mas_right {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeRight];
}

- (MASViewAttribute *)fs_mas_bottom {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeBottom];
}

- (MASViewAttribute *)fs_mas_leading {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeLeading];
}

- (MASViewAttribute *)fs_mas_trailing {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeTrailing];
}

- (MASViewAttribute *)fs_mas_width {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeWidth];
}

- (MASViewAttribute *)fs_mas_height {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeHeight];
}

- (MASViewAttribute *)fs_mas_centerX {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeCenterX];
}

- (MASViewAttribute *)fs_mas_centerY {
    return [[MASViewAttribute alloc] initWithView:self.owningView item:self layoutAttribute:NSLayoutAttributeCenterY];
}

@end
