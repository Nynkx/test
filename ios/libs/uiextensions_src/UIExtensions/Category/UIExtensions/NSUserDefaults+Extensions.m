/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "NSUserDefaults+Extensions.h"

static NSString *const CerTrustedInfos = @"CerTrustedInfos";
@implementation NSUserDefaults (Extensions)

+ (nullable id)fs_objectForKey:(NSString *)defaultName{
    return [self.standardUserDefaults objectForKey:defaultName];
}

+ (void)fs_setObject:(nullable id)value forKey:(NSString *)defaultName{
    [self.standardUserDefaults setObject:value forKey:defaultName];
    [self.standardUserDefaults synchronize];
}

+ (NSArray *)fs_getTrustedCerInfos{
    NSArray *arr = [self fs_objectForKey:CerTrustedInfos];
    if ([arr isKindOfClass:[NSArray class]]) {
        return arr;
    }
    return [NSArray array];
}

+ (void)fs_setTrustedCerInfo:(NSDictionary *)cerInfo{
    if (!cerInfo) return;
    if ([self fs_isTrustedCerInfo:cerInfo]) return;
    NSMutableArray *cerInfos = [self fs_getTrustedCerInfos].mutableCopy;
    if (!cerInfos) return;
    [cerInfos addObject:cerInfo];
    [self fs_setObject:cerInfos.copy forKey:CerTrustedInfos];
}

+ (void)fs_removeTrustedCerInfo:(NSDictionary *)cerInfo{
    if (![self fs_isTrustedCerInfo:cerInfo]) return;
    NSString *serialNumber = cerInfo[@"serialNumber"];
    if (!serialNumber.length) return;
    NSMutableArray *cerInfos = [self fs_getTrustedCerInfos].mutableCopy;
    if (!cerInfos) return;
    [cerInfos enumerateObjectsUsingBlock:^(NSDictionary  *_Nonnull cerInfo1, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([cerInfo1[@"serialNumber"] isEqualToString:serialNumber]) {
            [cerInfos removeObject:cerInfo1];
            [self fs_setObject:cerInfos.copy forKey:CerTrustedInfos];
            *stop = YES;
        }
    }];
}

+ (BOOL)fs_isTrustedCerInfo:(NSDictionary *)cerInfo{
    if (!cerInfo) return NO;
    NSString *serialNumber = cerInfo[@"serialNumber"];
    if (!serialNumber.length) return NO;
    NSMutableArray *cerInfos = [self fs_getTrustedCerInfos].mutableCopy;
    if (!cerInfos) return NO;
    for (NSDictionary  *_Nonnull cerInfo1 in cerInfos) {
        if ([cerInfo1[@"serialNumber"] isEqualToString:serialNumber]) {
            return YES;
        }
    }
    return NO;
}

@end
