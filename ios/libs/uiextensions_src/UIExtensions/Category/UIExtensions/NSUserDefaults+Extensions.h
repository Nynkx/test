/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (Extensions)

+ (nullable id)fs_objectForKey:(NSString *)defaultName;

+ (void)fs_setObject:(nullable id)value forKey:(NSString *)defaultName;

+ (NSArray *)fs_getTrustedCerInfos;

+ (void)fs_setTrustedCerInfo:(NSDictionary *)cerInfo;

+ (void)fs_removeTrustedCerInfo:(NSDictionary *)cerInfo;

+ (BOOL)fs_isTrustedCerInfo:(NSDictionary *)cerInfo;

@end

NS_ASSUME_NONNULL_END
