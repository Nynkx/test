/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Extensions)

+ (UIImage *)fs_ImageNamed:(NSString *)name;
+ (UIImage *)fs_imageWithColor:(UIColor *)color size:(CGSize)size;
- (UIImage *)fs_applyingAlpha:(CGFloat)alpha;

@end

@interface UIImage (DarkMode)

@property (nonatomic, strong) UIImage *fs_darkImage;

+ (UIImage *)fs_ImageNamedForDark:(NSString *)name;
+ (UIImage *)fs_imageWithLightName:(NSString *)lightName darkName:(NSString *)darkName;
+ (UIImage *)fs_imageWithLight:(UIImage *)light dark:(UIImage *)dark;
@end


NS_ASSUME_NONNULL_END
