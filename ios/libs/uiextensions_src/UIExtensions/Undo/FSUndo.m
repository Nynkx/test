/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSUndo.h"

@interface UndoItem ()
@property (nonatomic, strong) FSAnnotAttributes *attributes;
@property (nonatomic, strong) NSArray<FSAnnotAttributes *> *attributesArray;
@end

@implementation UndoItem

+ (instancetype)itemWithUndo:(UndoBlock)undo redo:(RedoBlock)redo pageIndex:(int)pageIndex {
    return [self itemWithUndo:undo redo:redo pageIndex:pageIndex attributes:nil];
}

+ (instancetype)itemWithUndo:(UndoBlock)undo redo:(RedoBlock)redo pageIndex:(int)pageIndex attributes:(id)attributes{
    return [[UndoItem alloc] initWithUndo:undo redo:redo pageIndex:pageIndex attributes:attributes];
}

+ (instancetype)itemByMergingItems:(NSArray<UndoItem *> *)items {
    NSMutableArray<UndoBlock> *undoArray = [NSMutableArray<UndoBlock> array];
    NSMutableArray<RedoBlock> *redoArray = [NSMutableArray<UndoBlock> array];
    NSMutableArray<FSAnnotAttributes *> *attributesArray = [NSMutableArray<FSAnnotAttributes *> array];
    for (UndoItem *undoItem in items) {
        [undoArray addObject:undoItem.undo];
        [redoArray addObject:undoItem.redo];
        [attributesArray addObject:undoItem.attributes];
    }
    return [UndoItem itemWithUndo:^(UndoItem *item) {
        for (UndoBlock undo in undoArray) {
            undo(item);
        }
    }
        redo:^(UndoItem *item) {
            for (RedoBlock redo in redoArray) {
                redo(item);
            }
        }
        pageIndex:[items objectAtIndex:0].pageIndex attributes:attributesArray];
}

- (instancetype)initWithUndo:(UndoBlock)undo redo:(RedoBlock)redo pageIndex:(int)pageIndex attributes:(FSAnnotAttributes *)attributes;
{
    if (self = [super init]) {
        self.undo = undo;
        self.redo = redo;
        self.pageIndex = pageIndex;
        if ([attributes isKindOfClass:[NSArray class]]) {
            self.attributesArray = (NSArray *)attributes;
        }else{
            self.attributes = attributes;
        }
    }
    return self;
}

+ (instancetype)itemForUndoGroupsAnnotAttributes:(FSGroupsAnnotAttributes *)groupsAttributes completed:(dispatch_block_t)completed page:(FSPDFPage *)page ungroup:(BOOL)ungroup{
    BOOL (^doGroupBlock) (BOOL group) = ^(BOOL group){
        __block BOOL ret = YES;
        if (group) {
            FSMarkupArray *markupArray = [[FSMarkupArray alloc] init];
            for (int i = 0; i < groupsAttributes.groupAnnotAttributes.count; i ++) {
                FSAnnot *annot = [Utility getAnnotByNM:(groupsAttributes.groupAnnotAttributes[i]).NM inPage:page];
                FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
                [markupArray add:markup];
            }
            return [page setAnnotGroup:markupArray header_index:groupsAttributes.header_index];
        }else{
            FSAnnot *annot = [Utility getAnnotByNM:groupsAttributes.groupHeaderAnnotAttributes.NM inPage:page];
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            if (markup.isGrouped) {
                if (![markup ungroup]) ret = NO;
            }
            
            if (groupsAttributes.groupsAnnotAttributes.count > 1) {
                [groupsAttributes.groupsAnnotAttributes enumerateObjectsUsingBlock:^(FSGroupAnnotAttributes * _Nonnull groupAttributes, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (groupAttributes.groupAnnotAttributes.count > 1) {
                        FSMarkupArray *markupArray = [[FSMarkupArray alloc] init];
                        for (int i = 0; i < groupAttributes.groupAnnotAttributes.count; i ++) {
                            FSAnnot *annot = [Utility getAnnotByNM:(groupsAttributes.groupAnnotAttributes[i]).NM inPage:page];
                            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
                            [markupArray add:markup];
                        }
                        if (![page setAnnotGroup:markupArray header_index:groupAttributes.header_index]) ret = NO;
                    }
                }];
            }
            return ret;
        }
    };
    return [UndoItem itemWithUndo:^(UndoItem *item) {
            doGroupBlock(ungroup);
            completed();
    }redo:^(UndoItem *item) {
            doGroupBlock(!ungroup);
            completed();
    }pageIndex:groupsAttributes.pageIndex
    attributes:groupsAttributes.groupAnnotAttributes];
}

+ (instancetype)itemForUndoModifyOldAnnotsAttributes:(NSArray *)oldAnnotsAttributes newAnnotsAttributes:(NSArray *)newAnnotsAttributes pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl page:(FSPDFPage *)page{
    if (oldAnnotsAttributes.count && newAnnotsAttributes.count && pdfViewCtrl && page) {
        int pageIndex = page.getIndex;
        return [UndoItem itemWithUndo:^(UndoItem *item) {
                for (FSAnnotAttributes *attributes in oldAnnotsAttributes) {
                    FSAnnot *annot = [Utility getAnnotByNM:attributes.NM inPage:page];
                    [attributes resetAnnotFromDict:annot];
                }
                [pdfViewCtrl refresh:pageIndex needRender:YES];
            }redo:^(UndoItem *item) {
                for (FSAnnotAttributes *attributes in newAnnotsAttributes) {
                    FSAnnot *annot = [Utility getAnnotByNM:attributes.NM inPage:page];
                    [attributes resetAnnotFromDict:annot];
                }
                [pdfViewCtrl refresh:pageIndex needRender:YES];
            }pageIndex:pageIndex];
    }
    return nil;
}

+ (instancetype)itemForUndoModifyAnnotWithOldAttributes:(FSAnnotAttributes *)oldAttributes newAttributes:(FSAnnotAttributes *)newAttributes pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl page:(FSPDFPage *)page annotHandler:(id<IAnnotHandler>)annotHandler {
    if (oldAttributes && newAttributes && pdfViewCtrl && page && annotHandler) {
        int pageIndex = [page getIndex];
        return [UndoItem itemWithUndo:^(UndoItem *item) {
            FSAnnot *annot = [Utility getAnnotByNM:oldAttributes.NM inPage:page];
            CGRect currentPvRect = [pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
            [annot applyAttributes:oldAttributes];
            [pdfViewCtrl refresh:CGRectInset(currentPvRect, -30, -30) pageIndex:pageIndex];
            [annotHandler modifyAnnot:annot addUndo:NO];
        }
            redo:^(UndoItem *item) {
                FSAnnot *annot = [Utility getAnnotByNM:newAttributes.NM inPage:page];
                CGRect currentPvRect = [pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
                [annot applyAttributes:newAttributes];
                [pdfViewCtrl refresh:CGRectInset(currentPvRect, -30, -30) pageIndex:pageIndex];
                [annotHandler modifyAnnot:annot addUndo:NO];
            }
            pageIndex:pageIndex
                attributes:newAttributes];
    }
    return nil;
}

+ (instancetype)itemForUndoAddAnnotWithAttributes:(FSAnnotAttributes *)attributes page:(FSPDFPage *)page annotHandler:(id<IAnnotHandler>)annotHandler {
    if (attributes && page && annotHandler) {
        return [UndoItem itemWithUndo:^(UndoItem *item) {
            FSAnnot *annot = [Utility getAnnotByNM:attributes.NM inPage:page];
            if (annot && ![annot isEmpty]) {
                [annotHandler removeAnnot:annot addUndo:NO];
            }
        }
            redo:^(UndoItem *item) {
                FSAnnot *annot = nil;
                BOOL isReply = (attributes.type == FSAnnotNote && ((FSNoteAttributes *) attributes).replyTo.length > 0);
                BOOL isRedact = (attributes.type == FSAnnotRedact);
                BOOL isForm = (attributes.type == FSAnnotWidget);
                if (isRedact) {
                    @try {
                        FSRedaction *redaction = [[FSRedaction alloc] initWithDocument:page.getDocument];
                        FSRedact *redact = [redaction markRedactAnnot:page rects:((FSRedactAttributes *)attributes).rects];
                        annot = redact;
                    } @catch (NSException *exception) {
                        return ;
                    }
                }else if (isForm){
                    FSWidgetAttributes *widgetAttributes = (FSWidgetAttributes *)attributes;
                    annot = [widgetAttributes redoWidgetWithPage:page];
                }
                else if (!isReply) {
                    annot = [page addAnnot:attributes.type rect:attributes.rect];
                } else {
                    FSMarkup *parent = [[FSMarkup alloc] initWithAnnot:[Utility getAnnotByNM:((FSNoteAttributes *) attributes).replyTo inPage:page]];
                    annot = [parent addReply];
                }
                if (annot && ![annot isEmpty]) {
                    [attributes resetAnnot:annot];
                    [annotHandler addAnnot:annot addUndo:NO];
                }
            }
            pageIndex:[page getIndex]
                           attributes:attributes];
    }
    return nil;
}

+ (instancetype)itemForUndoDeleteAnnotWithAttributes:(FSAnnotAttributes *)attributes page:(FSPDFPage *)page annotHandler:(id<IAnnotHandler>)annotHandler {
    if (attributes && page && annotHandler) {
        return [UndoItem itemWithUndo:^(UndoItem *item) {
            FSAnnot *annot = nil;
            BOOL isReply = (attributes.type == FSAnnotNote && ((FSNoteAttributes *) attributes).replyTo.length > 0);
            BOOL isRedact = (attributes.type == FSAnnotRedact);
            BOOL isForm = (attributes.type == FSAnnotWidget);
            if (isRedact) {
                @try {
                    FSRedaction *redaction = [[FSRedaction alloc] initWithDocument:page.getDocument];
                    FSRedact *redact = [redaction markRedactAnnot:page rects:((FSRedactAttributes *)attributes).rects];
                    annot = redact;
                } @catch (NSException *exception) {
                    return ;
                }
            }else if (isForm){
                FSWidgetAttributes *widgetAttributes = (FSWidgetAttributes *)attributes;
                annot = [widgetAttributes redoWidgetWithPage:page];
            }
            else if (!isReply) {
                @try {
                    annot = [page addAnnot:attributes.type rect:attributes.rect];
                } @catch (NSException *exception) {
                    if (![[exception name] isEqualToString:@"FSErrParam"]) {@throw exception;}
                }
            } else {
                FSMarkup *parent = [[FSMarkup alloc] initWithAnnot:[Utility getAnnotByNM:((FSNoteAttributes *) attributes).replyTo inPage:page]];
                annot = [parent addReply];
            }
            if (annot && ![annot isEmpty]) {
                [attributes resetAnnot:annot];
                [annotHandler addAnnot:annot addUndo:NO];
                if (annot.isMarkup) {
                    FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
                    for (int i = 0; i < attributes.replyAnnotAttributes.count; i++) {
                        FSAnnotAttributes *replyAttributes = attributes.replyAnnotAttributes[i];
                        FSNote *reply = [markup addReply];
                        [replyAttributes resetAnnot:reply];
                        [annotHandler addAnnot:reply];
                    }
                }
            }
        }
            redo:^(UndoItem *item) {
                FSAnnot *annot = [Utility getAnnotByNM:attributes.NM inPage:page];
                if (annot && ![annot isEmpty]) {
                    [annotHandler removeAnnot:annot addUndo:NO];
                }
            }
            pageIndex:[page getIndex]
                           attributes:attributes];
    }
    return nil;
}

+ (NSArray <UndoItem *>*)itemForUndoItemsWithAttributes:(FSAnnotAttributes *)attributes extensionsManager:(UIExtensionsManager *)extensionsManager replyAttributes:(NSArray<FSAnnotAttributes *> *)replyAttributes{
    NSMutableArray *undoItems = @[].mutableCopy;
    [self getNeedRemoveItems:extensionsManager.undoItems containerItems:undoItems attributes:attributes replyAttributes:replyAttributes];
    [self getNeedRemoveItems:extensionsManager.redoItems containerItems:undoItems attributes:attributes replyAttributes:replyAttributes];
    return undoItems.copy;
}

+ (void)getNeedRemoveItems:(NSMutableArray *)items containerItems:(NSMutableArray *)containerItems attributes:(FSAnnotAttributes *)attributes replyAttributes:(NSArray<FSAnnotAttributes *> *)replyAttributes{
    for (UndoItem *item in items) {
        if ([item isKindOfClass:[NSMutableArray class]]) {
            NSMutableArray *multipleItems = (NSMutableArray *)item;
            [self getNeedRemoveItems:multipleItems containerItems:containerItems attributes:attributes replyAttributes:replyAttributes];
            continue;
        }
        if (item.attributesArray) {
            NSArray *attributesArray = item.attributesArray;
            for (FSAnnotAttributes *attribute in attributesArray) {
                if ([attribute.NM isEqualToString:attributes.NM] && attribute.pageIndex == attributes.pageIndex) {
                    [containerItems addObject:item];
                    break;
                }
            }
        }else{
            if ([item.attributes.NM isEqualToString:attributes.NM] && item.pageIndex == attributes.pageIndex){
                [containerItems addObject:item];
            }
            else if (replyAttributes.count) {
                for (FSAnnotAttributes *replyAttribute in replyAttributes) {
                    if ([item.attributes.NM isEqualToString:replyAttribute.NM] && item.attributes.pageIndex == replyAttribute.pageIndex){
                        [containerItems addObject:item];
                    }
                }
            }
        }
    }
}

@end
