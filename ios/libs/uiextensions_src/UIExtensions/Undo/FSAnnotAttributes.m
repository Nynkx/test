/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSAnnotAttributes.h"
#import "FSAnnotExtent.h"
#import "Utility.h"
#import "FSAnnot+Extensions.h"

static inline BOOL CGFloatEqual(CGFloat v1, CGFloat v2) {
    return fabs(v1 - v2) < 1e-4;
}

static BOOL stringsEqual(NSString *str1, NSString *str2) {
    return (str1 == nil && str2 == nil) || [str1 isEqualToString:str2];
}

static BOOL pointEqual(FSPointF *pt1, FSPointF *pt2) {
    return CGFloatEqual(pt1.x, pt2.x) && CGFloatEqual(pt1.y, pt2.y);
}

static BOOL pointsEqual(FSPointFArray *array1, FSPointFArray *array2) {
    if ([array1 getSize] != [array2 getSize]) {
        return NO;
    }
    for (int i = 0; i < [array1 getSize]; i++) {
        if (!pointEqual([array1 getAt:i], [array2 getAt:i])) {
            return NO;
        }
    }
    return YES;
}

static BOOL borderInfoEqual(FSBorderInfo *border1, FSBorderInfo *border2) {
    if (border1 == nil && border1 == nil) {
        return YES;
    }
    return border1 && border2 &&
           [border1 getStyle] == [border2 getStyle] &&
           CGFloatEqual([border1 getWidth], [border2 getWidth]);
}

static BOOL (isQuadsEqual)(NSArray *quads1, NSArray *quads2) {
    if (quads1.count != quads2.count) {
        return NO;
    }
    for (int i = 0; i < quads1.count; i++) {
        if (![Utility quadsEqualToQuads:quads1[i] quads:quads2[i]]) {
            return NO;
        }
    }
    return YES;
}

static BOOL (isChoiceOptionsEqual)(FSChoiceOptionArray *options1, FSChoiceOptionArray *options2) {
    if (options1.getSize != options1.getSize) {
        return NO;
    }
    for (int i = 0; i < options1.getSize; i++) {
        FSChoiceOption *option1 = [options1 getAt:i];
        FSChoiceOption *option2 = [options2 getAt:i];
        if (!([option1.option_value isEqualToString:option2.option_value] &&
              [option1.option_label isEqualToString:option2.option_label] &&
              (option1.selected == option2.selected) &&
              (option1.default_selected == option2.default_selected))) {
            return NO;
        }
    }
    return YES;
}

static NSDictionary * (clonePdfDictToDic)(FSPDFDictionary *pdfDict) {
    __block NSMutableDictionary *muDict = [NSMutableDictionary dictionary];
    long pos = [pdfDict moveNext:0];
    int index=0;
    dispatch_semaphore_t wait = dispatch_semaphore_create(0);
    dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_USER_INITIATED, 0);
    dispatch_queue_t queue = dispatch_queue_create("com.foxit.rdk.clone.pdfdict", attr);
    dispatch_group_t group = dispatch_group_create();
    while (pos != 0) {
        dispatch_group_async(group, queue, ^{
            if(!muDict) return;
            @try {
                NSString* strKey = [pdfDict getKey:pos];
                FSPDFObject* getObj = [pdfDict getValue:pos];
                [muDict setValue:[getObj cloneObject] forKey:strKey];
            } @catch (NSException *exception) {
                muDict = nil;
            }
        });
        long nextpos = [pdfDict moveNext:pos];
        pos = nextpos;
        index++;
    }
    dispatch_group_notify(group, queue, ^{
        dispatch_semaphore_signal(wait);
    });
    dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
    return muDict.copy;
}

static NSDictionary * (cloneAnnotDictToDict)(FSAnnot *annot) {
    NSMutableDictionary *muDict = clonePdfDictToDic(annot.getDict).mutableCopy;
    if (annot.getType == FSAnnotWidget){
        FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
        if ([[widget getField] getType] == FSFieldTypeRadioButton){
            for (NSString *key in [muDict allKeys]){
                if ([key isEqualToString:@"Parent"]){
                    [muDict removeObjectForKey:key];
                    break;
                }
            }
        }
    }

    return muDict.copy;
}


static void (resetDictToPdfDict)(NSDictionary *dict, FSPDFDictionary *pdfDict) {
    for (NSString *key in [dict allKeys]){
        FSPDFObject *obj = [dict valueForKey:key];
        @try {
            [pdfDict setAt:key pdf_object:obj];
        } @catch (NSException *exception) {
        }
    }
}

@interface FSAnnotAttributes ()
@property (nonatomic, assign) BOOL useDict;
@end
@implementation FSAnnotAttributes

+ (instancetype)attributesWithAnnot:(FSAnnot *)annot {
    Class attributesClass = nil;
    switch (annot.type) {
    case FSAnnotNote:
        attributesClass = [FSNoteAttributes class];
        break;
    case FSAnnotCaret:
        attributesClass = [FSCaretAttributes class];
        break;
    case FSAnnotStrikeOut:
    case FSAnnotHighlight:
    case FSAnnotSquiggly:
    case FSAnnotUnderline:
        attributesClass = [FSTextMarkupAttributes class];
        break;
    case FSAnnotLine:
        attributesClass = [FSLineAttributes class];
        break;
    case FSAnnotInk:
        attributesClass = [FSInkAttributes class];
        break;
    case FSAnnotStamp:
        attributesClass = [FSStampAttributes class];
        break;
    case FSAnnotCircle:
    case FSAnnotSquare:
        attributesClass = [FSShapeAttributes class];
        break;
    case FSAnnotFreeText:
        attributesClass = [FSFreeTextAttributes class];
        break;
    case FSAnnotFileAttachment:
        attributesClass = [FSFileAttachmentAttributes class];
        break;
    case FSAnnotScreen:
        attributesClass = [FSScreenAttributes class];
        break;
    case FSAnnotPolygon:
        attributesClass = [FSPolygonAttributes class];
        break;
    case FSAnnotPolyLine:
        attributesClass = [FSPolyLineAttributes class];
        break;
    case FSAnnotRedact:
        attributesClass = [FSRedactAttributes class];
        break;
    case FSAnnotWidget:
        attributesClass = [FSWidgetAttributes class];
        break;
    default:
        attributesClass = [FSAnnotAttributes class];
    }
    return [[attributesClass alloc] initWithAnnot:annot];
}

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSMarkupArray *markupArray = [annot canGetGroupElements];
    return [self initWithAnnot:annot needGroupElements:markupArray.getSize];
}

- (instancetype)initWithAnnot:(FSAnnot *)annot useDict:(BOOL)useDict{
    self = [FSAnnotAttributes  attributesWithAnnot:annot];
    if (self) {
        [self cloneAnnotDict:annot];
        _useDict = YES;
    }
    return self;
}

- (instancetype)initWithAnnot:(FSAnnot *)annot needGroupElements:(BOOL)needGroupElements {
    if (self = [super init]) {
        self.pageIndex = annot.pageIndex;
        self.type = annot.type;
        self.NM = annot.NM;
        self.author = annot.author;
        self.rect = annot.fsrect;
        self.color = annot.color;
        self.opacity = annot.opacity;
        self.creationDate = annot.createDate;
        self.modificationDate = annot.modifiedDate;
        self.flags = annot.flags;
        self.content = annot.content;
        [self saveReplyAnnotAttributes:annot];
        if (needGroupElements){
            FSMarkupArray *markupArray = [annot canGetGroupElements];
            if (markupArray) {
                self.isGroup = YES;
                self.groupAnnotAttributes = [[FSGroupAnnotAttributes alloc] initWithAnnot:annot];
            }
        }
    }
    return self;
}

- (void)saveReplyAnnotAttributes:(FSAnnot *)annot{
    NSArray *replyAnnots = annot.replyAnnots;
    if (replyAnnots.count) {
        NSMutableArray *tmp = @[].mutableCopy;
        for (FSAnnot *reply in replyAnnots) {
            FSAnnotAttributes *attributes = [FSAnnotAttributes  attributesWithAnnot:reply];
            [tmp addObject:attributes];
        }
        _replyAnnotAttributes = tmp.copy;
    }
}

- (void)clonePageDict:(FSPDFPage *)page{
    self.pageDict = clonePdfDictToDic(page.getDict);
}

- (void)cloneAnnotDict:(FSAnnot *)annot{
    self.dict = clonePdfDictToDic(annot.getDict);
}

- (void)resetAnnotFromDict:(FSAnnot *)annot{
    resetDictToPdfDict(self.dict, annot.getDict);
    [self cloneAnnotDict:annot];
    if (self.isGroup) [self resetAnnotGroup:annot];
//MARK: If you use a dictionary to save the uniqueness of annot you need to set NM.
//    if (![annot.NM isEqualToString:self.NM]) {
//        annot.NM = self.NM;
//    }
}

- (void)resetPageFromDict:(FSPDFPage *)page{
    resetDictToPdfDict(self.pageDict, page.getDict);
    [self clonePageDict:page];
}

- (void)resetAnnot:(FSAnnot *)annot {
    if (![annot.NM isEqualToString:self.NM]) annot.NM = self.NM;
    annot.author = self.author;
    annot.fsrect = self.rect;
    annot.color = self.color;
    annot.opacity = self.opacity;
    annot.createDate = self.creationDate;
    annot.modifiedDate = self.modificationDate;
    annot.flags = self.flags;
    annot.content = self.content;
    if (self.isGroup) [self resetAnnotGroup:annot];
}

- (void)resetAnnotGroup:(FSAnnot *)annot {
    FSMarkup *markup = annot.canGetGroupHeader;
    if (!markup) {
        FSMarkupArray *markupArray = [[FSMarkupArray alloc] init];
        for (int i = 0; i < self.groupAnnotAttributes.groupAnnotAttributes.count; i++) {
            NSString *NM = (self.groupAnnotAttributes.groupAnnotAttributes[i]).NM;
            FSAnnot *annoti = [Utility getAnnotByNM:NM inPage:[annot getPage]];
            if (annoti) {
                FSMarkup *markupi = [[FSMarkup alloc] initWithAnnot:annoti];
                [markupArray add:markupi];
            }
        }
        if (markupArray.getSize > 1) {
            if (markupArray.getSize > self.groupAnnotAttributes.header_index) {
                [annot.getPage setAnnotGroup:markupArray header_index:self.groupAnnotAttributes.header_index];
            }else{
                [annot.getPage setAnnotGroup:markupArray header_index:0];
            }
        }
    }
}

//- (BOOL)isEqualToAttributesFromDict:(FSAnnotAttributes *)attributes {
//    return self.dict == attributes.dict;
//}


- (BOOL)isEqualToAttributes:(FSAnnotAttributes *)attributes {
    return self.type == attributes.type &&
           [Utility rectEqualToRect:self.rect
                               rect:attributes.rect] &&
           stringsEqual(self.author, attributes.author) &&
           self.color == attributes.color &&
           self.opacity == attributes.opacity &&
           [self.creationDate compare:attributes.creationDate] == NSOrderedSame &&
           [self.modificationDate compare:attributes.modificationDate] == NSOrderedSame &&
           self.replyAnnotAttributes.count == attributes.replyAnnotAttributes.count &&
           self.flags == attributes.flags;
}

@end

@implementation FSMarkupAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot needGroupElements:(BOOL)needGroupElements {
    if (self = [super initWithAnnot:annot needGroupElements:needGroupElements]) {
        if (![annot isEmpty] && [annot isMarkup]) {
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            if (![markup.popup isEmpty]) {
                self.popupAttributes = [FSAnnotAttributes attributesWithAnnot:markup.popup];
            }
        }
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot{
    [super resetAnnot:annot];
    FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
    if (self.popupAttributes && markup.popup && ![markup.popup isEmpty]) {
        FSPopup *popup =  markup.popup;
        [self.popupAttributes resetAnnot:popup];
    }else if(self.popupAttributes){
        FSAnnot *annotPopup = [markup.getPage addAnnot:FSAnnotPopup rect:self.popupAttributes.rect];
        FSPopup *popup = [[FSPopup alloc] initWithAnnot:annotPopup];
        [self.popupAttributes resetAnnot:popup];
        if (![popup isEmpty]) {
            markup.popup = popup;
        }
    }
}

@end

@implementation FSNoteAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSNote *note = [[FSNote alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:note]) {
        self.icon = note.icon;
        FSAnnot *replyTo = [note getReplyTo];
        if (replyTo && ![replyTo isEmpty]) {
            self.replyTo = replyTo.NM;
        }
        self.contents = note.contents;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSNote *note = [[FSNote alloc] initWithAnnot:annot];
    assert(note.type == FSAnnotNote);
    [super resetAnnot:note];
    note.icon = self.icon;
    note.contents = self.contents;
    [note resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSNoteAttributes *)attributes {
    return [attributes class] == [self class] &&
           self.icon == attributes.icon &&
           stringsEqual(self.contents, attributes.contents) &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSCaretAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSCaret *caret = [[FSCaret alloc] initWithAnnot:annot];
    assert(caret.type == FSAnnotCaret);
    if (self = [super initWithAnnot:caret]) {
        self.contents = caret.contents;
        self.subject = caret.subject;
        self.intent = caret.intent;
        self.innerRect = [caret getInnerRect];
        FSPDFDictionary *dict = [caret getDict];
        self.rotation = [dict hasKey:@"Rotate"] ? [[dict getElement:@"Rotate"] getInteger] : 0;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSCaret *caret = [[FSCaret alloc] initWithAnnot:annot];
    assert(caret.type == FSAnnotCaret);
    caret.contents = self.contents;
    caret.subject = self.subject;
    caret.intent = self.intent;
    [caret setInnerRect:self.innerRect];
    if (self.rotation != 0) {
        [[caret getDict] setAt:@"Rotate" pdf_object:[FSPDFObject createFromInteger:self.rotation]];
    }
    
    [super resetAnnot:caret];
    [caret resetAppearanceStream]; // neccessary?
}

- (BOOL)isEqualToAttributes:(FSCaretAttributes *)attributes {
    return [attributes class] == [self class] &&
           self.rotation == attributes.rotation &&
           [super isEqualToAttributes:attributes] &&
           stringsEqual(self.contents, attributes.contents) &&
           stringsEqual(self.intent, attributes.intent) &&
           stringsEqual(self.subject, attributes.subject);
}

@end

@implementation FSTextMarkupAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSTextMarkup *markup = [[FSTextMarkup alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:markup]) {
        self.quads = markup.quads;
        self.subject = markup.subject;
        self.contents = markup.contents;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSTextMarkup *textmk = [[FSTextMarkup alloc] initWithAnnot:annot];
    [super resetAnnot:textmk];
    textmk.quads = self.quads;
    textmk.subject = self.subject;
    textmk.contents = self.contents;
    [textmk resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSTextMarkupAttributes *)attributes {
    BOOL (^isQuadsEqual)(void) = ^BOOL {
        if (self.quads.count != attributes.quads.count) {
            return NO;
        }
        for (int i = 0; i < self.quads.count; i++) {
            if (![Utility quadsEqualToQuads:self.quads[i] quads:attributes.quads[i]]) {
                return NO;
            }
        }
        return YES;
    };
    return [super isEqualToAttributes:attributes] &&
           stringsEqual(self.contents, attributes.contents) &&
           stringsEqual(self.subject, attributes.subject) &&
           isQuadsEqual();
}

@end

@implementation FSLineAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSLine *line = [[FSLine alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:line]) {
        self.lineWidth = line.lineWidth;
        self.subject = line.subject;
        self.intent = line.intent;
        self.startPoint = [line getStartPoint];
        self.endPoint = [line getEndPoint];
        self.startPointStyle = [line getLineStartStyle];
        self.endPointStyle = [line getLineEndStyle];
        self.captionStyle = [line getCaptionPositionType];
        self.captionOffset = [line getCaptionOffset];
        self.fillColor = [line getStyleFillColor];
        self.leaderLineLength = line.leaderLineLength;
        self.leaderLineExtensionLength = line.leaderLineExtensionLength;
        self.leaderLineOffset = line.leaderLineOffset;
        
        self.contents = line.contents;
        if ([[line getIntent] isEqualToString:@"LineDimension"]){
            self.measureUnit = [line getMeasureUnit:0];
            self.measureRatio = [line getMeasureRatio];
            self.measureConversionFactor = [line getMeasureConversionFactor:0];
        }
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSLine *line = [[FSLine alloc] initWithAnnot:annot];
    [super resetAnnot:line];

    line.lineWidth = self.lineWidth;
    line.subject = self.subject;
    line.contents = self.contents;
    if (self.intent) {
        line.intent = self.intent;
    }
    [line setStartPoint:self.startPoint];
    [line setEndPoint:self.endPoint];
    [line setLineStartStyle:self.startPointStyle];
    [line setLineEndStyle:self.endPointStyle];
    [line setCaptionOffset:self.captionOffset];
    line.leaderLineLength = self.leaderLineLength;
    line.leaderLineExtensionLength = self.leaderLineExtensionLength;
    line.leaderLineOffset = self.leaderLineOffset;
    if (self.captionStyle) {
        [line setCaptionPositionType:self.captionStyle];
    }
    [line setStyleFillColor:self.fillColor];

    if ([self.intent isEqualToString:@"LineDimension"]){
        [line setMeasureUnit:0 unit:self.measureUnit];
        [line setMeasureRatio:self.measureRatio];
        [line setMeasureConversionFactor:0 factor:self.measureConversionFactor];
    }

    [line resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSLineAttributes *)attributes {
    return self.class == attributes.class &&
           stringsEqual(self.contents, attributes.contents) &&
           fabsf(self.lineWidth - attributes.lineWidth) < 1e-4 &&
           [Utility pointEqualToPoint:self.startPoint
                                point:attributes.startPoint] &&
           [Utility pointEqualToPoint:self.endPoint
                                point:attributes.endPoint] &&
           self.startPointStyle == attributes.startPointStyle &&
           self.endPointStyle == attributes.endPointStyle &&
           self.fillColor == attributes.fillColor &&
           [Utility pointEqualToPoint:self.captionOffset
                                point:attributes.captionOffset] &&
           stringsEqual(self.intent, attributes.intent) &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSInkAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSInk *ink = [[FSInk alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:ink]) {
        self.lineWidth = ink.lineWidth;
        self.inkList = [Utility cloneInkList:[ink getInkList]];
        if(!self.inkList)
            return nil;
        self.contents = ink.contents;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSInk *ink = [[FSInk alloc] initWithAnnot:annot];
    [super resetAnnot:ink];
    ink.lineWidth = self.lineWidth;
    [ink setInkList:self.inkList];
    ink.contents = self.contents;
    [ink resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSInkAttributes *)attributes {
    return self.class == attributes.class &&
           fabsf(self.lineWidth - attributes.lineWidth) < 1e-4 &&
           stringsEqual(self.contents, attributes.contents) &&
           [super isEqualToAttributes:attributes] &&
           [Utility inkListEqualToInkList:self.inkList
                                  inkList:attributes.inkList];
}

@end

@implementation FSStampAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSStamp *stamp = [[FSStamp alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:stamp]) {
        [self cloneAnnotDict:annot];
        self.iconName = [stamp getIconName];
        self.contents = stamp.contents;
        self.rotation = stamp.rotation;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSStamp *stamp = [[FSStamp alloc] initWithAnnot:annot];
    if (stamp.rotation != self.rotation) {
        stamp.rotation = self.rotation;
    }
    [stamp resetAppearanceStream];
    [super resetAnnotFromDict:annot];
}

- (BOOL)isEqualToAttributes:(FSStampAttributes *)attributes {
    return stringsEqual(self.iconName, attributes.iconName) &&
           stringsEqual(self.contents, attributes.contents) &&
           self.rotation == attributes.rotation &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSShapeAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    if (self = [super initWithAnnot:annot]) {
        [self cloneAnnotDict:annot];
        self.lineWidth = annot.lineWidth;
        self.subject = annot.subject;
        self.contents = annot.contents;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    [self resetAnnotFromDict:annot];
    [super resetAnnot:annot];
    annot.lineWidth = self.lineWidth;
    annot.subject = self.subject;
    annot.contents = self.contents;
    [annot resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSShapeAttributes *)attributes {
    return fabsf(self.lineWidth - attributes.lineWidth) < 1e-4 &&
           stringsEqual(self.contents, attributes.contents) &&
           stringsEqual(self.subject, attributes.subject) &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSFreeTextAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSFreeText *freetext = [[FSFreeText alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:freetext]) {
        [self cloneAnnotDict:annot];
        self.contents = freetext.contents;
        self.subject = freetext.subject;
        self.intent = freetext.intent;
        FSDefaultAppearance *ap = [freetext getDefaultAppearance];
        FSFont *font = ap.font;
        self.fontName = [font isEmpty] ? nil : [font getName];
        self.defaultAppearanceFlags = ap.flags;
        self.fontSize = ap.text_size;
        self.textColor = ap.text_color; // & 0xffffff; // to filter out alpha channel
        if ([self.intent isEqualToString:@"FreeTextCallout"]) {
            FSPointFArray *calloutPoints = [freetext getCalloutLinePoints];
            int numPoints = [calloutPoints getSize];
            if (numPoints == 3) {
                self.startPoint = [calloutPoints getAt:0];
                self.kneePoint = [calloutPoints getAt:1];
                self.endPoint = [calloutPoints getAt:2];
            } else if (numPoints == 2) {
                self.startPoint = [calloutPoints getAt:0];
                self.kneePoint = nil;
                self.endPoint = [calloutPoints getAt:1];
            } else {
                self.startPoint = nil;
                self.kneePoint = nil;
                self.endPoint = nil;
            }
            self.lineEndingStyle = [freetext getCalloutLineEndingStyle];
        }
        self.innerRect = [freetext getInnerRect];
        self.rotation = freetext.rotation;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    [self resetAnnotFromDict:annot];
    FSFreeText *freetext = [[FSFreeText alloc] initWithAnnot:annot];
    [super resetAnnot:freetext];
    freetext.contents = self.contents;
    freetext.subject = self.subject;
    freetext.intent = self.intent;
    freetext.rotation = self.rotation;
    {
        FSDefaultAppearance *ap = [[[FSFreeText alloc] initWithAnnot:freetext] getDefaultAppearance];
        ap.flags = self.defaultAppearanceFlags;
        if (self.fontName) {
            int fontID = [Utility toStandardFontID:self.fontName];
            FSFont *font = nil;
            if (fontID == -1) {
                font = [[FSFont alloc] initWithName:self.fontName styles:0 charset:FSFontCharsetDefault weight:0];
            } else {
                font = [[FSFont alloc] initWithFont_id:fontID];
            }
            ap.font = font;
        }
        ap.text_size = self.fontSize;
        ap.text_color = self.textColor;
        [freetext setDefaultAppearance:ap];
    }
    
    if ([self.intent isEqualToString:@"FreeTextCallout"]) {
        [freetext setInnerRect:self.innerRect];
        if (self.startPoint && self.endPoint) {
            FSPointFArray *points = [[FSPointFArray alloc] init];
            [points add:self.startPoint];
            if (self.kneePoint) {
                [points add:self.kneePoint];
            }
            [points add:self.endPoint];
            [freetext setCalloutLinePoints:points];
        }
        [freetext setCalloutLineEndingStyle:self.lineEndingStyle];
    }
    [freetext resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSFreeTextAttributes *)attributes {
    return self.class == attributes.class &&
           self.textColor == attributes.textColor &&
           self.fontSize == attributes.fontSize &&
           self.defaultAppearanceFlags == attributes.defaultAppearanceFlags &&
           stringsEqual(self.fontName, attributes.fontName) &&
           stringsEqual(self.contents, attributes.contents) &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSFileAttachmentAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSFileAttachment *attachment = [[FSFileAttachment alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:attachment]) {
        self.iconName = [attachment getIconName];
        self.fileName = [[attachment getFileSpec] getFileName];
        self.attachmentPath = [Utility getAttachmentTempFilePath:attachment];
        self.contents = attachment.contents;
        self.fileCreationTime = nil;
        @try {
            self.fileCreationTime = [[attachment getFileSpec] getCreationDateTime];
        } @catch (NSException *_) {
        }
        self.fileModificationTime = nil;
        @try {
            self.fileModificationTime = [[attachment getFileSpec] getModifiedDateTime];
        } @catch (NSException *_) {
        }
        if (![[NSFileManager defaultManager] fileExistsAtPath:self.attachmentPath]) {
            [Utility loadAttachment:attachment toPath:self.attachmentPath];
        }
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSFileAttachment *attachment = [[FSFileAttachment alloc] initWithAnnot:annot];
    [super resetAnnot:attachment];
    [attachment setIconName:self.iconName];
    attachment.contents = self.contents;

    if ([[NSFileManager defaultManager] fileExistsAtPath:self.attachmentPath]) {
        FSFileSpec *attachFile = [[FSFileSpec alloc] initWithDocument:[[attachment getPage] getDocument]];
        if (self.fileName.length > 0) {
            [attachFile setFileName:self.fileName];
        }
        if (attachFile && [attachFile embed:self.attachmentPath]) {
            if (self.fileCreationTime) {
                @try {
                    [attachFile setCreationDateTime:self.fileCreationTime];
                } @catch (NSException *_) {
                }
            }
            if (self.fileModificationTime) {
                @try {
                    [attachFile setModifiedDateTime:self.fileModificationTime];
                } @catch (NSException *_) {
                }
            }
            @try {
                [attachment setFileSpec:attachFile];
            } @catch (NSException *_) {
            }
        }
    }
    [attachment resetAppearanceStream];
}

// todo wei - (BOOL)isEqualToAttributes:

@end

@implementation FSScreenAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSScreen *screen = [[FSScreen alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:screen]) {
        self.intent = screen.intent;
        self.contents = [screen getContent];
        self.rotation = [screen getRotation];
        self.markupDict = [screen getMKDict];
        self.action = screen.action;
        if (screen.screenActiontype == FSScreenActionTypeAudio || screen.screenActiontype == FSScreenActionTypeVideo)
        self.borderInfo = screen.borderInfo;
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSScreen *screen = [[FSScreen alloc] initWithAnnot:annot];
    [super resetAnnot:screen];
    if (self.intent != nil) {
        [screen setIntent:self.intent];
    }
    [screen setContent:self.contents ?: @""];
    [screen setRotation:self.rotation];
    if (![self.action isEmpty]) {
        [screen setAction:self.action];
    }
    if (self.borderInfo) {
        [screen setBorderInfo:self.borderInfo];
    }
    if (self.markupDict) {
        [screen setMKDict:(FSPDFDictionary *) [self.markupDict cloneObject]]; // use a clone because when annot's markup dict is overwritten the old value is released, use a clone won't affect markupDict property of FSScreenAttributes
    }
    [screen resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSScreenAttributes *)attributes {
    return self.class == attributes.class &&
           stringsEqual(self.intent, attributes.intent) &&
           stringsEqual(self.contents, attributes.contents) &&
           self.rotation == attributes.rotation &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSPolygonAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSPolygon *polygon = [[FSPolygon alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:polygon]) {
        [self cloneAnnotDict:annot];
        self.borderInfo = [polygon getBorderInfo];
        self.fillColor = [polygon getFillColor];
        self.vertexes = polygon.vertexes;
        self.contents = [polygon getContent];
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    [self resetAnnotFromDict:annot];
    FSPolygon *polygon = [[FSPolygon alloc] initWithAnnot:annot];
    [super resetAnnot:polygon];
    if (self.borderInfo) {
        [polygon setBorderInfo:self.borderInfo];
    }
    if ((0xff000000 & self.fillColor) != 0) {
        [polygon setFillColor:self.fillColor];
    }
    [polygon setVertexes:self.vertexes];
    if (self.contents) {
        [polygon setContent:self.contents];
    }
    [polygon resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSPolygonAttributes *)attributes {
    return self.class == attributes.class &&
           borderInfoEqual(self.borderInfo, attributes.borderInfo) &&
           self.fillColor == attributes.fillColor &&
           pointsEqual(self.vertexes, attributes.vertexes) &&
           stringsEqual(self.contents, attributes.contents) &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSPolyLineAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSPolyLine *polyline = [[FSPolyLine alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:polyline]) {
        self.borderInfo = [polyline getBorderInfo];
        self.vertexes = polyline.vertexes;
        self.contents = [polyline getContent];
        self.styleFillColor = [polyline getStyleFillColor];
        self.lineStartingStyle = [polyline getLineStartStyle];
        self.lineEndingStyle = [polyline getLineEndStyle];
    }
    return self;
}

- (void)resetAnnot:(FSAnnot *)annot {
    FSPolyLine *polyline = [[FSPolyLine alloc] initWithAnnot:annot];
    [super resetAnnot:polyline];
    if (self.borderInfo) {
        [polyline setBorderInfo:self.borderInfo];
    }
    [polyline setVertexes:self.vertexes];
    if (self.contents) {
        [polyline setContent:self.contents];
    }
    if ((0xff000000 & self.styleFillColor) != 0) {
        [polyline setStyleFillColor:self.styleFillColor];
    }
    if (self.lineStartingStyle) {
        [polyline setLineStartStyle:self.lineStartingStyle];
    }
    if (self.lineEndingStyle) {
        [polyline setLineEndStyle:self.lineEndingStyle];
    }
    [polyline resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSPolyLineAttributes *)attributes {
    return self.class == attributes.class &&
           borderInfoEqual(self.borderInfo, attributes.borderInfo) &&
           pointsEqual(self.vertexes, attributes.vertexes) &&
           stringsEqual(self.contents, attributes.contents) &&
           self.styleFillColor == attributes.styleFillColor &&
           self.lineStartingStyle == attributes.lineStartingStyle &&
           self.lineEndingStyle == attributes.lineEndingStyle &&
           [super isEqualToAttributes:attributes];
}

@end

@implementation FSRedactAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
    redact.color = [UISettingsModel modelWithDict:[NSObject defaultSettings]].annotations.redaction.color.rgbValue;
    if (self = [super initWithAnnot:redact]) {
        self.applyFillColor = redact.applyFillColor;
        self.fillColor = redact.fillColor;
        self.overlayText = redact.overlayText;
        FSRectFArray *rects = [[FSRectFArray alloc] init];
        [rects add:self.rect];
        self.rects = rects;
        
        self.quads = redact.quads;
        self.subject = redact.subject;
        self.contents = redact.contents;
        
        FSDefaultAppearance *ap = [redact getDefaultAppearance];
        FSFont *font = ap.font;
        self.fontName = [font isEmpty] ? nil : [font getName];
        self.defaultAppearanceFlags = ap.flags;
        self.fontSize = ap.text_size;
        self.textColor = ap.text_color;
    }
    return self;
}
- (void)resetAnnot:(FSAnnot *)annot {
    FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
    [super resetAnnot:redact];
//    if (self.applyFillColor) {
        [redact setApplyFillColor:self.applyFillColor];
//    }
//    if (self.fillColor) {
//        [redact setFillColor:self.fillColor];
//    }
    if (self.overlayText) {
        [redact setOverlayText:self.overlayText];
    }
    redact.quads = self.quads;
    redact.subject = self.subject;
    redact.contents = self.contents;
    {
        FSDefaultAppearance *ap = [redact getDefaultAppearance];
        ap.flags = self.defaultAppearanceFlags;
        if (self.fontName) {
            int fontID = [Utility toStandardFontID:self.fontName];
            FSFont *font = nil;
            if (fontID == -1) {
                font = [[FSFont alloc] initWithName:self.fontName styles:0 charset:FSFontCharsetDefault weight:0];
            } else {
                font = [[FSFont alloc] initWithFont_id:fontID];
            }
            ap.font = font;
        }
        ap.text_size = self.fontSize;
        ap.text_color = self.textColor;
        [redact setDefaultAppearance:ap];
    }
    [redact resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSRedactAttributes *)attributes {
    return self.class == attributes.class &&
    self.applyFillColor == attributes.applyFillColor &&
    [self.overlayText isEqualToString:attributes.overlayText] &&
    self.textColor == attributes.textColor &&
    self.fontSize == attributes.fontSize &&
    self.defaultAppearanceFlags == attributes.defaultAppearanceFlags &&
    stringsEqual(self.fontName, attributes.fontName) &&
    isQuadsEqual(self.quads,attributes.quads) &&
    [self.contents isEqualToString:attributes.contents] &&
    [super isEqualToAttributes:attributes];
}
        
@end

@implementation FSWidgetAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot {
    FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
    if (self = [super initWithAnnot:widget]) {
        self.rotation = [widget getMKRotation];
        FSField *field = [widget getField];
        FSControl *control = [widget getControl];
        self.fieldName = [field getName];
        self.fieldType = [field getType];
        self.fieldValue = field.value;
        if (self.fieldType == FSFieldTypeRadioButton) {
            [self cloneControlDict:control];
            NSMutableArray *controlsDictArr = [NSMutableArray array];
            int controlCount = [field getControlCount];
            self.controlCount = controlCount;
            @try {
                for (int i = 0; i < controlCount; i++) {
                    FSControl *indexControl = [field getControl:i];
                    if (indexControl && ![indexControl isEmpty]) {
                        [controlsDictArr addObject:cloneAnnotDictToDict(indexControl.getWidget)];
                         if (indexControl.isChecked) {
                             self.isCheckedIndex = i;
                         }
                    }
                }
            } @catch (NSException *exception) {
            }
            self.controlsDict = controlsDictArr.copy;
            self.isRadioButton = YES;
        }
        FSDefaultAppearance *ap = [control getDefaultAppearance];
        FSFont *font = ap.font;
        self.fontName = [font isEmpty] ? nil : [font getName];
        self.fontSize = ap.text_size;
        self.textColor = ap.text_color;
        self.field_flags =  field.flags;
        FSChoiceOptionArray *options = [[FSChoiceOptionArray alloc] init];
        for (int i = 0; i < field.options.getSize; i++) {
            FSChoiceOption *choiceOption = [field.options getAt:i];
            FSChoiceOption *newChoiceOption = [[FSChoiceOption alloc] initWithOption_value:choiceOption.option_value option_label:choiceOption.option_label selected:choiceOption.selected default_selected:choiceOption.default_selected];
            [options add:newChoiceOption];
        }
        self.choiceOptionArray = options;
    }
    return self;
}

- (void)cloneControlDict:(FSControl *)control{
    self.controlDict = clonePdfDictToDic(control.getWidgetDict);
}

- (void)resetControlFromDict:(FSControl *)control{
    resetDictToPdfDict(self.controlDict, control.getWidgetDict);
    [self cloneControlDict:control];
}

- (FSWidget *)redoWidgetWithPage:(FSPDFPage *)page{
    if (self.fieldType == FSFieldTypeSignature) {
        FSSignature *signature = [page addSignatureWithFieldName:self.rect field_name:self.fieldName];
        FSControl *control = [signature getControl:0];
        if (![control isEmpty]) {
            return [control getWidget];
        }
    }
    FSForm *form = [[FSForm alloc] initWithDocument:page.getDocument];
    FSControl *control = [form addControl:page field_name:self.fieldName field_type:self.fieldType rect:self.rect];
    return [control getWidget];
}
        
- (void)resetAnnot:(FSAnnot *)annot{
    [super resetAnnot:annot];
    FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
    FSControl *control = [widget getControl];
    FSField *field = [control getField];
    if (self.isRadioButton) {
        if (self.controlsDict.count == [field getControlCount]) {
            NSMutableArray *controlsDictArr = [NSMutableArray array];
            for (int i = 0; i < self.controlsDict.count; i++) {
                FSControl *indexControl = [field getControl:i];
                if (indexControl && ![indexControl isEmpty]) {
                    NSDictionary *resetIndexDict = self.controlsDict[i];
                    resetDictToPdfDict(resetIndexDict, indexControl.getWidget.getDict);
                    [controlsDictArr addObject:clonePdfDictToDic(indexControl.getWidget.getDict)];
                    [[indexControl getWidget] resetAppearanceStream];
                }
            }
            self.controlsDict = controlsDictArr.copy;
        }else{
            [self resetControlFromDict:control];
        }
        return;
    }
    FSForm *form = [[FSForm alloc] initWithDocument:annot.getPage.getDocument];
    if ( self.field_flags != field.flags  && self.field_flags != 0) {
        field.flags =  self.field_flags;
    }
    if (self.choiceOptionArray) {
        field.options = self.choiceOptionArray;
    }

    FSDefaultAppearance *ap = [control getDefaultAppearance];
    if (self.fontName) {
        int fontID = [Utility toStandardFontID:self.fontName];
        FSFont *font = nil;
        if (fontID == -1) {
            font = [[FSFont alloc] initWithName:self.fontName styles:0 charset:FSFontCharsetDefault weight:0];
        } else {
            font = [[FSFont alloc] initWithFont_id:fontID];
        }
        ap.font = font;
    }
    ap.text_size = self.fontSize;
    ap.text_color = self.textColor;
    ap.flags |= FSDefaultAppearanceFlagFont | FSDefaultAppearanceFlagTextColor | FSDefaultAppearanceFlagFontSize;
    [control setDefaultAppearance:ap];

    if (self.fieldName.length && ![[field getName] isEqualToString:self.fieldName]) {
        if (field.getControlCount == 1) {
            [form renameField:field new_field_name:self.fieldName];
        }else{
             [form moveControl:control field_name:self.fieldName];
        }
    }
    if (self.fieldValue) {
        field = [widget getField];
        [field setValue:self.fieldValue];
    }
    
    if (self.rotation != FSRotationUnknown) {
       widget.mKRotation = self.rotation;
    }
    [widget resetAppearanceStream];
}

- (BOOL)isEqualToAttributes:(FSWidgetAttributes *)attributes {
    return self.class == attributes.class &&
    [self.fieldName isEqualToString:attributes.fieldName] &&
    self.textColor == attributes.textColor &&
    self.fontSize == attributes.fontSize &&
    stringsEqual(self.fontName, attributes.fontName) &&
    [self.fieldValue isEqualToString:attributes.fieldValue] &&
     self.field_flags == attributes.field_flags &&
    isChoiceOptionsEqual(self.choiceOptionArray, attributes.choiceOptionArray) &&
    self.isCheckedIndex == attributes.isCheckedIndex &&
    self.controlCount == attributes.controlCount &&
    self.rotation == attributes.rotation &&
    [super isEqualToAttributes:attributes];
}

@end

@implementation FSGroupAnnotAttributes

- (instancetype)initWithAnnot:(FSAnnot *)annot{
    FSMarkup *groupHeader = [annot canGetGroupHeader];
    if (groupHeader) {
        FSMarkupArray *markupArray = [groupHeader getGroupElements];
        int header_index = 0;
        for (int i = 0; i < markupArray.getSize; i++) {
            FSMarkup *markup = [markupArray getAt:i];
            if ([markup.NM isEqualToString:groupHeader.NM]) {
                header_index = i;
                break;
            }
        }
        return [self initWithMarkupArray:markupArray header_index:header_index];
    }
    return nil;
}

- (instancetype)initWithMarkupArray:(FSMarkupArray *)markupArray header_index:(int)header_index{
    self = [super init];
    if (self) {
        if (header_index == -1) {
            header_index = 0;
        }
        FSMarkup *groupHeader = [markupArray getAt:header_index];
        _header_index = header_index;
        _pageIndex = groupHeader.pageIndex;
        NSMutableArray *groupAnnotAttributes = @[].mutableCopy;
        for (int i = 0; i < markupArray.getSize; i ++) {
            FSMarkup *markup = [markupArray getAt:i];
            FSAnnotAttributes *attributes = [[FSAnnotAttributes alloc] initWithAnnot:markup needGroupElements:NO];
            [groupAnnotAttributes addObject:attributes];
        }
        _groupAnnotAttributes = groupAnnotAttributes.copy;
        _groupHeaderAnnotAttributes = groupAnnotAttributes[header_index];
    }
    return self;
}

@end

@implementation FSGroupsAnnotAttributes
- (instancetype)initWithGroupsAnnotAttributes:(NSArray<FSGroupAnnotAttributes *> *)groupsAnnotAttributes pageIndex:(int)pageIndex{
    self = [super init];
    if (self) {
        self.header_index = 0;
        self.pageIndex = pageIndex;
        self.groupsAnnotAttributes = groupsAnnotAttributes.copy;
        
        NSMutableArray *groupAnnotAttributes = @[].mutableCopy;
        [groupsAnnotAttributes enumerateObjectsUsingBlock:^(FSGroupAnnotAttributes * _Nonnull groupAttributes, NSUInteger idx, BOOL * _Nonnull stop) {
            [groupAnnotAttributes addObjectsFromArray:groupAttributes.groupAnnotAttributes];
        }];
        self.groupAnnotAttributes = groupAnnotAttributes.copy;
        self.groupHeaderAnnotAttributes = groupAnnotAttributes[self.header_index];
    }
    return self;
}
@end
