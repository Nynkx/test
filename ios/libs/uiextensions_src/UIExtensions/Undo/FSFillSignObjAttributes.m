/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSFillSignObjAttributes.h"

@implementation FSFillSignObjAttributes

+ (instancetype)attributesWithFillSignObject:(FSFillSignObject *)fillSignObject{
    Class attributesClass = nil;
    switch ([fillSignObject getType]) {
    case FSFillSignFillSignObjectTypeText:
    case FSFillSignFillSignObjectTypeCrossMark:
    case FSFillSignFillSignObjectTypeCheckMark:
    case FSFillSignFillSignObjectTypeRoundRectangle:
    case FSFillSignFillSignObjectTypeLine:
    case FSFillSignFillSignObjectTypeDot:
        attributesClass = [FSTextFillSignObjAttributes class];
        break;
    default:
        attributesClass = [FSFillSignObjAttributes class];
    }
    return [[attributesClass alloc] initWithFillSignObject:fillSignObject];
}
- (instancetype)initWithFillSignObject:(FSFillSignObject *)fillSignObject{
    if (self = [super init]) {
        self.type = fillSignObject.getType;
        self.NM = [Utility getUUID];
        self.rect = [fillSignObject getRect];
    }
    return self;
}
- (void)resetFillSignObject:(FSFillSignObject *)fillSignObject{
//    FSPointF *pointf = [[FSPointF alloc] initWithX:self.rect.left Y:self.rect.bottom];
//    [fillSignObject move:pointf width:ABS(self.rect.left - self.rect.right) height:ABS(self.rect.top - self.rect.bottom)];
}
- (BOOL)isEqualToAttributes:(FSFillSignObjAttributes *)attributes{
    return self.type == attributes.type && [Utility rectEqualToRect:self.rect rect:attributes.rect];
}

@end

@implementation FSTextFillSignObjAttributes

- (instancetype)initWithFillSignObject:(FSFillSignObject *)fillSignObject{
    if (self = [super initWithFillSignObject:fillSignObject]) {
        FSTextFillSignObject *textFillSign = [[FSTextFillSignObject alloc] initWithFillsign_object:fillSignObject];
        self.textDataArray = textFillSign.getTextDataArray;
    }
    return self;
}
- (void)resetFillSignObject:(FSFillSignObject *)fillSignObject{
    [super resetFillSignObject:fillSignObject];
    if ([self.textDataArray getSize]) {
//        FSTextFillSignObject *textFillSign = [[FSTextFillSignObject alloc] initWithFillsign_object:fillSignObject];
//        textFillSign.getTextDataArray = self.textDataArray;
    }
}
- (BOOL)isEqualToAttributes:(FSTextFillSignObjAttributes *)attributes{
    return [super isEqualToAttributes:attributes] && self.textDataArray == attributes.textDataArray;
}

@end
