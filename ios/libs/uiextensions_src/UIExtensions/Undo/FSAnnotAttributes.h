/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#ifndef FSANNOT_ATTRIBUTES_H
#define FSANNOT_ATTRIBUTES_H

@interface FSGroupAnnotAttributes : NSObject
@property (nonatomic, assign) int pageIndex;
@property (nonatomic, assign) int header_index;
@property (nonatomic, copy) NSArray<FSAnnotAttributes *> *groupAnnotAttributes;
@property (nonatomic, strong) FSAnnotAttributes *groupHeaderAnnotAttributes;

- (instancetype)initWithAnnot:(FSAnnot *)annot;
- (instancetype)initWithMarkupArray:(FSMarkupArray *)markupArray header_index:(int)header_index;
@end

@interface FSGroupsAnnotAttributes : FSGroupAnnotAttributes
@property (nonatomic, copy) NSArray<FSGroupAnnotAttributes *> *groupsAnnotAttributes;

- (instancetype)initWithGroupsAnnotAttributes:(NSArray<FSGroupAnnotAttributes *> *)groupsAnnotAttributes pageIndex:(int)pageIndex;
@end

@interface FSAnnotAttributes : NSObject
//MARK: Page dict has some problems in use and is not recommended
@property (nonatomic, copy) NSDictionary *pageDict;
@property (nonatomic, copy) NSDictionary *dict;
@property (nonatomic, assign) int pageIndex;
@property (nonatomic, assign) FSAnnotType type;
@property (nonatomic, strong) NSString *NM;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) FSRectF *rect;
@property (nonatomic, assign) unsigned int color;
@property (nonatomic, assign) float opacity;
@property (nonatomic, strong) NSDate *modificationDate;
@property (nonatomic, strong) NSDate *creationDate;
@property (nonatomic, assign) unsigned int flags;
@property (nonatomic, assign) BOOL isGroup;
@property (nonatomic, copy) NSArray<FSAnnotAttributes *> *replyAnnotAttributes;
@property (nonatomic, strong) FSGroupAnnotAttributes *groupAnnotAttributes;
+ (instancetype)attributesWithAnnot:(FSAnnot *)annot;
- (instancetype)initWithAnnot:(FSAnnot *)annot;
- (instancetype)initWithAnnot:(FSAnnot *)annot useDict:(BOOL)useDict;
- (void)resetAnnotFromDict:(FSAnnot *)annot;
- (void)resetAnnot:(FSAnnot *)annot;
- (BOOL)isEqualToAttributes:(FSAnnotAttributes *)attributes;
@end

@interface FSMarkupAttributes : FSAnnotAttributes
@property (nonatomic, strong) FSAnnotAttributes *popupAttributes;
@end

@interface FSNoteAttributes : FSAnnotAttributes
@property (nonatomic, assign) int icon;
@property (nonatomic, strong) NSString *replyTo;
@property (nonatomic, strong) NSString *contents;
@end

@interface FSCaretAttributes : FSMarkupAttributes
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *intent;
@property (nonatomic, assign) int rotation;
@property (nonatomic, strong) FSRectF *innerRect;
@end

@interface FSTextMarkupAttributes : FSAnnotAttributes
@property (nonatomic, strong) NSArray<FSQuadPoints *> *quads;
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, strong) NSString *subject;
@end

@interface FSLineAttributes : FSMarkupAttributes
@property (nonatomic, assign) float lineWidth;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *intent;
@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic) FSMarkupEndingStyle startPointStyle;
@property (nonatomic) FSMarkupEndingStyle endPointStyle;
@property (nonatomic, strong) FSPointF *captionOffset;
@property (nonatomic) FSLineCapPos captionStyle;
@property (nonatomic, assign) unsigned int fillColor;
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, strong) NSString *measureRatio;
@property (nonatomic, strong) NSString *measureUnit;
@property (nonatomic, assign) float measureConversionFactor;
@property (nonatomic, assign) float leaderLineLength;
@property (nonatomic, assign) float leaderLineExtensionLength;
@property (nonatomic, assign) float leaderLineOffset;
@end

@interface FSInkAttributes : FSAnnotAttributes
@property (nonatomic, assign) float lineWidth;
@property (nonatomic, strong) FSPath *inkList;
@property (nonatomic, strong) NSString *contents;
@end

@interface FSStampAttributes : FSAnnotAttributes
@property (nonatomic, strong) NSString *iconName;
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, assign) int rotation;
@end

@interface FSShapeAttributes : FSAnnotAttributes
@property (nonatomic, assign) float lineWidth;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *contents;
@end

@interface FSFreeTextAttributes : FSAnnotAttributes
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, strong) NSString *intent;
@property (nonatomic, strong) NSString *subject;
//callout
@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *kneePoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic, strong) FSRectF *innerRect;
@property (nonatomic) FSMarkupEndingStyle lineEndingStyle;
// default appearance
@property (nonatomic, strong) NSString *fontName;
@property (nonatomic, assign) int defaultAppearanceFlags;
@property (nonatomic, assign) float fontSize;
@property (nonatomic, assign) unsigned int textColor;
@property (nonatomic, assign) FSRotation rotation;
@end

@interface FSFileAttachmentAttributes : FSAnnotAttributes
@property (nonatomic, strong) NSString *iconName;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSString *attachmentPath;
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, strong) FSDateTime *fileCreationTime;
@property (nonatomic, strong) FSDateTime *fileModificationTime;
@end

@interface FSScreenAttributes : FSAnnotAttributes
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, copy) NSString *intent;
@property (nonatomic) FSRotation rotation;
@property (nonatomic, strong) FSPDFDictionary *markupDict;
@property (nonatomic, strong) FSAction *action;
@property (nonatomic, strong) FSBorderInfo *borderInfo;
@end

@interface FSPolygonAttributes : FSAnnotAttributes
@property (nonatomic, strong) FSBorderInfo *borderInfo;
@property (nonatomic) unsigned int fillColor;
@property (nonatomic, strong) FSPointFArray *vertexes;
@property (nonatomic, strong) NSString *contents;
@end

@interface FSPolyLineAttributes : FSAnnotAttributes
@property (nonatomic, strong) FSBorderInfo *borderInfo;
@property (nonatomic, strong) FSPointFArray *vertexes;
@property (nonatomic, strong) NSString *contents;
@property (nonatomic) unsigned int styleFillColor;
@property (nonatomic) FSMarkupEndingStyle lineStartingStyle;
@property (nonatomic) FSMarkupEndingStyle lineEndingStyle;
@end

@interface FSRedactAttributes : FSAnnotAttributes
@property (nonatomic, copy) NSString *fontName;
@property (nonatomic, assign) int defaultAppearanceFlags;
@property (nonatomic, assign) float fontSize;
@property (nonatomic, assign) unsigned int textColor;
@property (nonatomic, strong) FSRectFArray *rects;
@property (nonatomic) unsigned int fillColor;
@property (nonatomic) unsigned int applyFillColor;
@property (nonatomic, copy) NSArray<FSQuadPoints *> *quads;
@property (nonatomic, copy) NSString *overlayText;
@property (nonatomic, copy) NSString *contents;
@property (nonatomic, copy) NSString *subject;
@end

@interface FSWidgetAttributes : FSAnnotAttributes
@property (nonatomic, copy) NSDictionary *controlDict;
@property (nonatomic, copy) NSArray *controlsDict;
@property (nonatomic, copy) NSString *fieldName;
@property (nonatomic, assign) FSFieldType fieldType;
@property (nonatomic, assign) int isCheckedIndex;
@property (nonatomic, assign) int controlCount;
@property (nonatomic, assign) BOOL isRadioButton;
@property (nonatomic, copy) NSString *fieldValue;
@property (nonatomic, strong) FSChoiceOptionArray *choiceOptionArray;
@property (nonatomic, assign) unsigned int field_flags;

@property (nonatomic, strong) NSString *fontName;
@property (nonatomic, assign) float fontSize;
@property (nonatomic, assign) unsigned int textColor;
@property (nonatomic) FSRotation rotation;

- (FSWidget *)redoWidgetWithPage:(FSPDFPage *)page;
@end

#endif
