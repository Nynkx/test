/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FSFillSignObjAttributes : NSObject
@property (nonatomic, strong) FSRectF *rect;
@property (nonatomic, assign) int pageIndex;
@property (nonatomic, assign) FSFillSignFillSignObjectType type;
@property (nonatomic, copy) NSString *NM;
+ (instancetype)attributesWithFillSignObject:(FSFillSignObject *)fillSignObject;
- (instancetype)initWithFillSignObject:(FSFillSignObject *)fillSignObject;
- (void)resetFillSignObject:(FSFillSignObject *)fillSignObject;
- (BOOL)isEqualToAttributes:(FSFillSignObjAttributes *)attributes;

@end
@interface FSTextFillSignObjAttributes : FSFillSignObjAttributes
@property (nonatomic, strong) FSTextFillSignObjectDataArray *textDataArray;

@end


NS_ASSUME_NONNULL_END
