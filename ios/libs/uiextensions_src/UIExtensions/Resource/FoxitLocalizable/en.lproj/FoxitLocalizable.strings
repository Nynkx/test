
//Common
"kSuccess" = "Success";
"kFailed" = "Failed";
"kWarning" = "Warning";
"kError" = "Error";
"kConfirm" = "Confirm";
"kOK" = "OK";
"kCancel" = "Cancel";
"kYes" = "Yes";
"kNo" = "No";
"kAdd" = "Add";
"kRename" = "Rename";
"kPlay" = "Play";
"kDelete" = "Delete";
"kRotate" = "Rotate";
"kExtract" = "Extract";
"kCopy" = "Copy";
"kDone" = "Done";
"kSave" = "Save";
"kDonotSave" = "Don't save";
"kLoading" = "Loading...";
"kShare" = "Share";
"kReplace" = "Replace";
"kSkip" = "Skip";
"kZip" = "Zip";
"kEdit" = "Edit";
"kBack" = "Back";
"kSelectAll" = "Select All";
"kDeselectAll" = "Deselect All";
"kCreate" = "Create";
"kFlatten" = "Flatten";
"kAuto" = "Auto";
"kMultiPicking" = "Multi Picking";
"kSaveAS" = "Save As";
"kFileErrorNotLoaded" = "Format error: not a PDF or corrupted";

//Error
"kWrongPageNumber" = "Wrong page number. The number should be";
"kDocPasswordError" = "Invalid password.\nPlease try again.";
"kNoFileInDisk" = "Cannot find the file %@ in disk. Remove it from file list.";
"kInvalidSecurityHandler" = "Invalid security handler.";
"kErrorCode" = "Error code (%d).";
"kUnfoundOrCannotOpen" = "Cannot find or open the file.";
"kInvalidFormat" = "Invalid format.";
"kHandlerError" = "Invalid handler.";
"kWrongCertificate" = "Invalid certificate.";
"kCertExpired" = "Certificate is expired.";
"kUnknownError" = "Unknown error.";
"kInvalidLibraryLicense" = "Invalid library license.";
"kInvalidParameter" = "Invalid parameter.";
"kUnsupportedType" = "Unsupported type.";
"kOutOfMemory" = "Out of memory.";
"kNoAppropriateApplication" = "No appropriate application exists.";
"kInvalidUrl" = "Invalid url.";
"kUnsupportedDocFormat" = "The XFA document is unsupported to operate.";
"kPasswordNotSame" = "The Document Open and Owner passwords cannot be the same,please enter a different password in either the Document Open Password field or the Owner Password field.";
"kEditOutsideDocument" = "You can't edit outside the document.";
"kXFAUnsupportedOP" = "The XFA document do not support this operation";
"kShouldSaveFileTips" = "PDF document has been changed. Do you want to save?";
"kShouldSaveEncryptionDocument" = "Do you want to save document before encryption?";
"kShouldSaveEecryptionDocument" = "Do you want to save document before decryption?";
"kTrimWhitespace" = "Head and tail whitespace trimmed!";
"kFailMovePage" = "Failed to move pages.";
"kFailRotatePage" = "Failed to rotate page.";
"kFailExtract" = "Failed to extract!";
"kFailCopy" = "Failed to copy!";
"kFailInsert" = "Failed to insert!";
"kFailDelete" = "Failed to delete!";
"kInvalidPermissionTip" = "You do not have permission to view these pages.";
"kInvalidLogin" = "Please Sign in first";



//Confirm - PDFView Thumbnial mode
"kSureToDeletePage" = "Are you sure you want to delete the selected page(s)? You cannot undo this action.";
"kNotAllowDeleteAll" = "Deleting all page(s) is not allowed";
"kDeletePageFailed" = "Cannot delete the page(s).";
"kMovePageFailed" = "Cannot move the page(s).";


//File management
"kDocuments" = "Documents";
"kSearchPlaceholder" = "Search PDF";
"kByFileName" = "By name";
"kItems" = "%lld items";
"kSelectFile" = "Select";
"kIllegalNameWarning" = "You have entered an illegal name";

"kFileInformation" = "File Information";
"kReduceFileSize" = "Reduce File Size";
"kReduceFileSizeDescription" = "You choose to reduce the PDF file size, but it will not be applied to the document until you save the document. You may continue to edit the document.";
"kAirPrint" = "Wireless Print";
"kAirPrintNotAvailable" = "Wireless print is not available, please check your system settings.";
"kScreenCapture" = "Screen Capture";
"kScreenCaptureVersion" = "Screen Capture supports iOS 6.0 and above, please upgrade your system.";

"kName" = "Name";
"kAdded" = "Added";
"kModify" = "Updated";
"kDeleted" = "Deleted";
"kReply" = "Reply";
"kOKHelp" = "Select \"Save\" to save the document here.";
"kInputNewFileName" = "Please input a new file name";
"kFileAlreadyExists" = "The file already exists, do you want to replace it?";
"kCloudUploadFile" = "Upload";
"kCopyToFolder" = "Copy To";
"kMoveToFolder" = "Move To Folder";
"kSelectToFolder" = "Select Folder";
"kMove" = "Move";
"kCopyHelp" = "Select \"Copy\" to copy the document here.";
"kMoveHelp" = "Select \"Move\" to move the document here.";
"kAddServer" = "Add";
"kMultiSelectLimitFaild" = "Select at least %d file(s)";

//EGOTableViewPullRefresh
"kLastUpdated" = "Last Updated";
"kReleaseToRefresh" = "Release to refresh...";
"kPullDownToRefresh" = "Pull down to refresh...";

//Document
"kDocNeedPassword" = "This file is password protected.\nPlease input the password.";
"kViewModeSingle" = "Single";
"kViewModeContinuous" = "Continuous Scrolling";
"kViewModeThumbnail" = "Thumbnail";
"kReflow" = "Reflow";
"kViewModeCover" = "Coverpage";
"kViewModeNoCover" = "Facing";
"kFitPage" = "Fit Page";
"kFitWidth" = "Fit Width";
"kRotateView" = "Rotate View";
"kPageMode" = "Page Mode";
"kPageView" = "Page View";
"kNightMode" = "Night Mode";

//Crop
"kCropMode" = "Crop Mode";
"kCropSmartCalculating" = "Calculating page crop, please wait...";
"kCropNo" = "No Crop";
"kCropSmart" = "Smart Crop";
"kCropDone" = "Crop";
"kCropDetect" = "Detect";
"kCropFull" = "Full";
"kApply2All" = "Use on All";
"kApply2Odd" = "Use on Odd";
"kApply2Even" = "Use on Even";

//Form
"kForm" = "Form";
"kCreateFormFields" = "Create Form Fields";
"kResetFormFields" = "Reset Form Fields";
"kImportForm" = "Import Form Data";
"kExportForm" = "Export Form Data";
"kSureToResetFormFields" = "Are you sure you want to reset all form fields?";
"kNoFormAvailable" = "This document has no form data";
"kImportFormSuccess" = "Successfully imported data to the current document.";
"kImportFormFailed" = "Couldn't import or access data. Please check the form data file status and retry.";
"kExportFormSuccess" = "Form data was successfully exported.";
"kExportFormFailed" = "Form exporting failed.";
"kReset" = "Reset";
"kClickHereFormTip" = "Click here to add item";
"kItemList" = "Item List";
"kOptions" = "Options";
"kCustomText" = "Custom text";
"kNewItem" = "New Item";
"kMultipleSelection" = "Multiple Selection";
"kFieldRenamFailed" = "The name you have inputted has already been used in an existing field. Please choose a different name, or change the field type to match the existing field.";

//Annotation
"kAnnotation" = "Annotations";
"kNote" = "Comment";
"kReply" = "Reply";
"kCopyText" = "Copy Text";
"kIcon" = "Icon";
"kFont" = "Font";
"kOpacity" = "Opacity";
"kHighlight" = "Highlight";
"kSquiggly" = "Squiggly";
"kStrikeout" = "Strikeout";
"kUnderline" = "Underline";
"kRedaction" = "Redaction";
"kApply" = "Apply";
"kApplyAllRedaction" = "Apply all redaction";
"kLine" = "Line";
"kArrow" = "Arrow";
"kPencil" = "Pencil";
"kRectangle" = "Rectangle";
"kCircle" = "Oval";
"kEraser" = "Eraser";
"kInsertText" = "Insert Text";
"kReplaceText" = "Replace Text";
"kAttachment" = "Attachment";
"kAttachments" = "Attachments";
"kRotation" = "Rotation";
"kImage" = "Image";
"kPolygon" = "Polygon";
"kCloud" = "Cloud";
"kPolyLine" = "PolyLine";
"kCustomColor" = "Custom Color";
"kMultipleSelect" = "Multi Picking";
"kGroup" = "Group";
"kUngroup" = "Ungroup";

"kGraph" = "Graph";
"kPushpin" = "Pushpin";
"kPaperclip" = "Paperclip";
"kTag" = "Tag";

"kTypewriter" = "Typewriter";
"kTextbox" = "Textbox";
"kCallout" = "Callout";
"kHintTitleTypewriter" = "Tap screen to add Typewriter";

"kDistance" = "Distance";
"kUnit" = "Unit";
"kDistanceLengthError" = "The value can't be empty or more than 6 digits";
"kDistanceNumberError" = "The value must be number";

"kRedactTip" = "You are about to permanently redact the marked content, this operation can\'t be undone. Are you sure you want to continue?";
"kFailedLoadModuleForLicenseTip" = "You are not authorized to use this add-on module， please contact us for upgrading your license.";

//Bookmark
"kBookmark" = "Reading Bookmarks";
"kRenameBookmark" = "Rename Bookmark";
"kClearBookmark" = "Clear all bookmarks?";
"kInputBookmarkName" = "Please input a new bookmark name";

//Outline
"kOutline" = "Outline";

//Search
"kSearch" = "Search";
"kTotalFound" = "Total found";
"kSearchGoogle" = "Search in Google";
"kSearchWiki" = "Search in Wikipedia";
"kPage" = "Page";
"kSearching" = "Searching...";
"kWholeWordsOnly" = "Whole Words Only";
"kCaseSensitive" = "Case-Sensitive";
"kSearchInInternet" = "Search in Internet";

//Signature
"kSignatureTitle" = "Signature";
"kSignAction" = "Sign";
"kEditSavedSign" = "Edit Saved Signature";
"kConfirmSign" = "After applying a signature, the signature and all other changes to the file will be saved.";
"kCDRMCanNotSign" = "The current document is protected, you can not create signatures";

"kSignListTitle" = "Signature List";
"kSignListIconTitle" = "List";
"kHadProtect" = "This document had been protected by Digital Signature or RMS.";
"kCertInfo" = "Certificate information";
"kCertSerialNum" = "Serial Number:";
"kCertIssuer" = "Certificate Issuer:";
"kCertStartTime" = "Valid start date:";
"kCertEndTime" = "Valid end date:";
"kCertEmail" = "Email Address:";
"kSignDate" = "Signed Date:";
"kVerifyVaildResult" = "Digital Signature is valid.";
"kVerifyVaildModifyResult" = "The revision of the document that was covered by this signature has not been altered. However, there have been subsequent changes to the document.";
"kVerifyInvaildResult" = "Digital Signature is invalid.";
"kVerifyUnknownResult" = "Digital Signature is UNKNOWN.";
"kVerifyResultErrorByteRange" = "Digital Signature is invalid as error byte range.";
"kVerifyResultIssueExpire" = "Digital Signature is UNKNOWN.\nCertificate for verifying issuer is expired.";
"kVerifyResultErrorData" = "Unknown error is happened to digital signature verification.";
"kVerifyResultInvaildAndOther" = "Digital Signature is invalid as other reasons.";
"kVerifyTitle" = "Verification Results";
"kCurrentSelectCert" = "Current Certificate:";
"kVerifySign" = "VerifyDigitalSign";
"kSignListSectionHadCert" = " Signature with Certificate";
"kSignListSectionWithoutCert" = "Signature without Certificate";
"kSelectAddCert" = "+ Select certificate to add ";
"kCertFileError" = "The certificate file error";
"kRequestPermissionTip" = "Permissions Requesting:";
"kPermissionCopy" = "Copying of text, images, and other content";
"kPermissionEdit" = "Commenting, filling in form fields and signing existing signature fields";
"kNoCert" = "No Certificate";

"kFieldSignWithoutCert" = "Please select a certificate to sign this document.";
"kAddSign" = "Sign";
"kSignList" = "SignatureList";
"kDeleteSign" = "Delete";
"kOfflineCopyAlterTitle" = "Please input the password";
"kPassWordErrorAlterTitle" = "Password is probably not correct. Try again?";
"kSaveSignedDocSuccess" = "Succeed to save a new PDF document with digital signature";
"kSaveSignedDocFailure" = "Failed to sign and save";

"kLTVStateEnable" = "Signature is LTV enabled.";
"kDigitalSignatureTitle" = "Digital Signatures";
"kDigitalSignatureBtnView" = "View";
"kDigitalSignatureBtnVerify" = "Verify";
"kDigitalSignatureTableEmptyData" = "No digital signatures";
"kDigitalSignatureSignedTitleVersion" = "Rev.%d:";
"kDigitalSignatureSignedTitleSigner" = "Signed by %@";
"kDigitalSignatureUnsigned" = "Unsigned signature field";
"kDigitalSignatureAllReadyView" = "You are already viewing this version of document covered by the selected signature";
"kDigitalSignatureFaildView" = "Failed to view this verson";
"kDigitalSignatureListSignerUnknown" = "Unknown";

"kAddTrustedCertTitle" = "Add to Trusted Certificates";
"kAddTrustedCertDesciption" = "Do you want to trust the certificate when validating signatures or certified documents?";
"kTrustedCertName" = "Name:";
"kTrustedCertIssuer" = "Certificate Issuer:";
"kTrustedCertValidFrom" = "Valid from:";
"kTrustedCertValidTo" = "Valid to:";
"kTrustedCertKeyUsage" = "Intended key usage:";

"kuDIGITALSIGNATURE" = "Digital Signature";
"kuNONREPUDIATION" = "Non Repudiation";
"kuKEYENCIPHERMENT" = "Key Encipherment";
"kuDATAENCIPHERMENT" = "Data Encipherment";
"kuKEYAGREEMENT" = "Key Agreement";
"kuKEYCERTSIGN" = "Key Cert Sign";
"kuCRLSIGN" = "Crl Sign";
"kuENCIPHERONLY" = "Not Specified";
"kuDECIPHERONLY" = "Not Specified";

//Selection
"kColor" = "Color";
"kSize" = "Size";
"kThickness" = "Thickness";
"kFontName" = "Font Name";
"kFontSize" = "Font Size";
"kIconCheck" = "Check";
"kIconCircle" = "Circle";
"kIconComment" = "Comment";
"kIconCross" = "Cross";
"kIconHelp" = "Help";
"kIconInsert" = "Insert";
"kIconKey" = "Key";
"kIconNewParagraph" = "New Paragraph";
"kIconNote" = "Note";
"kIconParagraph" = "Paragraph";
"kIconRightArrow" = "Right Arrow";
"kIconRightPointer" = "Right Pointer";
"kIconStar" = "Star";
"kIconUpArrow" = "Up Arrow";
"kIconUpLeftArrow" = "UpLeft Arrow";

//Record
"KStart" = "Start";

//ACN
"kDiscardChange" = "Discard Changes";

//RMS rights
"kRMSFILLFORM" = "Fill in a form";
"kRMSANNOTATE" = "Comment in the document";
"kRMSASSEMBLE" = "Manage pages and bookmarks";
"kRMSEDIT" = "Modify document";
"kRMSEXTRACTACCESS" = "Content copying for accessibility";
"kRMSEXTRACT" = "Extract the contents of the document";

"kRMSNoAccess" = "This document is protected, please check your permission.";
"kCDRMNoEncrypt" = "This document is protected by drm , please don't encrypt it.";
"kRMSAlreadyEncrypt" = "This document is already encrypted by Microsoft AD RMS.";

//Permission
"kPermission" = "Permission";
"kPermissionPrint" = "Printing";
"kEnable" = "Allowed";
"kDisable" = "Not Allowed";

"kPwdDocumentLimitation" = "Add Document Restrictions";
"kPwdFillForm" = "Fill Form";
"kPwdEditDocument" = "Edit Document";
"kPwdExtractContent" =  "Extract Content";

"kDecryption" = "Remove Password";
"kEncryptionTitle" = "Encryption";
"kEncryptMissOpenDocPass" = "Please input password for \"Open Document\" or turn the option off.";
"kEncryptMissOtherPass" = "Please input password for other permission, or turn the option off.";
"kEncryptRemovePass" = "Are you sure you want to remove password for this document?";
"kEncryptProcess" = "Encrypting...";
"kEncryptRemove" = "Removing...";
"kTryAgain" = "Try Again";
"kEncryptAddFail" = "Fail to encrypt this document. Do you want to try again?";
"kEncryptAddSuccess" = "Document encrypted successfully!";
"kEncryptRemoveSuccess" = "This password has been removed successfully!";
"kEncryptionSectionTitle" = "Security Options";
"kEncryptionSectionFooter" = "- When \"Open Document\" is turned on, password is required to open the document.\n- When other permission is turned on, password is required to do the action.";
"kEncryptionDoneNoPass" = "This document is not encrypted by any password. Do you want to encrypt it?";

//EncryptOption xib
"kCellEncryptOpenDocumentTitle" = "Open Document";
"kCellEncryptPrintDocumentTitle" = "Print Document";
"kCellEncryptCopyContentTitle" = "Content copying for accessibility";
"kCellEncryptPasswordTitle" = "Password:";

//others
"kInvalidJSON" = "Invalid JSON";
"kRecordVideo" = "Recording Video";
"kRecordAudio" = "Recording Audio";
"kFromDocument" = "From Document";
"kFromAlbum" = "From Album";
"kFromCamera" = "From Camera";
"kGo" = "Go";
"kDocinfo" = "File information";
"kFilePath" = "Path";
"kFileAuthor" = "Author";
"kFileSubject" = "Subject";
"kCreateDate" = "Created";
"kModifyDate" = "Last Modified";
"kSecurity" = "Security";
"kEncryption" = "File Encryption";
"kPasswordEncryption" = "Password Protection";
"kNoEncryption" = "No Protection";
"kMoreTextMarkup" = "Text Markup";
"kMoreDrawing" = "Drawing";
"kMoreOthers" = "Others";
"kReadList" = "  List  ";
"kReadView" = "  View  ";
"kReadComment" = "Comment";
"kPropertyFill" = "Fill";
"kOverlayText" = "Overlay Text";
"kText" = "Text";
"kInputPlaceholderText" = "Please enter text";
"kStamp" = "Stamp";
"kPropertyImage" = "Image";
"kAudio" = "Audio";
"kVideo" = "Video";
"kMore" = "More";
"kStyle" = "Appearance";
"kAnnotContinue" = "Keep tool selected";
"kAnnotSingle" = "Single Comment";
"kScreenLock" = "Screen Lock";
"kPanAndZoom" = "Pan&Zoom";
"kAutoBrightness" = "Auto-Brightness";
"kClear" = "Clear";
"kClearAnnotations" = "Clear all annotations?";
"kTimeSort" = "Time";
"kOpen" = "Open";
"kOtherDocumentsFile" =  "File";
"kRequiredPlaceHolder" = "Required";
"kOptionalPlaceHolder" = "Optional";
"kDocumentAttachmentTab" = "Attachments Tab";
"kRenameAndAttached" = "Rename and Attached";
"kAttachmentMaxSize" = "This comment is too large to be attached. The maximum size is %dMB.";
"kFailedAddAttachment" = "Unable to add file attachment.";
"kFailedAddAttachmentForExistedName" = "Attachment name already exist.";
"kDescription" = "Description";
"kSureToReplaceFile" = "Are you sure you want to replace file '%@'";
"kCopyFileFailed" = "Fail to copy this file:\n%@: %@\n";
"kFailedChangeDocumentAttachmentDescription" = "Failed to change description of document attachment: '%@'";
"kFailedSaveAttachmentBadFileName" = "There was an error saving this attachment file. Bad file name";
"kFailedOpenAttachmentBadFileName" = "There was an error opening this attachment file. Bad file name";
"kRMSEncryption" = "RMS Protection";
"kMediaFileValid" = "This file type is not supported.Please select a vaild file.";
"kFailedToOpenFile" = "Failed to open this file, try opening it with another app?";
"kFailedOpenAttachment" = "Failed to open attachment '%@'.";
"kFailedOpenAlbum" = "This device doesn't support album.";
"kFailedOpenCamera" = "This device doesn't support camera.";
"kFailedLoadJSON" = "Extensions manager could not be loaded.";
"kFlattenTip" = "Flatten the annotation to make it becomes a part of page content and it cannot be modified or removed.";

//Insert page
"kBlankPage" = "Blank Page";
"kPageSize" = "Page Size";
"kPageColor" = "Page Color";
"kOrientation" = "Orientation";
"kPagecount" = "Page Count";
"kPortrait" = "Portrait";
"kLandscape" = "Landscape";
"kLetter" = "Letter";
"kLegal" = "Legal";
"kledger" = "Ledger";
"kAddPage" = "Add Page";
"kCurrent" = "Current";
"kNew" = "New";
"kThemeColors" = "Theme Colors";
"kBlank" = "Blank";
"kLined" = "Lined";
"kGrid" = "Grid";
"kGraph" = "Graph";
"kMusic" = "Music";

//Comment
"kImportComments" = "Import Comments";
"kExportComments" = "Export Comments";
"kSuccessExportData" = "Successfully exported the data.";
"kFailedExportData" = "Failed to export data from current document.";
"kSuccessImportData" = "Successfully imported data to current document.";
"kFailedImportData" = "Failed to import data to current document.";
"kNoCommentsImport" = "No comments have been imported.";
"kOptimizing" = "Optimizing document...";
"kOptimizeFailed" = "Optimizing failed";
"kOptimizeSizeTip" = "File optimized from %@ to %@";
"kMinimum" = "Minimum";
"kLow" = "Low";
"kMedium" = "Medium";
"kHigh" = "High";
"kMaximum" = "Maximum";

//Save as
"kOriginalDocument" = "Original Document";
"kFlattenedCopy" = "Flattened Copy";
"kFlattenedCopyTip" = "Flatten annotations or PDF forms to make them become a part of page content and prevent further editing";
"kSaving" = "Saving…";
"kCouldNotSavedTip" = "The document could not be saved. Please save the document with a different name or in a different folder.";
"kFileName" = "File name";
"kFormat" = "Format";
"kRename" = "Rename";
"kReducedFileSizeCopy" = "Reduced File Size Copy";
"kLossy" = "Lossy";
"kLossless" = "Lossless";
"kGrayscaleImages" = "Color/Grayscale Images";
"kMonochromeImages" = "Monochrome Images";
"kQuality" = "Quality";

//Trust Cert
"kFailedImportCert" = "A unknown error occurred while importing the certificate.";
"kDeleteTrustedCer" = "Are you sure you want to delete this certificate?\n You will no longer be able to verify the identity of the owner if you do.";
"kTrustedCert" = "Trusted Certificates";
"kInfo" = "Info";
"kCertInfo" = "Certificate Information";
"kViewCert" = "View Certificate";
"kUsage" = "Usage";
"kTrust" = "Trust";
"kTrustedCertTip" = "If you change the trusted settings, you will need to revalidate any signatures to see the changes. Trusting certificates directly from a document is not recommended. Are you sure you want to continue?";
"kVerifyingDigital" = "Verifying Digital Signature";

// CPDF SIGH
"kSignIn" = "Sign In";
"kSignOut" = "Sign Out";
"kSignOutTitle" = "Are you sure you want to sign out?";

// Comparison
"kComparisonVCTitle" = "Compare";
"kCompareItemTitle" = "Compare Mode";
"kCompareButtonOverlay" = "Overlay";
"kCompareButtonAbreast" = "Side-by-Side";
"kCompareFileInfoOld" = "Old File";
"kCompareFileInfoNew" = "New File";
"kCompareFileSelectedColor" = "Selected color";

"kComapreContainerLegendTitle" = "Legend";
"kComapreContainerLegendReplace" = "Replaced";
"kComapreContainerLegendDelete" = "Deleted";
"kComapreContainerLegendInsert" = "Inserted";
"kComapreContainerHideLegend" = "Hide Legend";
"kComapreContainerShowLegend" = "Show Legend";
"kCompareNotSupportFileFormat" = "The file format is not supported %@";
"kCompareNotSomeFile" = "Please select two different documents for comparison.";
"kCompareing" = "Document comparing...";

//scan
"kScan" = "Scan";
"kStartTap" = "Click to scan";
"kRecentTips" = "Recent transformation";
"kNoDetect" = "No detection";
"kMoveClear" = "Move closer";
"kGlareDetect" = "Glare detected";
"kDark" = "Too dark";
"kHoldStill" = "Hold Still";
"kAddPhotos" = "Add Photos";
"kScaneLightTip" = "Flash";
"kScaneDetechTip" = "Auto-Detection";
"kScaneFrameSizeTip" = "Page Size";
"kClose" = "Close";
"kNeedFullScreen" = "Please switch to the full screen mode to use the camera.";
"kALBUM" = "Could not access the Photo Album without permission.";
"kNoTorch" = "This device does not have camera flash.";
"kLightOpen" = "Open Flash";
"kLightClose" = "Close Flash";
"kContinueMode" = "Continuous";
"kSigleMode" = "Single";
"kAutoDetectOpen" = "Open Auto-Detect";
"kAutoDetectClose" = "Close Auto-Detect";
"kContrast" = "Contrast";
"kColorModelNone" = "None";
"kColorModelBlackWhite" = "Black White";
"kColorModeGray" = "Gray";
"kWidth" = "Width";
"kHeight" = "Height";
"kOutPageSizeTip" = "The measurement must be between 1 mm and 5587 mm.";
"kOriginal" = "Original";
"kCustom" = "Custom";
"kStorePDF" = "Store as PDF";
"kSharePDF" = "Share as PDF";
"kConvertPhotos" = "Gallery";
"kRenameFileName" = "Rename File";
"kBrightness" = "Brightness";
"kSaveSuccess" = "Saved successfully";
"kImageLimit" = "Choose up to 9 images one time.";
"kNoFilesYet" = "No files yet";
"kAddNewScanImageTip" = "Click the \"+\" at the bottom to add a picture";
"kNotAllowTakePhoto" = "Cannot take photo";
"kCAMERADenied" = "Foxit scanner does not have access to the Camera. You are enable it in Privacy Settings of your device.";
"kGoSetting" = "Go to Settings";
"kFileFolderExist" = "The file or folder name already exists, please input a new name.";

//speech
"kSpeak" = "Speak";
"kSpeakString" = "Read aloud selected content";
"kSpeakStart" = "Read aloud from current position";
"kSpeechRateTitle" = "Playback Speed";

//fill sign
"kProfiletitle" = "Predefined Text";
"kProfileFullName" = "Full Name:";
"kProfileAddFullName" = "Add Full Name";
"kProfileFirstName" = "First Name:";
"kProfileAddFirstName" = "Add First Name";
"kProfileMiddleName" = "Middle Name:";
"kProfileAddMiddleName" = "Add Middle Name";
"kProfileLastName" = "Last Name:";
"kProfileAddLastName" = "Add Last Name";
"kProfileAddress" = "Address";
"kProfileStreet1" = "Street 1:";
"kProfileAddStreet1" = "Add Street 1";
"kProfileStreet2" = "Street 2:";
"kProfileAddStreet2" = "Add Street 2";
"kProfileCity" = "City:";
"kProfileAddCity" = "Add City";
"kProfileState" = "State:";
"kProfileAddState" = "Add State";
"kProfileZip" = "Zip:";
"kProfileAddZip" = "Add Zip";
"kProfileCountry" = "Country:";
"kProfileAddCountry" = "Add Country";
"kProfileContactInfo" = "Contact Information";
"kProfileEmail" = "Email:";
"kProfileAddEmail" = "Add Email";
"kProfileTel" = "Tel:";
"kProfileAddTel" = "Add Tel";
"kProfileDates" = "Dates";
"kProfileDate" = "Date:";
"kProfileBirthDate" = "Birth Date:";
"kProfileAddBirthDate" = "Add Birth Date";
"kProfileCustomfield" = "Custom field";
"kProfileAddCustom" = "Add Custom field";
"kProfileAddCustomKey" = "Type Label";
"kProfileAddCustomValue" = "Type Value";
"kProfileAddCustomTitle" = "Edit Predefined Text";
"kFillClickPrompt" = "Click anywhere to place content";
"kFillDoSignPrompt" = "After signing, form fields cannot be edited.";
"kFill" = "Fill";
"kAddText" = "Add text";
"kAddCheck" = "Add check mark";
"kAddX" = "Add X";
"kAddDot" = "Add dot";
"kAddLine" = "Add line";
"kAddRoundedRectangle" = "Add rounded rectangle";
"kAddCombField" = "Add comb field";
