/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIExtensionsConfig.h"
#import <objc/runtime.h>

@interface SettingObj ()
@property (nonatomic, copy) NSSet *scaleUnitSet;
@property (nonatomic, assign) BOOL supportAutoFont;
@end

@implementation SettingObj

- (instancetype)init{
    self = [super init];
    if (self) {
        self.supportAutoFont = YES;
    }
    return self;
}

- (NSSet *)scaleUnitSet{
    if (!_scaleUnitSet) {
        _scaleUnitSet = [NSSet setWithArray:@[@"pt",@"m",@"cm", @"mm", @"inch", @"ft", @"yd", @"p"]];
    }
    return _scaleUnitSet;
}

- (CGFloat)textSize{
    if (!_supportAutoFont && _textSize <= 0) {
        return 1;
    }
    if (_textSize <= 0) {
        return 0;
    }
    return _textSize;
}

- (void)setOpacity:(CGFloat)opacity{
    if (opacity < 0) {
        opacity = 0;
    }else if (opacity > 1){
        opacity = 1;
    }
    _opacity = opacity;
}

- (void)setThickness:(unsigned int)thickness{
    if (thickness == 0 ) {
        thickness = 1;
    }else if (thickness > 12){
        thickness = 12;
    }
    _thickness = thickness;
}

- (void)setRotation:(unsigned int)rotation{
    NSSet *rotationSet = [NSSet setWithObjects:@0,@90,@180,@270, nil];
    if (![rotationSet containsObject:@(rotation)]) {
        rotation = 0;
    }
    _rotation = rotation;
}

- (void)setScaleFromValue:(unsigned int)scaleFromValue{
    if (scaleFromValue == 0) {
        scaleFromValue = 1;
    }
    _scaleFromValue = scaleFromValue;
}

- (void)setScaleToValue:(unsigned int)scaleToValue{
    if (scaleToValue == 0) {
        scaleToValue = 1;
    }
    _scaleToValue = scaleToValue;
}

- (void)setScaleToUnit:(NSString *)scaleToUnit{
    if (![self.scaleUnitSet containsObject:scaleToUnit]) {
        scaleToUnit = @"inch";
    }
    _scaleToUnit = scaleToUnit;
}

- (void)setScaleFromUnit:(NSString *)scaleFromUnit{
    if (![self.scaleUnitSet containsObject:scaleFromUnit]) {
        scaleFromUnit = @"inch";
    }
    _scaleFromUnit = scaleFromUnit;
}

- (void)setTextFace:(NSString *)textFace{
    NSSet *textFaceSet = [NSSet setWithObjects: @"Courier",@"Courier-Bold",@"Courier-BoldOblique", @"Courier-Oblique", @"Helvetica",@"Helvetica-Bold",@"Helvetica-BoldOblique",@"Helvetica-Oblique",@"Times-Roman", @"Times-Bold",@"Times-Italic",@"Times-BoldItalic", nil];
    if (![textFaceSet containsObject:textFace]) {
        textFace = @"Courier";
    }
    _textFace = textFace;
}

@end

@implementation Annotations
@end

@implementation Form
@end

@implementation UISettingsModel
@end

@implementation NSObject (UISettingsModel)

+ (NSDictionary *) defaultSettings {
    return @{
             @"pageMode":@"Signle",
             @"continuous":@0,
             @"colorMode" : @"Normal",
             @"zoomMode" : @"FitWidth",
             @"mapForegroundColor" : @"#5d5b71",
             @"mapBackgroundColor" : @"#00001b",
             @"reflowBackgroundColor" : @"#ffffff",
             //             @"autoBrightness" : @0,
             //             @"screenLock" : @0,
             @"disableFormNavigationBar" : @0,
             @"highlightForm" : @1,
             @"highlightLink" : @1,
             @"highlightFormColor" : @"#200066cc",
             @"highlightLinkColor" : @"#16007fff",
             @"fullscreen" : @1,
             @"annotations": @{
                     @"continuouslyAdd" : @0,
                     @"highlight": @{
                             @"color" : @"#ffff00",
                             @"opacity" : @1.0
                             },
                     @"underline": @{
                             @"color" : @"#66cc33",
                             @"opacity" : @1.0
                             },
                     @"squiggly": @{
                             @"color" : @"#ff6633",
                             @"opacity" : @1.0
                             },
                     @"strikeout": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0
                             },
                     @"insert": @{
                             @"color" : @"#993399",
                             @"opacity" : @1.0
                             },
                     @"replace": @{
                             @"color" : @"#0000ff",
                             @"opacity" : @1.0
                             },
                     @"line": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"rectangle": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"oval": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"arrow": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"pencil": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"polygon": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"cloud": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"polyline": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2
                             },
                     @"typewriter": @{
                             @"textColor" : @"#0000ff",
                             @"opacity" : @1.0,
                             @"textFace" : @"Courier",
                             @"textSize" : @18.0,
                             @"supportAutoFont" : @0
                             },
                     @"textbox": @{
                             @"color" : @"#ff0000",
                             @"textColor": @"#0000ff",
                             @"opacity" : @1.0,
                             @"textFace" : @"Courier",
                             @"textSize" : @18.0,
                             @"supportAutoFont" : @0
                             },
                     @"callout": @{
                             @"color": @"#ff0000",
                             @"textColor": @"#0000ff",
                             @"opacity" : @1.0,
                             @"textFace" : @"Courier",
                             @"textSize" : @18.0,
                             @"supportAutoFont" : @0
                             },
                     @"note": @{
                             @"color" : @"#ff6633",
                             @"opacity" : @1.0,
                             @"icon" : @"Comment"
                             },
                     @"attachment": @{
                             @"color" : @"#ff6633",
                             @"opacity" : @1.0,
                             @"icon" : @"Pushpin"
                             },
                     @"image": @{
                             @"rotation" : @0,
                             @"opacity" : @1.0
                             },
                     @"distance": @{
                             @"color" : @"#ff0000",
                             @"opacity" : @1.0,
                             @"thickness" : @2,
                             @"scaleFromUnit" : @"inch",
                             @"scaleToUnit" : @"inch",
                             @"scaleFromValue" : @1,
                             @"scaleToValue" : @1
                             },
                     @"redaction": @{
                             @"color" : @"#ff0000",
                             @"fillColor" : @"#000000",
                             @"textColor": @"#ff0000",
                             @"textFace" : @"Courier",
                             @"textSize" : @12,
                             @"supportAutoFont" : @0
                             }
                     },
             @"form": @{
                 @"textField": @{
                 @"textColor": @"#000000",
                 @"textFace": @"Courier",
                 @"textSize": @0
               },
               @"checkBox": @{
                 @"textColor": @"#000000"
               },
               @"radioButton": @{
                 @"textColor": @"#000000"
               },
               @"comboBox": @{
                 @"textColor": @"#000000",
                 @"textFace": @"Courier",
                 @"textSize": @0,
                 @"customText": @0
               },
               @"listBox": @{
                 @"textColor": @"#000000",
                 @"textFace": @"Courier",
                 @"textSize": @0,
                 @"multipleSelection": @0
               }
             },
             @"signature": @{
                     @"color" : @"#000000",
                     @"thickness" : @4
                     },
             @"commonlyUsed": @{
                     @"color" : @"#ff0000",
                     @"thickness" : @2
                     },
             };
    
}


+ (instancetype)modelWithDict:(NSDictionary *)dict{
    id objc = [[self alloc] init];
    unsigned int count;
    Ivar *ivarList = class_copyIvarList(self, &count);
    
    for (int i = 0; i < count; i++) {
        Ivar ivar = ivarList[i];
        NSString *name = [NSString stringWithUTF8String:ivar_getName(ivar)];
        NSString *key = [name substringFromIndex:1];
        id value = dict[key];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSString *type = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
            NSRange range = [type rangeOfString:@"\""];
            type = [type substringFromIndex:range.location + range.length];
            range = [type rangeOfString:@"\""];
            type = [type substringToIndex:range.location];
            Class modelClass = NSClassFromString(type);
            if (modelClass) {
                value  =  [modelClass modelWithDict:value];
            }
        }
        if (value) {
            if ([key hasSuffix:@"Color"] || [key hasSuffix:@"color"]) {
                NSString *colorHexStr = [((NSString *)value) stringByReplacingOccurrencesOfString:@"#" withString:@""];
                UIColor *color = [UIColor fs_colorWithARGBHexString:colorHexStr];
                if (color) {
                    [objc setValue:color forKey:key];
                }
            }
            else{
                [objc setValue:value forKey:key];
            }
        }
    }
    free(ivarList);
    return objc;
}

- (id)replaceSettingsWithDict:(NSDictionary *)dict{
    id objc = self;
    unsigned int count;
    Ivar *ivarList = class_copyIvarList(object_getClass(self), &count);
    
    for (int i = 0; i < count; i++) {
        Ivar ivar = ivarList[i];
        NSString *name = [NSString stringWithUTF8String:ivar_getName(ivar)];
        NSString *key = [name substringFromIndex:1];
        id value = dict[key];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSString *type = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
            NSRange range = [type rangeOfString:@"\""];
            type = [type substringFromIndex:range.location + range.length];
            range = [type rangeOfString:@"\""];
            type = [type substringToIndex:range.location];
            id modelObjc = [objc valueForKey:key];
            if (modelObjc) {
                value = [modelObjc replaceSettingsWithDict:value];
            }
        }
        if (value) {
            if ([key hasSuffix:@"Color"] || [key hasSuffix:@"color"]) {
                NSString *colorHexStr = [((NSString *)value) stringByReplacingOccurrencesOfString:@"#" withString:@""];
                UIColor *color = [UIColor fs_colorWithARGBHexString:colorHexStr];
                if (color) {
                    [objc setValue:color forKey:key];
                }
            }
            else{
                [objc setValue:value forKey:key];
            }
        }
    }
    free(ivarList);
    return objc;
}

@end



@interface UIExtensionsConfig ()
@property (nonatomic, nullable, readwrite) UISettingsModel *defaultSettings;

+ (NSSet<NSString *> *)standardTools;

@end

@implementation UIExtensionsConfig {
    
}

+ (NSSet<NSString *> *) standardTools {
    // exclude Tool_Attachment, Tool_Signature which are explicitly controlled by loadXXX property
    return [NSSet setWithObjects:Tool_Select, Tool_Note, Tool_Freetext, Tool_Textbox, Tool_Callout, Tool_Pencil, Tool_Eraser,
                                 Tool_Stamp, Tool_Insert, Tool_Replace, Tool_Highlight, Tool_Squiggly, Tool_Strikeout,
                                 Tool_Underline, Tool_Rectangle, Tool_Oval, Tool_Line, Tool_Arrow, Tool_Image,
                                 Tool_Polygon, Tool_Cloud, Tool_Distance, Tool_PolyLine, Tool_Audio, Tool_Video, Tool_Multiple_Selection, Tool_Redaction, Tool_Form, nil];
}

- (id)init {
    if ((self = [super init])) {
        [self commonInit];
    }
    return self;
}

- (id)initWithJSONData:(NSData *__nonnull)data {
    if (self = [super init]) {
        [self commonInit];

        NSError *error = nil;
        id JSONObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            return nil;
        }
        if ([JSONObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *modules = [((NSDictionary *) JSONObject) objectForKey:@"modules"];
            if ([modules isKindOfClass:[NSDictionary class]]) {
                id thumbnail = [modules objectForKey:@"thumbnail"];
                if (thumbnail)
                    self.loadThumbnail = [thumbnail boolValue];
                id outline = [modules objectForKey:@"outline"];
                if (outline)
                    self.loadOutline = [outline boolValue];
                id readingBookmark = [modules objectForKey:@"readingbookmark"];
                if (readingBookmark)
                    self.loadReadingBookmark = [readingBookmark boolValue];
                id attachment = [modules objectForKey:@"attachment"];
                if (attachment)
                    self.loadAttachment = [attachment boolValue];
                id signature = [modules objectForKey:@"signature"];
                if (signature)
                    self.loadSignature = [signature boolValue];
                id fillSign = [modules objectForKey:@"fillSign"];
                if (fillSign)
                    self.fillSign = [fillSign boolValue];
                
                id search = [modules objectForKey:@"search"];
                if (search)
                    self.loadSearch = [search boolValue];
                id pageNavigation = [modules objectForKey:@"pagenavigation"];
                if (!pageNavigation){
                     pageNavigation = [modules objectForKey:@"navigation"];
                }
                if (pageNavigation){
                    self.loadPageNavigation = [pageNavigation boolValue];
                }
                id form = [modules objectForKey:@"form"];
                if (form){
                    self.loadForm = [form boolValue];
                }else{
                    [_tools removeObject:Tool_Form];
                }
                
                id encryption = [modules objectForKey:@"encryption"];
                if (encryption)
                    self.loadEncryption = [encryption boolValue];
                id tools = [modules objectForKey:@"tools"];
                if (!tools) {
                    tools = [modules objectForKey:@"annotations"];
                }
                if (tools) {
                    [self loadToolsFromJSONObject:tools];
                }
                // back comparability, selection option is now moved to "tools"
                id selection = [modules objectForKey:@"selection"];
                if (selection && ![selection boolValue]) {
                    [_tools removeObject:Tool_Select];
                }
                id mulSelection = [modules objectForKey:@"multipleSelection"];
                if (mulSelection && ![mulSelection boolValue]) {
                    [_tools removeObject:Tool_Multiple_Selection];
                }
                
                
            } else {
                if ([(id) modules boolValue] == false) {
                    self.loadThumbnail = false;
                    self.loadReadingBookmark = false;
                    self.loadOutline = false;
                    self.loadAttachment = false;
                    self.loadForm = false;
                    self.loadSignature = false;
                    self.fillSign = false;
                    self.loadSearch = false;
                    self.loadPageNavigation = false;
                    self.loadEncryption = false;
                    self.tools = nil;
                }
            }
            NSDictionary *permissions = [((NSDictionary *) JSONObject) objectForKey:@"permissions"];
            if (permissions) {
                if ([permissions objectForKey:@"runJavaScript"]) {
                    self.runJavaScript = [[permissions objectForKey:@"runJavaScript"] boolValue];
                }
                if ([permissions objectForKey:@"copyText"]) {
                    self.copyText = [[permissions objectForKey:@"copyText"] boolValue];
                }
                if ([permissions objectForKey:@"disableLink"]) {
                    self.disableLink = [[permissions objectForKey:@"disableLink"] boolValue];
                }
            }
             NSDictionary *uiSettings = [((NSDictionary *) JSONObject) objectForKey:@"uiSettings"];
             if ([uiSettings isKindOfClass:[NSDictionary class]]) {
                 [self.defaultSettings replaceSettingsWithDict:uiSettings];
             }

        }
    }
    return self;
}

- (void)commonInit {
    _loadThumbnail = YES;
    _loadReadingBookmark = YES;
    _loadOutline = YES;
    _loadAttachment = YES;
    _loadSignature = YES;
    _fillSign = YES;
    _loadSearch = YES;
    _loadPageNavigation = YES;
    _loadForm = YES;
    _loadEncryption = YES;
    _runJavaScript = YES;
    _copyText = YES;
    _disableLink = NO;
    _tools = self.class.standardTools.mutableCopy;
    _defaultSettings = [UISettingsModel modelWithDict:[UISettingsModel defaultSettings]];
    //    _supportedAnnotationTypes = [NSMutableArray arrayWithObjects:@(FSAnnotNote), @(FSAnnotHighlight),
    //                                 @(FSAnnotUnderline), @(FSAnnotSquiggly), @(FSAnnotStrikeOut), @(FSAnnotSquare),
    //                                 @(FSAnnotCircle), @(FSAnnotFreeText), @(FSAnnotStamp), @(FSAnnotInk),
    //                                 @(FSAnnotCaret), @(FSAnnotLine), @(FSAnnotFileAttachment), nil];
}

- (void)loadToolsFromJSONObject:(id __nonnull)tools {
    if ([tools isKindOfClass:[NSDictionary class]]) {
        [(NSDictionary *) tools enumerateKeysAndObjectsUsingBlock:^(id _Nonnull key, id _Nonnull obj, BOOL *_Nonnull stop) {
            if ([obj boolValue]) {
                return;
            }
            if (![key isKindOfClass:[NSString class]]) {
                return;
            }
            [self removeTool:key];
        }];
    } else if ([tools boolValue] == false) {
        _tools = nil;
    }
}

- (BOOL)canInteractWithAnnot:(FSAnnot *)annot {
    NSString *tool = nil;
    const FSAnnotType type = [annot getType];
    switch (type) {
    case FSAnnotFileAttachment:
        return self.loadAttachment;
    case FSAnnotWidget: {
        FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
        FSField *field = [widget getField];
        if (field && ![field isEmpty] && [field getType] == FSFieldTypeSignature) {
            return self.loadSignature;
        } else {
            return self.loadForm;
        }
    }
    case FSAnnotNote:
        tool = Tool_Note;
        break;
    case FSAnnotInk:
        tool = Tool_Pencil;
        break;
    case FSAnnotStamp:
        tool = Tool_Stamp;
        break;
    case FSAnnotCaret:
        tool = [Utility isReplaceText:[[FSCaret alloc] initWithAnnot:annot]] ? Tool_Replace : Tool_Insert;
        break;
    case FSAnnotHighlight:
        tool = Tool_Highlight;
        break;
    case FSAnnotSquiggly:
        tool = Tool_Squiggly;
        break;
    case FSAnnotStrikeOut:
        tool = [Utility isReplaceText:[[FSStrikeOut alloc] initWithAnnot:annot]] ? Tool_Replace : Tool_Strikeout;
        break;
    case FSAnnotUnderline:
        tool = Tool_Underline;
        break;
    case FSAnnotSquare:
        tool = Tool_Rectangle;
        break;
    case FSAnnotCircle:
        tool = Tool_Oval;
        break;
    case FSAnnotFreeText: {
        NSString *intent = [([[FSFreeText alloc] initWithAnnot:annot]) getIntent];
        if (intent.length == 0) {
            tool = Tool_Textbox;
        } else if ([intent caseInsensitiveCompare:@"FreeTextTypewriter"] == NSOrderedSame) {
            tool = Tool_Freetext;
        } else if ([intent caseInsensitiveCompare:@"FreeTextCallout"] == NSOrderedSame) {
            tool = Tool_Callout;
        }
        break;
    }
    case FSAnnotLine: {
        FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
        BOOL isArrow = [[markup getIntent] caseInsensitiveCompare:@"LineArrow"] == NSOrderedSame;
        if (isArrow) {
            tool = Tool_Arrow;
            break;
        }
        BOOL isDistance = [[markup getIntent] caseInsensitiveCompare:@"LineDimension"] == NSOrderedSame;
        if (isDistance) {
            tool = Tool_Distance;
            break;
        }
        tool = Tool_Line;
        break;
    }
        case FSAnnotScreen: {
            FSScreen *screen = [[FSScreen alloc] initWithAnnot:annot];
            if (screen.screenActiontype == FSScreenActionTypeUnknow || screen.screenActiontype == FSScreenActionTypeImage) {
                tool = Tool_Image;
                break;
            }
            if (screen.screenActiontype == FSScreenActionTypeAudio) {
                tool = Tool_Audio;
            }else if (screen.screenActiontype == FSScreenActionTypeVideo){
                tool = Tool_Video;
            }
        break;
    }
    case FSAnnotPolygon: {
        FSBorderInfo *border = [[[FSPolygon alloc] initWithAnnot:annot] getBorderInfo];
        BOOL isCloud = ([border getStyle] == FSBorderInfoCloudy);
        if (isCloud) {
            tool = Tool_Cloud;
        } else {
            tool = Tool_Polygon;
        }
        break;
    }
    case FSAnnotPolyLine:
        tool = Tool_PolyLine;
        break;
    case FSAnnotRedact:
            tool = Tool_Redaction;
            break;
    case FSAnnotSound:
            tool = Tool_Audio;
            break;
    default:
        break;
    }
    if (!tool) {
        return false;
    }
    return [_tools containsObject:tool];
}

- (BOOL)isToolEnabled:(NSString *)tool {
    tool = [self getStandardNameForTool:tool];
    if (tool) {
        return [_tools containsObject:tool];
    }
    return false;
}

- (void)addTool:(NSString *)tool {
    tool = [self getStandardNameForTool:tool];
    if (tool) {
        [_tools addObject:tool];
    }
}

- (void)removeTool:(NSString *)tool {
    tool = [self getStandardNameForTool:tool];
    if (tool) {
        [_tools removeObject:tool];
    }
}

- (BOOL)isTool:(NSString *)tool1 equivalentToTool:(NSString *)tool2 {
    if (([tool1 caseInsensitiveCompare:tool2] == NSOrderedSame)) {
        return true;
    }
    tool1 = [tool1 lowercaseString];
    tool2 = [tool2 lowercaseString];
    for (NSArray *aliasArray in @[ @[ @"note", @"comment" ],
                                   @[ @"freetext", @"free text", @"typewriter", @"type writer" ],
                                   @[ @"textbox", @"text box" ],
                                   @[ @"callout", @"call out" ],
                                   @[ @"ink", @"pencil" ],
                                   @[ @"replace", @"replace text", @"replacetext" ],
                                   @[ @"insert", @"insert text", @"inserttext" ],
                                   @[ @"circle", @"oval" ],
                                   @[ @"rectangle", @"square" ],
                                   @[ @"arrow", @"arrow line" ],
                                   @[ @"attachment", @"fileattachment", @"file attachment" ],
                                   @[ @"select", @"selection", @"select text", @"selecttext" ],
                                   @[ @"image" ],
                                   @[ @"distance" ],
                                   @[ @"audio" ],
                                   @[ @"video" ]]) {
        if ([aliasArray containsObject:tool1] && [aliasArray containsObject:tool2]) {
            return true;
        }
    }
    return false;
}

- (NSString *)getStandardNameForTool:(NSString *)tool {
    if ([self.class.standardTools containsObject:tool]) {
        return tool;
    }
    for (NSString *standardTool in self.class.standardTools) {
        if ([self isTool:standardTool equivalentToTool:tool]) {
            return standardTool;
        }
    }
    return nil;
}

@end


