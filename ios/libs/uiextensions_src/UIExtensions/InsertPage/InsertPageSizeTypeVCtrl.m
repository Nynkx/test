/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "InsertPageSizeTypeVCtrl.h"

#import "InsertPageConfigTypeTableCell.h"

static NSString *const InsertPageConfigTypeTableCellID = @"InsertPageConfigTypeTableCell";

@interface InsertPageSizeTypeVCtrl ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (nonatomic, copy) NSArray *dataSource;
@property (nonatomic, strong) InsertPageConfigTypeObj *currentCellObj;
@end

@implementation InsertPageSizeTypeVCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kPageSize");
    
    [self creatDataSource];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.cancelBtn setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    [self.okBtn setTitle:FSLocalizedForKey(@"kOK") forState:UIControlStateNormal];
    
    [self tableView:self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedSizeTypeRow inSection:0]];
}

- (void)creatDataSource{
    NSArray *tempArr = @[ FSLocalizedForKey(@"kLetter"), @"A3", @"A4", FSLocalizedForKey(@"kLegal"), FSLocalizedForKey(@"kledger") ];
    NSMutableArray *dataSource = @[].mutableCopy;
    for (int i = 0 ; i < tempArr.count; i++) {
        InsertPageConfigTypeObj *obj = [[InsertPageConfigTypeObj alloc] init];
        obj.sizeTypeLabText = tempArr[i];
        obj.selected = NO;
        if (i == self.selectedSizeTypeRow) {
            obj.selected = YES;
            self.currentCellObj = obj;
        }
        [dataSource addObject:obj];
    }
    self.dataSource = dataSource.copy;
}

- (void)tableView:(UITableView *)tableView selectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:0];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)clickedCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)clickedOK:(id)sender {
    !self.selectedSizeType?:self.selectedSizeType(self.selectedSizeTypeRow, ((InsertPageConfigTypeObj *)self.dataSource[self.selectedSizeTypeRow]).sizeTypeLabText);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma -mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InsertPageConfigTypeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:InsertPageConfigTypeTableCellID];
    [cell setSizeTypeTableCellObj:self.dataSource[indexPath.row]];
    return cell;
}

#pragma -mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:[self.dataSource indexOfObject:self.currentCellObj] inSection:0];
    self.currentCellObj.selected = NO;
    InsertPageConfigTypeObj *obj = self.dataSource[indexPath.row];
    obj.selected = YES;
    self.currentCellObj = obj;
    
    [tableView reloadRowsAtIndexPaths:@[lastIndexPath, indexPath] withRowAnimation:0];
    self.selectedSizeTypeRow = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end

