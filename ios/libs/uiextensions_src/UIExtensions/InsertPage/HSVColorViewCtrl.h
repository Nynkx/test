/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "FSViewController.h"

typedef void (^SelectedColor)(UIColor * _Nullable color);

NS_ASSUME_NONNULL_BEGIN

@interface HSVColorViewCtrl : FSViewController
@property (nonatomic, strong) UIColor *currentColor;

@property (nonatomic, copy) SelectedColor selectedColor;
@end

NS_ASSUME_NONNULL_END
