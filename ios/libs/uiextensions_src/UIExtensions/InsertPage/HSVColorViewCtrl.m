/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "HSVColorViewCtrl.h"
#import "BrightnessView.h"
#import "SelectedColorView.h"
#import "ColorWheel.h"


@interface HSVColorViewCtrl ()<BrightnessViewDelegate, ColorWheelDelegate>
@property (weak, nonatomic) IBOutlet ColorWheel *wheelView;
@property (weak, nonatomic) IBOutlet BrightnessView *brightnessView;
@property (weak, nonatomic) IBOutlet SelectedColorView *selectedColorView;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (nonatomic, copy) IBOutletCollection(UIView) NSArray *viewsArr;

@property (nonatomic, strong) UIView *seletedView;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat hue;
@property (nonatomic, assign) CGFloat saturation;
@property (nonatomic, assign) CGFloat brightness;
@end

@implementation HSVColorViewCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = FSLocalizedForKey(@"kPageColor");
    [self.cancelBtn setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    [self.okBtn setTitle:FSLocalizedForKey(@"kOK") forState:UIControlStateNormal];
    
    [self setViewColor:_currentColor];
    [self addTap];
    
    self.selectedColorView.leftColorView.backgroundColor = self.currentColor;
    self.wheelView.delegate = self;
    self.brightnessView.delegate = self;
    
    _hue = 1.0;
    _saturation = 1.0;
    _brightness = 1.0;
}

- (void)addTap{
    for (int i = 0; i < _viewsArr.count; i++) {
        UIView *view = _viewsArr[i];
        view.tag = 300 + i;
        view.layer.cornerRadius = 3;
        view.layer.borderWidth = 1.f;
        view.layer.borderColor = [UIColor colorWithRed:239.f/255.0 green:239.f/255.0 blue:244.f/255.0 alpha:1].CGColor;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDefaultColor:)];
        [view addGestureRecognizer:tap];
    }
}

- (void)selectDefaultColor:(UITapGestureRecognizer *)tap{
    UIView *view = tap.view;
    if (_seletedView != view) {
        [self deselectView];
        _seletedView = view;
        [self setColorView:_seletedView];
    }
}

- (void)setColorView:(UIView *)view{
    _color = view.backgroundColor;
    _selectedColorView.rightColorView.backgroundColor = _color;
    _seletedView = view;
    
    CGRect viewRect = view.frame;
    CALayer *selectLayer = [[CALayer alloc] init];
    selectLayer.frame = CGRectMake(- 2, - 2, viewRect.size.width + 4, viewRect.size.height + 4);
    selectLayer.borderWidth = 2.f;
    selectLayer.borderColor = [ThemeLikeBlueColor CGColor];
    selectLayer.cornerRadius = 3.0f;
    selectLayer.backgroundColor = [UIColor clearColor].CGColor;
    [view.layer addSublayer:selectLayer];
}

- (void)deselectView{
    for (CALayer *layer in _seletedView.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    _seletedView = nil;
}

- (void)setViewColor:(UIColor *)color{
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    BOOL flag = [color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (!flag) {
        NSLog(@"The color provided to HSVColorPicker is not convertible to HSV");
    }
    
    _hue = hue;
    _saturation = saturation;
    _brightness = brightness;
    _color = color;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    UIView *view = _seletedView;
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        [self deselectView];
        self.seletedView = view;
        [self setColorView:self.seletedView];
    }];
}

- (IBAction)clickedCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)clickedOK:(id)sender {
    !self.selectedColor?:self.selectedColor(_color);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - BrightnessViewDelegate
- (void)brightnessSelected:(CGFloat)brightness{
    [self deselectView];
    _brightness = brightness;
    _color = [UIColor colorWithHue:_hue saturation:_saturation brightness:_brightness alpha:1.0];
    [_wheelView setViewBrightness:_brightness];
    _selectedColorView.rightColorView.backgroundColor = _color;
    
}

#pragma mark - ColorWheelDelegate
- (void)hueSaturationAtPoint:(CGFloat)hue saturation:(CGFloat)saturation{
    [self deselectView];
    _hue = hue;
    _saturation = saturation;
    _color = [UIColor colorWithHue:_hue saturation:_saturation brightness:_brightness alpha:1.0];
    [_brightnessView setViewColor:_color];
     _selectedColorView.rightColorView.backgroundColor = _color;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
