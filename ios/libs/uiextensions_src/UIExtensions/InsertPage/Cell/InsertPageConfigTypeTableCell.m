/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "InsertPageConfigTypeTableCell.h"

@interface InsertPageConfigTypeTableCell ()
@property (weak, nonatomic) IBOutlet UILabel *sizeTypeLab;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@end

@implementation InsertPageConfigTypeTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.useCustomSeparator = YES;
}

- (void)setSizeTypeTableCellObj:(InsertPageConfigTypeObj *)obj{
    self.sizeTypeLab.text = obj.sizeTypeLabText;
    self.doneBtn.hidden = !obj.selected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@implementation InsertPageConfigTypeObj
@end
