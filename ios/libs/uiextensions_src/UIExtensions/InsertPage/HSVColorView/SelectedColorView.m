/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SelectedColorView.h"

@interface SelectedColorView ()

@end

@implementation SelectedColorView

- (instancetype)initWithFrame:(CGRect)frame color:(UIColor *)color{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self commonInit];
}

- (void)commonInit{
    [self configViewBorder:self];
    [self configViewBorder:self.leftColorView.superview];
}

- (void)configViewBorder:(UIView *)view{
    view.layer.cornerRadius = 3;
    view.layer.borderWidth = 1.f;
    view.layer.borderColor = [UIColor colorWithRed:239.f/255.0 green:239.f/255.0 blue:244.f/255.0 alpha:1].CGColor;
}

@end
