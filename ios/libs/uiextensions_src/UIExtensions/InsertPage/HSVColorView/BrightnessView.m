/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "BrightnessView.h"


@interface BrightnessView ()
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) CAShapeLayer *indicator;
@property (nonatomic, strong) CAGradientLayer *colorLayer;

@property (nullable) CGColorRef indicatorColor;
@property (nullable) CGColorRef indicatorStrokeColor;

@property (nonatomic, assign) CGPoint point;
@property (nonatomic, assign) CGFloat indicatorBorderWidth;

@end

@implementation BrightnessView
- (instancetype)initWithFrame:(CGRect)frame color:(UIColor *)color{
    self = [super initWithFrame:frame];
    if (self) {
        _color = color;
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    _color = [UIColor whiteColor];
    
    [self commonInit];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    _colorLayer.frame = CGRectMake(0, 1, self.frame.size.width, 24);
    
    _point = [self getPointFromColor:_color];

    [self drawIndicator];
}

- (void)commonInit{
    _indicatorStrokeColor = [UIColor grayColor].CGColor;
    _indicatorColor = [UIColor whiteColor].CGColor;
    _indicatorBorderWidth = 1;
    
    _point = [self getPointFromColor:_color];
    
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    BOOL flag = [_color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (!flag) {
        NSLog(@"The color provided to HSVColorPicker is not convertible to HSV");
    }
    _colorLayer = [[CAGradientLayer alloc] init];
    _colorLayer.colors = @[
                           (__bridge id)[UIColor blackColor].CGColor,
                           (__bridge id)[UIColor colorWithHue:hue saturation:saturation brightness:1 alpha:1].CGColor
                           ];
    _colorLayer.locations = @[@0.0, @1.0];
    _colorLayer.startPoint = CGPointMake(0.0, 0.5);
    _colorLayer.endPoint = CGPointMake(1.0, 0.5);
    _colorLayer.frame = CGRectMake(0, 1, self.frame.size.width, 24);
    [self.layer insertSublayer:_colorLayer below:self.layer];
    
    _indicator = [[CAShapeLayer alloc] init];
    _indicator.strokeColor = _indicatorStrokeColor;
    _indicator.fillColor = _indicatorColor;
    _indicator.lineWidth = _indicatorBorderWidth;
    [self.layer addSublayer:_indicator];
    
    [self drawIndicator];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event{
    [self touchHandler:touches];
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event{
    [self touchHandler:touches];
}

- (void)touchHandler:(NSSet<UITouch *> *)touches{
    
    UITouch *firstTouch = [touches anyObject];
    if (firstTouch) {
        _point = [firstTouch locationInView:self];
    }
    
    _point.y = self.frame.size.height/2;
    _point.x = [self getXCoordinate:_point.x];
    if (self.delegate && [self.delegate respondsToSelector:@selector(brightnessSelected:)]) {
        [self.delegate brightnessSelected:[self getBrightnessFromPoint]];
    }
    [self drawIndicator];
}

- (CGFloat)getXCoordinate:(CGFloat)coord{
    if (coord < 1) {
        return 1;
    }
    if (coord > self.frame.size.width - 1 ) {
        return self.frame.size.width - 1;
    }
    return coord;
}

- (void)drawIndicator{
    if (!CGPointEqualToPoint(_point, CGPointZero)) {
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(_point.x - 3, 0, 6, 28) cornerRadius:3];
        _indicator.path = path.CGPath;
    }
}

- (CGFloat)getBrightnessFromPoint{
    return _point.x / self.frame.size.width;
}

- (CGPoint)getPointFromColor:(UIColor *)color{
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    BOOL flag = [color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (!flag) {
        NSLog(@"The color provided to HSVColorPicker is not convertible to HSV");
    }
    return CGPointMake( brightness * self.frame.size.width, self.frame.size.height / 2);
}

- (void)setViewColor:(UIColor *)color{
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    BOOL flag = [color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (!flag) {
        NSLog(@"The color provided to HSVColorPicker is not convertible to HSV");
    }
    _colorLayer.colors = @[
                          (__bridge id)[UIColor blackColor].CGColor,
                          (__bridge id)[UIColor colorWithHue:hue saturation:saturation brightness:1 alpha:1].CGColor
                          ];
}

@end
