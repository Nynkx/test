/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ColorWheel.h"
#import <CoreGraphics/CoreGraphics.h>
#import "Const.h"

struct IndicatorCoordinate {
    CGPoint point;
    BOOL isCenter;
};

struct Color {
    CGFloat hue;
    CGFloat saturation;
};

typedef struct IndicatorCoordinate IndicatorCoordinate;
typedef struct Color Color;

@interface ColorWheel ()
@property (nonatomic, strong) CAShapeLayer *indicatorLayer;
@property (nonatomic, strong) CALayer *wheelLayer;
@property (nonatomic, strong) CAShapeLayer *brightnessLayer;

@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign) CGPoint point;
@property (nonatomic, assign) CGFloat brightness;
@property (nonatomic, assign) CGFloat indicatorBorderWidth;
@property (nonatomic, assign) CGFloat indicatorCircleRadius;

@property (nullable) CGColorRef indicatorColor;

@end

@implementation ColorWheel

- (instancetype)initWithFrame:(CGRect)frame color:(UIColor *)color{
    self = [super initWithFrame:frame];
    if (self) {
        _color = color;
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    _color = [UIColor whiteColor];
    
    [self commonInit];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    _wheelLayer.frame = CGRectMake(10, 10, self.frame.size.width - 20, self.frame.size.height - 20);
    CGImageRef imageRef = [self later_iOS10_createColorWheel:_wheelLayer.frame.size];
    _wheelLayer.contents = (__bridge id _Nullable)(imageRef);
    CFRelease(imageRef);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(10.5, 10.5, self.frame.size.width - 20.5, self.frame.size.height - 20.5) cornerRadius:(self.frame.size.height - 20.5)/2];
    _brightnessLayer.path = path.CGPath;
    
    [self setViewColor:_color];
}

- (void)commonInit{
    _brightness = 1.0;
    _indicatorCircleRadius = 8;
    _indicatorColor = [UIColor lightGrayColor].CGColor;
    _indicatorBorderWidth = 2.f;
    _scale = [UIScreen mainScreen].scale;
    
    _wheelLayer = [[CALayer alloc] init];
    _wheelLayer.frame = CGRectMake(10, 10, self.frame.size.width - 20, self.frame.size.height - 20);
    CGImageRef imageRef = [self later_iOS10_createColorWheel:_wheelLayer.frame.size];
    _wheelLayer.contents = (__bridge id _Nullable)(imageRef);
    CFRelease(imageRef);
    [self.layer addSublayer:_wheelLayer];
    
    _brightnessLayer = [[CAShapeLayer alloc] init];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(10.5, 10.5, self.frame.size.width - 20.5, self.frame.size.height - 20.5) cornerRadius:(self.frame.size.height - 20.5)/2];
    _brightnessLayer.path = path.CGPath;
    
    [self.layer addSublayer:_brightnessLayer];
    
    _indicatorLayer = [[CAShapeLayer alloc] init];
    _indicatorLayer.strokeColor = _indicatorColor;
    _indicatorLayer.lineWidth = _indicatorBorderWidth;
    _indicatorLayer.fillColor = nil;
    [self.layer addSublayer:_indicatorLayer];
    
    [self setViewColor:_color];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event{
    _indicatorCircleRadius = 12;
    [self touchHandler:touches];
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event{
    [self touchHandler:touches];
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event{
    _indicatorCircleRadius = 8;
    [self touchHandler:touches];
}

- (void)touchHandler:(NSSet<UITouch *> *)touches{
    
    UITouch *firstTouch = [touches anyObject];
    if (firstTouch) {
        _point = [firstTouch locationInView:self];
    }
    
    IndicatorCoordinate indicator =  [self getIndicatorCoordinate:_point];
    _point = indicator.point;
    
    Color color = {0,0};
    if (!indicator.isCenter) {
        color = [self hueSaturationAtPoint:CGPointMake(_point.x * _scale, _point.y * _scale)];
    }
    self.color = [UIColor colorWithHue:0 saturation:0 brightness:self.brightness alpha:1.0];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(hueSaturationAtPoint:saturation:)]) {
        [self.delegate hueSaturationAtPoint:color.hue saturation:color.saturation];
    }
    [self drawIndicator];
}

- (IndicatorCoordinate)getIndicatorCoordinate:(CGPoint)coord{
    CALayer *wheelLayer = self.wheelLayer;
    CGFloat dimension = MIN(wheelLayer.frame.size.width, wheelLayer.frame.size.height);
    CGFloat radius = dimension/2;
    CGPoint wheelLayerCenter = CGPointMake(wheelLayer.frame.origin.x + radius, wheelLayer.frame.origin.y + radius);
    
    CGFloat dx = coord.x - wheelLayerCenter.x;
    CGFloat dy = coord.y - wheelLayerCenter.y;
    CGFloat distance = sqrt(dx*dx + dy*dy);
    CGPoint outputCoord = coord;
    
    if (distance > radius) {
        CGFloat theta = atan2(dy, dx);
        outputCoord.x = radius * cos(theta) + wheelLayerCenter.x;
        outputCoord.y = radius * sin(theta) + wheelLayerCenter.y;
    }

    CGFloat whiteThreshold = 10;
    BOOL isCenter = NO;
    if (distance < whiteThreshold) {
        outputCoord.x = wheelLayerCenter.x;
        outputCoord.y = wheelLayerCenter.y;
        isCenter = YES;
    }
    IndicatorCoordinate indicatorCoordinate = {outputCoord,isCenter};
    return indicatorCoordinate;
}

- (void)drawIndicator{
    if (!CGPointEqualToPoint(self.point, CGPointZero)) {
        CGRect rect = CGRectMake(self.point.x - _indicatorCircleRadius, self.point.y - _indicatorCircleRadius, _indicatorCircleRadius * 2, _indicatorCircleRadius * 2);
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:_indicatorCircleRadius];
        _indicatorLayer.path = path.CGPath;
        _indicatorLayer.fillColor = self.color.CGColor;
    }
}

- (CGImageRef)later_iOS10_createColorWheel:(CGSize)size{
    CGFloat dimension = MIN(size.width, size.height);
    NSDictionary *parameters = @{@"inputColorSpace": (__bridge_transfer id)CGColorSpaceCreateDeviceRGB(),
                                 @"inputDither": @0,
                                 @"inputRadius": @(dimension),
                                 @"inputSoftness": @0,
                                 @"inputValue": @1};
    CIFilter *filter = [CIFilter filterWithName:@"CIHueSaturationValueGradient" withInputParameters:parameters];
    CIImage *outputImage = [filter outputImage];
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef imageRef = [context createCGImage:outputImage fromRect:[outputImage extent]];
    return imageRef;
}

- (CGImageRef)before_iOS10_createColorWheel:(CGSize)size{
    
    CGFloat originalWidth = size.width;
    CGFloat originalHeight = size.height;
    CGFloat dimension = MIN(originalWidth * _scale, originalHeight * _scale);
    int bufferLength  = (int)(dimension * dimension * 4);
    
    CFMutableDataRef bitmapData = CFDataCreateMutable(NULL, 0);
    CFDataSetLength(bitmapData, (CFIndex)bufferLength);
    UInt8 *bitmap = CFDataGetMutableBytePtr(bitmapData);
    
    for (int y = 0; y < dimension; y++) {
        for (int x = 0; x < dimension; x++) {
            HSV hsv = { 0,  0, 0,  0};
            RGB rgb = { 0,  0, 0,  0};
            
            Color color = [self hueSaturationAtPoint:CGPointMake(x, y)];
            CGFloat hue = color.hue;
            CGFloat saturation = color.saturation;
            CGFloat a = 0.0;
            
            if (saturation < 1.0) {
                if (saturation > 0.99) {
                    a = (1.0 - saturation) * 100;
                } else {
                    a = 1.0;
                }
                hsv.hue = hue;
                hsv.saturation = saturation;
                hsv.brightness = 1.0;
                hsv.alpha = a;
                rgb = hsv2rgb(hsv);
            }
            
            NSInteger i = 4 * (x + y * dimension);
            bitmap[i] = rgb.red * 0xff;
            bitmap[i + 1] = rgb.green * 0xff;
            bitmap[i + 2] = rgb.blue * 0xff;
            bitmap[i + 3] = rgb.alpha * 0xff;
        }
    }

    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(bitmapData);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef imageRef = CGImageCreate((int)dimension, (int)dimension, 8, 32, (int)dimension * 4, colorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaLast, dataProvider, NULL, 0, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    CFRelease(bitmapData);
    
    return imageRef;
}

- (Color)hueSaturationAtPoint:(CGPoint)position{
    CGFloat c = _wheelLayer.frame.size.width * _scale / 2;
    CGFloat dx = (position.x - c) / c;
    CGFloat dy = (c - position.y) / c;
    CGFloat d = sqrt((dx * dx + dy * dy));
    CGFloat saturation = d;
    CGFloat hue;
    if (d == 0) {
        hue = 0;
    } else {
        hue = acos(dx/d) / (M_PI) / 2.0;
        if (dy < 0) {
            hue = 1.0 - hue;
        }
    }
    Color color = {hue, saturation};
    return color;
}

- (CGPoint)pointAtHueSaturation:(CGFloat)hue saturation:(CGFloat)saturation{
    
    CGFloat dimension = MIN(_wheelLayer.frame.size.width, _wheelLayer.frame.size.height);
    CGFloat radius = saturation * dimension / 2;
    CGFloat x = dimension / 2 + radius * cos(hue * (CGFloat)(M_PI) * 2) + 10;
    CGFloat y = dimension / 2 + radius * sin(hue * (CGFloat)(M_PI) * 2) + 10;
    return CGPointMake(x, y);
}

- (void)setViewColor:(UIColor *)viewColor{
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    
    BOOL flag = [viewColor getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (!flag) {
        NSLog(@"The color provided to HSVColorPicker is not convertible to HSV");
    }
    _color = viewColor;
    _brightness = brightness;
    _brightnessLayer.fillColor = [UIColor colorWithWhite:0 alpha:1.0 - self.brightness].CGColor;
    _point = [self pointAtHueSaturation:hue saturation:saturation];
    [self drawIndicator];
}

- (void)setViewBrightness:(CGFloat)viewBrightness{
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    
    BOOL flag = [self.color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (!flag) {
        NSLog(@"The color provided to HSVColorPicker is not convertible to HSV");
    }
    _brightness = viewBrightness;
    _brightnessLayer.fillColor = [UIColor colorWithWhite:0 alpha:1.0 - self.brightness].CGColor;
    _color = [UIColor colorWithHue:hue saturation:saturation brightness:viewBrightness alpha:1];
    [self drawIndicator];
}

@end
