/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "InsertPageConfigVCtrl.h"

#import "IQKeyboardManager.h"
#import "InsertPageSizeTypeVCtrl.h"
#import "InsertPageOrientationTypeVCtrl.h"
#import "HSVColorViewCtrl.h"

@interface InsertPageConfigVCtrl ()<UITextFieldDelegate, UIScrollViewDelegate,IRotationEventListener>
{
    __weak UIExtensionsManager *_extensionsManager;
}
@property (weak, nonatomic) IBOutlet UITextField *pageCount;
@property (weak, nonatomic) IBOutlet UILabel *pageSize;
@property (weak, nonatomic) IBOutlet UIView *pageColor;
@property (weak, nonatomic) IBOutlet UILabel *pageOrientation;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (nonatomic, assign) NSUInteger selectedSizeTypeRow;
@property (nonatomic, assign) InsertPageOrientationType orientationType;
@property (nonatomic, strong) UIColor *selectedColor;

@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIButton *leftPageBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightPageBtn;

@property (nonatomic, strong) UIButton *curselectPageTexturebtn;


@end

@implementation InsertPageConfigVCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = FSLocalizedForKey(@"kAddPage");
    self.pageSize.text = FSLocalizedForKey(@"kLetter");
    self.pageOrientation.text = FSLocalizedForKey(@"kPortrait");
    [self.cancelBtn setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    [self.okBtn setTitle:FSLocalizedForKey(@"kOK") forState:UIControlStateNormal];
    
    self.pageCount.delegate = self;
    self.pageScrollView.delegate = self;
    
    self.selectedSizeTypeRow = 0;
    self.orientationType = InsertPageOrientationTypePortrait;
    
    self.selectedColor = [UIColor whiteColor];
    self.pageColor.layer.borderWidth = 0.5;
    self.pageColor.layer.borderColor = [UIColor grayColor].CGColor;
    self.pageColor.backgroundColor = self.selectedColor;
}

- (void)dealloc{
    [_extensionsManager unregisterRotateChangedListener:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [self createPageTextureSelect];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

- (void)setUIExtensionsManager:(UIExtensionsManager *)extensionsManager{
    [extensionsManager registerRotateChangedListener:self];
    _extensionsManager = extensionsManager;
    
}

- (IBAction)touchRow:(id)sender {
    [self.pageCount resignFirstResponder];
}


- (IBAction)clickedCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)clickedOK:(id)sender {

    FSPDFPageSize pageSize = FSPDFPageSizeLetter;
    switch (self.selectedSizeTypeRow) {
        case 0:
            pageSize = FSPDFPageSizeLetter;
            break;
        case 1:
            pageSize = FSPDFPageSizeA3;
            break;
        case 2:
            pageSize = FSPDFPageSizeA4;
            break;
        case 3:
            pageSize = FSPDFPageSizeLegal;
            break;
            
        default:
            break;
    }
    if (self.selectedSizeTypeRow != 4) {
        if([_extensionsManager.pdfViewCtrl insertPages:self.delegate.insertIndex pageSize:pageSize style:(int)(_curselectPageTexturebtn.tag - 200) color:_selectedColor.fs_argbValue rotation:(FSRotation)self.orientationType count:_pageCount.text.intValue]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(InsertPageSucceed:count:)]) {
                [self.delegate InsertPageSucceed:_extensionsManager.pdfViewCtrl.currentDoc count:_pageCount.text.intValue];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }else{
        CGFloat width = 72 * 11;
        CGFloat height = 72 * 17;
        if ([_extensionsManager.pdfViewCtrl insertPages:self.delegate.insertIndex width:width height:height style:(int)(_curselectPageTexturebtn.tag - 200) color:_selectedColor.fs_argbValue rotation:(FSRotation)self.orientationType count:_pageCount.text.intValue]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(InsertPageSucceed:count:)]) {
                [self.delegate InsertPageSucceed:_extensionsManager.pdfViewCtrl.currentDoc count:_pageCount.text.intValue];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }

}
- (IBAction)selectPageTexture:(UIButton *)sender {
    if (self.pageScrollView.contentOffset.x > 0) {
        if (sender == self.leftPageBtn) {
            self.leftPageBtn.selected = YES;
            self.rightPageBtn.selected = NO;
            [self.pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }else
    {
        if (sender == self.rightPageBtn) {
            self.rightPageBtn.selected = YES;
            self.leftPageBtn.selected = NO;
            [self.pageScrollView setContentOffset:CGPointMake(_curselectPageTexturebtn.frame.size.width * 2 + _curselectPageTexturebtn.frame.size.width/2, 0) animated:YES];
        }
    }
    
}

- (void)createPageTextureSelect{
    
    CGFloat scrW = self.view.fs_width - 30 * 2;
    CGFloat spacing = scrW/12;
    CGFloat pageTextureBtnW = (scrW - spacing * 2)/ 3;
    CGFloat pageTextureBtnH = CGRectGetHeight(self.pageScrollView.frame);
    [self.pageScrollView setContentSize:CGSizeMake(scrW * 2  , CGRectGetHeight(self.pageScrollView.frame))];
    if (DEVICE_iPAD || UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        spacing = scrW/20;
        pageTextureBtnW = (scrW - spacing * 4)/ 5;
        [self.pageScrollView setContentSize:CGSizeMake(scrW  , CGRectGetHeight(self.pageScrollView.frame))];
    }
    
    NSArray *imgName = @[ @"", @"thumb_lined", @"thumb_grid", @"thumb_graph", @"thumb_music" ];
    NSArray *titleStr = @[ FSLocalizedForKey(@"kBlank"), FSLocalizedForKey(@"kLined"), FSLocalizedForKey(@"kGrid"), FSLocalizedForKey(@"kGraph"), FSLocalizedForKey(@"kMusic") ];
    void (^createPageTexture)(void) = ^{
        for (int i = 0; i < imgName.count; i++) {
            UIImage *image = nil;
            if (i == 0) {
                image = [UIImage imageNamed:imgName[1] inBundle:[NSBundle bundleForClass:[UIExtensionsManager class]] compatibleWithTraitCollection:nil];
                image = [self imageWithColor:[UIColor whiteColor] size:image.size];
            }else{
                image = [UIImage imageNamed:imgName[i] inBundle:[NSBundle bundleForClass:[UIExtensionsManager class]] compatibleWithTraitCollection:nil];
            }
            
            image = [self imageWithBorderWidth:2.f borderColor:[UIColor colorWithRed:239.f/255.0 green:239.f/255.0 blue:244.f/255.0 alpha:1] image:image];
            
            UIButton *pageTextureBtn = [[UIButton alloc] initWithFrame:CGRectMake(pageTextureBtnW * i + spacing * i, 0, pageTextureBtnW, pageTextureBtnH - 20)];
            pageTextureBtn.tag = 200 + i;
            [pageTextureBtn addTarget:self action:@selector(didSelectedPageTexture:) forControlEvents:UIControlEventTouchUpInside];
            if (i == 0) {
                pageTextureBtn.selected = YES;
                self->_curselectPageTexturebtn = pageTextureBtn;
            }
            [pageTextureBtn setImage:image forState:UIControlStateNormal];
            [pageTextureBtn setImage:[self imageWithBorderWidth:2.f borderColor:ThemeLikeBlueColor image:image] forState:UIControlStateSelected];
            
            [self.pageScrollView addSubview:pageTextureBtn];
            
            CGRect labRect = pageTextureBtn.frame;
            labRect.origin.y = CGRectGetMaxY(pageTextureBtn.frame);
            labRect.size.height = 20.f;
            UILabel *titleLab = [[UILabel alloc] initWithFrame:labRect];
            titleLab.textColor = [UIColor colorWithRed:111/255.f green:113/255.f blue:121/255.f alpha:1];
            titleLab.font = [UIFont systemFontOfSize:15.f];
            titleLab.textAlignment = NSTextAlignmentCenter;
            titleLab.text = titleStr[i];
            titleLab.tag = 300 + i;
            [self.pageScrollView addSubview:titleLab];
            
            [self.pageScrollView setContentSize:CGSizeMake(CGRectGetMaxX(pageTextureBtn.frame)  , CGRectGetHeight(self.pageScrollView.frame))];
        }
    };
    
    BOOL isExist = YES;
    if (self.pageScrollView.subviews.count) {
        for (int i = 0 ; i < self.pageScrollView.subviews.count; i++ ) {
            CGRect frame = CGRectMake(pageTextureBtnW * i + spacing * i, 0, pageTextureBtnW, pageTextureBtnH - 20);
            UIView *pageTextureBtn = [self.pageScrollView viewWithTag:200+i];
            if (!pageTextureBtn && ((imgName.count - 1) > i)) {
                isExist = NO;
                break;
            }
            pageTextureBtn.frame = frame;
            UIView *titleLab = [self.pageScrollView viewWithTag:300+i];
            frame.origin.y = CGRectGetMaxY(frame);
            frame.size.height = 20.f;
            titleLab.frame = frame;
        }
        if (!isExist) {
            createPageTexture();
        }
    }else{
        createPageTexture();
    }

    if (ABS(scrW - self.pageScrollView.contentSize.width) <= 2) {
        self.leftPageBtn.hidden = YES;
        self.rightPageBtn.hidden = YES;
    }else{
        self.leftPageBtn.hidden = NO;
        self.rightPageBtn.hidden = NO;
        [self scrollViewDidEndDecelerating:self.pageScrollView];
    }
}

- (void)didSelectedPageTexture:(UIButton *)btn{
    if (btn != _curselectPageTexturebtn) {
        btn.selected = YES;
        _curselectPageTexturebtn.selected = NO;
        _curselectPageTexturebtn = btn;
    }
}
#if _MAC_CATALYST_
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:nil
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {[self createPageTextureSelect];}];
}
#endif

#pragma -mark IRotationEventListener
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self createPageTextureSelect];
}

#pragma -mark UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > scrollView.frame.size.width * 0.5) {
        self.rightPageBtn.selected = YES;
        self.leftPageBtn.selected = NO;
    }else
    {
        self.leftPageBtn.selected = YES;
        self.rightPageBtn.selected = NO;
    }
}

#pragma -mark UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!textField.text.length) {
        textField.text = @"1";
        NSString *title = FSLocalizedForKey(@"kWarning");
        NSString *message = [NSString stringWithFormat:@"%@ 1-100", FSLocalizedForKey(@"kWrongPageNumber")];
        AlertViewCtrlShow (title, message);
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length == 0 && range.length == 1) {
        return YES;
    }
    NSString *currentString = [textField.text stringByAppendingString:string];
    if (currentString.intValue>100) {
        textField.text = @"100";
    }
    NSString *regex = @"(^[0-9]{1,3})";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL flag = [pred evaluateWithObject:currentString];
    if (!flag || currentString.intValue>100 || !currentString.intValue) {
        NSString *title = FSLocalizedForKey(@"kWarning");
        NSString *message = [NSString stringWithFormat:@"%@ 1-100", FSLocalizedForKey(@"kWrongPageNumber")];
        AlertViewCtrlShow (title, message);
        return NO;
    }
    return flag;
}

#pragma -mark UITextFieldDelegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"GoToInsertPageSizeTypeVCtrl"]) {
        InsertPageSizeTypeVCtrl *sizeTypeVC = segue.destinationViewController;
        sizeTypeVC.selectedSizeTypeRow = self.selectedSizeTypeRow;
        [sizeTypeVC setSelectedSizeType:^(NSUInteger selectedSizeTypeRow, NSString *sizeTypeString) {
            self.selectedSizeTypeRow = selectedSizeTypeRow;
            self.pageSize.text = sizeTypeString;
        }];
    }
    else if ([segue.identifier isEqualToString:@"GoToInsertPageOrientationTypeVCtrl"]) {
        InsertPageOrientationTypeVCtrl *orientationTypeVC = segue.destinationViewController;
        orientationTypeVC.orientationType = self.orientationType;
        [orientationTypeVC setSelectedOrientationType:^(InsertPageOrientationType orientationType, NSString *orientationTypeString) {
            self.orientationType = orientationType;
            self.pageOrientation.text = orientationTypeString;
        }];
    }
    else if ([segue.identifier isEqualToString:@"GoToHSVColorSelectVCtrl"]) {
        HSVColorViewCtrl *hsvColorSelectVC = segue.destinationViewController;
        hsvColorSelectVC.currentColor = self.selectedColor;
        [hsvColorSelectVC setSelectedColor:^(UIColor *color) {
            self.selectedColor = color;
            self.pageColor.backgroundColor = color;
        }];
    }
}

- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size{
    CGSize imageSize = size;
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}

- (UIImage *)imageWithBorderWidth:(CGFloat)borderW borderColor:(UIColor *)color image:(UIImage *)image{
    
    CGSize size = CGSizeMake(image.size.width + 2 * borderW, image.size.height + 2 * borderW);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, size.width, size.height)];
    [color set];
    [path fill];

    path = [UIBezierPath bezierPathWithRect:CGRectMake(borderW, borderW, image.size.width, image.size.height)];
    [path addClip];
    [image drawInRect:CGRectMake(borderW, borderW, image.size.width, image.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    return newImage;
}

@end
