/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSViewController.h"

typedef void (^SelectedSizeType)(NSUInteger selectedSizeTypeRow, NSString * _Nullable sizeTypeString);

NS_ASSUME_NONNULL_BEGIN

@interface InsertPageSizeTypeVCtrl : FSViewController

@property (nonatomic, assign) NSUInteger selectedSizeTypeRow;
@property (nonatomic, copy) SelectedSizeType selectedSizeType;
@end

NS_ASSUME_NONNULL_END
