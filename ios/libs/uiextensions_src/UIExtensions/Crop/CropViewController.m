/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CropViewController.h"
#import "MBProgressHUD.h"
#import "SettingBar+private.h"
#import "SettingBar.h"
#import "Utility.h"

@class SettingBar;

@interface CropPDFView : UIView
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, assign) int pageIndex;
@property (nonatomic, assign) BOOL needPauseNow;
- (id)initWithFrame:(CGRect)frame;
@end

@interface CropView : UIView

@property (nonatomic, assign) CGRect originalRect;
@property (nonatomic, assign) CGSize originalSize;
@property (nonatomic, assign) CGRect currentRect;
@property (nonatomic, assign) CGRect currentRealRect;
@property (nonatomic, assign) CGRect maxCropRect;
@property (nonatomic, assign) int currentEditPointIndex;
@property (nonatomic, strong) CropPDFView *pdfView;

- (instancetype)initWithCropPDFView:(CropPDFView *)pdfView;
- (void)resetDefaultCrop;
- (void)resetNoCrop;

@end

@interface CropViewController ()<UIToolbarDelegate> {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

@property (nonatomic, strong) CropPDFView *pdfView;
@property (nonatomic, strong) CropView *cropView;
@property (assign, nonatomic) BOOL isApplyToAllOddPages;
@property (assign, nonatomic) BOOL isApplyToAllEvenPages;
@property (nonatomic, assign) CGRect currentRect;
@end

@implementation CropViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.isApplyToAllOddPages = NO;
        self.isApplyToAllEvenPages = NO;
    }
    return self;
}

- (void)setBottomToolbar:(UIToolbar *)bottomToolbar{
    _bottomToolbar = bottomToolbar;
    if (bottomToolbar) {
        bottomToolbar.delegate = self;
        [bottomToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(bottomToolbar.superview.fs_mas_bottom);
        }];
    }
}

- (void)setTopToolbar:(UIToolbar *)topToolbar{
    _topToolbar = topToolbar;
    if (topToolbar) {
        [topToolbar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(topToolbar.superview.fs_mas_top);
        }];
    }
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar{
    return UIBarPositionBottom;
}

- (void)setExtension:(UIExtensionsManager *)extensionsManager {
    _extensionsManager = extensionsManager;
    _pdfViewCtrl = extensionsManager.pdfViewCtrl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.extendedLayoutIncludesOpaqueBars = YES; //replace "self.wantsFullScreenLayout = YES;" by deprecation
    self.topToolbar.clipsToBounds = YES;
    self.viewStatusBar.backgroundColor = NormalBarBackgroundColor;
    self.topToolbar.backgroundColor = NormalBarBackgroundColor;
    self.bottomToolbar.backgroundColor = NormalBarBackgroundColor;

    [self.buttonCrop setTitle:FSLocalizedForKey(@"kCropDone") forState:UIControlStateNormal];
    [self.buttonDetect setTitle:FSLocalizedForKey(@"kCropDetect") forState:UIControlStateNormal];
    [self.buttonFull setTitle:FSLocalizedForKey(@"kCropFull") forState:UIControlStateNormal];
    [self.buttonNoCrop setTitle:FSLocalizedForKey(@"kCropNo") forState:UIControlStateNormal];
    [self.buttonSmartCrop setTitle:FSLocalizedForKey(@"kCropSmart") forState:UIControlStateNormal];
    [self.buttonPageIndex setTitle:[NSString stringWithFormat:@"%d", self.pdfView.pageIndex + 1] forState:UIControlStateNormal];
    [self.buttonApply2All setTitle:FSLocalizedForKey(@"kApply2All") forState:UIControlStateNormal];
    if (self.pdfView.pageIndex & 1) {
        [self.buttonApply2OddEven setTitle:FSLocalizedForKey(@"kApply2Even") forState:UIControlStateNormal];
    } else {
        [self.buttonApply2OddEven setTitle:FSLocalizedForKey(@"kApply2Odd") forState:UIControlStateNormal];
    }

    [self.buttonCrop setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.buttonDetect setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.buttonFull setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.buttonNoCrop setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.buttonSmartCrop setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.buttonPageIndex setTitleColor:DarkGrayThemeTextColor forState:UIControlStateNormal];
    [self.buttonApply2All setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.buttonApply2OddEven setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];

    [self.buttonPrevPage setImage:[UIImage imageNamed:@"formfill_pre_normal.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.buttonPrevPage setImage:[UIImage imageNamed:@"formfill_pre_pressed.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateDisabled];
    [self.buttonNextPage setImage:[UIImage imageNamed:@"formfill_next_normal.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.buttonNextPage setImage:[UIImage imageNamed:@"formfill_next_pressed.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateDisabled];

    self.buttonPageIndex.enabled = NO;
    
    self.viewStatusBar.backgroundColor = UIColor_DarkMode(self.viewStatusBar.backgroundColor, ThemeBarColor_Dark);
    self.viewBackground.backgroundColor = UIColor_DarkMode(self.viewBackground.backgroundColor, PageBackgroundColor_Dark);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.pdfView.needPauseNow = YES;
}

- (CGRect)getPageContentCGRect:(FSPDFPage *)page isWholePage:(BOOL)isWholePage {
    CGRect rect = CGRectZero;

    float pageWidth = [page getWidth];
    float pageHeight = [page getHeight];
    
//    FSRotation viewRotation = (FSRotation)[_pdfViewCtrl getViewRotation];
//    if (viewRotation == FSRotation90 || viewRotation == FSRotation270) {
//        float tmp = pageWidth;
//        pageWidth =  pageHeight;
//        pageHeight = tmp;
//    }
    
    FSRectF *rectBBox = nil;
    if (isWholePage) {
        return CGRectMake(0, 0, pageWidth, pageHeight);
    } else {
        if ([page isParsed] == NO) {
            BOOL parseSuccess = [Utility parsePage:page];
            if (!parseSuccess) {
                return rect;
            }
        }

        rectBBox = [page calcContentBBox:FSPDFPageCalcContentsBox];
        if ([rectBBox isEmpty]) {
            rectBBox = [Utility getPageRealRect:page];
        }
    }
    
    if (rectBBox.left != 0 || rectBBox.right != 0 || rectBBox.top != 0 || rectBBox.bottom != 0) {
        FSRectF *newRectBBox = [[FSRectF alloc] init];
        newRectBBox.left = rectBBox.left;
        newRectBBox.right = rectBBox.right;
        newRectBBox.top = rectBBox.top;
        newRectBBox.bottom = rectBBox.bottom;

        CGRect newRect = CGRectZero;
        FSRotation rotation = [page getRotation];
        
//        FSRotation viewrotation = (FSRotation)[_pdfViewCtrl getViewRotation];
//        int endrotate = (rotation + viewrotation) % 4;
//        rotation = (FSRotation)endrotate;
        FSRectF *realRect = [Utility getPageRealRect:page];
        
        switch (rotation) {
        case FSRotation0:
            newRect = CGRectMake(ABS(MIN(realRect.left, realRect.right) -  MIN(newRectBBox.left, newRectBBox.right)),
                                 MAX(realRect.top, realRect.bottom) - MAX(newRectBBox.top, newRectBBox.bottom),
                                 ABS(newRectBBox.right - newRectBBox.left),
                                 ABS(newRectBBox.top - newRectBBox.bottom));
            break;
        case FSRotation90:
            newRect = CGRectMake(ABS(realRect.bottom - newRectBBox.bottom),
                                 ABS(realRect.left - newRectBBox.left),
                                 ABS(newRectBBox.top - newRectBBox.bottom),
                                 ABS(newRectBBox.right - newRectBBox.left));
            break;
        case FSRotation180:
            newRect = CGRectMake(ABS(realRect.right - newRectBBox.right),
                                 ABS(realRect.bottom - newRectBBox.bottom),
                                 ABS(newRectBBox.right - newRectBBox.left),
                                 ABS(newRectBBox.top - newRectBBox.bottom));
            break;
        case FSRotation270:
            newRect = CGRectMake(
                ABS(realRect.top - newRectBBox.top),
                ABS(realRect.right - newRectBBox.right),
                ABS(newRectBBox.top - newRectBBox.bottom),
                ABS(newRectBBox.right - newRectBBox.left));
            break;
        default:
            break;
        }

        rect = newRect;
    }

    return rect;
}

- (FSRectF *)convertCGRect2PDFRect:(CGRect)cgrect {
    FSRectF *pdfrect = [[FSRectF alloc] init];
    [pdfrect setLeft:0];
    [pdfrect setBottom:0];
    [pdfrect setRight:0];
    [pdfrect setTop:0];
    CGRect temp = cgrect;
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:self.pdfView.pageIndex];
    FSRotation rotation = [page getRotation];
    
    FSRotation viewrotation = (FSRotation)[_pdfViewCtrl getViewRotation];
    int endrotate = (rotation + viewrotation) % 4;
    rotation = (FSRotation)endrotate;
    
    if (rotation == FSRotation90 || rotation == FSRotation270) {
        //to do
    }

    pdfrect.left = temp.origin.x;
    pdfrect.right = temp.origin.x + temp.size.width;
    pdfrect.top = [page getHeight] - temp.origin.y;
    pdfrect.bottom = pdfrect.top - temp.size.height;
    return pdfrect;
}


- (FSRectF *)convertCGRect2PDFRect:(CGRect)cgrect pageIndex:(int)pageIndex{
    
    /**
     * Same page size, different rotation angle, using PDF FSRectF
     */
    FSPDFPage *currentCropPage = [_pdfViewCtrl.currentDoc getPage:self.pdfView.pageIndex];
    if ([currentCropPage isEmpty]) return [[FSRectF alloc] init];
    float currentCropPageWidth = [currentCropPage getWidth];
    float currentCropPageHeight = [currentCropPage getHeight];
//    FSRotation viewRotation = (FSRotation)[_pdfViewCtrl getViewRotation];
//    if (viewRotation == FSRotation90 || viewRotation == FSRotation270) {
//        float tmp = currentCropPageWidth;
//        currentCropPageWidth =  currentCropPageHeight;
//        currentCropPageHeight = tmp;
//    }
    CGSize currentCropPageSize = CGSizeMake(currentCropPageWidth, currentCropPageHeight);
    UIEdgeInsets currentCropInsets = [Utility convertCGRect2Insets:cgrect size:currentCropPageSize];
    FSRotation currentCropPageRotation = [currentCropPage getRotation];
    
    FSRotation viewrotation = (FSRotation)[_pdfViewCtrl getViewRotation];
    int endrotate = (currentCropPageRotation + viewrotation) % 4;
    currentCropPageRotation = (FSRotation)endrotate;
    
    if (currentCropPageRotation == FSRotation90) {
        currentCropInsets = UIEdgeInsetsMake(currentCropInsets.right, currentCropInsets.top, currentCropInsets.left, currentCropInsets.bottom);
    }else if (currentCropPageRotation == FSRotation180) {
        currentCropInsets = UIEdgeInsetsMake(currentCropInsets.bottom, currentCropInsets.right, currentCropInsets.top, currentCropInsets.left);
    }else if (currentCropPageRotation == FSRotation270) {
        currentCropInsets = UIEdgeInsetsMake(currentCropInsets.left, currentCropInsets.bottom, currentCropInsets.right, currentCropInsets.top);
    }
    
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if ([page isEmpty]) return [[FSRectF alloc] init];
    float pageWidth = [page getWidth];
    float pageHeight = [page getHeight];
    
//    if (viewRotation == FSRotation90 || viewRotation == FSRotation270) {
//        float tmp = pageWidth;
//        pageWidth =  pageHeight;
//        pageHeight = tmp;
//    }
    CGSize pageSize = CGSizeMake(pageWidth, pageHeight);
    UIEdgeInsets insets = currentCropInsets;
    FSRotation rotation = [page getRotation];
    
    int tmpEndrotate = (rotation + viewrotation) % 4;
    rotation = (FSRotation)tmpEndrotate;
    
//    CGFloat top, left, bottom, right;
    if (rotation == FSRotation90) {
        insets = UIEdgeInsetsMake(insets.left, insets.bottom, insets.right, insets.top);
    }else if (rotation == FSRotation180) {
        insets = UIEdgeInsetsMake(insets.bottom, insets.right, insets.top, insets.left);
    }else if (rotation == FSRotation270) {
        insets = UIEdgeInsetsMake(insets.right, insets.top, insets.left, insets.bottom);
    }
    /**
     * When top, left, bottom, right, any item is larger than the page size, crop will not be applied to the page
     */
    
    if (insets.top >= pageHeight ||
        insets.bottom >= pageHeight ||
        insets.left >= pageWidth ||
        insets.right >= pageWidth) {
        insets.top = 0;
        insets.bottom = 0;
        insets.left = 0;
        insets.right = 0;
    }
    
    FSRectF *pdfrect = [[FSRectF alloc] init];
    pdfrect.top = insets.top;
    pdfrect.left = insets.left;
    pdfrect.bottom = pageSize.height - insets.bottom;
    pdfrect.right = pageSize.width - insets.right;
    return pdfrect;
}

- (void)calcCurrentPreviewedCropRects {
    FSPDFPage *page = nil;
    @try {
        page = [_pdfViewCtrl.currentDoc getPage:self.pdfView.pageIndex];
    } @catch (NSException *_) {
        self.cropView.currentRect = CGRectMake(0, 0, 0, 0);
        return;
    }
    if ([page isEmpty]) {
        self.cropView.currentRect = CGRectMake(0, 0, 0, 0);
        return;
    }
    float pageWidth = [page getWidth];
    float pageHeight = [page getHeight];

//    FSRotation viewRotation = (FSRotation)[_pdfViewCtrl getViewRotation];
//    if (viewRotation == FSRotation90 || viewRotation == FSRotation270) {
//        float tmp = pageWidth;
//        pageWidth =  pageHeight;
//        pageHeight = tmp;
//    }
    
    float bgWidth = self.viewBackground.bounds.size.width;
    float bgHeight = self.viewBackground.bounds.size.height;

    float scale = bgWidth / pageWidth;
    if (pageWidth / pageHeight - bgWidth / bgHeight < 0.000001) {
        scale = bgHeight / pageHeight;
    }
    CGSize size = CGSizeMake(pageWidth * scale, pageHeight * scale);
    [self.pdfView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.pdfView.superview);
        make.size.mas_equalTo(size);
    }];
    
    [self.cropView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.mas_equalTo(self.pdfView);
    }];
    
    [self.cropView layoutIfNeeded];
    [self.pdfView layoutIfNeeded];
    
    self.cropView.originalSize = CGSizeMake(pageWidth, pageHeight);
    self.cropView.originalRect = [self getPageContentCGRect:[_pdfViewCtrl.currentDoc getPage:self.pdfView.pageIndex] isWholePage:NO];
    int margin = 15;
    self.cropView.originalRect = [Utility convertCGRectWithMargin:self.cropView.originalRect size:self.cropView.originalSize margin:margin];
    self.cropView.currentRect = ({
        CGSize pageSize = CGSizeMake(pageWidth, pageHeight);
        CGRect pageContentRect = [self getPageContentCGRect:page isWholePage:NO];
        int margin = 15;
        CGRect pageCropRect = [Utility convertCGRectWithMargin:pageContentRect size:pageSize margin:margin];
        pageCropRect;
    });
    self.cropView.maxCropRect = ({
        CGRect pageContentRect = [self getPageContentCGRect:page isWholePage:YES];
        pageContentRect;
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self creatPdfView];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ( DEVICE_iPAD){
        self.pdfView.needPauseNow = YES;
        [self.pdfView removeFromSuperview];
        self.pdfView = nil;
        [self creatPdfView];
    }
}

- (void)creatPdfView{
    if (!self.pdfView) {
        self.pdfView = [[CropPDFView alloc] init];
        self.pdfView.pdfViewCtrl = _pdfViewCtrl;
        self.pdfView.pageIndex = [_pdfViewCtrl getCurrentPage];

        self.cropView = [[CropView alloc] initWithCropPDFView:self.pdfView];
        self.cropView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.cropView.opaque = NO;
        self.cropView.userInteractionEnabled = YES;

        [self.pdfView addSubview:self.cropView];
        [self.viewBackground addSubview:self.pdfView];
        
        [self.view layoutIfNeeded];
        [self.viewBackground layoutIfNeeded];
        
        [self calcCurrentPreviewedCropRects];
        [self setPreviousAndNextBtnEnable];
    }
}

#if _MAC_CATALYST_
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:nil
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
                                    self.pdfView.needPauseNow = YES;
                                    [self.pdfView removeFromSuperview];
                                    self.currentRect = self.cropView.currentRect;
                                    self.pdfView = nil;
                                    [self creatPdfView];
                                    self.cropView.currentRect = self.currentRect;
    }];
}
#endif

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}
*/

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)orientationDidChange:(NSNotification *)aNotification {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:self.pdfView.pageIndex];
    float pageWidth = [page getWidth];
    float pageHeight = [page getHeight];
    
    [self.viewBackground layoutIfNeeded];
    
    float bgWidth = self.viewBackground.bounds.size.width;
    float bgHeight = self.viewBackground.bounds.size.height;

    float scale = bgWidth / pageWidth;
    if (pageWidth / pageHeight - bgWidth / bgHeight < 0.000001) {
        scale = bgHeight / pageHeight;
    }
    CGSize size = CGSizeMake(pageWidth * scale, pageHeight * scale);
    [self.pdfView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.pdfView.superview);
        make.size.mas_equalTo(size);
    }];
}

/*
- (void)viewDidUnload {
    [self setViewBackground:nil];
    [self setButtonNoCrop:nil];
    [self setButtonSmartCrop:nil];
    [self setButtonCrop:nil];
    [self setButtonDetect:nil];
    [self setButtonFull:nil];
    [self setButtonApply2All:nil];
    [self setButtonApply2OddEven:nil];
    [self setButtonPrevPage:nil];
    [self setButtonNextPage:nil];
    [super viewDidUnload];
}
*/

- (IBAction)autoCropClicked:(id)sender {
    [self.cropView resetDefaultCrop];
}

- (IBAction)fullCropClicked:(id)sender {
    [self.cropView resetNoCrop];
}

- (IBAction)apply2allClicked:(id)sender {
    self.isApplyToAllOddPages = YES;
    self.isApplyToAllEvenPages = YES;
}

- (IBAction)apply2oddevenClicked:(id)sender {
    UIButton *button = (UIButton *)sender;
    if ([FSLocalizedForKey(@"kApply2Odd") isEqualToString:button.titleLabel.text]) {
        self.isApplyToAllOddPages = YES;
        self.isApplyToAllEvenPages = NO;
    } else {
        self.isApplyToAllOddPages = NO;
        self.isApplyToAllEvenPages = YES;
    }
}

- (void)setPreviousAndNextBtnEnable {
    if (self.pdfView.pageIndex <= 0) {
        self.buttonPrevPage.enabled = NO;
    } else {
        self.buttonPrevPage.enabled = YES;
    }

    if (self.pdfView.pageIndex >= [_pdfViewCtrl getPageCount] - 1) {
        self.buttonNextPage.enabled = NO;
    } else {
        self.buttonNextPage.enabled = YES;
    }

    [self.buttonPageIndex setTitle:[NSString stringWithFormat:@"%d", self.pdfView.pageIndex + 1] forState:UIControlStateNormal];
    if (self.pdfView.pageIndex & 1) {
        [self.buttonApply2OddEven setTitle:FSLocalizedForKey(@"kApply2Even") forState:UIControlStateNormal];
    } else {
        [self.buttonApply2OddEven setTitle:FSLocalizedForKey(@"kApply2Odd") forState:UIControlStateNormal];
    }
}

- (IBAction)prevPageClicked:(id)sender {
    if (self.pdfView.pageIndex > 0) {
        [_pdfViewCtrl gotoPage:--self.pdfView.pageIndex animated:NO];
        [self setPreviousAndNextBtnEnable];
        [self calcCurrentPreviewedCropRects];
        [self.pdfView setNeedsDisplay];
        [self.cropView setNeedsDisplay];
    }
    self.isApplyToAllOddPages = NO;
    self.isApplyToAllEvenPages = NO;
}

- (IBAction)nextPageClicked:(id)sender {
    if (self.pdfView.pageIndex < [_pdfViewCtrl getPageCount] - 1) {
        [_pdfViewCtrl gotoPage:++self.pdfView.pageIndex animated:NO];
        [self setPreviousAndNextBtnEnable];
        [self calcCurrentPreviewedCropRects];
        [self.pdfView setNeedsDisplay];
        [self.cropView setNeedsDisplay];
    }
    self.isApplyToAllOddPages = NO;
    self.isApplyToAllEvenPages = NO;
}

- (IBAction)smartCropClicked:(id)sender {
    MBProgressHUD *progressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressView.labelText = FSLocalizedForKey(@"kCropSmartCalculating");
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            [self->_pdfViewCtrl setCropMode:PDF_CROP_MODE_CONTENTSBOX];
            [self close];
        });
    });
}

- (IBAction)noCropClicked:(id)sender {
    ((UIButton *) [_extensionsManager.settingBar getItemView:CROPPAGE]).selected = NO;
    [_pdfViewCtrl setCropMode:PDF_CROP_MODE_NONE];
    _extensionsManager.settingBar.panAndZoomBtn.enabled = YES;
    [self close];
}

- (IBAction)doneClicked:(id)sender {
    int currentPageIndex = self.cropView.pdfView.pageIndex;
    CGRect cropRect = self.cropView.currentRect;
    void (^setPageCropRect)(int pageIndex) = ^(int pageIndex) {
        [self->_pdfViewCtrl setCropPageRect:pageIndex pdfRect:[self convertCGRect2PDFRect:cropRect pageIndex:pageIndex]];
    };
    setPageCropRect(currentPageIndex);
    int pageCount = [_pdfViewCtrl getPageCount];
    for (int i = 0; i < pageCount; i++) {
        if (i == currentPageIndex) {
            continue;
        }
        if (i % 2 == 1 && self.isApplyToAllEvenPages) {
            setPageCropRect(i);
        } else if (i % 2 == 0 && self.isApplyToAllOddPages) {
            setPageCropRect(i);
        }
    }

    [_pdfViewCtrl setCropMode:PDF_CROP_MODE_CUSTOMIZED];
    [self close];
}

- (void)close {
    self.isApplyToAllOddPages = NO;
    self.isApplyToAllEvenPages = NO;
    if (self.cropViewClosedHandler) {
        self.cropViewClosedHandler();
    }
    [self dismissViewControllerAnimated:YES
                             completion:^{
                             }];
}

@end

@implementation CropPDFView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.pageIndex = 0;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

// Draw into the layer
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)context {
    CGRect clipRect = CGContextGetClipBoundingBox(context);
    
    if (clipRect.size.width == 0 || clipRect.size.height == 0) {
        return;
    }

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        FSPDFPage *page = [self.pdfViewCtrl.currentDoc getPage:self.pageIndex];
        [Utility drawPage:page targetSize:clipRect.size shouldDrawAnnotation:YES progressive:^(UIImage * _Nonnull image, BOOL finish) {
            dispatch_async(dispatch_get_main_queue(), ^{
                layer.contents = (__bridge id)image.CGImage;
            });
        } needPause:^BOOL{
            return  self.needPauseNow;
        } rotation:FSRotation0 enableForPrint:NO];
    });
}
@end

@implementation CropView

- (instancetype)initWithCropPDFView:(CropPDFView *)pdfView;
{
    self = [super init];
    if (self) {
        // Initialization code
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        [self addGestureRecognizer:panGesture];

        self.pdfView = pdfView;
        _currentRealRect = CGRectZero;
    }
    return self;
}

- (void)setMaxCropRect:(CGRect)maxCropRect{
    float scale = self.bounds.size.width / self.originalSize.width;
    maxCropRect = CGRectMake(maxCropRect.origin.x * scale, maxCropRect.origin.y * scale, maxCropRect.size.width * scale, maxCropRect.size.height * scale);
    _maxCropRect =  maxCropRect;
}

- (void)resetDefaultCrop {
    self.currentRect = self.originalRect;
    [self setNeedsDisplay];
}

- (void)resetNoCrop {
    self.currentRect = CGRectMake(0, 0, self.originalSize.width, self.originalSize.height);
    [self setNeedsDisplay];
}

- (NSArray *)getMovePointInRect:(CGRect)rect {
    if (rect.size.width == 0 || rect.size.height == 0) {
        return nil;
    }
    float iconRadius = 7.5f;
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - iconRadius, rect.origin.y - iconRadius, iconRadius * 2, iconRadius * 2)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - iconRadius, rect.origin.y - iconRadius, iconRadius * 2, iconRadius * 2)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x - iconRadius, rect.origin.y + rect.size.height - iconRadius, iconRadius * 2, iconRadius * 2)]];
    [array addObject:[NSValue valueWithCGRect:CGRectMake(rect.origin.x + rect.size.width - iconRadius, rect.origin.y + rect.size.height - iconRadius, iconRadius * 2, iconRadius * 2)]];
    return array;
}

- (CGRect)getCurrentRealRect {
    float scale = self.bounds.size.width / self.originalSize.width;
    CGRect croppedRect = CGRectMake(self.currentRect.origin.x * scale, self.currentRect.origin.y * scale, self.currentRect.size.width * scale, self.currentRect.size.height * scale);
    return croppedRect;
}

- (CGRect)getCurrentRect:(CGRect)rect {
    float scale = self.originalSize.width / self.bounds.size.width;
    CGRect pageRect = CGRectMake(rect.origin.x * scale, rect.origin.y * scale, rect.size.width * scale, rect.size.height * scale);
    return pageRect;
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _currentEditPointIndex = -1;
        _currentRealRect = [self getCurrentRealRect];
        NSArray *movePointArray = [self getMovePointInRect:_currentRealRect];
        [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            CGRect dotRect = [obj CGRectValue];
            dotRect = CGRectInset(dotRect, -20, -20);
            if (CGRectContainsPoint(dotRect, point)) {
                self->_currentEditPointIndex = (int) idx;
                *stop = YES;
            }
        }];
        if (_currentEditPointIndex == -1) {
            if (CGRectContainsPoint(_currentRealRect, point)) {
                _currentEditPointIndex = 4;
            }
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (_currentEditPointIndex == -1) {
            return;
        }

        CGPoint translationPoint = [recognizer translationInView:self];
        [recognizer setTranslation:CGPointZero inView:self];
        float tw = translationPoint.x;
        float th = translationPoint.y;
        CGRect rect = _currentRealRect;

        if (_currentEditPointIndex == 0) {
            rect.origin.x += tw;
            rect.origin.y += th;
            rect.size.width -= tw;
            rect.size.height -= th;
        } else if (_currentEditPointIndex == 1) {
            rect.size.width += tw;
            rect.origin.y += th;
            rect.size.height -= th;
        } else if (_currentEditPointIndex == 2) {
            rect.origin.x += tw;
            rect.size.width -= tw;
            rect.size.height += th;
        } else if (_currentEditPointIndex == 3) {
            rect.size.width += tw;
            rect.size.height += th;
        } else if (_currentEditPointIndex == 4) {
            rect.origin.x += tw;
            rect.origin.y += th;
        }
        
        if (rect.size.height  == self.maxCropRect.size.height && rect.size.width == self.maxCropRect.size.width) {
            return;
        }
        if (rect.origin.x < 0) {
            rect.origin.x = 0;
        }
        if (rect.origin.y < 0) {
            rect.origin.y = 0;
        }
        if (rect.size.width > self.maxCropRect.size.width) {
            rect.size.width = self.maxCropRect.size.width;
        }
        if (rect.size.height > self.maxCropRect.size.height) {
            rect.size.height = self.maxCropRect.size.height;
        }
        
        if (rect.origin.x + rect.size.width > self.maxCropRect.size.width  && _currentEditPointIndex!= 0 && _currentEditPointIndex!= 2) {
            rect.size.width = rect.size.width;
            rect.origin.x = self.maxCropRect.size.width - rect.size.width;
        }
        if (rect.origin.y + rect.size.height > self.maxCropRect.size.height && _currentEditPointIndex!= 1 && _currentEditPointIndex!= 3) {
            rect.size.height = rect.size.height;
            rect.origin.y = self.maxCropRect.size.height - rect.size.height;
        }
        _currentRealRect = rect;
        [self setNeedsDisplay];
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        _currentEditPointIndex = -1;
        self.currentRect = [self getCurrentRect:CGRectStandardize(_currentRealRect)];
        _currentRealRect = CGRectZero;
        [self setNeedsDisplay];
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0, 0, 0, .2);
    CGContextFillRect(context, rect);

    CGRect croppedRect = _currentRealRect;
    if (CGRectEqualToRect(croppedRect, CGRectZero)) {
        croppedRect = [self getCurrentRealRect];
    }

    CGContextClearRect(context, croppedRect);
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, [[UIColor colorWithRGB:0x179cd8] CGColor]);
    CGContextStrokeRect(context, croppedRect);
    
    UIImage *dragDot = [UIImage imageNamed:@"annotation_drag" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    NSArray *movePointArray = [self getMovePointInRect:croppedRect];
    [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGRect dotRect = [obj CGRectValue];
        [dragDot drawAtPoint:dotRect.origin];
    }];
}

@end
