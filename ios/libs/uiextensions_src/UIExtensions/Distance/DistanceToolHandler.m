/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "DistanceToolHandler.h"

#define DEFAULT_RECT_WIDTH 200

@interface DistanceToolHandler ()

@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic, strong) FSLine *annot;
@property (nonatomic, assign) int currentPageIndex;
@property (nonatomic, strong) NSString *currentUnit;

@end

@implementation DistanceToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotLine;

        [_extensionsManager registerAnnotEventListener:self];
    }
    return self;
}

- (void)setStartPoint:(FSPointF *)startPoint {
    _startPoint = startPoint;
}

- (NSString *)getName {
    return Tool_Distance;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
    [self releaseData];
    if(self.annot)
    {
        FSAnnot* annot = self.annot;
        self.annot = nil;
        id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
        [annotHandler addAnnot:annot addUndo:YES];
    }
}

-(void)releaseData{
    if (self.currentPageIndex) {
        [_pdfViewCtrl refresh:self.currentPageIndex];
    }
    self.startPoint = nil;
    self.endPoint = nil;
    self.annot = nil;
    self.currentPageIndex = -1;
}
#pragma mark PageView Gesture+Touch

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    void (^end)(void) = ^{
        if (self.annot) {
            FSAnnot* annot = self.annot;
            self.annot = nil;
            id<IAnnotHandler> annotHandler = [self->_extensionsManager getAnnotHandlerByAnnot:annot];
            [annotHandler addAnnot:annot addUndo:YES];
            [self updateDistanceDataFromStartPoint:self.startPoint toEndPoint:self.endPoint];
        }
    };
    if (point.x > pageView.frame.size.width || point.y > pageView.frame.size.height || point.x < 0 || point.y < 0){
        end();
        return NO;
    }
    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];



    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if(self.annot)
            end();
        
        FSRectF *dibRect = [[FSRectF alloc] init];
        
        if (self.startPoint && self.endPoint) {
            dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        } else {
            [dibRect setLeft:dibPoint.x];
            [dibRect setBottom:dibPoint.y];
            [dibRect setRight:dibPoint.x + 0.1];
            [dibRect setTop:dibPoint.y + 0.1];
        }
        
        self.currentPageIndex = pageIndex;
        FSLine *annot = [self addAnnotToPage:pageIndex withRect:dibRect];
        if (!annot || [annot isEmpty]) {
            return NO;
        }
        self.annot = annot;
        [annot setLineStartStyle:FSMarkupEndingStyleOpenArrow];
        [annot setLineEndStyle:FSMarkupEndingStyleOpenArrow];
        self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];

        [annot setStartPoint:self.startPoint];
        [annot setEndPoint:self.endPoint];
        [_pdfViewCtrl lockRefresh];
        [annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        [_pdfViewCtrl refresh:CGRectZero pageIndex:pageIndex needRender:NO];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (pageIndex != self.annot.pageIndex) {
            end();
            return NO;
        }
        CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
        self.endPoint = dibPoint;
        [self.annot setEndPoint:dibPoint];
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        
        FSRectF *dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:pageIndex];
        CGRect refreshRect = CGRectUnion(oldRect, newRect);
        refreshRect = UIEdgeInsetsInsetRect(refreshRect, UIEdgeInsetsMake(-50, -50, -50, -50));
        [_pdfViewCtrl refresh:refreshRect pageIndex:pageIndex needRender:NO];
        
        [self updateDistanceDataFromStartPoint:self.startPoint toEndPoint:self.endPoint];
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        if ([Utility pointEqualToPoint:self.startPoint point:self.endPoint]) {
            FSAnnot* annot = self.annot;
            self.annot = nil;
            [self removeAnnot:annot];
            [_pdfViewCtrl refresh:self.currentPageIndex];
            return NO;
        }
        FSLine *annot1 = self.annot;
        [annot1 setStartPoint:self.startPoint];
        [annot1 setEndPoint:self.endPoint];
        [_pdfViewCtrl lockRefresh];
        [annot1 resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        
        end();
        return YES;
    }
    return NO;
}

- (void)removeAnnot:(FSAnnot *)annot {
    FSPDFPage *page = [annot getPage];
    [page removeAnnot:annot];
    
    self.annot = nil;
}

- (FSLine *)addAnnotToPage:(int)pageIndex withRect:(FSRectF *)rect {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;

    FSLine *annot = [[FSLine alloc] initWithAnnot:[page addAnnot:self.type rect:rect]];
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.color = [_extensionsManager getPropertyBarSettingColor:self.type];
    annot.opacity = [_extensionsManager getAnnotOpacity:self.type] / 100.0f;
    annot.lineWidth = [_extensionsManager getAnnotLineWidth:self.type];
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    [annot setIntent:@"LineDimension"];
    
    [annot setMeasureRatio:_extensionsManager.distanceUnit];
    
    NSArray *result = [Utility getDistanceUnitInfo:_extensionsManager.distanceUnit];
    _currentUnit = [result objectAtIndex:3];
    [annot setMeasureUnit:0 unit:[result objectAtIndex:3]];
    [annot setMeasureConversionFactor:0 factor:[[result objectAtIndex:2] floatValue]/[[result objectAtIndex:0] floatValue]];
    
    float distance = [Utility getDistanceFromX:self.startPoint toY:self.endPoint withUnit:_extensionsManager.distanceUnit];
    [annot setContent:[NSString stringWithFormat:@"%.2f %@",distance,_currentUnit]];
    
    annot.flags = FSAnnotFlagPrint;
    annot.subject = @"Distance";
    return annot;
}

-(void)updateDistanceDataFromStartPoint:(FSPointF *)start toEndPoint:(FSPointF *)end {
    float distance = [Utility getDistanceFromX:start toY:end withUnit:_extensionsManager.distanceUnit];
    NSString *distanceContent = [NSString stringWithFormat:@"%.2f %@",distance,_currentUnit];
    [self.annot setContent:distanceContent];
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (void)onAnnotWillDelete:(FSPDFPage *)page annot:(FSAnnot *)annot {
    FSLine *lineAnnot = [[FSLine alloc] initWithAnnot:annot];
    if (annot.type == FSAnnotLine && [[lineAnnot getIntent] isEqualToString:@"LineDimension"]) {
        [self releaseData];
    }
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (self.annot != nil && ![self.annot isEmpty] && self.startPoint && self.endPoint && self.currentPageIndex == pageIndex && ![Utility pointEqualToPoint:self.startPoint point:self.endPoint]) {
        FSLine *lineAnnot = [[FSLine alloc] initWithAnnot:self.annot];
        CGPoint startPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint  pageIndex:pageIndex];

        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:lineAnnot.fsrect pageIndex:pageIndex];
        CGContextSaveGState(context);

        CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
        CGContextTranslateCTM(context, 0, rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
        CGContextDrawImage(context, rect, [[Utility getAnnotImage:lineAnnot pdfViewCtrl:_pdfViewCtrl] CGImage]);

        CGContextRestoreGState(context);

        if ([[lineAnnot getIntent] isEqualToString:@"LineDimension"]) {
            //Draw Distance Text
            NSMutableParagraphStyle *textStyle = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
            textStyle.alignment = NSTextAlignmentLeft;

            NSDictionary *textFontAttributes = @{
                NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12],
                NSForegroundColorAttributeName : UIColor.redColor,
                NSParagraphStyleAttributeName : textStyle
            };

            float distance = [Utility getDistanceFromX:self.startPoint toY:self.endPoint withUnit:_extensionsManager.distanceUnit];
            NSString *distanceContent = [NSString stringWithFormat:@"%.2f %@",distance,_currentUnit];
            [distanceContent drawAtPoint:startPoint withAttributes:textFontAttributes];

        }
    }
}

@end
