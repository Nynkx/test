/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FileListViewController.h"
#import "ComparisonViewController.h"
#import "ComparisonContainerViewController.h"
#import "FileItem.h"
#import "FileManageListViewController.h"

@interface FSFileListViewController () <FileDelegate> {
    UIView *sortContainerView;
    //    TbBaseBar *topToolbar;
    UIView *contentView;
    FileBrowser *browser;
    Popover *popover;
    TbBaseBar *fileBrowser;
    UILabel *previousPath;
    UILabel *currentPath;
    NSMutableArray *pathItems;
    UIImageView *nextImage;
    UIImageView *dateimage;
    UIImageView *nameimage;
    UIImageView *sizeimage;
    TbBaseItem *thumbnailItem;
    FileItem *currentFileItem;
    
    TbBaseItem *photoToPDFItem;

    BOOL isShowMorePopover;
    BOOL isShowSortPopover;

    FileSortType sortType;
    FileSortMode sortMode;
    int viewMode;
}

@property (nonatomic, strong) FileItem *previousItem;
@property (nonatomic, strong) TbBaseItem *ipadsortNameItem;
@property (nonatomic, strong) TbBaseItem *ipadsortTimeItem;
@property (nonatomic, strong) TbBaseItem *ipadsortTypeItem;
@property (nonatomic, strong) TbBaseItem *sortNameItem;
@property (nonatomic, strong) UIView *rootView;

@end

@implementation FSFileListViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        [self comminit];
    }
    return self;
}

- (void)comminit{
    sortType = FileSortType_Name;
    sortMode = FileSortMode_Ascending;
    viewMode = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIApplicationWillChangeStatusBarOrientationNotification object:nil];
    
    pathItems = [[NSMutableArray alloc] init];
    
    self.topToolbar = [[UIToolbar alloc] init];
    UIColor *color = ThemeBarColor;
    self.topToolbar.tintColor = color;
    self.topToolbar.barTintColor = color;
    self.topToolbar.clipsToBounds = YES;
    contentView = [[UIView alloc] init];
    
    contentView.backgroundColor = ThemeViewBackgroundColor;
    browser = [[FileBrowser alloc] init];
    browser.sortType = sortType;
    browser.sortMode = sortMode;
    [browser initializeViewWithDelegate:self];
    UIView *browContentView = [browser getContentView];
    browContentView.backgroundColor = ThemeViewBackgroundColor;
    [contentView addSubview:browContentView];
    
    fileBrowser = [[TbBaseBar alloc] init];
    fileBrowser.backgroundColor = ThemeCellBackgroundColor;
    fileBrowser.direction = Orientation_HORIZONTAL;
    fileBrowser.interval = NO;
    fileBrowser.hasDivide = NO;
    [contentView addSubview:fileBrowser.contentView];
    
    [fileBrowser.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(contentView);
        make.height.mas_equalTo(NAVIGATION_BAR_HEIGHT_NORMAL + DIVIDE_VIEW_THICKNES);
    }];
    
    [browContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(browContentView.superview);
        make.top.mas_equalTo(fileBrowser.contentView.mas_bottom);
    }];
    
    fileBrowser.top = NO;
    
    UIButton *titleButton = [Utility createButtonWithTitle:FSLocalizedForKey(@"kDocuments")];
    [titleButton setTitleColor:WhiteThemeTextColor forState:UIControlStateNormal];
    titleButton.titleLabel.font = [UIFont systemFontOfSize:(DEVICE_iPHONE) ? 18.f : 20.f];
    titleButton.userInteractionEnabled = NO;
    [titleButton sizeToFit];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    self.topToolbar.items = @[ flexibleSpace, [[UIBarButtonItem alloc] initWithCustomView:titleButton], flexibleSpace ];
    
    __block FSFileListViewController *docModule = self;
    if (DEVICE_iPHONE) {
        UINavigationController *nai = [browser getNaviController];
        FileManageListViewController *fileList = [nai.viewControllers objectAtIndex:0];
        
        [fileList setViewMode:viewMode];
        thumbnailItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"document_thumb_state_iphone" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] imageSelected:nil imageDisable:nil];
        if (viewMode == 1) {
            [thumbnailItem.button setImage:[UIImage imageNamed:@"document_thumb_state_iphone" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        } else if (viewMode == 0) {
            [thumbnailItem.button setImage:[UIImage imageNamed:@"document_list_state_iphone" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        }
        thumbnailItem.tag = 5;
        TbBaseItem *__weak weakThumbnailItem = thumbnailItem;
        thumbnailItem.onTapClick = ^(TbBaseItem *item) {
            [docModule->browser switchStyle];
            
            self->viewMode = self->viewMode == 1 ? 0 : 1;
            
            if (self->viewMode == 1) {
                [weakThumbnailItem.button setImage:[UIImage imageNamed:@"document_thumb_state_iphone" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            } else if (self->viewMode == 0) {
                [weakThumbnailItem.button setImage:[UIImage imageNamed:@"document_list_state_iphone" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            }
        };
        [fileBrowser addItem:thumbnailItem displayPosition:Position_LT];
        [thumbnailItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(thumbnailItem.contentView.superview).offset(ITEM_MARGIN_LITTLE_NORMAL);
            make.centerY.mas_equalTo(thumbnailItem.contentView.superview);
            make.size.mas_equalTo(thumbnailItem.contentView.fs_size);
        }];
        
        TbBaseItem *sortNameItem = nil;
        if (sortType == FileSortType_Name) {
            if (sortMode == FileSortMode_Ascending) {
                UIImage *imageNormal = ImageNamed(@"document_sortupselect_black");
                UIImage *imageSelected = ImageNamed(@"document_sortupselect_black");
                sortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kName") imageNormal:imageNormal imageSelected:imageSelected imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
            } else {
                UIImage *imageNormal = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                UIImage *imageSelected = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                sortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kName") imageNormal:imageNormal imageSelected:imageSelected imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
            }
        } else if (sortType == FileSortType_Date) {
            if (sortMode == FileSortMode_Ascending) {
                UIImage *imageNormal = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                UIImage *imageSelected = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                sortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kTimeSort") imageNormal:imageNormal imageSelected:imageSelected imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
            } else {
                UIImage *imageNormal = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                UIImage *imageSelected = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                sortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kTimeSort") imageNormal:imageNormal imageSelected:imageSelected imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
            }
        } else if (sortType == FileSortType_Size) {
            if (sortMode == FileSortMode_Ascending) {
                UIImage *imageNormal = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                UIImage *imageSelected = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                sortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kSize") imageNormal:imageNormal imageSelected:imageSelected imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
            } else {
                UIImage *imageNormal = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                UIImage *imageSelected = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
                sortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kSize") imageNormal:imageNormal imageSelected:imageSelected imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
            }
        }
        self.sortNameItem = sortNameItem;
        isShowSortPopover = NO;
        self.sortNameItem.textColor = BlackThemeTextColor;
        self.sortNameItem.tag = 6;
        typeof(self) __weak weakSelf = self;
        self.sortNameItem.onTapClick = ^(TbBaseItem *item) {
            [weakSelf setSortButtonPopover];
        };
        [fileBrowser addItem:self.sortNameItem displayPosition:Position_CENTER];
        [self.sortNameItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.sortNameItem.contentView.superview);
            make.size.mas_equalTo(self.sortNameItem.contentView.fs_size);
        }];
        
    } else {
        UINavigationController *nai = [browser getNaviController];
        FileManageListViewController *fileList = [nai.viewControllers objectAtIndex:0];
        [fileList setViewMode:0];
        
        UIImage *image = ImageNamed(@"document_thumb_state_ipad");
        UIButton *thumbnailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        if (viewMode == 1) {
            [thumbnailButton setImage:image forState:UIControlStateNormal];
        } else if (viewMode == 0) {
            [thumbnailButton setImage:[UIImage imageNamed:@"document_list_state_ipad" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        }
        thumbnailButton.tag = 5;
        [thumbnailButton addTarget:self action:@selector(onTapThumbnailButton:) forControlEvents:UIControlEventTouchUpInside];
        NSMutableArray *items = (self.topToolbar.items ?: @[]).mutableCopy;
        [items addObject:[[UIBarButtonItem alloc] initWithCustomView:thumbnailButton]];
        self.topToolbar.items = items;
        
        UIImage *imageNormal = [UIImage fs_imageWithColor:ThemeCellBackgroundColor size:CGSizeMake(18.f, 18.f)];
        self.ipadsortNameItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kName") imageNormal:imageNormal imageSelected:[UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
        self.ipadsortNameItem.textColor = BlackThemeTextColor;
        self.ipadsortNameItem.textFont = [UIFont systemFontOfSize:15.0f];
        self.ipadsortNameItem.tag = 2;
        self.ipadsortNameItem.selected = YES;
        self.ipadsortNameItem.textColor = [UIColor colorWithRGB:0x179cd8];
        
        self.ipadsortNameItem.onTapClick = ^(TbBaseItem *item) {
            [docModule.ipadsortNameItem.button setImage:[UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            docModule.ipadsortNameItem.selected = !docModule.ipadsortNameItem.selected;
            docModule.ipadsortNameItem.textColor = [UIColor colorWithRGB:0x179cd8];
            
            docModule.ipadsortTimeItem.selected = NO;
            [docModule.ipadsortTimeItem.button setImage:imageNormal forState:UIControlStateNormal];
            docModule.ipadsortTimeItem.textColor = BlackThemeTextColor;
            
            docModule.ipadsortTypeItem.selected = NO;
            [docModule.ipadsortTypeItem.button setImage:imageNormal forState:UIControlStateNormal];
            docModule.ipadsortTypeItem.textColor = BlackThemeTextColor;
            
            [docModule->browser sortFileByType:FileSortType_Name fileSortMode:docModule.ipadsortNameItem.selected ? FileSortMode_Ascending : FileSortMode_Descending];
        };
        
        self.ipadsortTimeItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kTimeSort") imageNormal:imageNormal imageSelected:[UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
        self.ipadsortTimeItem.textColor = BlackThemeTextColor;
        self.ipadsortTimeItem.textFont = [UIFont systemFontOfSize:15.0f];
        self.ipadsortTimeItem.tag = 3;
        self.ipadsortTimeItem.onTapClick = ^(TbBaseItem *item) {
            [docModule.ipadsortTimeItem.button setImage:[UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            docModule.ipadsortTimeItem.selected = !docModule.ipadsortTimeItem.selected;
            docModule.ipadsortTimeItem.textColor = [UIColor colorWithRGB:0x179cd8];
            
            docModule.ipadsortNameItem.selected = NO;
            [docModule.ipadsortNameItem.button setImage:imageNormal forState:UIControlStateNormal];
            docModule.ipadsortNameItem.textColor = BlackThemeTextColor;
            
            docModule.ipadsortTypeItem.selected = NO;
            [docModule.ipadsortTypeItem.button setImage:imageNormal forState:UIControlStateNormal];
            docModule.ipadsortTypeItem.textColor = BlackThemeTextColor;
            [docModule->browser sortFileByType:FileSortType_Date fileSortMode:docModule.ipadsortTimeItem.selected ? FileSortMode_Ascending : FileSortMode_Descending];
        };
        
        self.ipadsortTypeItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kSize") imageNormal:imageNormal imageSelected:[UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] imageDisable:nil background:nil imageTextRelation:RELATION_LEFT];
        self.ipadsortTypeItem.textColor = BlackThemeTextColor;
        self.ipadsortTimeItem.textFont = [UIFont systemFontOfSize:15.0f];
        self.ipadsortTypeItem.tag = 4;
        self.ipadsortTypeItem.onTapClick = ^(TbBaseItem *item) {
            [docModule.ipadsortTypeItem.button setImage:[UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            docModule.ipadsortTypeItem.selected = !docModule.ipadsortTypeItem.selected;
            docModule.ipadsortTypeItem.textColor = [UIColor colorWithRGB:0x179cd8];
            
            docModule.ipadsortNameItem.selected = NO;
            [docModule.ipadsortNameItem.button setImage:imageNormal forState:UIControlStateNormal];
            docModule.ipadsortNameItem.textColor = BlackThemeTextColor;
            
            docModule.ipadsortTimeItem.selected = NO;
            [docModule.ipadsortTimeItem.button setImage:imageNormal forState:UIControlStateNormal];
            docModule.ipadsortTimeItem.textColor = BlackThemeTextColor;
            
            [docModule->browser sortFileByType:FileSortType_Size fileSortMode:docModule.ipadsortTypeItem.selected ? FileSortMode_Ascending : FileSortMode_Descending];
        };
        
        sortContainerView = [[UIView alloc] init];
        sortContainerView.backgroundColor = [UIColor clearColor];
        
        [sortContainerView addSubview:self.ipadsortNameItem.contentView];
        [sortContainerView addSubview:self.ipadsortTimeItem.contentView];
        [sortContainerView addSubview:self.ipadsortTypeItem.contentView];
        
        [self.ipadsortNameItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.ipadsortTimeItem.contentView.mas_left).offset(-2 * ITEM_MARGIN_LITTLE_NORMAL);
            make.centerY.mas_equalTo(self.ipadsortNameItem.contentView.superview);
            make.size.mas_equalTo(self.ipadsortNameItem.contentView.fs_size);
        }];
        
        [self.ipadsortTimeItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.ipadsortTimeItem.contentView.superview);
            make.size.mas_equalTo(self.ipadsortTimeItem.contentView.fs_size);
        }];
        
        [self.ipadsortTypeItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.ipadsortTimeItem.contentView.mas_right).offset(2 * ITEM_MARGIN_LITTLE_NORMAL);
            make.centerY.mas_equalTo(self.ipadsortNameItem.contentView.superview);
            make.size.mas_equalTo(self.ipadsortTypeItem.contentView.fs_size);
        }];
        
        sortContainerView.center = fileBrowser.contentView.center;
        [fileBrowser.contentView addSubview:sortContainerView];
        [sortContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.centerX.mas_equalTo(sortContainerView.superview);
            make.width.mas_equalTo(250);
        }];
    }
    
    UIView *linetwo = [[UIView alloc] init];
    linetwo.backgroundColor = DividingLineColor;
    [fileBrowser.contentView addSubview:linetwo];
    [linetwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(linetwo.superview);
        make.height.mas_equalTo(DIVIDE_VIEW_THICKNES);
    }];
    
    [self pathView];
    
    UIToolbar *topToolbarWrapper = [[UIToolbar alloc] init];
    topToolbarWrapper.tintColor = color;
    topToolbarWrapper.barTintColor = color;
    [self.view addSubview:topToolbarWrapper];
    [self.view addSubview:contentView];
    [topToolbarWrapper addSubview:self.topToolbar];

    [topToolbarWrapper mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(topToolbarWrapper.superview);
    }];

    [self.topToolbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topToolbar.superview.fs_mas_left);
        make.right.mas_equalTo(self.topToolbar.superview.fs_mas_right);
        make.bottom.mas_equalTo(self.topToolbar.superview);
        if (@available(iOS 11.0, *)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(self.mas_topLayoutGuide);
        }
        make.height.mas_equalTo(NAVIGATION_BAR_HEIGHT_NORMAL);
    }];

    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.topToolbar);
        make.top.mas_equalTo(topToolbarWrapper.mas_bottom);
        make.bottom.mas_equalTo(contentView.superview);
    }];
    
    if (@available(iOS 11.0, *)) {
        [topToolbarWrapper layoutIfNeeded];
        [topToolbarWrapper bringSubviewToFront:self.topToolbar];
    }
    
    UIBarButtonItem *leftPadding = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    const CGFloat paddingWidth = (IOS11_OR_LATER) ? 0 : 10;
    leftPadding.width = paddingWidth - [Utility getUIToolbarPaddingX];
    
    NSMutableArray *items = (self.topToolbar.items ?: @[]).mutableCopy;
    [items insertObject:leftPadding atIndex:0];
    self.topToolbar.items = items;
    
    
    UIImage *documentEditNormalImage = [UIImage imageNamed:@"document_edit" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
    
    UIButton *editBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, documentEditNormalImage.size.width, documentEditNormalImage.size.height)];
    [editBtn setImage:documentEditNormalImage forState:UIControlStateNormal];
    UIImage *translucentImage2 = [Utility imageByApplyingAlpha:documentEditNormalImage alpha:0.5];
    [editBtn setImage:translucentImage2 forState:UIControlStateHighlighted];
    [editBtn setImage:translucentImage2 forState:UIControlStateDisabled];

    [editBtn addTarget:self action:@selector(editFile) forControlEvents:UIControlEventTouchUpInside];
    
    if (!DEVICE_iPHONE) {
        UIBarButtonItem *editPadding = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        const CGFloat editPaddingWidth = 5;
        editPadding.width = editPaddingWidth;
        [items addObject:editPadding];
    }
    
    UIBarButtonItem *editBtnItem = [[UIBarButtonItem alloc] initWithCustomView:editBtn];
    [items addObject:editBtnItem];
    self.topToolbar.items = items;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_13_0
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection{
    [super traitCollectionDidChange:previousTraitCollection];
    if (@available(iOS 13.0, *)) {
        if ([self.traitCollection hasDifferentColorAppearanceComparedToTraitCollection:previousTraitCollection]) {
            self.ipadsortNameItem.imageNormal = [UIImage fs_imageWithColor:ThemeCellBackgroundColor size:CGSizeMake(18.f, 18.f)];
            self.ipadsortTimeItem.imageNormal = [UIImage fs_imageWithColor:ThemeCellBackgroundColor size:CGSizeMake(18.f, 18.f)];
            self.ipadsortTypeItem.imageNormal = [UIImage fs_imageWithColor:ThemeCellBackgroundColor size:CGSizeMake(18.f, 18.f)];
        }
    }
}
#endif

-(void)editFile {
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_MultiSelect;
//    selectDestination.expectFileType = @[@"pdf"];
    selectDestination.expectFileType = @[@"*"];
    [selectDestination setFileMultiSelectLimit:2];
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    
    __weak typeof(self) weakself = self;
    
    selectDestination.multiSelectDoneHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder, NSMutableDictionary *passwordDict) {
        [controller dismissViewControllerAnimated:YES completion:^{

            if (destinationFolder.count == 2) {
                ComparisonViewController *comparisonVC = [[ComparisonViewController alloc] init];
                comparisonVC.selectedFileArr = [[NSMutableArray alloc] initWithArray:destinationFolder];
                comparisonVC.selectedFilePwdDict = passwordDict;
                
                comparisonVC.doneHandler = ^(ComparisonViewController *controller, NSMutableDictionary *comparisonOptionResult) {
                    
                    __strong typeof(self) strongself = weakself;
                    
                    ComparisonContainerViewController *pdfContainerVC = [[ComparisonContainerViewController alloc] initWithDictonary:comparisonOptionResult];
                    pdfContainerVC.automaticallyAdjustsScrollViewInsets = NO;
                    [strongself.navigationController pushViewController:pdfContainerVC animated:YES ];
                };
                
                FSNavigationController *comparisonNavController = [[FSNavigationController alloc] initWithRootViewController:comparisonVC];
                comparisonNavController.modalPresentationStyle = UIModalPresentationFullScreen;
                comparisonNavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;

                [[Utility getTopMostViewController] presentViewController:comparisonNavController animated:YES completion:nil];
                
            }
        }];
    };
    
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    if (DEVICE_iPAD) {
        selectDestinationNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        selectDestinationNavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    }
    [[Utility getTopMostViewController] presentViewController:selectDestinationNavController animated:YES completion:nil];
}
-(void)goback{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// setup content view of popover of sort type
- (UIView *)configureSortPopoverView {
    UIView *menu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    menu.backgroundColor = UIColor_DarkMode(menu.backgroundColor, ThemePopViewColor_Dark);
    UIButton *date = [[UIButton alloc] initWithFrame:CGRectMake(10, 15, 150, 20)];
    date.tag = 200;
    [date setTitle:FSLocalizedForKey(@"kTimeSort") forState:UIControlStateNormal];
    [date setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [date setTitleColor:WhiteThemeTextColor forState:UIControlStateHighlighted];
    date.titleLabel.font = [UIFont systemFontOfSize:15];
    date.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [date addTarget:self action:@selector(fileSort:) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:date];

    UIButton *name = [[UIButton alloc] initWithFrame:CGRectMake(10, 65, 150, 20)];
    name.tag = 210;
    [name setTitle:FSLocalizedForKey(@"kName") forState:UIControlStateNormal];
    [name setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [name setTitleColor:WhiteThemeTextColor forState:UIControlStateHighlighted];
    name.titleLabel.font = [UIFont systemFontOfSize:15];
    name.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [name addTarget:self action:@selector(fileSort:) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:name];

    UIButton *size = [[UIButton alloc] initWithFrame:CGRectMake(10, 115, 150, 20)];
    size.tag = 220;
    [size setTitle:FSLocalizedForKey(@"kSize") forState:UIControlStateNormal];
    [size setTitleColor:[UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1] forState:UIControlStateNormal];
    [size setTitleColor:WhiteThemeTextColor forState:UIControlStateHighlighted];
    size.titleLabel.font = [UIFont systemFontOfSize:15];
    size.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [size addTarget:self action:@selector(fileSort:) forControlEvents:UIControlEventTouchUpInside];
    [menu addSubview:size];

    dateimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil]];
    dateimage.frame = CGRectMake(120, 15, 18, 18);
    nameimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil]];
    nameimage.frame = CGRectMake(120, 65, 18, 18);
    sizeimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil]];
    sizeimage.frame = CGRectMake(120, 115, 18, 18);

    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, 50, 130, [Utility realPX:1.0f])];
    line.backgroundColor = ThemePopViewDividingLineColor;
    [menu addSubview:line];
    UIView *linetwo = [[UIView alloc] initWithFrame:CGRectMake(10, 100, 130, [Utility realPX:1.0f])];
    linetwo.backgroundColor = ThemePopViewDividingLineColor;
    [menu addSubview:linetwo];
    [menu addSubview:dateimage];
    [menu addSubview:nameimage];
    [menu addSubview:sizeimage];

    nameimage.hidden = YES;
    sizeimage.hidden = YES;
    dateimage.hidden = YES;

    if (sortMode == FileSortMode_Ascending) {
        if (sortType == FileSortType_Name) {
            nameimage.hidden = NO;
            nameimage.image = [UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            name.selected = YES;
        } else if (sortType == FileSortType_Date) {
            dateimage.hidden = NO;
            dateimage.image = [UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            date.selected = YES;
        } else if (sortType == FileSortType_Size) {
            sizeimage.hidden = NO;
            sizeimage.image = [UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            size.selected = YES;
        }
    } else {
        if (sortType == FileSortType_Name) {
            nameimage.hidden = NO;
            nameimage.image = [UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            name.selected = NO;
        } else if (sortType == FileSortType_Date) {
            dateimage.hidden = NO;
            dateimage.image = [UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            date.selected = NO;
        } else if (sortType == FileSortType_Size) {
            sizeimage.hidden = NO;
            sizeimage.image = [UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            size.selected = NO;
        }
    }
    return menu;
}

// popover sort type of file list
- (void)setSortButtonPopover {
    if (DEVICE_iPHONE) {
        UIView *menu = [self configureSortPopoverView];
        CGRect frame = self.sortNameItem.contentView.frame;
        CGPoint startPoint = CGPointMake(CGRectGetMidX(frame), CGRectGetMaxY(frame));
        startPoint = [self.sortNameItem.contentView.superview convertPoint:startPoint toView:self.view];
        popover = [Popover popover];
        [popover showAtPoint:startPoint popoverPosition:DXPopoverPositionDown withContentView:menu inView:self.view];
        isShowSortPopover = YES;
        popover.didDismissHandler = ^{
            self->isShowSortPopover = NO;
        };
    }
}

- (void)fileSort:(id)sender {
    UIButton *button = (UIButton *) sender;
    button.selected = !button.selected;
    sortMode = button.selected ? FileSortMode_Ascending : FileSortMode_Descending;
    if (button.tag == 200) // sort by time
    {
        dateimage.hidden = NO;
        nameimage.hidden = YES;
        sizeimage.hidden = YES;

        self.sortNameItem.text = FSLocalizedForKey(@"kTimeSort");
        if (button.selected) {
            self.sortNameItem.imageNormal = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            dateimage.image = [UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
        } else {
            self.sortNameItem.imageNormal = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            dateimage.image = [UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
        }

        sortType = FileSortType_Date;
        [self->browser sortFileByType:sortType fileSortMode:sortMode];

    } else if (button.tag == 210) // sort by name
    {
        dateimage.hidden = YES;
        nameimage.hidden = NO;
        sizeimage.hidden = YES;

        self.sortNameItem.text = FSLocalizedForKey(@"kName");

        if (button.selected) {
            self.sortNameItem.imageNormal = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            nameimage.image = [UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
        } else {
            self.sortNameItem.imageNormal = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            nameimage.image = [UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
        }
        sortType = FileSortType_Name;
        [self->browser sortFileByType:sortType fileSortMode:sortMode];

    } else if (button.tag == 220) // sort by size
    {
        dateimage.hidden = YES;
        nameimage.hidden = YES;
        sizeimage.hidden = NO;

        self.sortNameItem.text = FSLocalizedForKey(@"kSize");

        if (button.selected) {
            self.sortNameItem.imageNormal = [UIImage imageNamed:@"document_sortupselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            sizeimage.image = [UIImage imageNamed:@"document_sortupselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
        } else {
            self.sortNameItem.imageNormal = [UIImage imageNamed:@"document_sortselect_black" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
            sizeimage.image = [UIImage imageNamed:@"document_sortselect" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
        }
        sortType = FileSortType_Size;
        [self->browser sortFileByType:sortType fileSortMode:sortMode];
    }
    __block FSFileListViewController *docModule = self;
    [docModule->popover dismiss];
    docModule->isShowMorePopover = NO;
    docModule->isShowSortPopover = NO;
}

// add a view showing current path
- (void)pathView {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(returnPreviousPath:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;

    UITapGestureRecognizer *tapGestureTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(returnPreviousPath:)];
    tapGestureTwo.numberOfTapsRequired = 1;
    tapGestureTwo.numberOfTouchesRequired = 1;
    _rootView = [[UIView alloc] initWithFrame:CGRectMake(DEVICE_iPHONE ? 10 : 15, DEVICE_iPHONE ? 40 : 0, 270, 40)];
    _rootView.backgroundColor = [UIColor clearColor];
    UIImageView *previousImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 13, 18, 18)];
    previousImage.userInteractionEnabled = YES;
    previousImage.image = [UIImage imageNamed:@"document_path_back" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil];
    previousImage.tag = 20;
    [previousImage addGestureRecognizer:tapGestureTwo];

    previousPath = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 100, 25)];
    previousPath.lineBreakMode = NSLineBreakByTruncatingMiddle;
    previousPath.font = [UIFont systemFontOfSize:16];
    previousPath.textColor = [UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1];

    nextImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"document_path_arrow" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil]];
    nextImage.frame = CGRectMake(0, 0, 6, 6);

    currentPath = [[UILabel alloc] initWithFrame:CGRectMake(DEVICE_iPHONE ? 145 : 155, 20, 100, 25)];
    currentPath.lineBreakMode = NSLineBreakByTruncatingMiddle;
    currentPath.font = [UIFont systemFontOfSize:16];
    currentPath.textColor = WhiteThemeTextColor;
    [_rootView addSubview:previousPath];
    [_rootView addSubview:currentPath];
    [previousPath addGestureRecognizer:tapGesture];
    previousPath.tag = 21;
    previousPath.userInteractionEnabled = YES;
    [_rootView addSubview:previousImage];
    [_rootView addSubview:nextImage];
    [contentView addSubview:_rootView];
    _rootView.hidden = YES;
}

- (void)returnPreviousPath:(UIGestureRecognizer *)tapGesture {
    if ([tapGesture.view isKindOfClass:[UIImageView class]]) {
        [pathItems removeAllObjects];
        [self setPathTitle:nil currentTitle:nil isHidden:YES];
        [[browser getNaviController] popToRootViewControllerAnimated:YES];
        [browser changeThumbnailFrame:NO];
        return;
    }

    [pathItems removeLastObject];
    if ([pathItems count] == 1 || [pathItems count] == 0) {
        if ([pathItems count] == 1) {
            FileItem *currentfileItem = [pathItems lastObject];
            [self setPathTitle:nil currentTitle:currentfileItem.fileName isHidden:NO];
        } else {
            [self setPathTitle:nil currentTitle:nil isHidden:YES];
        }
    } else {
        FileItem *currentfileItem = [pathItems lastObject];
        FileItem *previousfileItem = [pathItems objectAtIndex:[pathItems indexOfObject:currentfileItem] - 1];
        [self setPathTitle:previousfileItem.fileName currentTitle:currentfileItem.fileName isHidden:NO];
    }
    [[browser getNaviController] popViewControllerAnimated:YES];
    if (DEVICE_iPHONE) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->browser changeThumbnailFrame:YES];
        });
    }
}

- (void)setPathTitle:(NSString *)previousTitle currentTitle:(NSString *)currentTitle isHidden:(BOOL)hidden {
    previousPath.text = previousTitle == nil ? @"Document" : [previousTitle lastPathComponent];
    currentPath.text = [currentTitle lastPathComponent];
    _rootView.hidden = hidden;
    CGSize textSize = [previousPath.text boundingRectWithSize:CGSizeMake(110, 25) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16]} context:nil].size;
    previousPath.frame = CGRectMake(23, 10, textSize.width, 25);
    nextImage.frame = CGRectMake(CGRectGetMaxX(previousPath.frame) + 8, 20, 6, 6);
    currentPath.frame = CGRectMake(CGRectGetMaxX(nextImage.frame) + 8, 10, 110, 25);
}

- (void)onItemCliked:(FileItem *)item {
    kPreventRepeatClickTime(0.3);
    if (!item.isFolder) {
        [self.delegate didFileSelected:item.path];
        item.isOpen = YES;
        currentFileItem = item;
    } else {
        if ([pathItems count] == 0) {
            [self setPathTitle:nil currentTitle:item.fileName isHidden:NO];

        } else {
            FileItem *fileItem = [pathItems lastObject];
            [self setPathTitle:fileItem.fileName currentTitle:item.fileName isHidden:NO];
        }
        [pathItems addObject:item];
        if (DEVICE_iPHONE) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->browser changeThumbnailFrame:YES];
            });
        }
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = ThemeViewBackgroundColor;
}

- (void)onItemsChecked:(NSArray *)item {
}

- (UIToolbar *)getTopToolBar {
    return self.topToolbar;
}

- (UIView *)getTopToolbar {
    return self.topToolbar;
}

- (UIView *)getContentView {
    return contentView;
}

#pragma mark - Orientation changed notification method
- (void)orientationChanged:(NSNotification *)object {
    if (isShowMorePopover) {
        isShowMorePopover = NO;
        [popover.contentView removeFromSuperview];
        [popover.blackOverlay removeFromSuperview];
        [popover removeFromSuperview];
        if (popover.didDismissHandler) {
            popover.didDismissHandler();
        }
    } else if (isShowSortPopover) {
        isShowSortPopover = NO;
        [popover.contentView removeFromSuperview];
        [popover.blackOverlay removeFromSuperview];
        [popover removeFromSuperview];
        if (popover.didDismissHandler) {
            popover.didDismissHandler();
        }
        [self performSelector:@selector(setSortButtonPopover) withObject:self afterDelay:0.2f];
    }
    [self performSelector:@selector(reframingSortContainerView) withObject:self afterDelay:0.2f];
}

- (void)reframingSortContainerView {
    sortContainerView.center = fileBrowser.contentView.center;
}

#pragma mark IDocEventListener
- (void)onDocSaved:(FSPDFDoc *)document error:(int)error {
    //update folder size dictionary
    if (!currentFileItem)
        return;
    NSString *key = [[currentFileItem.path componentsSeparatedByString:DOCUMENT_PATH] objectAtIndex:1];
    NSMutableDictionary<NSString *, NSNumber *> *folderSizeDictionary = [FileManageListViewController getFolderSizeDictionary];
    while (![(key = [key stringByDeletingLastPathComponent]) isEqualToString:@"/"]) {
        [folderSizeDictionary removeObjectForKey:key];
    }
}

- (void)onTapThumbnailButton:(UIButton *)button {
    [browser switchStyle];
    viewMode = viewMode == 1 ? 0 : 1;
    if (viewMode == 1) {
        [button setImage:[UIImage imageNamed:@"document_thumb_state_ipad" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    } else if (viewMode == 0) {
        [button setImage:[UIImage imageNamed:@"document_list_state_ipad" inBundle:[NSBundle bundleForClass:[FSFileListViewController class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    }
}

@end
