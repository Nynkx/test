/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FileSelectDestinationViewController.h"
#import "FileBrowser.h"
#import "FileItem.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"

@implementation FileViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.fileTypeImage = [[UIImageView alloc] initWithFrame:CGRectMake(DEVICE_iPHONE ? 0 : 10, 0, 40, 40)];
        self.previousImage = [[UIImageView alloc] initWithFrame:CGRectMake(DEVICE_iPHONE ? 0 : 10, 0, 18, 18)];
        self.previousImage.image = [UIImage imageNamed:@"document_path_back" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.previousImage.center = CGPointMake(DEVICE_iPHONE ? 14 : 24, self.bounds.size.height / 2);

        self.backName = [[UILabel alloc] initWithFrame:CGRectMake(DEVICE_iPHONE ? 30 : 40, 0, 200, 40)];
        self.backName.textColor = [UIColor colorWithRed:23.f / 255.f green:156.f / 255.f blue:216.f / 255.f alpha:1];
        self.backName.center = CGPointMake(self.backName.center.x, self.bounds.size.height / 2);
        self.backName.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

        self.folderName = [[UILabel alloc] initWithFrame:CGRectMake(DEVICE_iPHONE ? 50 : 60, 0, 2000, 40)];
        self.folderName.textColor = BlackThemeTextColor;
        self.fileSelectImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - 26, 0, 26, 26)];
        self.fileSelectImage.image = [UIImage imageNamed:@"document_cellfile_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.fileTypeImage.center = CGPointMake(DEVICE_iPHONE ? 25 : 35, self.bounds.size.height / 2);

        self.fileTypeImage.layer.borderWidth = 1.0;
        self.fileTypeImage.layer.borderColor = UIColorHex(D8D8D8).CGColor;

        self.fileSelectImage.center = CGPointMake(self.fileSelectImage.center.x, self.bounds.size.height / 2);
        self.folderName.center = CGPointMake(self.folderName.center.x, self.bounds.size.height / 2);
        self.fileTypeImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        self.folderName.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        self.fileSelectImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
        self.previousImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

        self.separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height - 1, self.contentView.frame.size.width, [Utility realPX:1.0f])];
        self.separatorLine.backgroundColor = DividingLineColor;
        self.separatorLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;

        [self.contentView addSubview:self.fileTypeImage];
        [self.contentView addSubview:self.folderName];
        [self.contentView addSubview:self.fileSelectImage];
        [self.contentView addSubview:self.previousImage];
        [self.contentView addSubview:self.backName];
        [self.contentView addSubview:self.separatorLine];

        self.fileSelectImage.hidden = YES;
        self.contentView.autoresizesSubviews = YES;
    }
    return self;
}

@end

@interface FileSelectDestinationViewController () {
    UILabel *lblInstructionMsg;
    dispatch_queue_t _drawThumbnailQueue;
}
@property (nonatomic, strong) NSMutableArray *selectedArray;
@property (nonatomic, strong) NSString *currentDirectoryPath;

@property (nonatomic, assign) int MultiSelectLimit;
@property (nonatomic, strong) NSMutableArray *multiSelectedFilesArr;
@property (nonatomic, strong) NSMutableDictionary *multiSelectedIndexPathsDict;
@property (nonatomic, strong) NSMutableArray *multiSelectedIndexPathsArr;

@property(nonatomic,strong)NSIndexPath *lastPath;

- (void)loadFilesWithPath:(NSString *)filePath;
@end

@implementation FileSelectDestinationViewController

#pragma mark - Initialization
- (instancetype)init {
    if (self = [super init]) {
        self.fileItemsArray = [NSMutableArray array];
        self.selectedArray = [NSMutableArray array];
        self.multiSelectedFilesArr = [NSMutableArray array];
        self.multiSelectedIndexPathsDict = [NSMutableDictionary dictionary];
        self.multiSelectedIndexPathsArr = [NSMutableArray array];
        self.filePasswordDict = @{}.mutableCopy;
        dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_USER_INITIATED, 0);
        _drawThumbnailQueue = dispatch_queue_create("com.foxit.rdk.file.select.thumbnail", attr);
    }
    return self;
}

#pragma mark - View lifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationHandleOpenURL:) name:NOTIFICATION_NAME_APP_HANDLE_OPEN_URL object:nil];

    self.tableViewFolders = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableViewFolders.delegate = self;
    self.tableViewFolders.dataSource = self;
    self.tableViewFolders.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableViewFolders.cellLayoutMarginsFollowReadableWidth = false;
    [self.view addSubview:self.tableViewFolders];
    
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        self.tableViewFolders.allowsMultipleSelectionDuringEditing = YES;
        [self.tableViewFolders setEditing:YES animated:YES];
    }
    
    if (self.fileOperatingMode == FileListMode_Select || self.fileOperatingMode == FileListMode_SaveTo)
        [self configureInstructionMessage];
    [self initNavigationBar];
    [self refreshInterface];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.fileItemsArray.count == 0)
        [lblInstructionMsg setHidden:NO];
    else if (!self.isRootFileDirectory && self.fileItemsArray.count == 1)
        [lblInstructionMsg setHidden:NO];
    else
        [lblInstructionMsg setHidden:YES];
    
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        NSString *title = self.multiSelectedFilesArr.count == 0 ? FSLocalizedForKey(@"kDocuments") : [NSString stringWithFormat:@"%@ (%d)",FSLocalizedForKey(@"kDocuments"),(int)self.multiSelectedFilesArr.count];
        self.title = title ;
    }
    
    [self.multiSelectedIndexPathsDict enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSMutableArray *indexPathArr, BOOL * _Nonnull stop) {
        NSString *currentDirectory = [self getDirectoryPath:self.currentDirectoryPath];
        if ([currentDirectory isEqualToString: key]) {
            [indexPathArr enumerateObjectsUsingBlock:^(NSIndexPath *indexPath, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.tableViewFolders selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            }];
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_NAME_APP_HANDLE_OPEN_URL object:nil];
}

#pragma mark - UI configuration
- (void)configureInstructionMessage {
    NSString *msg = @"";
    switch (self.fileOperatingMode) {
    case FileListMode_Select:
        msg = @"kOKHelp";
        break;

    case FileListMode_SaveTo:
        msg = @"kOKHelp";
        break;

    default:
        break;
    }

    CGSize titleSize = [Utility getTextSize:FSLocalizedForKey(msg) fontSize:20.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];

    if (lblInstructionMsg == nil) {
        lblInstructionMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleSize.width + 3, titleSize.height)];
        lblInstructionMsg.center = self.view.center;
        lblInstructionMsg.textAlignment = NSTextAlignmentCenter;
        lblInstructionMsg.numberOfLines = 0;
        lblInstructionMsg.text = FSLocalizedForKey(msg);
        lblInstructionMsg.font = [UIFont systemFontOfSize:20.0f];
        lblInstructionMsg.textColor = BlackThemeTextColor;
        [self.view addSubview:lblInstructionMsg];
    } else {
        lblInstructionMsg.frame = CGRectMake(0, 0, titleSize.width + 3, titleSize.height);
        lblInstructionMsg.center = self.view.center;
    }
    [lblInstructionMsg mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX).offset(0);
        make.centerY.mas_equalTo(self.view.mas_centerY).offset(0);
        make.width.mas_equalTo(titleSize.width);
        make.height.mas_equalTo(titleSize.height);
    }];
}

- (void)refreshInterface {
    
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.tableViewFolders.separatorColor = DividingLineColor;
    self.tableViewFolders.backgroundColor = ThemeViewBackgroundColor;
    self.tableViewFolders.backgroundView = [[UIView alloc] init];

    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [self.tableViewFolders setTableFooterView:view];
}

#pragma mark - Configure NavigationBar
- (void)initNavigationBar {
    
    self.navigationController.navigationBar.barTintColor = ThemeNavBarColor;
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLeft.frame = CGRectMake(0.0, 0.0, 65.0, 32);
    buttonLeft.titleLabel.font = [UIFont systemFontOfSize:DEVICE_iPHONE ? 15.0f : 18.0f];
    [buttonLeft setTitleColor:WhiteThemeTextColor forState:UIControlStateHighlighted];
    [buttonLeft setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    [buttonLeft addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem addLeftBarButtonItem:buttonLeft ? [[UIBarButtonItem alloc] initWithCustomView:buttonLeft] : nil];
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        UIImage *image = [UIImage fs_imageWithLightName:@"document_compare" darkName:@"document_compare_dark"];
        UIImage *documentEditNormalImage = image;
        #if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_12_0
        if (@available(iOS 12.0, *)) {
            if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
                documentEditNormalImage = image.fs_darkImage;
            }
        }
        #endif
        UIButton *compareBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [compareBtn setImage:documentEditNormalImage forState:UIControlStateNormal];
        UIImage *translucentImage2 = [Utility imageByApplyingAlpha:documentEditNormalImage alpha:0.5];
        [compareBtn setImage:translucentImage2 forState:UIControlStateHighlighted];
        [compareBtn setImage:translucentImage2 forState:UIControlStateDisabled];
        [compareBtn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        
        self.buttonDone = [[UIBarButtonItem alloc] initWithCustomView:compareBtn];
        [self.buttonDone setTintColor:WhiteThemeTextColor];
        self.multiSelectedFilesArr.count == self.MultiSelectLimit ? [self.buttonDone setEnabled:YES] : [self.buttonDone setEnabled:NO];
        
        [self.navigationItem setRightBarButtonItem:self.buttonDone];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        
        NSString *title = self.multiSelectedFilesArr.count == 0 ? FSLocalizedForKey(@"kDocuments") : [NSString stringWithFormat:@"%@ (%d)",FSLocalizedForKey(@"kDocuments"),(int)self.multiSelectedFilesArr.count];
        self.title = title ;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : WhiteThemeNavBarTextColor};
        return ;
    }
    
    self.buttonDone = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self.buttonDone setTintColor:WhiteThemeTextColor];
    [self.buttonDone setEnabled:YES];
    
    [self.navigationItem setRightBarButtonItem:self.buttonDone];
    
    if (self.fileOperatingMode == FileListMode_Select) {
        [self.buttonDone setTitle:FSLocalizedForKey(@"kSave")];
    } else if (self.fileOperatingMode == FileListMode_Import) {
        [self.buttonDone setTitle:FSLocalizedForKey(@"kSelectFile")];
        [self.buttonDone setEnabled:NO];
    } else if (self.fileOperatingMode == FileListMode_SaveTo) {
        [self.buttonDone setTitle:FSLocalizedForKey(@"kSave")];
        self.title = FSLocalizedForKey(@"kSelectToFolder");
    }

    NSDictionary *fontAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:DEVICE_iPHONE ? 15.0f : 18.0f]};
    [self.buttonDone setTitleTextAttributes:fontAttributes forState:UIControlStateNormal];

    NSDictionary *titleFontAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:DEVICE_iPHONE ? 17.0 : 20.0], NSFontAttributeName, WhiteThemeNavBarTextColor, NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = titleFontAttributes;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_13_0
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection{
    [super traitCollectionDidChange:previousTraitCollection];
    if (@available(iOS 13.0, *)) {
        if ([self.traitCollection hasDifferentColorAppearanceComparedToTraitCollection:previousTraitCollection]) {
            if (self.fileOperatingMode == FileListMode_MultiSelect) {
                UIImage *image = [UIImage fs_imageWithLightName:@"document_compare" darkName:@"document_compare_dark"];
                UIImage *documentEditNormalImage = image;
                #if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_12_0
                if (@available(iOS 12.0, *)) {
                    if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
                        documentEditNormalImage = image.fs_darkImage;
                    }
                }
                #endif
                UIButton *compareBtn = self.buttonDone.customView;
                [compareBtn setImage:documentEditNormalImage forState:UIControlStateNormal];
                UIImage *translucentImage2 = [Utility imageByApplyingAlpha:documentEditNormalImage alpha:0.5];
                [compareBtn setImage:translucentImage2 forState:UIControlStateHighlighted];
                [compareBtn setImage:translucentImage2 forState:UIControlStateDisabled];
            }
        }
    }
}
#endif

#pragma mark <UINavigationControllerDelegate>

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    navigationController.navigationBar.tag = 1;
    if (viewController == self) {
        [self viewWillAppear:NO];
    }
}

- (void)setNavigationTitle:(NSString *)title {
    self.title = title;
}

#pragma mark - Action methods
-(void)showTips:(NSString *)message {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.mode = MBProgressHUDModeText;
    progressHUD.detailsLabelText =  message;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}
- (void)doneAction {
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        if (self.multiSelectedFilesArr.count != self.MultiSelectLimit) {
            //alert
            [self showTips:[NSString stringWithFormat:FSLocalizedForKey(@"kMultiSelectLimitFaild"), self.MultiSelectLimit]];
        }
        else{
            for (int i =0 ; i < self.multiSelectedFilesArr.count; i++) {
                NSString *filePath = [self.multiSelectedFilesArr objectAtIndex:i];
                NSString *pathExtension = [filePath pathExtension];
                if( ! ([pathExtension isEqualToString:@"pdf"] || pathExtension.length == 0) ){
                    [self showTips:[NSString stringWithFormat:FSLocalizedForKey(@"kCompareNotSupportFileFormat"), [filePath lastPathComponent]]];
                    return ;
                }
            }
            
            void (^doneOperationHandler)(NSMutableDictionary *) = ^(NSMutableDictionary *passwordDict){
                for (int i =0 ; i < self.multiSelectedFilesArr.count; i++) {
                    NSString *filePath = [self.multiSelectedFilesArr objectAtIndex:i];
                    
                    
                    FSPDFDoc *doc = [[FSPDFDoc alloc] initWithPath:filePath];
                    NSString *password = [self.filePasswordDict objectForKey:filePath];
                    [doc load:password];
                    
                    if ([doc isXFA] || [doc isWrapper] || [Utility checkFileisRMS:filePath password:@""]) {
                        [self showTips:[NSString stringWithFormat:FSLocalizedForKey(@"kCompareNotSupportFileFormat"), [filePath lastPathComponent]]];
                        return ;
                    };
                }
                
                [self.buttonDone setEnabled:NO];
                self.multiSelectDoneHandler(self,  [NSArray arrayWithArray:self.multiSelectedFilesArr],passwordDict);
                
                [self.multiSelectedFilesArr removeAllObjects];
                self.multiSelectedFilesArr = nil;
            };
            
            [Utility tryLoadDoc:self.multiSelectedFilesArr[0] withPassword:@"" success:^(NSString * _Nonnull password) {
                [self.filePasswordDict setObject:password forKey:self.multiSelectedFilesArr[0]];

                [Utility tryLoadDoc:self.multiSelectedFilesArr[1] withPassword:@"" success:^(NSString * _Nonnull password) {
                    [self.filePasswordDict setObject:password forKey:self.multiSelectedFilesArr[1]];
                    
                    NSMutableDictionary *filePasswordDic = [[NSMutableDictionary alloc] initWithDictionary:self.filePasswordDict];
                    
                    doneOperationHandler(filePasswordDic);
                    
                } error:^(NSString * _Nonnull description) {
                    [self showTips:[NSString stringWithFormat:@"%@ %@",description,[self.multiSelectedFilesArr[1] lastPathComponent]]];
                } abort:nil];
                
            } error:^(NSString * _Nonnull description) {
                [self showTips:[NSString stringWithFormat:@"%@ %@",description,[self.multiSelectedFilesArr[0] lastPathComponent]]];
            } abort:nil];
            
        }
        
        return ;
    }
    
    if (self.isNeedGetPassword) {
        NSString *filePath = self.currentDirectoryPath;
        NSString *pathExtension = [filePath pathExtension];
        if( ! ([pathExtension isEqualToString:@"pdf"] || pathExtension.length == 0) ){
            [self showTips:[NSString stringWithFormat:FSLocalizedForKey(@"kCompareNotSupportFileFormat"), [filePath lastPathComponent]]];
            return ;
        }
        
        [Utility tryLoadDoc:filePath withPassword:@"" success:^(NSString * _Nonnull password) {
            [self.filePasswordDict setObject:password forKey:filePath];
            
            FSPDFDoc *doc = [[FSPDFDoc alloc] initWithPath:filePath];
            [doc load:password];
            if ([doc isXFA] || [doc isWrapper] || [Utility checkFileisRMS:filePath password:@""]) {
                [self showTips:[NSString stringWithFormat:FSLocalizedForKey(@"kCompareNotSupportFileFormat"), [filePath lastPathComponent]]];
                return ;
            }
            
            [self.buttonDone setEnabled:NO];
            self.operatingHandler(self, [NSArray arrayWithObject:self.currentDirectoryPath]);
        } error:^(NSString * _Nonnull description) {
            [self showTips:description];
        } abort:nil];
    }else{
        [self.buttonDone setEnabled:NO];
        self.operatingHandler(self, [NSArray arrayWithObject:self.currentDirectoryPath]);
    }
}

- (void)cancelAction {
    if (self.cancelHandler) {
        self.cancelHandler(self);
    }
    [[self.navigationController.viewControllers objectAtIndex:0] dismissViewControllerAnimated:YES
                                                                                    completion:^{

                                                                                    }];
}

#pragma mark - Other Methods
//Avi - new method addded to add the cloudname in copy page to fix the issue 0071868
- (void)_loadFilesWithPath:(NSString *)filePath {
    __block NSMutableArray *folderList = [[NSMutableArray alloc] init];
    [FileBrowser getFileListWithFolderPath:filePath
                         withSearchKeyword:nil
                                  userData:nil
                               withHandler:^(NSMutableArray *fileList) {
                                   NSArray *favFileListArr = [fileList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                                       FileItem *fileObj1 = (FileItem *) obj1;
                                       FileItem *fileObj2 = (FileItem *) obj2;
                                       if (fileObj1.isFolder != fileObj2.isFolder) {
                                           return fileObj1.isFolder ? NSOrderedAscending : NSOrderedDescending;
                                       } else {
                                           NSComparisonResult compareResult = NSOrderedSame;
                                           return compareResult;
                                       }
                                   }];

                                   [folderList addObjectsFromArray:[NSArray arrayWithArray:favFileListArr]];
                               }];

    for (FileItem *fileitem in folderList) {
        if (fileitem.isFolder) {
            BOOL willAdd = YES;
            for (NSString *expFolder in self.exceptFolderList) {
                if ([[expFolder lastPathComponent] isEqualToString:[fileitem.path lastPathComponent]]) {
                    willAdd = NO;
                    break;
                }
            }
            if (willAdd) {
                FileItem *item = [[FileItem alloc] init];
                item.filekey = fileitem.path;
                item.title = fileitem.fileName;
                item.isFolder = YES;
                [self.fileItemsArray addObject:item];
            }
        }
    }
}

- (void)loadFilesWithPath:(NSString *)filePath {
    if (!self.isRootFileDirectory) {
        [self.fileItemsArray addObject:FSLocalizedForKey(@"kBack")];
    }

    //Avi - added
    [self _loadFilesWithPath:filePath];

    if (self.fileOperatingMode == FileListMode_Select || self.fileOperatingMode == FileListMode_Import || self.fileOperatingMode == FileListMode_MultiSelect) {
        NSArray *files = [Utility searchFilesWithFolder:filePath recursive:NO];

        for (NSString *file in files) {
            NSString *fileDisplayName = [file lastPathComponent];
            BOOL willAdd = NO;
            NSString *extName = [fileDisplayName pathExtension];
            for (NSString *ext in _expectFileType) {
                if ([Utility isGivenExtension:extName type:ext]) {
                    willAdd = YES;
                    break;
                }
            }
            if (willAdd) {
                FileItem *item = [[FileItem alloc] init];
                item.filekey = file;
                item.isFolder = NO;
                item.title = fileDisplayName;
                [self.fileItemsArray addObject:item];
            }
        }
    }

    self.currentDirectoryPath = filePath;
}

- (void)setFileMultiSelectLimit:(int)limit{
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        self.MultiSelectLimit = limit;
    }else{
        self.MultiSelectLimit = -1;
    }
}
#pragma mark - Memory Management Methods
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fileItemsArray count];
}

- (FileViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"cellIdentifier";
    FileViewCell *folderCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (folderCell == nil) {
        folderCell = [[FileViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    if (!self.isRootFileDirectory && indexPath.row == 0) {
        folderCell.folderName.hidden = YES;
        folderCell.fileTypeImage.hidden = YES;
        folderCell.fileSelectImage.hidden = YES;
        folderCell.accessoryType = UITableViewCellAccessoryNone;
        folderCell.previousImage.hidden = NO;
        folderCell.backName.hidden = NO;
        folderCell.backName.text = [self.fileItemsArray objectAtIndex:0];
        folderCell.separatorLine.frame = CGRectMake(10, folderCell.contentView.frame.size.height - 1, 2000, [Utility realPX:1.0f]);
    } else {
        FileItem *fileItem = [self.fileItemsArray objectAtIndex:indexPath.row];
        folderCell.folderName.text = fileItem.title;
        folderCell.separatorLine.frame = CGRectMake(0, folderCell.contentView.frame.size.height - 1, 2000, [Utility realPX:1.0f]);
        if (fileItem.isFolder) {
            folderCell.fileSelectImage.hidden = YES;
            folderCell.fileTypeImage.image = [UIImage imageNamed:@"list_newfolder" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
            folderCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            if (self.fileOperatingMode != FileListMode_MultiSelect) {
                folderCell.accessoryType = UITableViewCellAccessoryNone;
                if (fileItem.selected) {
                    folderCell.fileSelectImage.hidden = NO;
                    folderCell.accessoryType = UITableViewCellAccessoryCheckmark;
                } else {
                    folderCell.fileSelectImage.hidden = YES;
                    folderCell.accessoryType = UITableViewCellAccessoryNone;
                }
            }else{
                if (fileItem.selected) {
                    folderCell.fileSelectImage.hidden = NO;
                } else {
                    folderCell.fileSelectImage.hidden = YES;
                }
            }
            
            if (self.fileOperatingMode != FileListMode_MultiSelect) {
                NSInteger row = [indexPath row];
                NSInteger oldRow = [_lastPath row];
                if (row == oldRow && _lastPath != nil) {
                    folderCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }else{
                    folderCell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            if ([Utility isPDFPath:fileItem.filekey]) {
                folderCell.fileTypeImage.image = nil;
                fileItem.path = fileItem.filekey;
                [fileItem getThumbnailForPageIndex:0 dispatchQueue:_drawThumbnailQueue WithHandler:^(UIImage *imageThumbnail, int pageIndex, NSString *pdfPath) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                        if ([[fileItem.filekey lastPathComponent] isEqualToString: folderCell.folderName.text]) {
                            folderCell.fileTypeImage.image = imageThumbnail;
                        }
                    });
                }];
                
            } else {
                folderCell.fileTypeImage.image = nil;
                dispatch_async(_drawThumbnailQueue, ^{
                    UIImage *image = [UIImage imageNamed:[Utility getIconName:fileItem.filekey] inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        folderCell.fileTypeImage.image = image;
                    });
                });
            }
        }
        
        folderCell.folderName.hidden = NO;
        folderCell.fileTypeImage.hidden = NO;
        folderCell.previousImage.hidden = YES;
        folderCell.backName.hidden = YES;
    }
    return folderCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isRootFileDirectory && indexPath.row == 0) {
        [self.navigationController popViewControllerAnimated:YES];
        
        if (self.fileOperatingMode == FileListMode_MultiSelect) {
            int viewControllersCount = (int)self.navigationController.viewControllers.count;
            FileSelectDestinationViewController *tmpController = self.navigationController.viewControllers[viewControllersCount-1];
            if (tmpController && [tmpController isKindOfClass:[self class]]) {
                tmpController.multiSelectedFilesArr = self.multiSelectedFilesArr;
                tmpController.multiSelectedIndexPathsDict = self.multiSelectedIndexPathsDict;
            }
        }
        return;
    }
    for (FileItem *fileItem in self.fileItemsArray) {
        if ([fileItem isKindOfClass:[FileItem class]]) {
            fileItem.selected = NO;
        }
    }
    FileItem *fileItem = [self.fileItemsArray objectAtIndex:indexPath.row];
    
    if (!fileItem.isFolder) {
        self.currentDirectoryPath = fileItem.filekey;
        
        if (self.fileOperatingMode == FileListMode_MultiSelect) {
            if (![self.multiSelectedFilesArr containsObject:fileItem.filekey]) {
                [self.multiSelectedFilesArr addObject:fileItem.filekey];
                
                self.multiSelectedFilesArr.count == self.MultiSelectLimit ? [self.buttonDone setEnabled:YES] : [self.buttonDone setEnabled:NO];
                
                NSString *title = self.multiSelectedFilesArr.count == 0 ? FSLocalizedForKey(@"kDocuments") : [NSString stringWithFormat:@"%@ (%d)",FSLocalizedForKey(@"kDocuments"),(int)self.multiSelectedFilesArr.count];
                self.title = title ;
            }
            
            [self.multiSelectedIndexPathsArr addObject:indexPath];
            
            NSString *directoryName = [self getDirectoryPath:fileItem.filekey];
            [self.multiSelectedIndexPathsDict setObject:self.multiSelectedIndexPathsArr forKey:directoryName];
        }
        
        
        fileItem.selected = YES;

//        [tableView reloadData];
        if (self.fileOperatingMode == FileListMode_Import) {
            if ([self.expectFileType containsObject:fileItem.filekey.pathExtension.lowercaseString]) {
                [self.buttonDone setEnabled:YES];
            } else {
                [self.buttonDone setEnabled:NO];
            }
            if ([[self.expectFileType objectAtIndex:0] isEqualToString:@"*"]) {
                [self.buttonDone setEnabled:YES];
            }
        }

        if (self.fileOperatingMode != FileListMode_MultiSelect) {
            int newRow = (int)[indexPath row];
            int oldRow = (_lastPath != nil) ? (int)[_lastPath row] : -1;
            
            if (newRow != oldRow) {
                UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:_lastPath];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
                
                _lastPath = indexPath;
            }
            
            if (self.fileOperatingMode != FileListMode_MultiSelect) {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
        }
        
    }else{
        [self.selectedArray removeAllObjects];
        for (FileItem *fileItem in self.fileItemsArray) {
            if ([fileItem isKindOfClass:[FileItem class]]) {
                fileItem.selected = NO;
            }
//            [tableView reloadData];
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
        selectDestination.fileOperatingMode = self.fileOperatingMode;
        selectDestination.operatingHandler = self.operatingHandler;
        selectDestination.cancelHandler = self.cancelHandler;
        selectDestination.isRootFileDirectory = NO;
        selectDestination.exceptFolderList = self.exceptFolderList;
        selectDestination.expectFileType = self.expectFileType;
        selectDestination.operatingFiles = self.operatingFiles;
        if (self.fileOperatingMode == FileListMode_MultiSelect) {
            selectDestination.multiSelectDoneHandler = self.multiSelectDoneHandler;
            [selectDestination setMultiSelectLimit:self.MultiSelectLimit];
            selectDestination.multiSelectedFilesArr = self.multiSelectedFilesArr;
            selectDestination.multiSelectedIndexPathsDict = self.multiSelectedIndexPathsDict;
            selectDestination.filePasswordDict = self.filePasswordDict;
        }
        [selectDestination loadFilesWithPath:fileItem.filekey];
        [self.navigationController pushViewController:selectDestination animated:YES];
    }
    
}

-(NSString *)getDirectoryPath:(NSString *)filePath {
    BOOL isDirectory = NO;
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    if (!isDirectory) {
        filePath = [filePath stringByDeletingLastPathComponent];
    }
    
    NSRange range = [filePath rangeOfString:@"Documents"];
    NSString *directoryName = [filePath substringFromIndex:range.location];

    return directoryName;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        FileItem *fileItem = nil;
        if (![[self.fileItemsArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            fileItem = [self.fileItemsArray objectAtIndex:indexPath.row];
        }
        
        if (fileItem && !fileItem.isFolder) {
            [self.multiSelectedFilesArr removeObject:fileItem.filekey];
            [self.multiSelectedIndexPathsArr removeObject:indexPath];
            
            self.multiSelectedFilesArr.count == self.MultiSelectLimit ? [self.buttonDone setEnabled:YES] : [self.buttonDone setEnabled:NO];
            
            NSString *title = self.multiSelectedFilesArr.count == 0 ? FSLocalizedForKey(@"kDocuments") : [NSString stringWithFormat:@"%@ (%d)",FSLocalizedForKey(@"kDocuments"),(int)self.multiSelectedFilesArr.count];
            self.title = title ;
        }
    }
}

- (BOOL)tableView:(UITableView*)tableView canEditRowAtIndexPath:(NSIndexPath*)indexPath {
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        if (!self.isRootFileDirectory && indexPath.row == 0) {
            return NO;
        } else {
            FileItem *fileItem = [self.fileItemsArray objectAtIndex:indexPath.row];
            if (fileItem.isFolder){
                return NO;
            }
        }
        return YES;
    }

    return YES;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.fileOperatingMode == FileListMode_MultiSelect) {
        FileItem *fileItem = nil;
        if (![[self.fileItemsArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            fileItem = [self.fileItemsArray objectAtIndex:indexPath.row];
        }
        FileViewCell *folderCell = [tableView cellForRowAtIndexPath:indexPath];
        if (fileItem && !fileItem.isFolder && self.multiSelectedFilesArr.count + 1 > self.MultiSelectLimit && ![self.multiSelectedFilesArr containsObject:fileItem.filekey]) {
            folderCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return nil;
        }
        
        folderCell.selectionStyle = UITableViewCellSelectionStyleDefault;
        return indexPath;
    }
    
    return indexPath;
}

#pragma mark - Orientation Method
- (void)orientationChanged:(NSNotification *)object {
    [self configureInstructionMessage];
}

#pragma mark - NSNotification handler Method
- (void)applicationHandleOpenURL:(NSNotification *)object {
    [self cancelAction];
}
@end
