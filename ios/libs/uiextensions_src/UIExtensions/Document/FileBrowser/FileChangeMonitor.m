/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FileChangeMonitor.h"
#import "Const.h"

#include <sys/stat.h> 
#include <sys/types.h> 
#include <sys/event.h> 
#include <sys/time.h> 
#include <fcntl.h>
//File change monitor Notification
static NSString *FILE_CHANGED = @"FileChangedNotification";
@interface FileChangeMonitor (Private)

- (void)kqueueFired:(CFFileDescriptorRef)kqRef;
- (void)getDirEvents:(NSString *)directoryPath;

@end

@implementation FileChangeMonitor

#pragma mark - public methods

+ (FileChangeMonitor *)sharedFileChangeMonitor
{
    static FileChangeMonitor *_sharedMonitor;
    if (_sharedMonitor == nil)
    {
        _sharedMonitor = [[FileChangeMonitor alloc] init];
    }
    return _sharedMonitor;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _monitorPaths = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_monitorPaths release];
    [super dealloc];
}

- (void)startMonitorDirectory:(NSString *)directoryPath
{
    [self getDirEvents:directoryPath];
    NSFileManager *fileManager = [[[NSFileManager alloc] init] autorelease];
    NSDirectoryEnumerator *dictEnum = [fileManager enumeratorAtPath:directoryPath];
    BOOL isDirectory = NO;
    for (NSString *path in dictEnum)
    {
        path = [directoryPath stringByAppendingPathComponent:path];
        if ([fileManager fileExistsAtPath:path isDirectory:&isDirectory] && isDirectory)
        {
            [self getDirEvents:path];
        }
    }
}

- (void)stopMonitorDirectory
{
	for(NSValue *key in _monitorPaths)
    {
        CFFileDescriptorDisableCallBacks([key pointerValue], kCFFileDescriptorReadCallBack);
		CFFileDescriptorInvalidate([key pointerValue]);
		close(CFFileDescriptorGetNativeDescriptor([key pointerValue]));
        CFRelease([key pointerValue]);
	}
    [_monitorPaths removeAllObjects];
}

#pragma mark - private methods

- (void)kqueueFired:(CFFileDescriptorRef)kqRef
{
    int             kq;
    struct kevent   event;
    struct timespec timeout = { 0, 0 };
    int             eventCount;
    
    kq = CFFileDescriptorGetNativeDescriptor(kqRef);
    if (kq < 0 ) return;
    
    eventCount = kevent(kq, NULL, 0, &event, 1, &timeout);
    if ((eventCount < 0) || (eventCount >= 2))return;
    
    if (eventCount == 1) 
    {
        NSValue *key = [NSValue valueWithPointer:kqRef];		
		NSString *directoryPath = [_monitorPaths objectForKey:key];
		[[NSNotificationCenter defaultCenter] postNotificationName:FILE_CHANGED object:directoryPath];
        //if this path not exist anymore, stop monitor it
		NSFileManager *fileManager = [[[NSFileManager alloc] init] autorelease];
		if(![fileManager fileExistsAtPath:directoryPath]) 
        {	
			CFFileDescriptorInvalidate([key pointerValue]);
			close(CFFileDescriptorGetNativeDescriptor([key pointerValue]));
			[_monitorPaths removeObjectForKey:key];
			return;
        }
        //if this path exist, need to check does it has new folder
        NSArray *arraySubs = [fileManager contentsOfDirectoryAtPath:directoryPath error:nil];
        BOOL isFolder= NO;
        for(NSString *subPath in arraySubs)
        {
            subPath= [directoryPath stringByAppendingPathComponent:subPath];
            if ([fileManager fileExistsAtPath:subPath isDirectory:&isFolder] && isFolder)
            {
                if (![_monitorPaths.allValues containsObject:subPath])
                {
                    [self getDirEvents:subPath];
                }
            }
        }
        //continue to monitor
        CFFileDescriptorEnableCallBacks(kqRef, kCFFileDescriptorReadCallBack);
    }
}
    
static void KQCallback(CFFileDescriptorRef kqRef, CFOptionFlags callBackTypes, void *info)
{
    FileChangeMonitor *obj = (FileChangeMonitor *)info;
    if ([obj isKindOfClass:[FileChangeMonitor class]] && callBackTypes == kCFFileDescriptorReadCallBack) {
        [obj kqueueFired:kqRef];
    }
}

- (void)getDirEvents:(NSString *)directoryPath
{
    int                     dirFD;
    int                     kq;
    int                     retVal;
    struct kevent           eventToAdd;
    CFFileDescriptorContext context = { 0, self, NULL, NULL, NULL };
    CFFileDescriptorRef     kqRef;
    CFRunLoopSourceRef      rls;
    
    if (directoryPath.length <= 0) return;
    
    dirFD = open([directoryPath fileSystemRepresentation], O_EVTONLY);
    
    kq = kqueue();
    if (kq <0) return;
    
    eventToAdd.ident  = dirFD;
    eventToAdd.filter = EVFILT_VNODE;
    eventToAdd.flags  = EV_ADD | EV_CLEAR;
    eventToAdd.fflags = NOTE_WRITE;
    eventToAdd.data   = 0;
    eventToAdd.udata  = NULL;
    
    retVal = kevent(kq, &eventToAdd, 1, NULL, 0, NULL);
    if (retVal != 0) return;
    
    kqRef = CFFileDescriptorCreate(NULL, kq, true, KQCallback, &context);
    if (kqRef == NULL) return;

    //add this record for directory path into monitor
    NSValue *val = [NSValue valueWithPointer:kqRef];	
    [_monitorPaths setObject:directoryPath forKey:val];
    
    rls = CFFileDescriptorCreateRunLoopSource(NULL, kqRef, 0);
    if (rls == NULL) return;
    
    CFRunLoopAddSource(CFRunLoopGetCurrent(), rls, kCFRunLoopDefaultMode);
    
    CFRelease(rls);
    
    CFFileDescriptorEnableCallBacks(kqRef, kCFFileDescriptorReadCallBack);
}

@end
