/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/
#import <Foundation/Foundation.h>

//An object to monitor a directory.
//It monitors the directory and subdirectory inside it. when file/directory changes, it sends out FileChangedNotification with the current directory path
//Refer to:
//http://stackoverflow.com/questions/3181821/notification-of-changes-to-the-iphones-documents-directory
//http://www.themusingsofalostprogrammer.com/2011/06/file-monitor-ios-os-x.html
@interface FileChangeMonitor : NSObject
{
    //a dictionary for the current monitored directory paths.
    //key is CFFileDescriptorRef, value is directory path.
    //one CFFileDescriptorRef can only monitor one directory, so for sub-director, create more CFFileDescriptorRef for it and store in this dictionary.
    NSMutableDictionary *_monitorPaths;
}

//The API is simple, get a shared singleton of FileChangeMonitor, and start/stop monitor it
+ (FileChangeMonitor *)sharedFileChangeMonitor;
- (void)startMonitorDirectory:(NSString *)directoryPath;
- (void)stopMonitorDirectory;

@end
