/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "DynamicXFAHandler.h"
#import "UIExtensionsConfig+private.h"
#import "MenuItem.h"
#import "MenuControl.h"
#import "DynamicXFASignatureAnnotHandler.h"

typedef NS_ENUM(NSInteger, DXFAHandlerPreNextOption) {
    DXFAHandlerPreNextOptionPRE = 0,
    DXFAHandlerPreNextOptionNEXT = 1,
};

static const CGFloat DEFAULT_INPUTVIEW_HEIGHT = 49;

@interface CustomInputAccessoryViewXFA : UIView

@property CGFloat bottomConstraintValue;

@end

@implementation CustomInputAccessoryViewXFA

- (instancetype)init{
    self = [super init];
    if (self) {
        float screenWidth = [UIScreen mainScreen].bounds.size.width;
        self.frame = CGRectMake(0, 0, screenWidth, DEFAULT_INPUTVIEW_HEIGHT);
        
        _bottomConstraintValue = 0;
    }
    return self;
}

-(CGSize)intrinsicContentSize{
    CGSize size = [super intrinsicContentSize];
    float newHeight = _bottomConstraintValue ? DEFAULT_INPUTVIEW_HEIGHT + _bottomConstraintValue : DEFAULT_INPUTVIEW_HEIGHT;
    return CGSizeMake(size.width, newHeight);
}

-(void)layoutMarginsDidChange {
    [super layoutMarginsDidChange];
    _bottomConstraintValue = self.layoutMargins.bottom;
    [self invalidateIntrinsicContentSize];
}
@end


static NSString *FORM_CHAR_BACK = @"BACK";

@interface DynamicXFAHandler ()
@property (nonatomic, strong) UIFSDocProviderCallback *docProviderCallback;
@end
@implementation DynamicXFAHandler {
    int _focusedWidgetIndex;
    int _focusedPageIndex;
    BOOL _lockKeyBoardPosition;
    BOOL _needRotate;
    dispatch_semaphore_t _lock;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        self.extensionsManager = extensionsManager;
        self.pdfViewCtrl = self.extensionsManager.pdfViewCtrl;
        [self.extensionsManager registerRotateChangedListener:self];
        [_pdfViewCtrl registerScrollViewEventListener:(id)self];
        [_pdfViewCtrl registerPageEventListener:(id)self];
        
        self.displayCaretRect = [[FSRectF alloc] init];
        
        _lock = dispatch_semaphore_create(1);

        _keyboardHeight = 0;
        _handlerCurrentWidget = nil;
        _focusedWidgetIndex = -1;
        _focusedPageIndex = -1;
        _inputCharacterLocation = 0;

        
        if (!self.extensionsManager.config.defaultSettings.disableFormNavigationBar) {
            self.formNaviBar = [self buildFormNaviBar];
            [self.pdfViewCtrl addSubview:self.formNaviBar.contentView];
            [self.formNaviBar.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(self.pdfViewCtrl);
                make.height.mas_equalTo(49);
                if (@available(iOS 11.0, *)) {
                    make.bottom.mas_equalTo(self.pdfViewCtrl.mas_safeAreaLayoutGuideBottom);
                } else {
                    make.bottom.mas_equalTo(self.pdfViewCtrl.mas_bottom);
                }
            }];
            [self.formNaviBar.contentView setHidden:YES];
            
            if (IS_IPHONE_OVER_TEN) {
                _formnaviBackView = [[UIView alloc] init];
                _formnaviBackView.backgroundColor = self.formNaviBar.contentView.backgroundColor;
                [self.formNaviBar.contentView insertSubview:_formnaviBackView atIndex:0 ];
                [_formnaviBackView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.left.right.mas_equalTo(self.pdfViewCtrl);
                    make.height.mas_equalTo(70);
                }];
            }
        }

        self.hiddenTextField = [[UITextField alloc] init];
        self.hiddenTextField.hidden = YES;
        self.hiddenTextField.delegate = self;
        self.hiddenTextField.text = @"";
        self.hiddenTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        [self colorChange];
        self.lastText = @"";
        
        self.hiddenTextField.inputAccessoryView = [self inputAccessoryView];
        
//        [self.pdfViewCtrl addSubview:self.hiddenTextField];
        [self.pdfViewCtrl insertSubview:self.hiddenTextField belowSubview:_extensionsManager.topToolbar];
        _lockKeyBoardPosition = NO;
    }
    return self;
}

- (void)colorChange
{
    BOOL isFirst = [self.hiddenTextField isFirstResponder];
    if (isFirst) [self.hiddenTextField resignFirstResponder];
    [self.hiddenTextField setTintColor:[UIColor blackColor]];
    self.hiddenTextField.backgroundColor = [UIColor clearColor];
    if (isFirst) [self.hiddenTextField becomeFirstResponder];
}

- (void)dealloc{
    [self.extensionsManager unregisterRotateChangedListener:self];
}

- (NSString *)getName {
    return @"FSXFAWidget";
}

- (BOOL)canShowKeybord:(FSXFAWidget *)widget {
    FSXFAWidgetWidgetType widgetType = [widget getType];
    return widgetType == FSXFAWidgetWidgetTypeTextEdit || FSXFAWidgetWidgetTypeNumericEdit == widgetType || FSXFAWidgetWidgetTypePasswordEdit == widgetType || FSXFAWidgetWidgetTypeDateTimeEdit == widgetType || widgetType ==  FSXFAWidgetWidgetTypeBarcode;
}

- (BOOL)canFocusXFAWidget:(FSXFAWidget *)widget {
    FSXFAWidgetWidgetType widgetType = [widget getType];
    if (!(widgetType == FSXFAWidgetWidgetTypeArc || FSXFAWidgetWidgetTypeLine == widgetType || FSXFAWidgetWidgetTypePasswordEdit == widgetType || FSXFAWidgetWidgetTypeUnknown == widgetType) ) {
        return YES;
    }
    return NO;
}

- (void)gotoPreNextXFAWidget:(DXFAHandlerPreNextOption)option {
    int curPageIndex = (self.extensionsManager.currentWidget && ![self.extensionsManager.currentWidget isEmpty])?self.extensionsManager.currentWidget.getXFAPage.getIndex:[self.pdfViewCtrl getCurrentPage];
    FSXFADoc *xfaDoc =  [self.pdfViewCtrl getXFADoc];
    FSXFAPage *xfaPage = [xfaDoc getPage:curPageIndex];
    int pageCount = [xfaDoc getPageCount];
    int widgetCount = [xfaPage getWidgetCount];
    BOOL canShowKeybord = NO;
    
    int savedPos = _focusedWidgetIndex;
    int savedPageIndex = curPageIndex;
    
    int prePos = 0;
    int next = 0;
    
    if (option == DXFAHandlerPreNextOptionPRE) {
        prePos = _focusedWidgetIndex < 0 ? 0 : _focusedWidgetIndex - 1;
    }else{
        next = _focusedWidgetIndex + 1;
    }
    
    while (true) {
        if (option == DXFAHandlerPreNextOptionPRE) {
            if (prePos == -1) {
                if (curPageIndex == 0)
                    curPageIndex = pageCount - 1;
                else
                    curPageIndex--;
                
                xfaPage = [xfaDoc getPage:curPageIndex];
                widgetCount = [xfaPage getWidgetCount];
                prePos = widgetCount - 1;
                if (widgetCount == 0)
                    continue;
            }
            if (savedPageIndex == curPageIndex && savedPos == prePos)
                break;
            
            FSXFAWidget *widget = [xfaPage getWidget:prePos];
            
            if (![widget isEmpty] && [widget getPresence] == FSXFAWidgetPresenceVisible && [self canFocusXFAWidget:widget]) {
                canShowKeybord = [self canShowKeybord:widget];
                self.handlerCurrentWidget = widget;
                [self gotoWidget:widget canShowKeybord:canShowKeybord];
                _focusedWidgetIndex = prePos;
                break;
            }
            
            prePos--;
        }else{
            if (next == widgetCount) {
                if (curPageIndex == pageCount - 1)
                    curPageIndex = 0;
                else
                    curPageIndex++;
                
                xfaPage = [xfaDoc getPage:curPageIndex];
                widgetCount = [xfaPage getWidgetCount];
                next = 0;
                if (widgetCount == 0)
                    continue;
            }
            if (savedPageIndex == curPageIndex && next == savedPos)
                break;
            
            FSXFAWidget *widget = [xfaPage getWidget:next];
            if (![widget isEmpty] && [widget getPresence] == FSXFAWidgetPresenceVisible && [self canFocusXFAWidget:widget]) {
                        canShowKeybord = [self canShowKeybord:widget];
                        self.handlerCurrentWidget = widget;
                        [self gotoWidget:widget canShowKeybord:canShowKeybord];
                        _focusedWidgetIndex = next;
                        break;
                    }
            next++;
        }
    }
    
    if (canShowKeybord || (_MAC_CATALYST_ && self.editFormControlNeedTextInput)) {
        [self beginInput];
    } else{
        [self endTextInput];
    }
}

- (UIView *)inputAccessoryView{
    if (self.extensionsManager.config.defaultSettings.disableFormNavigationBar) return nil;
    self.textFormNaviBar = [self buildFormNaviBar];
    if (IS_IPHONE_OVER_TEN) {
        CustomInputAccessoryViewXFA *backview = [[CustomInputAccessoryViewXFA alloc] init];
        backview.translatesAutoresizingMaskIntoConstraints = NO;
        self.textFormNaviBar.contentView.frame = CGRectMake(0, 0, self.pdfViewCtrl.frame.size.width, 49);
        [backview addSubview:self.textFormNaviBar.contentView];
        return backview;
    }else{
        return self.textFormNaviBar.contentView;
    }
}

- (TbBaseBar *)buildFormNaviBar {
    @weakify(self);
    CGRect screenFrame = _pdfViewCtrl.bounds;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        screenFrame = CGRectMake(0, 0, screenFrame.size.height, screenFrame.size.width);
    }

    TbBaseBar *formNaviBar = [[TbBaseBar alloc] init];
    formNaviBar.top = NO;
    formNaviBar.contentView.frame = CGRectMake(0, screenFrame.size.height - 49, screenFrame.size.width, 49);
    formNaviBar.contentView.backgroundColor = NormalBarBackgroundColor;

    UIImage *prevImg = [UIImage imageNamed:@"formfill_pre_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    TbBaseItem *prevItem = [TbBaseItem createItemWithImage:prevImg background:nil];
    prevItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self gotoPreNextXFAWidget:DXFAHandlerPreNextOptionPRE];
    };
    [formNaviBar addItem:prevItem displayPosition:Position_CENTER];

    UIImage *nextImg = [UIImage imageNamed:@"formfill_next_normal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    TbBaseItem *nextItem = [TbBaseItem createItemWithImage:nextImg background:nil];
    nextItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self gotoPreNextXFAWidget:DXFAHandlerPreNextOptionNEXT];
    };
    [formNaviBar addItem:nextItem displayPosition:Position_CENTER];

    TbBaseItem *resetItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kReset") imageNormal:nil imageSelected:nil imageDisable:nil background:nil imageTextRelation:0];
    resetItem.textColor = BlackThemeTextColor;
    resetItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        FSXFADoc *xfadoc = [self.pdfViewCtrl getXFADoc];
        
        @try {
            [xfadoc killFocus];
        } @catch (NSException *exception) {
        }
        
        int curPageIndex = self->_focusedPageIndex;
        FSXFAPage *xfapage = [xfadoc getPage:curPageIndex];
        FSXFAWidget *widget = [xfapage getWidget:self->_focusedWidgetIndex];
        
        if (widget !=nil && ![widget isEmpty]) {
            [widget resetData];
        }
        [xfadoc setFocus:widget];
        
        CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[widget getRect] pageIndex:widget.getXFAPage.getIndex];
        newRect = CGRectInset(newRect, -30, -30);
        [self.pdfViewCtrl refresh:newRect pageIndex:widget.getXFAPage.getIndex needRender:YES];

        self.extensionsManager.isDocModified = YES;
    };
    [formNaviBar addItem:resetItem displayPosition:Position_CENTER];

    TbBaseItem *doneItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kDone") imageNormal:nil imageSelected:nil imageDisable:nil background:nil imageTextRelation:0];
    doneItem.textColor = BlackThemeTextColor;
    doneItem.onTapClick = ^(TbBaseItem *item) {
        @strongify(self);
        [self closeWidget];
    };
    [formNaviBar addItem:doneItem displayPosition:Position_CENTER];
    
    NSArray<UIView *> *tmpItemViews = @[ prevItem.contentView, nextItem.contentView, resetItem.contentView ,doneItem.contentView];
    UIView *superview = formNaviBar.contentView;
    UIView *lastIntervalView = nil;
    int count = (int)tmpItemViews.count;
    
    for (int i = 0; i < count + 1; i++) {
        UIView *intervalView = [[UIView alloc] init];
        [superview addSubview:intervalView];
        
        if (i == 0) {
            [intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.bottom.equalTo(superview);
            }];
        }
        else{
            UIView *itemView = tmpItemViews[i-1];
            [itemView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastIntervalView.mas_right);
                make.centerY.equalTo(superview);
                make.size.sizeOffset(itemView.frame.size);
            }];
            
            [intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(itemView.mas_right);
                make.bottom.equalTo(lastIntervalView);
                make.width.height.equalTo(lastIntervalView);
                if (i == count) {
                    make.right.equalTo(superview);
                }
            }];
        }
        lastIntervalView = intervalView;
    }
    return formNaviBar;
}

-(void)closeWidget {
    self.handlerCurrentWidget = nil;
    [self endTextInput];
    self->_focusedWidgetIndex = -1;
    self->_focusedPageIndex = -1;
    [self.extensionsManager setCurrentWidget:nil];
    [self setFocus:nil isHidden:YES];
}

-(void)gotoWidget:(FSXFAWidget *)widget canShowKeybord:(BOOL)canShowKeybord{
    int curPageIndex = [[widget getXFAPage] getIndex];
    FSRectF *rect = [widget getRect];
    [self setFocus:widget isHidden:NO];
    
    [self.extensionsManager setCurrentWidget:widget];
    
    if (canShowKeybord) {
        [self gotoFormField];
    } else {
        CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:curPageIndex];
        CGPoint pvPt = CGPointZero;
        pvPt.x = pvRect.origin.x - (self.pdfViewCtrl.frame.size.width - pvRect.size.width) / 2;
        pvPt.y = pvRect.origin.y - (self.pdfViewCtrl.frame.size.height - pvRect.size.height) / 2;
        FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvPt pageIndex:curPageIndex];
        [self.pdfViewCtrl gotoPage:curPageIndex withDocPoint:pdfPt animated:YES];
        [self.pdfViewCtrl refresh:curPageIndex needRender:YES];
    }
}

- (BOOL)isHitWidget:(FSXFAWidget *)widget point:(FSPointF *)point {
    if ([widget getPresence] != FSXFAWidgetPresenceVisible) {
        return NO;
    }
    FSXFAWidgetHitTestArea hitAreaType = [widget onHitTest:point];
    if (hitAreaType != FSXFAWidgetHitTestAreaUnknown) {
        return YES;
    }
    return NO;
}

- (void)clickFormField:(FSXFAWidget *)xfawidget point:(FSPointF *)point isHidden:(BOOL)hidden {
    if (xfawidget != nil && ![xfawidget isEmpty]) {
        @try {
            [xfawidget onLButtonDown:point flags:0];
            
        } @catch (NSException *exception) {
        }
        @try {
            [xfawidget onMouseMove:point flags:0];
        } @catch (NSException *exception) {
        }
#if _MAC_CATALYST_
        @try {
            [xfawidget onLButtonUp:point flags:0];
        } @catch (NSException *exception) {
        }
#endif
    }
    
    if (!hidden) {
        FSXFAWidgetWidgetType fieldType = [xfawidget getType];
        if (FSXFAWidgetWidgetTypePushButton == fieldType) {
            hidden = YES;
        }
    }
    [self.formNaviBar.contentView setHidden:hidden];
}

- (void)setFocus:(FSXFAWidget *)widget isHidden:(BOOL)hidden {
    FSXFADoc *xfadoc = [_pdfViewCtrl getXFADoc];
    if (widget && ![widget isEmpty]) {
        @try {
            [xfadoc setFocus:widget];
        } @catch (NSException *exception) {
        }
    } else {
        @try {
            [xfadoc killFocus];
        } @catch (NSException *exception) {
        }
    }
    
    [self.formNaviBar.contentView setHidden:hidden];
}

- (BOOL)compareTwoWidgetIsEqualTo:(FSXFAWidget *)widget compareWidget:(FSXFAWidget *)cwidget {
    if (cwidget == widget) {
        return YES;
    }
    if (!widget || [widget isEmpty] || !cwidget || [cwidget isEmpty]) {
        return NO;
    }
    if ([[widget getXFAPage] getIndex] != [[cwidget getXFAPage] getIndex]) {
        return NO;
    }
    return [widget getIndex] == [cwidget getIndex];
}

#pragma mark IAnnotHandler event
- (void)onXFAWidgetSelected:(FSXFAWidget *)widget {
    _isOver = NO;
    
    FSXFAWidgetWidgetType widgetType = [widget getType];
    if (widgetType != FSXFAWidgetWidgetTypeTextEdit)
        self.editFormControlNeedSetCursor = YES;
    if ([self canShowKeybord:widget])
        self.editFormControlNeedTextInput = YES;
}

- (void)onXFAWidgetDeselected:(FSXFAWidget *)widget {
    int pageIndex = widget.getXFAPage.getIndex;
    [self endForm:pageIndex];
    [_extensionsManager removeThumbnailCacheOfPageAtIndex:pageIndex];
    
    if ([self isXFAWidgetSignature:widget]) {
        [self hideAnnotMenu];
    }
}

// PageView Gesture+Touch
- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer widget:(FSXFAWidget *)widget {
    BOOL canFillForm = [Utility canFillForm:self.pdfViewCtrl];
    if (!canFillForm) {
        return NO;
    }

    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [gestureRecognizer locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    
    if (pageIndex == widget.getXFAPage.getIndex && [self isHitWidget:widget point:pdfPoint]) {
        return YES;
    }
    return NO;
}

-(void)gotoWidgetOnError:(FSXFAWidget *)widget{
    self.hiddenTextField.keyboardType = [widget getType] == FSXFAWidgetWidgetTypeNumericEdit ? UIKeyboardTypeNumberPad : UIKeyboardTypeDefault;
    
    _focusedWidgetIndex = [widget getIndex];
    _focusedPageIndex = [[widget getXFAPage] getIndex];
    _hasFormChanged = NO;
    _editFormControlNeedSetCursor = NO;
    
    BOOL isHasWidgetClick = self.extensionsManager.currentWidget != nil ? YES : NO;
    
    if (self.editFormControlNeedTextInput) {
        _lockKeyBoardPosition = NO;
        [self beginInput];
        
        _hasFormChanged = YES;
        
        if (isHasWidgetClick) {
            BOOL canShowKeybord = [self canShowKeybord:widget];
            if (canShowKeybord) {
                [self gotoFormField];
            }
        }
    } else {
        [self.formNaviBar.contentView setHidden:NO];
        [self endTextInput];
    }
    
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event widget:(FSXFAWidget *)widget {
    BOOL canFillForm = [Utility canFillForm:self.pdfViewCtrl];
    if (!canFillForm) {
        return NO;
    }
//    BOOL needReturn = _isOver;
//    if (needReturn) {
//        return YES;
//    }
    _isInputError = NO;
    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [[touches anyObject] locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    
    if (![self isHitWidget:self.extensionsManager.currentWidget point:pdfPoint]) {
        _handlerCurrentWidget = [self.extensionsManager getXFAWidgetAtPoint:point pageIndex:pageIndex];
    }

    if (_handlerCurrentWidget == nil || [_handlerCurrentWidget isEmpty] || [_handlerCurrentWidget getPresence] != FSXFAWidgetPresenceVisible) {
        [self clickFormField:nil point:pdfPoint isHidden:YES];
        [self.extensionsManager setCurrentWidget:nil];
        [self setFocus:nil isHidden:YES];
        return NO;
    }

    self.hiddenTextField.keyboardType = [widget getType] == FSXFAWidgetWidgetTypeNumericEdit ? UIKeyboardTypeNumberPad : UIKeyboardTypeDefault;
    
    _focusedWidgetIndex = [_handlerCurrentWidget getIndex];
    _focusedPageIndex = pageIndex;
    [self clickFormField:_handlerCurrentWidget point:pdfPoint isHidden:NO];
    
    if (_isInputError) {
        [self clickFormField:nil point:pdfPoint isHidden:YES];
        return NO;
    }
    
    _hasFormChanged = NO;
    _editFormControlNeedSetCursor = NO;
    
    BOOL isHasWidgetClick = self.extensionsManager.currentWidget != nil ? YES : NO;
    
    [self.extensionsManager setCurrentWidget:_handlerCurrentWidget];
    
    if (self.editFormControlNeedTextInput) {
        _lockKeyBoardPosition = NO;
        [self beginInput];
        
        _hasFormChanged = YES;
        
        if (isHasWidgetClick) {
            BOOL canShowKeybord = [self canShowKeybord:widget];
            if (canShowKeybord) {
                [self gotoFormField];
            }
        }
    } else {
        if ([_handlerCurrentWidget getType] != FSXFAWidgetWidgetTypePushButton ) {
            [self.formNaviBar.contentView setHidden:YES];
            [self endTextInput];
        }
        
        if ([self isXFAWidgetSignature:_handlerCurrentWidget] ) {
            @try {
                FSSignature *signature = [_handlerCurrentWidget getSignature];
                if ([signature isEmpty]) return YES;
                
                CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[_handlerCurrentWidget getRect] pageIndex:pageIndex];
                CGRect resultRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:pageIndex];

                DynamicXFASignatureAnnotHandler *signatureAnnotHandler = [[DynamicXFASignatureAnnotHandler alloc] initWithUIExtensionsManager:_extensionsManager];
                [_pdfViewCtrl registerScrollViewEventListener:signatureAnnotHandler];
                [_extensionsManager registerRotateChangedListener:signatureAnnotHandler];
                [_extensionsManager registerAnnotHandler:signatureAnnotHandler];
                signatureAnnotHandler.isDynamicXFAFile = YES;
                signatureAnnotHandler.menuRect = resultRect;
                signatureAnnotHandler.editXFAWidget = _handlerCurrentWidget;
                [signatureAnnotHandler dynamicXFASignatureSelected:signature shouldShowMenu:YES];
                
                
            } @catch (NSException *exception) {
            }
        }
    }
    
    return YES;
}

-(void)hideAnnotMenu {
    MenuControl *annotMenu = _extensionsManager.menuControl;
    if (annotMenu.isMenuVisible) {
        [annotMenu setMenuVisible:NO animated:YES];
    }
}

-(BOOL)isXFAWidgetSignature:(FSXFAWidget *)xfawidget {
    if (xfawidget == nil || [xfawidget isEmpty]) return NO;
    @try {
        if ([xfawidget getType] == FSXFAWidgetWidgetTypeSignature) return YES;
    } @catch (NSException *exception) {
    }
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event widget:(FSXFAWidget *_Nullable)widget{
    if (![self compareTwoWidgetIsEqualTo:self.extensionsManager.currentWidget compareWidget:widget]) {
        return NO;
    }
    
    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [[touches anyObject] locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    @try {
        [widget onMouseMove:pdfPoint flags:0];
    } @catch (NSException *exception) {
    }
    
    return YES;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context widget:(FSXFAWidget *_Nullable)widget{
    if (pageIndex == widget.getXFAPage.getIndex && [self compareTwoWidgetIsEqualTo:widget compareWidget:_handlerCurrentWidget] && [widget getPresence] == FSXFAWidgetPresenceVisible) {
        CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[widget getRect] pageIndex:pageIndex];
        if (rect.size.width > 0 && rect.size.height > 0 && ![self isXFAWidgetSignature:_handlerCurrentWidget]) {
            UIEdgeInsets paddingEdge = UIEdgeInsetsMake(-5, -5, -5, -5);
            if ([widget getType] == FSXFAWidgetWidgetTypeDateTimeEdit) {
                paddingEdge = UIEdgeInsetsMake(-5, -5, -5, -10);
            }
            rect = UIEdgeInsetsInsetRect(rect, paddingEdge);
            CGContextSetLineWidth(context, 2.0);
            CGFloat dashArray[] = {3, 3, 3, 3};
            CGContextSetLineDash(context, 3, dashArray, 4);
            UIColor *dashLineColor = [UIColor colorWithRGB:0x708090];
            CGContextSetStrokeColorWithColor(context, [dashLineColor CGColor]);
            CGContextStrokeRect(context, rect);
        }

        if (self.displayCaretRect != nil || ![self.displayCaretRect isEmpty]) {
            CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.displayCaretRect pageIndex:pageIndex];
            CGRect caretRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:pageIndex];
            self.hiddenTextField.frame = caretRect;
        }
    }
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event widget:(FSXFAWidget *_Nullable)widget {
    if (![self compareTwoWidgetIsEqualTo:self.extensionsManager.currentWidget compareWidget:widget]) {
        return NO;
    }
    
    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [[touches anyObject] locationInView:pageView];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    
    @try {
        [widget onLButtonUp:pdfPoint flags:0];
    } @catch (NSException *exception) {
    }
    return YES;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer widget:(FSXFAWidget *_Nullable)widget{
    int wpageIndex = [[widget getXFAPage] getIndex];
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:wpageIndex]];
    FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:wpageIndex];
    
    if ([self compareTwoWidgetIsEqualTo:widget compareWidget:_extensionsManager.currentWidget]) {
        if (!(pageIndex == wpageIndex && [self isHitWidget:widget point:pdfPoint])) {
            [_extensionsManager setCurrentWidget:nil];
        }
    } else {
        [_extensionsManager setCurrentWidget:widget];
    }
    return YES;

}

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    return YES;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
}

- (BOOL)addAnnot:(nonnull FSAnnot *)annot {
    return NO;
}

- (BOOL)addAnnot:(nonnull FSAnnot *)annot addUndo:(BOOL)addUndo {
    return NO;
}

- (FSAnnotType)getType {
    return 0;
}

- (BOOL)isHitAnnot:(nonnull FSAnnot *)annot point:(nonnull FSPointF *)point {
    return YES;
}

- (BOOL)modifyAnnot:(nonnull FSAnnot *)annot {
    return NO;
}

- (BOOL)modifyAnnot:(nonnull FSAnnot *)annot addUndo:(BOOL)addUndo {
    return NO;
}
- (void)onAnnotDeselected:(nonnull FSAnnot *)annot {
}

- (void)onAnnotSelected:(nonnull FSAnnot *)annot {
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(nonnull UIGestureRecognizer *)gestureRecognizer annot:(FSAnnot * _Nullable)annot {
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(nonnull NSSet *)touches withEvent:(nonnull UIEvent *)event annot:(nonnull FSAnnot *)annot {
    return YES;
}

- (BOOL)removeAnnot:(nonnull FSAnnot *)annot {
    return NO;
}

- (BOOL)removeAnnot:(nonnull FSAnnot *)annot addUndo:(BOOL)addUndo {
    return NO;
}

- (void)endOp:(BOOL)needDelay {
    if (!_isOver) {
        _isOver = YES;
    }
    [self.pdfViewCtrl refresh:CGRectZero pageIndex:[self.pdfViewCtrl getCurrentPage]];
}

- (void)beginInput{
    #if !_MAC_CATALYST_
    [self.formNaviBar.contentView setHidden:YES];
    #endif
    [self.hiddenTextField becomeFirstResponder];
    self.hiddenTextField.text = [_handlerCurrentWidget isEmpty] ? @"" : [_handlerCurrentWidget getValue];
    
}

- (void)endTextInput {
    self.editFormControlNeedTextInput = NO;
    if (self.hiddenTextField) {
        [self.hiddenTextField resignFirstResponder];
        _isOver = NO;
        self.lastText = @"";
        self.hiddenTextField.text = @"";
        
    }
}

- (void)endForm:(int)pageIndex {
    if (self.extensionsManager.currentWidget) {
        [self endTextInput];
    }
}

- (void)textViewInputText:(NSString *)text{
    if(_inputCharacterLocation == 0xFFFF) return;
    _inputCharacterLocation = 0xFFFF;
    for(NSUInteger i=0; i<text.length; i++)
    {
        unichar cha = [text characterAtIndex:i];
        //Control character as space.
        if(cha == 0x2006) cha = FSFillerVkeySpace;
        [self formInputText:[[NSString alloc] initWithCharacters:&cha length:1]];
    }
    if (!self.editFormControlNeedTextInput) {
        [self endOp:YES];
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.length > 0 )
    {
        for(int i=0; i<range.length; i++)
            [self formInputText:FORM_CHAR_BACK];
    }
    else
    {
        if([string isEqualToString:@""])
            [self formInputText:FORM_CHAR_BACK];
    }
    
    _inputCharacterLocation = range.location;
    [self.pdfViewCtrl lockRefresh];
    [self textViewInputText:string];
    [self.pdfViewCtrl unlockRefresh];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.editFormControlNeedTextInput = NO;
    if (self.hiddenTextField) {
        _isOver = NO;
        self.lastText = @"";
        self.hiddenTextField.text = @"";
    }
}

#pragma mark -
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (void)gotoFormField {
    
     dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER);
    FSXFAWidget *widget = self.extensionsManager.currentWidget;
    if (widget && ![widget isEmpty]) {
        int widgetPageIndex = widget.getXFAPage.getIndex;
        CGPoint oldPvPoint = [self.pdfViewCtrl convertDisplayViewPtToPageViewPt:CGPointMake(0, 0) pageIndex:widgetPageIndex];
        FSPointF *oldPdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:oldPvPoint pageIndex:widgetPageIndex];
        
        CGRect pvAnnotRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[widget getRect] pageIndex:widgetPageIndex];
        CGRect dvAnnotRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:pvAnnotRect pageIndex:widgetPageIndex];
        
         PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
        
        if (_needRotate) {
            CGPoint annotPVOrigin = [self.pdfViewCtrl convertDisplayViewPtToPageViewPt:pvAnnotRect.origin pageIndex:widgetPageIndex];
            FSPointF *annotPVOriginf = [self.pdfViewCtrl convertPageViewPtToPdfPt:annotPVOrigin pageIndex:widgetPageIndex];
            [self.pdfViewCtrl gotoPage:widgetPageIndex withDocPoint:annotPVOriginf animated:YES];
            _needRotate = NO;
            pvAnnotRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[widget getRect] pageIndex:widgetPageIndex];
            dvAnnotRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:pvAnnotRect pageIndex:widgetPageIndex];
            if ([self.pdfViewCtrl isContinuous]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self gotoFormField];
                });
                dispatch_semaphore_signal(_lock);
                return;
            }
        }
        
        CGFloat dvAnnotMaxYExtra = CGRectGetHeight(_pdfViewCtrl.bounds) - CGRectGetMaxY(dvAnnotRect);
        
        if (dvAnnotMaxYExtra < _keyboardHeight) {
            float dvOffsetY = _keyboardHeight - fabs(dvAnnotMaxYExtra) + 20;
            
            if (dvOffsetY + CGRectGetHeight(pvAnnotRect) > CGRectGetHeight(_pdfViewCtrl.bounds)) {
                dvOffsetY =  CGRectGetHeight(_pdfViewCtrl.bounds) - _keyboardHeight - 88;
            }
            CGRect offsetRect = CGRectMake(0, 0, 100, dvOffsetY);
            
            CGRect pvRect = [self.pdfViewCtrl convertDisplayViewRectToPageViewRect:offsetRect pageIndex:widgetPageIndex];
            FSRectF *pdfRect = [self.pdfViewCtrl convertPageViewRectToPdfRect:pvRect pageIndex:widgetPageIndex];
            float pdfOffsetY = pdfRect.top - pdfRect.bottom;
            
            FSPointF *jumpPdfPoint = [[FSPointF alloc] init];
            [jumpPdfPoint set:oldPdfPoint.x y:oldPdfPoint.y - pdfOffsetY];
            if (pageLayoutMode == PDF_LAYOUT_MODE_SINGLE || pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE) {
                [self.pdfViewCtrl setBottomOffset:dvOffsetY];
                if (self.displayCaretRect != nil || ![self.displayCaretRect isEmpty]) {
                    CGRect caretRect = self.hiddenTextField.frame;
                    CGRect rect = CGRectOffset(caretRect, 0, -dvOffsetY);
                    self.hiddenTextField.frame = rect;
                }
            } else if ([self.pdfViewCtrl isContinuous]) {
                int currentPage = [self.pdfViewCtrl getCurrentPage];
                if (currentPage == currentPage - 1) {
                    FSRectF *fsRect = [[FSRectF alloc] init];
                    [fsRect setLeft:0];
                    [fsRect setBottom:pdfOffsetY];
                    [fsRect setRight:pdfOffsetY];
                    [fsRect setTop:0];
                    float tmpPvOffset = [self.pdfViewCtrl convertPdfRectToPageViewRect:fsRect pageIndex:widgetPageIndex].size.width;
                    CGRect tmpPvRect = CGRectMake(0, 0, 10, tmpPvOffset);
                    CGRect tmpDvRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:tmpPvRect pageIndex:widgetPageIndex];
                    [self.pdfViewCtrl setBottomOffset:tmpDvRect.size.height];
                } else {
                     [self.pdfViewCtrl setBottomOffset:dvOffsetY];
//                    [self.pdfViewCtrl gotoPage:widgetPageIndex withDocPoint:jumpPdfPoint animated:YES];
                }
            }
        } else {
            CGPoint dvPt = CGPointZero;
            dvPt.x = dvAnnotRect.origin.x - (self.pdfViewCtrl.frame.size.width - dvAnnotRect.size.width) / 2;
            dvPt.y = dvAnnotRect.origin.y - (self.pdfViewCtrl.frame.size.height - dvAnnotRect.size.height) / 2;
            CGPoint pvPt = [self.pdfViewCtrl convertDisplayViewPtToPageViewPt:dvPt pageIndex:widgetPageIndex];
            FSPointF *pdfPt = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvPt pageIndex:widgetPageIndex];
            if (![self.pdfViewCtrl isContinuous]) {
                [self.pdfViewCtrl gotoPage:widgetPageIndex withDocPoint:pdfPt animated:YES];
            }
        }
    }
     dispatch_semaphore_signal(_lock);
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    if (_keyboardShown)
        return;
    _keyboardShown = YES;
    NSDictionary *info = [aNotification userInfo];
    // Get the frame of the keyboard.
    NSValue *frame = nil;
    frame = [info objectForKey:UIKeyboardBoundsUserInfoKey];
    CGRect keyboardFrame = [frame CGRectValue];
    _keyboardHeight = keyboardFrame.size.height;
    [self gotoFormField];
}

- (void)keyboardWasHidden:(NSNotification *)aNotification {
    _keyboardShown = NO;
    [self endOp:YES];
    FSXFAWidget *xfaWidget = self.extensionsManager.currentWidget;
    if (xfaWidget && ![xfaWidget isEmpty] && !_lockKeyBoardPosition) {
        PDF_LAYOUT_MODE pageLayoutMode = [self.pdfViewCtrl getPageLayoutMode];
        int pageIndex = xfaWidget.getXFAPage.getIndex;
        if (pageLayoutMode == PDF_LAYOUT_MODE_SINGLE || pageIndex == [self.pdfViewCtrl.currentDoc getPageCount] - 1 || pageLayoutMode == PDF_LAYOUT_MODE_TWO || pageLayoutMode == PDF_LAYOUT_MODE_TWO_LEFT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageLayoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE) {
            [self.pdfViewCtrl setBottomOffset:0];
        }
        else if ([self.pdfViewCtrl isContinuous]) {
            [self.pdfViewCtrl setBottomOffset:0];
        }
    }
}

- (void)formInputText:(NSString *)character {
    @synchronized(self) {
        unsigned int code = 0;

        if (!character || character.length == 0) {
            return;
        } else if ([character isEqualToString:@"\n"]) {
            code = 0x0D; //enter key
        } else if ([character isEqualToString:FORM_CHAR_BACK]) {
            code = 0x08; //backspace key
        } else {
            NSData *myD = [character dataUsingEncoding:NSUTF16LittleEndianStringEncoding];
            Byte *bytes = (Byte *) [myD bytes];
            //byte to hex
            NSString *hexStr = @"";
            for (int i = 0; i < [myD length]; i++) {
                NSString *newHexStr = [NSString stringWithFormat:@"%x", bytes[i] & 0xff]; //hex
                if ([newHexStr length] == 1)
                    hexStr = [NSString stringWithFormat:@"0%@%@", newHexStr, hexStr];
                else
                    hexStr = [NSString stringWithFormat:@"%@%@", newHexStr, hexStr];
            }
            code = (unsigned int) strtoul([[hexStr substringWithRange:NSMakeRange(0, hexStr.length)] UTF8String], 0, 16);
        }

        [_handlerCurrentWidget onKeyDown:code flags:0];
        [_handlerCurrentWidget onChar:code flags:0];
        [_handlerCurrentWidget onKeyUp:code flags:0];
        
        _hasFormChanged = YES;
    }
}

#pragma mark IDocEventListener
- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess) {
        return;
    }
    _handlerCurrentWidget = nil;
    
    if ([_pdfViewCtrl isDynamicXFA]) {
        FSXFADoc *xfadoc = [_pdfViewCtrl getXFADoc];
        UIFSDocProviderCallback *xfadocProviderCB = [[UIFSDocProviderCallback alloc] initWithExtensionsManager:_extensionsManager];
        [xfadoc setDocProviderCallback:xfadocProviderCB];
        _docProviderCallback = xfadocProviderCB;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasShown:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    [self.formNaviBar.contentView setHidden:YES];
    if (document)
        _handlerCurrentWidget = nil;
    
    if ([_pdfViewCtrl isDynamicXFA]) {
        _docProviderCallback = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }

}

#pragma mark IRotationEventListener

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (_keyboardShown) {
        [self endTextInput];
        _needRotate = YES;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    self.hiddenTextField.inputAccessoryView = [self inputAccessoryView];
    if (_needRotate) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self beginInput];
        });
    }
}
#pragma scrollViewEventListener methods
-(void)onScrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_handlerCurrentWidget != nil || ![self.displayCaretRect isEmpty]) {
        int pageIndex = _handlerCurrentWidget.getXFAPage.getIndex;
        CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.displayCaretRect pageIndex:pageIndex];
        CGRect caretRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:pageIndex];
        self.hiddenTextField.frame = caretRect;
    }
}

@end
