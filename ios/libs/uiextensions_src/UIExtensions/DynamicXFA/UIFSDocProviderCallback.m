/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIFSDocProviderCallback.h"

@implementation UIFSDocProviderCallback
- (id)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
    }
    return self;
}

-(void)invalidateRect:(int)page_index rect:(FSRectF*)rect flag:(FSDocProviderCallbackInvalidateFlag)flag{
    CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:page_index];
    newRect = CGRectInset(newRect, -5, -5);
    [self.pdfViewCtrl refresh:newRect pageIndex:page_index needRender:YES];
}

-(void)displayCaret:(int)page_index is_visible:(BOOL)is_visible rect:(FSRectF*)rect{
    CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:rect pageIndex:page_index];
    CGRect caretRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:newRect pageIndex:page_index];
//    CGRect view2Rect = CGRectOffset(caretRect, 1, 0);
    DynamicXFAHandler *annotHandler = nil;
    annotHandler = (DynamicXFAHandler *)[_extensionsManager getDynamicAnnotHandler];
    annotHandler.displayCaretRect = [[FSRectF alloc] initWithLeft1:[rect getLeft] bottom1:[rect getBottom] right1:[rect getRight] top1:[rect getTop]];
   
    if (is_visible) {
        annotHandler.hiddenTextField.frame = caretRect;
        annotHandler.hiddenTextField.hidden = NO;
    }else{
        annotHandler.hiddenTextField.hidden = YES;
    }
}

-(BOOL)getPopupPos:(int)page_index min_popup:(int)min_popup max_popup:(int)max_popup rect_widget:(FSRectF*)rect_widget inout_rect_popup:(FSRectF*)inout_rect_popup {
    CGRect pageViewRect = [_pdfViewCtrl getPageView:page_index].frame;
    FSRectF *newRect = [_pdfViewCtrl convertPageViewRectToPdfRect:pageViewRect pageIndex:page_index];
    
    BOOL ret = NO;
    inout_rect_popup.left = 0;
    if (rect_widget.right > newRect.right) {
        inout_rect_popup.left -= rect_widget.right - newRect.right;
        inout_rect_popup.right -= rect_widget.right - newRect.right;
    }else if (fabs( newRect.bottom - rect_widget.bottom) >= max_popup) {
        inout_rect_popup.top = fabs(rect_widget.bottom - rect_widget.top);
        inout_rect_popup.bottom = inout_rect_popup.top - max_popup;
        ret = YES;
    }else if (rect_widget.top - newRect.top >= max_popup) {
        inout_rect_popup.top = -max_popup;
        inout_rect_popup.bottom = inout_rect_popup.top - max_popup;
        ret = YES;
    }else if (newRect.bottom - rect_widget.bottom >= min_popup) {
        inout_rect_popup.top = fabs(rect_widget.top - rect_widget.bottom);
        inout_rect_popup.bottom = inout_rect_popup.top - (newRect.bottom - rect_widget.bottom);
        ret = YES;
    }else if (rect_widget.top - newRect.top >= min_popup) {
        inout_rect_popup.top = -(inout_rect_popup.top - newRect.top);
        inout_rect_popup.bottom = inout_rect_popup.top - (rect_widget.top - newRect.top);
        ret = YES;
    }
    return ret;
}
-(BOOL)popupMenu:(int)page_index rect_popup:(FSPointF*)rect_popup{
    return YES;
}
-(int)getCurrentPage:(FSXFADoc*) doc{
    return [_pdfViewCtrl getCurrentPage];
}

-(void)setCurrentPage:(FSXFADoc*) doc current_page_index:(int)current_page_index{
    if ([NSThread isMainThread]) {
        [_pdfViewCtrl gotoPage:current_page_index animated:NO];
    }else{
        dispatch_semaphore_t wait = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.pdfViewCtrl gotoPage:current_page_index animated:NO];
            dispatch_semaphore_signal(wait);
        });
        dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
    }
}
-(void)setChangeMark:(FSXFADoc*) doc{
    _extensionsManager.isDocModified = YES;
}
-(NSString*)getTitle:(FSXFADoc*) doc{
    return @"";
}
-(void)setFocus:(FSXFAWidget *) xfa_widget{
    if (xfa_widget == nil || [xfa_widget isEmpty]) return;
    DynamicXFAHandler *annotHandler = (DynamicXFAHandler *)[_extensionsManager getDynamicAnnotHandler];
    
    int focusPageIndex = [[xfa_widget getXFAPage] getIndex];

    if (focusPageIndex != self.extensionsManager.currentPageIndex) {
        [_pdfViewCtrl gotoPage:focusPageIndex animated:NO];
        annotHandler.handlerCurrentWidget = nil;
        [annotHandler.formNaviBar.contentView setHidden:YES];
        
        FSRectF *rect = [xfa_widget getRect];
        FSPointF *point = [[FSPointF alloc] initWithX:rect.left Y:rect.bottom];
        @try {
            [xfa_widget onLButtonUp:point flags:0];
        } @catch (NSException *exception) {
        }
    }
    
    // if handlerCurrentWidget = nil , mean click done button or click space place
    // if handlerCurrentWidget != nil, mean click other widget
    
    BOOL isCurrentWidgetEmpty = (annotHandler.handlerCurrentWidget == nil && self.extensionsManager.currentWidget == nil);
    BOOL isSameWidget = (annotHandler.handlerCurrentWidget != nil && self.extensionsManager.currentWidget != nil
                         && ![annotHandler compareTwoWidgetIsEqualTo:self.extensionsManager.currentWidget compareWidget:annotHandler.handlerCurrentWidget]);
    if ((isCurrentWidgetEmpty || isSameWidget) && [GlableInstance shareInstance].xfaInputError) {
        annotHandler.isInputError = YES;
        annotHandler.isOver = NO;
        annotHandler.handlerCurrentWidget = xfa_widget;
        [self.extensionsManager setCurrentWidget:xfa_widget];
        [annotHandler gotoWidgetOnError:xfa_widget];
        [GlableInstance shareInstance].xfaInputError = NO;
    }
}
-(void)exportData:(FSXFADoc*)doc file_path:(NSString*) file_path{
}
-(void)importData:(FSXFADoc*)doc file_path:(NSString*) file_path{
}
-(void)gotoURL:(FSXFADoc*)doc url:(NSString*) url{
}
-(void)print:(FSXFADoc*)doc start_page_index:(int)start_page_index end_page_index:(int)end_page_index options:(int)options{
}
-(int)getHighlightColor:(FSXFADoc*)doc{
    return self.extensionsManager.highlightFormColor.fs_argbValue;
}
-(BOOL)submitData:(FSXFADoc*)doc target:(NSString*)target format:(FSDocProviderCallbackSubmitFormat)format text_encoding:(FSDocProviderCallbackTextEncoding)text_encoding content:(NSString*)content{
    return YES;
}
-(void)pageViewEvent:(int)page_index page_view_event_type:(FSDocProviderCallbackPageViewEventType)page_view_event_type{
    if (page_view_event_type == FSDocProviderCallbackPageViewEventTypeRemoved) {
        page_index = page_index == -1 ? [[self.pdfViewCtrl getXFADoc] getPageCount] : page_index;
        [self.pdfViewCtrl reloadXFADoc:page_view_event_type page_index:@[@(page_index)]];
    }
    
    if (page_view_event_type == FSDocProviderCallbackPageViewEventTypeAdded) {
        page_index = page_index == -1 ? [[self.pdfViewCtrl getXFADoc] getPageCount] : page_index;
        [self.pdfViewCtrl reloadXFADoc:page_view_event_type page_index:@[@(page_index)]];
    }
}
-(void)widgetEvent:(FSXFAWidget *) xfa_widget widget_event_type:(FSDocProviderCallbackWidgetEventType)widget_event_type {
    if (widget_event_type == FSDocProviderCallbackWidgetEventTypeBeforeRemoved) {
        FSXFADoc *xfadoc = [self.pdfViewCtrl getXFADoc];
        DynamicXFAHandler *annotHandler = nil;
        annotHandler = (DynamicXFAHandler *)[_extensionsManager getDynamicAnnotHandler];
        if ([annotHandler compareTwoWidgetIsEqualTo:annotHandler.handlerCurrentWidget compareWidget:xfa_widget]) {
            annotHandler.handlerCurrentWidget = nil;
            [_extensionsManager setCurrentWidget:nil];
            
            @try {
                [xfadoc killFocus];
            } @catch (NSException *exception) {
            }
        }
    }
}
@end
