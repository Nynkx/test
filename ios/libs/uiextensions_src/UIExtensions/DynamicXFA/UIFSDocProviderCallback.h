/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "DynamicXFAHandler.h"

@interface UIFSDocProviderCallback : NSObject <FSDocProviderCallback>
@property (nonatomic, strong) FSXFAWidget *focusXFAWidget;

@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;

- (id)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager;
-(void)invalidateRect:(int)page_index rect:(FSRectF*)rect flag:(FSDocProviderCallbackInvalidateFlag)flag;
-(void)displayCaret:(int)page_index is_visible:(BOOL)is_visible rect:(FSRectF*)rect;
-(BOOL)getPopupPos:(int)page_index min_popup:(int)min_popup max_popup:(int)max_popup rect_widget:(FSRectF*)rect_widget inout_rect_popup:(FSRectF*)inout_rect_popup;
-(BOOL)popupMenu:(int)page_index rect_popup:(FSPointF*)rect_popup;
-(int)getCurrentPage:(FSXFADoc*) doc;
-(void)setCurrentPage:(FSXFADoc*) doc current_page_index:(int)current_page_index;
-(void)setChangeMark:(FSXFADoc*) doc;
-(NSString*)getTitle:(FSXFADoc*) doc;
-(void)setFocus:(FSXFAWidget *) xfa_widget;
-(void)exportData:(FSXFADoc*)doc file_path:(NSString*) file_path;
-(void)importData:(FSXFADoc*)doc file_path:(NSString*) file_path;
-(void)gotoURL:(FSXFADoc*)doc url:(NSString*) url;
-(void)print:(FSXFADoc*)doc start_page_index:(int)start_page_index end_page_index:(int)end_page_index options:(int)options;
-(int)getHighlightColor:(FSXFADoc*)doc;
-(BOOL)submitData:(FSXFADoc*)doc target:(NSString*)target format:(FSDocProviderCallbackSubmitFormat)format text_encoding:(FSDocProviderCallbackTextEncoding)text_encoding content:(NSString*)content;
-(void)pageViewEvent:(int)page_index page_view_event_type:(FSDocProviderCallbackPageViewEventType)page_view_event_type;
-(void)widgetEvent:(FSXFAWidget *) xfa_widget widget_event_type:(FSDocProviderCallbackWidgetEventType)widget_event_type;
@end
