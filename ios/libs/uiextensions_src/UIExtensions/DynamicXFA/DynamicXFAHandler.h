/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "TbBaseBar.h"
#import "UIFSDocProviderCallback.h"

@protocol IGestureEventListener;

@interface DynamicXFAHandler : NSObject <IAnnotHandler, IDocEventListener, IRotationEventListener, UITextFieldDelegate> {
    BOOL _keyboardShown;
    float _keyboardHeight;
    
    NSUInteger _inputCharacterLocation;
}

@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (nonatomic, assign) BOOL hasFormChanged;
@property (nonatomic, assign) int editFormControlNeedTextInput;
@property (nonatomic, assign) BOOL editFormControlNeedSetCursor;
@property (nonatomic, strong) UITextField *hiddenTextField;
@property (nonatomic, copy) NSString *lastText;
@property (nonatomic, strong) TbBaseBar *formNaviBar;
@property (nonatomic, strong) TbBaseBar *textFormNaviBar;
@property (nonatomic, strong) UIView *formnaviBackView;

@property (nonatomic, assign) BOOL isOver;
@property (nonatomic, strong) FSXFAWidget *handlerCurrentWidget;
@property (nonatomic, assign) BOOL isInputError;
@property (nonatomic, strong) FSRectF *displayCaretRect;

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager;
- (TbBaseBar *)buildFormNaviBar;
- (void)endTextInput;

-(void)gotoWidgetOnError:(FSXFAWidget *)widget;
- (BOOL)compareTwoWidgetIsEqualTo:(FSXFAWidget *)widget compareWidget:(FSXFAWidget *)cwidget ;
@end
