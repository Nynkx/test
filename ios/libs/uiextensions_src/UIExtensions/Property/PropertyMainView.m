/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PropertyMainView.h"
#import "PropertyBar.h"

@interface PropertyMainView ()

@property (nonatomic, strong) ColorLayout *currentCoverTextColorLayout;
@property (nonatomic, strong) ColorLayout *currentColorLayout;
@property (nonatomic, strong) OpacityLayout *currentOpacityLayout;
@property (nonatomic, strong) LineWidthLayout *currentLineWidthLayout;
@property (nonatomic, strong) FontLayout *currentFontLayout;
@property (nonatomic, strong) IconLayout *currentIconLayout;
@property (nonatomic, strong) DistanceUnitLayout *currentdistanceUnitLayout;
@property (nonatomic, strong) RotationLayout *currentRotationLayout;
@property (nonatomic, strong) TextInputLayout *currentTextInputLayout;
@property (nonatomic, strong) FormListLayount *currentFormListLayout;

@end

@implementation PropertyMainView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.segmentItems = [NSMutableArray array];
    }
    return self;
}

- (void)showTab:(Property_TabType)type {
    if (self.currentColorLayout) {
        self.currentColorLayout.hidden = YES;
    }
    if (self.currentOpacityLayout) {
        self.currentOpacityLayout.hidden = YES;
    }
    if (self.currentLineWidthLayout) {
        self.currentLineWidthLayout.hidden = YES;
    }
    if (self.currentFontLayout) {
        self.currentFontLayout.hidden = YES;
    }
    if (self.currentIconLayout) {
        self.currentIconLayout.hidden = YES;
    }
    if (self.currentdistanceUnitLayout) {
        self.currentdistanceUnitLayout.hidden = YES;
    }
    if (self.currentTextInputLayout) {
        self.currentTextInputLayout.hidden = YES;
    }
    if (self.currentCoverTextColorLayout) {
        self.currentCoverTextColorLayout.hidden = YES;
    }
    if (self.currentFormListLayout) {
        self.currentFormListLayout.hidden = YES;
    }
    switch (type) {
    case TAB_FILL:
        if (self.currentColorLayout) {
            self.currentColorLayout.hidden = NO;
        }
        if (self.currentOpacityLayout) {
            self.currentOpacityLayout.hidden = NO;
        }
        if (self.currentCoverTextColorLayout) {
            self.currentColorLayout.hidden = NO;
        }
        break;
    case TAB_BORDER:
        if (self.currentLineWidthLayout) {
            self.currentLineWidthLayout.hidden = NO;
        }
        break;
    case TAB_FONT:
        if (self.currentFontLayout) {
            self.currentFontLayout.hidden = NO;
        }
        if (self.currentFormListLayout) {
            self.currentColorLayout.hidden = NO;
        }
        break;
    case TAB_TYPE:
        if (self.currentIconLayout) {
            self.currentIconLayout.hidden = NO;
        }
        if (self.currentCoverTextColorLayout) {
            self.currentColorLayout.hidden = YES;
            self.currentTextInputLayout.hidden = NO;
            self.currentCoverTextColorLayout.hidden = NO;
            self.currentFontLayout.hidden = NO;
        }
        break;
    case TAB_OVERLAY_TEXT:
        if (self.currentCoverTextColorLayout) {
            self.currentColorLayout.hidden = YES;
            self.currentTextInputLayout.hidden = NO;
            self.currentCoverTextColorLayout.hidden = NO;
            self.currentFontLayout.hidden = NO;
        }
    case TAB_DISTANCE_UNIT:
        if (self.currentdistanceUnitLayout) {
            self.currentdistanceUnitLayout.hidden = NO;
        }
        break;
    case TAB_OPTION:
        if (self.currentFormListLayout) {
            self.currentFormListLayout.hidden = NO;
        }
            break;
    default:
        break;
    }
}

- (void)addLayoutAtTab:(UIView *)layout tab:(Property_TabType)tab {
    switch (tab) {
    case TAB_FILL: {
        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if ([(id) layout supportProperty] & PROPERTY_COLOR) {
                self.currentColorLayout = (ColorLayout *) layout;
                CGRect colorFrame = layout.frame;
                colorFrame.origin.y = TABHEIGHT;
                colorFrame.size.height = [(ColorLayout *) layout layoutHeight];
                layout.frame = colorFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }

        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if ([(id) layout supportProperty] & PROPERTY_OPACITY) {
                self.currentOpacityLayout = (OpacityLayout *) layout;
                CGRect opacityFrame = layout.frame;
                opacityFrame.origin.y = TABHEIGHT + self.currentColorLayout.layoutHeight;
                opacityFrame.size.height = [(OpacityLayout *) layout layoutHeight];
                layout.frame = opacityFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
            if ([(id) layout supportProperty] & PROPERTY_ROTATION) {
                self.currentRotationLayout = (RotationLayout *) layout;
                CGRect rotationFrame = layout.frame;
                rotationFrame.origin.y = TABHEIGHT + self.currentColorLayout.layoutHeight;
                rotationFrame.size.height = [(RotationLayout *) layout layoutHeight];
                layout.frame = rotationFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
            
            if ([(id) layout supportProperty] & PROPERTY_TEXTINPUT || [(id) layout supportProperty] & PROPERTY_TEXTNAME) {
                self.currentTextInputLayout = (TextInputLayout *) layout;
                CGRect textInputFrame = layout.frame;
                textInputFrame.origin.y = TABHEIGHT;
                textInputFrame.size.height = [(TextInputLayout *) layout layoutHeight];
                layout.frame = textInputFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }
    } break;
    case TAB_BORDER: {
        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if ([(id) layout supportProperty] & PROPERTY_LINEWIDTH) {
                self.currentLineWidthLayout = (LineWidthLayout *) layout;
                CGRect lineWidthFrame = layout.frame;
                lineWidthFrame.origin.y = TABHEIGHT;
                lineWidthFrame.size.height = [(LineWidthLayout *) layout layoutHeight];
                layout.frame = lineWidthFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }
    } break;
    case TAB_FONT: {
        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if ([(id) layout supportProperty] & PROPERTY_FONTNAME) {
                self.currentFontLayout = (FontLayout *) layout;
                CGRect fontFrame = layout.frame;
                fontFrame.origin.y = TABHEIGHT;
                fontFrame.size.height = [(FontLayout *) layout layoutHeight];
                layout.frame = fontFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }
    } break;
    case TAB_DISTANCE_UNIT: {
        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if ([(id) layout supportProperty] & PROPERTY_DISTANCE_UNIT) {
                self.currentdistanceUnitLayout = (DistanceUnitLayout *) layout;
                CGRect fontFrame = layout.frame;
                fontFrame.origin.y = TABHEIGHT;
                fontFrame.size.height = [(DistanceUnitLayout *) layout layoutHeight];
                layout.frame = fontFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }
    } break;            
    case TAB_TYPE: {
        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if ([(id) layout supportProperty] & PROPERTY_ICONTYPE || [(id) layout supportProperty] & PROPERTY_ATTACHMENT_ICONTYPE) {
                self.currentIconLayout = (IconLayout *) layout;
                CGRect typeFrame = layout.frame;
                typeFrame.origin.y = TABHEIGHT;
                typeFrame.size.height = [(IconLayout *) layout layoutHeight];
                layout.frame = typeFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
            if (layout.tag == PROPERTY_TEXTINPUT) {
                
                self.currentCoverTextColorLayout = (ColorLayout *) layout;
                CGRect typeFrame = layout.frame;
                typeFrame.origin.y = TABHEIGHT;
                typeFrame.size.height = [(ColorLayout *) layout layoutHeight];
                layout.frame = typeFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }
        
        BOOL isTyped = NO;
        for (SegmentItem *item in self.segmentItems) {
            if (item.tag == TAB_FILL) {
                isTyped = YES;
                break;
            }
        } 
        if (!isTyped) {
            SegmentItem *typeItem = [[SegmentItem alloc] init];
            typeItem.title = self.currentCoverTextColorLayout ? FSLocalizedForKey(@"kPropertyFill") : FSLocalizedForKey(@"kIcon");
            typeItem.image = nil;
            typeItem.tag = self.currentCoverTextColorLayout ? TAB_FILL : TAB_TYPE;
            typeItem.titleNormalColor = UIColor_DarkMode(UIColorHex(0x179cd8), ThemeTextColor_Dark);        
            typeItem.titleSelectedColor = WhiteThemeTextColor;
            [self.segmentItems addObject:typeItem];
        }

        BOOL isFilled = NO;
        for (SegmentItem *item in self.segmentItems) {
            if (self.currentCoverTextColorLayout) {
                if (item.tag == TAB_OVERLAY_TEXT) {
                    isFilled = YES;
                    break;
                }
            }else{
                if (item.tag == TAB_FILL) {
                    isFilled = YES;
                    break;
                }
            }
        }
        if (!isFilled) {
            SegmentItem *fillItem = [[SegmentItem alloc] init];
            fillItem.title = self.currentCoverTextColorLayout ? FSLocalizedForKey(@"kOverlayText") : FSLocalizedForKey(@"kPropertyFill");
            fillItem.image = nil;
            fillItem.tag = self.currentCoverTextColorLayout ? TAB_OVERLAY_TEXT : TAB_FILL;
            fillItem.titleNormalColor =  UIColor_DarkMode(UIColorHex(0x179cd8), ThemeTextColor_Dark);  ;
            fillItem.titleSelectedColor = WhiteThemeTextColor;
            [self.segmentItems addObject:fillItem];
        }
    } break;
    case TAB_OPTION:{
        if ([layout respondsToSelector:@selector(supportProperty)]) {
            if (layout.tag == PROPERTY_FORMOPTION) {
                self.currentFormListLayout = (FormListLayount *) layout;
                CGRect typeFrame = layout.frame;
                typeFrame.origin.y = TABHEIGHT;
                typeFrame.size.height = [(FormListLayount *) layout layoutHeight];
                layout.frame = typeFrame;
                layout.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
                [self addSubview:layout];
            }
        }
        
        BOOL isFontName = NO;
        for (SegmentItem *item in self.segmentItems) {
            if (item.tag == TAB_OPTION) {
                isFontName = YES;
                break;
            }
        }
        if (!isFontName) {
            SegmentItem *typeItem = [[SegmentItem alloc] init];
            typeItem.title = FSLocalizedForKey(@"kFont");
            typeItem.image = nil;
            typeItem.tag = TAB_FONT;
            typeItem.titleNormalColor = UIColor_DarkMode(UIColorHex(0x179cd8), ThemeTextColor_Dark);
            typeItem.titleSelectedColor = WhiteThemeTextColor;
            [self.segmentItems addObject:typeItem];
        }
        
        BOOL isOption = NO;
        for (SegmentItem *item in self.segmentItems) {
            if (item.tag == TAB_OPTION) {
                isOption = YES;
                break;
            }
        }
        if (!isOption) {
            SegmentItem *typeItem = [[SegmentItem alloc] init];
            typeItem.title = FSLocalizedForKey(@"kOptions");
            typeItem.image = nil;
            typeItem.tag = TAB_OPTION;
            typeItem.titleNormalColor = UIColor_DarkMode(UIColorHex(0x179cd8), ThemeTextColor_Dark);
            typeItem.titleSelectedColor = WhiteThemeTextColor;
            [self.segmentItems addObject:typeItem];
        }
    }
    default:
        break;
    }
    int mainHeight = 0;
    int fillHeight = 0;
    int typeHeight = TABHEIGHT;
    if (self.currentColorLayout && !self.currentCoverTextColorLayout) {
        fillHeight += self.currentColorLayout.layoutHeight;
    }
    if (self.currentOpacityLayout) {
        fillHeight += self.currentOpacityLayout.layoutHeight;
    }
    if (self.currentRotationLayout) {
        fillHeight += self.currentRotationLayout.layoutHeight;
    }
    if (self.currentLineWidthLayout) {
        fillHeight += self.currentLineWidthLayout.layoutHeight;
    }
    if (self.currentFontLayout) {
        fillHeight += self.currentFontLayout.layoutHeight;
    }
    if (self.currentdistanceUnitLayout) {
        fillHeight += self.currentdistanceUnitLayout.layoutHeight;
    }
    if (self.currentTextInputLayout) {
        fillHeight += self.currentTextInputLayout.layoutHeight;
    }
    if (self.currentIconLayout) {
        typeHeight += self.currentIconLayout.layoutHeight;
        fillHeight += TABHEIGHT;
    }
    if (self.currentCoverTextColorLayout) {
        typeHeight = TABHEIGHT + self.currentColorLayout.layoutHeight;
        fillHeight += self.currentCoverTextColorLayout.layoutHeight + TABHEIGHT;
    }
    if (self.currentFormListLayout) {
        typeHeight = TABHEIGHT + self.currentFormListLayout.layoutHeight;
        fillHeight += TABHEIGHT;
    }
    mainHeight = MAX(fillHeight, typeHeight);
    CGRect mainRect = self.frame;
    mainRect.size.height = mainHeight;
    self.frame = mainRect;

    if (self.currentIconLayout) {
        CGRect typeFrame = self.currentIconLayout.frame;
        typeFrame.origin.y = TABHEIGHT;
        typeFrame.size.height = mainHeight - TABHEIGHT;
        self.currentIconLayout.frame = typeFrame;
        typeFrame.origin.y = 0;
        typeFrame.size.height = mainHeight - TABHEIGHT;
        self.currentIconLayout.tableView.frame = typeFrame;

        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = TABHEIGHT;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];

        CGRect opacityFrame = self.currentOpacityLayout.frame;
        opacityFrame.origin.y = TABHEIGHT + colorFrame.size.height;
        self.currentOpacityLayout.frame = opacityFrame;
    }else if (self.currentCoverTextColorLayout){
        CGRect typeFrame = self.currentColorLayout.frame;
        typeFrame.origin.y = TABHEIGHT;
        typeFrame.size.height = mainHeight - TABHEIGHT;
        self.currentColorLayout.frame = typeFrame;
        
        CGRect textInputFrame = self.currentTextInputLayout.frame;
        textInputFrame.origin.y = TABHEIGHT;
        self.currentTextInputLayout.frame = textInputFrame;
        
        CGRect fontFrame = self.currentFontLayout.frame;
        fontFrame.origin.y = CGRectGetMaxY(self.currentTextInputLayout.frame);
        self.currentFontLayout.frame = fontFrame;
        self.currentFontLayout.mainLayoutHeight = mainHeight;
        
        CGRect colorFrame = self.currentCoverTextColorLayout.frame;
        colorFrame.origin.y = CGRectGetMaxY(self.currentFontLayout.frame);;
        self.currentCoverTextColorLayout.frame = colorFrame;
    }else if (self.currentFormListLayout){
        CGRect fontFrame = self.currentFontLayout.frame;
        fontFrame.origin.y = TABHEIGHT;
        self.currentFontLayout.frame = fontFrame;
        self.currentFontLayout.mainLayoutHeight += TABHEIGHT;
        [self.currentFontLayout addDivideView];

        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = fontFrame.size.height + TABHEIGHT;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];
        
        CGRect formLsitFrame = self.currentFormListLayout.frame;
        formLsitFrame.origin.y = TABHEIGHT;
        self.currentFormListLayout.frame = formLsitFrame;
    }
     else {
        [self resetAllLayout];
        if (self.currentFontLayout) {
            self.currentFontLayout.mainLayoutHeight = mainHeight;
        }
        if (self.currentdistanceUnitLayout) {
            self.currentdistanceUnitLayout.mainLayoutHeight = mainHeight;
        }
    }
}

- (void)removeAllLayout{
    [self.currentCoverTextColorLayout removeFromSuperview];
    self.currentCoverTextColorLayout = nil;
    [self.currentColorLayout removeFromSuperview];
    self.currentColorLayout = nil;
    [self.currentOpacityLayout removeFromSuperview];
    self.currentOpacityLayout = nil;
    [self.currentLineWidthLayout removeFromSuperview];
    self.currentLineWidthLayout = nil;
    [self.currentFontLayout removeFromSuperview];
    self.currentFontLayout = nil;
    [self.currentIconLayout removeFromSuperview];
    self.currentIconLayout = nil;
    [self.currentdistanceUnitLayout removeFromSuperview];
    self.currentdistanceUnitLayout = nil;
    [self.currentRotationLayout removeFromSuperview];
    self.currentRotationLayout = nil;
    [self.currentTextInputLayout removeFromSuperview];
    self.currentTextInputLayout = nil;
    [self.currentFormListLayout removeFromSuperview];
    self.currentFormListLayout = nil;
}

- (void)resetAllLayout {
    if (self.currentColorLayout && self.currentOpacityLayout && !self.currentLineWidthLayout && !self.currentFontLayout && !self.currentdistanceUnitLayout) //markup
    {
        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = 0;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];

        CGRect opacityFrame = self.currentOpacityLayout.frame;
        opacityFrame.origin.y = colorFrame.size.height;
        self.currentOpacityLayout.frame = opacityFrame;
    } else if (self.currentColorLayout && self.currentOpacityLayout && self.currentLineWidthLayout && !self.currentFontLayout  && !self.currentdistanceUnitLayout) //line shape pencil
    {
        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = 0;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];

        CGRect linewidthFrame = self.currentLineWidthLayout.frame;
        linewidthFrame.origin.y = colorFrame.size.height;
        self.currentLineWidthLayout.frame = linewidthFrame;
        [self.currentLineWidthLayout addDivideView];

        CGRect opacityFrame = self.currentOpacityLayout.frame;
        opacityFrame.origin.y = colorFrame.size.height + linewidthFrame.size.height;
        self.currentOpacityLayout.frame = opacityFrame;
    } else if (self.currentColorLayout && self.currentOpacityLayout && !self.currentLineWidthLayout && self.currentFontLayout) //freetext
    {
        CGRect fontFrame = self.currentFontLayout.frame;
        fontFrame.origin.y = 0;
        self.currentFontLayout.frame = fontFrame;
        [self.currentFontLayout addDivideView];

        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = fontFrame.size.height;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];

        CGRect opacityFrame = self.currentOpacityLayout.frame;
        opacityFrame.origin.y = fontFrame.size.height + colorFrame.size.height;
        self.currentOpacityLayout.frame = opacityFrame;
    } else if (!self.currentColorLayout && !self.currentOpacityLayout && !self.currentFontLayout && self.currentLineWidthLayout) //erase
    {
        CGRect linewidthFrame = self.currentLineWidthLayout.frame;
        linewidthFrame.origin.y = 0;
        self.currentLineWidthLayout.frame = linewidthFrame;
    } else if (self.currentColorLayout && !self.currentOpacityLayout && self.currentLineWidthLayout && !self.currentFontLayout) //sigature
    {
        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = 0;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];

        CGRect linewidthFrame = self.currentLineWidthLayout.frame;
        linewidthFrame.origin.y = colorFrame.size.height;
        self.currentLineWidthLayout.frame = linewidthFrame;
    } else if (self.currentOpacityLayout && self.currentRotationLayout) { // image
        CGRect rotationFrame = self.currentRotationLayout.frame;
        rotationFrame.origin.y = 0;
        self.currentRotationLayout.frame = rotationFrame;
        [self.currentRotationLayout addDivideView];

        CGRect opacityFrame = self.currentOpacityLayout.frame;
        opacityFrame.origin.y = rotationFrame.size.height;
        self.currentOpacityLayout.frame = opacityFrame;
    } else if (self.currentColorLayout && !self.currentOpacityLayout && !self.currentLineWidthLayout && !self.currentFontLayout && !self.currentCoverTextColorLayout && !self.currentTextInputLayout) {
        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = 0;
        self.currentColorLayout.frame = colorFrame;
    }
    else if (self.currentLineWidthLayout && self.currentColorLayout && self.currentOpacityLayout && self.currentdistanceUnitLayout){
        CGRect distanceFrame = self.currentdistanceUnitLayout.frame;
        distanceFrame.origin.y = 0;
        self.currentdistanceUnitLayout.frame = distanceFrame;
        [self.currentdistanceUnitLayout addDivideView];
        
        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = distanceFrame.size.height;
        self.currentColorLayout.frame = colorFrame;
        [self.currentColorLayout addDivideView];

        CGRect lineFrame = self.currentLineWidthLayout.frame;
        lineFrame.origin.y = colorFrame.size.height + distanceFrame.size.height;
        self.currentLineWidthLayout.frame = lineFrame;
        [self.currentLineWidthLayout addDivideView];
        
        CGRect opacityFrame = self.currentOpacityLayout.frame;
        opacityFrame.origin.y = lineFrame.size.height + colorFrame.size.height + distanceFrame.size.height;
        self.currentOpacityLayout.frame = opacityFrame;
    }
    else if (self.currentColorLayout && !self.currentOpacityLayout && !self.currentLineWidthLayout && self.currentFontLayout) //fieldText
    {
        CGRect fontFrame = self.currentFontLayout.frame;
        fontFrame.origin.y = 0;
        self.currentFontLayout.frame = fontFrame;
        [self.currentFontLayout addDivideView];
        
        CGRect colorFrame = self.currentColorLayout.frame;
        colorFrame.origin.y = fontFrame.size.height;
        self.currentColorLayout.frame = colorFrame;
    }
    else if (self.currentTextInputLayout && self.currentColorLayout)
       {
           CGRect textFrame = self.currentTextInputLayout.frame;
           textFrame.origin.y = 0;
           self.currentTextInputLayout.frame = textFrame;
           [self.currentTextInputLayout addDivideView];
           
           CGRect colorFrame = self.currentColorLayout.frame;
           colorFrame.origin.y = textFrame.size.height;
           self.currentColorLayout.frame = colorFrame;
       }
}

#pragma mark SegmentDeletate
- (void)itemClickWithItem:(SegmentItem *)item {
    [self showTab:(Property_TabType)item.tag];
}

@end
