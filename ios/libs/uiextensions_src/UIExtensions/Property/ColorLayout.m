/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */


#import "Const.h"
#import "PropertyBar.h"
#import "Utility.h"
#import "ColorBox.h"

#define PAGEBTNWIDTH 15
#define PAGEBTNHEIGH 9

@interface ColorLayout ()<UIScrollViewDelegate>

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) UIScrollView *contentView;
@property (nonatomic, strong) UIView *colorItemContentView;
@property (nonatomic, strong) UIView *colorBoxContentView;
@property (nonatomic, strong) UIView *pageControlView;
@property (nonatomic, strong) ColorBox *colorBoxView;
@property (nonatomic, strong) UIView *currentColorBoxColor;
@property (nonatomic, strong) UIButton *leftPageBtn;
@property (nonatomic, strong) UIButton *rightPageBtn;
@property (nonatomic, assign) uint32_t currentColor;
@property (nonatomic, weak) id<IPropertyValueChangedListener> currentListener;

@end

@implementation ColorLayout {
    PropertyBar *_propertyBar;
}

- (instancetype)initWithFrame:(CGRect)frame propertyBar:(PropertyBar *)propertyBar {
    self = [super initWithFrame:frame];
    if (self) {
        _propertyBar = propertyBar;
//        self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, frame.size.width, LAYOUTTITLEHEIGHT)];
//        self.title.text = FSLocalizedForKey(@"kColor");
//        self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
//        self.title.font = [UIFont systemFontOfSize:11.0f];
//        self.items = [NSMutableArray array];
//        [self addSubview:self.title];
        self.contentView = [[UIScrollView alloc] initWithFrame:frame];
        self.contentView.showsHorizontalScrollIndicator = NO;
        self.contentView.contentSize = CGSizeMake(frame.size.width * 2.0, frame.size.height);
        self.contentView.pagingEnabled = YES;
        self.contentView.bounces = NO;
        self.contentView.delegate = self;
        [self addSubview:self.contentView];
        
        self.colorItemContentView = [[UIView alloc] initWithFrame:frame];
        [self.contentView addSubview:self.colorItemContentView];
        
        self.colorBoxContentView = [[UIView alloc] initWithFrame:CGRectMake(frame.size.width, 0, frame.size.width, frame.size.height)];
        [self.contentView addSubview:self.colorBoxContentView];
        
        self.colorBoxView = [[ColorBox alloc] initWithFrame:self.frame];
        [self.colorBoxContentView addSubview:self.colorBoxView];
        
        self.currentColorBoxColor = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - 80, LAYOUTTITLEHEIGHT + TOP_MARGIN, 50, 100)];
        [self.colorBoxContentView addSubview:self.currentColorBoxColor];
        self.currentColorBoxColor.isAccessibilityElement = YES;
        [self.currentColorBoxColor setAccessibilityLabel:FSLocalizedForKey(@"kCustomColor")];
        
        self.pageControlView = [[UIView alloc] init];
        self.pageControlView.frame = CGRectMake((frame.size.width - PAGEBTNWIDTH * 2 - 5) * 0.5, frame.size.height - PAGEBTNHEIGH, PAGEBTNWIDTH * 2 + 5, PAGEBTNHEIGH);
        [self addSubview:self.pageControlView];
        
        self.leftPageBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, PAGEBTNWIDTH, PAGEBTNHEIGH)];
        [self.leftPageBtn setImage:GetImage(@"property_colorPageNormal") forState:UIControlStateNormal];
        [self.leftPageBtn setImage:GetImage(@"property_colorPageSelected") forState:UIControlStateSelected];
        [self.leftPageBtn addTarget:self action:@selector(changeColorPage:) forControlEvents:UIControlEventTouchUpInside];
        self.leftPageBtn.selected = YES;
        [self.pageControlView addSubview:self.leftPageBtn];
        
        self.rightPageBtn = [[UIButton alloc] initWithFrame:CGRectMake(PAGEBTNWIDTH + 5, 0, PAGEBTNWIDTH, PAGEBTNHEIGH)];
        [self.rightPageBtn setImage:GetImage(@"property_colorPageNormal") forState:UIControlStateNormal];
        [self.rightPageBtn setImage:GetImage(@"property_colorPageSelected") forState:UIControlStateSelected];
        [self.rightPageBtn addTarget:self action:@selector(changeColorPage:) forControlEvents:UIControlEventTouchUpInside];
        [self.pageControlView addSubview:self.rightPageBtn];
        
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, frame.size.width, LAYOUTTITLEHEIGHT)];
        self.title.text = FSLocalizedForKey(@"kColor");
        self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.title.font = [UIFont systemFontOfSize:11.0f];
        self.items = [NSMutableArray array];
        [self addSubview:self.title];
    }
    return self;
}

- (long)supportProperty {
    return PROPERTY_COLOR;
}

- (void)setColors:(NSArray *)array {
    _colors = array;
    [self addColorItem];
}

- (void)setCurrentColor:(uint32_t)color {
    _currentColor = color;
    if (_propertyBar.lineWidthLayout) {
        [_propertyBar.lineWidthLayout setCurrentColor:color];
    }
    
    self.currentColorBoxColor.backgroundColor = [UIColor colorWithRGB:color];
    
    BOOL itemSelected = NO;
    for (ColorItem *item in self.items) {
        if (item.color == color) {
            [item setSelected:YES];
            itemSelected = YES;
        } else {
            [item setSelected:NO];
        }
    }
    
    if (!itemSelected && !self.rightPageBtn.selected) {
        [self changeColorPage:self.rightPageBtn];
    }
}

- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener {
    _currentListener = currentListener;
}

- (void)addColorItem {
    CGRect layoutFrame = self.frame;

    int totalColumns = 6;
    int totalRows = 3;

    CGFloat horMargin = 28.f;
    CGFloat verMargin = 15;
    CGFloat margin = 25.f;
    CGFloat topMargin = 15.f;
    
    CGFloat topPageMargin = 12.f;
    CGFloat bottomPageMargin = 11.f;
    
    /*
     ||topMargin||||||||||||||||||||||||||||||||||||||
     |           -----               ----- |||||||||||
     |<-margin->|     |<-horMargin->|     |<-margin->|
     |           -----               ----- |||||||||||
     |||||||||||verMargin|||||||||||verMargin|||||||||
     |           -----               -----
     |<-margin->|     |<-horMargin->|     |<-margin->|
     ||||||||||| ----- ||||||||||||| ----- |||||||||||
     |||                topPageMargin             ||||
     ||bottomMargin|  |||PAGEBTNHEIGH||       ||||||||
     |||||||||||||||  ||bottomPageMargin||    ||||||||
     */
    
    if (([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) && DEVICE_iPHONE) {
        
        totalColumns = 9;
        totalRows = 2;
        
        horMargin = 40.f;
        verMargin = 13.f;
        margin = 34.f;
        topMargin = 12.f;
        
        topPageMargin = 10.f;
        bottomPageMargin = 6.f;
    }else if (DEVICE_iPAD){
        
        horMargin = 15.f;
        verMargin = 9.f;
        margin = 20.f;
        topMargin = 12.f;
        
        topPageMargin = 12.f;
        bottomPageMargin = 8.f;
    }
    
    CGFloat bottomMargin = topPageMargin + bottomPageMargin  + PAGEBTNHEIGH;
    
    int itemW = (layoutFrame.size.width - (totalColumns - 1) * horMargin - margin * 2) / totalColumns;
    int itemH = itemW;
    
    CGFloat totalItemsH = itemH * totalRows + verMargin * (totalRows - 1);
    self.layoutHeight = totalItemsH + LAYOUTTITLEHEIGHT + topMargin + bottomMargin;
    
    for (int i = 0; i < _colors.count; i++) {
        
        int row = i / totalColumns;
        int col = i % totalColumns;
        CGFloat itemX = margin + col * (itemW + horMargin);
        CGFloat itemY = row * (itemH + verMargin) + LAYOUTTITLEHEIGHT + topMargin;
        
        NSNumber *color = [_colors objectAtIndex:i];
        CGRect itemFrame = CGRectMake(itemX, itemY, itemW, itemH);
        ColorItem *item = [[ColorItem alloc] initWithFrame:itemFrame];
        item.color = color.intValue;
        [item setSelected:NO];
        item.callback = ^(long property, int value) {
            [self->_currentListener onProperty:property changedFrom:@(self->_currentColor) to:@(value)];
            [self setCurrentColor:value];
            self.currentColorBoxColor.backgroundColor = [UIColor colorWithRGB:value];
        };
        [self.colorItemContentView addSubview:item];
        [self.items addObject:item];
    }
    
    self.contentView.frame = CGRectMake(0.0, 0.0, layoutFrame.size.width, self.layoutHeight);
    self.contentView.contentSize = CGSizeMake(layoutFrame.size.width * 2.0, self.layoutHeight);
    self.colorItemContentView.frame = CGRectMake(0.0, 0.0, layoutFrame.size.width, self.layoutHeight);
    self.colorBoxContentView.frame = CGRectMake(layoutFrame.size.width, 0.0, layoutFrame.size.width, self.layoutHeight);
    self.pageControlView.frame = CGRectMake((layoutFrame.size.width - PAGEBTNWIDTH * 2 - 5) * 0.5, self.layoutHeight - PAGEBTNHEIGH - 10, PAGEBTNWIDTH * 2 + 5, PAGEBTNHEIGH);
    
    self.colorBoxView.frame = CGRectMake(margin, LAYOUTTITLEHEIGHT + topMargin, layoutFrame.size.width - margin - 50.0, totalItemsH);
    self.currentColorBoxColor.frame = CGRectMake(CGRectGetMaxX(self.colorBoxView.frame) + 10, CGRectGetMinY(self.colorBoxView.frame), 30, CGRectGetHeight(self.colorBoxView.frame));
    
    if (self.contentView.contentOffset.x > 0) {
        self.contentView.contentOffset = CGPointMake(layoutFrame.size.width, 0);
    }
    else
    {
        self.contentView.contentOffset = CGPointMake(0, 0);
    }
    __weak typeof(self) weakSelf = self;
    self.colorBoxView.CallBackInt = ^(long property, UInt32 value){
        weakSelf.currentColorBoxColor.backgroundColor = [UIColor colorWithRGB:value];
        [weakSelf.currentListener onProperty:property changedFrom:@(weakSelf.currentColor) to:@(value)];
//        ColorItem *item = [weakSelf.items firstObject];
//        [item setColor:value];
        [weakSelf setCurrentColor:value];
    };
}

- (void)lockProperty:(BOOL)lock{
    for (ColorItem *item in self.items) {
        [item setDisabled:lock];
    }
    self.colorBoxView.userInteractionEnabled = !lock;
    self.alpha = !lock?:ALPHA_LOCKED;
}

-(void)addDivideView
{
    for (UIView *view in self.subviews) {
        if (view.tag == 1000) {
            [view removeFromSuperview];
        }
    }
    UIView *divide = [[UIView alloc] initWithFrame:CGRectMake(20, self.frame.size.height - 1, self.frame.size.width - 40, [Utility realPX:1.0f])];
    divide.tag = 1000;
    divide.backgroundColor = [UIColor colorWithRGB:0x5c5c5c];
    divide.alpha = 0.2f;
    [self addSubview:divide];
    
}

- (void)resetLayout
{
    for (UIView *view in self.colorItemContentView.subviews) {
        [view removeFromSuperview];
    }
    [self.items removeAllObjects];
    self.title.frame = CGRectMake(20, 3, self.frame.size.width, LAYOUTTITLEHEIGHT);
    [self addColorItem];
    [self setCurrentColor:_currentColor];
}

- (void)changeColorPage:(UIButton *)sender
{
    if (self.contentView.contentOffset.x > 0) {
        if (sender == self.leftPageBtn) {
            self.leftPageBtn.selected = YES;
            self.rightPageBtn.selected = NO;
            self.contentView.contentOffset = CGPointMake(0, 0);
        }
    }else
    {
        if (sender == self.rightPageBtn) {
            self.rightPageBtn.selected = YES;
            self.leftPageBtn.selected = NO;
            self.contentView.contentOffset = CGPointMake(self.frame.size.width, 0);
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > scrollView.frame.size.width * 0.5) {
        self.rightPageBtn.selected = YES;
        self.leftPageBtn.selected = NO;
    }else
    {
        self.leftPageBtn.selected = YES;
        self.rightPageBtn.selected = NO;
    }
}

@end
