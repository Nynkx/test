/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <Foundation/Foundation.h>
#import "FSBaseViews.h"

@protocol IPropertyValueChangedListener;

@interface IconLayout : FSCellView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) float layoutHeight;
@property (nonatomic, strong) UITableView *tableView;

- (instancetype)initWithFrame:(CGRect)frame iconType:(long)iconType;
- (long)supportProperty;
- (void)setCurrentIconType:(int)type;
- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener;
- (void)addDivideView;
- (void)resetLayout;
- (void)lockProperty:(BOOL)lock;
@end
