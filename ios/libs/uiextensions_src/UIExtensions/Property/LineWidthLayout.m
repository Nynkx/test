/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "LineWidthLayout.h"

#import "PropertyBar.h"
#import "Utility.h"

@interface LineWidthLayout ()

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, assign) int currentColor;
@property (nonatomic, assign) int currentLineWidth;
@property (nonatomic, weak) id<IPropertyValueChangedListener> currentListener;

@property (nonatomic, strong) UIImageView *circleView;
@property (nonatomic, strong) UILabel *numberView;
@property (nonatomic, strong) UISlider *silder;

@end

@implementation LineWidthLayout

- (instancetype)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, frame.size.width, LAYOUTTITLEHEIGHT)];
        self.title.text = FSLocalizedForKey(@"kThickness");
        self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.title.font = [UIFont systemFontOfSize:11.0f];
        [self addSubview:self.title];

        self.circleView = [[UIImageView alloc] initWithFrame:CGRectMake(20, LAYOUTTITLEHEIGHT + TOP_MARGIN, 32, 32)];
        self.circleView.layer.cornerRadius = 16.f;
        [self addSubview:self.circleView];

        self.numberView = [[UILabel alloc] initWithFrame:CGRectMake(70, LAYOUTTITLEHEIGHT + TOP_MARGIN + 6, 50, 20)];
        self.numberView.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.numberView.font = [UIFont systemFontOfSize:15];
        [self addSubview:self.numberView];

        self.silder = [[UISlider alloc] initWithFrame:CGRectMake(120, LAYOUTTITLEHEIGHT + TOP_MARGIN + 6, frame.size.width - 140, 20)];
        [self.silder setThumbImage:[UIImage imageNamed:@"property_linewidth_slider.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.silder setThumbImage:[UIImage imageNamed:@"property_linewidth_slider.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
        self.silder.minimumValue = 1.0f;
        self.silder.maximumValue = 12.0f;

        [self.silder addTarget:self action:@selector(sliderChangedValue) forControlEvents:UIControlEventValueChanged];
        self.layoutHeight = LAYOUTTITLEHEIGHT + LAYOUTTBSPACE + TOP_MARGIN + 30;
        [self addSubview:self.silder];
    }
    return self;
}

- (long)supportProperty {
    return PROPERTY_LINEWIDTH;
}

- (void)setCurrentColor:(int)color {
    _currentColor = color;
    self.circleView.backgroundColor = [UIColor colorWithRGB:color];
}

- (void)setCurrentLineWidth:(int)lineWidth {
    _currentLineWidth = lineWidth;
    [self.silder setValue:lineWidth animated:NO];
    self.numberView.text = [NSString stringWithFormat:@"%d %@", lineWidth, @"pt"];
    self.circleView.backgroundColor = [UIColor colorWithRGB:_currentColor];
    int circleWidth = self.silder.value * 16 / 12;
    self.circleView.layer.cornerRadius = circleWidth;
    self.circleView.frame = CGRectMake(20 + 16 - circleWidth, LAYOUTTITLEHEIGHT + TOP_MARGIN + 16 - circleWidth, circleWidth * 2, circleWidth * 2);
}

- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener {
    _currentListener = currentListener;
}

- (void)sliderChangedValue {
    self.numberView.text = [NSString stringWithFormat:@"%d %@", (int) self.silder.value, @"pt"];
    self.circleView.backgroundColor = [UIColor colorWithRGB:_currentColor];
    int circleWidth = self.silder.value * 16 / 12;
    self.circleView.layer.cornerRadius = circleWidth;
    self.circleView.frame = CGRectMake(20 + 16 - circleWidth, LAYOUTTITLEHEIGHT + TOP_MARGIN + 16 - circleWidth, circleWidth * 2, circleWidth * 2);
    [self.currentListener onProperty:PROPERTY_LINEWIDTH changedFrom:@(_currentLineWidth) to:@(self.silder.value)];
    _currentLineWidth = self.silder.value;
}

- (void)addDivideView {
    for (UIView *view in self.subviews) {
        if (view.tag == 1000) {
            [view removeFromSuperview];
        }
    }
    UIView *divide = [[UIView alloc] initWithFrame:CGRectMake(20, self.frame.size.height - 1, self.frame.size.width - 40, [Utility realPX:1.0f])];
    divide.tag = 1000;
    divide.backgroundColor = [UIColor colorWithRGB:0x5c5c5c];
    divide.alpha = 0.2f;
    [self addSubview:divide];
}

- (void)resetLayout {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }

    self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, self.frame.size.width, LAYOUTTITLEHEIGHT)];
    self.title.text = FSLocalizedForKey(@"kThickness");
    self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
    self.title.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:self.title];

    self.circleView = [[UIImageView alloc] initWithFrame:CGRectMake(20, LAYOUTTITLEHEIGHT + TOP_MARGIN, 32, 32)];
    self.circleView.layer.cornerRadius = 16.f;
    [self addSubview:self.circleView];

    self.numberView = [[UILabel alloc] initWithFrame:CGRectMake(70, LAYOUTTITLEHEIGHT + TOP_MARGIN + 6, 50, 20)];
    self.numberView.textColor = [UIColor colorWithRGB:0x5c5c5c];
    self.numberView.font = [UIFont systemFontOfSize:15];
    [self addSubview:self.numberView];

    self.silder = [[UISlider alloc] initWithFrame:CGRectMake(120, LAYOUTTITLEHEIGHT + TOP_MARGIN + 6, self.frame.size.width - 140, 20)];
    [self.silder setThumbImage:[UIImage imageNamed:@"property_linewidth_slider.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.silder setThumbImage:[UIImage imageNamed:@"property_linewidth_slider.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
    self.silder.minimumValue = 1.0f;
    self.silder.maximumValue = 12.0f;

    [self.silder addTarget:self action:@selector(sliderChangedValue) forControlEvents:UIControlEventValueChanged];
    self.layoutHeight = LAYOUTTITLEHEIGHT + LAYOUTTBSPACE + TOP_MARGIN + 30;
    [self addSubview:self.silder];
    [self setCurrentColor:_currentColor];
    [self setCurrentLineWidth:_currentLineWidth];
}

- (void)lockProperty:(BOOL)lock{
    self.silder.enabled = !lock;
    self.alpha = !lock?:ALPHA_LOCKED;
}

@end
