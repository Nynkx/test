/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FormListLayount.h"
#import "PropertyBar.h"
#import "FSViewController.h"
#import "FSSelectTableViewCell.h"

typedef void(^DidClickedDone)(NSArray *choiceOptions, BOOL isUpdate);
@interface FSAddFieldVC : FSViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) IBOutlet FSTableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *addChoiceOption;
@property (nonatomic, copy) NSArray *dataSource;
@property (nonatomic, assign) BOOL multiSelect;
@property (nonatomic, weak) FSChoiceOption *currentOption;
@property (nonatomic, strong) NSIndexPath *currentIndexPath;
@property (nonatomic, assign) BOOL isUpdate;
@property (nonatomic, copy) DidClickedDone didClickedDone;
@property (nonatomic, weak) id<IRotationEventListener>delegate;
@property (nonatomic, assign) UIInterfaceOrientation formListLayountPreferredInterfaceOrientation;
- (void)setCurrentWidget:(FSWidget *)widget;
@end

@interface FormListLayount ()
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *switchTitle;
@property (nonatomic, strong) FSSwitch *optionSwitch;
@property (nonatomic, weak) id<IPropertyValueChangedListener> currentListener;
@property (nonatomic, strong) FSWidget *currentWidget;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataSource;

@property (nonatomic, assign) UIInterfaceOrientation preferredInterfaceOrientationForPresentation;
@end
@implementation FormListLayount

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, CGRectGetWidth(frame), LAYOUTTITLEHEIGHT)];
        self.title.text = FSLocalizedForKey(@"kItemList");
        self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.title.font = [UIFont systemFontOfSize:11.0f];
        [self addSubview:self.title];
        
        self.tableView = [[FSTableView alloc] initWithFrame:CGRectMake(20, self.title.fs_bottom, CGRectGetWidth(frame) - 40, 200)];
        self.tableView.layer.masksToBounds = YES;
        self.tableView.layer.cornerRadius = 3.f;
        self.tableView.layer.borderColor =  [UIColor colorWithRGB:0x5c5c5c].CGColor;
        self.tableView.layer.borderWidth = 0.5;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self addSubview:self.tableView];
        
        self.optionSwitch = [[FSSwitch alloc] initWithFrame:CGRectZero];
        self.optionSwitch.fs_right = CGRectGetWidth(frame) - 20;
        self.optionSwitch.fs_top = self.tableView.fs_bottom + 1 ;
        [self.optionSwitch addTarget:self action:@selector(changeOption:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.optionSwitch];
        
        self.switchTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, self.tableView.fs_bottom,  CGRectGetWidth(frame) - 50 - 55, LAYOUTTITLEHEIGHT)];
        self.switchTitle.fs_centerY = self.optionSwitch.fs_centerY;
        self.switchTitle.text = FSLocalizedForKey(@"kCustomText");
        self.switchTitle.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.switchTitle.font = [UIFont systemFontOfSize:11.0f];
        [self addSubview:self.switchTitle];
        
        self.layoutHeight = self.optionSwitch.fs_bottom;
        
        self.preferredInterfaceOrientationForPresentation = [UIApplication sharedApplication].statusBarOrientation;
    }
    return self;
}

- (void)dealloc{
    self.currentWidget = nil;
}

- (long)supportProperty{
     return PROPERTY_FORMOPTION;
}

- (void)setCurrentWidget:(FSWidget *)widget{
    _currentWidget = widget;
    FSField *field = [widget getField];
    if ([field getType] == FSFieldTypeComboBox) {
        self.optionSwitch.on = ((field.flags & FSFieldFlagComboEdit) == FSFieldFlagComboEdit);
        self.switchTitle.text = FSLocalizedForKey(@"kCustomText");
    }else if ([field getType] == FSFieldTypeListBox){
        self.optionSwitch.on = ((field.flags & FSFieldFlagChoiceMultiSelect) == FSFieldFlagChoiceMultiSelect);
        self.switchTitle.text = FSLocalizedForKey(@"kMultipleSelection");
    }
    [self loadData];
}

- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener {
    _currentListener = currentListener;
}

- (void)addDivideView {
    for (UIView *view in self.subviews) {
        if (view.tag == 1000) {
            [view removeFromSuperview];
        }
    }
    UIView *divide = [[UIView alloc] initWithFrame:CGRectMake(20, self.frame.size.height - 1, self.frame.size.width - 40, [Utility realPX:1.0f])];
    divide.tag = 1000;
    divide.backgroundColor = [UIColor colorWithRGB:0x5c5c5c];
    divide.alpha = 0.2f;
    [self addSubview:divide];
}

- (void)changeOption:(FSSwitch *)sender{
    [_currentListener onProperty:PROPERTY_FORMOPTION changedFrom:@(!sender.on) to:@(sender.on)];
    if ([self.switchTitle.text isEqualToString:FSLocalizedForKey(@"kMultipleSelection")]) {
        BOOL isSelect = NO;
        int selectedIndex = -1;
        for (int i = 0; i < self.dataSource.count; i++) {
            FSChoiceOption *choiceOption = self.dataSource[i];
            if (choiceOption.selected) {
                if (selectedIndex < 0) {
                    isSelect = YES;
                    selectedIndex = i;
                }
                choiceOption.selected = NO;
            }
        }
        if (isSelect) {
            FSChoiceOption *choiceOption = self.dataSource[selectedIndex];
            choiceOption.selected = YES;
            FSField *field = [self.currentWidget getField];
            FSChoiceOptionArray *choiceOptions = [field getOptions];
            FSChoiceOptionArray *newChoiceOptions = [[FSChoiceOptionArray alloc] init];
            for (FSChoiceOption *choiceOption in self.dataSource) {
                [newChoiceOptions add:choiceOption];
            }
            [self.currentListener onProperty:PROPERTY_FORMOPTION changedFrom:[NSValue valueWithNonretainedObject:choiceOptions] to:[NSValue valueWithNonretainedObject:newChoiceOptions]];
        }
    }
    [self.tableView reloadData];
}

- (void)resetLayout{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, CGRectGetWidth(self.frame), LAYOUTTITLEHEIGHT)];
    self.title.text = FSLocalizedForKey(@"kItemList");
    self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
    self.title.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:self.title];
    
    self.tableView = [[FSTableView alloc] initWithFrame:CGRectMake(20, self.title.fs_bottom, CGRectGetWidth(self.frame) - 40, 200)];
    self.tableView.layer.masksToBounds = YES;
    self.tableView.layer.cornerRadius = 3.f;
    self.tableView.layer.borderColor =  [UIColor colorWithRGB:0x5c5c5c].CGColor;
    self.tableView.layer.borderWidth = 0.5;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubview:self.tableView];
    
    self.optionSwitch.fs_right = CGRectGetWidth(self.frame) - 20;
    self.optionSwitch.fs_top = self.tableView.fs_bottom + 1;
    [self addSubview:self.optionSwitch];
    
    self.switchTitle.frame = CGRectMake(20, self.tableView.fs_bottom,  CGRectGetWidth(self.frame) - 50 - 55, LAYOUTTITLEHEIGHT);
    self.switchTitle.fs_centerY = self.optionSwitch.fs_centerY;
    [self addSubview:self.switchTitle];
    
    self.layoutHeight = self.optionSwitch.fs_bottom;
}

- (void)lockProperty:(BOOL)lock{
    self.userInteractionEnabled = !lock;
    self.alpha = !lock?:ALPHA_LOCKED;
}

- (void)loadData{
    if ([_currentWidget isEmpty] || !_currentWidget) {
        return;
    }
    FSField *field = [_currentWidget getField];
    NSMutableArray *dataSource = @[].mutableCopy;
    FSChoiceOptionArray *choiceOptions = [field getOptions];
    for (int i = 0; i < [choiceOptions getSize]; i++) {
        FSChoiceOption *choiceOption = [choiceOptions getAt:i];
        [dataSource addObject:choiceOption];
    }
    self.dataSource = dataSource.copy;
}

- (void)openItemListVC{
    FSAddFieldVC *VC = [self fs_storyBoardViewControllerWithName:@"CommonView" identifier:@"FSAddFieldVC"];
    VC.formListLayountPreferredInterfaceOrientation = self.preferredInterfaceOrientationForPresentation;
    VC.delegate = self.delegate;
    @weakify(self)
    VC.didClickedDone = ^(NSArray *choiceOptionsArr, BOOL isUpdate) {
        @strongify(self)
        if (isUpdate) {
            self.dataSource = choiceOptionsArr.copy;
            [self.tableView reloadData];
            FSField *field = [self.currentWidget getField];
            FSChoiceOptionArray *choiceOptions = [field getOptions];
            FSChoiceOptionArray *newChoiceOptions = [[FSChoiceOptionArray alloc] init];
            for (FSChoiceOption *choiceOption in choiceOptionsArr) {
                [newChoiceOptions add:choiceOption];
            }
            [self.currentListener onProperty:PROPERTY_FORMOPTION changedFrom:[NSValue valueWithNonretainedObject:choiceOptions] to:[NSValue valueWithNonretainedObject:newChoiceOptions]];
        }
    };
    [VC setCurrentWidget:self.currentWidget];
    FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:VC];
    [[Utility getTopMostViewController] presentViewController:nav animated:YES completion:nil];
}

#pragma mark -  table view delegate handler
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self openItemListVC];
}


#pragma mark -  table view datasource handler

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataSource.count == 0) {
        UIButton *messageBtn = [UIButton new];
        
        messageBtn.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        [messageBtn setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
        messageBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [messageBtn sizeToFit];
        [messageBtn setTitle:FSLocalizedForKey(@"kClickHereFormTip") forState:UIControlStateNormal];
        @weakify(self)
        [messageBtn setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            @strongify(self)
            [self openItemListVC];;
        }];
        
        tableView.backgroundView = messageBtn;
        return 0;
    }{
        tableView.backgroundView = nil;
    }
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"FSTableViewCellID";
    FSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[FSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    FSChoiceOption *choiceOption = self.dataSource[indexPath.row];
    cell.textLabel.text = choiceOption.option_label;
    if (choiceOption.selected) {
        cell.textLabel.textColor = ThemeLikeBlueColor;
    }else{
        cell.textLabel.textColor = BlackThemeTextColor;
    }
    return cell;
}

@end

@implementation FSAddFieldVC

- (void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationController.navigationBar.barTintColor = ThemeNavBarColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : WhiteThemeNavBarTextColor};
    self.title = FSLocalizedForKey(@"kItemList");
    UIBarButtonItem *cancel = self.navigationItem.leftBarButtonItem;
    [cancel setTitle:FSLocalizedForKey(@"kCancel")];
    [cancel setTintColor:WhiteThemeNavBarTextColor];
    UIBarButtonItem *done = self.navigationItem.rightBarButtonItem;
    [done setTintColor:WhiteThemeNavBarTextColor];
    [done setTitle:FSLocalizedForKey(@"kDone")];
    
    self.addChoiceOption.layer.masksToBounds = YES;
    self.addChoiceOption.layer.cornerRadius = 29.f;
}

- (IBAction)cancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(didRotateFromInterfaceOrientation:)] && self.formListLayountPreferredInterfaceOrientation != self.preferredInterfaceOrientationForPresentation) {
            [self.delegate didRotateFromInterfaceOrientation:self.preferredInterfaceOrientationForPresentation];
        }
    }];
}

- (IBAction)done:(id)sender{
    !self.didClickedDone?:self.didClickedDone(self.dataSource, self.isUpdate);
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(didRotateFromInterfaceOrientation:)] && self.formListLayountPreferredInterfaceOrientation != self.preferredInterfaceOrientationForPresentation) {
            [self.delegate didRotateFromInterfaceOrientation:self.preferredInterfaceOrientationForPresentation];
        }
    }];
}

- (IBAction)addChoice:(UIButton *)sender{
    NSMutableArray *dataSoucre = self.dataSource.mutableCopy;
    InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kAdd") message:nil buttonClickHandler:^(InputAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            if (self.multiSelect) {
                for (FSChoiceOption *choiceOption in dataSoucre) {
                    choiceOption.selected = NO;
                }
            }
            FSChoiceOption *choiceOption = [[FSChoiceOption alloc] initWithOption_value:alertView.inputTextField.text option_label:alertView.inputTextField.text selected:YES default_selected:NO];
            [dataSoucre addObject:choiceOption];
            if (!self.multiSelect) {
                self.currentOption.selected = NO;
            }
            self.dataSource = dataSoucre.copy;
            [self.tableView reloadData];
            self.isUpdate = YES;
        }
    } cancelButtonTitle:FSLocalizedForKey(@"kCancel") otherButtonTitles:FSLocalizedForKey(@"kOK"), nil];
    NSString *text = [NSString stringWithFormat:@"New Item-%@",[[Utility getUUID] substringToIndex:6]];
    inputAlertView.inputTextField.text = text;
    inputAlertView.style = TSAlertViewStyleInputText;
    inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
    inputAlertView.usesMessageTextView = NO;
    
    @weakify(self)
    UIButton *OKBtn = inputAlertView.buttons[1];
    [inputAlertView.inputTextField setBlockForControlEvents:UIControlEventEditingChanged block:^(UITextField * _Nonnull sender) {
        @strongify(self)
        if (!sender.text.length) {
            OKBtn.enabled = NO;
        }else{
            BOOL enabled = YES;
            for (FSChoiceOption *option in self.dataSource) {
                if ([option.option_label isEqualToString:sender.text]) {
                    enabled = NO;
                    break;
                }
            }
            OKBtn.enabled = enabled;
        }
    }];
    [inputAlertView show];
}

- (void)setCurrentWidget:(FSWidget *)widget{
    FSField *field = [widget getField];
    if ([field getType] == FSFieldTypeComboBox) {
        self.multiSelect = NO;
    }else if ([field getType] == FSFieldTypeListBox && (field.flags & FSFieldFlagChoiceMultiSelect) == FSFieldFlagChoiceMultiSelect){
        self.multiSelect = YES;
    }
    NSMutableArray *dataSource = @[].mutableCopy;
    FSChoiceOptionArray *choiceOptions = [field getOptions];
    for (int i = 0; i < [choiceOptions getSize]; i++) {
        FSChoiceOption *choiceOption = [choiceOptions getAt:i];
        [dataSource addObject:choiceOption];
    }
    self.dataSource = dataSource.copy;
}

#pragma mark -  table view delegate handler
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FSChoiceOption *choiceOption = self.dataSource[indexPath.row];
    if (![self.currentOption isEqual:choiceOption] && self.currentIndexPath) {
        self.currentOption.selected = NO;
        [tableView reloadRowsAtIndexPaths:@[self.currentIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        self.currentIndexPath = indexPath;
        self.currentOption = choiceOption;
    }
    choiceOption.selected = !choiceOption.selected;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    self.isUpdate = YES;
}

#pragma mark -  table view datasource handler

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FSSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FSSelectTableViewCell"];
    FSOptionsMoreItem *renameItem = [FSOptionsMoreItem createItemWithImageAndTitle:FSLocalizedForKey(@"kRename") imageNormal:ImageNamed(@"document_edit_small_rename") imageSelected:ImageNamed(@"document_edit_small_rename")];
    FSOptionsMoreItem *deleteItem = [FSOptionsMoreItem createItemWithImageAndTitle:FSLocalizedForKey(@"kDelete") imageNormal:ImageNamed(@"panel_more_delete") imageSelected:ImageNamed(@"panel_more_delete")];
    @weakify(self)
    [cell.selectBtn setBlockForControlEvents:UIControlEventTouchUpInside block:^(UIButton * _Nonnull sender) {
        @strongify(self)
        [self tableView:tableView didSelectRowAtIndexPath:indexPath];
    }];
    cell.more.optionItems = @[ renameItem, deleteItem];
    [cell.more setup];
    FSChoiceOption *choiceOption = self.dataSource[indexPath.row];
    [renameItem setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        InputAlertView *inputAlertView = [[InputAlertView alloc] initWithTitle:FSLocalizedForKey(@"kRename") message:nil buttonClickHandler:^(InputAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                @strongify(self)
                if (![alertView.inputTextField.text isEqualToString:choiceOption.option_label]) {
                    choiceOption.option_label = alertView.inputTextField.text;
                    choiceOption.option_value = alertView.inputTextField.text;
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    self.isUpdate = YES;
                }
            }
        } cancelButtonTitle:FSLocalizedForKey(@"kCancel") otherButtonTitles:FSLocalizedForKey(@"kOK"), nil];
        NSString *text = choiceOption.option_label;
        inputAlertView.inputTextField.text = text;
        inputAlertView.style = TSAlertViewStyleInputText;
        inputAlertView.buttonLayout = TSAlertViewButtonLayoutNormal;
        inputAlertView.usesMessageTextView = NO;
        
        UIButton *OKBtn = inputAlertView.buttons[1];
        [inputAlertView.inputTextField setBlockForControlEvents:UIControlEventEditingChanged block:^(UITextField * _Nonnull sender) {
            @strongify(self)
            if (!sender.text.length) {
                OKBtn.enabled = NO;
            }else{
                BOOL enabled = YES;
                for (FSChoiceOption *option in self.dataSource) {
                    if ([option.option_label isEqualToString:sender.text]) {
                        enabled = NO;
                        break;
                    }
                }
                OKBtn.enabled = enabled;
            }
        }];
        [inputAlertView show];
    }];
    [deleteItem setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        @strongify(self)
        if (self.currentIndexPath.row == indexPath.row && self.currentIndexPath.section == indexPath.section) {
            self.currentIndexPath = nil;
            self.currentOption = nil;
        }
        self.isUpdate = YES;
        FSChoiceOption *deleteChoiceOption = self.dataSource[indexPath.row];
        NSMutableArray *dataSoucre = self.dataSource.mutableCopy;
        [dataSoucre removeObject:deleteChoiceOption];
        self.dataSource = dataSoucre.copy;
        [self.tableView reloadData];
    }];
    cell.titleLab.text = choiceOption.option_label;
    if (self.multiSelect) {
        cell.selectBtn.selected = choiceOption.selected;
        cell.titleLab.textColor = choiceOption.selected ? ThemeLikeBlueColor : BlackThemeTextColor;
    }else{
        if (choiceOption.selected) {
            self.currentOption = choiceOption;
            self.currentIndexPath = indexPath;
        }
        cell.selectBtn.selected = choiceOption.selected;
        cell.titleLab.textColor = choiceOption.selected ? ThemeLikeBlueColor : BlackThemeTextColor;
    }
    return cell;
}
@end
