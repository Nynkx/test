/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "TextInputLayout.h"
#import "PropertyBar.h"
#import "Utility.h"

@interface TextInputLayout ()<IPropertyValueChangedListener, UITextFieldDelegate>
@property (nonatomic, strong) UITextField *textfield;
@property (nonatomic, copy) NSString *lastText;
@property (nonatomic, weak) id<IPropertyValueChangedListener> currentListener;

@end

@implementation TextInputLayout

- (instancetype)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, frame.size.width, LAYOUTTITLEHEIGHT)];
        self.title.text = FSLocalizedForKey(@"kText");
        self.title.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.title.font = [UIFont systemFontOfSize:11.0f];
        [self addSubview:self.title];
        
        UIView *textContainer = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.title.frame), frame.size.width - 40.f, 30.f)];
        textContainer.layer.borderWidth = 1.0f;
        textContainer.layer.cornerRadius = 3.0f;
        textContainer.layer.borderColor = [[UIColor colorWithRGB:0x5c5c5c alpha:0.2] CGColor];
        [self addSubview:textContainer];
        
        CGRect textfieldFrame = textContainer.frame;
        textfieldFrame.origin = CGPointMake(5, 0);
        textfieldFrame.size.width -= 10;
        self.textfield = [[UITextField alloc] initWithFrame:textfieldFrame];
        self.textfield.textColor = [UIColor colorWithRGB:0x5c5c5c];
        self.textfield.font = [UIFont systemFontOfSize:11.0f];
        self.textfield.placeholder = FSLocalizedForKey(@"kInputPlaceholderText");
        self.textfield.delegate = self;
        [textContainer addSubview:self.textfield];

        self.layoutHeight = CGRectGetMaxY(textContainer.frame);
    }
    return self;
}

- (long)supportProperty {
    if (![self.title.text isEqualToString:FSLocalizedForKey(@"kText")]) {
        return PROPERTY_TEXTNAME;
    }
    return PROPERTY_TEXTINPUT;
}

- (void)setCurrentCoverText:(NSString *)coverText{
    _textfield.text = coverText;
}

- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener {
    _currentListener = currentListener;
}

- (void)addDivideView {
    for (UIView *view in self.subviews) {
        if (view.tag == 1000) {
            [view removeFromSuperview];
        }
    }
    UIView *divide = [[UIView alloc] initWithFrame:CGRectMake(20, self.frame.size.height - 1, self.frame.size.width - 40, [Utility realPX:1.0f])];
    divide.tag = 1000;
    divide.backgroundColor = [UIColor colorWithRGB:0x5c5c5c];
    divide.alpha = 0.2f;
    [self addSubview:divide];
}

- (void)resetLayout {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    self.title.frame = CGRectMake(20, 3, self.frame.size.width, LAYOUTTITLEHEIGHT);
    [self addSubview:self.title];
    
    UIView *textContainer = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.title.frame), self.frame.size.width - 40.f, 30.f)];
    textContainer.layer.borderWidth = 1.0f;
    textContainer.layer.cornerRadius = 3.0f;
    textContainer.layer.borderColor = [[UIColor colorWithRGB:0x5c5c5c alpha:0.2] CGColor];
    [self addSubview:textContainer];
    
    CGRect textfieldFrame = textContainer.frame;
    textfieldFrame.origin = CGPointMake(5, 0);
    textfieldFrame.size.width -= 10;
    self.textfield.frame = textfieldFrame;
    [textContainer addSubview:self.textfield];
    
    self.layoutHeight = CGRectGetMaxY(textContainer.frame);
}

- (void)lockProperty:(BOOL)lock{
    self.userInteractionEnabled = !lock;
    self.alpha = !lock?:ALPHA_LOCKED;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.currentListener onProperty:self.supportProperty changedFrom:[NSValue valueWithNonretainedObject:_lastText] to:[NSValue valueWithNonretainedObject:textField.text]];
    _lastText = textField.text;
}

- (void)onProperty:(long)property changedFrom:(NSValue *)oldValue to:(NSValue *)newValue{
    [self.currentListener onProperty:self.supportProperty changedFrom:oldValue to:newValue];
}

@end
