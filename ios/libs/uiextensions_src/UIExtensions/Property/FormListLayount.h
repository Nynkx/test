/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSBaseViews.h"

NS_ASSUME_NONNULL_BEGIN

@protocol IPropertyValueChangedListener;

@interface FormListLayount : FSCellView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) float layoutHeight;
@property (nonatomic, weak) id<IRotationEventListener>delegate;

- (instancetype)initWithFrame:(CGRect)frame;
- (long)supportProperty;
- (void)setCurrentWidget:(FSWidget *)widget;
- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener;
- (void)addDivideView;
- (void)resetLayout;
- (void)lockProperty:(BOOL)lock;
@end

NS_ASSUME_NONNULL_END
