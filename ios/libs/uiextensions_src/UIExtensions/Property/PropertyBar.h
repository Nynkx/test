/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIExtensionsManager.h"
#import "ColorItem.h"
#import "ColorLayout.h"
#import "FontLayout.h"
#import "IconLayout.h"
#import "LineWidthLayout.h"
#import "OpacityItem.h"
#import "OpacityLayout.h"
#import "RotationLayout.h"
#import <Foundation/Foundation.h>
#import "DistanceUnitLayout.h"
#import "TextInputLayout.h"
#import "FormListLayount.h"

#define TABHEIGHT 44
#define LAYOUTTITLEHEIGHT 20
#define LAYOUTTBSPACE 15
#define ITEMLRSPACE 20

static const CGFloat TOP_MARGIN = 5.f;
static const CGFloat ALPHA_LOCKED = 0.6;
typedef NS_ENUM(NSUInteger, FSPropertyType) {
    PROPERTY_UNKNOWN = 0x00000000,
    PROPERTY_COLOR = 0x00000001,
    PROPERTY_OPACITY = 0x00000002,
    PROPERTY_LINEWIDTH = 0x00000004,
    PROPERTY_FONTNAME = 0x00000008,
    PROPERTY_FONTSIZE = 0x00000010,
    PROPERTY_ICONTYPE = 0x00000100,
    PROPERTY_ATTACHMENT_ICONTYPE = 0x00000040,
    PROPERTY_DISTANCE_UNIT = 0x00001000,
    PROPERTY_ROTATION = 0x00000080, // image only
    PROPERTY_TEXTINPUT = 0x00000020, //
    PROPERTY_TEXTNAME = 0x00000400, //form radio only
    PROPERTY_FORMOPTION = 0x00002000 // form only
};

enum Property_TabType {
    TAB_FILL = 100,
    TAB_BORDER,
    TAB_FONT,
    TAB_TYPE,
    TAB_DISTANCE_UNIT,
    TAB_OVERLAY_TEXT,
    TAB_OPTION,
};

typedef enum Property_TabType Property_TabType;

@protocol IPropertyValueChangedListener <NSObject>

@required
- (void)onProperty:(long)property changedFrom:(NSValue *)oldValue to:(NSValue *)newValue;
@end

@protocol IPropertyBarListener <NSObject>

@required
- (void)onPropertyBarDismiss;

@optional
- (void)onPropertyBarShow;

@end

@class FSPDFViewCtrl;
@protocol IRotationEventListener;

@interface PropertyBar : NSObject <UIPopoverControllerDelegate, IRotationEventListener>

@property (nonatomic, strong) FontLayout *fontLayout;
@property (nonatomic, strong) LineWidthLayout *lineWidthLayout;
@property (nonatomic, assign) BOOL isShowing;

- (instancetype)initWithPDFViewController:(FSPDFViewCtrl *)pdfViewCtrl extensionsManager:(UIExtensionsManager *)extensionsManager;
- (void)resetBySupportedItems:(long)items frame:(CGRect)frame;

- (void)setColors:(NSArray *)array;
- (void)setProperty:(long)property intValue:(int)value;
- (void)setProperty:(long)property floatValue:(float)value;
- (void)setProperty:(long)property stringValue:(NSString *)value;
- (void)setProperty:(long)property value:(id)value;
- (void)setProperty:(long)property lock:(BOOL)lock;
- (void)addListener:(id<IPropertyValueChangedListener>)listener;

- (void)addTabByTitle:(NSString *)title atIndex:(int)tabIndex;

- (void)updatePropertyBar:(CGRect)frame;
- (void)showPropertyBar:(CGRect)frame inView:(UIView *)view viewsCanMove:(NSArray *)views;

- (void)dismissPropertyBar;

- (void)registerPropertyBarListener:(id<IPropertyBarListener>)listener;
- (void)unregisterPropertyBarListener:(id<IPropertyBarListener>)listener;
- (void)setDistanceLayoutsForbidEdit;
@end
