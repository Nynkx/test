//
//  ColorBox.m
//  FoxitApp
//
//  Created by Apple on 16/4/26.
//
//

#import "ColorBox.h"

#import "PropertyBar.h"

@interface ColorBox ()
@property (nonatomic) CAGradientLayer *gradientLayer;
@end

@implementation ColorBox

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectColor:)];
        [self addGestureRecognizer:recognizer];
        self.isAccessibilityElement = YES;
        [self setAccessibilityLabel:FSLocalizedForKey(@"kCustomColor")];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.gradientLayer removeFromSuperlayer];
    [self drawColorBox];
}

- (void)drawColorBox{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    self.gradientLayer = gradientLayer;
    gradientLayer.frame = self.bounds;
    [self.layer addSublayer:gradientLayer];
    
    //set gradient colors
    gradientLayer.colors = @[(__bridge id)[UIColor colorWithRGB:0xFFFF0000].CGColor, (__bridge id)[UIColor colorWithRGB:0xFFFF00FF].CGColor, (__bridge id)[UIColor colorWithRGB:0xFF0000FF].CGColor, (__bridge id)[UIColor colorWithRGB:0xFF00FFFF].CGColor, (__bridge id)[UIColor colorWithRGB:0xFF00FF00].CGColor, (__bridge id)[UIColor colorWithRGB:0xFFFFFF00].CGColor, (__bridge id)[UIColor colorWithRGB:0xFFFF0000].CGColor];
    gradientLayer.locations = @[@0.0, @0.16, @0.32, @0.48, @0.64, @0.80,@1.0];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1, 0.5);
}

- (void)selectColor:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self];
    self.colorHex = [self colorOfPoint:point].rgbValue;
    if (self.CallBackInt) {
        self.CallBackInt(PROPERTY_COLOR, self.colorHex);
    }
    
    for (CALayer *subLayer in self.gradientLayer.sublayers) {
        [subLayer removeFromSuperlayer];
    }
    CALayer *clickLayer = [CALayer layer];
    clickLayer.frame = CGRectMake(point.x - 3.5, point.y - 3.5, 7, 7);
    clickLayer.contents = (__bridge id _Nullable)(GetImage(@"property_clickcolor").CGImage);
    [self.gradientLayer addSublayer:clickLayer];
}

- (UIColor *)colorOfPoint:(CGPoint)point {
    unsigned char pixel[4] = {0};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -point.x, -point.y);
    
    [self.gradientLayer renderInContext:context];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    UIColor *color = [UIColor colorWithRed:pixel[0]/255.0 green:pixel[1]/255.0 blue:pixel[2]/255.0 alpha:pixel[3]/255.0];
    
    return color;
}

@end
