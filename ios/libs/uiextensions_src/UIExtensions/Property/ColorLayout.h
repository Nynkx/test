/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <Foundation/Foundation.h>
#import "FSBaseViews.h"

@class PropertyBar;
@protocol IPropertyValueChangedListener;

@interface ColorLayout : FSCellView

@property (nonatomic, assign) float layoutHeight;

- (instancetype)initWithFrame:(CGRect)frame propertyBar:(PropertyBar *)propertyBar;
- (long)supportProperty;
- (void)setColors:(NSArray *)array;
- (void)setCurrentColor:(uint32_t)color;
- (void)setCurrentListener:(id<IPropertyValueChangedListener>)currentListener;
- (void)lockProperty:(BOOL)lock;
- (void)addDivideView;
- (void)resetLayout;
@end
