/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PropertyBar.h"
#import "ColorLayout.h"
#import "FontLayout.h"
#import "LineWidthLayout.h"
#import "OpacityLayout.h"
#import "PropertyMainView.h"
#import "IQKeyboardManager.h"

#define PDFVIEWCTRLWIDTH _pdfViewCtrl.bounds.size.width
#define PDFVIEWCTRLHEIGHT _pdfViewCtrl.bounds.size.height

@interface PropertyBar () <UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) NSPointerArray *propertyBarListeners;
@property (nonatomic, weak) id<IPropertyValueChangedListener> currentListener;
@property (nonatomic, assign) long currentItems;
@property (nonatomic, assign) BOOL currentLocked;
@property (nonatomic, assign) BOOL currentForbidEdit;
@property (nonatomic, strong) NSArray *currentColors;
@property (nonatomic, assign) int currentColor;
@property (nonatomic, strong) PropertyMainView *mainView;
@property (nonatomic, strong) ColorLayout *colorLayout;
@property (nonatomic, strong) ColorLayout *textInputColorLayout;
@property (nonatomic, strong) TextInputLayout *textInputLayout;
@property (nonatomic, strong) OpacityLayout *opacityLayout;
@property (nonatomic, strong) IconLayout *typeLayout;
@property (nonatomic, strong) RotationLayout *rotationLayout;
@property (nonatomic, strong) FormListLayount *formListLayount;
@property (nonatomic, strong) UIControl *maskView;
@property (nonatomic, strong) UIViewController *popViewCtr;
@property (nonatomic, assign) CGRect tempFrame;
@property (nonatomic, strong) DistanceUnitLayout *distanceUnitLayout;
@end

@implementation PropertyBar {
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak UIExtensionsManager *_extensionsManager;
}

- (instancetype)initWithPDFViewController:(FSPDFViewCtrl *)pdfViewCtrl extensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _pdfViewCtrl = pdfViewCtrl;
        _extensionsManager = extensionsManager;
        self.maskView = [[UIControl alloc] initWithFrame:_pdfViewCtrl.bounds];
        self.maskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
        self.propertyBarListeners = [NSPointerArray weakObjectsPointerArray];
        [_extensionsManager registerRotateChangedListener:self];
        [IQKeyboardManager sharedManager].layoutIfNeededOnUpdate = YES;
    }
    return self;
}

- (void)resetBySupportedItems:(long)items frame:(CGRect)frame {
    self.currentItems = items;
    self.currentLocked = NO;
    self.currentForbidEdit = NO;
    self.mainView = [[PropertyMainView alloc] init];
    self.mainView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;
    CGRect mainFrame;
    if (DEVICE_iPHONE) {
        if (!CGRectIsEmpty(frame))
            mainFrame = frame;
        else {
            mainFrame = CGRectMake(0, _pdfViewCtrl.bounds.size.height, _pdfViewCtrl.bounds.size.width, 500);
        }
        if (@available(iOS 11.0, *)) {
            UIView *topViewControllerView = _pdfViewCtrl.fs_viewController.view;
            mainFrame.origin.x = topViewControllerView.safeAreaInsets.left;
            mainFrame.size.width -= topViewControllerView.safeAreaInsets.left + topViewControllerView.safeAreaInsets.right;
        }
    } else {
        mainFrame = CGRectMake(0, 0, 300, 300);
    }

    self.mainView.frame = mainFrame;
    self.mainView.backgroundColor = ThemeViewBackgroundColor;
    if (items & PROPERTY_COLOR) {
        CGRect colorFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
        self.colorLayout = [[ColorLayout alloc] initWithFrame:colorFrame propertyBar:self];
        [self.colorLayout setColors:self.currentColors];
        self.colorLayout.tag = PROPERTY_COLOR;
        [self.mainView addLayoutAtTab:self.colorLayout tab:TAB_FILL];
    }
    if (items & PROPERTY_TEXTINPUT) {
        [[IQKeyboardManager sharedManager] setEnable:YES];
        [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
        CGRect textInputFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
        self.textInputLayout = [[TextInputLayout alloc] initWithFrame:textInputFrame];
        self.textInputLayout.tag = PROPERTY_TEXTINPUT;
        [self.mainView addLayoutAtTab:self.textInputLayout tab:TAB_FILL];
        
        CGRect colorFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
        self.textInputColorLayout = [[ColorLayout alloc] initWithFrame:colorFrame propertyBar:self];
        [self.textInputColorLayout setColors:self.currentColors];
        self.textInputColorLayout.tag = PROPERTY_TEXTINPUT;
        [self.mainView addLayoutAtTab:self.textInputColorLayout tab:TAB_TYPE];
    }
    
    if (items & PROPERTY_TEXTNAME) {
        [[IQKeyboardManager sharedManager] setEnable:YES];
        [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
        CGRect textInputFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
        self.textInputLayout = [[TextInputLayout alloc] initWithFrame:textInputFrame];
        self.textInputLayout.tag = PROPERTY_TEXTNAME;
        self.textInputLayout.title.text = FSLocalizedForKey(@"kName");
        [self.mainView addLayoutAtTab:self.textInputLayout tab:TAB_FILL];
    }
    
    if (items & PROPERTY_OPACITY) {
        CGRect opacityFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
        self.opacityLayout = [[OpacityLayout alloc] initWithFrame:opacityFrame];
        self.opacityLayout.tag = PROPERTY_OPACITY;
        [self.mainView addLayoutAtTab:self.opacityLayout tab:TAB_FILL];
    }
    if (items & PROPERTY_ROTATION) {
        CGRect rotationFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
        self.rotationLayout = [[RotationLayout alloc] initWithFrame:rotationFrame];
        self.rotationLayout.tag = PROPERTY_ROTATION;
        [self.mainView addLayoutAtTab:self.rotationLayout tab:TAB_FILL];
    }
    if (items & PROPERTY_LINEWIDTH) {
        self.lineWidthLayout = [[LineWidthLayout alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 40)];
        self.lineWidthLayout.tag = PROPERTY_LINEWIDTH;
        [self.mainView addLayoutAtTab:self.lineWidthLayout tab:TAB_BORDER];
    }
    if (items & PROPERTY_FONTNAME) {
        self.fontLayout = [[FontLayout alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 100)];
        self.fontLayout.tag = PROPERTY_FONTNAME;
        [self.mainView addLayoutAtTab:self.fontLayout tab:TAB_FONT];
    }
    if (items & PROPERTY_FORMOPTION) {
        self.formListLayount = [[FormListLayount alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 100)];
        self.formListLayount.delegate = self;
        self.formListLayount.tag = PROPERTY_FORMOPTION;
        [self.mainView addLayoutAtTab:self.formListLayount tab:TAB_OPTION];
    }
    if (items & PROPERTY_FONTSIZE) {
    }
    if (items & PROPERTY_ICONTYPE) {
        self.typeLayout = [[IconLayout alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 100) iconType:PROPERTY_ICONTYPE];
        self.typeLayout.tag = PROPERTY_ICONTYPE;
        [self.mainView addLayoutAtTab:self.typeLayout tab:TAB_TYPE];
    }
    if (items & PROPERTY_ATTACHMENT_ICONTYPE) {
        self.typeLayout = [[IconLayout alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 100) iconType:PROPERTY_ATTACHMENT_ICONTYPE];
        self.typeLayout.tag = PROPERTY_ATTACHMENT_ICONTYPE;
        [self.mainView addLayoutAtTab:self.typeLayout tab:TAB_TYPE];
    }
    
    if (items & PROPERTY_DISTANCE_UNIT) {
        self.distanceUnitLayout = [[DistanceUnitLayout alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 100)];
        self.distanceUnitLayout.tag = PROPERTY_DISTANCE_UNIT;
        [self.mainView addLayoutAtTab:self.distanceUnitLayout tab:TAB_DISTANCE_UNIT];
    }
    
    if (self.mainView.segmentItems.count > 0) {
        SegmentView *segmentView = [[SegmentView alloc] initWithFrame:CGRectMake(20, 5, self.mainView.frame.size.width - 40, TABHEIGHT - 10)];
        [segmentView loadWithItems:self.mainView.segmentItems];
        segmentView.delegate = self.mainView;
        [self.mainView addSubview:segmentView];
        if (items & PROPERTY_COLOR) {
            [self.mainView showTab:TAB_FILL];
            for (SegmentItem *item in [segmentView getItems]) {
                if (item.tag == TAB_FILL) {
                    [segmentView setSelectItem:item];
                }
            }
        }
        if (items & PROPERTY_FORMOPTION) {
            [self.mainView showTab:TAB_FONT];
            for (SegmentItem *item in [segmentView getItems]) {
                if (item.tag == TAB_FONT) {
                    [segmentView setSelectItem:item];
                }
            }
        }
    }
    self.tempFrame = self.mainView.frame;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && DEVICE_iPHONE) {
        if (CGRectGetHeight(self.tempFrame) > CGRectGetHeight(_pdfViewCtrl.bounds)) {
            UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanForHide:)];
            [self.mainView addGestureRecognizer:recognizer];
        }
    }
}

- (void)setColors:(NSArray *)array {
    self.currentColors = array;
}

- (void)setProperty:(long)property intValue:(int)value {
    if (property & PROPERTY_COLOR) {
        self.currentColor = [[UIColor colorWithRGB:value] rgbValue];
        [self.colorLayout setCurrentColor:[[UIColor colorWithRGB:value] rgbValue]];
    }
    if (property & PROPERTY_OPACITY) {
        [self.opacityLayout setCurrentOpacity:value];
    }
    if (property & PROPERTY_LINEWIDTH) {
        [self.lineWidthLayout setCurrentColor:self.currentColor];
        [self.lineWidthLayout setCurrentLineWidth:value];
    }
    if (property & PROPERTY_ICONTYPE) {
        [self.typeLayout setCurrentIconType:value];
    }
    if (property & PROPERTY_ATTACHMENT_ICONTYPE) {
        [self.typeLayout setCurrentIconType:value];
    }
    if (property & PROPERTY_ROTATION) {
        [self.rotationLayout setCurrentRotation:value];
    }
    if (property & PROPERTY_TEXTINPUT) {
        self.currentColor = [[UIColor colorWithRGB:value] rgbValue];
        [self.textInputColorLayout setCurrentColor:[[UIColor colorWithRGB:value] rgbValue]];
    }
}

- (void)setProperty:(long)property floatValue:(float)value {
    if (property & PROPERTY_FONTSIZE) {
        [self.fontLayout setCurrentFontSize:value];
    }
}

- (void)setProperty:(long)property stringValue:(NSString *)value {
    if (property & PROPERTY_FONTNAME) {
        [self.fontLayout setCurrentFontName:value];
    }
    
    if (property & PROPERTY_DISTANCE_UNIT) {
        [self.distanceUnitLayout setCurrentUnitName:value];
    }
    
    if (property & PROPERTY_TEXTINPUT || property & PROPERTY_TEXTNAME) {
        [self.textInputLayout setCurrentCoverText:value];
    }
}

- (void)setProperty:(long)property value:(id)value {
    if (property & PROPERTY_FONTNAME) {
        [self.fontLayout setCurrentFontName:value];
    }
    
    if (property & PROPERTY_DISTANCE_UNIT) {
        [self.distanceUnitLayout setCurrentUnitName:value];
    }
    
    if (property & PROPERTY_FORMOPTION) {
        if ([value isKindOfClass:[FSWidget class]]) {
            [self.formListLayount setCurrentWidget:value];
        }
    }
}


- (void)setProperty:(long)property lock:(BOOL)lock{
    self.currentLocked = lock;
    if (property & PROPERTY_COLOR) {
        [self.colorLayout lockProperty:lock];
    }
    if (property & PROPERTY_OPACITY) {
        [self.opacityLayout lockProperty:lock];
    }
    if (property & PROPERTY_LINEWIDTH) {
        [self.lineWidthLayout lockProperty:lock];
    }
    if (property & PROPERTY_ICONTYPE || property & PROPERTY_ATTACHMENT_ICONTYPE) {
        [self.typeLayout lockProperty:lock];
    }
    if (property & PROPERTY_ROTATION) {
        [self.rotationLayout lockProperty:lock];
    }
    if (property & PROPERTY_DISTANCE_UNIT) {
        [self.distanceUnitLayout lockProperty:lock];
    }
    if (property & PROPERTY_FONTNAME) {
        [self.fontLayout lockProperty:lock];
    }
    if (property & PROPERTY_TEXTINPUT) {
        [self.textInputColorLayout lockProperty:lock];
        [self.textInputLayout lockProperty:lock];
    }
    if (property & PROPERTY_FORMOPTION) {
        [self.formListLayount lockProperty:lock];
    }
}

- (void)addListener:(id<IPropertyValueChangedListener>)listener {
    self.currentListener = listener;
    if (self.colorLayout) {
        [self.colorLayout setCurrentListener:listener];
    }
    if (self.opacityLayout) {
        [self.opacityLayout setCurrentListener:listener];
    }
    if (self.lineWidthLayout) {
        [self.lineWidthLayout setCurrentListener:listener];
    }
    if (self.fontLayout) {
        [self.fontLayout setCurrentListener:listener];
    }
    if (self.typeLayout) {
        [self.typeLayout setCurrentListener:listener];
    }

    if (self.distanceUnitLayout) {
        [self.distanceUnitLayout setCurrentListener:listener];
    }
    if (self.rotationLayout) {
        [self.rotationLayout setCurrentListener:listener];
    }
    if (self.textInputLayout) {
        [self.textInputLayout setCurrentListener:listener];
        if (self.textInputColorLayout) {
            [self.textInputColorLayout setCurrentListener:(id<IPropertyValueChangedListener>)self.textInputLayout];
        }
    }
    if (self.formListLayount) {
        [self.formListLayount setCurrentListener:listener];
    }
}

- (void)addTabByTitle:(NSString *)title atIndex:(int)tabIndex {
}

- (void)updatePropertyBar:(CGRect)frame {
}

- (void)handlePanForHide:(UIPanGestureRecognizer *)panRecognizer{
    CGPoint point = [panRecognizer translationInView:self.mainView];
    CGFloat height = CGRectGetHeight(self.mainView.frame);
    CGFloat supHeight = CGRectGetHeight(self.mainView.superview.frame);
    CGFloat minY = supHeight - height;
    CGFloat minCenterY = supHeight - height / 2;
    CGPoint center =  CGPointMake(self.mainView.center.x, self.mainView.center.y + point.y);
    self.mainView.center = center;
    if (CGRectGetMinY(self.mainView.frame) < minY) {
        self.mainView.frame = CGRectMake(CGRectGetMinX(self.mainView.frame), minY, CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame));
    }
    if (panRecognizer.state == UIGestureRecognizerStateEnded || panRecognizer.state == UIGestureRecognizerStateCancelled) {
        if (CGRectGetMinY(self.mainView.frame) < minCenterY) {
            [UIView animateWithDuration:0.2 animations:^{
                self.mainView.frame = CGRectMake(CGRectGetMinX(self.mainView.frame), minY, CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame));
            }];
        }else{
            [self dismissPropertyBar];
        }
    }
    [panRecognizer setTranslation:CGPointMake(0, 0) inView:self.mainView];
}

- (void)showPropertyBar:(CGRect)frame inView:(UIView *)view viewsCanMove:(NSArray *)views {
    float DISPLAYVIEWWIDTH = view.bounds.size.width;
    float DISPLAYVIEWHEIGHT = view.bounds.size.height;
    FSAnnot *annot = _extensionsManager.currentAnnot;
    if (annot && ![annot isEmpty]) {
        int pageIndex = annot.pageIndex;
        CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:pageIndex];
        float width =  DISPLAYVIEWWIDTH;
        float height = DISPLAYVIEWHEIGHT;
        
        if (((dvRect.origin.x + dvRect.size.width) <= 0 || dvRect.origin.y + dvRect.size.height <= 0 || dvRect.origin.x > width || dvRect.origin.y > height) && CGRectEqualToRect(dvRect, frame)) {
            return;
        }
    }

    for (id<IPropertyBarListener> listener in self.propertyBarListeners) {
        if ([listener respondsToSelector:@selector(onPropertyBarShow)]) {
            [listener onPropertyBarShow];
        }
    }

    frame = CGRectInset(frame, -10, -10);
    if (DEVICE_iPHONE) {
        self.maskView.backgroundColor = [UIColor blackColor];
        self.maskView.alpha = 0.3f;
        self.maskView.tag = 200;

        [view addSubview:self.maskView];
        [view addSubview:self.mainView];
        [self.maskView addTarget:self action:@selector(dismissPropertyBar) forControlEvents:UIControlEventTouchUpInside];
        CGRect newFrame = self.mainView.frame;
        newFrame.origin.y = DISPLAYVIEWHEIGHT - newFrame.size.height;
        self.maskView.frame = CGRectMake(0, 0, DISPLAYVIEWWIDTH, DISPLAYVIEWHEIGHT);
        self.maskView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
        if (@available(iOS 11.0, *)) {
            newFrame.origin.x = view.safeAreaInsets.left;
            newFrame.size.width = DISPLAYVIEWWIDTH - view.safeAreaInsets.left - view.safeAreaInsets.right;
        }
        
        [UIView animateWithDuration:0.4 animations:^{
            self.mainView.frame = newFrame;
        } completion:^(BOOL finished) {
            if (finished) {
                [self scrollToSelectedAnnot];
            }
        }];
    } else {
        UIViewController *oldPopViewCtr;
        if ([Utility getTopMostViewController] == self.popViewCtr) {
            oldPopViewCtr = _popViewCtr;
            _popViewCtr = nil;
        }
        self.mainView.frame = self.tempFrame;
        [self.mainView removeFromSuperview];
        self.popViewCtr.preferredContentSize = CGSizeMake(300, self.mainView.frame.size.height);
        if (frame.origin.x < 300 && frame.origin.y < self.mainView.frame.size.height && _pdfViewCtrl.bounds.size.width - frame.size.width - frame.origin.x < 300 && _pdfViewCtrl.bounds.size.height - frame.size.height - frame.origin.y < self.mainView.frame.size.height) {
            frame = CGRectMake(_pdfViewCtrl.bounds.size.width / 2, _pdfViewCtrl.bounds.size.height / 2, 10, 10);
        }
        self.popViewCtr.popoverPresentationController.sourceView = view;
        self.popViewCtr.popoverPresentationController.sourceRect = frame;
        self.popViewCtr.popoverPresentationController.passthroughViews = [NSArray arrayWithArray:views];
        if (@available(iOS 13.0, *)) {
            [self.popViewCtr.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull subview, NSUInteger idx, BOOL * _Nonnull stop) {
                [subview removeFromSuperview];
            }];
             [self.popViewCtr.view addSubview:self.mainView];
            UILayoutGuide *guide = self.popViewCtr.view.safeAreaLayoutGuide;
            self.mainView.translatesAutoresizingMaskIntoConstraints = NO;
            [self.mainView.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
            [self.mainView.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
            [self.mainView.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
            [self.mainView.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
            [self.mainView layoutIfNeeded];
        }else{
            self.popViewCtr.view = self.mainView;
        }
        _extensionsManager.shouldShowMenu = NO; //same as mobile reader
        self.popViewCtr.popoverPresentationController.delegate = self;
        UIViewController *topMostViewController = [Utility getTopMostViewController];
        if (topMostViewController == oldPopViewCtr) {
            [_pdfViewCtrl.fs_viewController presentViewController:self.popViewCtr animated:true completion:nil];
        }else{
             [topMostViewController presentViewController:self.popViewCtr animated:true completion:nil];
        }
       
        self.isShowing = YES;
    }
}
- (UIViewController *)popViewCtr {
    if (_popViewCtr == nil) {
        _popViewCtr = [[UIViewController alloc] init];
        _popViewCtr.modalPresentationStyle = UIModalPresentationPopover;
    }
    return _popViewCtr;
}

- (void)scrollToSelectedAnnot{
    CGRect manViewFrame = self.mainView.frame;
    FSAnnot *annot = self->_extensionsManager.currentAnnot;
    if (!annot || [annot isEmpty]) {
        return;
    }
    [self->_pdfViewCtrl setBottomOffset:0];
    int pageIndex = annot.pageIndex;
    
    float (^getPositionY)(void) = ^(){
        
        CGRect pvAnnotRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        CGRect dvAnnotRect = [self->_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvAnnotRect pageIndex:pageIndex];
        
        float positionY;
        positionY = self->_pdfViewCtrl.bounds.size.height - CGRectGetMaxY(dvAnnotRect);
        return positionY;
    };
    
    float positionY = getPositionY();
    
    if (positionY < manViewFrame.size.height) {
        
        CGRect pvAnnotRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        CGPoint point = CGPointMake(CGRectGetMidX(pvAnnotRect) - CGRectGetMidX(manViewFrame), CGRectGetMaxY(pvAnnotRect) - (self->_pdfViewCtrl.bounds.size.height - manViewFrame.size.height) + 20);
        FSPointF *pointF = [self->_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        [self->_pdfViewCtrl gotoPage:pageIndex withDocPoint:pointF animated:YES];
        
        float positionY = getPositionY();
        
        float dvOffsetY = manViewFrame.size.height - positionY + 20;
        
        CGRect offsetRect = CGRectMake(0, 0, 100, dvOffsetY);
        
        CGRect pvRect = [self->_pdfViewCtrl convertDisplayViewRectToPageViewRect:offsetRect pageIndex:annot.pageIndex];
        FSRectF *pdfRect = [self->_pdfViewCtrl convertPageViewRectToPdfRect:pvRect pageIndex:annot.pageIndex];
        float pdfOffsetY = [pdfRect getTop] - [pdfRect getBottom];
        
        if ([self->_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_SINGLE && ![self->_pdfViewCtrl isContinuous]) {
            if (CGRectGetMaxY([self->_pdfViewCtrl getPageView:pageIndex].frame) - CGRectGetMaxY(pvAnnotRect) < manViewFrame.size.height + 20) {
                [self->_pdfViewCtrl setBottomOffset:dvOffsetY];
            }
        } else if ([self->_pdfViewCtrl isContinuous]) {
            if ([self->_pdfViewCtrl getCurrentPage] == [self->_pdfViewCtrl.currentDoc getPageCount] - 1) {
                FSRectF *fsRect = [[FSRectF alloc] init];
                [fsRect setLeft:0];
                [fsRect setBottom:pdfOffsetY];
                [fsRect setRight:pdfOffsetY];
                [fsRect setTop:0];
                float tmpPvOffset = [self->_pdfViewCtrl convertPdfRectToPageViewRect:fsRect pageIndex:annot.pageIndex].size.width;
                CGRect tmpPvRect = CGRectMake(0, 0, 10, tmpPvOffset);
                CGRect tmpDvRect = [self->_pdfViewCtrl convertPageViewRectToDisplayViewRect:tmpPvRect pageIndex:annot.pageIndex];
                [self->_pdfViewCtrl setBottomOffset:tmpDvRect.size.height];
            } else {
                double scrollY = [self->_pdfViewCtrl getVScrollPos];
                [self->_pdfViewCtrl setVScrollPos:scrollY+dvOffsetY animated:YES];
            }
        }
    }
}

#pragma mark <UIPopoverPresentationControllerDelegate>

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    [self dismissPropertyBar];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection {
    return UIModalPresentationNone;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView  * __nonnull * __nonnull)view{
    FSAnnot *annot = self->_extensionsManager.currentAnnot;
    CGRect pvRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self->_pdfViewCtrl convertPageViewRectToDisplayViewRect:pvRect pageIndex:annot.pageIndex];
    if (showRect.origin.x < 300 && showRect.origin.y < self.mainView.frame.size.height && self->_pdfViewCtrl.bounds.size.width - showRect.size.width - showRect.origin.x < 300 && self->_pdfViewCtrl.bounds.size.height - showRect.size.height - showRect.origin.y < self.mainView.frame.size.height) {
        showRect = CGRectMake(self->_pdfViewCtrl.bounds.size.width / 2, self->_pdfViewCtrl.bounds.size.height / 2, 10, 10);
    }
    *rect = showRect;
}

- (BOOL)isShowing {
    if (DEVICE_iPHONE) {
        if (self.maskView.alpha == 0.3f) {
            return YES;
        } else {
            return NO;
        }
    } else {
        return _isShowing;
    }
}

- (void)dismissPropertyBar {
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:NO];
    if (!self.isShowing) {
        return;
    }
    
    if (DEVICE_iPHONE) {
        [UIView animateWithDuration:0.4
            animations:^{
                self.maskView.alpha = 0.1f;
            }
            completion:^(BOOL finished) {
                [self.maskView removeFromSuperview];
            }];

        CGRect newFrame = self.mainView.frame;
        newFrame.origin.y = _pdfViewCtrl.bounds.size.height;
        
        if (newFrame.origin.y == 0) {
            newFrame.origin.y = SCREENHEIGHT;
        }

        [UIView animateWithDuration:0.4
            animations:^{
                self.mainView.frame = newFrame;
            }
            completion:^(BOOL finished) {
                [self.mainView removeFromSuperview];
            }];
        FSAnnot *annot = _extensionsManager.currentAnnot;
        if (annot && ![annot isEmpty]) {
            if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_SINGLE || annot.pageIndex == [_pdfViewCtrl.currentDoc getPageCount] - 1) {
                [_pdfViewCtrl setBottomOffset:0];
            }
        }

    } else {
        [_popViewCtr.presentingViewController dismissViewControllerAnimated:true completion:nil];
        self.isShowing = NO;
    }
    for (id<IPropertyBarListener> listener in self.propertyBarListeners) {
        if ([listener respondsToSelector:@selector(onPropertyBarDismiss)]) {
            [listener onPropertyBarDismiss];
        }
    }
}

- (void)registerPropertyBarListener:(id<IPropertyBarListener>)listener {
    if (listener && ![[self.propertyBarListeners allObjects] containsObject:listener]) {
        [self.propertyBarListeners addPointer:(__bridge void*)listener];
    }
}

- (void)unregisterPropertyBarListener:(id<IPropertyBarListener>)listener {
    if ([[self.propertyBarListeners allObjects] containsObject:listener]) {
        for(int i = 0; i < self.propertyBarListeners.count; i++) {
            if(((__bridge void*)listener) == [self.propertyBarListeners pointerAtIndex:i]) {
                [self.propertyBarListeners removePointerAtIndex:i];
                break;
            }
        }
    }
}

- (void)setDistanceLayoutsForbidEdit {
    self.currentForbidEdit = YES;
    [self.distanceUnitLayout forbidUnit];
}

#pragma mark IRotationEventListener
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (![self isShowing])
        return;

    if (!DEVICE_iPHONE) {
        if (!_extensionsManager.currentAnnot && !_extensionsManager.currentToolHandler)
            [self dismissPropertyBar];
    }
    
    for (UIView *view in self.mainView.subviews) {
        [view removeFromSuperview];
    }
    
    [self.mainView removeAllLayout];
    
    for (UIGestureRecognizer *gestureRecognizer in self.mainView.gestureRecognizers) {
        [self.mainView removeGestureRecognizer:gestureRecognizer];
    }
    
    CGRect mainFrame;
    if (DEVICE_iPHONE) {
        mainFrame = CGRectMake(0, _pdfViewCtrl.bounds.size.height, _pdfViewCtrl.bounds.size.width, 500);
        if (@available(iOS 11.0, *)) {
            UIView *topViewControllerView = _pdfViewCtrl.fs_viewController.view;
            mainFrame.origin.x = topViewControllerView.safeAreaInsets.left;
            mainFrame.size.width -= topViewControllerView.safeAreaInsets.left + topViewControllerView.safeAreaInsets.right;
        }
    } else {
        mainFrame = CGRectMake(0, 0, 300, 300);
    }
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.mainView.frame = mainFrame;
    } completion:^(BOOL finished) {
        if (finished) {
        if (self.currentItems & PROPERTY_COLOR) {
            CGRect colorFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.colorLayout.frame = colorFrame;
            [self.colorLayout resetLayout];
            [self.mainView addLayoutAtTab:self.colorLayout tab:TAB_FILL];
        }
        if (self.currentItems & PROPERTY_TEXTINPUT) {
            [[IQKeyboardManager sharedManager] setEnable:YES];
            [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
            CGRect textInputFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.textInputLayout.frame = textInputFrame;
            self.textInputLayout.tag = PROPERTY_TEXTINPUT;
            [self.textInputLayout resetLayout];
            [self.mainView addLayoutAtTab:self.textInputLayout tab:TAB_FILL];
            
            CGRect colorFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.textInputColorLayout.frame = colorFrame;
            [self.textInputColorLayout setColors:self.currentColors];
            self.textInputColorLayout.tag = PROPERTY_TEXTINPUT;
            [self.textInputColorLayout resetLayout];
            [self.mainView addLayoutAtTab:self.textInputColorLayout tab:TAB_TYPE];
        }
        if (self.currentItems & PROPERTY_TEXTNAME) {
            [[IQKeyboardManager sharedManager] setEnable:YES];
            [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
            CGRect textInputFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.textInputLayout.frame = textInputFrame;
            self.textInputLayout.tag = PROPERTY_TEXTNAME;
            self.textInputLayout.title.text = FSLocalizedForKey(@"kName");
            [self.textInputLayout resetLayout];
            [self.mainView addLayoutAtTab:self.textInputLayout tab:TAB_FILL];
        }
        if (self.currentItems & PROPERTY_OPACITY) {
            CGRect opacityFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.opacityLayout.frame = opacityFrame;
            [self.opacityLayout resetLayout];
            [self.mainView addLayoutAtTab:self.opacityLayout tab:TAB_FILL];
        }
        if (self.currentItems & PROPERTY_ROTATION) {
            CGRect rotationFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.rotationLayout.frame = rotationFrame;
            [self.rotationLayout resetLayout];
            [self.mainView addLayoutAtTab:self.rotationLayout tab:TAB_FILL];
        }
        if (self.currentItems & PROPERTY_LINEWIDTH) {
            CGRect linewidthFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.lineWidthLayout.frame = linewidthFrame;
            [self.lineWidthLayout resetLayout];
            [self.mainView addLayoutAtTab:self.lineWidthLayout tab:TAB_BORDER];
        }
        if (self.currentItems & PROPERTY_FONTNAME) {
            CGRect fontNameFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.fontLayout.frame = fontNameFrame;
            [self.fontLayout resetLayout];
            [self.mainView addLayoutAtTab:self.fontLayout tab:TAB_FONT];
        }
        if (self.currentItems & PROPERTY_FONTSIZE) {
        }
        if (self.currentItems  & PROPERTY_FORMOPTION) {
            self.formListLayount.frame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            [self.formListLayount resetLayout];
            [self.mainView addLayoutAtTab:self.formListLayount tab:TAB_OPTION];
        }
        if (self.currentItems & PROPERTY_ICONTYPE || self.currentItems & PROPERTY_ATTACHMENT_ICONTYPE) {
            CGRect typeFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.typeLayout.frame = typeFrame;
            [self.typeLayout resetLayout];
            [self.mainView addLayoutAtTab:self.typeLayout tab:TAB_TYPE];
        }
        if (self.currentItems & PROPERTY_DISTANCE_UNIT) {
            CGRect fontNameFrame = CGRectMake(0, 0, self.mainView.frame.size.width, 100);
            self.distanceUnitLayout.frame = fontNameFrame;
            [self.distanceUnitLayout resetLayout];
            [self.mainView addLayoutAtTab:self.distanceUnitLayout tab:TAB_DISTANCE_UNIT];
            if (self.currentForbidEdit) {
                [self setDistanceLayoutsForbidEdit];
            }
        }
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && DEVICE_iPHONE) {
            if (CGRectGetHeight(self.tempFrame) > CGRectGetHeight(self->_pdfViewCtrl.bounds)) {
                UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanForHide:)];
                [self.mainView addGestureRecognizer:recognizer];
            }
        }
        [self setProperty:self.currentItems lock:self.currentLocked];
    
            CGRect rect = self.mainView.frame;
            rect.origin.y = self->_pdfViewCtrl.bounds.size.height - rect.size.height;
            [self->_pdfViewCtrl setBottomOffset:0];

            [UIView animateWithDuration:0.2 animations:^{
                if (DEVICE_iPHONE) {
                    self.mainView.frame = rect;
                }
            } completion:^(BOOL finished) {
                if (finished) {
        if (self.mainView.segmentItems.count > 0) {
            SegmentView *segmentView = [[SegmentView alloc] initWithFrame:CGRectMake(20, 5, self.mainView.frame.size.width - 40, TABHEIGHT - 10)];
            [segmentView loadWithItems:self.mainView.segmentItems];
            segmentView.delegate = self.mainView;
            [self.mainView addSubview:segmentView];
            for (SegmentItem *item in [segmentView getItems]) {
                if (item.selected) {
                    [self.mainView showTab:(Property_TabType)item.tag];
                    [segmentView setSelectItem:item];
                }
            }
        }
        
        if (DEVICE_iPHONE) {
            [self scrollToSelectedAnnot];
        }
                    }
                }];
            }
        }];
    

}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
}

@end
