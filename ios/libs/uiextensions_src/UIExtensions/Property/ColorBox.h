//
//  ColorBox.h
//  FoxitApp
//
//  Created by Apple on 16/4/26.
//
//

#import <UIKit/UIKit.h>

@interface ColorBox : UIView

@property (nonatomic, assign) UInt32 colorHex;
@property (nonatomic, copy) void(^CallBackInt)(long property,UInt32 value);
- (instancetype)initWithFrame:(CGRect)frame;

@end
