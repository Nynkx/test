/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ColorItem.h"


@interface ColorItem ()

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) CALayer *selectLayer;

@end

@implementation ColorItem


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake(0, 0, frame.size.width , frame.size.height);
        self.button.layer.borderWidth = 1.0;
        self.button.layer.borderColor = [UIColor colorWithRGB:0xB2B2B2].CGColor;
        [self.button addTarget:self action:@selector(onClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.button];
    }
    return self;
}

- (CALayer *)selectLayer{
    if (!_selectLayer) {
        _selectLayer = [[CALayer alloc] init];
        _selectLayer.frame = CGRectMake(-4, -4, self.button.frame.size.width + 8, self.button.frame.size.height + 8);
    }
    return _selectLayer;
}

- (void)setColor:(uint32_t)color {
    _color = color;
    self.button.backgroundColor = [UIColor colorWithRGB:color];
}

- (void)onClick {
    if (self.callback) {
        self.callback(PROPERTY_COLOR, _color);
    }
}


- (void)setSelected:(BOOL)selected {
    if (selected) {
        self.selectLayer.borderWidth = 2.f;
        self.selectLayer.borderColor = [ThemeLikeBlueColor CGColor];
        self.selectLayer.cornerRadius = 5.0f;
        self.selectLayer.backgroundColor = [UIColor clearColor].CGColor;
        if (![self.layer.sublayers containsObject:self.selectLayer]) {
            [self.layer addSublayer:self.selectLayer];
        }
    } else {
        self.selectLayer.borderWidth = 0.0f;
        [self.selectLayer removeFromSuperlayer];
    }
}

- (void)setDisabled:(BOOL)disablle{
    self.button.enabled = !disablle;
}

@end
