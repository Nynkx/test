/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PencilAnnotHandler.h"

@implementation PencilAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
        self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-30, -30, -30, -30);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotInk;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    if ([[self.extensionsManager.currentToolHandler getName] isEqualToString:Tool_Eraser]) {
        //noop, todo, add to undo/redo
        return;
    }
    [super onAnnotSelected:annot];
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
    return options;
}

- (void)showStyle {
    [self.extensionsManager.propertyBar setColors:@[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff]];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:annot.lineWidth];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];

    int pageIndex = annot.pageIndex;
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
}


@end
