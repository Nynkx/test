/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PencilToolHandler.h"
#import "PencilAnnotHandler.h"
#import "PaintUtility.h"

@interface PencilToolHandler () <IAnnotPropertyListener,IAnnotEventListener>

@property (nonatomic, strong) FSInk *annot;
@property (nonatomic, assign) BOOL isMoving;
@property (nonatomic, assign) BOOL isZooming;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, assign) CGRect lastRect;
@property (nonatomic, assign) BOOL isBegin;
@property (nonatomic, assign) int currentPageIndex;

@property (nonatomic, strong) NSMutableArray *linesArray;
@property (nonatomic, assign) BOOL isEndDrawingLine;
@property (nonatomic, strong) PaintUtility *paintUtility;

@property (nonatomic, strong) FSAnnotAttributes *attributesBeforeModify;
@property (nonatomic, assign) BOOL added;

@end

@implementation PencilToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotInk;
        
        _linesArray = [NSMutableArray array];
        [_extensionsManager registerAnnotEventListener:self];
    }
    return self;
}

- (NSString *)getName {
    return Tool_Pencil;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
    _isEndDrawingLine = NO;
}

- (void)onDeactivate {
    _isEndDrawingLine = NO;
    [_linesArray removeAllObjects];
    self.annot = nil;
}

- (void)onAnnotWillDelete:(FSPDFPage *)page annot:(FSAnnot *)annot{
    if ([self.annot isEqualToAnnot:annot]) {
        self.annot = nil;
    }
}

- (void)onAnnotAdded:(FSPDFPage *)page annot:(FSAnnot *)annot{
    if ([self.annot isEqualToAnnot:annot]) {
        self.added = YES;
        self.attributesBeforeModify = [FSAnnotAttributes attributesWithAnnot:annot];;
    }
}

- (void)setAnnot:(FSInk *)annot{
    _annot = annot;
    self.attributesBeforeModify = nil;
    self.added = NO;
}

- (void)addAnnot:(FSInk *)annot{
    id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:FSAnnotInk];
    [annotHandler addAnnot:self.annot addUndo:YES];
}

- (void)modifyAnnot:(FSInk *)annot{
    if (![annot canModify]) {
        return;
    }
    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return;
    }
    id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:FSAnnotInk];
    annot.modifiedDate = [NSDate date];
    FSAnnotAttributes *attributesBeforeModify = [FSAnnotAttributes attributesWithAnnot:annot];
    [_extensionsManager addUndoItem:[UndoItem itemForUndoModifyAnnotWithOldAttributes:self.attributesBeforeModify newAttributes:attributesBeforeModify pdfViewCtrl:_pdfViewCtrl page:page annotHandler:annotHandler]];
    self.attributesBeforeModify = attributesBeforeModify;
    _extensionsManager.isDocModified = YES;
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return YES;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return YES;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (FSInk *)createInkAnnotationInPage:(int)pageIndex atPos:(FSPointF *)pos {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;

    CGRect rect = CGRectMake(pos.x - 20, pos.y - 20, 40, 40);
    FSInk *annot = [[FSInk alloc] initWithAnnot:[page addAnnot:FSAnnotInk rect:[Utility CGRect2FSRectF:rect]]];
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.color = [_extensionsManager getPropertyBarSettingColor:self.type];
    annot.opacity = [_extensionsManager getAnnotOpacity:self.type] / 100.0;
    annot.lineWidth = [_extensionsManager getAnnotLineWidth:self.type];
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    annot.flags = FSAnnotFlagPrint;
    annot.subject = @"Pencil";
    
    FSPath *path = [[FSPath alloc] init];
    [path moveTo:pos];
    [annot setInkList:path];
    [_pdfViewCtrl lockRefresh];
    [annot resetAppearanceStream];
    [_pdfViewCtrl unlockRefresh];
    return annot;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    self.currentPageIndex = pageIndex;
    _isEndDrawingLine = NO;

    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [[touches anyObject] locationInView:pageView];
    if (point.x < 10 || point.x > pageView.bounds.size.width - 10 ||
        point.y < 10 || point.y > pageView.bounds.size.height - 10) {
        return NO;
    }

    _isBegin = YES;
    _isMoving = YES;
    
    NSMutableArray *pointArray = [NSMutableArray array];
    NSValue *pointValue = [NSValue valueWithCGPoint:point];
    [pointArray addObject:pointValue];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys: pointArray, @"pointArray", nil];
    [_linesArray addObject:dic];
    
    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];

    if (self.annot == nil) //this is the first line
    {
        self.annot = [self createInkAnnotationInPage:pageIndex atPos:dibPoint];
    } else {
        if (pageIndex != self.annot.pageIndex) {
            return NO;
        }

        FSPath *path = [self.annot getInkList];
        [path moveTo:dibPoint];
        [self.annot setInkList:path];
    }

    CGRect cgRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
    [_pdfViewCtrl refresh:cgRect pageIndex:pageIndex];
    _lastPoint = point;
    
    FSRectF *lineFSRect = [[FSRectF alloc] initWithLeft1:0 bottom1:0 right1:self.annot.lineWidth top1:self.annot.lineWidth];
    CGRect lineRect = [_pdfViewCtrl convertPdfRectToPageViewRect:lineFSRect pageIndex:pageIndex];
    float lineWidth = lineRect.size.width;
    
    self.paintUtility = [[PaintUtility alloc] init];
    self.paintUtility.annotLineWidth = lineWidth;
    self.paintUtility.annotColor = self.annot.color;
    self.paintUtility.annotOpacity = self.annot.opacity;
    
    return YES;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_isMoving && !_isZooming && self.currentPageIndex == pageIndex) {
        UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
        CGPoint point = [[touches anyObject] locationInView:pageView];
        if (point.x < 10 || point.x > pageView.bounds.size.width - 10 ||
            point.y < 10 || point.y > pageView.bounds.size.height - 10) {
            return NO;
        }

        __weak NSMutableArray *pointArray = [_linesArray lastObject][@"pointArray"];
        NSValue *pointValue = [NSValue valueWithCGPoint:point];
        [pointArray addObject:pointValue];
        
        if (self.annot == nil) //this is the first line
        {
            FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
            self.annot = [self createInkAnnotationInPage:pageIndex atPos:dibPoint];
            CGRect cgRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
            [_pdfViewCtrl refresh:cgRect pageIndex:pageIndex];
            _lastPoint = point;
            return YES;
        } else {
            if (pageIndex != self.annot.pageIndex) {
                //annotation cannot cross page
                return NO;
            } else {
                FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];

                FSPath *path = [self.annot getInkList];
                if ([path getPointCount] == 0) {
                    FSPath *path = [[FSPath alloc] init];
                    [path moveTo:dibPoint];
                    [self.annot setInkList:path];
                    return YES;
                }
                [path lineTo:dibPoint];
                [self.annot setInkList:path];

                CGRect lineRect = [self getRectFromPoints:_linesArray];
                float scale = [_pdfViewCtrl getPageViewWidth:pageIndex] / 1000;
                float margin = self.annot.lineWidth * 3 * scale;
                lineRect = CGRectInset(lineRect, -margin, -margin);
                lineRect = [Utility getStandardRect:lineRect];
                [_pdfViewCtrl refresh:lineRect pageIndex:pageIndex];

                _lastPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:dibPoint pageIndex:pageIndex];
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    _isBegin = NO;
    _isMoving = NO;
    _isEndDrawingLine = YES;
    
    [_pdfViewCtrl lockRefresh];
    [self.annot resetAppearanceStream];
    [_pdfViewCtrl unlockRefresh];
    
    if (!self.added) {
        [self addAnnot:self.annot];
    }else{
        [self modifyAnnot:self.annot];
    }

    CGRect lineRect = [self getRectFromPoints:_linesArray];
    float scale = [_pdfViewCtrl getPageViewWidth:pageIndex] / 1000;
    float margin = self.annot.lineWidth * 3 * scale;
    lineRect = CGRectInset(lineRect, -margin, -margin);
    lineRect = [Utility getStandardRect:lineRect];
    [_pdfViewCtrl refresh:lineRect pageIndex:pageIndex];
    
    return YES;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (CGRect)getRectFromPoints:(NSArray *)array {
    NSMutableArray *pointsXArr = [NSMutableArray new];
    NSMutableArray *pointsYArr = [NSMutableArray new];
    
    for (NSUInteger i = 0; i < _linesArray.count; i++) {
        __weak NSMutableArray *pointArray = _linesArray[i][@"pointArray"];
        
        for (NSUInteger j = 0; j < pointArray.count - 1; j++) {
            __weak NSValue *pointValue = pointArray[j];
            CGPoint point = [pointValue CGPointValue];
            [pointsXArr addObject:@(point.x)];
            [pointsYArr addObject:@(point.y)];
        }
    }

    CGFloat xmaxValue = [[pointsXArr valueForKeyPath:@"@max.floatValue"] floatValue];
    CGFloat xminValue = [[pointsXArr valueForKeyPath:@"@min.floatValue"] floatValue];
    
    CGFloat ymaxValue = [[pointsYArr valueForKeyPath:@"@max.floatValue"] floatValue];
    CGFloat yminValue = [[pointsYArr valueForKeyPath:@"@min.floatValue"] floatValue];
    
    CGRect result = CGRectMake(xminValue, yminValue, fabs(xmaxValue-xminValue), fabs(ymaxValue-yminValue));

    return result;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_isEndDrawingLine) {
        return ;
    }
    if (self.annot != nil && ![self.annot isEmpty] && self.currentPageIndex == pageIndex) {
        [self.paintUtility drawPencilWith:_linesArray inContext:context];
    }
}

#pragma IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotInk) {
        if (self.annot) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.annot.type];
            [annotHandler addAnnot:self.annot addUndo:YES];
            self.annot = nil;
        }
    }
}

- (void)onAnnotLineWidthChanged:(unsigned int)lineWidth annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotInk) {
        if (self.annot) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.annot.type];
            [annotHandler addAnnot:self.annot addUndo:YES];
            self.annot = nil;
        }
    }
}

- (void)onAnnotOpacityChanged:(unsigned int)opacity annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotInk) {
        if (self.annot) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.annot.type];
            [annotHandler addAnnot:self.annot addUndo:YES];
            self.annot = nil;
        }
    }
}

@end
