/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PencilModule.h"
#import "PencilAnnotHandler.h"
#import "PencilToolHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>

@interface PencilModule ()

@property (nonatomic, weak) TbBaseItem *propertyItem;

@end

@implementation PencilModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Pencil";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
        PencilAnnotHandler* annotHandler = [[PencilAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];

        PencilToolHandler* toolHandler = [[PencilToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        [_extensionsManager registerAnnotPropertyListener:toolHandler];
    }
    return self;
}

- (void)loadModule {
    _extensionsManager.annotationToolsBar.pencileClicked = ^() {
        [self annotItemClicked];
    };
    [_extensionsManager registerAnnotPropertyListener:self];
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    id<IToolHandler> toolHandler = [_extensionsManager getToolHandlerByName:Tool_Pencil];
    [_extensionsManager setCurrentToolHandler:toolHandler];

    [_extensionsManager.toolSetBar removeAllItems];

    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    self.propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:FSAnnotInk]];
    [_extensionsManager.toolSetBar addItem:self.propertyItem displayPosition:Position_CENTER];

    self.propertyItem.onTapClick = ^(TbBaseItem *item) {
        CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_pdfViewCtrl];
        if (DEVICE_iPHONE) {
            [self->_extensionsManager showProperty:FSAnnotInk rect:rect inView:self->_pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:FSAnnotInk rect:item.contentView.bounds inView:item.contentView];
        }
    };

    [Utility showAnnotationType:FSLocalizedForKey(@"kPencil") type:FSAnnotInk pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];
    
    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.propertyItem.contentView.superview);
        make.centerX.mas_equalTo(self.propertyItem.contentView.superview.mas_centerX).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
    
    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(doneItem.contentView);
        make.centerX.equalTo(self.propertyItem.contentView.superview.mas_centerX).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];
}

#pragma mark - IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotInk) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

@end
