/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "EraseModule.h"
#import <FoxitRDK/FSPDFViewControl.h>

#import "EraseToolHandler.h"
#import "Utility.h"

@interface EraseModule ()

@property (nonatomic, weak) TbBaseItem *propertyItem;
@end

@implementation EraseModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Eraser";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];
        EraseToolHandler* toolHandler = [[EraseToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
    }
    return self;
}

- (void)loadModule {
    _extensionsManager.annotationToolsBar.eraserClicked = ^() {
        [self annotItemClicked];
    };
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    id<IToolHandler> toolHandler = [_extensionsManager getToolHandlerByName:Tool_Eraser];
    [_extensionsManager setCurrentToolHandler:toolHandler];

    [_extensionsManager.toolSetBar removeAllItems];

    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];

    __weak typeof(self) weakSelf = self;
    doneItem.onTapClick = ^(TbBaseItem *item) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf->_extensionsManager setCurrentToolHandler:nil];
        [strongSelf->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    self.propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[UIColor grayColor].rgbValue];
    [_extensionsManager.toolSetBar addItem:self.propertyItem displayPosition:Position_CENTER];
    self.propertyItem.onTapClick = ^(TbBaseItem *item) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:strongSelf->_pdfViewCtrl];
        [strongSelf->_extensionsManager showProperty:FSAnnotInk rect:rect inView:strongSelf->_pdfViewCtrl];
    };

    [Utility showAnnotationType:FSLocalizedForKey(@"kEraser") type:FSAnnotInk pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.propertyItem.contentView.superview);
        make.centerX.mas_equalTo(self.propertyItem.contentView.superview.mas_centerX).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
    
    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(doneItem.contentView);
        make.centerX.equalTo(self.propertyItem.contentView.superview.mas_centerX).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];
}

@end
