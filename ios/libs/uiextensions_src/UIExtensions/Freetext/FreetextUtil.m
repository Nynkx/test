/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FreetextUtil.h"

@implementation FreetextUtil

//+ (UIButton*)createDoneButton:(UITextView*)textView
//{
//    UIDeviceOrientation o = [[UIDevice currentDevice] orientation];
//    if (DEVICE_iPHONE) {
//        if (((STYLE_CELLWIDTH_IPHONE * STYLE_CELLHEIHGT_IPHONE) >= (375 * 667)) && (o == UIDeviceOrientationLandscapeLeft || o == UIDeviceOrientationLandscapeRight)) {
//            UIView *doneView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 1)];
//            doneView.backgroundColor = [UIColor clearColor];
//            textView.inputAccessoryView = doneView;
//        }
//    } else if (((STYLE_CELLWIDTH_IPHONE * STYLE_CELLHEIHGT_IPHONE) >= (375 * 667)) && (o == UIDeviceOrientationPortrait || o == UIDeviceOrientationPortraitUpsideDown)) {
//        UIView *doneView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 40)];
//        doneView.backgroundColor = [UIColor clearColor];
//        UIButton *doneBT = [UIButton buttonWithType:UIButtonTypeCustom];
//        [doneBT setBackgroundImage:[UIImage imageNamed:@"common_keyboard_done" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
//        [doneView addSubview:doneBT];
//        [doneBT mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(doneView.mas_right).offset(0);
//            make.top.equalTo(doneView.mas_top).offset(0);
//            make.size.mas_equalTo(CGSizeMake(40, 40));
//        }];
//        textView.inputAccessoryView = doneView;
//    }
//}

+ (void)setTextViewProperties:(UITextView*)textView UIExtensionsManager:(UIExtensionsManager *)extensionsManager isCallout:(BOOL)isCallout
{
    textView.textContainerInset = UIEdgeInsetsMake(2, -4, 2, -4);
    textView.layer.borderColor = isCallout ? [UIColor clearColor].CGColor : extensionsManager.config.defaultSettings.annotations.textbox.color.CGColor;
    textView.layer.borderWidth = 1;
    textView.backgroundColor = [UIColor clearColor];
    textView.textColor = ({
        UInt32 color = [extensionsManager getPropertyBarSettingColor:FSAnnotFreeText];
        float opacity = [extensionsManager getAnnotOpacity:FSAnnotFreeText];
        BOOL isMappingColorMode = (extensionsManager.pdfViewCtrl.colorMode == FSRendererColorModeMapping);
        if (isMappingColorMode && color == 0) {
            color = 16775930;
        }
        if (!isMappingColorMode && color == 16775930) {
            color = 0;
        }
        [UIColor colorWithRGB:color alpha:opacity];
    });
    textView.showsVerticalScrollIndicator = NO;
    textView.showsHorizontalScrollIndicator = NO;
    textView.scrollEnabled = NO;
    textView.clipsToBounds = NO;
}

#pragma mark - keyboard

+ (void)afterKeyboardShown:(NSNotification*)notification textView:(UITextView*)textView UIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex
{
    NSDictionary* info = [notification userInfo];
    FSPDFViewCtrl* pdfViewCtrl = extensionsManager.pdfViewCtrl;

    CGRect keyboardEndFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardFrame = [textView convertRect:keyboardEndFrame toView:nil];
    CGRect textFrame = textView.frame;
    
    CGPoint oldPvPoint = [pdfViewCtrl convertDisplayViewPtToPageViewPt:CGPointMake(0, 0) pageIndex:pageIndex];
    FSPointF *oldPdfPoint = [pdfViewCtrl convertPageViewPtToPdfPt:oldPvPoint pageIndex:pageIndex];
    
    CGRect dvAnnotRect = [pdfViewCtrl convertPageViewRectToDisplayViewRect:textFrame pageIndex:pageIndex];
    float positionY;
    positionY = SCREENHEIGHT - dvAnnotRect.origin.y - dvAnnotRect.size.height;
    
    if (positionY < keyboardFrame.size.height) {
        float dvOffsetY = keyboardFrame.size.height - positionY + 40;
        CGRect offsetRect = CGRectMake(0, 0, 100, dvOffsetY);
        
        CGRect pvRect = [pdfViewCtrl convertDisplayViewRectToPageViewRect:offsetRect pageIndex:pageIndex];
        FSRectF *pdfRect = [pdfViewCtrl convertPageViewRectToPdfRect:pvRect pageIndex:pageIndex];
        float pdfOffsetY = pdfRect.top - pdfRect.bottom;
        
        PDF_LAYOUT_MODE layoutMode = [pdfViewCtrl getPageLayoutMode];
        if (layoutMode == PDF_LAYOUT_MODE_SINGLE ||
            layoutMode == PDF_LAYOUT_MODE_TWO ||
            layoutMode == PDF_LAYOUT_MODE_TWO_LEFT ||
            layoutMode == PDF_LAYOUT_MODE_TWO_RIGHT ||
            layoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE) {
            float tmpPvOffset = pvRect.size.height;
            CGRect tmpPvRect = CGRectMake(0, 0, 10, tmpPvOffset);
            CGRect tmpDvRect = [pdfViewCtrl convertPageViewRectToDisplayViewRect:tmpPvRect pageIndex:pageIndex];
            [pdfViewCtrl setBottomOffset:tmpDvRect.size.height];
        } else if ([pdfViewCtrl isContinuous]) {
            if ([pdfViewCtrl getCurrentPage] == [pdfViewCtrl getPageCount] - 1) {
                float tmpPvOffset = pvRect.size.height;
                CGRect tmpPvRect = CGRectMake(0, 0, 10, tmpPvOffset);
                CGRect tmpDvRect = [pdfViewCtrl convertPageViewRectToDisplayViewRect:tmpPvRect pageIndex:pageIndex];
                [pdfViewCtrl setBottomOffset:tmpDvRect.size.height];
            } else {
                FSPointF *jumpPdfPoint = [[FSPointF alloc] init];
                [jumpPdfPoint set:oldPdfPoint.x y:oldPdfPoint.y - pdfOffsetY];
                [pdfViewCtrl gotoPage:pageIndex withDocPoint:jumpPdfPoint animated:YES];
            }
        }
    }
}

+ (void)adjustBottomOffset:(FSPDFViewCtrl *)pdfViewCtrl pageIndex:(int)pageIndex
{
    PDF_LAYOUT_MODE layoutMode = [pdfViewCtrl getPageLayoutMode];
    if (layoutMode == PDF_LAYOUT_MODE_SINGLE ||
        layoutMode == PDF_LAYOUT_MODE_TWO ||
        layoutMode == PDF_LAYOUT_MODE_TWO_LEFT ||
        layoutMode == PDF_LAYOUT_MODE_TWO_RIGHT ||
        layoutMode == PDF_LAYOUT_MODE_TWO_MIDDLE ||
        pageIndex == [pdfViewCtrl getPageCount] - 1) {
        [pdfViewCtrl setBottomOffset:0];
    }
}

+ (void)adjustTextViewFrame:(UITextView *)textView inPageView:(UIView *)pageView forMinPageInset:(CGFloat)inset {
    CGRect frame = textView.frame;
    CGSize constraintSize = CGSizeMake(frame.size.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraintSize];
    textView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, size.height);
    if (textView.frame.size.height >= (CGRectGetHeight(pageView.frame) - 20)) {
        [textView endEditing:YES];
    }
    if (textView.frame.size.width >= (CGRectGetWidth(pageView.frame) - 20)) {
        [textView endEditing:YES];
    }
    
    CGRect bounds = CGRectInset(pageView.bounds, inset, inset);
    if (!CGRectIntersectsRect(textView.frame, bounds)) {
        return;
    }
    if (CGRectGetMinX(textView.frame) < CGRectGetMinX(bounds)) {
        CGPoint center = textView.center;
        center.x += CGRectGetMinX(bounds) - CGRectGetMinX(textView.frame);
        textView.center = center;
    }
    if (CGRectGetMaxX(textView.frame) > CGRectGetMaxX(bounds)) {
        CGPoint center = textView.center;
        center.x -= CGRectGetMaxX(textView.frame) - CGRectGetMaxX(bounds);
        textView.center = center;
    }
    if (CGRectGetMinY(textView.frame) < CGRectGetMinY(bounds)) {
        CGPoint center = textView.center;
        center.y += CGRectGetMinY(bounds) - CGRectGetMinY(textView.frame);
        textView.center = center;
    }
    if (CGRectGetMaxY(textView.frame) > CGRectGetMaxY(bounds)) {
        CGPoint center = textView.center;
        center.y -= CGRectGetMaxY(textView.frame) - CGRectGetMaxY(bounds);
        textView.center = center;
    }
}

//for callout

+ (CGRect)calculateBorderRectWithInnerRect:(CGRect)innerRect startPoint:(CGPoint)startPoint kneePoint:(CGPoint)kneePoint
{
    CGRect lineRect;
    lineRect.origin.x = MIN(startPoint.x, kneePoint.x);
    lineRect.origin.y = MIN(startPoint.y, kneePoint.y);
    lineRect.size.width = ABS(startPoint.x - kneePoint.x);
    lineRect.size.height = ABS(startPoint.y - kneePoint.y);
    return CGRectUnion(lineRect, innerRect);
}

+ (CGRect)resetCalloutAppearanceWhenTextviewChanged:(FSPDFViewCtrl*)pdfViewCtrl annot:(FSFreeText*)annot textFrame:(CGRect)textFrame
{
    if (!annot || [annot isEmpty]) return CGRectZero;
    CGRect newInnerRect = textFrame;
    CGRect newBorderRect;
    
    CGRect oldBorderRect = [pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect oldInnerRect = [pdfViewCtrl convertPdfRectToPageViewRect:[annot getInnerRect] pageIndex:annot.pageIndex];
    FSPointFArray *calloutlinePoints = [annot getCalloutLinePoints];
    CGPoint startPoint = [pdfViewCtrl convertPdfPtToPageViewPt:[calloutlinePoints getAt:0] pageIndex:annot.pageIndex];
    CGPoint kneePoint = [pdfViewCtrl convertPdfPtToPageViewPt:[calloutlinePoints getAt:1] pageIndex:annot.pageIndex];
    CGPoint endPoint = [pdfViewCtrl convertPdfPtToPageViewPt:[calloutlinePoints getAt:2] pageIndex:annot.pageIndex];
    
    if (endPoint.x == oldInnerRect.origin.x || ABS(endPoint.x - ((CGRectGetWidth(oldInnerRect)/2 + oldInnerRect.origin.x))) < 0.5 || ABS(endPoint.y - ((CGRectGetHeight(oldInnerRect)/2 + oldInnerRect.origin.y))) < 0.5) {
        if (ABS(oldInnerRect.origin.x - endPoint.x) < 0.5 ||
            ABS(oldInnerRect.origin.x + oldInnerRect.size.width - endPoint.x) < 0.5) {
            endPoint.y = newInnerRect.origin.y + newInnerRect.size.height / 2.0;
            kneePoint.y = endPoint.y;
        } else if (ABS(oldInnerRect.origin.y + oldInnerRect.size.height - endPoint.y) < 0.5) {
            endPoint.y = newInnerRect.origin.y + newInnerRect.size.height;
            kneePoint.y = endPoint.y + 20.0;
        } else {
            endPoint.y = newInnerRect.origin.y;
            kneePoint.y = endPoint.y - 20.0;
        }
    }
    
    
    newBorderRect = [FreetextUtil calculateBorderRectWithInnerRect:newInnerRect startPoint:startPoint kneePoint:kneePoint];
    annot.fsrect = [pdfViewCtrl convertPageViewRectToPdfRect:newBorderRect pageIndex:annot.pageIndex];
    [annot setInnerRect:[pdfViewCtrl convertPageViewRectToPdfRect:newInnerRect pageIndex:annot.pageIndex]];
    FSPointFArray *points = [[FSPointFArray alloc] init];
    [points add:[pdfViewCtrl convertPageViewPtToPdfPt:startPoint pageIndex:annot.pageIndex]];
    [points add:[pdfViewCtrl convertPageViewPtToPdfPt:kneePoint pageIndex:annot.pageIndex]];
    [points add:[pdfViewCtrl convertPageViewPtToPdfPt:endPoint pageIndex:annot.pageIndex]];
    [annot setCalloutLinePoints:points];
    [pdfViewCtrl lockRefresh];
    [annot resetAppearanceStream];
    [pdfViewCtrl unlockRefresh];
    
    CGRect refreshRect = CGRectInset(CGRectUnion(oldBorderRect, newBorderRect), -30, -30);
    return refreshRect;
}

@end
