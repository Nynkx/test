/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FtAnnotHandler.h"
#import "FSAnnotAttributes.h"
#import "MenuItem.h"
#import "ShapeUtil.h"
#import "StringDrawUtil.h"
#import "FreetextUtil.h"

@interface FtAnnotHandler () <UITextViewDelegate>

@property (nonatomic, assign) BOOL shouldShowMenu;
@property (nonatomic, assign) BOOL shouldShowPropertyBar;
@property (nonatomic, strong) FSRectF *oldRect;

@end

@implementation FtAnnotHandler {
    UITextView *_textView;
    BOOL _isSaved;
    BOOL _keyboardShown;
    
    NSString* _savedText;
    
    //for callout
    BOOL _isMovingCalloutStartPoint;
    CGPoint _startPoint;
    CGPoint _kneePoint;
    CGPoint _endPoint;
    CGFloat _polylineLength;
}

- (id)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
//        self.isShowStyle = NO;
        self.editType = FSAnnotEditTypeFull;
        _isMovingCalloutStartPoint = NO;
        _startPoint = CGPointZero;
        _kneePoint = CGPointZero;
        _endPoint = CGPointZero;
        _polylineLength = 20.0;
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotFreeText;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionNone;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
        if (annot.canEdit) {
            options |= FSMenuOptionEdit;
        }
    }
    if (annot.canCopyText && [Utility canCopyText:self.extensionsManager.pdfViewCtrl] && self.extensionsManager.config.copyText) {
        options |= FSMenuOptionCopyText;
    }
    return options;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    [super onAnnotSelected:annot];
    _savedText = annot.contents;
    _isSaved = YES;
    if ([annot.intent isEqualToString:@"FreeTextCallout"]) {
        
        FSFreeText *freetext = [[FSFreeText alloc] initWithAnnot:annot];
        FSPointFArray *calloutPoints = [freetext getCalloutLinePoints];
        int numPoints = [calloutPoints getSize];
        if (numPoints < 2) {
            FSPointF *startPoint = [[FSPointF alloc] initWithX:0 Y:0];
            FSPointF *kneePoint = [[FSPointF alloc] initWithX:0 Y:0];
            FSPointF *endPoint = [[FSPointF alloc] initWithX:0 Y:0];
            
            FSPointFArray *points = [[FSPointFArray alloc] init];
            [points add:startPoint];
            [points add:kneePoint];
            [points add:endPoint];
            [freetext setCalloutLinePoints:points];
        }
    }
}

- (UIFont *)getUIFontForFreeText:(FSFreeText *)freeText {
    FSDefaultAppearance *appearance = [freeText getDefaultAppearance];
    float fontSize = [appearance getText_size] ?: 10;
    fontSize = [Utility convertWidth:fontSize fromPageViewToPDF:self.pdfViewCtrl pageIndex:freeText.pageIndex];
    FSFont *font = [appearance getFont];
    NSString *fontName = nil;
    if (font && ![font isEmpty]) {
        fontName = [font getName];
    }
    if (fontName.length == 0) {
        fontName = @"Helvetica";
    }
    return [self getSysFont:fontName size:fontSize];
}

- (void)edit {
    _isSaved = NO;
    FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:self.extensionsManager.currentAnnot];
    BOOL isCallout = [annot.intent isEqualToString:@"FreeTextCallout"];
    BOOL isTextbox = (annot.intent.length == 0);
    
    int pageIndex = annot.pageIndex;
    {
        CGRect textFrame;
        if (isCallout) {
            textFrame = [self.pdfViewCtrl convertPdfRectToPageViewRect:[annot getInnerRect] pageIndex:pageIndex];
        } else {
            textFrame = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        }
        _textView = [[UITextView alloc] initWithFrame:textFrame];
        _textView.delegate = self;
        if (isTextbox) {
            _textView.layer.borderColor = self.extensionsManager.config.defaultSettings.annotations.textbox.color.CGColor;
            _textView.layer.borderWidth = 1;
        }
        if (isCallout) {
            _textView.layer.borderColor = [UIColor clearColor].CGColor;
        }
        _textView.textContainerInset = UIEdgeInsetsMake(2, -4, 2, -4);
        _textView.backgroundColor = [UIColor clearColor];
        _textView.textColor = [UIColor colorWithRGB:annot.color alpha:annot.opacity];
        _textView.font = [self getUIFontForFreeText:annot];
        _textView.showsVerticalScrollIndicator = NO;
        _textView.showsHorizontalScrollIndicator = NO;
        _textView.scrollEnabled = NO;
        _textView.clipsToBounds = NO;
        [_textView becomeFirstResponder];
        _textView.text = annot.contents;
        [[self.pdfViewCtrl getPageView:pageIndex] addSubview:_textView];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasShown:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        if (isCallout || isTextbox) {
            annot.contents = nil;
            [annot resetAppearanceStream];
            CGRect refreshRect = CGRectInset(textFrame, -30, -30);;
            if (isCallout) {
                refreshRect = [FreetextUtil resetCalloutAppearanceWhenTextviewChanged:self.pdfViewCtrl annot:annot textFrame:textFrame];
            }
            self.annotImage = [Utility getAnnotImage:annot pdfViewCtrl:self.pdfViewCtrl];
            if (isCallout) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.pdfViewCtrl refresh:refreshRect pageIndex:pageIndex];
                });
            }else{
                [self.pdfViewCtrl refresh:refreshRect pageIndex:pageIndex needRender:NO];
            }
        }
        else{
            self.annotImage = nil;
            textFrame = CGRectInset(textFrame, -30, -30);
            [self.pdfViewCtrl refresh:textFrame pageIndex:pageIndex needRender:NO];
        }
    }
}

- (void)fixInvalidFontForFreeText:(FSFreeText *)annot {
    FSDefaultAppearance *ap = [annot getDefaultAppearance];
    if (!ap.font || [ap.font isEmpty] || ap.text_size == 0) {
        if (!ap.font || [ap.font isEmpty]) {
            FSFont *font = [[FSFont alloc] initWithFont_id:FSFontStdIDHelvetica];
            ap.font = font;
        }
        if (ap.text_size == 0) {
            ap.text_size = 10;
        }
        ap.flags = FSDefaultAppearanceFlagFont | FSDefaultAppearanceFlagTextColor | FSDefaultAppearanceFlagFontSize;
        [annot setDefaultAppearance:ap];
    }
}

- (void)endEdit:(BOOL *_Nonnull)shouldRemoveAnnot {
    *shouldRemoveAnnot = NO;
    
    if (!_isSaved) {
        _isSaved = YES;
        _textView.selectedRange = NSMakeRange(_textView.text.length, 0);
        FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:self.extensionsManager.currentAnnot];
        BOOL isCallout = [annot.intent isEqualToString:@"FreeTextCallout"];
        BOOL isTextBox = [annot.subject isEqualToString:@"Textbox"];
        if (_savedText.length == 0 && (!isCallout && !isTextBox)) {
            *shouldRemoveAnnot = YES;
        } else {
            annot.contents = _savedText;
            [self fixInvalidFontForFreeText:annot];
            CGRect textFrame = _textView.frame;
            
            // move annot if exceed page edge, as the annot size may be changed after reset ap (especially for Chinese char, the line interspace is changed)
            if (textFrame.origin.y + textFrame.size.height > [self.pdfViewCtrl getPageView:annot.pageIndex].bounds.size.height) {
                textFrame.origin.y = [self.pdfViewCtrl getPageView:annot.pageIndex].bounds.size.height - textFrame.size.height;
            }
            
            if (!isCallout) {
                annot.fsrect = [self.pdfViewCtrl convertPageViewRectToPdfRect:_textView.frame pageIndex:annot.pageIndex];
            }
            [self.pdfViewCtrl lockRefresh];
            [annot resetAppearanceStream];
            [self.pdfViewCtrl unlockRefresh];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->_textView resignFirstResponder];
            [self->_textView removeFromSuperview];
            self->_textView = nil;
            [[NSNotificationCenter defaultCenter] removeObserver:self];
        });
    }
}

- (void)deleteAnnot {
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    Task *task = [[Task alloc] init];
    task.run = ^() {
        [self removeAnnot:annot];
    };
    [self.extensionsManager.taskServer executeSync:task];
    [self.extensionsManager setCurrentAnnot:nil];
}

- (void)showStyle {
    FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:self.extensionsManager.currentAnnot];
//    BOOL isTextboxOrCallout = (annot.intent.length == 0) || [annot.intent isEqualToString:@"FreeTextCallout"];
    NSArray *colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.extensionsManager.propertyBar setColors:colors];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_FONTNAME | PROPERTY_FONTSIZE frame:CGRectZero];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_FONTNAME | PROPERTY_FONTSIZE lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];

    [self fixInvalidFontForFreeText:annot];
    FSDefaultAppearance *appearance = [annot getDefaultAppearance];
    float fontSize = [appearance getText_size];
    FSFont *font = [appearance getFont];
    NSString *fontName = @"";
    if (font && ![font isEmpty]) {
        fontName = [font getName];
    }
    [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTNAME stringValue:fontName];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_FONTSIZE floatValue:fontSize];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];
    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
//    self.isShowStyle = YES;
    self.shouldShowMenu = NO;
    self.shouldShowPropertyBar = YES;
}

- (void)onAnnotDeselected:(FSAnnot *)annot {
    BOOL shouldRemoveAnnot;
    [self endEdit:&shouldRemoveAnnot];
    if (shouldRemoveAnnot) {
        [self removeAnnot:annot];
    } else {
        [super onAnnotDeselected:annot];
    }
}

- (void)saveCalloutPoints:(FSFreeText *)annot {
    FSPointFArray *points = [annot getCalloutLinePoints];
    int pageindex = annot.pageIndex;
    _startPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[points getAt:0] pageIndex:pageindex];
    _kneePoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[points getAt:1] pageIndex:pageindex];
    _endPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[points getAt:2] pageIndex:pageindex];
}

- (BOOL)adjustCalloutDragpoint:(FSFreeText*)annot translationPoint:(CGPoint)translationPoint {
    CGFloat tw = translationPoint.x;
    CGFloat th = translationPoint.y;

    [self saveCalloutPoints:annot];
    _startPoint.x += tw;
    _startPoint.y += th;
    
    CGRect innerRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[annot getInnerRect] pageIndex:annot.pageIndex];
    CGRect realPageRect = [self.pdfViewCtrl getPageView:annot.pageIndex].bounds;
    if ( CGRectContainsPoint(innerRect, _startPoint) || (!CGRectContainsPoint(realPageRect, _startPoint)) ) {
        return NO;
    }
    
    if (_startPoint.y < innerRect.origin.y) {
        //top
        _endPoint.x = innerRect.origin.x + innerRect.size.width / 2.0;
        _endPoint.y = innerRect.origin.y;
        _kneePoint.x = _endPoint.x;
        _kneePoint.y = _endPoint.y - _polylineLength;
    } else if (_startPoint.y > innerRect.origin.y + innerRect.size.height) {
        //bottom
        _endPoint.x = innerRect.origin.x + innerRect.size.width / 2.0;
        _endPoint.y = innerRect.origin.y + innerRect.size.height;
        _kneePoint.x = _endPoint.x;
        _kneePoint.y = _endPoint.y + _polylineLength;
    } else if (_startPoint.x < innerRect.origin.x) {
        //left
        _endPoint.x = innerRect.origin.x;
        _endPoint.y = innerRect.origin.y + innerRect.size.height / 2.0;
        _kneePoint.x = _endPoint.x - _polylineLength;
        _kneePoint.y = _endPoint.y;
    } else {
        //right
        _endPoint.x = innerRect.origin.x + innerRect.size.width;
        _endPoint.y = innerRect.origin.y + innerRect.size.height / 2.0;
        _kneePoint.x = _endPoint.x + _polylineLength;
        _kneePoint.y = _endPoint.y;
    }
    return YES;
}

- (CGRect)calculateRectWithEditType:(CGRect)rect translationPoint:(CGPoint)translationPoint {
    CGFloat tw = translationPoint.x;
    CGFloat th = translationPoint.y;
    if (self.editType == FSAnnotEditTypeLeftTop || self.editType == FSAnnotEditTypeMiddleTop || self.editType == FSAnnotEditTypeRightTop) {
        // top
        rect.origin.y += th;
        rect.size.height -= th;
    } else if (self.editType == FSAnnotEditTypeLeftBottom || self.editType == FSAnnotEditTypeMiddleBottom || self.editType == FSAnnotEditTypeRightBottom) {
        // bottom
        rect.size.height += th;
    }
    if (self.editType == FSAnnotEditTypeLeftTop || self.editType == FSAnnotEditTypeLeftMiddle || self.editType == FSAnnotEditTypeLeftBottom) {
        // left
        rect.origin.x += tw;
        rect.size.width -= tw;
    } else if (self.editType == FSAnnotEditTypeRightTop || self.editType == FSAnnotEditTypeRightMiddle || self.editType == FSAnnotEditTypeRightBottom) {
        // right
        rect.size.width += tw;
    }
    return rect;
}

- (CGRect)correctBorderRect:(CGRect)borderRect pageIndex:(int)pageIndex
{
    CGRect realPageRect = [self.pdfViewCtrl getPageView:pageIndex].bounds;
    FSRectF *pageViewRect = [Utility CGRect2FSRectF:realPageRect];
    FSRectF *rect = [Utility CGRect2FSRectF:borderRect];
    if (borderRect.size.width <= realPageRect.size.width &&
        borderRect.size.height <= realPageRect.size.height) {
        if (rect.left < pageViewRect.left) {
            float diferrence = pageViewRect.left - rect.left + 5;
            rect.left += diferrence;
            rect.right += diferrence;
        }
        if (rect.top < pageViewRect.top) {
            float diferrence = pageViewRect.top - rect.top + 5;
            rect.top += diferrence;
            rect.bottom += diferrence;
        }
        if (rect.right > pageViewRect.right) {
            float diferrence = rect.right - pageViewRect.right + 5;
            rect.right -= diferrence;
            rect.left -= diferrence;
        }
        if (rect.bottom > pageViewRect.bottom) {
            float diferrence = rect.bottom - pageViewRect.bottom + 5;
            rect.top -= diferrence;
            rect.bottom -= diferrence;
        }
    }
    float _minWidth = 10;
    float _minHeight = 10;
    if ((rect.left < _minWidth && rect.left < self.oldRect.left) ||
        (rect.right > [self.pdfViewCtrl getPageViewWidth:pageIndex] - _minWidth && rect.right > self.oldRect.right) ||
        (rect.right - rect.left < _minWidth) ||
        (rect.bottom > [self.pdfViewCtrl getPageViewHeight:pageIndex] - _minHeight && rect.bottom > self.oldRect.bottom) ||
        (rect.top < _minHeight && rect.top < self.oldRect.top) ||
        (rect.bottom - rect.top < _minHeight)) {
        return CGRectZero;
    }
    CGRect newRect = [Utility FSRectF2CGRect:rect];
    return newRect;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if ([_textView isFirstResponder]) return NO;
    BOOL canMove = [self canMoveAnnot:annot];
    BOOL canResize = [self canResizeAnnot:annot];
    if (!canMove && !canResize) {
        if (self.extensionsManager.currentAnnot) {
            [self showMenu];
        }
        return YES;
    }
    if (![Utility isAnnot:annot uniqueToAnnot:self.extensionsManager.currentAnnot]) {
        return NO;
    }
    FSFreeText *freetext = [[FSFreeText alloc] initWithAnnot:annot];
    BOOL isCallout = [annot.intent isEqualToString:@"FreeTextCallout"];
    BOOL isTextbox = (annot.intent.length == 0);

    if (recognizer.state == UIGestureRecognizerStateBegan) {
        //state began
        self.shouldShowMenu = self.extensionsManager.shouldShowMenu;
        self.shouldShowPropertyBar = self.extensionsManager.shouldShowPropertyBar;
        [self hideMenu];
        [self.extensionsManager hidePropertyBar];
        
        if (isTextbox) {
            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
            UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
            CGPoint point = [recognizer locationInView:pageView];
            self.editType = (FSAnnotEditType) [ShapeUtil getEditTypeWithPoint:point rect:CGRectInset(pvRect, -12, -12) defaultEditType:FSAnnotEditTypeFull];
        } else if (isCallout) {
            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[freetext getInnerRect] pageIndex:annot.pageIndex];
            UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
            CGPoint point = [recognizer locationInView:pageView];
            self.editType = (FSAnnotEditType) [ShapeUtil getEditTypeWithPoint:point rect:CGRectInset(pvRect, -12, -12) defaultEditType:FSAnnotEditTypeFull];

            if (self.editType == FSAnnotEditTypeFull) {
                FSPointFArray *calloutPoints = [freetext getCalloutLinePoints];
                if ([calloutPoints getSize] > 0) {
                    _startPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[calloutPoints getAt:0] pageIndex:annot.pageIndex];
                }
                CGFloat diff1 = ABS(_startPoint.x - point.x) + ABS(_startPoint.y - point.y);
                const CGFloat tolerance = 25.0;
                _isMovingCalloutStartPoint = (diff1 < tolerance);
            }
        } else {
            self.editType = FSAnnotEditTypeFull;
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        //state changed
        self.oldRect = annot.fsrect;
        CGPoint translationPoint = [recognizer translationInView:[self.pdfViewCtrl getPageView:pageIndex]];
        [recognizer setTranslation:CGPointZero inView:[self.pdfViewCtrl getPageView:pageIndex]];
        if (!annot.canModify) {
            return YES;
        }
        CGFloat tw = translationPoint.x;
        CGFloat th = translationPoint.y;
        
        CGRect borderRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
        CGRect newRect;
        
        if (isCallout) {
            [self saveCalloutPoints:freetext];
            CGRect innerRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[freetext getInnerRect] pageIndex:annot.pageIndex];

            if (_isMovingCalloutStartPoint) {
                if (![self adjustCalloutDragpoint:freetext translationPoint:translationPoint]) {
                    return NO;
                }
            } else {
                CGRect newInnerRect;
                if (self.editType == FSAnnotEditTypeFull) {
                    newInnerRect = innerRect;
                    newInnerRect.origin.x += tw;
                    newInnerRect.origin.y += th;
                } else {
                    newInnerRect = [self calculateRectWithEditType:innerRect translationPoint:translationPoint];
                    if (newInnerRect.size.height < _polylineLength || newInnerRect.size.width < _polylineLength) {
                        return NO;
                    }
                }
                //calculate new points
                if (_endPoint.x <= innerRect.origin.x + 2.0) {
                    //left
                    _endPoint.x = newInnerRect.origin.x;
                    _endPoint.y = newInnerRect.origin.y + newInnerRect.size.height / 2.0;
                    _kneePoint.x = _endPoint.x - _polylineLength;
                    _kneePoint.y = _endPoint.y;
                } else if (_endPoint.x >= (innerRect.origin.x + innerRect.size.width) - 2.0) {
                    //right
                    _endPoint.x = newInnerRect.origin.x + newInnerRect.size.width;
                    _endPoint.y = newInnerRect.origin.y + newInnerRect.size.height / 2.0;
                    _kneePoint.x = _endPoint.x + _polylineLength;
                    _kneePoint.y = _endPoint.y;
                } else if (_endPoint.y <= innerRect.origin.y + 2.0) {
                    //top
                    _endPoint.y = newInnerRect.origin.y;
                    _endPoint.x = newInnerRect.origin.x + newInnerRect.size.width / 2.0;
                    _kneePoint.y = _endPoint.y - _polylineLength;
                    _kneePoint.x = _endPoint.x;
                } else {
                    //bottom
                    _endPoint.y = newInnerRect.origin.y + newInnerRect.size.height;
                    _endPoint.x = newInnerRect.origin.x + newInnerRect.size.width / 2.0;
                    _kneePoint.y = _endPoint.y + _polylineLength;
                    _kneePoint.x = _endPoint.x;
                }
                innerRect = newInnerRect;
            }
            
            CGRect realPageRect = [self.pdfViewCtrl getPageView:annot.pageIndex].bounds;
            borderRect = [FreetextUtil calculateBorderRectWithInnerRect:innerRect startPoint:_startPoint kneePoint:_kneePoint];
            if ( !CGRectContainsRect(realPageRect, borderRect) ) {
                return NO;
            }
            annot.fsrect = [self.pdfViewCtrl convertPageViewRectToPdfRect:borderRect pageIndex:pageIndex];
            FSFreeText *freetext = [[FSFreeText alloc] initWithAnnot:annot];
            [freetext setInnerRect:[self.pdfViewCtrl convertPageViewRectToPdfRect:innerRect pageIndex:pageIndex]];
            FSPointFArray *points = [[FSPointFArray alloc] init];
            [points add:[self.pdfViewCtrl convertPageViewPtToPdfPt:_startPoint pageIndex:pageIndex]];
            [points add:[self.pdfViewCtrl convertPageViewPtToPdfPt:_kneePoint pageIndex:pageIndex]];
            [points add:[self.pdfViewCtrl convertPageViewPtToPdfPt:_endPoint pageIndex:pageIndex]];
            [freetext setCalloutLinePoints:points];
        } else {
            if (self.editType == FSAnnotEditTypeFull) {
                borderRect.origin.x += tw;
                borderRect.origin.y += th;
            } else {
                borderRect = [self calculateRectWithEditType:borderRect translationPoint:translationPoint];
            }
            newRect = [self correctBorderRect:borderRect pageIndex:pageIndex];
            if (CGRectEqualToRect(newRect, CGRectZero)) {
                [self.pdfViewCtrl refresh:pageIndex];
                return NO;
            }
            annot.fsrect = [self.pdfViewCtrl convertPageViewRectToPdfRect:newRect pageIndex:pageIndex];
        }
        
        // change annot's appearance and refresh
        CGRect oldRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.oldRect pageIndex:pageIndex];
        newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        CGRect allRect = CGRectUnion(newRect, oldRect);
        allRect = CGRectInset(allRect, -30, -30);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.pdfViewCtrl lockRefresh];
            [annot resetAppearanceStream];
            [self.pdfViewCtrl unlockRefresh];
            self.annotImage = [Utility getAnnotImage:annot pdfViewCtrl:self.pdfViewCtrl];
            [self.pdfViewCtrl refresh:allRect pageIndex:pageIndex needRender:NO];
        });
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        //state ended or candelled
        self.editType = FSAnnotEditTypeUnknown;
        _isMovingCalloutStartPoint = NO;
        if (annot.canModify) {
            [self modifyAnnot:annot addUndo:NO];
        }

        CGRect newRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
        CGRect oldRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:self.oldRect pageIndex:annot.pageIndex];
        if (self.shouldShowMenu) {
            [self showMenu];
        } else if (self.shouldShowPropertyBar) {
            [self.extensionsManager showPropertyBar];
        }

        newRect = CGRectUnion(newRect, oldRect);
        newRect = CGRectInset(newRect, -30, -30);
        [self.pdfViewCtrl refresh:pageIndex];
    }
    return YES;
}

- (UIFont *)getSysFont:(NSString *)name size:(float)size {
    UIFont *font = [UIFont fontWithName:[Utility convert2SysFontString:name] size:size];
    if (!font) {
        font = [UIFont systemFontOfSize:size];
    }
    return font;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    if (![annot isMarkup] && pageIndex == annot.pageIndex && [Utility isAnnot:annot uniqueToAnnot:self.annot])
        return;
    FSFreeText *freetext = [[FSFreeText alloc] initWithAnnot:annot];
    if (pageIndex == annot.pageIndex) {
        if (_textView) {
            if (([annot.intent isEqualToString:@"FreeTextCallout"] || annot.intent.length == 0)&& self.annotImage) {
                CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
                
                CGContextSaveGState(context);
                CGRect rect = CGRectMake(ceilf(pvRect.origin.x), ceilf(pvRect.origin.y), ceilf(pvRect.size.width), ceilf(pvRect.size.height));
                CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
                CGContextTranslateCTM(context, 0, rect.size.height);
                CGContextScaleCTM(context, 1.0, -1.0);
                CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
                CGContextDrawImage(context, rect, [self.annotImage CGImage]);
                CGContextRestoreGState(context);
            } else {
                [self textViewDidChange:_textView];
            }
        } else {
            CGRect pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect  pageIndex:annot.pageIndex];
            self.oldRect = annot.fsrect;

            if (self.annotImage) {
                CGContextSaveGState(context);
                CGRect rect = CGRectMake(ceilf(pvRect.origin.x), ceilf(pvRect.origin.y), ceilf(pvRect.size.width), ceilf(pvRect.size.height));
                CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
                CGContextTranslateCTM(context, 0, rect.size.height);
                CGContextScaleCTM(context, 1.0, -1.0);
                CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
                CGContextDrawImage(context, rect, [self.annotImage CGImage]);
                CGContextRestoreGState(context);
            }

            if (self.extensionsManager.currentAnnot == annot) {
                BOOL isTextboxOrCallout = (annot.intent.length == 0) || [annot.intent isEqualToString:@"FreeTextCallout"];
                if ([annot.intent isEqualToString:@"FreeTextCallout"]) {
                    pvRect = [self.pdfViewCtrl convertPdfRectToPageViewRect:[freetext getInnerRect] pageIndex:annot.pageIndex];
                }
                
                CGRect rect = isTextboxOrCallout ? CGRectInset(pvRect, -5, -5) : CGRectInset(pvRect, -2, -2);
                CGContextSetLineWidth(context, 2.0);
                CGFloat dashArray[] = {3, 3, 3, 3};
                CGContextSetLineDash(context, 3, dashArray, 4);
                CGContextSetStrokeColorWithColor(context, [[UIColor colorWithRGB:annot.color] CGColor]);
                CGContextStrokeRect(context, rect);

                if (isTextboxOrCallout) {
                    UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                    CGFloat dragWidth = dragDot.size.width;
                    NSMutableArray* moveablePointArray = (NSMutableArray*)[ShapeUtil getMovePointInRect:rect];
                    if ([annot.intent isEqualToString:@"FreeTextCallout"]) {
                        FSPointFArray *calloutPoints = [freetext getCalloutLinePoints];
                        if ([calloutPoints getSize] > 0) {
                            _startPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[calloutPoints getAt:0] pageIndex:annot.pageIndex];
                        }

                        [moveablePointArray addObject:[NSValue valueWithCGRect:CGRectMake(_startPoint.x - dragWidth / 2.0, _startPoint.y - dragWidth / 2.0, 1.0, 1.0)]];
                    }
                    
                    [moveablePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        CGRect dotRect = [obj CGRectValue];
                        CGPoint point = CGPointMake(dotRect.origin.x, dotRect.origin.y);
                        [dragDot drawAtPoint:point];
                    }];
                }
            }
        }
    }
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    CGSize oneSize = [Utility getTestSize:textView.font];
    CGPoint point = textView.frame.origin;
    
    if (!self.extensionsManager.currentAnnot) return;

    FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:self.extensionsManager.currentAnnot];

    int pageIndex = annot.pageIndex;
    BOOL isCallout = [annot.intent isEqualToString:@"FreeTextCallout"];
    if (annot.intent.length == 0) {
        CGRect frame = textView.frame;
        CGSize constraintSize = CGSizeMake(frame.size.width, MAXFLOAT);
        CGSize size = [textView sizeThatFits:constraintSize];
        textView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, MAX(size.height, frame.size.height));
    } else if (isCallout) {
        UIView *pageView = [self.pdfViewCtrl getPageView:annot.pageIndex];
        [FreetextUtil adjustTextViewFrame:textView inPageView:pageView forMinPageInset:5];
    } else {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        CGSize size = [textView.text boundingRectWithSize:CGSizeMake([self.pdfViewCtrl getPageViewWidth:pageIndex] - point.x - oneSize.width, 99999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : textView.font, NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
        size.width += oneSize.width;
        size.height += oneSize.height;
        CGRect frame = textView.frame;
        frame.size = size;
        textView.frame = frame;
    }
    float textViewHeight = textView.frame.origin.y + textView.frame.size.height;
    if (textViewHeight >= ([self.pdfViewCtrl getPageViewHeight:pageIndex] - 20)) {
        CGRect textViewFrame = textView.frame;
        textViewFrame.origin.y -= (textViewHeight - [self.pdfViewCtrl getPageViewHeight:pageIndex]);
        textView.frame = textViewFrame;
    }
    CGRect textViewFrame = textView.frame;
    if (isCallout) {
        annot.contents = nil;
        CGRect refreshRect = [FreetextUtil resetCalloutAppearanceWhenTextviewChanged:self.pdfViewCtrl annot:annot textFrame:textViewFrame];
        self.annotImage = [Utility getAnnotImage:annot pdfViewCtrl:self.pdfViewCtrl];
        textView.frame = [self.pdfViewCtrl convertPdfRectToPageViewRect:[annot getInnerRect] pageIndex:annot.pageIndex];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.pdfViewCtrl refresh:refreshRect pageIndex:pageIndex];
        });
    } else {
        annot.fsrect = [self.pdfViewCtrl convertPageViewRectToPdfRect:textViewFrame pageIndex:pageIndex];
    }
    _savedText = textView.text;
    if (textView.frame.size.height >= ([self.pdfViewCtrl getPageViewHeight:pageIndex] - 20)) {
        [textView endEditing:YES];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [_textView removeFromSuperview];
    _textView = nil;
    BOOL endEdit;
    [self endEdit:&endEdit];
    self.annotImage = [Utility getAnnotImage:self.annot pdfViewCtrl:self.pdfViewCtrl];
}

#pragma mark - keyboard
- (void)keyboardWasShown:(NSNotification *)notification {
    if (_keyboardShown)
        return;
    _keyboardShown = YES;
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    
    [FreetextUtil afterKeyboardShown:notification textView:_textView UIExtensionsManager:self.extensionsManager pageIndex:annot.pageIndex];
}

- (void)keyboardWasHidden:(NSNotification *)notification {
    int pageIndex = [self.pdfViewCtrl getCurrentPage];
    [self.pdfViewCtrl refresh:pageIndex];
    _keyboardShown = NO;
    
    [FreetextUtil adjustBottomOffset:self.pdfViewCtrl pageIndex:pageIndex];
}

@end
