/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FreetextModule.h"
#import <FoxitRDK/FSPDFViewControl.h>

#import "FtAnnotHandler.h"
#import "FtToolHandler.h"
#import "Utility.h"

@interface FreetextModule ()

@property (nonatomic, weak) TbBaseItem *propertyItem;

@end

@implementation FreetextModule {
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Freetext";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        [_extensionsManager registerAnnotPropertyListener:self];
        [self loadModule];
        FtAnnotHandler* annotHandler = [[FtAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];

        FtToolHandler* toolHandler = [[FtToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        [_extensionsManager registerRotateChangedListener:toolHandler];
        [_extensionsManager registerUndoEventListener:toolHandler];
        [_extensionsManager.pdfViewCtrl registerScrollViewEventListener:toolHandler];
    }
    return self;
}

- (void)loadModule {
    _extensionsManager.annotationToolsBar.typerwriterClicked = ^() {
        [self annotItemClicked];
    };
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    [_extensionsManager setCurrentToolHandler:[_extensionsManager getToolHandlerByName:Tool_Freetext]];
    [_extensionsManager.toolSetBar removeAllItems];
    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    _propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:FSAnnotFreeText]];
    [_extensionsManager.toolSetBar addItem:_propertyItem displayPosition:Position_CENTER];
    _propertyItem.onTapClick = ^(TbBaseItem *item) {
        CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_extensionsManager.pdfViewCtrl];

        if (DEVICE_iPHONE) {
            [self->_extensionsManager showProperty:FSAnnotFreeText rect:rect inView:self->_extensionsManager.pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:FSAnnotFreeText rect:item.contentView.bounds inView:item.contentView];
        }

    };

    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_continue")];
    } else {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_single")];
    }
    continueItem.tag = 3;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_extensionsManager.pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            item.imageNormal = ImageNamed(@"annot_continue");
            item.imageSelected = ImageNamed(@"annot_continue");
        } else {
            item.imageNormal = ImageNamed(@"annot_single");
            item.imageSelected = ImageNamed(@"annot_single");
        }

        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_extensionsManager.pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };

 
    [Utility showAnnotationType:FSLocalizedForKey(@"kTypewriter") type:FSAnnotFreeText pdfViewCtrl:_extensionsManager.pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.propertyItem.contentView.superview);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];
    
    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.left.equalTo(self.propertyItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];
    
    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.right.equalTo(self.propertyItem.contentView.mas_left).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_extensionsManager.pdfViewCtrl];
}

#pragma mark - IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotFreeText && [[_extensionsManager.currentToolHandler getName] isEqualToString:Tool_Freetext]) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

@end
