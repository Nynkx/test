/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIExtensionsManager.h"
#import <FoxitRDK/FSPDFViewControl.h>

@interface FreetextUtil : NSObject

//+ (UIButton*)createDoneButton:(UITextView*)textView;
+ (void)setTextViewProperties:(UITextView*)textView UIExtensionsManager:(UIExtensionsManager *)extensionsManager isCallout:(BOOL)isCallout;
+ (void)afterKeyboardShown:(NSNotification*)notification textView:(UITextView*)textView UIExtensionsManager:(UIExtensionsManager *)extensionsManager pageIndex:(int)pageIndex;
+ (void)adjustBottomOffset:(FSPDFViewCtrl *)pdfViewCtrl pageIndex:(int)pageIndex;
+ (CGRect)calculateBorderRectWithInnerRect:(CGRect)innerRect startPoint:(CGPoint)startPoint kneePoint:(CGPoint)kneePoint;
+ (CGRect)resetCalloutAppearanceWhenTextviewChanged:(FSPDFViewCtrl*)pdfViewCtrl annot:(FSFreeText*)annot textFrame:(CGRect)textFrame;
+ (void)adjustTextViewFrame:(UITextView *)textView inPageView:(UIView *)pageView forMinPageInset:(CGFloat)inset;

@end
