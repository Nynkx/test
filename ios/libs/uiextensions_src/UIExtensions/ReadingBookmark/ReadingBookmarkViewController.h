/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PanelController.h"
#import "ReadingBookmarkListCell.h"
#import <FoxitRDK/FSPDFViewControl.h>
#import <UIKit/UIKit.h>
@class FileDetailViewController;

typedef void (^ReadingBookmarkGotoPageHandler)(int page);
typedef void (^ReadingBookmarkSelectionHandler)(void);
typedef void (^ReadingBookmarkDeleteHandler)(void);

@interface ReadingBookmarkViewController : UITableViewController <IPanelChangedListener> {
    __weak FSReadingBookmark *selectBookmark;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
}

@property (nonatomic, copy) ReadingBookmarkGotoPageHandler bookmarkGotoPageHandler;
@property (nonatomic, copy) ReadingBookmarkSelectionHandler bookmarkSelectionHandler;
@property (nonatomic, copy) ReadingBookmarkDeleteHandler bookmarkDeleteHandler;

//a common way to do for bookmark and annotation

@property (nonatomic, strong) NSMutableArray *arrayBookmarks;
@property (nonatomic, assign) BOOL isContentEditing;
@property (nonatomic, assign) BOOL isShowMore;
@property (nonatomic, strong) NSIndexPath *moreIndexPath;
@property (nonatomic, strong) NSObject *currentVC;
@property (nonatomic, weak) FSPanelController *panelController;

- (id)initWithStyle:(UITableViewStyle)style pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl panelController:(FSPanelController *)panelController;
- (void)loadData;
- (void)clearData:(BOOL)fromPDF;
- (NSUInteger)getBookmarkCount;

- (void)addBookmark:(FSReadingBookmark *)newBookmark;
- (void)renameBookmark:(FSReadingBookmark *)renameBookmark;

- (void)renameBookmarkWithIndex:(NSInteger)index;
- (void)deleteBookmarkWithIndex:(NSInteger)index;

- (void)hideCellEditView;

@end
