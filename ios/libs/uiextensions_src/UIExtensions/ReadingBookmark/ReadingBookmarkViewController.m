/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ReadingBookmarkViewController.h"
#import "UIExtensionsManager.h"
#import "AlertView.h"
#import "AnnotationListCell.h"
#import "AnnotationListMore.h"
#import "UniversalEditViewController.h"
#import "FSNavigationController.h"

@interface ReadingBookmarkViewController () <ReadingBookmarkListCellDelegate, TSAlertViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic) BOOL statusBarHidden;

@property (nonatomic, strong) UIViewController *originalPresentedViewController;

- (void)refreshInterface;

@end

@implementation ReadingBookmarkViewController

@synthesize bookmarkGotoPageHandler = _bookmarkGotoPageHandler;
@synthesize bookmarkSelectionHandler = _bookmarkSelectionHandler;
@synthesize isContentEditing = _isContentEditing;
@synthesize arrayBookmarks = _arrayBookmarks;

#pragma mark - View lifecycle

- (id)initWithStyle:(UITableViewStyle)style pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl panelController:(FSPanelController *)panelController {
    self = [super initWithStyle:style];
    if (self) {
        _pdfViewCtrl = pdfViewCtrl;
        _panelController = panelController;
        self.arrayBookmarks = [[NSMutableArray alloc] init];
        _bookmarkGotoPageHandler = nil;
        _bookmarkSelectionHandler = nil;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationChange) name:ORIENTATIONCHANGED object:nil];

        self.tableView.allowsSelectionDuringEditing = YES;
        self.moreIndexPath = nil;
        self.tapGesture = nil;

        [panelController registerPanelChangedListener:self];

        _statusBarHidden = false;
    }
    return self;
}

- (void)dealloc {
    self.arrayBookmarks = nil;
}

- (void)deviceOrientationChange {
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[ReadingBookmarkListCell class] forCellReuseIdentifier:@"bookmarkcell"];
    [self refreshInterface];
        self.view.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.separatorColor = DividingLineColor;
}

- (void)viewDidLayoutSubviews {
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }

    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}
*/

- (void)onPanelChanged:(BOOL)isHidden {
    if (isHidden) {
        [self hideCellEditView];
    }
}

#pragma mark - Table view data source and delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayBookmarks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReadingBookmarkListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bookmarkcell"];
    cell.delegate = self;
    cell.backgroundColor = ThemeViewBackgroundColor;
    //    cell.editView.indexPath = indexPath;

    FSReadingBookmark *bookmarkItem = [_arrayBookmarks objectAtIndex:indexPath.row];
    cell.pageLabel.text = @"";
    @try {
        cell.pageLabel.text = [bookmarkItem getTitle];
    } @catch (NSException *exception) {
        cell.pageLabel.text = @"";
    } @finally {
        
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    //    cell.detailButton.object = cell.editView;
    if ([Utility canAssemble:_pdfViewCtrl]) {
        cell.detailButton.enabled = YES;
        cell.detailButton.hidden = NO;
    } else {
        cell.detailButton.enabled = NO;
        cell.detailButton.hidden = YES;
    }
    return cell;
}

#pragma mark <ReadingBookmarkListCellDelegate>

- (void)readingBookmarkListCellWillShowEditView:(ReadingBookmarkListCell *)cell {
    [self hideCellEditView];
}

- (void)readingBookmarkListCellDidShowEditView:(ReadingBookmarkListCell *)cell {
    self.isShowMore = YES;
    self.moreIndexPath = [self.tableView indexPathForCell:cell];
    if (!self.tapGesture) {
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self.view addGestureRecognizer:self.tapGesture];
    }
    self.tapGesture.enabled = YES;
}

- (void)readingBookmarkListCellDelete:(ReadingBookmarkListCell *)cell {
    [self deleteBookmarkWithIndex:[self.tableView indexPathForCell:cell].item];
}

- (void)readingBookmarkListCellRename:(ReadingBookmarkListCell *)cell {
    [self renameBookmarkWithIndex:[self.tableView indexPathForCell:cell].item];
}

- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
    assert(self.isShowMore);
    assert(self.moreIndexPath);
    [self hideCellEditView];
}

- (void)hideCellEditView {
    if (self.isShowMore) {
        assert(self.moreIndexPath);
        ReadingBookmarkListCell *cell = (ReadingBookmarkListCell *) [self.tableView cellForRowAtIndexPath:self.moreIndexPath];
        [cell setEditViewHidden:YES];
        self.isShowMore = NO;
        self.moreIndexPath = nil;
        self.tapGesture.enabled = NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FSReadingBookmark *bookmarkItem = [_arrayBookmarks objectAtIndex:indexPath.row];
    if (self.isContentEditing) {
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        if (self.bookmarkSelectionHandler) {
            self.bookmarkSelectionHandler();
        }
    } else {
        _panelController.isHidden = YES;
        [_pdfViewCtrl gotoPage:[bookmarkItem getPageIndex] animated:NO];
    }
}

#pragma mark status bar

- (void)setStatusBarHidden:(BOOL)statusBarHidden {
    _statusBarHidden = statusBarHidden;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (BOOL)prefersStatusBarHidden {
    return self.statusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

- (void)renameBookmarkWithIndex:(NSInteger)index {
    FSReadingBookmark *bookmark = [_arrayBookmarks objectAtIndex:index];
    if (!DEVICE_iPHONE) {
        selectBookmark = bookmark;
        TSAlertView *alertView = [[TSAlertView alloc] init];
        alertView.title = FSLocalizedForKey(@"kRenameBookmark");
        alertView.message = [NSString stringWithFormat:@"%@ %@", FSLocalizedForKey(@"kRenameBookmark"), [bookmark getTitle]];
        [alertView addButtonWithTitle:FSLocalizedForKey(@"kCancel")];
        [alertView addButtonWithTitle:FSLocalizedForKey(@"kRename")];
        alertView.style = TSAlertViewStyleInputText;
        alertView.buttonLayout = TSAlertViewButtonLayoutNormal;
        alertView.usesMessageTextView = NO;
        alertView.delegate = self;
        alertView.tag = 1;
        alertView.inputTextField.text = [bookmark getTitle];
        [alertView show];
        self.currentVC = alertView;
    } else {
        BOOL isFullScreen = APPLICATION_ISFULLSCREEN;
        UniversalEditViewController *editController = [[UniversalEditViewController alloc] initWithNibName:[Utility getXibName:@"UniversalEditViewController"] bundle:[NSBundle bundleForClass:[self class]]];
        FSNavigationController *editNavController = [[FSNavigationController alloc] initWithRootViewController:editController];
        editController.title = FSLocalizedForKey(@"kRenameBookmark");
        editController.textContent = [bookmark getTitle];
        editController.autoIntoEditing = YES;
        //        self.currentVC = editNavController;
        self.currentVC = editController;
        __weak typeof(editController) weakEditController = editController;
        editController.editingCancelHandler = ^{
            [weakEditController dismissViewControllerAnimated:YES completion:nil];
            self.statusBarHidden = isFullScreen;
            //            [[UIApplication sharedApplication] setStatusBarHidden:isFullScreen withAnimation:UIStatusBarAnimationFade];
        };
        editController.editingDoneHandler = ^(NSString *text) {
            text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (text == nil || text.length == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kInputBookmarkName") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                     style:UIAlertActionStyleCancel
                                                                   handler:^(UIAlertAction *_Nonnull action) {
                                                                       [self popoverDidCancel];
                                                                   }];
                    [alertController addAction:action];
                    [self presentAlertController:alertController];
                });
            } else {
                [bookmark setTitle:text];
                [self renameBookmark:bookmark];
                [weakEditController dismissViewControllerAnimated:YES completion:nil];
                self.statusBarHidden = isFullScreen;
                //                [[UIApplication sharedApplication] setStatusBarHidden:isFullScreen withAnimation:UIStatusBarAnimationFade];
            }
        };
        [[Utility getTopMostViewController] presentViewController:editNavController
                                         animated:YES
                                       completion:^{

                                       }];
    }
}

- (void)deleteBookmarkWithIndex:(NSInteger)index {
    FSReadingBookmark *deletedBookmark = [_arrayBookmarks objectAtIndex:index];
    NSUInteger pageIndex = [deletedBookmark getPageIndex];
    [_pdfViewCtrl.currentDoc removeReadingBookmark:deletedBookmark];

    NSAssert(deletedBookmark != nil, @"Delete bookmark cannot find the position of page index: %lu", (unsigned long) pageIndex);
    NSUInteger deletePos = [_arrayBookmarks indexOfObject:deletedBookmark];
    [_arrayBookmarks removeObject:deletedBookmark];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:deletePos inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    if (self.bookmarkDeleteHandler) {
        self.bookmarkDeleteHandler();
        [self performSelector:@selector(reloadtableViewAfterDeleteBookmark) withObject:nil afterDelay:0.5];
    }
}

- (void)reloadtableViewAfterDeleteBookmark {
    [self.tableView reloadData];
}

#pragma mark <TSAlertViewDelegate>

//- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
- (void)alertView:(TSAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            TSAlertView *tsAlertView = (TSAlertView *) alertView;
            NSString *newName = [tsAlertView.inputTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (newName == nil || newName.length == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kInputBookmarkName") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                     style:UIAlertActionStyleCancel
                                                                   handler:^(UIAlertAction *_Nonnull action) {
                                                                       [self popoverDidCancel];
                                                                   }];
                    [alertController addAction:action];
                    [self presentAlertController:alertController];
                });
                return;
            }

            if ([newName compare:[selectBookmark getTitle]] != NSOrderedSame) {
                [selectBookmark setTitle:newName];
                [self renameBookmark:selectBookmark];
            }
        }
    }
}

- (void)loadData {
    NSMutableArray *bookmarks = [[NSMutableArray alloc] init];
    @try {
        int count = [_pdfViewCtrl.currentDoc getReadingBookmarkCount];
        for (int i = 0; i < count; i++) {
            [bookmarks addObject:[_pdfViewCtrl.currentDoc getReadingBookmark:i]];
        }
    } @catch (NSException *exception) {
        NSLog(@"Failed to load reading bookmark: '%@'", exception.description);
    } @finally {
        self.arrayBookmarks = bookmarks;
        [self.tableView reloadData];
    }
}

- (void)clearData:(BOOL)fromPDF;
{
    if (fromPDF) {
        for (FSReadingBookmark *item in _arrayBookmarks) {
            [_pdfViewCtrl.currentDoc removeReadingBookmark:item];
        }
    }

    [self hideCellEditView];
    [_arrayBookmarks removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (NSUInteger)getBookmarkCount {
    return [_arrayBookmarks count];
}

- (void)addBookmark:(FSReadingBookmark *)newBookmark {
    [_arrayBookmarks addObject:newBookmark];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:_arrayBookmarks.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)renameBookmark:(FSReadingBookmark *)renameBookmark {
    NSUInteger rowIndex = [_arrayBookmarks indexOfObject:renameBookmark];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    ((UIExtensionsManager*)(_pdfViewCtrl.extensionsManager)).isDocModified = YES;
}

#pragma mark - Private methods

- (void)refreshInterface {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [self.tableView setTableFooterView:view];
}

#pragma mark pop over

- (void)presentAlertController:(UIAlertController *)alertController {
    UIViewController *rootVC = self;
    self.originalPresentedViewController = rootVC.presentedViewController;
    if (self.originalPresentedViewController) {
        [self.originalPresentedViewController dismissViewControllerAnimated:true
                                                                 completion:^{
                                                                     [rootVC presentViewController:alertController animated:YES completion:nil];
                                                                 }];
    } else {
        [rootVC presentViewController:alertController animated:YES completion:nil];
    }
    self.currentVC = alertController;
}

- (void)popoverDidCancel {
    if (self.originalPresentedViewController) {
        UIViewController *rootVC = self;
        [rootVC presentViewController:self.originalPresentedViewController animated:true completion:nil];
        self.originalPresentedViewController = nil;
    }
    self.currentVC = nil;
    if (!DEVICE_iPHONE) {
        // show TSAlertView back
        TSAlertView *alertView = [[TSAlertView alloc] init];
        alertView.title = FSLocalizedForKey(@"kRenameBookmark");
        alertView.message = [NSString stringWithFormat:@"%@ %@", FSLocalizedForKey(@"kRenameBookmark"), [selectBookmark getTitle]];
        [alertView addButtonWithTitle:FSLocalizedForKey(@"kCancel")];
        [alertView addButtonWithTitle:FSLocalizedForKey(@"kRename")];
        alertView.style = TSAlertViewStyleInputText;
        alertView.buttonLayout = TSAlertViewButtonLayoutNormal;
        alertView.usesMessageTextView = NO;
        alertView.delegate = self;
        alertView.tag = 1;
        alertView.inputTextField.text = [selectBookmark getTitle];
        [alertView show];
        self.currentVC = alertView;
    }
}

@end
