/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#ifndef PrivateDefine_h
#define PrivateDefine_h


#if __has_include(<uiextensionsDynamic/Defines.h>)
#import <uiextensionsDynamic/Defines.h>
#else
#import "Defines.h"
#endif

#import "NSObject+Extensions.h"
#import "NSUserDefaults+Extensions.h"
#import "NSString+Extensions.h"

#import "UIView+Extensions.h"
#import "UIButton+Extensions.h"
#import "UIMenuItem+ImageSupport.h"
#import "UIViewController+Extensions.h"
#import "UIAlertController+Extensions.h"
#import "UIImage+Extensions.h"
#import "UIColor+Extensions.h"
#import "UIViewController+FSTransition.h"

#ifdef _UIEXTENSIONS_BUILD_
#import "NSArray+Extensions.h"
#endif

#if __has_include(<FSPDFObjCExtensions.h>)
#define HAS_FSPDFOBJC YES
#endif

#if defined(HAS_FSPDFOBJC)
#import "FSPDFObjCExtensions.h"
#endif


//compile macro
#ifndef __IPHONE_12_0
#define __IPHONE_12_0 120000
#endif

#ifndef __IPHONE_13_0
#define __IPHONE_13_0 130000
#endif

//Commonly used color

/*
Create UIColor with a hex string.
Example: UIColorHex_DarkMode(0xF0F, 66ccff), UIColorHex_DarkMode(#66CCFF88, 66ccff), UIColorHex_DarkMode(66ccff, #66CCFF88)

Valid format: #RGB #RGBA #RRGGBB #RRGGBBAA 0xRGB ...
The `#` or "0x" sign is not required.
*/
#ifndef UIColorHex_DarkMode
#define UIColorHex_DarkMode(_lightHex_, _darkHex_)   [UIColor fs_colorWithLightHex:((__bridge NSString *)CFSTR(#_lightHex_)) darkHex:((__bridge NSString *)CFSTR(#_darkHex_))]
#endif

#ifndef UIColor_DarkMode
    #define UIColor_DarkMode(lightColor, darkColor)   [UIColor fs_colorWithLight:lightColor dark:darkColor]
#endif

#ifndef UIColor_Owner_DarkMode
    #define UIColor_Owner_DarkMode(owner, lightColor, darkColor)   [UIColor fs_colorWithOwner:owner light:lightColor dark:darkColor]
#endif

//dark mode commonly used color(12 kinds of patterns)
#define LinkTextColor_Dark              UIColorHex(0x179cdb)
#define ThemeTextColor_Dark             UIColorHex(0xc8c8c8)
#define SubjectTextColor_Dark           UIColorHex(0x8a8a90)
#define ThemeBarColor_Dark              UIColorHex(0x1b1b1b)
#define BarDividingLineColor_Dark       UIColorHex(0x3e3e41)
#define ReadingDividingLineColor_Dark   UIColorHex(0x333336)
#define DocDividingLineColor_Dark       UIColorHex(0x2c2c2e)
#define PageBackgroundColor_Dark        UIColorHex(0x080808)
#define CellBackgroundColor_Dark        UIColorHex(0x1c1c1e)
#define SmallHandColor_Dark             UIColorHex(0x2b2b2f)
#define NormalIconColor_Dark            UIColorHex(0x2a2a2a)
#define SelectedIconColor_Dark          UIColorHex(0x404043)

#define LikeBlueColor                   UIColorHex(0x179CD8)
#define ThemeLikeBlueColor              UIColorHex(0x249ed6)
//
#define GeneralColor                    UIColorHex(0xf4f4f4)
#define ThemePopViewColor_Dark          UIColorHex(0x2B2B2F)

//all currently in light mode
//bar
#define ThemeBarColor                   UIColor_DarkMode(ThemeLikeBlueColor, ThemeBarColor_Dark)
#define ThemeNavBarColor                UIColor_Owner_DarkMode(self, ThemeLikeBlueColor, ThemeBarColor_Dark) //NavBar cannot be updated as a system bug
#define WhiteThemeNavBarTextColor       UIColor_Owner_DarkMode(self, [UIColor fs_whiteColor], ThemeTextColor_Dark)

#define BarLikeBlackTextColor           UIColor_DarkMode(UIColorHex(0x3F3F3F), ThemeTextColor_Dark)
#define NormalBarBackgroundColor        UIColor_DarkMode(GeneralColor, ThemeBarColor_Dark)
#define PanelTopBarColor                UIColor_DarkMode([UIColor fs_whiteColor], ThemeBarColor_Dark)

//line
#define BarDividingLineColor            UIColor_DarkMode(UIColorHex(0xE2E2E2), DocDividingLineColor_Dark)
#define DividingLineColor               UIColor_DarkMode(UIColorHex(0xe6e6e6), DocDividingLineColor_Dark)

//view
#define ThemeViewBackgroundColor        UIColor_DarkMode([UIColor fs_whiteColor], PageBackgroundColor_Dark)
#define ThemeCellBackgroundColor        UIColor_DarkMode([UIColor fs_whiteColor], CellBackgroundColor_Dark)
#define ThemePopViewDividingLineColor   UIColor_DarkMode(UIColorHex(0xe6e6e6), SelectedIconColor_Dark)
#define MoreViewBackgroundColor         UIColorHex_DarkMode(0xe7e7e7, 0xe7e7e7)
#define ReplyThemeBackgroundColor       UIColor_DarkMode(UIColorHex(0xfffbdb), PageBackgroundColor_Dark)
//0xEAF8FF pale blue
#define GroupCellBackgroundColor        UIColor_DarkMode(UIColorHex(0xEAF8FF), UIColorHex(0x313131))
#define ItemSelectedBackgroundColor     UIColor_DarkMode(UIColorHex(0xD8D8D8), [UIColor fs_grayColor])

//text
#define WhiteThemeTextColor             UIColor_DarkMode([UIColor fs_whiteColor], ThemeTextColor_Dark)
#define BlackThemeTextColor             UIColor_DarkMode([UIColor fs_blackColor], ThemeTextColor_Dark)
#define GrayThemeTextColor              UIColor_DarkMode([UIColor fs_grayColor], ThemeTextColor_Dark)
#define DarkGrayThemeTextColor          UIColor_DarkMode([UIColor darkGrayColor], ThemeTextColor_Dark)
#define SubjectLabTextColor             UIColor_DarkMode(UIColorHex(0x6f7179), SubjectTextColor_Dark)
#define BlueThemeTextColor              LikeBlueColor

//tableView
#define TabViewSectionColor             UIColor_DarkMode(UIColorHex(0xcccccc), ThemeBarColor_Dark)
#define TabViewSectionTextColor         UIColor_DarkMode(UIColorHex(0x259dd6), ThemeTextColor_Dark)
#define TabViewGroupBackColor           UIColor_DarkMode(UIColorHex(0xe6e6fa), PageBackgroundColor_Dark)

//control
#define OptionMoreColor                 UIColor_DarkMode(UIColorHex(0xE7E7E7), CellBackgroundColor_Dark)
#define SwitchOnColor                   UIColorHex_DarkMode(0x33bd45, LikeBlueColor)

//speedy

#define SIZECLASS [NSObject fs_getForegroundActiveWindow].traitCollection.horizontalSizeClass

#ifndef ImageNamed
    #define ImageNamed(name) [UIImage fs_ImageNamed:name]
#endif

#ifndef ImageNamed_DarkMode
    #define ImageNamed_DarkMode(name) [UIImage fs_ImageNamedForDark:name]
#endif

#define CLANG_IGNORED_WARNING(ignored , ...) \
_Pragma("clang diagnostic push") \
_Pragma(#ignored) \
__VA_ARGS__; \
_Pragma("clang diagnostic pop") \

#define CLANG_IGNORED_SELECTOR_WARNING(...) CLANG_IGNORED_WARNING(clang diagnostic ignored "-Wundeclared-selector",##__VA_ARGS__)

#endif /* PrivateDefine_h */
