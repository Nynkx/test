/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "UIExtensionsManager+Private.h"

#define MKA_POPUPWIDTH      200
#define MKA_POPUPHEIGHT     100

@implementation UIExtensionsManager (Private)
- (void)addPopupWithAnnot:(FSAnnot *)annot{
    if (!annot || [annot isEmpty] || ![annot isMarkup]) {
        return;
    }
    FSAnnotType annotType = [annot getType];
    if (annotType == FSAnnotSound || annotType == FSAnnotFreeText) {
        return;
    }
    if (annotType == FSAnnotNote) {
        FSNote *note = [[FSNote alloc] initWithAnnot:annot];
        FSMarkup *replyTo = [note getReplyTo];
        if (replyTo && ![replyTo isEmpty]) {
            return;
        }
    }
    
    FSPDFPage *page = annot.getPage;
    if (![page isParsed]) [page startParse:0 pause:nil is_reparse:NO];
    
    FSRectF *rectF = [[FSRectF alloc] initWithLeft1:[page getWidth] bottom1:annot.fsrect.top-MKA_POPUPHEIGHT right1:[page getWidth]+MKA_POPUPWIDTH top1:annot.fsrect.top];
    if ([page getRotation] == 1) {
        rectF = [[FSRectF alloc] initWithLeft1:annot.fsrect.left bottom1:[page getWidth]-MKA_POPUPHEIGHT right1:annot.fsrect.left+MKA_POPUPWIDTH top1:[page getWidth]];
    }
    else if ([page getRotation] == 2) {
        rectF = [[FSRectF alloc] initWithLeft1:0  bottom1:annot.fsrect.bottom-MKA_POPUPHEIGHT right1:MKA_POPUPWIDTH top1:annot.fsrect.bottom];
    }
    else if ([page getRotation] == 3) {
        rectF = [[FSRectF alloc] initWithLeft1:annot.fsrect.right bottom1:-MKA_POPUPHEIGHT right1:annot.fsrect.right+MKA_POPUPWIDTH  top1:0];
    }
    FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
    FSPopup *popup = [[FSPopup alloc] initWithAnnot:[annot.getPage addAnnot:FSAnnotPopup rect:rectF]];
    popup.openStatus = YES;
    markup.popup = popup;
}

@end

