/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FoxitRDK/FSPDFViewControl.h>

@interface PaintUtility : NSObject

@property (nonatomic, assign) unsigned int annotColor;

@property (nonatomic, assign) CGFloat annotLineWidth;

@property (nonatomic, assign) CGFloat annotOpacity;

-(void)drawLineFromStartPoint:(CGPoint)startPoint toEndPoint:(CGPoint)endPoint inContext:(CGContextRef)context isArrowLine:(BOOL) isArrowLine;

-(void)drawPencilWith:(NSArray *)arrayLines inContext:(CGContextRef)context;

-(void)drawShapWithRect:(CGRect)rect type:(FSAnnotType)type inContext:(CGContextRef)context;
-(void)drawMultipleSelectWithRect:(CGRect)rect inContext:(CGContextRef)context;
-(void)drawMultipleSelectedWithRect:(CGRect)rect inContext:(CGContextRef)context ;
@end
