/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "FSGCDServer.h"

@interface FSGCDGroupServer ()
@property (nonatomic) dispatch_group_t group;
@property (nonatomic) dispatch_queue_t queue;
@end

@implementation FSGCDGroupServer

- (instancetype)init{
    self = [super init];
    if (self) {
        _isCancelled = NO;
    }
    return self;
}

- (instancetype)initWithQueue:(dispatch_queue_t)queue{
    self = [super init];
    if (self) {
        _queue = queue;
        _isCancelled = NO;
    }
    return self;
}

- (dispatch_group_t)group{
    if (!_group) {
        _group = dispatch_group_create();
    }
    return _group;
}

- (dispatch_queue_t)queue{
    if (!_queue) {
        _queue = dispatch_get_global_queue(0, 0);
    }
    return _queue;
}

- (void)cancelAllOperations{
    _isCancelled = YES;
}

- (void)enter{
    if (self.isCancelled) return;
    dispatch_group_enter(self.group);
}

- (void)leave{
    dispatch_group_leave(self.group);
}

- (void)executeBlock:(void(^)())block{
    dispatch_group_async(self.group, self.queue, ^{
        if (self.isCancelled) return;
        block();
    });
}
- (void)notifyBlock:(void(^)())block{
     dispatch_group_notify(self.group, self.queue, block);
}
@end

