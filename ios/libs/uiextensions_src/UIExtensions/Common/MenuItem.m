/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MenuItem.h"

@implementation MenuItem

- (instancetype)initWithImage:(UIImage *)image object:(id)object action:(SEL)action{
    return [self initWithTitle:nil image:image object:object action:action];
}

- (instancetype)initWithTitle:(NSString *)title object:(id)object action:(SEL)action {
    return [self initWithTitle:title image:nil object:object action:action];
}

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image object:(id)object action:(SEL)action{
    if (self = [super init]) {
        self.title = title;
        self.image = image;
        self.object = object;
        self.action = action;
    }
    return self;
}


- (void)setAction:(SEL)action {
    _action = action;
}

- (void)setTitle:(NSString *)title {
    if (_title != title) {
        _title = [title copy];
    }
}

- (BOOL)dontDismiss {
    return YES;
}

@end
