/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PropertyBar.h"
#import "ShapeUtil.h"
#import "UIExtensionsManager.h"

typedef NS_OPTIONS(NSUInteger, FSMenuOptions) {
    FSMenuOptionNone = 0,
    FSMenuOptionStyle = 1 << 0,
    FSMenuOptionOpen = 1 << 1,
    FSMenuOptionCopyText = 1 << 2,
    FSMenuOptionReply = 1 << 3,
    FSMenuOptionDelete = 1 << 4,
    FSMenuOptionEdit = 1 << 5,
    FSMenuOptionPlay = 1 << 6,
    FSMenuOptionFlatten = 1 << 7,
    FSMenuOptionApplyRedaction = 1 << 8,
    FSMenuOptionSpeech = 1 << 9,
};

NS_ASSUME_NONNULL_BEGIN

@interface FSAnnotHandler : NSObject <IAnnotHandler>

@property (nonatomic, weak, readonly) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak, readonly) UIExtensionsManager *extensionsManager;
@property (nonatomic, strong, readonly) FSAnnot *annot;
@property (nonatomic) UIEdgeInsets pageEdgeInsets;  // when move annot, annot stay in the inset rect.
@property (nonatomic) UIEdgeInsets annotSelectionEdgeInsets; // for hitTest
@property (nonatomic) BOOL shouldDrawAnnotBorderOnSelection; // should draw a dash line border around annot when it's selected.
@property (nonatomic) UIEdgeInsets annotBorderEdgeInsets;
@property (nonatomic) UIEdgeInsets annotRefreshRectEdgeInsets; // for refresh area of page view, usually same as annotSelectionEdgeInsets
@property (nonatomic, strong, nullable) UIImage *annotImage;
@property (nonatomic, strong, nullable) FSAnnotAttributes *attributesBeforeModify; // for undo
@property (nonatomic) FSAnnotEditType editType;
@property (nonatomic) BOOL shouldShowMenuOnSelection;


- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager;

// override points
- (FSAnnotType)getType;

// override these methods for pan gesture, or ignore them and implement onPageViewPan:recognizer:annot: yourself.
- (BOOL)canMoveAnnot:(FSAnnot *)annot;                    // defaults to YES
- (BOOL)canResizeAnnot:(FSAnnot *)annot;                  // defaults to YES
- (BOOL)keepAspectRatioForResizingAnnot:(FSAnnot *)annot; // defaults to NO
- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot;
- (CGSize)maxSizeForResizingAnnot:(FSAnnot *)annot;

// menu
- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot;
- (void)showMenu;
- (void)hideMenu;

// menu actions:
- (void)showStyle;
- (void)edit;
- (void)copyText;
- (void)comment;
- (void)reply;
- (void)delete;

// draw
- (UIColor *)borderColorForSelectedAnnot:(FSAnnot *)annot;
//misc
- (CGRect)getAnnotRect:(FSAnnot *)annot;
- (void)refreshPageForAnnot:(FSAnnot *)annot reRenderPage:(BOOL)reRenderPage;

// Update drawing appearance of annotation.
- (void)updateAnnotImage;
- (void)drawAnnotInContext:(CGContextRef)context;

// PageView Gesture+Touch
- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer annot:(FSAnnot *_Nullable)annot;
- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *_Nullable)annot;
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(nonnull UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *_Nullable)annot;
- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot;
- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot;
- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot;
- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot;
- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot;
- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *_Nullable)annot;

@end

NS_ASSUME_NONNULL_END
