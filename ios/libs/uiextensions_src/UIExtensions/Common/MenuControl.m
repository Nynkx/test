/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MenuControl.h"
#import "MenuItem.h"
#import "ReplyTableViewController.h"

//annotation from the longpress menu does not work in customers Xamarin SDK-19060
@interface UIView (FixXamarinBug)
@property (class, nonatomic, weak) id fs_menuItemActionTarget;
@end

#define  MAGIC_CLICKED_ @"magic_clicked_"
@interface MenuControl ()
@property (nonatomic, strong) UIMenuController *menuControl;
@end

@implementation MenuControl {
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    UIExtensionsManager * __weak _extensionsManager;
    UILongPressGestureRecognizer *_longpressGesture;
    UIPanGestureRecognizer *_panGesture;
    UITapGestureRecognizer *_tapGesture;
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)recognizer {
    [_extensionsManager onPan:recognizer];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)recognizer {
    if (recognizer == _tapGesture || recognizer == _longpressGesture)
        return YES;
    if (recognizer == _panGesture) {
        return [_extensionsManager onShouldBegin:recognizer];
    }
    return [super gestureRecognizerShouldBegin:recognizer];
}

- (id)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = _extensionsManager.pdfViewCtrl;
        self.menuControl = [UIMenuController sharedMenuController];

        //        [[NSNotificationCenter defaultCenter] addObserver:self
        //                                                 selector:@selector(willHideMenu)
        //                                                     name:UIMenuControllerWillHideMenuNotification
        //                                                   object:nil];
        //        [[NSNotificationCenter defaultCenter] addObserver:self
        //                                                 selector:@selector(didShowMenu)
        //                                                     name:UIMenuControllerDidShowMenuNotification
        //                                                   object:nil];

        _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        [self addGestureRecognizer:_panGesture];
    }
    return self;
}

- (void)_setMenuVisible:(BOOL)menuVisible animated:(BOOL)animated {
    if (menuVisible != [self isMenuVisible]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.menuControl setMenuVisible:menuVisible animated:animated];
        });
    }
    if (!menuVisible) {
        self.frame = CGRectZero;
        [self resignFirstResponder];
    }
}

- (void)setMenuVisible:(BOOL)menuVisible animated:(BOOL)animated {
    [self _setMenuVisible:menuVisible animated:animated];
    if (menuVisible) {
        [self.delegate menuControlDidShow:self];
    } else {
        [self.delegate menuControlDidHide:self];
    }
}

- (BOOL)isMenuVisible {
    return self.menuControl.isMenuVisible || !CGRectEqualToRect(self.frame, CGRectZero);
}

- (void)hideMenu {
    [self setMenuVisible:NO animated:YES];
}

- (void)setRect:(CGRect)rect {
    [self setRect:rect margin:10];
}

- (void)setRect:(CGRect)rect margin:(float)margin {
    UIView *displayView = [_pdfViewCtrl getDisplayView];
    CGRect frame = CGRectInset(rect, -35, -35);
    if ((CGRectGetMinX(frame) < 0 || CGRectGetMinY(frame) < 0) && (CGRectGetWidth(frame) > CGRectGetWidth(displayView.frame) && CGRectGetHeight(frame) > CGRectGetHeight(displayView.frame))) {
        frame = CGRectUnion(frame, displayView.frame);
    }
    self.frame = frame;
    [[_pdfViewCtrl getDisplayView] insertSubview:self atIndex:0];
    [self.menuControl setTargetRect:CGRectInset(self.bounds, 35 - margin, 35 - margin) inView:self];
}

- (void)showMenu {
    
    if (![[self topViewController] isKindOfClass:[ReplyTableViewController class]] && ![[self topViewController] isKindOfClass:[UIActivityViewController class]]) {

        NSMutableArray *menuArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < self.menuItems.count; i++) {
            MenuItem *mcItem = [self.menuItems objectAtIndex:i];
            UIMenuItem *menuItem = [[UIMenuItem alloc] initWithTitle:mcItem.title image:mcItem.image action:NSSelectorFromString([NSString stringWithFormat:@"%@%i",MAGIC_CLICKED_, i])];
            [menuArray addObject:menuItem];
        }
        [self.menuControl setMenuItems:menuArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.menuControl setMenuVisible:YES animated:YES];
        });
        [UIView setFs_menuItemActionTarget:self];
    }
    [self.delegate menuControlDidShow:self];
    
    if (_extensionsManager.shouldShowMenu) {
         [self becomeFirstResponder];
    }

}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    NSString *actionName = NSStringFromSelector(action);
    NSRange match = [actionName rangeOfString:MAGIC_CLICKED_];
    if (match.location == 0) {
        return YES;
    }
    return NO;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
    if ([super methodSignatureForSelector:sel]) {
        return [super methodSignatureForSelector:sel];
    }
    NSString *selName = NSStringFromSelector(sel);
    NSRange match = [selName rangeOfString:MAGIC_CLICKED_];
    if (match.location == 0) {
        return [super methodSignatureForSelector:@selector(onClickedAtIndex:)];
    }
    return nil;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    NSString *selName = NSStringFromSelector([anInvocation selector]);
    NSString *prefix = MAGIC_CLICKED_;
    NSRange match = [selName rangeOfString:prefix];
    if (match.location == 0) {
        [self onClickedAtIndex:[[selName substringFromIndex:prefix.length] intValue]];
    } else {
        [super forwardInvocation:anInvocation];
    }
}

- (void)onClickedAtIndex:(int)index {
    [self _setMenuVisible:NO animated:YES];
    if (self.menuItems.count < index + 1)
        return;
    MenuItem *mcItem = [self.menuItems objectAtIndex:index];
    ((void (*)(id, SEL, MenuItem *))[mcItem.object methodForSelector:mcItem.action])(mcItem.object, mcItem.action, mcItem);
}

- (UIViewController *)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController {
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *) rootViewController;
        return [self topViewControllerWithRootViewController:nav.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController *presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

@end

//annotation from the longpress menu does not work in customers Xamarin SDK-19060
@implementation UIView (FixXamarinBug)

+ (void)load{
    [super load];
    Class cls = NSClassFromString(@"UICalloutBar");
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        SEL selectors[] = {
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Wundeclared-selector"
            @selector(_targetForAction:)
            #pragma clang diagnostic pop
        };
        
        for (NSUInteger index = 0; index < sizeof(selectors) / sizeof(SEL); ++index) {
            SEL originalSel = selectors[index];
            SEL swizzledSel = NSSelectorFromString([@"fs_swizzled_" stringByAppendingString:NSStringFromSelector(originalSel)]);
            [cls fs_exchangeImpWithOriginalSel:originalSel swizzledSel:swizzledSel];
        }
    });
}

+ (void)setFs_menuItemActionTarget:(id)fs_menuItemActionTarget{
    [self fs_addWeakAssociatedObject:fs_menuItemActionTarget forKey:@"fs_menuItemActionTarget"];
}

+ (id)fs_menuItemActionTarget{
    return [self fs_getWeakAssociatedForKey:@"fs_menuItemActionTarget"];
}

- (id)fs_swizzled__targetForAction:(SEL)action{
    id target = [self fs_swizzled__targetForAction:action]; //The bug was to get target to be nil, and then some adjustments were made here
    if (!target) {
        NSString *selName = NSStringFromSelector(action);
        NSString *prefix = MAGIC_CLICKED_;
        NSRange match = [selName rangeOfString:prefix];
        if (match.location != NSNotFound) {
            return [UIView fs_menuItemActionTarget];
        }
    }
    return target;
}
@end
