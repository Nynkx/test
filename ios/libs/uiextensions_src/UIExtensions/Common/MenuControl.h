/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

@class MenuControl;
@class UIExtensionsManager;

@protocol MenuControlDelegate <NSObject>

- (void)menuControlDidHide:(MenuControl *)menuControl;
- (void)menuControlDidShow:(MenuControl *)menuControl;

@end

/** @brief Menu control to show, hide the menu, or perform action associated with the menu item. */
@interface MenuControl : UIView

@property (nonatomic, weak) id<MenuControlDelegate> delegate;
@property (nonatomic, copy) NSArray *menuItems;

- (id)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager;
- (void)showMenu;
- (void)hideMenu;
- (void)setMenuVisible:(BOOL)menuVisible animated:(BOOL)animated;
- (BOOL)isMenuVisible;
- (void)setRect:(CGRect)Rect;
- (void)setRect:(CGRect)rect margin:(float)margin;

@end
