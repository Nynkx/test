/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSAnnotHandler.h"
#import "FSAnnotAttributes.h"
#import "MenuItem.h"
#import "ReplyTableViewController.h"
#import "ReplyUtil.h"
#import "ShapeUtil.h"
#import "AudioPlayerControl.h"
#import "FSAnnot+Extensions.h"

@interface FSAnnotHandler ()

@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (nonatomic, assign) CGSize dragDotSize;
@property (nonatomic, strong) FSAnnot *annot;

// on pan, hide menu or property bar first and show it later
@property (nonatomic) BOOL shouldShowMenu;
@property (nonatomic) BOOL shouldShowPropertyBar;

@end

@implementation FSAnnotHandler

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = _extensionsManager.pdfViewCtrl;
        _annot = nil;
        _annotImage = nil;
        _attributesBeforeModify = nil;
        _editType = FSAnnotEditTypeUnknown;
        _pageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        _annotSelectionEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
        _annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
        _annotBorderEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
        _shouldDrawAnnotBorderOnSelection = YES;
        _shouldShowMenuOnSelection = YES;
    }
    return self;
}

// override point

- (FSAnnotType)getType {
    return FSAnnotUnknownType;
}

- (BOOL)canMoveAnnot:(FSAnnot *)annot {
    return annot.canMove;
}

- (BOOL)canResizeAnnot:(FSAnnot *)annot {
    return annot.canResize;
}

- (BOOL)keepAspectRatioForResizingAnnot:(FSAnnot *)annot {
    return NO;
}

- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot {
    return CGSizeMake(10, 10);
}

- (CGSize)maxSizeForResizingAnnot:(FSAnnot *)annot {
    UIView *pageView = [_pdfViewCtrl getPageView:annot.pageIndex];
    return pageView.bounds.size;
}

- (UIColor *)borderColorForSelectedAnnot:(FSAnnot *)annot {
    return [UIColor colorWithRGB:annot.color];
}

#pragma mark <IAnnotHandler>

- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    CGRect pvRect = [self getAnnotRect:annot];
    pvRect = UIEdgeInsetsInsetRect(pvRect, self.annotSelectionEdgeInsets);
    CGPoint pvPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:point pageIndex:annot.pageIndex];
    if (CGRectContainsPoint(pvRect, pvPoint)) {
        return YES;
    }
    return NO;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    self.annot = annot;
    if ([self shouldDrawAnnot:annot inPDFViewCtrl:self.pdfViewCtrl]) {
        [self refreshPageForAnnot:annot reRenderPage:NO];
    } else {
        [self updateAnnotImage];
        [self refreshPageForAnnot:annot reRenderPage:YES];
    }
    self.attributesBeforeModify = [FSAnnotAttributes attributesWithAnnot:annot];

    if (self.shouldShowMenuOnSelection) {
        _extensionsManager.menuControl.menuItems = [self getMenuItems];
        if (_extensionsManager.menuControl.menuItems.count > 0) {
            [self showMenu];
        }
    }
}

- (void)onAnnotDeselected:(FSAnnot *)annot {
    [self hideMenu];
    if (_extensionsManager.propertyBar.isShowing) {
        [_extensionsManager.propertyBar dismissPropertyBar];
    }
    if ([self shouldDrawAnnot:annot inPDFViewCtrl:self.pdfViewCtrl]) {
        [self refreshPageForAnnot:self.annot reRenderPage:NO];
    } else {
        [self refreshPageForAnnot:self.annot reRenderPage:YES];
    }

    if (self.attributesBeforeModify && ![self.attributesBeforeModify isEqualToAttributes:[FSAnnotAttributes attributesWithAnnot:annot]]) {
        [self modifyAnnot:annot addUndo:YES];
    }
    self.attributesBeforeModify = nil;
    self.annotImage = nil;
    self.annot = nil;
}

- (void)onAnnotChanged:(FSAnnot *)annot property:(long)property from:(NSValue *)oldValue to:(NSValue *)newValue {
    if ([self.annot isEqualToAnnot:annot]) {
        if ([self shouldDrawAnnot:annot inPDFViewCtrl:self.pdfViewCtrl]) {
            [self refreshPageForAnnot:annot reRenderPage:YES];
        } else {
            [self updateAnnotImage];
            [self refreshPageForAnnot:annot reRenderPage:NO];
        }
    }
}

- (BOOL)addAnnot:(FSAnnot *)annot {
    return [self addAnnot:annot addUndo:YES];
}

- (BOOL)addAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    FSPDFPage *page = [annot getPage];
    if (addUndo) {
        FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:annot];
        [_extensionsManager addUndoItem:[UndoItem itemForUndoAddAnnotWithAttributes:attributes page:page annotHandler:self]];
    }
    [_extensionsManager onAnnotAdded:page annot:annot];
    [self refreshPageForAnnot:annot reRenderPage:YES];
    return YES;
}

- (BOOL)modifyAnnot:(FSAnnot *)annot {
    return [self modifyAnnot:annot addUndo:YES];
}

- (BOOL)modifyAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    if (![annot canModify]) {
        return NO;
    }
    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return NO;
    }
    if (addUndo) {
        annot.modifiedDate = [NSDate date];
        [_extensionsManager addUndoItem:[UndoItem itemForUndoModifyAnnotWithOldAttributes:self.attributesBeforeModify newAttributes:[FSAnnotAttributes attributesWithAnnot:annot] pdfViewCtrl:_pdfViewCtrl page:page annotHandler:self]];
    }
    [_extensionsManager onAnnotModified:page annot:annot];
    [self refreshPageForAnnot:annot reRenderPage:YES];
    return YES;
}

- (BOOL)removeAnnot:(FSAnnot *)annot {
    return [self removeAnnot:annot addUndo:YES];
}

- (BOOL)removeAnnot:(FSAnnot *)annot addUndo:(BOOL)addUndo {
    
    [self hideMenu];
    if (self.extensionsManager.propertyBar.isShowing) {
        [self.extensionsManager.propertyBar dismissPropertyBar];
    }

    FSPDFPage *page = [annot getPage];
    if (!page || [page isEmpty]) {
        return NO;
    }
    int pageIndex = annot.pageIndex;
    CGRect rect = [self getAnnotRect:annot];
    rect = UIEdgeInsetsInsetRect(rect, self.annotRefreshRectEdgeInsets);
    rect = CGRectIntersection(rect, [_pdfViewCtrl getPageView:pageIndex].bounds);

    [_extensionsManager onAnnotWillDelete:page annot:annot];
    if (addUndo) {
        FSAnnotAttributes *attributes = self.attributesBeforeModify ?: [FSAnnotAttributes attributesWithAnnot:annot];
        [_extensionsManager addUndoItem:[UndoItem itemForUndoDeleteAnnotWithAttributes:attributes page:page annotHandler:self]];
    }
    
    AudioPlayerControl *audioPlay = [AudioPlayerControl sharedControl];
    NSString *medioFilePath = [Utility getDocumentRenditionTempFilePath:annot.ha_fileSpec PDFPath:self.extensionsManager.pdfViewCtrl.filePath];
    NSString *playFilePath = audioPlay.filePath.path;
    if ([medioFilePath isEqualToString:playFilePath]) {
        [audioPlay dismiss];
    }
    
    [_pdfViewCtrl lockRefresh];
    NSArray *replyAnnots = annot.replyAnnots;
    if (replyAnnots.count) {
        FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
        [markup removeAllReplies];
    }
    BOOL ret = [page removeAnnot:annot];
    [_pdfViewCtrl unlockRefresh];

    if (ret) {
        [_extensionsManager onAnnotDeleted:page annot:annot];
        
        [_pdfViewCtrl refresh:rect pageIndex:pageIndex];
        
        self.attributesBeforeModify = nil;
        self.annotImage = nil;
        self.annot = nil;
    }
    return ret;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer annot:(FSAnnot *)annot {
    if (annot && [Utility isAnnot:annot uniqueToAnnot:self.annot]) {
        BOOL canAddAnnot = [Utility canAddAnnot:_pdfViewCtrl];
        if (!canAddAnnot) {
            return NO;
        }
        CGPoint point = [gestureRecognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        if (pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint]) {
            return YES;
        }
        return NO;
    }
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:annot.pageIndex]];
    FSPointF *pdfPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:annot.pageIndex];
    if ([Utility isAnnot:_extensionsManager.currentAnnot uniqueToAnnot:annot]) {
        if (!(pageIndex == annot.pageIndex && [self isHitAnnot:annot point:pdfPoint])) {
            [_extensionsManager setCurrentAnnot:nil];
        } else if (self.extensionsManager.shouldShowMenu && !self.extensionsManager.shouldShowPropertyBar) {
            [self showMenu];
        }
    } else {
        [_extensionsManager setCurrentAnnot:annot];
    }
    return YES;
}

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(nonnull UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *_Nullable)annot {
    CGPoint point = [recognizer locationInView:[self.pdfViewCtrl getPageView:pageIndex]];
    FSPointF *pdfPoint = [self.pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    if (self.extensionsManager.currentAnnot) {
        if ((pageIndex != annot.pageIndex || ![self isHitAnnot:annot point:pdfPoint]) && recognizer.state ==  UIGestureRecognizerStateEnded) {
            [self.extensionsManager setCurrentAnnot:nil];
            return NO;
        }
    } else {
        [self.extensionsManager setCurrentAnnot:annot];
        #if _MAC_CATALYST_
            recognizer.enabled = NO;
            recognizer.enabled = YES;
        #endif
    }
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    if (![Utility isAnnot:annot uniqueToAnnot:_extensionsManager.currentAnnot]) {
        return NO;
    }
    if (!annot.canModify) {
        return YES;
    }
    BOOL canMove = [self canMoveAnnot:annot];
    BOOL canResize = [self canResizeAnnot:annot];
    if (!canMove && !canResize) {
        if (self.extensionsManager.currentAnnot) {
            [self showMenu];
        }
        return YES;
    }
    UIView *pageView = [_pdfViewCtrl getPageView:annot.pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    CGRect annotRect = [self getAnnotRect:annot];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.shouldShowMenu = self.extensionsManager.shouldShowMenu;
        self.shouldShowPropertyBar = self.extensionsManager.shouldShowPropertyBar;
        [self hideMenu];
        [self.extensionsManager hidePropertyBar];

        if (!canResize) {
            _editType = FSAnnotEditTypeFull;
        } else {
            BOOL keepAspectRatio = [self keepAspectRatioForResizingAnnot:annot];
            _editType = [ShapeUtil getAnnotEditTypeWithPoint:point rect:CGRectInset(annotRect, -10, -10) keepAspectRatio:keepAspectRatio defaultEditType:FSAnnotEditTypeFull];
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [recognizer translationInView:pageView];
        CGRect newAnnotRect = [self newAnnotRectForPanTranslation:translation annot:annot editType:self.editType];
        
        if ([annot getFlags] & FSAnnotFlagNoRotate || [annot getFlags] & FSAnnotFlagNoZoom || [annot getType] == FSAnnotNote) {
            FSRectF *newRectF = annot.fsrect;
            [newRectF translate:translation.x f:translation.y];
            
            FSRectF *newfsrect = [Utility convertPageViewRectToPdfRect:newRectF pageDisplayMatrix:[_pdfViewCtrl getDisplayMatrix:pageIndex] annot:annot oldPdfRect:[self.pdfViewCtrl convertPageViewRectToPdfRect:newAnnotRect pageIndex:annot.pageIndex] rotation:[_pdfViewCtrl getViewRotation]];
            [self.pdfViewCtrl lockRefresh];
            annot.fsrect = newfsrect;
            [self.pdfViewCtrl unlockRefresh];
        }else{
            [self.pdfViewCtrl lockRefresh];
            annot.fsrect = [self.pdfViewCtrl convertPageViewRectToPdfRect:newAnnotRect pageIndex:annot.pageIndex];
            [self.pdfViewCtrl unlockRefresh];
        }


        CGRect refreshRect = CGRectUnion(newAnnotRect, annotRect);
        refreshRect = UIEdgeInsetsInsetRect(refreshRect, self.annotRefreshRectEdgeInsets);
        refreshRect = CGRectIntersection(refreshRect, pageView.bounds);
        if ([self shouldDrawAnnot:annot inPDFViewCtrl:self.pdfViewCtrl]) {
            [_pdfViewCtrl refresh:refreshRect pageIndex:annot.pageIndex needRender:YES];
        } else {
            if (self.editType != FSAnnotEditTypeFull) {
                [self updateAnnotImage];
            }
            BOOL needRender = NO;
            if ([annot type] == FSAnnotWidget) {
                FSWidget *widget = [[FSWidget alloc] initWithAnnot:annot];
                FSField *field = [widget getField];
                if (![field isEmpty]) {
                    FSFieldType fieldType = [field getType];
                    if (fieldType == FSFieldTypeSignature) {
                        needRender = YES;
                    }
                }
                [widget resetAppearanceStream];
            }
            [_pdfViewCtrl refresh:refreshRect pageIndex:annot.pageIndex needRender:needRender];
        }
        translation = [self updatedPanTranslation:translation oldAnnotRect:annotRect newAnnotRect:newAnnotRect];
        [recognizer setTranslation:translation inView:pageView];
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        _editType = FSAnnotEditTypeUnknown;
        if (self.shouldShowMenu) {
            [self showMenu];
        } else if (self.shouldShowPropertyBar) {
            [self.extensionsManager showPropertyBar];
        }
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event annot:(FSAnnot *)annot {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    if (pageIndex == annot.pageIndex && [Utility isAnnot:annot uniqueToAnnot:self.annot]) {
        CGRect rect = [Utility getAnnotRect:annot pdfViewCtrl:_pdfViewCtrl];

        if (self.annotImage) {
            CGContextSaveGState(context);
            CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
            CGContextTranslateCTM(context, 0, rect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
            CGContextDrawImage(context, rect, [self.annotImage CGImage]);
            CGContextRestoreGState(context);
        }

        if (self.shouldDrawAnnotBorderOnSelection && rect.size.width > 0 && rect.size.height > 0) {
            rect = UIEdgeInsetsInsetRect(rect, self.annotBorderEdgeInsets);
            CGContextSetLineWidth(context, 2.0);
            CGFloat dashArray[] = {3, 3, 3, 3};
            CGContextSetLineDash(context, 3, dashArray, 4);
            UIColor *dashLineColor = [self borderColorForSelectedAnnot:annot];
            CGContextSetStrokeColorWithColor(context, [dashLineColor CGColor]);
            CGContextStrokeRect(context, rect);

            if ([self canResizeAnnot:annot]) {
                UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                self.dragDotSize = dragDot.size;
                BOOL keepAspectRatio = [self keepAspectRatioForResizingAnnot:annot];
                NSArray *movePointArray = keepAspectRatio ? [ShapeUtil getCornerMovePointInRect:rect] : [ShapeUtil getMovePointInRect:rect];
                [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    CGRect dotRect = [obj CGRectValue];
                    CGPoint point = CGPointMake(dotRect.origin.x, dotRect.origin.y);
                    [dragDot drawAtPoint:point];
                }];
            }else{
                self.dragDotSize = CGSizeZero;
            }
        }
    }
}

- (BOOL)shouldDrawAnnot:(FSAnnot *)annot inPDFViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl {
    if ([self.annot isEqualToAnnot:annot]) {
        return NO;
    }
    return YES;
}

#pragma mark menu

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
    if (annot.canCopyText && [Utility canCopyText:self.extensionsManager.pdfViewCtrl] && self.extensionsManager.config.copyText) {
        options |= FSMenuOptionCopyText;
    }
    return options;
}

- (NSArray<MenuItem *> *)getMenuItems {
    NSMutableArray<MenuItem *> *items = @[].mutableCopy;
    FSMenuOptions menuOptions = [self menuOptionsForAnnot:self.annot];
    if (menuOptions & FSMenuOptionApplyRedaction) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kApply") object:self action:@selector(applyRedaction)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionStyle) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kStyle") object:self action:@selector(showStyle)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionOpen) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kOpen") object:self action:@selector(comment)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionCopyText) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kCopyText") object:self action:@selector(copyText)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionReply) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kReply") object:self action:@selector(reply)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionPlay) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kPlay") object:self action:@selector(play)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionDelete) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kDelete") object:self action:@selector(delete)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionEdit) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kEdit") object:self action:@selector(edit)];
        [items addObject:item];
    }
    if (menuOptions & FSMenuOptionFlatten) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kFlatten") object:self action:@selector(flatten)];
        [items addObject:item];
    }
    
    if (menuOptions & FSMenuOptionSpeech) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSpeak") object:self action:@selector(speech)];
        [items addObject:item];
    }
    
    return items;
}

- (void)showMenu {
    if (!self.annot) {
        return;
    }
    CGRect rect = [self getAnnotRect:self.annot];
    CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:self.annot.pageIndex];
    [_extensionsManager.menuControl setRect:dvRect];
    [_extensionsManager.menuControl showMenu];
}

- (void)hideMenu {
    if (_extensionsManager.menuControl.isMenuVisible) {
        [_extensionsManager.menuControl setMenuVisible:NO animated:YES];
    }
}

- (void)applyRedaction{
    
}

- (void)showStyle {
    // noop, need override
}

- (void)copyText {
    if (self.annot) {
        FSAnnotType type = [self.annot getType];
        if (type == FSAnnotHighlight || type == FSAnnotUnderline ||
            type == FSAnnotStrikeOut || type == FSAnnotSquiggly) {
            FSMarkup *annot = [[FSMarkup alloc] initWithAnnot:self.annot];
            NSMutableString *str = [NSMutableString stringWithFormat:@""];
            NSArray *quads = annot.quads;
            for (int i = 0; i < quads.count; i++) {
                FSQuadPoints *arrayQuad = [quads objectAtIndex:i];
                FSRectF *rect = [Utility convertToFSRect:arrayQuad.getFirst p2:arrayQuad.getFourth];
                NSString *tmp = [[Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:annot.pageIndex] getTextInRect:rect];
                if (tmp) {
                    if (i != 0)
                        [str appendString:@"\r\n"];
                    [str appendString:tmp];
                }
            }
            if (str && ![str isEqualToString:@""]) {
                UIPasteboard *board = [UIPasteboard generalPasteboard];
                board.string = str;
            }
        }
        else
        {
            NSString *contents = [Utility decodeEmojiString:self.annot.contents];
            if (contents.length > 0) {
                [UIPasteboard generalPasteboard].string = contents;
            }
        }
            
        [_extensionsManager setCurrentAnnot:nil];
    }

}

- (void)comment {
    [self openOrReply:NO];
}

- (void)reply {
    [self openOrReply:YES];
}

- (void)openOrReply:(BOOL)isReply {

    ReplyTableViewController *replyCtr = [[ReplyTableViewController alloc] initWithStyle:UITableViewStylePlain extensionsManager:_extensionsManager];
    replyCtr.isNeedReply = isReply;
    NSMutableArray *array = [NSMutableArray array];
    if (self.annot) {
        [ReplyUtil getReplysInDocument:_pdfViewCtrl.currentDoc annot:self.annot replys:array];
        [array addObject:self.annot];
    }
    
    [replyCtr setTableViewAnnotations:array];
    replyCtr.editingDoneHandler = ^() {
        [self->_extensionsManager setCurrentAnnot:nil];
    };
    replyCtr.editingCancelHandler = ^() {
        [self->_extensionsManager setCurrentAnnot:nil];
    };

    FSNavigationController *navCtr = [[FSNavigationController alloc] initWithRootViewController:replyCtr];
    navCtr.delegate = replyCtr;
    if (DEVICE_iPAD) {
        navCtr.modalPresentationStyle = UIModalPresentationFormSheet;
        navCtr.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    }
    UIViewController *rootViewController = self.pdfViewCtrl.fs_viewController;
    [rootViewController presentViewController:navCtr animated:YES completion:nil];
}

- (void) delete {
    [_extensionsManager.taskServer executeBlockSync:^{
        [self removeAnnot:self.annot];
    }];
}

- (void)edit {
    // noop, need override
}

- (void)play{
    // noop, need override
}

- (void)flatten{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFlatten") message:FSLocalizedForKey(@"kFlattenTip") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self showMenu];
    }];
    UIAlertAction *flattenAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFlatten") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        FSAnnot *annot = self.annot;
        if (self.extensionsManager.currentAnnot) {
            [self.extensionsManager setCurrentAnnot:nil];
        }
        FSPDFPage *page = [annot getPage];
        FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:annot];
        BOOL isAttachment = (annot.type == FSAnnotFileAttachment);
        [Utility flattenAnnot:annot completed:^(NSArray * _Nonnull replyAttributes) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAnnotLsitUpdate object:nil];
            if (isAttachment) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAttachmentLsitUpdate object:nil];
            }
            int pageIndex = [page getIndex];
            [self.pdfViewCtrl refresh:pageIndex needRender:YES];
            self.extensionsManager.isDocModified = YES;
            NSArray *undoItems = [UndoItem itemForUndoItemsWithAttributes:attributes extensionsManager:self.extensionsManager replyAttributes:replyAttributes];
            for (UndoItem *item in undoItems) {
                [self.extensionsManager removeUndoItem:item];
            }
        }];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:flattenAction];
    [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)speech{}

- (void)updateAnnotImage {
    if (self.annot && ![self shouldDrawAnnot:self.annot inPDFViewCtrl:self.pdfViewCtrl]) {
        self.annotImage = [Utility getAnnotImage:self.annot pdfViewCtrl:_pdfViewCtrl];
    }
}

- (void)drawAnnotInContext:(CGContextRef)context
{
    if(!self.annotImage)
        [self updateAnnotImage];
    CGRect rect = [Utility getAnnotRect:self.annot pdfViewCtrl:_pdfViewCtrl];
    if (self.annotImage) {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
        CGContextTranslateCTM(context, 0, rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
        CGContextDrawImage(context, rect, [self.annotImage CGImage]);
        CGContextRestoreGState(context);
    }

}


#pragma mark misc

- (CGRect)getAnnotRect:(FSAnnot *)annot {
    CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    if ([annot getFlags] & FSAnnotFlagNoRotate || [annot getFlags] & FSAnnotFlagNoZoom || [annot getType] == FSAnnotNote) {
        rect = [Utility getAnnotRect:annot pdfViewCtrl:self.pdfViewCtrl];
    }
    return rect;
}

- (void)refreshPageForAnnot:(FSAnnot *)annot reRenderPage:(BOOL)reRenderPage {
    if (annot && ![annot isEmpty]) {
        int pageIndex = annot.pageIndex;
        CGRect rect = [self getAnnotRect:annot];
        rect = UIEdgeInsetsInsetRect(rect, self.annotRefreshRectEdgeInsets);
        rect = CGRectIntersection(rect, [_pdfViewCtrl getPageView:pageIndex].bounds);
        [_pdfViewCtrl refresh:rect pageIndex:pageIndex needRender:reRenderPage];
    }
}

#pragma mark private

- (CGRect)newAnnotRectForPanTranslation:(CGPoint)translation annot:(FSAnnot *)annot editType:(FSAnnotEditType)editType {
    int pageIndex = annot.pageIndex;
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    UIEdgeInsets pageEdgeInsets = UIEdgeInsetsMake(self.pageEdgeInsets.top + self.dragDotSize.height/2, self.pageEdgeInsets.left + self.dragDotSize.width/2, self.pageEdgeInsets.bottom + self.dragDotSize.height/2, self.pageEdgeInsets.right + self.dragDotSize.width/2);
    CGRect pageInsetRect = UIEdgeInsetsInsetRect(pageView.bounds, pageEdgeInsets);
    CGRect annotRect = [self getAnnotRect:annot];
    CGFloat w = CGRectGetWidth(annotRect);
    CGFloat h = CGRectGetHeight(annotRect);
    CGSize minSize = [self minSizeForResizingAnnot:annot];
    CGSize maxSize = [self maxSizeForResizingAnnot:annot];
    CGFloat minW = minSize.width;
    CGFloat maxW = maxSize.width;
    CGFloat minH = minSize.height;
    CGFloat maxH = maxSize.height;
    CGFloat dx = 0;
    CGFloat dy = 0;
    CGFloat dw = 0;
    CGFloat dh = 0;
    if (editType == FSAnnotEditTypeFull) {
        dx = translation.x;
        dy = translation.y;
        dx = MIN(dx, CGRectGetMaxX(pageInsetRect) - CGRectGetMaxX(annotRect));
        dx = MAX(dx, CGRectGetMinX(pageInsetRect) - CGRectGetMinX(annotRect));
        dy = MIN(dy, CGRectGetMaxY(pageInsetRect) - CGRectGetMaxY(annotRect));
        dy = MAX(dy, CGRectGetMinY(pageInsetRect) - CGRectGetMinY(annotRect));
    } else {
        BOOL keepAspectRatio = [self keepAspectRatioForResizingAnnot:annot];
        if (!keepAspectRatio) {
            if (editType == FSAnnotEditTypeLeftTop ||
                editType == FSAnnotEditTypeLeftMiddle ||
                editType == FSAnnotEditTypeLeftBottom) { // left
                dw = -translation.x;
                dw = MIN(dw, maxW - w);
                dw = MAX(dw, minW - w);
                dw = MIN(dw, CGRectGetMinX(annotRect) - CGRectGetMinX(pageInsetRect));
                dx = -dw;
            } else if (editType == FSAnnotEditTypeRightTop ||
                       editType == FSAnnotEditTypeRightMiddle ||
                       editType == FSAnnotEditTypeRightBottom) { // right
                dw = translation.x;
                dw = MIN(dw, maxW - w);
                dw = MAX(dw, minW - w);
                dw = MIN(dw, CGRectGetMaxX(pageInsetRect) - CGRectGetMaxX(annotRect));
            }
            if (editType == FSAnnotEditTypeLeftTop ||
                editType == FSAnnotEditTypeMiddleTop ||
                editType == FSAnnotEditTypeRightTop) { // top
                dh = -translation.y;
                dh = MIN(dh, maxH - h);
                dh = MAX(dh, minH - h);
                dh = MIN(dh, CGRectGetMinY(annotRect) - CGRectGetMinY(pageInsetRect));
                dy = -dh;
            } else if (editType == FSAnnotEditTypeLeftBottom ||
                       editType == FSAnnotEditTypeMiddleBottom ||
                       editType == FSAnnotEditTypeRightBottom) { // bottom
                dh = translation.y;
                dh = MIN(dh, maxH - h);
                dh = MAX(dh, minH - h);
                dh = MIN(dh, CGRectGetMaxY(pageInsetRect) - CGRectGetMaxY(annotRect));
            }
        } else { // keep aspect ratio
            if (editType == FSAnnotEditTypeRightBottom) {
                CGFloat tmp = (w * translation.x + h * translation.y) / (w * w + h * h);
                dw = w * tmp;
                dh = h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(pageInsetRect) - CGRectGetMinX(annotRect));
                maxH = MIN(maxH, CGRectGetMaxY(pageInsetRect) - CGRectGetMinY(annotRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
            } else if (editType == FSAnnotEditTypeLeftTop) {
                CGFloat tmp = (w * translation.x + h * translation.y) / (w * w + h * h);
                dw = -w * tmp;
                dh = -h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(annotRect) - CGRectGetMinX(pageInsetRect));
                maxH = MIN(maxH, CGRectGetMaxY(annotRect) - CGRectGetMinY(pageInsetRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
                dx = -dw;
                dy = -dh;
            } else if (editType == FSAnnotEditTypeRightTop) {
                CGFloat tmp = (w * translation.x - h * translation.y) / (w * w + h * h);
                dw = w * tmp;
                dh = h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(pageInsetRect) - CGRectGetMinX(annotRect));
                maxH = MIN(maxH, CGRectGetMaxY(annotRect) - CGRectGetMinY(pageInsetRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
                dy = -dh;
            } else if (editType == FSAnnotEditTypeLeftBottom) {
                CGFloat tmp = (w * translation.x - h * translation.y) / (w * w + h * h);
                dw = -w * tmp;
                dh = -h * tmp;
                maxW = MIN(maxW, CGRectGetMaxX(annotRect) - CGRectGetMinX(pageInsetRect));
                maxH = MIN(maxH, CGRectGetMaxY(pageInsetRect) - CGRectGetMinY(annotRect));
                dw = MIN(MAX(dw, minW - w), maxW - w);
                dh = h / w * dw;
                dh = MIN(MAX(dh, minH - h), maxH - h);
                dw = w / h * dh;
                dx = -dw;
            }
        }
    }
    CGRect newRect = annotRect;
    newRect.origin.x += dx;
    newRect.origin.y += dy;
    newRect.size.width += dw;
    newRect.size.height += dh;
    return newRect;
}

- (CGPoint)updatedPanTranslation:(CGPoint)translation oldAnnotRect:(CGRect)oldAnnotRect newAnnotRect:(CGRect)newAnnotRect {
    CGFloat absorbedTranslationX = 0;
    CGFloat absorbedTranslationY = 0;
    if (_editType == FSAnnotEditTypeFull) {
        absorbedTranslationX = CGRectGetMinX(newAnnotRect) - CGRectGetMinX(oldAnnotRect);
        absorbedTranslationY = CGRectGetMinY(newAnnotRect) - CGRectGetMinY(oldAnnotRect);
    } else {
        if (_editType == FSAnnotEditTypeLeftTop ||
            _editType == FSAnnotEditTypeLeftMiddle ||
            _editType == FSAnnotEditTypeLeftBottom) { // left
            absorbedTranslationX = CGRectGetMinX(newAnnotRect) - CGRectGetMinX(oldAnnotRect);
        } else if (_editType == FSAnnotEditTypeRightTop ||
                   _editType == FSAnnotEditTypeRightMiddle ||
                   _editType == FSAnnotEditTypeRightBottom) { // right
            absorbedTranslationX = CGRectGetMaxX(newAnnotRect) - CGRectGetMaxX(oldAnnotRect);
        } else {
            absorbedTranslationX = translation.x;
        }

        if (_editType == FSAnnotEditTypeLeftTop ||
            _editType == FSAnnotEditTypeMiddleTop ||
            _editType == FSAnnotEditTypeRightTop) { // top
            absorbedTranslationY = CGRectGetMinY(newAnnotRect) - CGRectGetMinY(oldAnnotRect);
        } else if (_editType == FSAnnotEditTypeLeftBottom ||
                   _editType == FSAnnotEditTypeMiddleBottom ||
                   _editType == FSAnnotEditTypeRightBottom) { // bottom
            absorbedTranslationY = CGRectGetMaxY(newAnnotRect) - CGRectGetMaxY(oldAnnotRect);
        } else {
            absorbedTranslationY = translation.y;
        }
    }
    translation.x -= absorbedTranslationX;
    translation.y -= absorbedTranslationY;
    return translation;
}

@end
