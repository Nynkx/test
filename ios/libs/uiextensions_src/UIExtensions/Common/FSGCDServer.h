/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FSGCDGroupServer : NSObject

@property (nonatomic, readonly) BOOL isCancelled;
- (instancetype)initWithQueue:(dispatch_queue_t)queue;

- (void)cancelAllOperations;
- (void)enter;
- (void)leave;
- (void)executeBlock:(void(^)())block;
- (void)notifyBlock:(void(^)())block;
@end


NS_ASSUME_NONNULL_END
