/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PrivateDefine.h"

/** Whether it is iphone/ipad. */
#define DEVICE_iPHONE ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define DEVICE_iPAD   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define DEVICE_MAC_CATALYST  _MAC_CATALYST_
#define IOS11_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 11.0)

/** Define notification center observer messages for annotation events. */
#define ANNOLIST_UPDATETOTAL @"AnnoList_UpdateTotal"
#define ANNOTATION_UNREAD_TOTALCOUNT @"AnnotationUnreadTotalcount"
#define CLEAN_ANNOTATIONLIST @"CLEAN_ANNOTATIONLIST"
#define ORIENTATIONCHANGED @"ORIENTATIONCHANGED"

//Define the internal used paths
#define TEMP_PATH NSTemporaryDirectory()
#define LIBRARY_PATH [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define SIGNATURE_FOLDER_NAME @"Signature"
#define DATA_FOLDER_NAME @"Data"
#define DATA_PATH [LIBRARY_PATH stringByAppendingPathComponent:DATA_FOLDER_NAME]
#define SIGNATURE_PATH [DATA_PATH stringByAppendingPathComponent:SIGNATURE_FOLDER_NAME]

//post Notification name
#define NOTIFICATION_NAME_APP_HANDLE_OPEN_URL @"app_handle_open_url"

/** Shorter side of main screen. */
#define STYLE_CELLWIDTH_IPHONE ([UIScreen mainScreen].bounds.size.width < [UIScreen mainScreen].bounds.size.height ? [UIScreen mainScreen].bounds.size.width : [UIScreen mainScreen].bounds.size.height)
/** longer side of main screen. */
#define STYLE_CELLHEIHGT_IPHONE ([UIScreen mainScreen].bounds.size.width > [UIScreen mainScreen].bounds.size.height ? [UIScreen mainScreen].bounds.size.width : [UIScreen mainScreen].bounds.size.height)

/** Note icon width. */
#define NOTE_ANNOTATION_WIDTH 20

/** Rectangle inset distance.*/
#define RECT_INSET -15

/** Operation types of annotation. */
#define AnnotationOperation_Add 1
#define AnnotationOperation_Delete 2
#define AnnotationOperation_Modify 3

#define APPLICATION_ISFULLSCREEN [UIApplication sharedApplication].statusBarHidden
#define Annot_Signature 11
#define Module_Signature @"Module_Signature"

#define SCREENWIDTH [UIScreen mainScreen].bounds.size.width
#define SCREENHEIGHT [UIScreen mainScreen].bounds.size.height

#define isIPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? [[UIScreen mainScreen] currentMode].size.height==2436 : NO)
#define isPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define IS_IPHONE_X_XS ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? ([[UIScreen mainScreen] currentMode].size.height == 2436 && !isPad) : NO)
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? ([[UIScreen mainScreen] currentMode].size.height == 1792 && !isPad) : NO)
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? ([[UIScreen mainScreen] currentMode].size.height == 2688 && !isPad) && !isPad : NO)
#define IS_IPHONE_OVER_TEN ((IS_IPHONE_X_XS==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs_Max== YES) ? YES : NO)

#define Height_StatusBar ((IS_IPHONE_X_XS==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs_Max== YES) ? 44.0 : 20.0)
#define Height_NavBar ((IS_IPHONE_X_XS==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs_Max== YES) ? 88.0 : 64.0)
#define Height_TabBar ((IS_IPHONE_X_XS==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs_Max== YES) ? 83.0 : 49.0)
