/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#ifndef UIExtensionsManager_Private_h
#define UIExtensionsManager_Private_h

#import "TbBaseBar.h"
#import "MoreView.h"
#import "MenuItem.h"
#import "MenuControl.h"
#import "AnnotationToolsBar.h"
#import "PropertyBar.h"
#import "UIExtensionsManager.h"
#import "FSUndo.h"
#import "TSAlertView.h"
#import "FSAnnotExtent.h"
#import "Preference.h"
#import "SettingPreference.h"
#import "TaskServer.h"
#import "GlableInstance.h"
#import "PropertyBar.h"
#import "FSThumbnailViewController.h"
#import "FSThumbnailCache.h"
#import "StampIconController.h"
#import "DXPopover.h"
#import "FSPresentedVCAnimator.h"
#import "FSNavigationController.h"
#import "UIExtensionsConfig+private.h"
#import "FileSelectDestinationViewController.h"

#import "SpeechModule.h"

/** @brief Module base. */
@protocol IModule <NSObject>
/** @brief Get the module name. */
- (NSString *_Nonnull)getName;
@end

NS_ASSUME_NONNULL_BEGIN

/** @brief The state change event listener. */
@protocol IStateChangeListener <NSObject>
@required
/**
 * @brief Triggered when state changed.
 *
 * @param[in] state     New state.
 */
- (void)onStateChanged:(int)state;
@end

typedef NS_ENUM(NSInteger, FSAnnotPropertyColorType) {
  FSAnnotPropertyColorTypeDefault = 0,
  FSAnnotPropertyColorTypeBorder = 1,
  FSAnnotPropertyColorTypeText = 2,
  FSAnnotPropertyColorTypeFill = 3
};
/** @brief A listener for annotation property bar. */
@protocol IAnnotPropertyListener <NSObject>
@optional
/** @brief Triggered when the color of annotation is changed. */
- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType;
- (void)onAnnotColorChanged:(unsigned int)color colorType:(FSAnnotPropertyColorType)colorType annotType:(FSAnnotType)annotType;
/** @brief Triggered when the opacity of annotation is changed. */
- (void)onAnnotOpacityChanged:(unsigned int)opacity annotType:(FSAnnotType)annotType;
- (void)onAnnotLineWidthChanged:(unsigned int)lineWidth annotType:(FSAnnotType)annotType;
- (void)onAnnotFontNameChanged:(NSString *)fontName annotType:(FSAnnotType)annotType;
- (void)onAnnotFontSizeChanged:(unsigned int)fontSize annotType:(FSAnnotType)annotType;
- (void)onAnnotIconChanged:(int)icon annotType:(FSAnnotType)annotType;
- (void)onAnnotDistanceUnitChanged:(int)icon annotType:(FSAnnotType)annotType;
- (void)onAnnotRotationChanged:(FSRotation)rotation annotType:(FSAnnotType)annotType;
@end

/** @brief The undo/redo listener. */
@protocol IFSUndoEventListener <NSObject>
@optional
/** @brief Triggered after the state of undo or redo changed. */
- (void)onUndoChanged;
- (void)onWillUndo;
- (void)onDidUndo;
- (void)onWillRedo;
- (void)onDidRedo;
@end

/** @brief The undo/redo handler,it should handle the operations about undo/redo. */
@protocol FSUndo <NSObject>
/** @brief Check whether can undo or not. */
- (BOOL)canUndo;
/** @brief Check whether can redo or not. */
- (BOOL)canRedo;
/** @brief Undo the previous operation. */
- (void)undo;
/** @brief Redo the previous operation. */
- (void)redo;
/** @brief Clear all the recorded undo/redo operations. */
- (void)clearUndoRedo;
@end

@class MenuControl;
@class MenuControlDelegate;
@class ExAnnotIconProviderCallback;
@class ExActionHandler;
@class PasswordModule;
@class PanZoomView;
@protocol FSPageOrganizerDelegate;

/** @brief Private implementation for extension manager, these properites and methods are not supposed to be called. */
@interface UIExtensionsManager () <FSUndo, IDrawEventListener, IPropertyValueChangedListener, IGestureEventListener, IScrollViewEventListener, ISearchEventListener, UIPopoverPresentationControllerDelegate, FSThumbnailViewControllerDelegate, SettingBarDelegate, ILayoutEventListener, MenuControlDelegate, IPropertyBarListener>
@property (nonatomic, strong) FSToolbar *editBar;
@property (nonatomic, strong) FSToolbar *editDoneContentBar;
@property (nonatomic, strong) FSToolbar *toolSetContentBar;
@property (nonatomic, strong) TbBaseBar *editDoneBar;
@property (nonatomic, strong) TbBaseBar *toolSetBar;
@property (nonatomic, strong) AnnotationToolsBar *annotationToolsBar;
@property (nonatomic, assign) BOOL hiddenMoreMenu;
@property (nonatomic, assign) BOOL hiddenEditBar;
@property (nonatomic, assign) BOOL hiddenEditDoneBar;
@property (nonatomic, assign) BOOL hiddenToolSetBar;

//@property (nonatomic, assign) BOOL isDocModified;
@property (nonatomic, assign) BOOL isFileEdited;
@property (nonatomic, assign) BOOL isSupportRedaction;
@property (nonatomic, assign) BOOL currentIsFullScreen;

@property (nonatomic, strong) NSMutableArray *stateChangeListeners;
@property (nonatomic, strong) NSMutableArray *annotListeners;
@property (nonatomic, strong) NSMutableArray *toolListeners;
@property (nonatomic, strong) NSMutableArray *searchListeners;
@property (nonatomic, strong) NSMutableArray *toolHandlers;
@property (nonatomic, strong) NSMutableArray *annotHandlers;
@property (nonatomic, strong) NSMutableArray *docModifiedListeners;
@property (nonatomic, assign) int noteIcon;
@property (nonatomic, assign) int attachmentIcon;
@property (nonatomic, assign) int eraserLineWidth;
@property (nonatomic, assign) int stampIcon;
@property (nonatomic, strong) NSString *distanceUnit;
@property (nonatomic, assign) FSRotation screenAnnotRotation;
@property (nonatomic, strong) PropertyBar *propertyBar;
@property (nonatomic, strong) TaskServer *taskServer;
@property (nonatomic, strong) MenuControl *menuControl;

@property (nonatomic, strong) ExAnnotIconProviderCallback *iconProvider;
//@property (nonatomic, strong) ExActionHandler *actionHandler;

@property (nonatomic, strong) UIExtensionsConfig *config;
@property (nonatomic, strong) PasswordModule *passwordModule;
@property (nonatomic, strong) SpeechModule *speechModule;

@property (nonatomic, assign) BOOL isShowBlankMenu;
@property (nonatomic, assign) CGPoint currentPoint;
@property (nonatomic, assign) int currentPageIndex;

@property (nonatomic, assign) BOOL hiddenPanel;
@property (nonatomic, assign) BOOL hiddenTopToolbar;
@property (nonatomic, assign) BOOL hiddenBottomToolbar;
@property (nonatomic, assign) BOOL hiddenSettingBar;

// in dragging, rotation and zooming, menu or property bar is hidden first and shown later
// these properties record states before animation, don't use them in other conditions
@property (nonatomic) BOOL shouldShowMenu;
@property (nonatomic) BOOL shouldShowPropertyBar;
@property (nonatomic) BOOL isRotating;
@property (nonatomic) BOOL isScrollViewDragging;
@property (nonatomic) BOOL isScrollViewDecelerating;
@property (nonatomic) BOOL isScrollViewZooming;

@property (nonatomic, strong) NSMutableArray<UndoItem *> *undoItems;
@property (nonatomic, strong) NSMutableArray<UndoItem *> *redoItems;
@property (nonatomic, strong) NSMutableArray<IFSUndoEventListener> *undoListeners;

@property (nonatomic, strong) NSMutableDictionary *propertyBarListeners;
@property (nonatomic, strong) NSMutableArray *rotateListeners;

@property (nonatomic, strong) NSMutableDictionary *annotColors;
@property (nonatomic, strong) NSMutableDictionary *annotTextColors;
@property (nonatomic, strong) NSMutableDictionary *annotOpacities;
@property (nonatomic, strong) NSMutableDictionary *annotLineWidths;
@property (nonatomic, strong) NSMutableDictionary *annotFontSizes;
@property (nonatomic, strong) NSMutableDictionary *annotFontNames;

@property (nonatomic, strong) NSMutableArray<IAnnotPropertyListener> *annotPropertyListeners;

@property (nonatomic, strong) NSMutableArray<IGestureEventListener> *guestureEventListeners;

@property (nonatomic, strong) NSMutableArray<ILinkEventListener> *linkEventListeners;

@property (nonatomic, strong) StampIconController *stampIconController;
//@property (nonatomic, strong) UIPopoverController *popOverController;

@property (nonatomic, strong) NSMutableArray *securityHandlers;
@property (nonatomic, strong) NSMutableArray *modules;

@property (nonatomic, strong, nullable) FSThumbnailViewController *thumbnailViewController;
@property (nonatomic, strong) FSThumbnailCache *thumbnailCache;
@property (nonatomic, strong) FSPresentedVCAnimator *presentedVCAnimator;

@property (nonatomic, strong) NSMutableArray *fullScreenListeners;
@property (nonatomic, strong) NSMutableArray *panelListeners;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIViewController *settingbarPopoverCtr;
@property (nonatomic, strong) UIButton *annotButton;
@property (nonatomic, assign) BOOL enableTopToolbar;
@property (nonatomic, assign) BOOL enableBottomToolbar;
@property (nonatomic, assign) BOOL isSuspendAutoFullScreen;
@property (nonatomic, assign) int currentState;
@property (nonatomic, strong, nullable) Popover *popover;
@property (nonatomic, strong, readwrite) MoreMenuView *more;

@property (nonatomic, strong) UIToolbar *topToolbarSaved;
@property (nonatomic, strong) UIToolbar *bottomToolbarSaved;

@property (nonatomic, strong) NSArray<UIBarButtonItem*> *topToolBarItemsArr;
@property (nonatomic, strong) NSArray<UIBarButtonItem*> *bottomToolBarItemsArr;

@property (nonatomic, strong) UIControl *settingBarMaskView;
@property (nonatomic, strong) UIView *settingBarContainer;
@property (nonatomic, strong) NSMutableArray *pageNumberListeners;

//pan&zoom
@property (nonatomic, strong, nullable) PanZoomView* panZoomView;
@property (nonatomic, strong, nullable) FSView *panZoomBottomContentView;
@property (nonatomic, strong) UISlider* zoomSlider;
@property (nonatomic, strong) UIBarButtonItem* prevPageButtonItem;
@property (nonatomic, strong) UIBarButtonItem* nextPageButtonItem;

@property (nonatomic, strong) NSArray *topToolbarVerticalConstraints;
@property (nonatomic, strong) NSArray *bottomToolbarVerticalConstraints;

@property (nonatomic, assign) BOOL isViewSignedDocument;
@property (nonatomic) dispatch_semaphore_t form_wait_catalyst;

- (void)setCurrentAnnot:(FSAnnot *_Nullable)anot;
- (int)getAnnotOpacity:(FSAnnotType)annotType;
- (void)setAnnotOpacity:(int)opacity annotType:(FSAnnotType)annotType;
- (int)getAnnotLineWidth:(FSAnnotType)annotType;
- (void)setAnnotLineWidth:(int)lineWidth annotType:(FSAnnotType)annotType;
- (int)getAnnotFontSize:(FSAnnotType)annotType;
- (void)setAnnotFontSize:(int)fontSize annotType:(FSAnnotType)annotType;
- (unsigned int)getAnnotTextColor:(FSAnnotType)annotType;
- (NSString *)getAnnotFontName:(FSAnnotType)annotType;
- (void)setAnnotFontName:(NSString *)fontName annotType:(FSAnnotType)annotType;
- (int)filterAnnotType:(FSAnnotType)annotType;
- (id<IAnnotHandler>)getAnnotHandlerByAnnot:(FSAnnot *)annot;
- (id<IAnnotHandler>)getDynamicAnnotHandler;
- (id<IModule>)getModuleByName:(NSString *)name;

- (void)saveAndCloseCurrentDoc:(void (^_Nullable)(BOOL success))completion;

- (void)registerRotateChangedListener:(id<IRotationEventListener>)listener;
- (void)unregisterRotateChangedListener:(id<IRotationEventListener>)listener;

- (void)registerGestureEventListener:(id<IGestureEventListener>)listener;
- (void)unregisterGestureEventListener:(id<IGestureEventListener>)listener;

- (void)removeThumbnailCacheOfPageAtIndex:(NSUInteger)pageIndex;
- (void)clearThumbnailCachesForPDFAtPath:(NSString *)path;

/** @brief Register annotation property change event listener. */
- (void)registerAnnotPropertyListener:(id<IAnnotPropertyListener>)listener;
/** @brief Unregister annotation property change event listener. */
- (void)unregisterAnnotPropertyListener:(id<IAnnotPropertyListener>)listener;

/** @brief Register a state change event listener. */
- (void)registerStateChangeListener:(id<IStateChangeListener>)listener;
/** @brief Unregister a state change event listener. */
- (void)unregisterStateChangeListener:(id<IStateChangeListener>)listener;

- (void)showPropertyBar;
- (void)hidePropertyBar;

-(FSXFAWidget *)getXFAWidgetAtPoint:(CGPoint)pvPoint pageIndex:(int)pageIndex;

#pragma mark - Undo/redo event listeners.
/** @brief Register the undo/redo event listener. */
- (void)registerUndoEventListener:(id<IFSUndoEventListener>)listener;
/** @brief Unregister the undo/redo event listener. */
- (void)unregisterUndoEventListener:(id<IFSUndoEventListener>)listener;

- (void)addUndoItem:(UndoItem *)undoItem;
- (void)removeUndoItem:(UndoItem *)undoItem;
- (void)addUndoItems:(NSMutableArray *)undoItems;
- (void)removeUndoItems;

@end

@interface UIExtensionsManager (Private)
- (void)addPopupWithAnnot:(FSAnnot *)annot;
@end

@interface ExAnnotIconProviderCallback : NSObject <FSIconProviderCallback>
- (NSString *)getProviderID;
- (NSString *)getProviderVersion;
- (BOOL)hasIcon:(FSAnnotType)annotType iconName:(NSString *)iconName;
- (BOOL)canChangeColor:(FSAnnotType)annotType iconName:(NSString *)iconName;
- (FSPDFPage *)getIcon:(FSAnnotType)annotType iconName:(NSString *)iconName color:(unsigned int)color;
- (FSShadingColor *)getShadingColor:(FSAnnotType)annotType iconName:(NSString *)iconName refColor:(unsigned int)refColor shadingIndex:(int)shadingIndex;
- (NSNumber *)getDisplayWidth:(FSAnnotType)annotType iconName:(NSString *)iconName;
- (NSNumber *)getDisplayHeight:(FSAnnotType)annotType iconName:(NSString *)iconName;
@end


@interface JSAlertViewDelegate: NSObject<TSAlertViewDelegate>
- (id)initWithType:(int)type;
- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
@property (nonatomic) int type;
@property (nonatomic, readwrite) int retCode;
@end

@interface ExActionHandler : NSObject <FSActionCallback>
@property (nonatomic, weak) FSPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, weak) UIExtensionsManager *extensionsManager;
@property (nonatomic, strong, nullable) JSAlertViewDelegate *delegate;

- (id)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager;
- (int)getCurrentPage:(FSPDFDoc *)pdfDoc;
- (void)setCurrentPage:(FSPDFDoc *)pdfDoc pageIndex:(int)pageIndex;
- (FSRotation)getPageRotation:(FSPDFDoc *)pdfDoc pageIndex:(int)pageIndex;
- (BOOL)setPageRotation:(FSPDFDoc *)pdfDoc pageIndex:(int)pageIndex rotation:(FSRotation)rotation;
- (int)alert:(NSString *)msg title:(NSString *)title type:(int)type icon:(int)icon;
- (FSIdentityProperties *)getIdentityProperties;
- (BOOL)setDocChangeMark:(FSPDFDoc*)document change_mark:(BOOL)change_mark;
- (BOOL)getDocChangeMark:(FSPDFDoc*)document;
@end

NS_ASSUME_NONNULL_END

#endif /* UIExtensionsManager_Private_h */
