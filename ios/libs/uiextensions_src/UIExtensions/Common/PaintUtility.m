/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PaintUtility.h"
#import "PrivateDefine.h"

@implementation PaintUtility

-(CGPoint)rotateVec:(float)px py:(float)py ang:(float)ang isChlen:(BOOL)isChlen newLine:(float)newLen
{
    CGPoint point = CGPointMake(0, 0);
    float vx = px * cosf(ang) - py * sinf(ang);
    float vy = px * sinf(ang) + py * cosf(ang);
    if (isChlen) {
        float d = sqrtf(vx * vx + vy * vy);
        vx = vx / d * newLen;
        vy = vy / d * newLen;
        point.x = vx;
        point.y = vy;
    }
    return point;
}

-(void)drawLineFromStartPoint:(CGPoint)startPoint toEndPoint:(CGPoint)endPoint inContext:(CGContextRef)context isArrowLine:(BOOL) isArrowLine{
    float lineWidth = self.annotLineWidth;
    CGContextSetLineWidth(context, lineWidth);
    
    UIColor *color = [UIColor colorWithRGB:self.annotColor];
    CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],self.annotOpacity);
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, startPoint.x, startPoint.y);
    CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
    CGContextSetBlendMode(context, kCGBlendModeDestinationAtop);
    CGContextStrokePath(context);
    
    if (isArrowLine)
    {
        float x1 = startPoint.x;
        float y1 = startPoint.y;
        float x2 = endPoint.x;
        float y2 = endPoint.y;
        
        float arctanglent = M_PI / 6;
        float arrow_len = lineWidth*4.0;
        
        CGPoint endPoint1 = [self rotateVec:(x2 - x1) py:(y2 - y1) ang:arctanglent isChlen:YES newLine:arrow_len];
        CGPoint endPoint2 = [self rotateVec:(x2 - x1) py:(y2 - y1) ang:-arctanglent isChlen:YES newLine:arrow_len];
        
        float x3 = x2 - endPoint1.x;
        float y3 = y2 - endPoint1.y;
        float x4 = x2 - endPoint2.x;
        float y4 = y2 - endPoint2.y;
        
        if (isnan(x3) || isnan(y3) || isnan(x2) || isnan(y2)|| isnan(x4) || isnan(y4)) {
            return;
        }
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, x3, y3);
        CGPathAddLineToPoint(path, NULL, x2, y2);
        CGPathAddLineToPoint(path, NULL, x4, y4);
        CGPathRef pathToDraw = CGPathCreateCopyByStrokingPath(path, NULL, 0.1, kCGLineCapRound, kCGLineJoinRound, 0);
        
        CGContextAddPath(context, pathToDraw);
        CGContextStrokePath(context);
        CGPathRelease(path);
        CGPathRelease(pathToDraw);
    }
}

-(void)drawPencilWith:(NSArray *)arrayLines inContext:(CGContextRef)context {
    float lineWidth = self.annotLineWidth;
    CGContextSetLineWidth(context, lineWidth);
    
    UIColor *color = [UIColor colorWithRGB:self.annotColor];
    CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],self.annotOpacity);
    
    CGContextBeginPath(context);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetBlendMode(context, kCGBlendModeDestinationAtop);
    
    for (int i = (int)arrayLines.count - 1; i >= 0; i--) {
        __weak NSMutableArray *pointArray = arrayLines[i][@"pointArray"];
        if (pointArray.count > 0) {
            CGPoint startPoint = [[pointArray objectAtIndex:0] CGPointValue];
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            for (int i = 1; i < pointArray.count; i++) {
                CGPoint endPoint = [[pointArray objectAtIndex:i] CGPointValue];
                CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            }
            CGContextStrokePath(context);
            break;
        }else{
            continue;
        }
    }
}

-(void)drawShapWithRect:(CGRect)rect type:(FSAnnotType)type inContext:(CGContextRef)context {
    float lineWidth = self.annotLineWidth;
    CGContextSetLineWidth(context, lineWidth);
    
    UIColor *color = [UIColor colorWithRGB:self.annotColor];
    CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],self.annotOpacity);
    
    if (type == FSAnnotSquare) {
        CGContextStrokeRect(context, rect);
    }else{
        CGContextStrokeEllipseInRect(context, rect);
    }
    
}

-(void)drawMultipleSelectWithRect:(CGRect)rect inContext:(CGContextRef)context {
    float lineWidth = self.annotLineWidth;
    CGContextSetLineWidth(context, lineWidth);
    
    UIColor *color = [UIColor colorWithRGB:self.annotColor];
    
    CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],1);
    CGContextStrokeRect(context, rect);
    
    CGContextSetRGBFillColor(context,[color red], [color green], [color blue],self.annotOpacity);
    CGContextFillRect(context, rect);
}

-(void)drawMultipleSelectedWithRect:(CGRect)rect inContext:(CGContextRef)context {
    float lineWidth = self.annotLineWidth;
    CGContextSetLineWidth(context, lineWidth);
    
    UIColor *color = [UIColor colorWithRGB:self.annotColor];
    
    CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],self.annotOpacity);
    CGContextStrokeRect(context, rect);
}

@end
