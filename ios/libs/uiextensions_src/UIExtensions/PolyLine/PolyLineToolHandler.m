/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PolyLineToolHandler.h"

@interface PolyLineToolHandler ()

@property (nonatomic, strong) FSPointFArray *vertexes;
@property (nonatomic, strong) UIImage *annotImage;
@property (nonatomic) int pageIndex;

@end

@implementation PolyLineToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotPolyLine;
        _vertexes = [[FSPointFArray alloc] init];
        _annot = nil;
        _annotImage = nil;
        _minVertexDistance = 5;
        _pageIndex = -1;
    }
    return self;
}

- (NSString *)getName {
    return Tool_PolyLine;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)reset {
    if (self.annot) {
        FSPolyLine *annot = self.annot;
        self.annot = nil;
        self.annotImage = nil;
        int pageIndex = annot.pageIndex;
        id<IAnnotHandler> annothandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
        [annothandler addAnnot:annot addUndo:YES];
        CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        
        if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_LEFT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_RIGHT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_MIDDLE) {
            [_pdfViewCtrl refresh:pageIndex];
            [_pdfViewCtrl refresh:pageIndex + 1];
            [_pdfViewCtrl refresh:pageIndex - 1];
        }else{
            [_pdfViewCtrl refresh:CGRectInset(pvRect, -30, -30) pageIndex:pageIndex needRender:YES];
        }
    } else if ([self.vertexes getSize] == 1) {
        int pageIndex = self.pageIndex;
        CGPoint vertex = [_pdfViewCtrl convertPdfPtToPageViewPt:[self.vertexes getAt:0] pageIndex:pageIndex];
        [_pdfViewCtrl refresh:CGRectInset(CGRectMake(vertex.x, vertex.y, 0, 0), -30, -30) pageIndex:pageIndex needRender:NO];
    }
    [self.vertexes removeAll];
    self.pageIndex = -1;
}

- (void)onDeactivate {
    [self reset];
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)isVertexValid:(CGPoint)vertex pageIndex:(int)pageIndex {
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect validRect = CGRectInset(pageView.bounds, 5, 5);
    if (!CGRectContainsPoint(validRect, vertex)) {
        return NO;
    }
    if ([self.vertexes getSize] > 0) {
        CGPoint lastPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:[self.vertexes getAt:[self.vertexes getSize] - 1] pageIndex:pageIndex];
        CGFloat dx = lastPoint.x - vertex.x;
        CGFloat dy = lastPoint.y - vertex.y;
        if (dx * dx + dy * dy < self.minVertexDistance * self.minVertexDistance) {
            return NO;
        }
    }
    return YES;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (self.pageIndex != pageIndex) {
        return;
    }
    if (self.annotImage) {
        CGRect annotRect = [Utility getAnnotRect:self.annot pdfViewCtrl:_pdfViewCtrl];
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
        annotRect.origin.x = rect.origin.x;
        annotRect.origin.y = rect.origin.y;
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, annotRect.origin.x, annotRect.origin.y);
        CGContextTranslateCTM(context, 0, annotRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextTranslateCTM(context, -annotRect.origin.x, -annotRect.origin.y);
        CGContextDrawImage(context, annotRect, [self.annotImage CGImage]);
        CGContextRestoreGState(context);
    }
    UIImage *dot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIImage *highlightDot = [UIImage imageNamed:@"annotation_drag_highlight.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    CGSize dotSize = dot.size;
    for (int i = 0; i < [self.vertexes getSize]; i++) {
        FSPointF *vertex = [self.vertexes getAt:i];
        CGPoint point = [_pdfViewCtrl convertPdfPtToPageViewPt:vertex pageIndex:pageIndex];
        if (i == 0) {
            point.x -= highlightDot.size.width / 2;
            point.y -= highlightDot.size.height / 2;
            [highlightDot drawAtPoint:point];
        } else {
            point.x -= dotSize.width / 2;
            point.y -= dotSize.height / 2;
            [dot drawAtPoint:point];
        }
    }
}

- (void)updateAnnotVertexesInPage:(int)pageIndex {
    if (self.annot == nil && [self.vertexes getSize] > 1) {
        self.annot = [self addAnnotToPage:pageIndex];
    }
    if (self.annot) {
        [self.annot setBorderInfo:({
            FSBorderInfo *borderInfo = [[FSBorderInfo alloc] init];
            [borderInfo setWidth:[_extensionsManager getAnnotLineWidth:self.type]];
            [borderInfo setStyle:FSBorderInfoSolid];
            borderInfo;
        })];

        [self.annot setVertexes:self.vertexes];
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        self.annotImage = [Utility getAnnotImage:self.annot pdfViewCtrl:_pdfViewCtrl];
        CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
        [_pdfViewCtrl refresh:CGRectInset(pvRect, -30, -30) pageIndex:pageIndex needRender:NO];
    } else if ([self.vertexes getSize] == 1) {
        CGPoint vertex = [_pdfViewCtrl convertPdfPtToPageViewPt:[self.vertexes getAt:0] pageIndex:pageIndex];
        [_pdfViewCtrl refresh:CGRectInset(CGRectMake(vertex.x, vertex.y, 0, 0), -30, -30) pageIndex:pageIndex needRender:NO];
    }
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    if (self.annot && self.annot.pageIndex != pageIndex) {
        return YES;
    }
    if (self.pageIndex != -1 && self.pageIndex != pageIndex) {
        [self reset];
        //        _extensionsManager.currentToolHandler = self;
    }
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    if (![self isVertexValid:point pageIndex:pageIndex]) {
        return YES;
    }
    self.pageIndex = pageIndex;
    [self.vertexes add:[_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex]];
    [self updateAnnotVertexesInPage:pageIndex];
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (FSPolyLine *)addAnnotToPage:(int)pageIndex {
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;

    FSPolyLine *annot = nil;
    FSRectF *rect = [[FSRectF alloc] init];
    [rect setLeft:0];
    [rect setBottom:0];
    [rect setRight:0];
    [rect setTop:0];
    @try {
        annot = [[FSPolyLine alloc] initWithAnnot:[page addAnnot:self.type rect:rect]];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return nil;
    }
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.color = [_extensionsManager getPropertyBarSettingColor:self.type];
    annot.opacity = [_extensionsManager getAnnotOpacity:self.type] / 100.0f;
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    annot.flags = FSAnnotFlagPrint;
    [annot setBorderInfo:({
               FSBorderInfo *borderInfo = [[FSBorderInfo alloc] init];
               [borderInfo setWidth:[_extensionsManager getAnnotLineWidth:self.type]];
               [borderInfo setStyle:FSBorderInfoSolid];
               borderInfo;
           })];
//    annot.intent = @"PolyLineDimension";
    annot.subject = @"PolyLine";
    return annot;
}

#pragma mark <IAnnotPropertyListener>

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (self.annot && annotType == self.type) {
        self.annot.color = color;
        [self updateAnnotProperty];
    }
}

- (void)onAnnotOpacityChanged:(unsigned int)opacity annotType:(FSAnnotType)annotType {
    if (self.annot && annotType == self.type) {
        self.annot.opacity = opacity / 100.0f;
        [self updateAnnotProperty];
    }
}

- (void)onAnnotLineWidthChanged:(unsigned int)lineWidth annotType:(FSAnnotType)annotType {
    if (self.annot && annotType == self.type) {
        self.annot.lineWidth = lineWidth;
        [self updateAnnotProperty];
    }
}

- (void)updateAnnotProperty {
    if (self.annot) {
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        self.annotImage = [Utility getAnnotImage:self.annot pdfViewCtrl:_pdfViewCtrl];
        CGRect pvRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:self.pageIndex];
        [_pdfViewCtrl refresh:CGRectInset(pvRect, -30, -30) pageIndex:self.pageIndex needRender:NO];
    }
}

@end
