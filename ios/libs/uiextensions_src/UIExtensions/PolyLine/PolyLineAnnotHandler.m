/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PolyLineAnnotHandler.h"

@interface PolyLineAnnotHandler () <IDocEventListener>

@property (nonatomic, assign) BOOL isShowStyle;

@property (nonatomic) CGFloat maxFingerBias;
@property (nonatomic, strong) FSPointFArray *vertexes;

typedef NS_ENUM(NSUInteger, EditType) {
    e_editTypeWholeAnnot,
    e_editTypeSingleVertex,
    e_editTypeUnknown
};
@property (nonatomic) EditType editType;
@property (nonatomic) int movingVertexIndex;

// on pan, hide menu or property bar first and show it later
@property (nonatomic) BOOL shouldShowMenu;
@property (nonatomic) BOOL shouldShowPropertyBar;

@end

@implementation PolyLineAnnotHandler

@dynamic editType;

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super initWithUIExtensionsManager:extensionsManager];
    if (self) {
        self.colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
        self.isShowStyle = NO;
        self.minVertexDistance = 5;
        self.maxFingerBias = 25;
        self.vertexes = [[FSPointFArray alloc] init];
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
    }
    return self;
}

- (FSAnnotType)getType {
    return FSAnnotPolyLine;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = FSMenuOptionOpen;
    options |= FSMenuOptionFlatten;
    if (![Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options = FSMenuOptionNone;
    }
    if (annot.canModify && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionStyle;
        if (annot.canDelete) {
            options |= FSMenuOptionDelete;
        }
    }
    if (annot.canReply && [Utility canAddAnnot:self.extensionsManager.pdfViewCtrl]) {
        options |= FSMenuOptionReply;
    }
    return options;
}

- (void)onAnnotSelected:(FSAnnot *)annot {
    [super onAnnotSelected:annot];
    self.vertexes = [[FSPolyLine alloc] initWithAnnot:annot].vertexes;
}

- (void)onAnnotDeselected:(FSAnnot *)annot {
    [super onAnnotDeselected:annot];
    self.vertexes = nil;
}

- (void)showStyle {
    [self.extensionsManager.propertyBar setColors:self.colors];
    [self.extensionsManager.propertyBar resetBySupportedItems:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH frame:CGRectZero];
    FSAnnot *annot = self.extensionsManager.currentAnnot;
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR | PROPERTY_OPACITY | PROPERTY_LINEWIDTH lock:!annot.canModifyAppearance];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_COLOR intValue:annot.color];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_OPACITY intValue:annot.opacity * 100.0];
    [self.extensionsManager.propertyBar setProperty:PROPERTY_LINEWIDTH intValue:annot.lineWidth];
    [self.extensionsManager.propertyBar addListener:self.extensionsManager];

    CGRect rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:annot.pageIndex];
    CGRect showRect = [self.pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:annot.pageIndex];
    NSArray *array = [NSArray arrayWithObject:[self.pdfViewCtrl getDisplayView]];
    [self.extensionsManager.propertyBar showPropertyBar:showRect inView:self.pdfViewCtrl viewsCanMove:array];
    self.isShowStyle = YES;
}

- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot {
    BOOL canMove = [self canMoveAnnot:annot];
    BOOL canResize = [self canResizeAnnot:annot];
    if (!canMove && !canResize) {
        if (self.extensionsManager.currentAnnot) {
            [self showMenu];
        }
        return YES;
    }
    if (![Utility isAnnot:annot uniqueToAnnot:self.extensionsManager.currentAnnot]) {
        return NO;
    }
    if (!annot.canModify) {
        return YES;
    }

    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.shouldShowMenu = self.extensionsManager.shouldShowMenu;
        self.shouldShowPropertyBar = self.extensionsManager.shouldShowPropertyBar;
        [self hideMenu];
        [self.extensionsManager hidePropertyBar];

        self.editType = e_editTypeWholeAnnot;
        for (int i = 0; i < [self.vertexes getSize]; i++) {
            CGPoint vertex = [self.pdfViewCtrl convertPdfPtToPageViewPt:[self.vertexes getAt:i] pageIndex:pageIndex];
            if ([self isFingerAtPos:point closeToVertex:vertex]) {
                self.editType = e_editTypeSingleVertex;
                self.movingVertexIndex = i;
                break;
            }
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        BOOL shouldMove = YES;
        // update self.vertexes
        CGPoint translation = [recognizer translationInView:pageView];
        if (self.editType == e_editTypeWholeAnnot) {
            FSPointFArray *newVertexes = [[FSPointFArray alloc] init];
            for (int i = 0; i < [self.vertexes getSize]; i++) {
                FSPointF *vertex = [self.vertexes getAt:i];
                CGPoint pvVertex = [self.pdfViewCtrl convertPdfPtToPageViewPt:vertex pageIndex:pageIndex];
                pvVertex.x += translation.x;
                pvVertex.y += translation.y;
                if ([self isVertexValid:pvVertex pageIndex:pageIndex]) {
                    FSPointF *translatedVertex = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvVertex pageIndex:pageIndex];
                    vertex.x = translatedVertex.x;
                    vertex.y = translatedVertex.y;
                    [newVertexes add:vertex];
                } else {
                    shouldMove = NO;
                    break;
                }
            }
            if (shouldMove) {
                self.vertexes = newVertexes;
            }
        } else {
            FSPointF *movingVertex = [self.vertexes getAt:self.movingVertexIndex];
            CGPoint pvVertex = [self.pdfViewCtrl convertPdfPtToPageViewPt:movingVertex pageIndex:pageIndex];
            pvVertex.x += translation.x;
            pvVertex.y += translation.y;
            if ([self isVertexValid:pvVertex vertexIndex:self.movingVertexIndex pageIndex:pageIndex]) {
                FSPointF *translatedVertex = [self.pdfViewCtrl convertPageViewPtToPdfPt:pvVertex pageIndex:pageIndex];
                movingVertex.x = translatedVertex.x;
                movingVertex.y = translatedVertex.y;
                [self.vertexes setAt:self.movingVertexIndex newElement:movingVertex];
            } else {
                shouldMove = NO;
            }
        }
        if (!shouldMove) {
            return YES;
        }
        [recognizer setTranslation:CGPointZero inView:[self.pdfViewCtrl getPageView:pageIndex]];
        CGRect oldRect = [Utility getAnnotRect:self.annot pdfViewCtrl:self.pdfViewCtrl];
        [[[FSPolyLine alloc] initWithAnnot:self.annot] setVertexes:self.vertexes];
        [self.pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [self.pdfViewCtrl unlockRefresh];
        CGRect newRect = [Utility getAnnotRect:self.annot pdfViewCtrl:self.pdfViewCtrl];
        if (self.editType == e_editTypeSingleVertex) {
            [self updateAnnotImage];
        }
        newRect = CGRectUnion(newRect, oldRect);
        newRect = CGRectInset(newRect, -30, -30);
        [self.pdfViewCtrl refresh:newRect pageIndex:pageIndex needRender:NO];
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        self.editType = e_editTypeUnknown;
        if (annot.canModify) {
            annot.modifiedDate = [NSDate date];
            [self modifyAnnot:annot addUndo:NO];
        }

        if (self.shouldShowMenu) {
            [self showMenu];
        } else if (self.shouldShowPropertyBar) {
            [self.extensionsManager showPropertyBar];
        }
    }
    return YES;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {
    if (self.extensionsManager.currentAnnot == annot && pageIndex == annot.pageIndex && annot.type == FSAnnotPolyLine) {
        [self drawAnnotInContext:context];
        
        CGRect rect = [Utility getAnnotRect:annot pdfViewCtrl:self.pdfViewCtrl];
        CGRect _rect = [self.pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:pageIndex];
        rect.origin.x = _rect.origin.x;
        rect.origin.y = _rect.origin.y;
        rect = CGRectInset(rect, -10, -10);
        
        CGContextSetLineWidth(context, 2.0);
        CGFloat dashArray[] = {3, 3, 3, 3};
        CGContextSetLineDash(context, 3, dashArray, 4);
        CGContextSetStrokeColorWithColor(context, [[UIColor colorWithRGB:annot.color] CGColor]);
        CGContextStrokeRect(context, rect);

        UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        CGSize dotSize = dragDot.size;
        for (int i = 0; i < [self.vertexes getSize]; i++) {
            FSPointF *vertex = [self.vertexes getAt:i];
            CGPoint point = [self.pdfViewCtrl convertPdfPtToPageViewPt:vertex pageIndex:pageIndex];
            point.x -= dotSize.width / 2;
            point.y -= dotSize.height / 2;
            [dragDot drawAtPoint:point];
        }
    }
}

#pragma private

- (BOOL)isFingerAtPos:(CGPoint)pos closeToVertex:(CGPoint)vertex {
    CGFloat dx = pos.x - vertex.x;
    CGFloat dy = pos.y - vertex.y;
    return dx * dx + dy * dy < self.maxFingerBias * self.maxFingerBias;
}

- (BOOL)isVertexValid:(CGPoint)vertex pageIndex:(int)pageIndex {
    UIView *pageView = [self.pdfViewCtrl getPageView:pageIndex];
    CGRect validRect = CGRectInset(pageView.bounds, 5, 5);
    if (!CGRectContainsPoint(validRect, vertex)) {
        return NO;
    }
    return YES;
}

- (BOOL)isVertexValid:(CGPoint)vertex vertexIndex:(int)vertexIndex pageIndex:(int)pageIndex {
    if (![self isVertexValid:vertex pageIndex:pageIndex]) {
        return NO;
    }
    if (vertexIndex > 0) {
        CGPoint prevPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[self.vertexes getAt:vertexIndex - 1] pageIndex:pageIndex];
        CGFloat dx = prevPoint.x - vertex.x;
        CGFloat dy = prevPoint.y - vertex.y;
        if (dx * dx + dy * dy < self.minVertexDistance * self.minVertexDistance) {
            return NO;
        }
    }
    if (vertexIndex < [self.vertexes getSize] - 1) {
        CGPoint nextPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:[self.vertexes getAt:vertexIndex + 1] pageIndex:pageIndex];
        CGFloat dx = nextPoint.x - vertex.x;
        CGFloat dy = nextPoint.y - vertex.y;
        if (dx * dx + dy * dy < self.minVertexDistance * self.minVertexDistance) {
            return NO;
        }
    }
    return YES;
}

@end
