/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ReflowModule.h"
#import "PanelController+private.h"
#import "SettingBar+private.h"
#import "DigitalSignaturePanel.h"
#import "AnnotationPanel.h"

@interface ReflowModule ()

@property (nonatomic, strong) TbBaseBar *topToolbar;
@property (nonatomic, strong) TbBaseItem *backItem;
@property (nonatomic, strong) TbBaseItem *titleItem;

@property (strong, nonatomic) FSToolbar *toolBarTopReflow;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBarBottomReflow;
@property (strong, nonatomic) IBOutlet UIButton *bookmark;
@property (strong, nonatomic) IBOutlet UIButton *bigger;
@property (strong, nonatomic) IBOutlet UIButton *smaller;
@property (strong, nonatomic) IBOutlet UIButton *showGraph;
@property (strong, nonatomic) IBOutlet UIButton *previousPage;
@property (strong, nonatomic) IBOutlet UIButton *nextPage;

@property (assign, nonatomic) BOOL needShowImageForReflow;
@property (assign, nonatomic) BOOL isReflowBarShowing;
@property (assign, nonatomic) BOOL isReflowByClick;
@property (strong, nonatomic) NSMutableDictionary *oldItemHiddenStatus;
@end

@implementation ReflowModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;

    PDF_LAYOUT_MODE currentPageLayoutMode;
    PDF_LAYOUT_MODE oldPageLayoutMode;
}

- (NSString *)getName {
    return @"Reflow";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _oldItemHiddenStatus = @{}.mutableCopy;
        [self loadModule];
    }
    return self;
}

- (void)loadModule {
    [_pdfViewCtrl registerGestureEventListener:self];
    [_extensionsManager registerStateChangeListener:self];
    [_pdfViewCtrl registerDocEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    self.needShowImageForReflow = YES;
    _isReflowByClick = NO;
//    [self showReflowBar:YES animation:YES];
}

- (void)enterReflowMode:(BOOL)flag {
    if (flag) {
        currentPageLayoutMode = [_pdfViewCtrl getPageLayoutMode];
        oldPageLayoutMode = currentPageLayoutMode;
        if (oldPageLayoutMode == PDF_LAYOUT_MODE_REFLOW) {
            oldPageLayoutMode = PDF_LAYOUT_MODE_SINGLE;
        }
        [_pdfViewCtrl setPageLayoutMode:PDF_LAYOUT_MODE_REFLOW];
        _isReflowByClick = YES;
        if (!self.needShowImageForReflow) {
            [self.showGraph setImage:[UIImage imageNamed:@"reflow_graphD.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [_pdfViewCtrl setReflowMode:PDF_REFLOW_ONLYTEXT];
        } else {
            [self.showGraph setImage:[UIImage imageNamed:@"reflow_graph.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [_pdfViewCtrl setReflowMode:PDF_REFLOW_WITHIMAGE];
        }
        [_pdfViewCtrl setZoom:_pdfViewCtrl.minZoomLimit];
    } else {
        [self showReflowBar:NO animation:YES];
        [_extensionsManager changeState:STATE_NORMAL];
        ((UIButton *) [_extensionsManager.settingBar getItemView:REFLOW]).selected = NO;
        currentPageLayoutMode = oldPageLayoutMode;
        if (currentPageLayoutMode == PDF_LAYOUT_MODE_UNKNOWN) {
            currentPageLayoutMode = PDF_LAYOUT_MODE_SINGLE;
        }
        if (!_isReflowByClick) {
            currentPageLayoutMode = PDF_LAYOUT_MODE_SINGLE;
        }
        [_pdfViewCtrl setPageLayoutMode:currentPageLayoutMode];
        [_extensionsManager.settingBar updateLayoutButtonsWithLayout:currentPageLayoutMode];
    }
    self.bigger.enabled = ([_pdfViewCtrl getScale] < 20.0f);
    self.smaller.enabled = ([_pdfViewCtrl getScale] > 1.0f);
    [self setPreviousAndNextBtnEnable];
    [_pdfViewCtrl layoutIfNeeded];
}

- (FSToolbar *)toolBarTopReflow {
    if (!_toolBarTopReflow) {
        
        _toolBarTopReflow = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
        
        self.topToolbar = [[TbBaseBar alloc] init];
        [_toolBarTopReflow addSubview:self.topToolbar.contentView];

        self.backItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]]; // assign to var to keep a strong reference, can't directly assign to weak property of self
        [self.topToolbar.contentView addSubview:self.backItem.contentView];
        CGSize size = self.backItem.contentView.frame.size;
        [self.backItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backItem.contentView.superview.mas_left).offset(10);
            make.centerY.mas_equalTo(self.backItem.contentView.superview.mas_centerY);
            make.width.mas_equalTo(size.width);
            make.height.mas_equalTo(size.height);
        }];
        __weak typeof(self) weakSelf = self;
        self.backItem.onTapClick = ^(TbBaseItem *item) {
            weakSelf.needShowImageForReflow = YES;
            [weakSelf cancelButtonClicked];
        };

        self.titleItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kReflow")]; // assign to var to keep a strong reference, can't directly assign to weak property of self
        self.titleItem.textColor = BlackThemeTextColor;
        self.titleItem.enable = NO;
        [self.topToolbar.contentView addSubview:self.titleItem.contentView];
        size = self.titleItem.contentView.frame.size;
        [self.titleItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.titleItem.contentView.superview);
            make.width.mas_equalTo(size.width);
            make.height.mas_equalTo(size.height);
        }];
        
        [_pdfViewCtrl insertSubview:self.toolBarTopReflow aboveSubview:[_pdfViewCtrl getDisplayView]];
        
        [self.topToolbar.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.mas_equalTo(self.topToolbar.contentView.superview);
        }];
        
        [_toolBarTopReflow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self->_pdfViewCtrl);
        }];
        _toolBarTopReflow.hidden = YES;
    }
    return _toolBarTopReflow;
}

- (UIToolbar *)toolBarBottomReflow {
    if (!_toolBarBottomReflow) {
        NSArray *nib1 = [[NSBundle bundleForClass:[self class]] loadNibNamed:@"ToolbarBottomReflow" owner:self options:nil];
        UIView *tmpCustomView1 = [nib1 objectAtIndex:0];
        self.toolBarBottomReflow = (UIToolbar *) tmpCustomView1;
        [self.bookmark setImage:[UIImage imageNamed:@"reflow_bookmark.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        if (_extensionsManager.panelController.panel.specs.count == 0) {
            self.bookmark.enabled = NO;
        }
        [self.bigger setImage:[UIImage imageNamed:@"reflow_bigger.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.bigger setImage:[UIImage imageNamed:@"reflow_biggerD.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateDisabled];
        [self.smaller setImage:[UIImage imageNamed:@"reflow_smaller.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.smaller setImage:[UIImage imageNamed:@"reflow_smallerD.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateDisabled];
        [self.showGraph setImage:[UIImage imageNamed:@"reflow_graph.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.previousPage setImage:[UIImage imageNamed:@"reflow_pre.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.previousPage setImage:[UIImage imageNamed:@"reflow_preD.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateDisabled];
        [self.nextPage setImage:[UIImage imageNamed:@"reflow_next.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [self.nextPage setImage:[UIImage imageNamed:@"reflow_nextD.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateDisabled];

        self.needShowImageForReflow = YES;
        
        _toolBarBottomReflow.barTintColor = NormalBarBackgroundColor;

        [_pdfViewCtrl insertSubview:_toolBarBottomReflow aboveSubview:[_pdfViewCtrl getDisplayView]];
        [_toolBarBottomReflow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self->_toolBarBottomReflow.superview.mas_left);
            make.right.mas_equalTo(self->_toolBarBottomReflow.superview.mas_right);
            if (@available(iOS 11.0, *)) {
                make.bottom.mas_equalTo(self->_toolBarBottomReflow.superview.mas_safeAreaLayoutGuideBottom);
            } else {
                make.bottom.mas_equalTo(self->_toolBarBottomReflow.superview.mas_bottom);
            }
            make.height.mas_equalTo(44);
        }];
        _toolBarBottomReflow.hidden = YES;
    }
    return _toolBarBottomReflow;
}

- (void)showReflowBar:(BOOL)isShow animation:(BOOL)animation {
    if (self.toolBarTopReflow.hidden != isShow) {
        return;
    }
    if (!isShow) {
        self.toolBarTopReflow.hidden = YES;
        self.toolBarBottomReflow.hidden  = YES;
    } else {
        self.toolBarTopReflow.hidden = NO;
        self.toolBarBottomReflow.hidden  = NO;
        [self delayHideReflowTopBottomToolBar];
    }
    if (animation) {
        [Utility addAnimation:_toolBarTopReflow.layer type:_toolBarTopReflow.hidden ? kCATransitionReveal : kCATransitionMoveIn subType:_toolBarTopReflow.hidden ? kCATransitionFromTop : kCATransitionFromBottom timeFunction:kCAMediaTimingFunctionEaseInEaseOut duration:0.3];
        [Utility addAnimation:_toolBarBottomReflow.layer type:_toolBarBottomReflow.hidden ? kCATransitionReveal : kCATransitionMoveIn subType:_toolBarBottomReflow.hidden ? kCATransitionFromBottom : kCATransitionFromTop timeFunction:kCAMediaTimingFunctionEaseInEaseOut duration:0.3];
    }
}

-(void)delayHideReflowTopBottomToolBar{
    if (_extensionsManager.isFullScreen) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoFullScreen) object:nil];
        [self performSelector:@selector(autoFullScreen) withObject:nil afterDelay:5.f];
    }
}

- (void)autoFullScreen{
    [self showReflowBar:NO animation:YES];
}

#pragma buttonClickEvent-- - Click on the button below
- (IBAction)bookmarkClicked:(id)sender {
    _extensionsManager.hiddenPanel = NO;
}

- (IBAction)biggerClicked:(id)sender {
    [self delayHideReflowTopBottomToolBar];
    float newZoom = [_pdfViewCtrl getScale] * 1.5f;
    newZoom = MIN(newZoom, 19.0f);
    [_pdfViewCtrl setZoom:newZoom];
    self.bigger.enabled = (newZoom < 19.0f);
    self.smaller.enabled = YES;
}

- (IBAction)smallerClicked:(id)sender {
    [self delayHideReflowTopBottomToolBar];
    [_pdfViewCtrl setZoom:[_pdfViewCtrl getScale] * 0.667f];
    self.smaller.enabled = ([_pdfViewCtrl getScale] > 2.0f);
    self.bigger.enabled = YES;
}

- (IBAction)showGraphClicked:(id)sender {
    [self delayHideReflowTopBottomToolBar];
    if (self.needShowImageForReflow) {
        [self.showGraph setImage:[UIImage imageNamed:@"reflow_graphD.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [_pdfViewCtrl setReflowMode:PDF_REFLOW_ONLYTEXT];
    } else {
        [self.showGraph setImage:[UIImage imageNamed:@"reflow_graph.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [_pdfViewCtrl setReflowMode:PDF_REFLOW_WITHIMAGE];
    }
    self.needShowImageForReflow = !self.needShowImageForReflow;
}

- (IBAction)previousPageClicked:(id)sender {
    [self delayHideReflowTopBottomToolBar];
    [_pdfViewCtrl gotoPrevPage:NO];
    [self setPreviousAndNextBtnEnable];
}

- (IBAction)nextPageClicked:(id)sender {
    [self delayHideReflowTopBottomToolBar];
    [_pdfViewCtrl gotoNextPage:NO];
    [self setPreviousAndNextBtnEnable];
}

- (void)cancelButtonClicked {
    [self enterReflowMode:NO];
}

- (void)setPreviousAndNextBtnEnable {
    if ([_pdfViewCtrl getCurrentPage] <= 0) {
        self.previousPage.enabled = NO;
    } else {
        self.previousPage.enabled = YES;
    }

    if ([_pdfViewCtrl getCurrentPage] >= [_pdfViewCtrl getPageCount] - 1) {
        self.nextPage.enabled = NO;
    } else {
        self.nextPage.enabled = YES;
    }
}

#pragma mark IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error{
    if ([_pdfViewCtrl isDynamicXFA] && _isReflowByClick) {
        [self enterReflowMode:NO];
    }
}

- (void)onDocWillClose:(FSPDFDoc *)document{
     _isReflowByClick = NO;
}

#pragma IGestureEventListener

- (BOOL)onTap:(UITapGestureRecognizer *)recognizer {
    if (!_extensionsManager.isFullScreen) {
        return NO;
    }
    if ([_extensionsManager getState] == STATE_REFLOW) {
        if (self.toolBarTopReflow.hidden) {
            [self showReflowBar:YES animation:YES];
            [_extensionsManager setFullScreen:NO];
            //            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
        } else {
            [self showReflowBar:NO animation:YES];
             [_extensionsManager setFullScreen:YES];
            //            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        }
        return YES;
    }
    return NO;
}

- (BOOL)onLongPress:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

#pragma IStateChangeListener

- (void)onStateChanged:(int)state {
    if (state == STATE_REFLOW) {
        [self showReflowBar:YES animation:YES];
        self.bigger.enabled = ([_pdfViewCtrl getScale] < 5.0f);
        self.smaller.enabled = ([_pdfViewCtrl getScale] > 1.0f);
        [self setPreviousAndNextBtnEnable];
        _oldItemHiddenStatus = [_extensionsManager.panelController getItemHiddenStatus].mutableCopy;
        if (![_extensionsManager.panelController isPanelHidden:FSPanelTypeDigitalSignature]) {
            [_extensionsManager.panelController setPanelHidden:YES type:FSPanelTypeDigitalSignature];
        }
        if (![_extensionsManager.panelController isPanelHidden:FSPanelTypeAnnotation]) {
            [_extensionsManager.panelController setPanelHidden:YES type:FSPanelTypeAnnotation];
        }
    }else{
        if ([_oldItemHiddenStatus allKeys].count) {
            if (![_oldItemHiddenStatus[@"DigitalSignature"] boolValue]) {
                if ([_extensionsManager.panelController isPanelHidden:FSPanelTypeDigitalSignature]) {
                    [_extensionsManager.panelController setPanelHidden:NO type:FSPanelTypeDigitalSignature];
                    DigitalSignaturePanel* digtalPanel =(DigitalSignaturePanel*)[_extensionsManager.panelController getPanel:FSPanelTypeDigitalSignature];
                    [digtalPanel onDocOpened:_pdfViewCtrl.currentDoc error:0];
                }
            }
            if (![_oldItemHiddenStatus[@"Annotation"] boolValue]) {
                if ([_extensionsManager.panelController isPanelHidden:FSPanelTypeAnnotation]) {
                    [_extensionsManager.panelController setPanelHidden:NO type:FSPanelTypeAnnotation];
                    AnnotationPanel* annotPanel =(AnnotationPanel*)[_extensionsManager.panelController getPanel:FSPanelTypeAnnotation];
                    [annotPanel onDocOpened:_pdfViewCtrl.currentDoc error:0];
                }
            }
        }
    }
}

#pragma IPageEventListener

- (void)onPageChanged:(int)oldIndex currentIndex:(int)currentIndex {
    [self setPreviousAndNextBtnEnable];
    if ([_extensionsManager getState] == STATE_REFLOW && _extensionsManager.isFullScreen) {
        [self delayHideReflowTopBottomToolBar];
    }
}

@end
