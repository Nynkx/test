/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MultipleSelectionModule.h"
#import "MultipleSelectionToolHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>
#import "UIExtensionsConfig.h"

@interface MultipleSelectionModule ()
@end

@implementation MultipleSelectionModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
    FSAnnotType _annotType;
}

- (NSString *)getName {
    return @"MultipleSelection";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        [self loadModule];

        MultipleSelectionToolHandler* toolHandler = [[MultipleSelectionToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
    }
    return self;
}
- (void)loadModule {

    [_extensionsManager registerAnnotPropertyListener:self];
    _extensionsManager.annotationToolsBar.multipleSelectClicked = ^{
        [self annotItemClicked];
    };
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    id<IToolHandler> toolHandler = [_extensionsManager getToolHandlerByName:Tool_Multiple_Selection];
    toolHandler.type = _annotType;
    [_extensionsManager setCurrentToolHandler:toolHandler];

    [_extensionsManager.toolSetBar removeAllItems];
    
    [Utility showTips:FSLocalizedForKey(@"kMultipleSelect") pdfViewCtrl:_pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_continue")];
    } else {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_single")];
    }
    continueItem.tag = 3;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_extensionsManager.pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            item.imageNormal = ImageNamed(@"annot_continue");
            item.imageSelected = ImageNamed(@"annot_continue");
        } else {
            item.imageNormal = ImageNamed(@"annot_single");
            item.imageSelected = ImageNamed(@"annot_single");
        }

        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_extensionsManager.pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };
    
    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(doneItem.contentView.superview);
        make.centerX.mas_equalTo(doneItem.contentView.superview.mas_centerX).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
    
    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(continueItem.contentView.superview);
        make.centerX.equalTo(continueItem.contentView.superview.mas_centerX).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_extensionsManager.pdfViewCtrl];
}

@end
