/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "MultipleSelectionToolHandler.h"
#import "PaintUtility.h"
#import "MenuItem.h"
#import "ShapeUtil.h"
#import "AudioPlayerControl.h"
#import "FSAnnot+Extensions.h"
#import "FSAnnotHandler.h"
#import "ReplyTableViewController.h"
#import "ReplyUtil.h"

@interface MultipleSelectionToolHandler ()<IFSUndoEventListener>

@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic, strong) FSAnnot *annot;

@property (nonatomic, assign) BOOL isEndDrawing;
@property (nonatomic, assign) int currentPageIndex;

@property (nonatomic, strong) PaintUtility *paintUtility;

@property (nonatomic, assign) CGRect multipleSelectionRect;
@property (nonatomic, assign) BOOL isNeedDrawBlueRect;

@property (nonatomic, strong) NSMutableArray *canMoveAndZoomAnnots;
@property (nonatomic, strong) NSMutableArray *canMoveNotZoomAnnots;
@property (nonatomic, strong) NSMutableArray *canNotMoveAndZoomAnnots;
@property (nonatomic, strong) NSMutableArray *canGroupAnnots;
@property (nonatomic, strong) NSMutableArray *selectedAnnots;
@property (nonatomic, strong) NSMutableArray *selectedGroupAnnots;
@property (nonatomic, assign) FSAnnotEditType editType;
@property (nonatomic, assign) BOOL isHiddeDeleteButton;

@property (nonatomic, assign) int annotPageIndex;

@property (nonatomic, strong) NSMutableArray *oldAnnotsAttributes;

@property (nonatomic, assign) BOOL canFlattenGroup;
@property (nonatomic, assign) BOOL canGroup;
@property (nonatomic, assign) BOOL canUngroup;
@property (nonatomic, assign) BOOL canReplyGroup;
@property (nonatomic, assign) BOOL isClickSelect;
@end

@implementation MultipleSelectionToolHandler {
    UIExtensionsManager * __weak _extensionsManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotCircle;
        _isNeedDrawBlueRect = NO;
        _canMoveAndZoomAnnots = @[].mutableCopy;
        _canMoveNotZoomAnnots = @[].mutableCopy;
        _canNotMoveAndZoomAnnots = @[].mutableCopy;
        _canGroupAnnots = @[].mutableCopy;
        _selectedAnnots = @[].mutableCopy;
        _selectedGroupAnnots = @[].mutableCopy;
        _isHiddeDeleteButton = NO;
        _oldAnnotsAttributes = @[].mutableCopy;
        _canFlattenGroup = YES;
        _canGroup = YES;
        _canUngroup = NO;
        _isClickSelect = NO;
        _canReplyGroup = NO;
        
        [_extensionsManager registerUndoEventListener:self];
    }
    return self;
}

- (NSString *)getName {
    return Tool_Multiple_Selection;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
    _isEndDrawing = NO;
    self.multipleSelectionRect = CGRectZero;
    _isNeedDrawBlueRect = NO;
    
    _canMoveAndZoomAnnots = @[].mutableCopy;
    _canMoveNotZoomAnnots = @[].mutableCopy;
    _canNotMoveAndZoomAnnots = @[].mutableCopy;
    _selectedAnnots = @[].mutableCopy;
    _selectedGroupAnnots = @[].mutableCopy;
    _isHiddeDeleteButton = NO;
    _oldAnnotsAttributes = @[].mutableCopy;
}

- (void)onDeactivate {
    
    _isClickSelect = NO;
    _currentAnoot = nil;
    
    self.startPoint = nil;
    self.endPoint = nil;
    self.multipleSelectionRect = CGRectZero;
    _isEndDrawing = NO;
    _isNeedDrawBlueRect = NO;
    
    [_pdfViewCtrl refresh:_currentPageIndex];
    _canMoveAndZoomAnnots = nil;
    _canMoveNotZoomAnnots = nil;
    _canNotMoveAndZoomAnnots = nil;
    _selectedAnnots = nil;
    _selectedGroupAnnots = nil;
    _isHiddeDeleteButton = NO;
    _oldAnnotsAttributes = nil;
    
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)selectedGroupAnnot:(FSAnnot *)annot{
    if (annot && ![annot isEmpty] && annot.isMarkup) {
        _currentAnoot = annot;
        _isClickSelect = YES;
        FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
        CGRect rect = CGRectZero;
        FSMarkupArray *markupArray = [markup getGroupElements];
        _selectedGroupAnnots = @[].mutableCopy;
        for (int i = 0; i < markupArray.getSize; i ++) {
            FSMarkup *markupi = [markupArray getAt:i];
            [_selectedGroupAnnots addObject:markupi];
            CGRect recti = [self getAnnotRect:markupi];
            if (i == 0) {
                rect = recti;
            }else{
                rect = CGRectUnion(rect,recti);
            }
        }
        self.currentPageIndex = annot.pageIndex;
        [self multipleSelected:rect pageIndex:annot.pageIndex];
        _annotPageIndex = self.currentPageIndex;
        return YES;
    }
    return NO;
}

- (void)selectedGroupAnnotForDelete:(FSAnnot *)annot{
    _extensionsManager.shouldShowMenu = NO;
    [self selectedGroupAnnot:annot];
    [self deleteAnnots];
    _isClickSelect = NO;
}

- (void)selectedGroupAnnotForFlatten:(FSAnnot *)annot{
    _extensionsManager.shouldShowMenu = NO;
    [self selectedGroupAnnot:annot];
    [self flattenAnnots];
    _isClickSelect = NO;
}

#define DEFAULT_RECT_WIDTH 200

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    if (_isClickSelect) {
        _currentAnoot = nil;
        if (_extensionsManager.currentToolHandler == self) {
            [_extensionsManager setCurrentToolHandler:nil];
        }
        _isClickSelect = NO;
        _selectedGroupAnnots = nil;
    }
    
    _extensionsManager.currentAnnot = nil;
    self.currentPageIndex = pageIndex;
    _isEndDrawing = NO;
    
    self.startPoint = nil;
    self.endPoint = nil;
    _editType = FSAnnotEditTypeUnknown;
    [_selectedAnnots removeAllObjects];
    _selectedAnnots = nil;
    self.multipleSelectionRect = CGRectZero;
    
    [_pdfViewCtrl refresh:pageIndex];
    
    return YES;
}

- (BOOL)isHitRect:(CGRect)rect point:(CGPoint)point {
    CGRect pvRect = rect;
    pvRect = UIEdgeInsetsInsetRect(pvRect, UIEdgeInsetsMake(-40, -30, -40, -30));
    CGPoint pvPoint = point;
    if (CGRectContainsPoint(pvRect, pvPoint)) {
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    
    [_extensionsManager.menuControl hideMenu];
    _extensionsManager.shouldShowMenu = YES;
    
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect rect = [pageView frame];
    CGSize size = rect.size;
    if (point.x > size.width || point.y > size.height || point.x < 0 || point.y < 0)
        return NO;
    self.currentPageIndex = pageIndex;

    CGPoint translation = [recognizer translationInView:pageView];
    CGRect newMultipleSelectionRect = self.multipleSelectionRect;
    newMultipleSelectionRect.origin.x += translation.x;
    newMultipleSelectionRect.origin.y += translation.y;
    
    if (_selectedAnnots.count > 1 && [self isHitRect:newMultipleSelectionRect point:point] && _annotPageIndex == pageIndex) {
        
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            
            _oldAnnotsAttributes = [self getAnnotsAttributes:_selectedAnnots];
            
            BOOL keepAspectRatio = NO;//[self keepAspectRatioForResizingAnnot:annot];
            _editType = [ShapeUtil getAnnotEditTypeWithPoint:point rect:CGRectInset(self.multipleSelectionRect, -10, -10) keepAspectRatio:keepAspectRatio defaultEditType:FSAnnotEditTypeFull];
            
            
        } else if (recognizer.state == UIGestureRecognizerStateChanged) {
            BOOL __block shouldMove = YES;
            NSArray *movePointArray = [ShapeUtil getCornerPointOfRect:newMultipleSelectionRect];
            [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                CGPoint point = [obj CGPointValue];
                
                if (![Utility isVertexValid:point pageView:pageView]) {
                    shouldMove = NO;
                }
            }];
            if (!shouldMove) {
                self.startPoint = nil;
                self.endPoint = nil;
                
                return YES;
            }
            
            // move and zoom
            if (_canNotMoveAndZoomAnnots.count > 0) {
                if (_extensionsManager.shouldShowMenu) {
                    [self showMenu:self.multipleSelectionRect];
                }
                return YES;
            }
            
            if (_editType == FSAnnotEditTypeFull) {
                if (newMultipleSelectionRect.origin.x < 15 || newMultipleSelectionRect.origin.x + newMultipleSelectionRect.size.width > pageView.frame.size.width - 15) {
                    if (_extensionsManager.shouldShowMenu) {
                        [self showMenu:self.multipleSelectionRect];
                    }
                    return YES;
                }
                
                if (newMultipleSelectionRect.origin.y < 15 || newMultipleSelectionRect.origin.y + newMultipleSelectionRect.size.height > pageView.frame.size.height - 15) {
                    if (_extensionsManager.shouldShowMenu) {
                        [self showMenu:self.multipleSelectionRect];
                    }
                    return YES;
                }
                
                [self moveAnnots:_selectedAnnots recognizer:recognizer pageIndex:pageIndex];
            }else{
                if (_canMoveNotZoomAnnots.count > 0) {
                    return YES;
                }
                [self zoomAnnots:_selectedAnnots recognizer:recognizer pageIndex:pageIndex editType:_editType];
            }
           
            CGRect allAnnotsRect = [self calculateAllAnnotsRect:_selectedAnnots];
            self.multipleSelectionRect = allAnnotsRect;
            [_pdfViewCtrl refresh:pageIndex];
        } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
            if (_oldAnnotsAttributes != nil && _oldAnnotsAttributes.count > 0 && (_canMoveAndZoomAnnots.count > 0 || _canMoveNotZoomAnnots.count >0 ) && _canNotMoveAndZoomAnnots.count == 0) {
                NSMutableArray *moveAndZoomAnnotsUndoItems = @[].mutableCopy;
                NSMutableArray *newAnnotsAttributes = [self getAnnotsAttributes:_selectedAnnots];
                
                for (int i = 0 ; i < _selectedAnnots.count; i++) {
                    FSAnnot *annot = [_selectedAnnots objectAtIndex:i];
                    
                    FSAnnotAttributes *oldAttributes = [_oldAnnotsAttributes objectAtIndex:i];
                    FSAnnotAttributes *newAttributes = [newAnnotsAttributes objectAtIndex:i];
                    
                    id<IAnnotHandler> annotHandler = nil;
                    annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
                    
                    FSPDFPage *page = [annot getPage];
                    if (!page || [page isEmpty]) {
                        continue;
                    }
                    
                    UndoItem *undoitem = [UndoItem itemForUndoModifyAnnotWithOldAttributes:oldAttributes newAttributes:newAttributes pdfViewCtrl:_pdfViewCtrl page:page annotHandler:annotHandler];
                    [moveAndZoomAnnotsUndoItems addObject:undoitem];
                }
                
                [_extensionsManager addUndoItems:moveAndZoomAnnotsUndoItems];
            }
            
            _editType = FSAnnotEditTypeUnknown;
            
            if (_extensionsManager.shouldShowMenu) {
                [self showMenu:newMultipleSelectionRect];
            }
            
            [_pdfViewCtrl refresh:pageIndex];
        }
        
        
        [recognizer setTranslation:CGPointZero inView:pageView];
        
        return YES;
    }
    
    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSRectF *dibRect = nil;
    
    self.multipleSelectionRect = CGRectZero;
    _isNeedDrawBlueRect = NO;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _isEndDrawing = NO;
        
        dibRect = [[FSRectF alloc] init];
        [dibRect setLeft:dibPoint.x];
        [dibRect setBottom:dibPoint.y];
        [dibRect setRight:dibPoint.x + 0.1];
        [dibRect setTop:dibPoint.y + 0.1];
        
        self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.endPoint   = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        
        self.paintUtility = [[PaintUtility alloc] init];
        self.paintUtility.annotLineWidth = 1;
        self.paintUtility.annotColor = 0x2da5da;
        self.paintUtility.annotOpacity = 0.3;
        
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        
        [_pdfViewCtrl refresh:pageIndex];

        FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
        FSRectF *realRect = [Utility getPageRealRect:page];
        
        CGFloat tolerance = 5;
        CGRect pointRects[] = {CGRectMake(point.x, point.y, tolerance, tolerance), CGRectMake(point.x, point.y-tolerance, tolerance, tolerance), CGRectMake(point.x-5, point.y-5, tolerance, tolerance), CGRectMake(point.x-5, point.y, tolerance, tolerance)};
        for (NSUInteger index = 0; index < sizeof(pointRects) / sizeof(CGRect); ++index) {
            CGRect pointRect = pointRects[index];
            FSRectF *rect = [_pdfViewCtrl convertPageViewRectToPdfRect:pointRect pageIndex:pageIndex];
            if (rect.right > realRect.right ||
                rect.top > realRect.top ||
                rect.left < realRect.left ||
                rect.bottom < realRect.bottom) {
                [recognizer setValue:@(UIGestureRecognizerStateEnded) forKey:@"state"];
                break;
            }
        }

    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {

        
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        
        CGPoint startPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint pageIndex:pageIndex];
        CGPoint endPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.endPoint pageIndex:pageIndex];
        CGRect rect = [self getRectFromStartPoint:startPoint endPoint:endPoint];

        [self multipleSelected:rect pageIndex:pageIndex];
        _annotPageIndex = pageIndex;
        
        self.startPoint = nil;
        self.endPoint = nil;
        [_pdfViewCtrl refresh:pageIndex];
        
        dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        
//        _isEndDrawing = YES;
    }
    return YES;
}

-(NSMutableArray *)getAnnotsAttributes:(NSMutableArray *)annotArr {
    NSMutableArray *annotAttributesItems = @[].mutableCopy;
    for (int i = 0 ; i < annotArr.count; i++) {
        FSAnnot *annot = [annotArr objectAtIndex:i];
        
        FSAnnotAttributes *oldAttributes = [FSAnnotAttributes attributesWithAnnot:annot];
        [annotAttributesItems addObject:oldAttributes];
    }
    return annotAttributesItems;
}

-(void)zoomAnnots:(NSMutableArray *)zoomAnnots recognizer:(UIPanGestureRecognizer *)recognizer pageIndex:(int)pageIndex editType:(FSAnnotEditType)editType{
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint translation = [recognizer translationInView:pageView];
    
    for (int i = 0 ; i < zoomAnnots.count; i++) {
        FSAnnot *annot = [zoomAnnots objectAtIndex:i];

        FSPDFPage *page = [annot getPage];
        if (!page || [page isEmpty]) {
            continue;
        }
        
        [_pdfViewCtrl lockRefresh];
        annot.modifiedDate = [NSDate date];
        CGRect newAnnotRect = [self newAnnotRectOfZoomForPanTranslation:translation annot:annot editType:editType];
        annot.fsrect = [_pdfViewCtrl convertPageViewRectToPdfRect:newAnnotRect pageIndex:annot.pageIndex];
        [_pdfViewCtrl unlockRefresh];
        
        [_extensionsManager onAnnotModified:page annot:annot];
    }
}

- (BOOL)keepAspectRatioForResizingAnnot:(FSAnnot *)annot {
    return NO;
}

- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot {
    return CGSizeMake(10, 10);
}

- (CGSize)maxSizeForResizingAnnot:(FSAnnot *)annot {
    UIView *pageView = [_pdfViewCtrl getPageView:annot.pageIndex];
    return pageView.bounds.size;
}

- (CGRect)newAnnotRectOfZoomForPanTranslation:(CGPoint)translation annot:(FSAnnot *)annot editType:(FSAnnotEditType)editType {
    int pageIndex = annot.pageIndex;
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect pageInsetRect = UIEdgeInsetsInsetRect(pageView.bounds, UIEdgeInsetsMake(-10, -10, -10, -10));
    CGRect annotRect = [self getAnnotRect:annot];
    CGFloat w = CGRectGetWidth(annotRect);
    CGFloat h = CGRectGetHeight(annotRect);
    CGSize minSize = [self minSizeForResizingAnnot:annot];
    CGSize maxSize = [self maxSizeForResizingAnnot:annot];
    CGFloat minW = minSize.width;
    CGFloat maxW = maxSize.width;
    CGFloat minH = minSize.height;
    CGFloat maxH = maxSize.height;
    CGFloat dx = 0;
    CGFloat dy = 0;
    CGFloat dw = 0;
    CGFloat dh = 0;
    
    BOOL keepAspectRatio = [self keepAspectRatioForResizingAnnot:annot];
    if (!keepAspectRatio) {
        if (editType == FSAnnotEditTypeLeftTop ||
            editType == FSAnnotEditTypeLeftMiddle ||
            editType == FSAnnotEditTypeLeftBottom) { // left
            dw = -translation.x;
            dw = MIN(dw, maxW - w);
            dw = MAX(dw, minW - w);
            dw = MIN(dw, CGRectGetMinX(annotRect) - CGRectGetMinX(pageInsetRect));
            dx = -dw;
        } else if (editType == FSAnnotEditTypeRightTop ||
                   editType == FSAnnotEditTypeRightMiddle ||
                   editType == FSAnnotEditTypeRightBottom) { // right
            dw = translation.x;
            dw = MIN(dw, maxW - w);
            dw = MAX(dw, minW - w);
            dw = MIN(dw, CGRectGetMaxX(pageInsetRect) - CGRectGetMaxX(annotRect));
        }
        if (editType == FSAnnotEditTypeLeftTop ||
            editType == FSAnnotEditTypeMiddleTop ||
            editType == FSAnnotEditTypeRightTop) { // top
            dh = -translation.y;
            dh = MIN(dh, maxH - h);
            dh = MAX(dh, minH - h);
            dh = MIN(dh, CGRectGetMinY(annotRect) - CGRectGetMinY(pageInsetRect));
            dy = -dh;
        } else if (editType == FSAnnotEditTypeLeftBottom ||
                   editType == FSAnnotEditTypeMiddleBottom ||
                   editType == FSAnnotEditTypeRightBottom) { // bottom
            dh = translation.y;
            dh = MIN(dh, maxH - h);
            dh = MAX(dh, minH - h);
            dh = MIN(dh, CGRectGetMaxY(pageInsetRect) - CGRectGetMaxY(annotRect));
        }
    } else { // keep aspect ratio
        if (editType == FSAnnotEditTypeRightBottom) {
            CGFloat tmp = (w * translation.x + h * translation.y) / (w * w + h * h);
            dw = w * tmp;
            dh = h * tmp;
            maxW = MIN(maxW, CGRectGetMaxX(pageInsetRect) - CGRectGetMinX(annotRect));
            maxH = MIN(maxH, CGRectGetMaxY(pageInsetRect) - CGRectGetMinY(annotRect));
            dw = MIN(MAX(dw, minW - w), maxW - w);
            dh = h / w * dw;
            dh = MIN(MAX(dh, minH - h), maxH - h);
            dw = w / h * dh;
        } else if (editType == FSAnnotEditTypeLeftTop) {
            CGFloat tmp = (w * translation.x + h * translation.y) / (w * w + h * h);
            dw = -w * tmp;
            dh = -h * tmp;
            maxW = MIN(maxW, CGRectGetMaxX(annotRect) - CGRectGetMinX(pageInsetRect));
            maxH = MIN(maxH, CGRectGetMaxY(annotRect) - CGRectGetMinY(pageInsetRect));
            dw = MIN(MAX(dw, minW - w), maxW - w);
            dh = h / w * dw;
            dh = MIN(MAX(dh, minH - h), maxH - h);
            dw = w / h * dh;
            dx = -dw;
            dy = -dh;
        } else if (editType == FSAnnotEditTypeRightTop) {
            CGFloat tmp = (w * translation.x - h * translation.y) / (w * w + h * h);
            dw = w * tmp;
            dh = h * tmp;
            maxW = MIN(maxW, CGRectGetMaxX(pageInsetRect) - CGRectGetMinX(annotRect));
            maxH = MIN(maxH, CGRectGetMaxY(annotRect) - CGRectGetMinY(pageInsetRect));
            dw = MIN(MAX(dw, minW - w), maxW - w);
            dh = h / w * dw;
            dh = MIN(MAX(dh, minH - h), maxH - h);
            dw = w / h * dh;
            dy = -dh;
        } else if (editType == FSAnnotEditTypeLeftBottom) {
            CGFloat tmp = (w * translation.x - h * translation.y) / (w * w + h * h);
            dw = -w * tmp;
            dh = -h * tmp;
            maxW = MIN(maxW, CGRectGetMaxX(annotRect) - CGRectGetMinX(pageInsetRect));
            maxH = MIN(maxH, CGRectGetMaxY(pageInsetRect) - CGRectGetMinY(annotRect));
            dw = MIN(MAX(dw, minW - w), maxW - w);
            dh = h / w * dw;
            dh = MIN(MAX(dh, minH - h), maxH - h);
            dw = w / h * dh;
            dx = -dw;
        }
    }
    
    CGRect newRect = annotRect;
    newRect.origin.x += dx;
    newRect.origin.y += dy;
    newRect.size.width += dw;
    newRect.size.height += dh;
    return newRect;
}

-(void)moveAnnots:(NSMutableArray *)moveannots recognizer:(UIPanGestureRecognizer *)recognizer pageIndex:(int)pageIndex{
    
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint translation = [recognizer translationInView:pageView];
    CGPoint point = [recognizer locationInView:pageView];
    
    for (int i = 0 ; i < moveannots.count; i++) {
        FSAnnot *annot = [moveannots objectAtIndex:i];
        FSAnnotType annotType = [annot getType];
        
        FSPDFPage *page = [annot getPage];
        if (!page || [page isEmpty]) {
            continue;
        }

        if (annotType == FSAnnotPolygon || annotType == FSAnnotPolyLine) {
            BOOL shouldMove = YES;
            FSPointFArray *vertexes = nil;
            if (annotType == FSAnnotPolygon ) {
                 vertexes = [[FSPolygon alloc] initWithAnnot:annot].vertexes;
            } else {
                vertexes = [[FSPolyLine alloc] initWithAnnot:annot].vertexes;
            }
            
            FSPointFArray *newVertexes = [[FSPointFArray alloc] init];
            for (int i = 0; i < [vertexes getSize]; i++) {
                FSPointF *vertex = [vertexes getAt:i];
                CGPoint pvVertex = [_pdfViewCtrl convertPdfPtToPageViewPt:vertex pageIndex:pageIndex];
                pvVertex.x += translation.x;
                pvVertex.y += translation.y;
                if ([Utility isVertexValid:pvVertex pageView:pageView]) {
                    FSPointF *translatedVertex = [_pdfViewCtrl convertPageViewPtToPdfPt:pvVertex pageIndex:pageIndex];
                    vertex.x = translatedVertex.x;
                    vertex.y = translatedVertex.y;
                    [newVertexes add:vertex];
                }else {
                    shouldMove = NO;
                    break;
                }
            }
            if (shouldMove) {
                vertexes = newVertexes;
            }
            
            if (annotType == FSAnnotPolygon ) {
                [[[FSPolygon alloc] initWithAnnot:annot] setVertexes:vertexes];
            } else {
                [[[FSPolyLine alloc] initWithAnnot:annot] setVertexes:vertexes];
            }

            [_pdfViewCtrl lockRefresh];
            annot.modifiedDate = [NSDate date];
            [annot resetAppearanceStream];
            [_pdfViewCtrl unlockRefresh];
            
        } else if (annotType == FSAnnotLine) {
            
            FSLine *lineAnnot = [[FSLine alloc] initWithAnnot:annot];
            FSPointF *startPoint = [lineAnnot getStartPoint];
            FSPointF *endPoint = [lineAnnot getEndPoint];

            CGPoint lastPoint = CGPointMake(point.x - translation.x, point.y - translation.y);
            FSPointF *cp = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
            FSPointF *lp = [_pdfViewCtrl convertPageViewPtToPdfPt:lastPoint pageIndex:pageIndex];
            float tw = cp.x - lp.x;
            float th = cp.y - lp.y;
            
            startPoint.x += tw;
            startPoint.y += th;
            endPoint.x += tw;
            endPoint.y += th;
            
            lineAnnot.startPoint = startPoint;
            lineAnnot.endPoint = endPoint;

            [_pdfViewCtrl lockRefresh];
            lineAnnot.modifiedDate = [NSDate date];
            [lineAnnot resetAppearanceStream];
            [_pdfViewCtrl unlockRefresh];
            
        } else {
            
            [_pdfViewCtrl lockRefresh];
            annot.modifiedDate = [NSDate date];
            CGRect newAnnotRect = [self newAnnotRectOfMoveForPanTranslation:translation annot:annot];
            
            if ([annot getType] == FSAnnotNote || [annot getType] == FSAnnotFileAttachment) {
                FSRectF *newRectF = annot.fsrect;
                [newRectF translate:translation.x f:translation.y];
                
                FSRectF *newfsrect = [Utility convertPageViewRectToPdfRect:newRectF pageDisplayMatrix:[_pdfViewCtrl getDisplayMatrix:annot.pageIndex] annot:annot oldPdfRect:[_pdfViewCtrl convertPageViewRectToPdfRect:newAnnotRect pageIndex:annot.pageIndex] rotation:[_pdfViewCtrl getViewRotation]];
                
                annot.fsrect = newfsrect;
            }else{
                annot.fsrect = [_pdfViewCtrl convertPageViewRectToPdfRect:newAnnotRect pageIndex:annot.pageIndex];
            }
            
            
            [_pdfViewCtrl unlockRefresh];

        }
        
        [_extensionsManager onAnnotModified:page annot:annot];
    }

}

- (CGRect)newAnnotRectOfMoveForPanTranslation:(CGPoint)translation annot:(FSAnnot *)annot {
    int pageIndex = annot.pageIndex;
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect pageInsetRect = UIEdgeInsetsInsetRect(pageView.bounds, UIEdgeInsetsMake(10, 10, 10, 10));
    CGRect annotRect = [self getAnnotRect:annot];

    CGFloat dx = 0;
    CGFloat dy = 0;
    CGFloat dw = 0;
    CGFloat dh = 0;
    
    dx = translation.x;
    dy = translation.y;
    dx = MIN(dx, CGRectGetMaxX(pageInsetRect) - CGRectGetMaxX(annotRect));
    dx = MAX(dx, CGRectGetMinX(pageInsetRect) - CGRectGetMinX(annotRect));
    dy = MIN(dy, CGRectGetMaxY(pageInsetRect) - CGRectGetMaxY(annotRect));
    dy = MAX(dy, CGRectGetMinY(pageInsetRect) - CGRectGetMinY(annotRect));
    
    CGRect newRect = annotRect;
    newRect.origin.x += dx;
    newRect.origin.y += dy;
    newRect.size.width += dw;
    newRect.size.height += dh;
    return newRect;
}

- (FSRectF *)normalizePDFRect:(FSRectF *)rectF {
    if (rectF.left > rectF.right) {
        float tmp = rectF.left;
        rectF.left = rectF.right;
        rectF.right = tmp;
    }
    
    if (rectF.top < rectF.bottom) {
        float tmp = rectF.top;
        rectF.top = rectF.bottom;
        rectF.bottom = tmp;
    }
    
    if (rectF.left == rectF.right) rectF.right += 1;
    if (rectF.top == rectF.bottom) rectF.top += 1;
    return rectF;
}

- (CGRect)getAnnotRect:(FSAnnot *)annot {
    FSRectF *annotRect = [self normalizePDFRect:annot.fsrect];
    CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:annotRect pageIndex:annot.pageIndex];
    if (annot.type == FSAnnotNote || annot.type == FSAnnotFileAttachment) {
        rect = [Utility getAnnotRect:annot pdfViewCtrl:_pdfViewCtrl];
    }
    return rect;
}

-(NSArray *)unSupportAnnotTypes {
    return  @[
              @(FSAnnotPrinterMark),
              @(FSAnnotTrapNet),
              @(FSAnnotWatermark),
              @(FSAnnot3D),
              @(FSAnnotPopup),
              @(FSAnnotUnknownType)
              ];
}
-(void)multipleSelected:(CGRect)rect pageIndex:(int)pageIndex {
    
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    NSArray *selectRangeAnnots = [Utility getAnnotationsForPageAtIndex:page withUIConfig:_extensionsManager.config];
    if (_isClickSelect) {
        selectRangeAnnots = [Utility getAnnotationsFromAnnots:_selectedGroupAnnots withUIConfig:_extensionsManager.config];
    }
    
    NSMutableArray *selectedAnnots = @[].mutableCopy;
    NSMutableArray *selectedGroupAnnots = @[].mutableCopy;
    
    NSMutableArray *canMoveAndZoomAnnots = @[].mutableCopy;
    NSMutableArray *canNotMoveAndZoomAnnots = @[].mutableCopy;
    NSMutableArray *canMoveNotZoomAnnots = @[].mutableCopy;

    NSMutableSet *groupHeadersSet = [NSMutableSet set];
    
    _canReplyGroup = YES;
    _canFlattenGroup = YES;
    for (int i = 0; i < selectRangeAnnots.count; i++) {
        FSAnnot *annot = [selectRangeAnnots objectAtIndex:i];
        FSAnnotType annotType = [annot getType];
        CGRect annotRect = [self getAnnotRect:annot];
        if (annotType == FSAnnotCaret) {
            FSAnnot *mkFsAnnot = nil;
            
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            if ([markup isGrouped]) {
                FSMarkupArray *groupAnnots = [markup getGroupElements];
                for (int i = 0; i < [groupAnnots getSize]; i++) {
                    FSAnnot *groupAnnot = [groupAnnots getAt:i];
                    if (groupAnnot.type == FSAnnotStrikeOut) {
                        mkFsAnnot = groupAnnot;
                        break;
                    }
                }
                if (mkFsAnnot && ![mkFsAnnot isEmpty]) {
                    annotRect = [self getAnnotRect:mkFsAnnot];
                }
            } else {
                annotRect = [self getAnnotRect:annot];
            }
        }
        
        NSArray *canGroupAnnotTypes = [self getCanGroupAnnotTypes];
        if(CGRectIntersectsRect(annotRect, rect)){
            if ([[self unSupportAnnotTypes] containsObject:@(annotType)]) {
                continue;
            }
            
            if (!annot.canDelete) {
                _isHiddeDeleteButton = YES;
            }
            
            if (annot.replyTo.length != 0) {
                continue;
            }
            
            if ([canGroupAnnotTypes containsObject:@(annotType)]) {
                FSMarkup *groupHeader = annot.canGetGroupHeader;
                if (groupHeader) {
                    BOOL canAdd = YES;
                    NSMutableArray *tmp = @[].mutableCopy;
                    for (int i = 0; i < groupHeader.getGroupElements.getSize; i ++) {
                        FSMarkup *markupi = [groupHeader.getGroupElements getAt:i];
                        [tmp addObject:markupi];
                        if (markupi.getType == FSAnnotFreeText || markupi.getType == FSAnnotFileAttachment ) _canReplyGroup = NO;
                        if (!markupi.canGroup) {
                            _canReplyGroup = NO;
                            _canFlattenGroup = NO;
                            canAdd = NO;
                            break;
                        }
                    }
                    if (![groupHeadersSet containsObject:groupHeader.NM]) {
                        [groupHeadersSet addObject:groupHeader.NM];
                    }
                    if (canAdd) {
                        [selectedGroupAnnots addObjectsFromArray:tmp];
                    }
                }else{
                    if(annot.canGroup){
                        [groupHeadersSet addObject:annot.NM];
                        [selectedGroupAnnots addObject:annot];
                        if (annotType == FSAnnotFreeText || annotType == FSAnnotFileAttachment ) _canReplyGroup = NO;
                    }else{
                        groupHeadersSet = nil;
                        selectedGroupAnnots = nil;
                    }
                }
            }else{
                groupHeadersSet = nil;
                selectedGroupAnnots = nil;
            }
            
            [selectedAnnots addObject:annot];
        }else{
            if ([canGroupAnnotTypes containsObject:@(annotType)]) {
                FSMarkupArray *markupArray = annot.canGetGroupElements;
                if (markupArray) {
                    for (int i = 0; i < markupArray.getSize; i ++) {
                        FSMarkup *markupi = [markupArray getAt:i];
                        if (!markupi.canGroup) {
                            [groupHeadersSet removeObject:markupi.NM];//can not group
                        }
                        CGRect recti = [self getAnnotRect:markupi];
                        if(CGRectIntersectsRect(recti, rect)){
                            for (FSAnnot *selectAnnot in selectRangeAnnots) {
                                if ([selectAnnot.NM isEqualToString:annot.NM]) {
                                    [selectedAnnots addObject:annot];
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }

        }
        if ([selectedAnnots containsObject:annot]) {
            
            NSMutableArray *canMoveAndZoomAnnotTypes = [self getCanMoveAndZoomAnnotTypes];
            NSMutableArray *canMoveNotZoomAnnotTypes = [self getCanMoveNotZoomAnnotTypes];
            NSMutableArray *canNotMoveAndZoomAnnotTypes = [self getCanNotMoveAndZoomAnnotTypes];
            
            if (!_isNeedDrawBlueRect &&
                ([canMoveNotZoomAnnotTypes containsObject:@(annotType)] || [canNotMoveAndZoomAnnotTypes containsObject:@(annotType)] || !annot.canResize) ) {
                _isNeedDrawBlueRect = YES;
            }
            
            if ([canMoveAndZoomAnnotTypes containsObject:@(annotType)] && annot.canMove && annot.canResize && annotType != FSAnnotRedact) {
                [canMoveAndZoomAnnots addObject:annot];
            }
            else if ([canNotMoveAndZoomAnnotTypes containsObject:@(annotType)] || (!annot.canMove && !annot.canResize)) {
                [canNotMoveAndZoomAnnots addObject:annot];
            }
            else if ([canMoveNotZoomAnnotTypes containsObject:@(annotType)] ||( annot.canMove && !annot.canResize)){
                [canMoveNotZoomAnnots addObject:annot];
            }
            else{
                if (annotType == FSAnnotFreeText) {
                    if ([annot.intent isEqualToString:@"FreeTextTypewriter"]) {
                        [canMoveNotZoomAnnots addObject:annot];
                        _isNeedDrawBlueRect = YES;
                    } else {
                        if (annot.canMove && annot.canResize) {
                            [canMoveAndZoomAnnots addObject:annot];
                        }
                        else if (!annot.canMove && !annot.canResize) {
                            [canNotMoveAndZoomAnnots addObject:annot];
                        }
                        else if (annot.canMove && !annot.canResize){
                            [canMoveNotZoomAnnots addObject:annot];
                        }
                    }
                }else if (annotType == FSAnnotRedact){
                    FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
                    if (!redact.quads.count && annot.canMove && annot.canResize) {
                         [canMoveAndZoomAnnots addObject:annot];
                    }else{
                        [canNotMoveAndZoomAnnots addObject:annot];
                        _isNeedDrawBlueRect = YES;
                    }
                }
            }
        }
    }
    
    if (!groupHeadersSet) {
        _canGroup = NO;
        _canUngroup = NO;
        _canReplyGroup = NO;
    }
    else if (groupHeadersSet.count > 1 || groupHeadersSet.count == 0) {
        _canGroup = YES;
        _canUngroup = NO;
        _canReplyGroup = NO;
    }else{
        _canGroup = NO;
        _canUngroup = YES;
    }
    
    if ([selectedAnnots count] >= 2) {
        _extensionsManager.menuControl.menuItems = [self getMenuItems];
        if (_extensionsManager.menuControl.menuItems.count > 0 && _extensionsManager.shouldShowMenu) {
            CGRect allAnnotsRect = [self calculateAllAnnotsRect:selectedAnnots];
            [self showMenu:allAnnotsRect];
            self.multipleSelectionRect = allAnnotsRect;

            [_pdfViewCtrl refresh:pageIndex];
        }
    }
    else if ([selectedAnnots count] == 1 && _extensionsManager.shouldShowMenu) {
        // equal to single select
        _extensionsManager.currentAnnot = nil;
        FSAnnot *annot = [selectedAnnots objectAtIndex:0];
        [_extensionsManager setCurrentAnnot:annot];
        
        [_extensionsManager setCurrentToolHandler:nil];
        [_extensionsManager changeState:STATE_EDIT];
    }

    _selectedAnnots = selectedAnnots;
    if (!_isClickSelect) _selectedGroupAnnots = selectedGroupAnnots;
    _canMoveAndZoomAnnots = canMoveAndZoomAnnots;
    _canMoveNotZoomAnnots = canMoveNotZoomAnnots;
    _canNotMoveAndZoomAnnots = canNotMoveAndZoomAnnots;
}

-(NSMutableArray *)getCanMoveAndZoomAnnotTypes {
    NSMutableArray *tmpArr = @[
                               @(FSAnnotLine),
                               @(FSAnnotPolyLine),
                               @(FSAnnotInk),
                               @(FSAnnotSquare),
                               @(FSAnnotPolygon),
                               @(FSAnnotCircle),
                               @(FSAnnotStamp),
                               @(FSAnnotRedact)
                               ].mutableCopy;
    return tmpArr;
}

-(NSMutableArray *)getCanMoveNotZoomAnnotTypes {
    NSMutableArray *tmpArr = @[
                               @(FSAnnotNote),
                               @(FSAnnotFileAttachment),
                               @(FSAnnotScreen),
                               ].mutableCopy;
    return tmpArr;
}

-(NSMutableArray *)getCanNotMoveAndZoomAnnotTypes {
    NSMutableArray *tmpArr = @[
                               @(FSAnnotHighlight),
                               @(FSAnnotUnderline),
                               @(FSAnnotStrikeOut),
                               @(FSAnnotSquiggly),
                               @(FSAnnotCaret),
                               ].mutableCopy;
    return tmpArr;
}

- (NSArray *)getCanGroupAnnotTypes{
    NSMutableArray *tmpArr = @[
                        @(FSAnnotNote), @(FSAnnotFreeText), @(FSAnnotLine), @(FSAnnotSquare), @(FSAnnotCircle),
                        @(FSAnnotPolygon), @(FSAnnotPolyLine), @(FSAnnotStamp), @(FSAnnotInk), @(FSAnnotPSInk), @(FSAnnotFileAttachment)
                        ].mutableCopy;
    
    return tmpArr.copy;
}

-(CGRect)calculateAllAnnotsRect:(NSMutableArray *)selectedAnnots {
    CGRect tmpRect = CGRectNull;
    for (int i = 0; i < [selectedAnnots count]; i++) {
        FSAnnot *annot = [selectedAnnots objectAtIndex:i];
        
        CGRect annotRect = [self getAnnotRect:annot];
        if ([annot getType] == FSAnnotCaret) {
            FSAnnot *mkFsAnnot = nil;
            
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            if ([markup isGrouped]) {
                FSMarkupArray *groupAnnots = [markup getGroupElements];
                for (int i = 0; i < [groupAnnots getSize]; i++) {
                    FSAnnot *groupAnnot = [groupAnnots getAt:i];
                    if (groupAnnot.type == FSAnnotStrikeOut) {
                        mkFsAnnot = groupAnnot;
                        break;
                    }
                }
                if (mkFsAnnot && ![mkFsAnnot isEmpty]) {
                    annotRect = [self getAnnotRect:mkFsAnnot];
                }
            } else {
                annotRect = [self getAnnotRect:annot];
            }
        }
        
        if (i == 0) {
            tmpRect = annotRect;
        } else {
            tmpRect = CGRectUnion(tmpRect, annotRect);
        }
    }
    return tmpRect;
}

- (NSArray<MenuItem *> *)getMenuItems {
    NSMutableArray<MenuItem *> *items = @[].mutableCopy;
    
    if (_canFlattenGroup) {
        MenuItem *flattenItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kFlatten") object:self action:@selector(flattenAnnots)];
        [items addObject:flattenItem];
    }
    
    if (_canReplyGroup) {
        MenuItem *replyItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kReply") object:self action:@selector(replyAnnots)];
        [items addObject:replyItem];
    }
    
    if (_canGroup) {
        MenuItem *groupItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kGroup") object:self action:@selector(groupAnnots)];
        [items addObject:groupItem];
    }
    if (_canUngroup) {
        MenuItem *ungroupItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kUngroup") object:self action:@selector(ungroupAnnots)];
        [items addObject:ungroupItem];
    }
    if (!_isHiddeDeleteButton) {
        MenuItem *item = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kDelete") object:self action:@selector(deleteAnnots)];
        [items addObject:item];
    }
    return items;
}

-(void)deleteAnnots {
    if (self->_extensionsManager.currentAnnot) {
        [self->_extensionsManager setCurrentAnnot:nil];
    }
    
    NSMutableArray *undoItemsOfAnnots = @[].mutableCopy;
    
    NSMutableArray<FSAnnotAttributes *> *attributesArr = @[].mutableCopy;
    for (int i = 0; i < _selectedAnnots.count; i++) {
        FSAnnot *annot = [_selectedAnnots objectAtIndex:i];
        FSAnnotType annotType = [annot getType];
        if (annotType != FSAnnotCaret || ![annot canModify] || ![[[FSMarkup alloc] initWithAnnot:annot] isGrouped]) {
            FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:annot];
            [attributesArr addObject:attributes];
        }
    }
    
    for (int i = 0; i < _selectedAnnots.count; i++) {
        FSAnnot *annot = [_selectedAnnots objectAtIndex:i];
        FSAnnotType annotType = [annot getType];
        
        id<IAnnotHandler> annotHandler = nil;
        annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
        //        [annotHandler removeAnnot:annot];
        
        FSPDFPage *page = [annot getPage];
        if (!page || [page isEmpty]) {
            return;
        }
        
        [_extensionsManager onAnnotWillDelete:page annot:annot];
        
        if (annotType != FSAnnotCaret || ![annot canModify] || ![[[FSMarkup alloc] initWithAnnot:annot] isGrouped]) {
            
            for (FSAnnotAttributes *attributes in attributesArr) {
                if ([attributes.NM isEqualToString:annot.NM]) {
                    UndoItem *undoitem = [UndoItem itemForUndoDeleteAnnotWithAttributes:attributes page:page annotHandler:annotHandler];
                    
                    [undoItemsOfAnnots addObject:undoitem];
                    
                    AudioPlayerControl *audioPlay = [AudioPlayerControl sharedControl];
                    NSString *medioFilePath = [Utility getDocumentRenditionTempFilePath:annot.ha_fileSpec PDFPath:_extensionsManager.pdfViewCtrl.filePath];
                    NSString *playFilePath = audioPlay.filePath.path;
                    if ([medioFilePath isEqualToString:playFilePath]) {
                        [audioPlay dismiss];
                    }
                    [attributesArr removeObject:attributes];
                    break;
                }
            }

            
        } else {
            
            NSArray<FSAnnotAttributes *> *attributesArray = [self createAnnotAttributes:[[FSCaret alloc] initWithAnnot:annot]];
            UndoItem *undoItem = [UndoItem itemWithUndo:^(UndoItem *item) {
                NSMutableArray<FSMarkup *> *annotArray = [NSMutableArray<FSMarkup *> arrayWithCapacity:[attributesArray count]];
                for (FSAnnotAttributes *attributes in attributesArray) {
                    FSAnnot *annot = [page addAnnot:attributes.type rect:attributes.rect];
                    if (annot && ![annot isEmpty]) {
                        [attributes resetAnnot:annot];
                        [annotArray addObject:[[FSMarkup alloc] initWithAnnot:annot]];
                    }
                }
                FSMarkupArray *fsAnnotArray = [[FSMarkupArray alloc] init];
                for (FSMarkup *annot in annotArray) {
                    [fsAnnotArray add:annot];
                }
                [page setAnnotGroup:fsAnnotArray header_index:0];
                for (FSAnnot *annot in annotArray) {
                    id<IAnnotHandler> annotHandler = [self->_extensionsManager getAnnotHandlerByAnnot:annot];
                    [annotHandler addAnnot:annot addUndo:NO];
                }
            }
            redo:^(UndoItem *item) {
               FSAnnot *annot = [Utility getAnnotByNM:attributesArray[0].NM inPage:page];
               if (annot && ![annot isEmpty]) {
                   FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:annot];
                   UndoItem *undoitem = [UndoItem itemForUndoDeleteAnnotWithAttributes:attributes page:page annotHandler:annotHandler];
                   
                   [undoItemsOfAnnots addObject:undoitem];
                   
//                   [self removeAnnot:annot addUndo:NO];
               }
            }
            pageIndex:annot.pageIndex
            attributes:attributesArray];
           
            [undoItemsOfAnnots addObject:undoItem];

            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            FSMarkupArray *annots = [markup getGroupElements];
            for (int i = 0; i < [annots getSize]; i++) {
                FSAnnot *groupAnnot = [annots getAt:i];
                if (![groupAnnot isEqualToAnnot:annot]) {
                    [[_extensionsManager getAnnotHandlerByAnnot:groupAnnot] removeAnnot:groupAnnot addUndo:NO];
                }
            }
        }
        
        [_pdfViewCtrl lockRefresh];
        NSArray *replyAnnots = annot.replyAnnots;
        if (replyAnnots.count) {
            FSMarkup *markup = [[FSMarkup alloc] initWithAnnot:annot];
            [markup removeAllReplies];
        }
        [page removeAnnot:annot];
        [_pdfViewCtrl unlockRefresh];
        
        [_extensionsManager onAnnotDeleted:page annot:annot];
        
    }
    
    [_extensionsManager addUndoItems:undoItemsOfAnnots];
    
    self.startPoint = nil;
    self.endPoint = nil;
    self.multipleSelectionRect = CGRectZero;
    [self->_selectedAnnots removeAllObjects];
    self->_selectedAnnots = nil;
    
    if (!_extensionsManager.continueAddAnnot) {
        [_extensionsManager setCurrentToolHandler:nil];
        [_extensionsManager changeState:STATE_EDIT];
    }
    
    [_pdfViewCtrl refresh:_currentPageIndex needRender:YES];
}

- (NSArray<FSAnnotAttributes *> *)createAnnotAttributes:(FSCaret *)annot {
    NSMutableArray<FSAnnotAttributes *> *attributesArray = [NSMutableArray<FSAnnotAttributes *> array];
    [attributesArray addObject:[FSAnnotAttributes attributesWithAnnot:annot]];
    NSString *NM = annot.NM;
    FSMarkupArray *groupAnnots = [annot getGroupElements];
    for (int i = 0; i < [groupAnnots getSize]; i++) {
        FSAnnot *groupAnnot = [groupAnnots getAt:i];
        if (![groupAnnot.NM isEqualToString:NM]) {
            [attributesArray addObject:[FSAnnotAttributes attributesWithAnnot:groupAnnot]];
        }
    }
    return attributesArray;
}

-(void)flattenAnnots {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kFlatten") message:FSLocalizedForKey(@"kFlattenTip") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (self->_extensionsManager.shouldShowMenu) {
            [self showMenu:self.multipleSelectionRect];
        }
    }];
    UIAlertAction *flattenAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kFlatten") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (self->_extensionsManager.currentAnnot) {
            [self->_extensionsManager setCurrentAnnot:nil];
        }
        BOOL isAttachment = NO;
        NSMutableArray *undoItems = [NSMutableArray array];
        for (int i = 0; i < self.selectedAnnots.count; i++) {
            FSAnnot *annot = [self.selectedAnnots objectAtIndex:i];
            
            if (!isAttachment) {
                isAttachment = (annot.type == FSAnnotFileAttachment);
            }
            
            [self->_pdfViewCtrl lockRefresh];
            FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:annot];
            [Utility flattenAnnot:annot completed:^(NSArray * _Nonnull replyAttributes) {
                [undoItems addObjectsFromArray:[UndoItem itemForUndoItemsWithAttributes:attributes extensionsManager:self->_extensionsManager replyAttributes:replyAttributes]];
            }];
            [self->_pdfViewCtrl unlockRefresh];
        }
        for (UndoItem *item in undoItems) {
            [self->_extensionsManager removeUndoItem:item];
        }
        self->_extensionsManager.isDocModified = YES;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAnnotLsitUpdate object:nil];
        if (isAttachment) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationNameAttachmentLsitUpdate object:nil];
        }
        
        self.startPoint = nil;
        self.endPoint = nil;
        self.multipleSelectionRect = CGRectZero;
        [self.selectedAnnots removeAllObjects];
        self.selectedAnnots = nil;
        
        if (!self->_extensionsManager.continueAddAnnot) {
            [self->_extensionsManager setCurrentToolHandler:nil];
            [self->_extensionsManager changeState:STATE_EDIT];
        }
        
        [self->_pdfViewCtrl refresh:self->_currentPageIndex needRender:YES];
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:flattenAction];
    [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)replyAnnots{
    for (int i = 0; i < self.selectedGroupAnnots.count; i++) {
        FSAnnot *annot = [self.selectedGroupAnnots objectAtIndex:i];
        if (annot.isMarkup) {
            FSMarkup *groupHeader = annot.canGetGroupHeader;
            if (groupHeader) {
                ReplyTableViewController *replyCtr = [[ReplyTableViewController alloc] initWithStyle:UITableViewStylePlain extensionsManager:_extensionsManager];
                replyCtr.isNeedReply = YES;;
                NSMutableArray *array = [NSMutableArray array];
                FSMarkupArray *groupElements = annot.canGetGroupElements;
                if (groupElements){
                    replyCtr.isGroup = YES;
                    [ReplyUtil getReplysInDocument:_pdfViewCtrl.currentDoc annot:groupHeader replys:array];
                    [array addObject:groupHeader];
                    
                    [replyCtr setTableViewAnnotations:array];
                    replyCtr.editingDoneHandler = ^() {
                        [self->_extensionsManager setCurrentAnnot:nil];
                    };
                    replyCtr.editingCancelHandler = ^() {
                        [self->_extensionsManager setCurrentAnnot:nil];
                    };

                    FSNavigationController *navCtr = [[FSNavigationController alloc] initWithRootViewController:replyCtr];
                    navCtr.delegate = replyCtr;
                    if (DEVICE_iPAD) {
                        navCtr.modalPresentationStyle = UIModalPresentationFormSheet;
                        navCtr.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                    }
                    UIViewController *rootViewController = _pdfViewCtrl.fs_viewController;
                    [rootViewController presentViewController:navCtr animated:YES completion:nil];
                }
                break;
            }
        }
    }
}

- (void)modifiedAnnotAttributesArray:(NSArray<FSAnnotAttributes *> *)groupAnnotAttributes page:(FSPDFPage *)page{
    for (FSAnnotAttributes *attributes in groupAnnotAttributes) {
        FSAnnot *annot = [Utility getAnnotByNM:attributes.NM inPage:page];
        if (annot && ![annot isEmpty]) {
            if ([_extensionsManager.config canInteractWithAnnot:annot]) {
                 [_extensionsManager onAnnotModified:page annot:annot];
            }
        }
    }
}

- (void)groupAnnots{
    
    NSMutableArray <FSGroupAnnotAttributes *> *groupsAttributes = [NSMutableArray array];
    FSMarkupArray *markupArray = [[FSMarkupArray alloc] init];
    for (int i = 0; i < self.selectedGroupAnnots.count; i++) {
        FSAnnot *annot = [self.selectedGroupAnnots objectAtIndex:i];
        if (annot.isMarkup) {
            FSMarkup *markupi = [[FSMarkup alloc] initWithAnnot:annot];
            FSMarkup *groupHeader = markupi.canGetGroupHeader;
            if (groupHeader && ![groupHeader isEmpty]) {
                if ([groupHeader.NM isEqualToString:markupi.NM]) {
                    FSMarkupArray *groupElements = markupi.getGroupElements;
                    for (int j = 0; j < groupElements.getSize; j++) {
                        FSMarkup *markupj = [groupElements getAt:j];
                        if ([markupj.NM isEqualToString:[markupj getGroupHeader].NM]) {
                            [groupsAttributes addObject:[[FSGroupAnnotAttributes alloc] initWithMarkupArray:groupElements header_index:j]];
                            break;
                        }
                    }
                }
            }else{
                FSMarkupArray *markupArray = [[FSMarkupArray alloc] init];
                [markupArray add:markupi];
                [groupsAttributes addObject:[[FSGroupAnnotAttributes alloc] initWithMarkupArray:markupArray header_index:0]];
            }
            [markupArray add:markupi];
        }

    }
    
    if (markupArray.getSize > 1 ) {
        FSPDFPage *page = [[_pdfViewCtrl getDoc] getPage:self.currentPageIndex];
        if (!page.isParsed) {
            if ([[page startParse:FSPDFPageParsePageNormal pause:nil is_reparse:NO] resume] != FSProgressiveFinished) return;
        }
        if (!groupsAttributes.count) {
            [groupsAttributes addObject:[[FSGroupAnnotAttributes alloc] initWithMarkupArray:markupArray header_index:0]];
        }
        if (groupsAttributes.count && markupArray.getSize == self.selectedGroupAnnots.count) {
            FSGroupsAnnotAttributes *groupsAnnotAttributes = [[FSGroupsAnnotAttributes alloc] initWithGroupsAnnotAttributes:groupsAttributes.copy pageIndex:self.currentPageIndex];
            UndoItem *undoItem = [UndoItem itemForUndoGroupsAnnotAttributes:groupsAnnotAttributes completed:^{
                [self modifiedAnnotAttributesArray:groupsAnnotAttributes.groupAnnotAttributes page:page];
                [self->_pdfViewCtrl refresh:self.currentPageIndex needRender:YES];
            } page:page ungroup:NO];
            
            [_extensionsManager addUndoItem:undoItem];
            
            if ([page setAnnotGroup:markupArray header_index:0]) {
                [self modifiedAnnotAttributesArray:groupsAnnotAttributes.groupAnnotAttributes page:page];
                if (!_extensionsManager.continueAddAnnot) {
                    [_extensionsManager changeState:STATE_EDIT];
                    [self->_extensionsManager setCurrentToolHandler:nil];
                }else{
                    _canGroup = NO;
                    _canUngroup = YES;
                    [self showMenu];
                }
                [self->_pdfViewCtrl refresh:self.currentPageIndex needRender:YES];
                self->_extensionsManager.isDocModified = YES;
            }
        }
    }
}

- (void)ungroupAnnots{
    FSMarkup *groupHeader = nil;
    FSMarkupArray *markupArray = nil;
    int header_index = 0;
    for (int i = 0; i < self.selectedGroupAnnots.count; i++) {
        FSAnnot *annot = [self.selectedGroupAnnots objectAtIndex:i];
        groupHeader = [annot canGetGroupHeader];
        if (groupHeader) {
            if ([groupHeader.NM isEqualToString:annot.NM]) {
                markupArray = [groupHeader getGroupElements];
                header_index = i;
                break;
            }
        }
    }
    
    if (groupHeader && ![groupHeader isEmpty] && markupArray.getSize == self.selectedGroupAnnots.count) {
        FSPDFPage *page = [groupHeader getPage];
        FSGroupAnnotAttributes *groupAnnotAttributes = [[FSGroupAnnotAttributes alloc] initWithMarkupArray:markupArray header_index:header_index];
        UndoItem *undoItem = [UndoItem itemForUndoGroupsAnnotAttributes:[[FSGroupsAnnotAttributes alloc] initWithGroupsAnnotAttributes:@[ groupAnnotAttributes ] pageIndex:self.currentPageIndex] completed:^{
            [self modifiedAnnotAttributesArray:groupAnnotAttributes.groupAnnotAttributes page:page];
            [self->_pdfViewCtrl refresh:self.currentPageIndex needRender:YES];
        } page:page ungroup:YES];
        
        [_extensionsManager addUndoItem:undoItem];
        
        if ([groupHeader ungroup]) {
            [self modifiedAnnotAttributesArray:groupAnnotAttributes.groupAnnotAttributes page:page];
            if (!_extensionsManager.continueAddAnnot || _isClickSelect) {
                [_extensionsManager changeState:STATE_EDIT];
                [self->_extensionsManager setCurrentToolHandler:nil];
            }else{
                _canGroup = YES;
                _canUngroup = NO;
                [self showMenu];
            }
             [_pdfViewCtrl refresh:self.currentPageIndex needRender:YES];
            _extensionsManager.isDocModified = YES;
        }
    }
}

- (void)showMenu{
    if (self.selectedAnnots.count >= 2) {
        _extensionsManager.menuControl.menuItems = [self getMenuItems];
        if (_extensionsManager.menuControl.menuItems.count > 0 && _extensionsManager.shouldShowMenu) {
        CGRect allAnnotsRect = [self calculateAllAnnotsRect:self.selectedAnnots];
        [self showMenu:allAnnotsRect];
        self.multipleSelectionRect = allAnnotsRect;
        }
    }
}


- (void)showMenu:(CGRect)rect{
    CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:self.currentPageIndex];
    [_extensionsManager.menuControl setRect:dvRect];
    [_extensionsManager.menuControl showMenu];
}

#pragma mark - PageView Gesture+Touch
- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    
    CGPoint point = [[touches anyObject] locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect rect = [pageView frame];
    CGSize size = rect.size;
    if (point.x > size.width || point.y > size.height || point.x < 0 || point.y < 0)
        return NO;
    self.currentPageIndex = pageIndex;

    CGRect newMultipleSelectionRect = self.multipleSelectionRect;
    
    if (_selectedAnnots.count > 1 && [self isHitRect:newMultipleSelectionRect point:point] && _annotPageIndex == pageIndex) {
        _oldAnnotsAttributes = [self getAnnotsAttributes:_selectedAnnots];
        
        BOOL keepAspectRatio = NO;//[self keepAspectRatioForResizingAnnot:annot];
        _editType = [ShapeUtil getAnnotEditTypeWithPoint:point rect:CGRectInset(self.multipleSelectionRect, -10, -10) keepAspectRatio:keepAspectRatio defaultEditType:FSAnnotEditTypeFull];
    }
            
    return YES;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_extensionsManager.currentToolHandler != self) {
        return NO;
    }
    return YES;
}

-(void)cancelMultipleSelectionRect{
    self.startPoint = nil;
    self.endPoint = nil;
    self.multipleSelectionRect = CGRectZero;
    
    [_pdfViewCtrl refresh:_currentPageIndex];
}
#pragma mark <IFSUndoEventListener>

- (void)onWillUndo {
    [self cancelMultipleSelectionRect];
}

- (void)onWillRedo {
    [self cancelMultipleSelectionRect];
}

#pragma mark draw by CGContext
- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_isEndDrawing) {
        return ;
    }
    if (self.startPoint != nil && self.endPoint != nil && self.currentPageIndex == pageIndex) {
        CGPoint startPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.startPoint pageIndex:pageIndex];
        CGPoint endPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.endPoint pageIndex:pageIndex];
        
        CGRect rect = [self getRectFromStartPoint:startPoint endPoint:endPoint];
        [self.paintUtility drawMultipleSelectWithRect:rect inContext:context];
    }

    if (!CGRectEqualToRect(self.multipleSelectionRect, CGRectZero) && _annotPageIndex == pageIndex) {
        CGRect allAnnotsRect = [self calculateAllAnnotsRect:_selectedAnnots];
        
        if (_isNeedDrawBlueRect) {
            PaintUtility *selectedPaint = [[PaintUtility alloc] init];
            selectedPaint.annotLineWidth = 1;
            selectedPaint.annotColor = 0x2da5da;
            selectedPaint.annotOpacity = 0.8;
            
            UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
            CGSize maxRectSize = pageView.bounds.size;
            
            CGRect rect ;
            if (allAnnotsRect.size.width > maxRectSize.width -10 || allAnnotsRect.size.height > maxRectSize.height - 10) {
                rect = CGRectInset(pageView.bounds, 5, 5);
            }else{
                rect = CGRectInset(allAnnotsRect, -10, -10);
            }
            
            [selectedPaint drawMultipleSelectedWithRect:allAnnotsRect inContext:context];
        }else{
            CGRect rect = allAnnotsRect;
            
            if (rect.size.width > 0 && rect.size.height > 0) {
                rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(-10, -10, -10, -10));
                CGContextSetLineWidth(context, 2.0);
                CGFloat dashArray[] = {3, 3, 3, 3};
                CGContextSetLineDash(context, 3, dashArray, 4);
                UIColor *dashLineColor = [UIColor redColor];
                CGContextSetStrokeColorWithColor(context, [dashLineColor CGColor]);
                CGContextStrokeRect(context, rect);
                
                UIImage *dragDot = [UIImage imageNamed:@"annotation_drag.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                BOOL keepAspectRatio = NO;//[self keepAspectRatioForResizingAnnot:annot];
                NSArray *movePointArray = keepAspectRatio ? [ShapeUtil getCornerMovePointInRect:rect] : [ShapeUtil getMovePointInRect:rect];
                [movePointArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    CGRect dotRect = [obj CGRectValue];
                    CGPoint point = CGPointMake(dotRect.origin.x, dotRect.origin.y);
                    [dragDot drawAtPoint:point];
                }];
                
            }
            
        }
        
    }
}

- (CGRect)getRectFromStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CGFloat xminValue = MIN(startPoint.x, endPoint.x);
    CGFloat yminValue = MIN(startPoint.y, endPoint.y);
    
    CGFloat xmaxValue = MAX(startPoint.x, endPoint.x);
    CGFloat ymaxValue = MAX(startPoint.y, endPoint.y);
    
    CGRect result = CGRectMake(xminValue, yminValue, fabs(xmaxValue-xminValue), fabs(ymaxValue-yminValue));
    return result;
}
@end
