/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "StampIconController.h"


@implementation StampButton

@end

@implementation StampCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        float stampWidth = 130;

        float divideWidth = (DEVICE_iPHONE ? (SCREENWIDTH - stampWidth * 2) : (300 - stampWidth * 2)) - 20 * 2;

        StampButton *left = [[StampButton alloc] initWithFrame:CGRectMake(15, 15, stampWidth, 42)];
        left.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        left.tag = 100;

        StampButton *right = [[StampButton alloc] initWithFrame:CGRectMake(15 + stampWidth + divideWidth, 15, stampWidth, 42)];
        right.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        right.tag = 101;

        StampButton *center = [[StampButton alloc] init];
        center.tag = 102;

        [self.contentView addSubview:left];
        [self.contentView addSubview:right];
        [self.contentView addSubview:center];
    }
    return self;
}

@end

#define STAMP_TYPE_STANDER 1
#define STAMP_TYPE_SIGNHERE 2
#define STAMP_TYPE_DYNAMIC 3

@interface StampIconController ()

@property (nonatomic, assign) CGPDFDocumentRef pdfDocumentRef;
@property (nonatomic, strong) UIView *toolbar;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UITableView *stampLayout;
@property (nonatomic, assign) int currentStampType;
@property (nonatomic, assign) BOOL isiPhoneLandscape;
@property (nonatomic, strong) SegmentView *segmengView;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation StampIconController {
    UIExtensionsManager * __weak _extensionManager;
    FSPDFViewCtrl * __weak _pdfViewCtrl;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        self.currentIcon = extensionsManager.stampIcon;

        self.isiPhoneLandscape = NO;
        NSURL *pdfURL = [[NSBundle bundleForClass:[self class]] URLForResource:@"icon" withExtension:@"pdf"];
        self.pdfDocumentRef = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL);
    }
    return self;
}

- (void)itemClickWithItem:(SegmentItem *)item;
{
    if (item.tag == 1) {
        _currentStampType = STAMP_TYPE_STANDER;
    } else if (item.tag == 2) {
        _currentStampType = STAMP_TYPE_SIGNHERE;
    } else if (item.tag == 3) {
        _currentStampType = STAMP_TYPE_DYNAMIC;
    }
    [self.stampLayout reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    self.isiPhoneLandscape = NO;
    if (DEVICE_iPHONE && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        self.isiPhoneLandscape = YES;
    }
    CGFloat titleHeight = 30;
    CGFloat segmentViewHeight = 32;
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    self.toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 104)];
    self.toolbar.backgroundColor = NormalBarBackgroundColor;
    [self.view addSubview:self.toolbar];

    [self.toolbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(20 + titleHeight + segmentViewHeight);
        } else {
            make.bottom.mas_equalTo(self.view.mas_top).offset(40 + titleHeight + segmentViewHeight);
        }
    }];

    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backBtn.frame = CGRectMake(20, 30, 26, 26);
    [self.backBtn setImage:[UIImage imageNamed:@"panel_cancel" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    if (DEVICE_iPHONE) {
        [self.toolbar addSubview:self.backBtn];
        [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(26);
            make.width.mas_equalTo(26);
            if (@available(iOS 11.0, *)) {
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);

            } else {
                make.left.equalTo(self.view.mas_left).offset(20);
                make.top.equalTo(self.view.mas_top).offset(DEVICE_iPHONE ? 30 : 10);
            }
        }];
    }

    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.toolbar.frame.size.width / 2 - 40, DEVICE_iPHONE ? 30 : 10, 80, titleHeight)];
    title.text = FSLocalizedForKey(@"kStamp");
    title.textColor = BarLikeBlackTextColor;
    title.font = [UIFont systemFontOfSize:18.0f];
    title.textAlignment = NSTextAlignmentCenter;
    self.titleLabel = title;
    [self.toolbar addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(titleHeight);
        make.centerX.equalTo(self.toolbar.mas_centerX);
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        } else {
            make.top.equalTo(self.view.mas_top).offset(DEVICE_iPHONE ? 30 : 10);
        }
    }];

    SegmentItem *standItem = [[SegmentItem alloc] init];
    standItem.image = [UIImage imageNamed:@"annot_stamp_standard" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    standItem.selectImage = [UIImage imageNamed:@"annot_stamp_standard_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    standItem.tag = 1;

    SegmentItem *signHereItem = [[SegmentItem alloc] init];
    signHereItem.image = [UIImage imageNamed:@"annot_stamp_sign" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    signHereItem.selectImage = [UIImage imageNamed:@"annot_stamp_sign_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    signHereItem.tag = 2;

    SegmentItem *dynamicItem = [[SegmentItem alloc] init];
    dynamicItem.image = [UIImage imageNamed:@"annot_stamp_dynamic" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    dynamicItem.selectImage = [UIImage imageNamed:@"annot_stamp_dynamic_selected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    dynamicItem.tag = 3;

    NSMutableArray *array = [NSMutableArray array];
    [array addObject:standItem];
    [array addObject:signHereItem];
    [array addObject:dynamicItem];

    SegmentView *segmentView = [[SegmentView alloc] initWithFrame:CGRectMake(20, DEVICE_iPHONE ? 65 : 45, self.view.frame.size.width - 40, segmentViewHeight)];
    [segmentView loadWithItems:array];
    segmentView.delegate = self;
    self.segmengView = segmentView;

    if (self.currentIcon >= 0 && self.currentIcon <= 11) {
        [segmentView setSelectItem:standItem];
        _currentStampType = STAMP_TYPE_STANDER;
    } else if (self.currentIcon >= 12 && self.currentIcon <= 16) {
        [segmentView setSelectItem:signHereItem];
        _currentStampType = STAMP_TYPE_SIGNHERE;
    } else if (self.currentIcon >= 17 && self.currentIcon <= 21) {
        [segmentView setSelectItem:dynamicItem];
        _currentStampType = STAMP_TYPE_DYNAMIC;
    } else {
        [segmentView setSelectItem:standItem];
        _currentStampType = STAMP_TYPE_STANDER;
    }
    [self.view addSubview:segmentView];
    [segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(20);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-20);
        } else {
            make.left.equalTo(self.view.mas_left).offset(20);
            make.right.equalTo(self.view.mas_right).offset(-10);
        }
        make.bottom.equalTo(self.toolbar.mas_bottom).offset(-10);
        make.height.mas_equalTo(segmentViewHeight);
    }];

    UIView *divide = [[UIView alloc] initWithFrame:CGRectMake(20, DEVICE_iPHONE ? 105 : 85, self.view.frame.size.width - 40, [Utility realPX:1.0f])];
    divide.backgroundColor = DividingLineColor;
    [self.view addSubview:divide];
    [divide mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(segmentView.mas_left);
        make.right.equalTo(segmentView.mas_right);
        make.height.mas_equalTo([Utility realPX:1.0f]);
        make.top.equalTo(self.toolbar.mas_bottom);
    }];

    CGRect layoutFrame;
    if (DEVICE_iPHONE) {
        layoutFrame = CGRectMake(0, 110, self.view.frame.size.width, self.view.frame.size.height - 110);
    } else {
        layoutFrame = CGRectMake(0, 90, self.view.frame.size.width, self.view.frame.size.height - 90);
    }
    self.stampLayout = [[UITableView alloc] initWithFrame:layoutFrame];
    self.stampLayout.backgroundColor = ThemeViewBackgroundColor;
    self.stampLayout.delegate = self;
    self.stampLayout.dataSource = self;
    [self.stampLayout setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.stampLayout];

    [self.stampLayout mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(divide.mas_top);
        if (@available(iOS 11.0, *)) {
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.left.equalTo(self.view.mas_left).offset(20);
            make.right.equalTo(self.view.mas_right).offset(-20);
            make.bottom.equalTo(self.view.mas_bottom);
        }
    }];
}

- (void)backClicked {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)dealloc {
    self.selectHandler = nil;
    if (self.pdfDocumentRef) {
        CGPDFDocumentRelease(self.pdfDocumentRef);
    }
}

#pragma mark - Properties

#pragma mark -  table view delegate handler
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark -  table view datasource handler

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_currentStampType == STAMP_TYPE_STANDER) {
        if (self.isiPhoneLandscape) {
            return 4;
        } else {
            return 6;
        }
    } else if (_currentStampType == STAMP_TYPE_SIGNHERE) {
        if (self.isiPhoneLandscape) {
            return 2;
        } else {
            return 3;
        }
    } else if (_currentStampType == STAMP_TYPE_DYNAMIC) {
        if (self.isiPhoneLandscape) {
            return 2;
        } else {
            return 3;
        }
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellidentifer = @"CellIdentifer";
    StampCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifer];
    if (cell == nil) {
        cell = [[StampCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifer];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.backgroundColor = [UIColor fs_clearColor];
    StampButton *left = (StampButton *) [cell.contentView viewWithTag:100];
    StampButton *right = (StampButton *) [cell.contentView viewWithTag:101];
    StampButton *center = (StampButton *) [cell.contentView viewWithTag:102];
    CGFloat width = CGRectGetWidth(tableView.bounds); // OS_ISVERSION8 is different?
    if (DEVICE_iPHONE) {
        if (self.isiPhoneLandscape) {
            center.hidden = NO;
            center.frame = CGRectMake(width * 0.5 - 130 * 0.5, 15, 130, 42);
            center.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        } else {
            center.hidden = YES;
        }
        right.frame = CGRectMake(width - 15 - 130, 15, 130, 42);
    }
    int index = 20;
    if (_currentStampType == STAMP_TYPE_STANDER) {
        index = 20;
        if (self.isiPhoneLandscape) {
            left.stampIcon = (int) indexPath.row * 3;
            center.stampIcon = (int) indexPath.row * 3 + 1;
            right.stampIcon = (int) indexPath.row * 3 + 2;
        } else {
            left.stampIcon = (int) indexPath.row * 2;
            right.stampIcon = (int) indexPath.row * 2 + 1;
        }
    } else if (_currentStampType == STAMP_TYPE_SIGNHERE) {
        index = 32;
        if (self.isiPhoneLandscape) {
            left.stampIcon = 12 + (int) indexPath.row * 3;
            center.stampIcon = 12 + (int) indexPath.row * 3 + 1;
            right.stampIcon = 12 + (int) indexPath.row * 3 + 2;
        } else {
            left.stampIcon = 12 + (int) indexPath.row * 2;
            right.stampIcon = 12 + (int) indexPath.row * 2 + 1;
        }
    } else if (_currentStampType == STAMP_TYPE_DYNAMIC) {
        index = 37;
        if (self.isiPhoneLandscape) {
            left.stampIcon = 17 + (int) indexPath.row * 3;
            center.stampIcon = 17 + (int) indexPath.row * 3 + 1;
            right.stampIcon = 17 + (int) indexPath.row * 3 + 2;
        } else {
            left.stampIcon = 17 + (int) indexPath.row * 2;
            right.stampIcon = 17 + (int) indexPath.row * 2 + 1;
        }
    }

    if (self.isiPhoneLandscape) {
        index += indexPath.row * 3;
    } else {
        index += indexPath.row * 2;
    }
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(200, 60), YES, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPDFPageRef page = CGPDFDocumentGetPage(_pdfDocumentRef, index);
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, 0.0, 60);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextScaleCTM(context, 2.0, 2.0);
    CGContextDrawPDFPage(context, page);
    CGContextRestoreGState(context);
    [left setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    [left addTarget:self action:@selector(onClicked:) forControlEvents:UIControlEventTouchUpInside];

    if (index - 20 == self.currentIcon) {
        left.layer.borderWidth = 2.0f;
        left.layer.borderColor = [ThemeLikeBlueColor CGColor];
        left.layer.cornerRadius = 5.0f;
        left.backgroundColor = [UIColor clearColor];
    } else {
        left.layer.borderWidth = 0.0f;
    }

    if (self.isiPhoneLandscape) {
        index++;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(200, 60), YES, [UIScreen mainScreen].scale);
        CGContextRef context3 = UIGraphicsGetCurrentContext();
        CGPDFPageRef page3 = CGPDFDocumentGetPage(_pdfDocumentRef, index);
        CGContextSaveGState(context3);
        CGContextTranslateCTM(context3, 0.0, 60);
        CGContextScaleCTM(context3, 1.0, -1.0);
        CGContextScaleCTM(context3, 2.0, 2.0);
        CGContextDrawPDFPage(context3, page3);
        CGContextRestoreGState(context3);
        [center setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];

        UIGraphicsEndImageContext();
        [center addTarget:self action:@selector(onClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (index - 20 == self.currentIcon) {
            center.layer.borderWidth = 2.0f;
            center.layer.borderColor = [ThemeLikeBlueColor CGColor];
            center.layer.cornerRadius = 5.0f;
            center.backgroundColor = [UIColor clearColor];
        } else {
            center.layer.borderWidth = 0.0f;
        }
    }

    index++;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(200, 60), YES, [UIScreen mainScreen].scale);
    CGContextRef context1 = UIGraphicsGetCurrentContext();
    CGPDFPageRef page1 = CGPDFDocumentGetPage(_pdfDocumentRef, index);
    CGContextSaveGState(context1);
    CGContextTranslateCTM(context1, 0.0, 60);
    CGContextScaleCTM(context1, 1.0, -1.0);
    CGContextScaleCTM(context1, 2.0, 2.0);
    CGContextDrawPDFPage(context1, page1);
    CGContextRestoreGState(context1);
    [right setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    if (index == 42 || index == 37) {
        right.hidden = YES;
    } else {
        right.hidden = NO;
    }
    UIGraphicsEndImageContext();
    [right addTarget:self action:@selector(onClicked:) forControlEvents:UIControlEventTouchUpInside];
    if (index - 20 == self.currentIcon) {
        right.layer.borderWidth = 2.0f;
        right.layer.borderColor = [ThemeLikeBlueColor CGColor];
        right.layer.cornerRadius = 5.0f;
        right.backgroundColor = [UIColor clearColor];
    } else {
        right.layer.borderWidth = 0.0f;
    }
    return cell;
}

- (void)onClicked:(id)sender {
    StampButton *button = (StampButton *) sender;
    self.currentIcon = button.stampIcon;
    _extensionManager.stampIcon = self.currentIcon;
    [self.stampLayout reloadData];
    if (_selectHandler) {
        _selectHandler(button.stampIcon);
    }
    [self dismissViewControllerAnimated:YES
                             completion:^{

                             }];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];

    self.isiPhoneLandscape = NO;
    if (DEVICE_iPHONE && size.width > size.height) {
        self.isiPhoneLandscape = YES;
    } else {
        self.isiPhoneLandscape = NO;
    }
    [self.stampLayout reloadData];
}


@end
