/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "StampToolHandler.h"

#define STANDARD_STAMP_WIDTH 200
#define STANDARD_STAMP_HEIGHT 60

@interface StampToolHandler ()
@property (nonatomic, strong) FSStamp *annot;
@property (nonatomic, strong) FSRectF *currentRect;

@end

@implementation StampToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotStamp;
    }
    return self;
}

- (NSString *)getName {
    return Tool_Stamp;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
}

- (FSStamp *)addStampAtPoint:(CGPoint)point pageIndex:(int)pageIndex {
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGRect pageRect = [pageView frame];
    CGSize size = pageRect.size;
    if (point.x > size.width || point.y > size.height || point.x < 0 || point.y < 0)
        return nil;
    CGRect tmpRect = pageView.frame;
    float scale = tmpRect.size.width / 1000;
    float width = STANDARD_STAMP_WIDTH * scale;
    float height = STANDARD_STAMP_HEIGHT * scale;

    CGRect rect = CGRectMake(point.x - width / 2.0, point.y - height / 2.0, width, height);
    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
    if (!page || [page isEmpty])
        return nil;
    float pageWidth = [_pdfViewCtrl getPageViewWidth:pageIndex];
    float pageHeight = [_pdfViewCtrl getPageViewHeight:pageIndex];

    if (pageHeight - (rect.origin.y + rect.size.height) < 0) {
        rect.origin.y = pageHeight - height;
    }
    if (rect.origin.y < 0) {
        rect.origin.y = 0;
    }
    if (pageWidth - (rect.origin.x + rect.size.width) < 0) {
        rect.origin.x = pageWidth - width;
    }
    if (rect.origin.x < 0) {
        rect.origin.x = 0;
    }
    FSRectF *pdfRect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:pageIndex];
    self.currentRect = pdfRect;
    FSStamp *annot = [[FSStamp alloc] initWithAnnot:[page addAnnot:FSAnnotStamp rect:pdfRect]];
    if (!annot || [annot isEmpty]) {
        return nil;
    }
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    annot.flags = FSAnnotFlagPrint;
    annot.icon = _extensionsManager.stampIcon;
    annot.subject = [Utility getIconNameWithIconType:_extensionsManager.stampIcon annotType:FSAnnotStamp];
    
    int rotation = ([page getRotation] +[_pdfViewCtrl getViewRotation]) % 4;
    rotation = rotation == 0 ? rotation : 4 - rotation;
    annot.rotation = rotation * 90;
    
    return annot;
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.annot = [self addStampAtPoint:point pageIndex:pageIndex];
        self.annot.opacity = 0.5;
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];

        CGRect rect1 = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
        [_pdfViewCtrl refresh:rect1 pageIndex:pageIndex];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (pageIndex != self.annot.pageIndex) {
            return NO;
        }
        CGRect tmpRect = pageView.frame;
        float scale = tmpRect.size.width / 1000;
        float width = STANDARD_STAMP_WIDTH * scale;
        float height = STANDARD_STAMP_HEIGHT * scale;
        CGRect rect = CGRectMake(point.x - width / 2.0, point.y - height / 2.0, width, height);
        FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:pageIndex];
        if (!page || [page isEmpty])
            return NO;
        float pageWidth = [_pdfViewCtrl getPageViewWidth:pageIndex];
        float pageHeight = [_pdfViewCtrl getPageViewHeight:pageIndex];

        if (pageHeight - (rect.origin.y + rect.size.height) <= 0) {
            rect.origin.y = pageHeight - height;
        }
        if (rect.origin.y < 0) {
            rect.origin.y = 0;
        }
        if (pageWidth - (rect.origin.x + rect.size.width) <= 0) {
            rect.origin.x = pageWidth - width;
        }
        if (rect.origin.x < 0) {
            rect.origin.x = 0;
        }
        FSRectF *pdfRect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:pageIndex];
        self.currentRect = pdfRect;
        CGRect oldRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:pageIndex];
        [_pdfViewCtrl lockRefresh];
        self.annot.fsrect = pdfRect;
        [_pdfViewCtrl unlockRefresh];
        CGRect newRect = [_pdfViewCtrl convertPdfRectToPageViewRect:pdfRect pageIndex:pageIndex];
        [_pdfViewCtrl refresh:CGRectUnion(oldRect, newRect) pageIndex:pageIndex];
    } else { // cancel or ended
        [_pdfViewCtrl lockRefresh];
        self.annot.opacity = 1;
        self.annot.fsrect = _currentRect;
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
        [annotHandler addAnnot:self.annot addUndo:YES];
    }
    return YES;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    if (CGRectContainsPoint(pageView.bounds, point)) {
        self.annot = [self addStampAtPoint:point pageIndex:pageIndex];
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByType:self.type];
        [annotHandler addAnnot:self.annot addUndo:YES];
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] || [gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}


- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
}

@end
