/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "StampAnnotHandler.h"

#import "ShapeUtil.h"

struct StampRotaryParam {
    CGPoint origin;
    CGPoint diagonalPoint;
    CGPoint dragDotOrigin;
    CGRect rotaryDragDotRect;
    CGRect stampOverallRect;
};

@interface StampAnnotHandler ()
@property (nonatomic, assign) int startRotaryAngle;
@property (nonatomic, assign) CGFloat stampStandardW;
@property (nonatomic, assign) CGPoint startRotaryPoint;
@property (nonatomic, strong) UIImage *dragDotImage;
@property (nonatomic, strong) CATextLayer *angleText;
@property (nonatomic, assign) struct StampRotaryParam stampRotaryParam;

@end
@implementation StampAnnotHandler

#define Radians_To_Degrees(radians) ((radians) * (180.0 / M_PI))
#define Degrees_To_Radians(angle) ((angle) / 180.0 * M_PI)

CGFloat angleBetweenLines(CGPoint line1Start, CGPoint line1End, CGPoint line2Start, CGPoint line2End) {
    
    CGFloat a = line1End.x - line1Start.x;
    CGFloat b = line1End.y - line1Start.y;
    CGFloat c = line2End.x - line2Start.x;
    CGFloat d = line2End.y - line2Start.y;
    CGFloat rads = acos(((a*c) + (b*d)) / ((sqrt(a*a + b*b)) * (sqrt(c*c + d*d))));
    int x1 = line1Start.x,y1 = line1Start.y,x2 = line1End.x,y2 = line1End.y,x3 = line2End.x,y3  =line2End.y;
    if((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1) < 0){
        rads = -rads;
    }
    return Radians_To_Degrees(rads);
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super initWithUIExtensionsManager:extensionsManager]) {
        self.annotSelectionEdgeInsets = UIEdgeInsetsMake(-30, -30, -30, -30);
        self.annotRefreshRectEdgeInsets = UIEdgeInsetsMake(-30, -30, -30, -30);
        self.annotBorderEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
        self.startRotaryPoint = CGPointZero;
        self.startRotaryAngle = -1;
        self.stampStandardW = -1;
    }
    return self;
}

- (UIImage *)dragDotImage{
    if (!_dragDotImage) {
        _dragDotImage = ImageNamed(@"annotation_drag");
    }
    return _dragDotImage;
}

- (CATextLayer *)angleText{
    if (!_angleText) {
        _angleText = [[CATextLayer alloc] init];
        _angleText.contentsScale = [[UIScreen mainScreen] scale];
        _angleText.alignmentMode = kCAAlignmentCenter;
        _angleText.foregroundColor = [UIColor colorWithRGB:0x2b83fe].CGColor;
        _angleText.font = (__bridge CFTypeRef _Nullable)(@"Helvetica");
        _angleText.fontSize = 15.f;
    }
    return _angleText;
}

- (FSAnnotType)getType {
    return FSAnnotStamp;
}

- (void)onAnnotSelected:(FSAnnot *)annot{
    [super onAnnotSelected:annot];
    FSStamp *stamp = [[FSStamp alloc] initWithAnnot:annot];
    self.stampRotaryParam = [self getStampRotaryParam:stamp];
    [self refreshRotaryDragDotRect:self.stampRotaryParam.rotaryDragDotRect];
}

- (void)onAnnotDeselected:(FSAnnot *)annot{
    [super onAnnotDeselected:annot];
    [self refreshRotaryDragDotRect:self.stampRotaryParam.rotaryDragDotRect];
}

- (void)refreshRotaryDragDotRect:(CGRect)rotaryDragDotRect{
    CGRect responseRect = UIEdgeInsetsInsetRect(rotaryDragDotRect, self.annotBorderEdgeInsets);
    CGRect rect = [Utility getAnnotRect:self.annot pdfViewCtrl:self.pdfViewCtrl];
    CGRect refreshRect =  UIEdgeInsetsInsetRect(rect,self.annotRefreshRectEdgeInsets);
    if (!CGRectContainsRect(refreshRect, responseRect)) {
        refreshRect = CGRectUnion(refreshRect, responseRect);
        [self.pdfViewCtrl refresh:responseRect pageIndex:self.annot.pageIndex needRender:YES];
    }
}

- (int)stampAngleForRotation:(FSStamp *)stamp{
    int rotation = ([stamp.getPage getRotation] +[self.pdfViewCtrl getViewRotation]) % 4;
    return (stamp.rotation +  rotation * 90) % 360;
}

- (struct StampRotaryParam)getStampRotaryParam:(FSStamp *)stamp{
    
    CGRect rect = [Utility getAnnotRect:stamp pdfViewCtrl:self.pdfViewCtrl];
    
    CGFloat r = 50.f ;
    int defaultAngle = 90;
    
    CGPoint origin = CGPointMake(rect.origin.x + rect.size.width/2, rect.origin.y + rect.size.height/2);
    
    int rotation = [self stampAngleForRotation:stamp];
    CGPoint diagonalPoint =  CGPointMake(origin.x + cos(Degrees_To_Radians(defaultAngle - rotation)) * r, origin.y - sin(Degrees_To_Radians( defaultAngle  - rotation)) * r);
    
    UIView *pageView = [self.pdfViewCtrl getPageView:stamp.pageIndex];
    CGRect frame = pageView.frame;
    frame.origin = CGPointZero;
    
    
    while (!CGRectContainsPoint(frame, diagonalPoint) && r > 30.f) {
        r --;
        diagonalPoint =  CGPointMake(origin.x + cos(Degrees_To_Radians(defaultAngle - rotation)) * r, origin.y - sin(Degrees_To_Radians( defaultAngle  - rotation)) * r);
    }

    if (!CGRectContainsPoint(frame, diagonalPoint) || (rotation >= 315 && rotation<= 360) || (rotation >= 0 && rotation<= 45)){
        
        CGRect edgeRect = UIEdgeInsetsInsetRect(rect, self.annotBorderEdgeInsets);
        CGRect intersection = CGRectIntersection(edgeRect, CGRectMake(origin.x > diagonalPoint.x ? diagonalPoint.x : origin.x, origin.y > diagonalPoint.y ? diagonalPoint.y : origin.y, ABS(origin.x - diagonalPoint.x) , ABS(origin.y - diagonalPoint.y)));
        
        if (rotation >= 0 && rotation < 90 ) {
            diagonalPoint = CGPointMake(CGRectGetMaxX(intersection), CGRectGetMinY(intersection));
        }else if (rotation >= 90 && rotation < 180 ){
            diagonalPoint = CGPointMake(CGRectGetMaxX(intersection), CGRectGetMaxY(intersection));
        }else if (rotation >= 180 && rotation < 270 ){
            diagonalPoint = CGPointMake(CGRectGetMinX(intersection), CGRectGetMaxY(intersection));
        }else {
            diagonalPoint = CGPointMake(CGRectGetMinX(intersection), CGRectGetMinY(intersection));
        }
    }
    
    CGPoint dragDotOrigin = CGPointMake(diagonalPoint.x - self.dragDotImage.size.width/2, diagonalPoint.y - self.dragDotImage.size.height/2);
    CGRect rotaryDragDotRect = CGRectMake(dragDotOrigin.x,  dragDotOrigin.y, self.dragDotImage.size.width, self.dragDotImage.size.height);
    CGRect stampOverallRect = [pageView convertRect:UIEdgeInsetsInsetRect(rect, self.annotSelectionEdgeInsets) toView:self.pdfViewCtrl];
    return (struct StampRotaryParam){origin, diagonalPoint, dragDotOrigin, rotaryDragDotRect, stampOverallRect};
}

- (CGSize)minSizeForResizingAnnot:(FSAnnot *)annot {
    return CGSizeMake(10, 10);
}

- (UIColor *)borderColorForSelectedAnnot:(FSAnnot *)annot {
    return [UIColor colorWithRGB:0x179cd8];
}

- (BOOL)keepAspectRatioForResizingAnnot:(FSAnnot *)annot {
    return YES;
}

- (FSMenuOptions)menuOptionsForAnnot:(FSAnnot *)annot {
    FSMenuOptions options = [super menuOptionsForAnnot:annot];
    
    options = options & (~FSMenuOptionCopyText);
    if (options & FSMenuOptionStyle) {
        return options ^= FSMenuOptionStyle;
    }
    return options;
}

- (BOOL)isHitAnnot:(FSAnnot *)annot point:(FSPointF *)point {
    CGRect pvRect = [self getAnnotRect:annot];
    pvRect = UIEdgeInsetsInsetRect(pvRect, self.annotSelectionEdgeInsets);
    CGPoint pvPoint = [self.pdfViewCtrl convertPdfPtToPageViewPt:point pageIndex:annot.pageIndex];
    
    CGRect roPiontRect = UIEdgeInsetsInsetRect(self.stampRotaryParam.rotaryDragDotRect, self.annotBorderEdgeInsets);
    if (CGRectContainsPoint(pvRect, pvPoint) || CGRectContainsPoint(roPiontRect, pvPoint)) {
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer annot:(FSAnnot *)annot{
    if (annot.type == FSAnnotStamp && annot.canRotate ) {
        FSStamp *stamp = [[FSStamp alloc] initWithAnnot:annot];
        if (self.startRotaryAngle == -1) {
            self.startRotaryAngle  = stamp.rotation;
        }
        CGRect rect = [Utility getAnnotRect:stamp pdfViewCtrl:self.pdfViewCtrl];
        
        int rotation = ([stamp.getPage getRotation] + [self.pdfViewCtrl getViewRotation]) % 4;
//        double (^ABS_Radians) (double) = ^(double angle){
//            angle -= rotation * 90;
//            angle = (int)angle%360;
//            if (angle < 0) angle +=360;
//            if ((angle>90 && angle<=180) || (angle>270 && angle<=360)) {
//                angle = -angle;
//            }
//            return Degrees_To_Radians(angle);
//        };
        
        float oldW = CGRectGetWidth(rect);
        float oldH = CGRectGetHeight(rect);
        
        if (rotation == 1 || rotation == 3) {
            float tmp = oldW;
            oldW = oldH;
            oldH = tmp;
        }
        oldW += 1;//The error between the pdf coordinates and the ios coordinates
        oldH += 1.5;//The error between the pdf coordinates and the ios coordinates

//        int angle = [self stampAngleForRotation:stamp];
//        float radians = ABS_Radians(angle);
//        
//        float w_h_scale = 3.125;
        
        CGRect responseRect = UIEdgeInsetsInsetRect(self.stampRotaryParam.rotaryDragDotRect, self.annotBorderEdgeInsets);
        UIView *pageView = [self.pdfViewCtrl getPageView:stamp.pageIndex];
        CGPoint point = [recognizer locationInView:pageView];
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            if (CGRectContainsPoint(responseRect, point)) {
                self.startRotaryPoint = point;
                [self.pdfViewCtrl.layer addSublayer:self.angleText];
                self.angleText.string = @"0°";
                
//                float scale = ABS((sin(radians) + w_h_scale * cos(radians)) / (cos(radians) + w_h_scale * sin(radians)));
//                float newW = oldW;
//
//                if (ABS(oldW/oldH - scale) > 0.1) {
//                    float newH = oldH;
//                    if (oldW >= oldH) {
//                        newH = newW / scale;
//                    }else{
//                        newW = scale * newH;
//                    }
//                    CGRect newRect = CGRectMake(CGRectGetMidX(rect) - newW/2, CGRectGetMidY(rect) - newH/2, newW, newH);
//                    FSRectF *newRectF = [self.pdfViewCtrl convertPageViewRectToPdfRect:newRect pageIndex:stamp.pageIndex];
//                    stamp.fsrect = newRectF;
//                }
//                float c = newW/(sin(radians)/w_h_scale + cos(radians));
//                self.stampStandardW = c;
            }else{
                return [super onPageViewPan:pageIndex recognizer:recognizer annot:annot];
            }
        }
        else if (recognizer.state == UIGestureRecognizerStateChanged) {
            if (CGPointEqualToPoint(self.startRotaryPoint, CGPointZero)) {
                BOOL res = [super onPageViewPan:pageIndex recognizer:recognizer annot:annot];
                if (!CGRectContainsRect(rect, responseRect)) {
                     [self.pdfViewCtrl refresh:responseRect pageIndex:annot.pageIndex needRender:YES];
                }
                return res;
            }
            
            int angleLines = angleBetweenLines(self.stampRotaryParam.origin, self.startRotaryPoint ,self.stampRotaryParam.origin,  point);
            //the angle is kind of phantom
            self.angleText.string = (id)[NSString stringWithFormat:@"%d°", -angleLines];
            int angle = (self.startRotaryAngle + angleLines)%360;
            if (angle < 0) angle += 360;
            
//            float newRadians = ABS_Radians(angle);
//
//            float newW = ABS((sin(newRadians)/w_h_scale + cos(newRadians)) * self.stampStandardW);
//            float newH = ABS((sin(newRadians) + cos(newRadians)/w_h_scale) * self.stampStandardW);
//
//            CGRect newRect = CGRectMake(CGRectGetMidX(rect) - newW/2, CGRectGetMidY(rect) - newH/2, newW, newH);
//            FSRectF *newRectF = [self.pdfViewCtrl convertPageViewRectToPdfRect:newRect pageIndex:stamp.pageIndex];
           
            [self.pdfViewCtrl lockRefresh];
            stamp.rotation = angle;
//            stamp.fsrect = newRectF;
            [stamp resetAppearanceStream];
            [self.pdfViewCtrl unlockRefresh];
            
//            CGRect refreshRect = CGRectInset(CGRectMake(CGRectGetMidX(newRect), CGRectGetMidY(newRect), 0, 0), -ABS(self.stampStandardW), -ABS(self.stampStandardW));
            CGRect refreshRect = rect;
            refreshRect = UIEdgeInsetsInsetRect(refreshRect , self.annotRefreshRectEdgeInsets);
            refreshRect = CGRectUnion(refreshRect, self.stampRotaryParam.rotaryDragDotRect);
            refreshRect = CGRectIntersection(refreshRect, pageView.bounds);
            [self updateAnnotImage];
            [self.pdfViewCtrl refresh:refreshRect pageIndex:annot.pageIndex needRender:YES];
        } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
            if (CGPointEqualToPoint(self.startRotaryPoint, CGPointZero)) {
                 return [super onPageViewPan:pageIndex recognizer:recognizer annot:annot];
            }
            self.startRotaryPoint = CGPointZero;
            self.startRotaryAngle = -1;
            self.stampStandardW = -1;
            [self.angleText removeFromSuperlayer];
            return [super onPageViewPan:pageIndex recognizer:recognizer annot:annot];
        }
    }
    return YES;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context annot:(FSAnnot *)annot {

    [super onDraw:pageIndex inContext:context annot:annot];
    
    if (pageIndex == annot.pageIndex && [Utility isAnnot:annot uniqueToAnnot:self.annot] && annot.canRotate) {
        if (annot.type == FSAnnotStamp ) {
            FSStamp *stamp = [[FSStamp alloc] initWithAnnot:annot];

            self.stampRotaryParam = [self getStampRotaryParam:stamp];
            
            CGFloat lineWidth = 3.f ;
            
            CGContextSetLineDash(context, 0, NULL, 0);
            CGContextSetLineWidth(context, lineWidth);
            CGContextSetStrokeColorWithColor(context, [self borderColorForSelectedAnnot:annot].CGColor);
            
            CGContextMoveToPoint(context, self.stampRotaryParam.origin.x, self.stampRotaryParam.origin.y);
            
            
            CGContextAddLineToPoint(context, self.stampRotaryParam.diagonalPoint.x, self.stampRotaryParam.diagonalPoint.y);
            CGContextStrokePath(context);
            
            [self.dragDotImage drawAtPoint:self.stampRotaryParam.dragDotOrigin];
            
            float angleTextW = 40.f;
            float angleTextH = 20.f;
            
            self.angleText.frame =  CGRectMake(CGRectGetMidX(self.stampRotaryParam.stampOverallRect) - angleTextW/2, CGRectGetMinY(self.stampRotaryParam.stampOverallRect) - angleTextH/2, angleTextW, angleTextH) ;
            
            if (!CGRectContainsRect(self.pdfViewCtrl.frame, self.angleText.frame)) {
                self.angleText.frame =  CGRectMake(CGRectGetMidX(self.stampRotaryParam.stampOverallRect) - angleTextW/2, CGRectGetMaxY(self.stampRotaryParam.stampOverallRect) + angleTextH/2, angleTextW, angleTextH);
                if (!CGRectContainsRect(self.pdfViewCtrl.frame, self.angleText.frame)) {
                    self.angleText.frame =  CGRectMake(CGRectGetMinX(self.stampRotaryParam.stampOverallRect) - angleTextW/2, CGRectGetMidY(self.stampRotaryParam.stampOverallRect) + angleTextH/2, angleTextW, angleTextH);
                    if (!CGRectContainsRect(self.pdfViewCtrl.frame, self.angleText.frame)) {
                        self.angleText.frame =  CGRectMake(CGRectGetMaxX(self.stampRotaryParam.stampOverallRect) + angleTextW/2, CGRectGetMaxY(self.stampRotaryParam.stampOverallRect) + angleTextH/2, angleTextW, angleTextH);
                    }
                }
            }
        }
    }

}
@end
