/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PageNavigationModule.h"
//#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface PageNavigationModule () {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
    int _oldReadFrameState;
}

@property (strong, nonatomic) FSToolbar *gotoPageToolbar;
@property (strong, nonatomic) UITextField *pageNumBar;
@property (strong, nonatomic) UIButton *goBtn;

@property (strong, nonatomic) UIView *pageNumView;
@property (strong, nonatomic) UILabel *totalNumLabel;
@property (strong, nonatomic) UIImageView *prevImage;
@property (strong, nonatomic) UIImageView *nextImage;
@property (assign, nonatomic) BOOL gotoToolbarShouldShow;

@property (nonatomic, assign) BOOL isFullScreen;
@property (nonatomic, assign) BOOL isQuitGotoing;
@end

@implementation PageNavigationModule

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _extensionsManager = extensionsManager;
        [self initSubViews];
        self.gotoToolbarShouldShow = NO;
        [self loadModule];
        _oldReadFrameState = STATE_NORMAL;
        _isQuitGotoing = NO;
    }
    return self;
}

- (NSString *)getName {
    return @"PageNavigation";
}

- (void)initSubViews {
    self.totalNumLabel = [[UILabel alloc] init];
    self.totalNumLabel.userInteractionEnabled = YES;
    [self.totalNumLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showGotoPageToolbar:)]];

    self.prevImage = [[UIImageView alloc] init];
    self.prevImage.image = [UIImage imageNamed:@"goto_page_jump_prev" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.prevImage.userInteractionEnabled = YES;
    [self.prevImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGotoPrevView:)]];

    self.nextImage = [[UIImageView alloc] init];
    self.nextImage.image = [UIImage imageNamed:@"goto_page_jump_next" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.nextImage.userInteractionEnabled = YES;
    [self.nextImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGotoNextView:)]];

    self.pageNumView = [[UIView alloc] init];
    [self.pageNumView addSubview:self.totalNumLabel];
    [self.pageNumView addSubview:self.prevImage];
    [self.pageNumView addSubview:self.nextImage];

    [_pdfViewCtrl addSubview:self.gotoPageToolbar];

    [_gotoPageToolbar addSubview:self.goBtn];
    [_gotoPageToolbar addSubview:self.pageNumBar];

    [_gotoPageToolbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self->_pdfViewCtrl);
    }];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize sizeName = [self.goBtn.titleLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 0.0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.goBtn.titleLabel.font, NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;

    [self.goBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.goBtn.superview).offset(-10);
        make.centerY.mas_equalTo(self.goBtn.superview);
        make.height.mas_equalTo(sizeName.height);
        make.width.mas_equalTo(sizeName.width + 10);
    }];

    [self.pageNumBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.pageNumBar.superview).offset(10);
        make.centerY.mas_equalTo(self.pageNumBar.superview);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(self.goBtn.mas_left).offset(-10);
    }];
    
    [self.prevImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.pageNumView).offset(10);
        make.centerY.mas_equalTo(self.pageNumView);
        make.size.mas_equalTo(self.prevImage.image.size);
    }];
    
    [self.nextImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.pageNumView).offset(-10);
        make.centerY.mas_equalTo(self.pageNumView);
        make.size.mas_equalTo(self.nextImage.image.size);
    }];
}

- (void)addPageNumberView {
    self.prevImage.hidden = YES;
    self.nextImage.hidden = YES;

    [_pdfViewCtrl insertSubview:_pageNumView aboveSubview:[_pdfViewCtrl getDisplayView]];
    [self.pageNumView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(DEVICE_iPHONE ? 34 : 30);
        make.left.mas_equalTo(self->_pdfViewCtrl.fs_mas_left).offset(DEVICE_iPHONE ? 15 : 20);
        make.bottom.mas_equalTo(self->_pdfViewCtrl.fs_mas_bottom).offset(DEVICE_iPHONE ? -59 : -64);
    }];
    self.pageNumView.layer.cornerRadius = DEVICE_iPHONE ? 17.0 : 15.0;
    self.pageNumView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.425];
}

- (void)removePageNumberView {
    UIView *pageNumView = self.pageNumView;
    dispatch_async(dispatch_get_main_queue(), ^{
        [pageNumView removeFromSuperview];
    });
}

- (NSString *)getDisplayPageLabel:(int)pageIndex needTotal:(BOOL)needTotal {
    // copied from single container scroll view
    NSString *ret;
    int pageCount = [_pdfViewCtrl getPageCount];

    PDF_LAYOUT_MODE pageMode = [_pdfViewCtrl getPageLayoutMode];
    
    if (pageMode == PDF_LAYOUT_MODE_TWO_LEFT || pageMode == PDF_LAYOUT_MODE_TWO_RIGHT || pageMode == PDF_LAYOUT_MODE_TWO_MIDDLE) {
        if(pageIndex == 0 || pageIndex == 1) {
            if (pageCount == 2 && ![_pdfViewCtrl isContinuous]) {
                if(_pdfViewCtrl.IsChangeLayoutMode ){
                    ret = [NSString stringWithFormat:@"%d",pageIndex ];
                }else{
                    ret = [NSString stringWithFormat:@"%d",pageIndex + 1];
                }
                _pdfViewCtrl.IsChangeLayoutMode = NO;
            }else{
                ret = [NSString stringWithFormat:@"%d",1];
                
                if ([_pdfViewCtrl isContinuous]) {
                    if(pageIndex == pageCount-1){
                        ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
                    }else{
                        if (pageMode == PDF_LAYOUT_MODE_TWO_RIGHT && pageIndex == 0) {
                            ret = [NSString stringWithFormat:@"%d", pageIndex+1];
                        }else{
                            ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
                        }
                    }
                }
            }
        } else{
            if ([_pdfViewCtrl isContinuous]) {
                if(pageIndex == pageCount-1){
                    ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
                }else{
                    ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
                }
            }
            else if (pageCount == 2) {
                ret = [NSString stringWithFormat:@"%d", 2];
             }
             else if(pageCount % 2 == 1){
                pageIndex--;
                ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
            }else{
                if(pageIndex == pageCount-1){
                    ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
                }else{
                    pageIndex--;
                    ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
                }
            }
        }

        if (needTotal) {
            ret = [NSString stringWithFormat:@"%@/%d", ret, pageCount];
        }
        return ret;
    }

    if (pageMode == PDF_LAYOUT_MODE_TWO &&
        !(pageCount % 2 == 1 && (pageIndex == pageCount - 1))) {
        if (pageIndex % 2 == 1) {
            pageIndex--;
        }
        ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
    } else {
        ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
    }

    if (needTotal) {
        ret = [NSString stringWithFormat:@"%@/%d", ret, pageCount];
    }
    return ret;
}

- (FSToolbar *)gotoPageToolbar {
    if (!_gotoPageToolbar) {
        //        CGRect frame = _pdfViewCtrl.bounds;
        _gotoPageToolbar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
        _gotoPageToolbar.backgroundColor = GeneralColor;
        _gotoPageToolbar.hidden = YES;
        _gotoPageToolbar.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;

        self.goBtn = [[UIButton alloc] init];
        self.goBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        self.goBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.goBtn setTitle:FSLocalizedForKey(@"kGo") forState:UIControlStateNormal];
        [self.goBtn setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
        [self.goBtn setTitleColor:GeneralColor forState:UIControlStateHighlighted | UIControlStateDisabled | UIControlStateSelected | UIControlStateApplication | UIControlStateReserved];
        self.goBtn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        [self.goBtn addTarget:self action:@selector(goAction) forControlEvents:UIControlEventTouchUpInside];

        self.pageNumBar = [[UITextField alloc] init];
        self.pageNumBar.keyboardType = UIKeyboardTypeNumberPad;
        self.pageNumBar.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        self.pageNumBar.borderStyle = UITextBorderStyleRoundedRect;
        self.pageNumBar.layer.borderColor = [UIColor colorWithRGB:0x179cdb].CGColor;
        self.pageNumBar.layer.borderWidth = 1.0f;
        self.pageNumBar.layer.cornerRadius = 4.0f;
        self.pageNumBar.delegate = self;

        UIView *divideView = [[UIView alloc] init];
        divideView.backgroundColor = DividingLineColor;
        [_gotoPageToolbar addSubview:divideView];
    }
    self.pageNumBar.placeholder = [self getDisplayPageLabel:[_pdfViewCtrl getCurrentPage] needTotal:YES];
    return _gotoPageToolbar;
}

- (void)gotoPageWithPageIndex:(int)pageIndex {
    if (pageIndex <= 0 || pageIndex > [_pdfViewCtrl getPageCount]) {
        self.pageNumBar.text = @"";
        self.pageNumBar.placeholder = [self getDisplayPageLabel:[_pdfViewCtrl getCurrentPage] needTotal:YES];

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:[NSString stringWithFormat:@"%@ %d - %d", FSLocalizedForKey(@"kWrongPageNumber"), 1, [_pdfViewCtrl getPageCount]] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [self showGotoPageToolbar:nil];
                                                       }];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    } else {
        [self gotoPage:pageIndex - 1 animated:NO];
    }
}

- (BOOL)gotoPage:(int)index animated:(BOOL)animated {
    int pageCount = [_pdfViewCtrl getPageCount];
    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_LEFT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_RIGHT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_MIDDLE) {
        pageCount = pageCount +1;
    }

    NSAssert1(index >= -1 && index < pageCount, @"Attempt to go to page index out of range: %d", index);

    if (index >= 0 && index < pageCount) {
        if (YES) {
            [self.pageNumBar resignFirstResponder];
            self.gotoPageToolbar.hidden = YES;
            [_pdfViewCtrl gotoPage:index animated:animated];
        }
        return YES;
    }
    return NO;
}

- (void)quitGotoPage {
    [self.pageNumBar resignFirstResponder];
    self.gotoPageToolbar.hidden = YES;
}

- (void)goAction {
    NSString *stringPage = [_pageNumBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    int pageIndex = 0;
    if (stringPage != nil && stringPage.length != 0) {
        pageIndex = stringPage.intValue;
    }
    [self.pageNumBar resignFirstResponder];
    [self gotoPageWithPageIndex:pageIndex];
    [self quitGotoMode];
}

- (void)quitGotoMode {
    if (_isQuitGotoing) return;
    _isQuitGotoing = YES;
    [self.pageNumBar resignFirstResponder];
    self.gotoPageToolbar.hidden = YES;
    if (!self.gotoToolbarShouldShow) {
        self.pageNumBar.text = nil;
    }
    [_extensionsManager changeState:_oldReadFrameState];
    _isQuitGotoing = NO;
}

- (void)showGotoPageToolbar:(UITapGestureRecognizer *)recognizer {
    if (_extensionsManager.getState == STATE_SEARCH) {
        [_extensionsManager changeState:STATE_NORMAL];
    }
    [_extensionsManager changeState:STATE_PAGENAVIGATE];
}

- (void)handleGotoPrevView:(UITapGestureRecognizer *)recognizer {
    [_extensionsManager setCurrentAnnot:nil];

    if ([_pdfViewCtrl hasPrevView]) {
        [_pdfViewCtrl gotoPrevView:YES];
    }
}

- (void)handleGotoNextView:(UITapGestureRecognizer *)recognizer {
    [_extensionsManager setCurrentAnnot:nil];

    if ([_pdfViewCtrl hasNextView]) {
        [_pdfViewCtrl gotoNextView:YES];
    }
}

- (void)updatePageNumLabel {
    _totalNumLabel.text = [self getDisplayPageLabel:[_pdfViewCtrl getCurrentPage] needTotal:YES];
    _totalNumLabel.font = [UIFont systemFontOfSize:15];
    
    [self.totalNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.pageNumView);
        make.left.mas_equalTo(self.pageNumView).offset(10.f).priorityMedium();
        make.right.mas_equalTo(self.pageNumView).offset(-10.f).priorityMedium();
    }];
}

- (void)setPageCountLabel {
    int state = [_extensionsManager getState];
    if (state == STATE_ANNOTTOOL || state == STATE_EDIT) {
        self.pageNumView.hidden = YES;
    } else {
        self.pageNumView.hidden = NO;
    }
    
    self.prevImage.hidden = YES;
    self.nextImage.hidden = YES;

    [self updatePageNumLabel];
    if ([_pdfViewCtrl hasPrevView]) {
        [self.totalNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
             make.left.mas_equalTo(self.prevImage.mas_right).offset(10.f).priorityHigh();
        }];
        self.prevImage.hidden = NO;
    }
    if ([_pdfViewCtrl hasNextView]) {
        [self.totalNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
             make.right.mas_equalTo(self.nextImage.mas_left).offset(-10.f).priorityHigh();
        }];
        self.nextImage.hidden = NO;
    }
    
    self.totalNumLabel.textColor = WhiteThemeTextColor;
}

- (void)loadModule {
    [_pdfViewCtrl registerScrollViewEventListener:self];
    [_pdfViewCtrl registerGestureEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_pdfViewCtrl registerDocEventListener:self];

    [_extensionsManager registerFullScreenListener:self];
    [_extensionsManager registerStateChangeListener:self];
    [_extensionsManager registerRotateChangedListener:self];
    
    [_extensionsManager registerPageNumberListener:self];
}

#pragma mark - textfiled delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self goAction];
    return YES;
}

#pragma pageEventListener methods

- (void)onPageChanged:(int)oldIndex currentIndex:(int)currentIndex {
    [self setPageCountLabel];
}

- (void)onPageJumped {
    [self setPageCountLabel];
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    // If the document has only one page, there should not be a previous page and next page
    if ([_pdfViewCtrl getPageCount] == 1) {
        [_pdfViewCtrl clearPrevNextStack];
    }
    [self setPageCountLabel];
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    // If insert a page, the data in the stack becomes invalid and needs to be empty
    [_pdfViewCtrl clearPrevNextStack];
    [self setPageCountLabel];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    // If insert a page, the data in the stack becomes invalid and needs to be empty
    [_pdfViewCtrl clearPrevNextStack];
    [self setPageCountLabel];
}

#pragma docEventlistener methods

- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if(error != FSErrSuccess) return;
    [self addPageNumberView];
    [self setPageCountLabel];
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    [self removePageNumberView];
    [self.pageNumBar resignFirstResponder];
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (!_pdfViewCtrl.currentDoc) {
        return ;
    }
    if (!self.gotoPageToolbar.hidden) {
        self.gotoToolbarShouldShow = YES;
    }
    self.pageNumView.hidden = YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (!_pdfViewCtrl.currentDoc) {
        return ;
    }
    
    [self setPageCountLabel];
    int state = [_extensionsManager getState];
    if (state == STATE_ANNOTTOOL || state == STATE_EDIT || state == STATE_FILLSIGN || _extensionsManager.currentIsFullScreen) {
        self.pageNumView.hidden = YES;
    } else {
        self.pageNumView.hidden = NO;
    }
    if (self.gotoToolbarShouldShow) {
        [self showGotoPageToolbar:nil];
        self.gotoToolbarShouldShow = NO;
    }
}

#pragma - IFullScreenListener

- (void)onFullScreen:(BOOL)isFullScreen {
    if (isFullScreen) {
        self.pageNumView.hidden = YES;
    } else {
        [self setPageCountLabel];
    }
}

#pragma mark IPageNumberListener
-(void)updatePageNumber {
    [self setPageCountLabel];
}

#pragma mark IGestureEventListener

- (BOOL)onTap:(UITapGestureRecognizer *)recognizer {
    if ([_extensionsManager getState] != STATE_PAGENAVIGATE)
        return NO;
    [self quitGotoMode];
    return YES;
}

- (BOOL)onLongPress:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

#pragma - (void) onStateChanged : (int) state;

- (void)onStateChanged:(int)state {
    if ([_extensionsManager getState] != STATE_PAGENAVIGATE)
        _oldReadFrameState = [_extensionsManager getState];
    
    if (state == STATE_PAGENAVIGATE) {
        if ([_extensionsManager getState] == STATE_SEARCH) [_extensionsManager showSearchBar:NO];
        [_extensionsManager setCurrentAnnot:nil];
        
        [self.pageNumBar becomeFirstResponder];
        self.gotoPageToolbar.hidden = NO;
    }

    if (state == STATE_ANNOTTOOL || state == STATE_EDIT || state == STATE_FILLSIGN) {
        self.pageNumView.hidden = YES;
    } else {
        if (state == STATE_NORMAL || state == STATE_REFLOW) {
            [self setPageCountLabel];
        }
        self.pageNumView.hidden = NO;
    }
}

@end
