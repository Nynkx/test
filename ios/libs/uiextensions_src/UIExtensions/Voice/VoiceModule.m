/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "VoiceModule.h"
#import "FSFileAndImagePicker.h"
#import "AudioVideoAnnotHandler.h"
#import "AudioVideoToolHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>

@interface VoiceModule ()

@property (nonatomic, strong) NSArray *colors;
@end

@implementation VoiceModule{
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Voice";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        AudioVideoToolHandler *toolHandler = [[AudioVideoToolHandler alloc] initWithUIExtensionsManager:extensionsManager annotType:FSAnnotSound];
        [_extensionsManager registerToolHandler:toolHandler];
        AudioVideoAnnotHandler *annotHandler = [[AudioVideoAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager annotType:FSAnnotSound];
        [_extensionsManager registerAnnotHandler:annotHandler];
        
        annotHandler.minPlayerLayerWidthInPage = annotHandler.minPlayerLayerHeightInPage = 0.1;
        annotHandler.maxPlayerLayerWidthInPage = annotHandler.maxPlayerLayerHeightInPage = 0.7;
        
        [self loadModule];
    }
    return self;
}

- (void)loadModule {}


@end
