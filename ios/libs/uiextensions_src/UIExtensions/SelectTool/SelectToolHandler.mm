/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "SelectToolHandler.h"
#import "FtToolHandler.h"
#import "MagnifierView.h"
#import "NoteDialog.h"
#import "SignToolHandler.h"

@interface SelectToolHandler () <IDocEventListener>

@property (nonatomic, assign) BOOL isTextSelect;
@property (nonatomic, assign) int currentPageIndex;
@property (nonatomic, strong) FSPointF *currentPoint;
@property (nonatomic, assign) int startPosIndex;
@property (nonatomic, assign) int endPosIndex;
@property (nonatomic, assign) int startReadPosIndex;
@property (nonatomic, assign) int endReadPosIndex;
@property (nonatomic, assign) BOOL isReading;
@property (nonatomic, assign) int currentReadPageIndex;
@property (nonatomic, strong) NSArray *arraySelectedRect;
@property (nonatomic, strong) FSRectF *currentEditPdfRect;
@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, strong) MagnifierView *magnifierView;
@end

@implementation SelectToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
}

@synthesize type = _type;

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _startPosIndex = -1;
        _endPosIndex = -1;
    }
    return self;
}

- (NSArray *)_getTextRects:(FSTextPage *)fstextPage start:(int)start end:(int)end {
    __block NSArray *ret = nil;
    Task *task = [[Task alloc] init];
    task.run = ^() {
        ret = [Utility getTextRects:fstextPage startCharIndex:start endCharIndex:end];
    };
    [_taskServer executeSync:task];
    return ret;
}

//get word range of string, including space
- (NSArray *)getUnitWordBoundary:(NSString *)str {
    NSMutableArray *array = [NSMutableArray array];
    CFStringTokenizerRef tokenizer = CFStringTokenizerCreate(kCFAllocatorDefault,
                                                             (CFStringRef) str,
                                                             CFRangeMake(0, [str length]),
                                                             kCFStringTokenizerUnitWordBoundary,
                                                             NULL);
    CFStringTokenizerTokenType tokenType = kCFStringTokenizerTokenNone;
    while ((tokenType = CFStringTokenizerAdvanceToNextToken(tokenizer)) != kCFStringTokenizerTokenNone) {
        CFRange tokenRange = CFStringTokenizerGetCurrentTokenRange(tokenizer);
        NSRange range = NSMakeRange(tokenRange.location, tokenRange.length);
        [array addObject:[NSValue valueWithRange:range]];
    }
    if (tokenizer) {
        CFRelease(tokenizer);
    }
    return array;
}

- (NSRange)getWordByTextIndex:(int)index textPage:(FSTextPage *)fstextPage {
    __block NSRange retRange = NSMakeRange(index, 1);

    int pageTotalCharCount = 0;

    if (fstextPage != nil) {
        pageTotalCharCount = [fstextPage getCharCount];
    }

    int startIndex = MAX(0, index - 25);
    int endIndex = MIN(pageTotalCharCount - 1, index + 25);
    index -= startIndex;

    NSString *str = [fstextPage getChars:MIN(startIndex, endIndex) count:ABS(endIndex - startIndex) + 1];
    NSArray *array = [self getUnitWordBoundary:str];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSRange range = [obj rangeValue];
        if (NSLocationInRange(index, range)) {
            NSString *tmp = [str substringWithRange:range];
            if ([tmp isEqualToString:@" "]) {
                NSUInteger nextIndex = idx + 1;
                if (nextIndex < array.count) {
                    range = [[array objectAtIndex:nextIndex] rangeValue];
                }
            }
            retRange = NSMakeRange(startIndex + range.location, range.length);
            *stop = YES;
        }
    }];

    return retRange;
}

- (int)getCharIndexAtPos:(int)pageIndex point:(CGPoint)point {
    FSPointF *dibPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    FSTextPage *textPage = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:pageIndex];
    if ([textPage isEmpty]) return -1;
    return (int) [textPage getIndexAtPos:dibPoint.x y:dibPoint.y tolerance:5];
}

- (NSArray *)getCurrentSelectRects:(int)pageIndex {
    NSMutableArray *retArray = [NSMutableArray array];
    __block CGRect unionRect = CGRectZero;

    FSTextPage *textPage = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:pageIndex];
    NSArray *array = [self _getTextRects:textPage start:self.startPosIndex end:self.endPosIndex];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGRect rect = [[obj objectAtIndex:0] CGRectValue];
        [retArray addObject:[NSValue valueWithCGRect:rect]];
        if (CGRectEqualToRect(unionRect, CGRectZero)) {
            unionRect = rect;
        } else {
            unionRect = CGRectUnion(unionRect, rect);
        }
    }];

    self.currentEditPdfRect = [Utility CGRect2FSRectF:unionRect];
    return retArray;
}

- (void)clearSelection {
    self.startPosIndex = -1;
    self.endPosIndex = -1;
    self.arraySelectedRect = nil;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
    if (self.isEdit && [_extensionsManager.menuControl isMenuVisible]) {
        [_extensionsManager.menuControl hideMenu];
    }
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    if (_extensionsManager.currentAnnot) {
        [_extensionsManager setCurrentAnnot:nil];
    }

    if ([self handleLongPressAndPan:pageIndex gestureRecognizer:recognizer]) {
        return YES;
    }

    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    id<IToolHandler> originToolHandler = _extensionsManager.currentToolHandler;
    if (_extensionsManager.currentToolHandler == self) {
        [_extensionsManager setCurrentToolHandler:nil];
    }
    if (!self.isEdit) {
        if (originToolHandler == self && [_extensionsManager.menuControl isMenuVisible]) {
            [_extensionsManager.menuControl hideMenu];
        }
        return NO;
    }
    if (_extensionsManager.currentAnnot) {
        [_extensionsManager setCurrentAnnot:nil];
    }

    self.isEdit = NO;
    [self clearSelection];
    [_pdfViewCtrl refresh:CGRectZero pageIndex:pageIndex needRender:NO];
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    if (!self.isEdit) {
        return NO;
    }
    if ([self handleLongPressAndPan:pageIndex gestureRecognizer:recognizer]) {
        return YES;
    }
    MenuControl *annotMenu = _extensionsManager.menuControl;
    if ([annotMenu isMenuVisible]) {
        [annotMenu hideMenu];
    }
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler != self || _extensionsManager.currentAnnot) {
        return NO;
    }
    if (self.isEdit) {
        CGPoint point = [gestureRecognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
        int index = [self getCharIndexAtPos:pageIndex point:point];
        index = [self verifyIndexRange:index count:100]; //is it near start or end dot
        if (index > -1) {
            return YES;
        }
    }
    return NO;
}
#pragma mark - private methods

- (BOOL)handleLongPressAndPan:(int)pageIndex gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint point = [gestureRecognizer locationInView:[_pdfViewCtrl getPageView:pageIndex]];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        MenuControl *annotMenu = _extensionsManager.menuControl;
        if ([annotMenu isMenuVisible]) {
            [annotMenu hideMenu];
        }
        if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) //only start when long press
        {
            self.currentPageIndex = pageIndex;
            //start a new range of select text
            int index = [self getCharIndexAtPos:pageIndex point:point];
            [self clearSelection];
            if (index > -1) //has some selection, make first character selected
            {
                self.isEdit = YES;
                self.isTextSelect = YES;
                FSTextPage *textPage = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:pageIndex];
                NSRange range = [self getWordByTextIndex:index textPage:textPage];
                self.startPosIndex = (int) range.location;
                self.endPosIndex = (int) (range.location + range.length - 1);
                [self showMagnifier:pageIndex index:index point:point];

                [_pdfViewCtrl refresh:pageIndex needRender:NO];
            } else {
                self.isTextSelect = NO;
                return NO;
            }
        } else if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) //pan start is used to drag the dot
        {
            int index = [self getCharIndexAtPos:pageIndex point:point];
            index = [self verifyIndexRange:index count:100]; //is it near start or end dot
            if (index > -1) {
                if (index == self.startPosIndex) {
                    //when drag dot always make drag end
                    int tmp = self.endPosIndex;
                    self.endPosIndex = self.startPosIndex;
                    self.startPosIndex = tmp;
                }
                [self showMagnifier:pageIndex index:index point:point];
                [_pdfViewCtrl refresh:pageIndex needRender:NO];
            }
        }
    } else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if (self.currentPageIndex != pageIndex) {
            return YES;
        }
        //position change, update the selected range
        if (self.startPosIndex == -1 || self.endPosIndex == -1) {
            return YES; //not start successfully
        }
        int index = [self getCharIndexAtPos:pageIndex point:point];
        if (index > -1) {
            self.endPosIndex = index;
            [_pdfViewCtrl refresh:pageIndex needRender:NO];
            [self showMagnifier:pageIndex index:index point:point];
            [self moveMagnifier:pageIndex index:index point:point];
        }
        return YES;
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        [self closeMagnifier];
        if (self.currentPageIndex != pageIndex) {
            [self showTextMenu:self.currentPageIndex rect:[_pdfViewCtrl convertPdfRectToPageViewRect:self.currentEditPdfRect pageIndex:self.currentPageIndex]];
            return YES;
        }

        //getsture end. if have selection, pop up menu to let user choose; otherwise change to none op.
        if (self.startPosIndex == -1 || self.endPosIndex == -1) {
            return NO;
        } else {
            [self getCurrentSelectRects:pageIndex];
            [self showTextMenu:pageIndex rect:[_pdfViewCtrl convertPdfRectToPageViewRect:self.currentEditPdfRect pageIndex:self.currentPageIndex]];
            return YES;
        }
    }
    return YES;
}

- (int)verifyIndexRange:(int)index count:(int)count {
    int ret = -1;
    if (index == -1) {
        ret = index;
    } else if (index >= self.startPosIndex - count && index <= self.startPosIndex + count) {
        ret = self.startPosIndex;
    } else if (index >= self.endPosIndex - count && index <= self.endPosIndex + count) {
        ret = self.endPosIndex;
    }
    return ret;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (void)showTextMenu:(int)pageIndex rect:(CGRect)rect {
    self.currentPageIndex = pageIndex;
    NSMutableArray *array = [NSMutableArray array];
    MenuItem *copyTextItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kCopyText") object:self action:@selector(copyText)];
    MenuItem *hightLightItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kHighlight") object:self action:@selector(addHighlight)];
    MenuItem *squigglyItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSquiggly") object:self action:@selector(addSquiggly)];
    MenuItem *strikeOutItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kStrikeout") object:self action:@selector(addStrikeout)];
    MenuItem *underlineItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kUnderline") object:self action:@selector(addUnderline)];
    MenuItem *redactItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kRedaction") object:self action:@selector(addRedact)];
    MenuItem *speechItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSpeak") object:self action:@selector(speech)];

    if ([Utility canCopyText:_pdfViewCtrl] && _extensionsManager.config.copyText) {
        [array addObject:copyTextItem];
    }

    if ([Utility canAddAnnot:_pdfViewCtrl]) {
        if ([_extensionsManager.config.tools containsObject:Tool_Highlight]) {
            [array addObject:hightLightItem];
        }
        if ([_extensionsManager.config.tools containsObject:Tool_Underline]) {
            [array addObject:underlineItem];
        }
        if ([_extensionsManager.config.tools containsObject:Tool_Strikeout]) {
            [array addObject:strikeOutItem];
        }
        if ([_extensionsManager.config.tools containsObject:Tool_Squiggly]) {
            [array addObject:squigglyItem];
        }
        if ([_extensionsManager.config.tools containsObject:Tool_Redaction]) {
            [array addObject:redactItem];
        }
    }
    
    BOOL canSpeak = [Utility canCopyText:_pdfViewCtrl] && _extensionsManager.config.copyText && ![_pdfViewCtrl isDynamicXFA];
    if (canSpeak) {
        [array addObject:speechItem];
    }

    if (array.count > 0) {
        CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:rect pageIndex:self.currentPageIndex];
        MenuControl *annotMenu = _extensionsManager.menuControl;
        annotMenu.menuItems = array;
        [annotMenu setRect:dvRect];
        [annotMenu showMenu];
    }
}

- (NSString *)copyText;
{
    self.isEdit = NO;
    NSMutableString *str = [NSMutableString stringWithFormat:@""];

    FSTextPage *textSelect = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:self.currentPageIndex];
    NSString *selStr = [textSelect getChars:MIN(self.startPosIndex, self.endPosIndex) count:ABS(self.endPosIndex - self.startPosIndex) + 1];
    if (selStr && selStr.length > 0) {
        [str appendString:selStr];
    }
    if (str && ![str isEqualToString:@""]) {
        UIPasteboard *board = [UIPasteboard generalPasteboard];
        board.string = str;
    }
    [_pdfViewCtrl refresh:self.currentPageIndex needRender:NO];
    [self clearSelection];
    return str;
}

- (void)addHighlight {
    self.isEdit = NO;
    [self addMarkup:FSAnnotHighlight];
    [self clearSelection];
}

- (void)addSquiggly {
    self.isEdit = NO;
    [self addMarkup:FSAnnotSquiggly];
    [self clearSelection];
}

- (void)addStrikeout {
    self.isEdit = NO;
    [self addMarkup:FSAnnotStrikeOut];
    [self clearSelection];
}

- (void)addUnderline {
    self.isEdit = NO;
    [self addMarkup:FSAnnotUnderline];
    [self clearSelection];
}

- (void)addRedact{
    if (!_extensionsManager.isSupportRedaction) {
        [Utility alert_OK_WithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailedLoadModuleForLicenseTip") actionBack:nil];
        [self clearSelection];
        return;
    }
    self.isEdit = NO;
    [self addMarkup:FSAnnotRedact];
    [self clearSelection];
}

- (void)speech{
    NSMutableArray *array = [NSMutableArray array];
    MenuItem *speechSeclectItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSpeakString") object:self action:@selector(speechSeclect)];
    MenuItem *speechFromItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSpeakStart") object:self action:@selector(speechFrom)];
    [array addObject:speechSeclectItem];
    [array addObject:speechFromItem];
    
    if (array.count > 0) {
        CGRect dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect: [_pdfViewCtrl convertPdfRectToPageViewRect:self.currentEditPdfRect pageIndex:self.currentPageIndex] pageIndex:self.currentPageIndex];
        MenuControl *annotMenu = _extensionsManager.menuControl;
        annotMenu.menuItems = array;
        [annotMenu setRect:dvRect];
        [annotMenu showMenu];
    }
}


- (void)speechSeclect
{
    self.isEdit = NO;
    _startReadPosIndex = MIN(_startPosIndex, _endPosIndex);
    _endReadPosIndex =   MAX(_startPosIndex, _endPosIndex);
    _currentReadPageIndex = _currentPageIndex;
    SpeechModule *speech = _extensionsManager.speechModule;
    [speech speakSelectPageIndex:self.currentPageIndex startCharPos:_startReadPosIndex endCharPos:_endReadPosIndex];
    _isReading = YES;
    [self getCurrentSelectRects:_currentPageIndex];
    [self clearSelection];
}

- (void)speechFrom
{
    self.isEdit = NO;

    _startReadPosIndex = MIN(_startPosIndex, _endPosIndex);
    _endReadPosIndex =   MAX(_startPosIndex, _endPosIndex);
    _currentReadPageIndex = _currentPageIndex;
    SpeechModule *speech = _extensionsManager.speechModule;
    [speech speakFromPageIndex:self.currentPageIndex startCharPos:_startReadPosIndex];
    [self getCurrentSelectRects:_currentPageIndex];
    [self clearSelection];
    return;
}

//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
//    self.isEdit = NO;
//    [_pdfViewCtrl refresh:CGRectZero pageIndex:self.currentPageIndex needRender:NO];
//}

- (void)addMarkup:(FSAnnotType)type {
    FSTextPage *textPage = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:self.currentPageIndex];
    NSArray *array = [self _getTextRects:textPage start:self.startPosIndex end:self.endPosIndex];
    NSMutableArray *arrayQuads = [NSMutableArray array];
    for (int i = 0; i < array.count; i++) {
        FSRectF *dibRect = [Utility CGRect2FSRectF:[[[array objectAtIndex:i] objectAtIndex:0] CGRectValue]];
        int direction = [[[array objectAtIndex:i] objectAtIndex:1] intValue];
        CGPoint point1;
        CGPoint point2;
        CGPoint point3;
        CGPoint point4;
        if (direction == 0 || direction == 4) //text is horizontal or unknown, left to right
        {
            point1.x = dibRect.left;
            point1.y = dibRect.top;
            point2.x = dibRect.right;
            point2.y = dibRect.top;
            point3.x = dibRect.left;
            point3.y = dibRect.bottom;
            point4.x = dibRect.right;
            point4.y = dibRect.bottom;
        } else if (direction == 1 || direction == 3) // test is vertical, left to right
        {
            point4.x = dibRect.right;
            point4.y = dibRect.top;
            point3.x = dibRect.right;
            point3.y = dibRect.bottom;
            point2.x = dibRect.left;
            point2.y = dibRect.top;
            point1.x = dibRect.left;
            point1.y = dibRect.bottom;
        } else if (direction == 2) //text is horizontal, right to left
        {
            point4.x = dibRect.left;
            point4.y = dibRect.top;
            point3.x = dibRect.right;
            point3.y = dibRect.top;
            point2.x = dibRect.left;
            point2.y = dibRect.bottom;
            point1.x = dibRect.right;
            point1.y = dibRect.bottom;
        // } else if (direction == 3) //text is vertical, right to left
        // {
        //     point1.x = dibRect.right;
        //     point1.y = dibRect.top;
        //     point2.x = dibRect.right;
        //     point2.y = dibRect.bottom;
        //     point3.x = dibRect.left;
        //     point3.y = dibRect.top;
        //     point4.x = dibRect.left;
        //     point4.y = dibRect.bottom;
        } else {
            continue;
        }

        FSQuadPoints *fsqp = [[FSQuadPoints alloc] init];
        FSPointF *pt1 = [[FSPointF alloc] init];
        [pt1 set:point1.x y:point1.y];
        FSPointF *pt2 = [[FSPointF alloc] init];
        [pt2 set:point2.x y:point2.y];
        FSPointF *pt3 = [[FSPointF alloc] init];
        [pt3 set:point3.x y:point3.y];
        FSPointF *pt4 = [[FSPointF alloc] init];
        [pt4 set:point4.x y:point4.y];
        [fsqp setFirst:pt1];
        [fsqp setSecond:pt2];
        [fsqp setThird:pt3];
        [fsqp setFourth:pt4];
        [arrayQuads addObject:fsqp];
    }
    if (0 == arrayQuads.count)
        return;
    CGRect insetRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.currentEditPdfRect pageIndex:self.currentPageIndex];
    FSRectF *rect = [_pdfViewCtrl convertPageViewRectToPdfRect:insetRect pageIndex:self.currentPageIndex];

    FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:self.currentPageIndex];
    if (!page || [page isEmpty])
        return;

    FSMarkup *annot = nil;
    if (type == FSAnnotRedact) {
        @try {
            FSRedaction *redaction = [[FSRedaction alloc] initWithDocument:page.getDocument];
            FSRectFArray *rects = [[FSRectFArray alloc] init];
            [rects add:rect];
            annot = [redaction markRedactAnnot:page rects:rects];
        } @catch (NSException *exception) {
            return;
        }
    }else{
        annot = [[FSMarkup alloc] initWithAnnot:[page addAnnot:type rect:rect]];
    }
    annot.NM = [Utility getUUID];
    annot.author = _extensionsManager.annotAuthor;
    annot.quads = arrayQuads;
    annot.createDate = [NSDate date];
    annot.modifiedDate = [NSDate date];
    annot.flags = FSAnnotFlagPrint;

    unsigned int color = [_extensionsManager getPropertyBarSettingColor:type];
    int opacity = [_extensionsManager getPropertyBarSettingOpacity:type];
    if (type == FSAnnotHighlight) {
        annot.subject = @"Highlight";
        annot.color = [Preference getIntValue:@"Highlight" type:@"Color" defaultValue:0] != 0 ? [Preference getIntValue:@"Highlight" type:@"Color" defaultValue:0] : color;
        opacity = [Preference getIntValue:@"Highlight" type:@"Opacity" defaultValue:0] != 0 ? [Preference getIntValue:@"Highlight" type:@"Opacity" defaultValue:0] : opacity;
    } else if (type == FSAnnotSquiggly) {
        annot.subject = @"Squiggly";
        annot.color = [Preference getIntValue:@"Squiggly" type:@"Color" defaultValue:0] != 0 ? [Preference getIntValue:@"Squiggly" type:@"Color" defaultValue:0] : color;
        opacity = [Preference getIntValue:@"Squiggly" type:@"Opacity" defaultValue:0] != 0 ? [Preference getIntValue:@"Squiggly" type:@"Opacity" defaultValue:0] : opacity;

    } else if (type == FSAnnotStrikeOut) {
        annot.subject = @"Strikeout";
        annot.color = [Preference getIntValue:@"Strikeout" type:@"Color" defaultValue:0] != 0 ? [Preference getIntValue:@"Strikeout" type:@"Color" defaultValue:0] : color;
        opacity = [Preference getIntValue:@"Strikeout" type:@"Opacity" defaultValue:0] != 0 ? [Preference getIntValue:@"Strikeout" type:@"Opacity" defaultValue:0] : opacity;

    } else if (type == FSAnnotUnderline) {
        annot.subject = @"Underline";
        annot.color = [Preference getIntValue:@"Underline" type:@"Color" defaultValue:0] != 0 ? [Preference getIntValue:@"Underline" type:@"Color" defaultValue:0] : color;
        opacity = [Preference getIntValue:@"Underline" type:@"Opacity" defaultValue:0] != 0 ? [Preference getIntValue:@"Underline" type:@"Opacity" defaultValue:0] : opacity;
    }else if (type == FSAnnotRedact) {
        annot.subject = @"Redact";
        annot.contents = @"Redaction";
        FSRedact *redact = [[FSRedact alloc] initWithAnnot:annot];
        redact.applyFillColor = [Preference getIntValue:@"Redact" type:@"applyFillColor" defaultValue:0] != 0 ? [Preference getIntValue:@"Redact" type:@"applyFillColor" defaultValue:0] : color;
    }
    self.colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    annot.opacity = opacity / 100.0f;

    if (textPage && type != FSAnnotRedact) {
        NSString *tmp = @"";
        tmp = [textPage getChars:MIN(self.startPosIndex, self.endPosIndex) count:ABS(self.endPosIndex - self.startPosIndex) + 1];
//        for (int i = 0; i < arrayQuads.count; i++) {
//            FSQuadPoints *arrayQuad = [arrayQuads objectAtIndex:i];
//            FSRectF *rect = [Utility convertToFSRect:arrayQuad.getFirst p2:arrayQuad.getFourth];
//            NSString *text = [textPage getTextInRect:rect];
//            if (text.length > 0) {
//                tmp = [tmp stringByAppendingString:[textPage getTextInRect:rect]];
//            }
//        }
        [annot setContents:[Utility encodeEmojiString:tmp]];
    }
    [_pdfViewCtrl lockRefresh];
    [annot resetAppearanceStream];
    [_pdfViewCtrl unlockRefresh];

    Task *task = [[Task alloc] init];
    task.run = ^() {
        id<IAnnotHandler> annotHandler = [self->_extensionsManager getAnnotHandlerByAnnot:annot];
        [annotHandler addAnnot:annot];

        CGRect cgRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:annot.fsrect pageIndex:self.currentPageIndex];
        cgRect = CGRectInset(cgRect, -20, -20);

        [self->_pdfViewCtrl refresh:cgRect pageIndex:self.currentPageIndex];
        [self clearSelection];
    };
    [_taskServer executeSync:task];
}

- (void)showBlankMenu:(int)pageIndex point:(CGPoint)point {
    self.currentPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    self.currentPageIndex = pageIndex;
    NSMutableArray *array = [NSMutableArray array];
    MenuItem *commentItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kNote") object:self action:@selector(comment)];
    MenuItem *typeWriterItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kTypewriter") object:self action:@selector(typeWriter)];
    MenuItem *signatureItem = [[MenuItem alloc] initWithTitle:FSLocalizedForKey(@"kSignAction") object:self action:@selector(signature)];

    if ([Utility canAddAnnot:_pdfViewCtrl]) {
        if ([_extensionsManager.config.tools containsObject:Tool_Note]) {
            [array addObject:commentItem];
        }
        if ([_extensionsManager.config.tools containsObject:Tool_Freetext]) {
            [array addObject:typeWriterItem];
        }
    }
    if ([Utility canAddSign:_pdfViewCtrl]) {
        if (_extensionsManager.config.loadSignature)
            [array addObject:signatureItem];
    }

    if (array.count > 0) {
        CGRect dvRect = CGRectMake(point.x, point.y, 2, 2);
        dvRect = [_pdfViewCtrl convertPageViewRectToDisplayViewRect:dvRect pageIndex:pageIndex];
        MenuControl *annotMenu = _extensionsManager.menuControl;
        annotMenu.menuItems = array;
        [annotMenu setRect:dvRect];
        [annotMenu showMenu];
    }
}

- (void)comment {
    self.isEdit = NO;
//    self.colors = @[ @0xFF9F40, @0x8080FF, @0xBAE94C, @0xFFF160, @0xC3C3C3, @0xFF4C4C, @0x669999, @0xC72DA1, @0x996666, @0x000000 ];
    self.colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    unsigned int color = [_extensionsManager getPropertyBarSettingColor:FSAnnotNote];

    float pageWidth = [_pdfViewCtrl getPageViewWidth:self.currentPageIndex];
    float pageHeight = [_pdfViewCtrl getPageViewHeight:self.currentPageIndex];

    float scale = pageWidth / 1000.0;
    CGPoint pvPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.currentPoint pageIndex:self.currentPageIndex];

    if (pvPoint.x > pageWidth - NOTE_ANNOTATION_WIDTH * scale * 2)
        pvPoint.x = pageWidth - NOTE_ANNOTATION_WIDTH * scale * 2;
    if (pvPoint.y > pageHeight - NOTE_ANNOTATION_WIDTH * scale * 2)
        pvPoint.y = pageHeight - NOTE_ANNOTATION_WIDTH * scale * 2;

    CGRect rect = CGRectMake(pvPoint.x - NOTE_ANNOTATION_WIDTH * scale / 2, pvPoint.y - NOTE_ANNOTATION_WIDTH * scale / 2, NOTE_ANNOTATION_WIDTH * scale, NOTE_ANNOTATION_WIDTH * scale);
    FSRectF *dibRect = [_pdfViewCtrl convertPageViewRectToPdfRect:rect pageIndex:self.currentPageIndex];

    NoteDialog *noteDialog = [[NoteDialog alloc] init];
    [noteDialog show:nil replyAnnots:nil title:nil];

    noteDialog.noteEditDone = ^(NoteDialog *dialog) {
        FSPDFPage *page = [self->_pdfViewCtrl.currentDoc getPage:self.currentPageIndex];
        if (!page || [page isEmpty])
            return;

        FSNote *note = [[FSNote alloc] initWithAnnot:[page addAnnot:FSAnnotNote rect:dibRect]];
        note.color = color;
        int opacity = [self->_extensionsManager getPropertyBarSettingOpacity:FSAnnotNote];
        note.opacity = opacity / 100.0f;
        note.icon = self->_extensionsManager.noteIcon;
        note.author = _extensionsManager.annotAuthor;
        note.contents = [dialog getContent];
        note.NM = [Utility getUUID];
        note.lineWidth = 2;
        note.modifiedDate = [NSDate date];
        note.createDate = [NSDate date];
        id<IAnnotHandler> annotHandler = [self->_extensionsManager getAnnotHandlerByAnnot:note];
        [annotHandler addAnnot:note];
    };
    if (_extensionsManager.currentToolHandler == self) {
        [_extensionsManager setCurrentToolHandler:nil];
    }
}

- (void)typeWriter {
    self.isEdit = NO;
    FtToolHandler *toolHandler = (FtToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Freetext];
    [_extensionsManager setCurrentToolHandler:toolHandler];
    toolHandler.freeTextStartPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.currentPoint pageIndex:self.currentPageIndex];
    [toolHandler onPageViewTap:self.currentPageIndex recognizer:nil];
    toolHandler.isTypewriterToolbarActive = NO;
    if (_extensionsManager.currentToolHandler == self) {
        [_extensionsManager setCurrentToolHandler:nil];
    }
}

- (void)signature {
    self.isEdit = NO;
    SignToolHandler *toolHandler = (SignToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Signature];
    [_extensionsManager setCurrentToolHandler:toolHandler];
    toolHandler.isAdded = NO;
    toolHandler.signatureStartPoint = [_pdfViewCtrl convertPdfPtToPageViewPt:self.currentPoint pageIndex:self.currentPageIndex];
    [toolHandler onPageViewTap:self.currentPageIndex recognizer:nil];
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (pageIndex != self.currentPageIndex) {
        return;
    }

    if (self.isEdit && self.isTextSelect) {
        if (self.startPosIndex == -1 || self.endPosIndex == -1) {
            return;
        }

        self.arraySelectedRect = [self getCurrentSelectRects:pageIndex];
        [self.arraySelectedRect enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            CGRect selfRect = [obj CGRectValue];
            FSRectF *docRect = [Utility CGRect2FSRectF:selfRect];
            CGRect pvRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:docRect pageIndex:pageIndex];

            UIColor *highlightColor = self->_extensionsManager.selectionHighlightColor;
            CGFloat red, green, blue, alpha;
            [highlightColor getRed:&red green:&green blue:&blue alpha:&alpha];
            CGContextSetRGBFillColor(context, red, green, blue, alpha);
            CGContextFillRect(context, pvRect);

            //draw the drag dot
            if (idx == 0) {
                UIImage *dragDot = [UIImage imageNamed:@"annotation_dragdot.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                CGRect leftCursor = CGRectMake((int) (pvRect.origin.x - 7.5), (int) (pvRect.origin.y - 12), 15, 17);
                [dragDot drawAtPoint:leftCursor.origin];
            }
            if (idx + 1 == self.arraySelectedRect.count) {
                UIImage *dragDot = [UIImage imageNamed:@"annotation_dragdot.png" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                CGRect rightCursor = CGRectMake((int) (pvRect.origin.x + pvRect.size.width - 7.5), (int) (pvRect.origin.y + pvRect.size.height - 5), 15, 17);
                [dragDot drawAtPoint:rightCursor.origin];
            }
        }];
    }
}

- (unsigned int)color {
    return [_extensionsManager getPropertyBarSettingColor:self.type];
}

- (void)setColor:(unsigned int)color {
    [_extensionsManager setAnnotColor:color annotType:self.type];
}

- (int)opacity {
    return [_extensionsManager getAnnotOpacity:self.type];
}

- (void)setOpacity:(int)opacity {
    return [_extensionsManager setAnnotOpacity:opacity annotType:self.type];
}

#pragma mark - IPageEventListener

- (void)onPageChanged:(int)oldIndex currentIndex:(int)currentIndex {
    if (oldIndex != currentIndex) {
        if (!self.isEdit) {
            return;
        }
        if (_extensionsManager.currentAnnot) {
            [_extensionsManager setCurrentAnnot:nil];
        }

        MenuControl *annotMenu = _extensionsManager.menuControl;
        if ([annotMenu isMenuVisible]) {
            [annotMenu hideMenu];
        }
        self.isEdit = NO;
        [self clearSelection];

        [_pdfViewCtrl refresh:CGRectZero pageIndex:oldIndex needRender:NO];
        [_pdfViewCtrl refresh:CGRectZero pageIndex:currentIndex needRender:NO];
    }
}

#pragma mark IRotationEventListener

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self dismissAnnotMenu];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self showAnnotMenu];
}

#pragma mark IGestureEventListener

- (BOOL)onTap:(UITapGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onLongPress:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (void)onScrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    [self showAnnotMenu];
}

- (void)onScrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self showAnnotMenu];
}

- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView {
    [self dismissAnnotMenu];
}

- (void)onScrollViewDidEndZooming:(UIScrollView *)scrollView {
    [self showAnnotMenu];
}

- (void)showAnnotMenu {
    if (self.isEdit) {
        double delayInSeconds = .05;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            MenuControl *annotMenu = self->_extensionsManager.menuControl;
            if (self.isTextSelect) {
                CGRect dvRect = [self->_pdfViewCtrl convertPdfRectToPageViewRect:self.currentEditPdfRect pageIndex:self.currentPageIndex];
                dvRect = [self->_pdfViewCtrl convertPageViewRectToDisplayViewRect:dvRect pageIndex:self.currentPageIndex];

                CGRect rectDisplayView = [[self->_pdfViewCtrl getDisplayView] bounds];
                if (CGRectIsEmpty(dvRect) || CGRectIsNull(CGRectIntersection(dvRect, rectDisplayView)))
                    return;

                [annotMenu setRect:dvRect];
                [annotMenu showMenu];
            } else {
                CGPoint dvPoint = [self->_pdfViewCtrl convertPdfPtToPageViewPt:self.currentPoint pageIndex:self.currentPageIndex];
                CGRect dvRect = CGRectMake(dvPoint.x, dvPoint.y, 2, 2);

                CGRect rectDisplayView = [[self->_pdfViewCtrl getDisplayView] bounds];
                if (CGRectIsEmpty(dvRect) || CGRectIsNull(CGRectIntersection(dvRect, rectDisplayView)))
                    return;

                dvRect = [self->_pdfViewCtrl convertPageViewRectToDisplayViewRect:dvRect pageIndex:self.currentPageIndex];
                [annotMenu setRect:dvRect];
                [annotMenu showMenu];
            }
        });
    }
}

- (void)dismissAnnotMenu {
    if (self.isEdit) {
        MenuControl *annotMenu = _extensionsManager.menuControl;
        if ([annotMenu isMenuVisible]) {
            [annotMenu hideMenu];
        }
    }
}

#pragma mark - Magnifier

- (void)showMagnifier:(int)pageIndex index:(int)index point:(CGPoint)point {
    if (_magnifierView == nil) {
        FSTextPage *textPage = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:pageIndex];
        NSArray *array = [self _getTextRects:textPage start:index end:index + 1];
        if (array.count > 0) {
            FSRectF *dibRect = [Utility CGRect2FSRectF:[[[array objectAtIndex:0] objectAtIndex:0] CGRectValue]];
            CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:pageIndex];
            point = CGPointMake(point.x, CGRectGetMidY(rect));
        }
        _magnifierView = [[MagnifierView alloc] init];
        _magnifierView.viewToMagnify = [_pdfViewCtrl getDisplayView];
        _magnifierView.touchPoint = point;
        _magnifierView.magnifyPoint = [[_pdfViewCtrl getPageView:pageIndex] convertPoint:point toView:[_pdfViewCtrl getDisplayView]];
        [[_pdfViewCtrl getPageView:pageIndex] addSubview:_magnifierView];
    }
}

- (void)moveMagnifier:(int)pageIndex index:(int)index point:(CGPoint)point {
    FSTextPage *textPage = [Utility getTextSelect:_pdfViewCtrl.currentDoc pageIndex:pageIndex];
    NSArray *array = [self _getTextRects:textPage start:index end:index + 1];
    if (array.count > 0) {
        FSRectF *dibRect = [Utility CGRect2FSRectF:[[[array objectAtIndex:0] objectAtIndex:0] CGRectValue]];
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:dibRect pageIndex:pageIndex];
        point = CGPointMake(point.x, CGRectGetMidY(rect));
    }
    _magnifierView.touchPoint = point;
    _magnifierView.magnifyPoint = [[_pdfViewCtrl getPageView:pageIndex] convertPoint:point toView:[_pdfViewCtrl getDisplayView]];
    [_magnifierView setNeedsDisplay];
}

- (void)closeMagnifier {
    [_magnifierView removeFromSuperview];
    _magnifierView = nil;
}

- (NSString *)getName {
    return Tool_Select;
}

#pragma mark IDocEventListener

- (void)onDocWillClose:(FSPDFDoc *)document {
    self.isEdit = NO;
    [self clearSelection];
}

@end
