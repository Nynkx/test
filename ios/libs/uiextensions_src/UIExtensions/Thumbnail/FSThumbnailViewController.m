/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "AlertView.h"
#import "FSThumbnailCell.h"
#import "FSThumbnailView.h"
#import "FSThumbnailViewController.h"

#import "FSFileAndImagePicker.h"
#import "FileSelectDestinationViewController.h"
#import "InsertPageConfigVCtrl.h"

#define DEVICE_iPHONE ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
static NSArray<NSNumber *> *arrayFromRange(NSRange range);
static NSArray<NSIndexPath *> *indexPathsFromRange(NSRange range);

@interface FSThumbnailViewController () <FSThumbnailCellDelegate, FSReorderableCollectionViewReorderDelegate, FSFileAndImagePickerDelegate, InsertPageConfigVCtrlDelegate>

@property (nonatomic) CGSize cellSize;
// buttom bar
@property (nonatomic, strong) TbBaseBar *bottomBar;
@property (nonatomic) BOOL isBottomBarHidden;
@property (nonatomic, strong) TbBaseItem *moreItem;
@property (nonatomic, strong) TbBaseItem *duplicateItem;
@property (nonatomic, strong) TbBaseItem *deleteItem;
@property (nonatomic, strong) TbBaseItem *rotateItem;
@property (nonatomic, strong) TbBaseItem *extractItem;

// top bar
@property (nonatomic, strong) FSToolbar *topBar;
@property (nonatomic, strong) TbBaseBar *topBaseBar;
@property (nonatomic, strong) TbBaseItem *backItem;
@property (nonatomic, strong) TbBaseItem *titleItem;
@property (nonatomic, strong) TbBaseItem *editItem;
@property (nonatomic, strong) TbBaseItem *doneItem;
@property (nonatomic, strong) TbBaseItem *selectAllItem;
@property (nonatomic, strong) TbBaseItem *insertItem;

// thumbnail loading
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSMutableDictionary<NSIndexPath *, NSBlockOperation *> *operations;

@property (nonatomic, strong) UIView *bottombarBackView;

// misc
@property (nonatomic) NSInteger insertIndex;
@property (nonatomic, assign) BOOL needPauseNow;
@end

@implementation FSThumbnailViewController

static NSString *const reuseIDForThumbnailCell = @"thumbnailCell";
static NSString *const reuseIDForPlaceholderCell = @"placeholderCell";
static const NSUInteger bottomBarHeight = 49;
NSUInteger topBarHeight = 64;

- (instancetype)initWithViewCtrl:(FSPDFViewCtrl *)viewControl {
    if (self = [super init]) {
        self.pdfViewCtrl = viewControl;
        self.delegate = nil;
        self.document = viewControl.currentDoc;
        self.isEditing = NO;
        self.operationQueue = ({
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            queue.name = @"load thumbnail queue";
            queue.maxConcurrentOperationCount = 1;
            queue;
        });
        self.operations = [NSMutableDictionary<NSIndexPath *, NSBlockOperation *> dictionary];
        self.cellSize = [self calculateCellSize];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect rect = CGRectMake(0, 0, self.pdfViewCtrl.frame.size.width, self.pdfViewCtrl.frame.size.height);
    [self.view setFrame:rect];
    
    topBarHeight = IS_IPHONE_OVER_TEN ? 88 : 64;
    
    [self buildTopBar];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGSize tmpCellSize = self.cellSize;
//    FSRotation rotation = (FSRotation)[_pdfViewCtrl getViewRotation];
//    if (rotation == FSRotation90 || rotation == FSRotation270) {
//        tmpCellSize = CGSizeMake(self.cellSize.height, self.cellSize.width);
//    }
    layout.itemSize = tmpCellSize;

    self.collectionView = [[FSReorderableCollectionView alloc] initWithFrame:CGRectMake(0, topBarHeight, self.view.bounds.size.width, self.view.bounds.size.height - topBarHeight) collectionViewLayout:layout isModifiable:[Utility canAssemble:self.pdfViewCtrl] && ![self.document isXFA]];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.collectionView registerClass:[FSThumbnailCell class] forCellWithReuseIdentifier:reuseIDForThumbnailCell];
    [self.collectionView registerClass:[FSReorderableCollectionViewPlaceholderCell class] forCellWithReuseIdentifier:reuseIDForPlaceholderCell];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColor_DarkMode([UIColor grayColor], PageBackgroundColor_Dark);
    self.collectionView.allowsMultipleSelection = YES;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.topBar.mas_bottom);
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.operationQueue cancelAllOperations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.operationQueue cancelAllOperations];
}

#pragma mark tool bars

- (void)setIsBottomBarHidden:(BOOL)isBottomBarHidden {
    if (!_bottomBar) {
        [self buildBottomBar];
    }
    if (_isBottomBarHidden == isBottomBarHidden) {
        return;
    }
    [_bottombarBackView removeFromSuperview];
    _bottombarBackView = nil;
    if (IS_IPHONE_OVER_TEN) {
        if (_bottombarBackView == nil) {
            _bottombarBackView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 34, self.view.frame.size.width, 34)];
            _bottombarBackView.backgroundColor = self.bottomBar.contentView.backgroundColor;
            [self.view addSubview:_bottombarBackView];
        }
    }
    _isBottomBarHidden = isBottomBarHidden;
    if (isBottomBarHidden) {
        CGRect newFrame = self.bottomBar.contentView.frame;
        newFrame.origin.y = [UIScreen mainScreen].bounds.size.height;
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.bottomBar.contentView.frame = newFrame;
                             self->_bottombarBackView.frame = newFrame;
                             [self.bottomBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                                 make.height.mas_equalTo(@48);
                                 make.left.equalTo(self.view.mas_left).offset(0);
                                 make.right.equalTo(self.view.mas_right).offset(0);
                                 make.top.equalTo(self.view.mas_bottom).offset(0);
                             }];
                         }];
    } else {
        CGRect newFrame = self.bottomBar.contentView.frame;
        newFrame.origin.y = [UIScreen mainScreen].bounds.size.height - self.bottomBar.contentView.frame.size.height;
        
        if (IS_IPHONE_OVER_TEN) {
            _bottombarBackView.frame = newFrame;
            [_bottombarBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.bottomBar.contentView.mas_bottom);
                make.left.mas_equalTo(self.view.mas_left);
                make.right.mas_equalTo(self.view.mas_right);
                make.height.mas_equalTo(34);
            }];
        }
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.bottomBar.contentView.frame = newFrame;
                             [self.bottomBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                                 make.height.mas_equalTo(@48);
                                 make.left.equalTo(self.view.mas_left).offset(0);
                                 make.right.equalTo(self.view.mas_right).offset(0);
                                 if (@available(iOS 11.0, *)) {
                                     make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
                                 } else {
                                     make.bottom.mas_equalTo(self.view.mas_bottom);
                                 }
                             }];
                         }];
    }
}

- (void)buildBottomBar {
    //    if (!_bottomBar) {
    _bottomBar = [[TbBaseBar alloc] init];
    _bottomBar.top = NO;
    _bottomBar.contentView.frame = CGRectMake(0, self.view.bounds.size.height - bottomBarHeight, self.view.bounds.size.width, bottomBarHeight);
    //        _bottomBar.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    _bottomBar.contentView.backgroundColor = NormalBarBackgroundColor;
    
    _rotateItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kRotate") imageNormal:[UIImage imageNamed:@"thumb_rotate" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"thumb_rotate" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"thumb_rotate" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageTextRelation:RELATION_BOTTOM];
    _rotateItem.textColor = BlackThemeTextColor;
    _rotateItem.textFont = [UIFont systemFontOfSize:12.f];

    _extractItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kExtract") imageNormal:[UIImage imageNamed:@"thumb_extract" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"thumb_extract" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"thumb_extract" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageTextRelation:RELATION_BOTTOM];
    _extractItem.textColor = BlackThemeTextColor;
    _extractItem.textFont = [UIFont systemFontOfSize:12.f];

    _deleteItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kDelete") imageNormal:[UIImage imageNamed:@"thumb_delete_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"thumb_delete_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"thumb_delete_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageTextRelation:RELATION_BOTTOM];
    _deleteItem.textColor = BlackThemeTextColor;
    _deleteItem.textFont = [UIFont systemFontOfSize:12.f];

    _duplicateItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kCopy") imageNormal:[UIImage imageNamed:@"thumb_copy" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageSelected:[UIImage imageNamed:@"thumb_copy" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageDisable:[UIImage imageNamed:@"thumb_copy" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageTextRelation:RELATION_BOTTOM];
    _duplicateItem.textColor = BlackThemeTextColor;
    _duplicateItem.textFont = [UIFont systemFontOfSize:12.f];

    typeof(self) __weak weakSelf = self;
    _rotateItem.onTapClick = ^(TbBaseItem *item) {
        weakSelf.rotateItem.enable = NO;
        [weakSelf rotateSelected];
        weakSelf.rotateItem.enable = weakSelf.collectionView.indexPathsForSelectedItems.count > 0;
    };

    _extractItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf extractSelected];
    };

    _deleteItem.onTapClick = ^(TbBaseItem *item) {
        weakSelf.deleteItem.enable = NO;
        [weakSelf deleteSelected];
    };

    _duplicateItem.onTapClick = ^(TbBaseItem *item) {
        weakSelf.duplicateItem.enable = NO;
        [weakSelf duplicatePages:^{
            weakSelf.duplicateItem.enable = YES;
            // reset selectall
            [weakSelf onCellSelectedOrDeselected];
        }];
    };

    [_bottomBar addItem:_rotateItem displayPosition:Position_CENTER];
    [_bottomBar addItem:_extractItem displayPosition:Position_CENTER];
    [_bottomBar addItem:_deleteItem displayPosition:Position_CENTER];
    [_bottomBar addItem:_duplicateItem displayPosition:Position_CENTER];

    //    }
    if (self.view != _bottomBar.contentView.superview) {
        [self.view addSubview:_bottomBar.contentView];
        
        [_bottomBar.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.offset(bottomBarHeight);
        }];
        
        NSArray<UIView *> *tmpItemViews = @[ _rotateItem.contentView, _extractItem.contentView, _deleteItem.contentView, _duplicateItem.contentView ];
        UIView *superview = _bottomBar.contentView;
        UIView *lastIntervalView = nil;
        int count = (int)tmpItemViews.count;
        
        for (int i = 0; i < count + 1; i++) {
            UIView *intervalView = [[UIView alloc] init];
            [superview addSubview:intervalView];
            
            if (i == 0) {
                [intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.bottom.equalTo(superview);
                }];
            }
            else{
                UIView *itemView = tmpItemViews[i-1];
                [itemView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(lastIntervalView.mas_right);
                    make.centerY.equalTo(superview);
                    make.size.sizeOffset(itemView.frame.size);
                }];
                
                [intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(itemView.mas_right);
                    make.bottom.equalTo(lastIntervalView);
                    make.width.height.equalTo(lastIntervalView);
                    if (i == count) {
                        make.right.equalTo(superview);
                    }
                }];
            }
            lastIntervalView = intervalView;
            
        }
    }
    self.isBottomBarHidden = YES;
}

- (void)buildTopBar {
    
    _topBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
    [self.view addSubview:_topBar];
    
    [_topBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
    }];
    
    _topBaseBar = [[TbBaseBar alloc] init];
    _topBaseBar.contentView.frame = CGRectMake(0, 0, self.view.bounds.size.width, NAVIGATION_BAR_HEIGHT_NORMAL);
    _topBaseBar.top = NO;
    [_topBar addSubview:_topBaseBar.contentView];
    
    [_topBaseBar.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.mas_equalTo(_topBaseBar.contentView.superview);
    }];
    
    _topBaseBar.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;

    _backItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"property_back" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    _titleItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kViewModeThumbnail")];
    _titleItem.textColor = BarLikeBlackTextColor;
    _titleItem.enable = NO;
    [_topBaseBar addItem:_titleItem displayPosition:Position_CENTER];

    _editItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kEdit")];
    _editItem.textColor = [UIColor colorWithRGB:0x179cd8];

    _doneItem = [TbBaseItem createItemWithTitle:FSLocalizedForKey(@"kDone")];
    _doneItem.textColor = [UIColor colorWithRGB:0x179cd8];

    _selectAllItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"thumb_select_all" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                       imageSelected:[UIImage imageNamed:@"thumb_selected_all" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                        imageDisable:[UIImage imageNamed:@"thumb_select_all" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    _insertItem = [TbBaseItem createItemWithImage:[UIImage imageNamed:@"thumb_insert" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                    imageSelected:[UIImage imageNamed:@"thumb_insert" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]
                                     imageDisable:[UIImage imageNamed:@"thumb_insert" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];

    typeof(self) __weak weakSelf = self;
    _backItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf.delegate exitThumbnailViewController:weakSelf];
    };

    _editItem.onTapClick = ^(TbBaseItem *item) {
        weakSelf.isEditing = YES;
    };

    _doneItem.onTapClick = ^(TbBaseItem *item) {
        weakSelf.isEditing = NO;
    };

    _selectAllItem.onTapClick = ^(TbBaseItem *item) {
        [weakSelf selectOrDeselectAll];
    };

    _insertItem.onTapClick = ^(TbBaseItem *item) {
        __block NSInteger maxSelectedPageIndex = -1;
        [weakSelf.collectionView.indexPathsForSelectedItems enumerateObjectsUsingBlock:^(NSIndexPath *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            maxSelectedPageIndex = MAX(maxSelectedPageIndex, obj.item);
        }];
        weakSelf.insertIndex = maxSelectedPageIndex != -1 ? (int) (maxSelectedPageIndex + 1) : [weakSelf getPageCount];
        [weakSelf showInsertMenu:item.button insertBeforeOrAfter:NO];
    };
    [self resetTopBar];
}

- (void)resetTopBar {
    [_topBaseBar removeItem:_backItem];
    [_topBaseBar removeItem:_editItem];
    [_topBaseBar removeItem:_doneItem];
    [_topBaseBar removeItem:_insertItem];
    [_topBaseBar removeItem:_selectAllItem];

    if (self.isEditing) {
        [_topBaseBar addItem:_doneItem displayPosition:Position_RB];
        [_topBaseBar addItem:_insertItem displayPosition:Position_RB];

        if (DEVICE_iPHONE) {
            [_topBaseBar addItem:_selectAllItem displayPosition:Position_LT];
        } else {
            [_topBaseBar addItem:_selectAllItem displayPosition:Position_RB];
        }
    } else {
        [_topBaseBar addItem:_backItem displayPosition:Position_LT];
        if ([Utility canAssemble:_pdfViewCtrl] && ![_pdfViewCtrl.currentDoc isXFA]) {
            [_topBaseBar addItem:_editItem displayPosition:Position_RB];
        }
        
        self.titleItem.text = FSLocalizedForKey(@"kViewModeThumbnail");
    }
}
-(int)getPageCount{
    BOOL isDynamciXFA = [_pdfViewCtrl isDynamicXFA];
    if (!isDynamciXFA) {
        return [self.document getPageCount];
    }
    return [[_pdfViewCtrl getXFADoc] getPageCount];
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    FSReorderableCollectionView *reorderCollectionView = (FSReorderableCollectionView *) collectionView;
    
    return [self getPageCount] - reorderCollectionView.indexSetForDraggingCells.count + (reorderCollectionView.indexPathForPlaceholderCell ? 1 : 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FSReorderableCollectionView *reorderableCollectionView = (FSReorderableCollectionView *) collectionView;
    if ([indexPath isEqual:reorderableCollectionView.indexPathForPlaceholderCell]) {
        return [reorderableCollectionView dequeueReusableCellWithReuseIdentifier:reuseIDForPlaceholderCell forIndexPath:indexPath];
    }
    FSThumbnailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIDForThumbnailCell forIndexPath:indexPath];
    cell.delegate = self;
    cell.isEditing = self.isEditing;
    // remove spinner
    for (UIView *subview in cell.contentView.subviews) {
        if ([subview isKindOfClass:[UIActivityIndicatorView class]]) {
            [subview removeFromSuperview];
            break;
        }
    }
    int pageIndex = (int) [reorderableCollectionView getOriginalIndexPathForIndexPath:indexPath].item;
    cell.labelNumber.text = [NSString stringWithFormat:@"%d", pageIndex + 1];
    // update button frames
    
    float width = 600;
    float height = 800;
    @try{
        if (![_pdfViewCtrl isDynamicXFA]) {
            FSPDFPage *page = [self.document getPage:pageIndex];
            if (![page isEmpty]) {
                width = [page getWidth];
                height = [page getHeight];
            }
        }else{
            FSXFAPage *page = [[_pdfViewCtrl getXFADoc] getPage:pageIndex];
            if (![page isEmpty]) {
                width = [page getWidth];
                height = [page getHeight];
            }
        }
        
    }
   @catch(NSException* e)
    {
    }
    
    FSRotation rotation = (FSRotation)[_pdfViewCtrl getViewRotation];
    if (rotation == FSRotation90 || rotation == FSRotation270) {
        int tmp = width;
        width =  height;
        height = tmp;
    }
    
    CGFloat realWidth = MIN(self.cellSize.width, self.cellSize.height * width / height);
    [cell updateButtonFramesWithThumbnailWidth:realWidth];

    BOOL selected = [[self.collectionView indexPathsForSelectedItems] containsObject:indexPath];
    cell.selected = selected;
    return cell;
}

#pragma mark <FSReorderableCollectionViewDataSource>

- (void)reorderableCollectionView:(FSReorderableCollectionView *)collectionView moveItemAtIndexPaths:(NSArray<NSIndexPath *> *)sourceIndexPaths toIndex:(NSUInteger)destinationIndex {
    NSArray *array = [sourceIndexPaths valueForKey:@"item"];
    BOOL isOK = [self.pageManipulationDelegate movePagesFromIndexes:array toIndex:destinationIndex];
    if (!isOK) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailMovePage") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark <UICollectionViewDelegate>

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (!self.isEditing) {
        int pageIndex = (int) indexPath.item;
        [self.delegate thumbnailViewController:self openPage:pageIndex];
        return NO;
    } else {
        return YES;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self onCellSelectedOrDeselected];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self onCellSelectedOrDeselected];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![cell isKindOfClass:[FSThumbnailCell class]]) {
        return;
    }
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(cell.contentView.bounds.size.width / 2, cell.contentView.bounds.size.height / 2);
    [cell.contentView addSubview:spinner];
    [spinner startAnimating];
    ((FSThumbnailCell *) cell).imageView.image = nil;

    FSReorderableCollectionView *reorderCollectionView = (FSReorderableCollectionView *) collectionView;
    NSUInteger pageIndex = [reorderCollectionView getOriginalIndexPathForIndexPath:indexPath].item;
    CGSize thumbnailSize = ({
        float width = 600;
        float height = 800;
        @try {
            if (![_pdfViewCtrl isDynamicXFA]) {
                FSPDFPage *page = [self.document getPage:(int)pageIndex];
                if (![page isEmpty]) {
                    width = [page getWidth];
                    height = [page getHeight];
                }
            }else{
                FSXFAPage *page = [[_pdfViewCtrl getXFADoc] getPage:(int)pageIndex];
                if (![page isEmpty]) {
                    width = [page getWidth];
                    height = [page getHeight];
                }
            }
        }
        @catch(NSException* e)
        {}
        CGFloat aspectRatio = width / height;
        CGFloat thumbnailWidth = MAX(self.cellSize.width, self.cellSize.height * aspectRatio);
        CGFloat thumbnailHeight = thumbnailWidth / aspectRatio;
        CGSizeMake(thumbnailWidth, thumbnailHeight);
    });

    FSRotation rotation = (FSRotation)[_pdfViewCtrl getViewRotation];
    if (rotation == FSRotation90 || rotation == FSRotation270) {
        int tmp = thumbnailSize.width;
        thumbnailSize.width =  thumbnailSize.height;
        thumbnailSize.height = tmp;
    }
    
    NSBlockOperation *op;
    __weak __block NSBlockOperation *weakOp;
    __weak typeof(self) weakSelf = self;
    weakOp = op = [NSBlockOperation blockOperationWithBlock:^{
        if (weakOp.isCancelled) {
            return;
        }
        
        [weakSelf.delegate thumbnailViewController:weakSelf
                        getThumbnailForPageAtIndex:pageIndex
                                     thumbnailSize:thumbnailSize
                                         needPause:^BOOL {
                                             return !weakOp || weakOp.isCancelled;
                                         }
                                          callback:^(UIImage *thumbnailImage) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  if (!weakOp.isCancelled && [weakSelf.collectionView.visibleCells containsObject:cell]) {
                                                      ((FSThumbnailCell *) cell).imageView.image = thumbnailImage;
                                                      [spinner stopAnimating];
                                                      [spinner removeFromSuperview];
                                                  }
                                              });
                                          }];
    }];

    [self.operationQueue addOperation:op];
    if (self.operations[indexPath]) {
        [self.operations[indexPath] cancel];
    }
    self.operations[indexPath] = op;

    BOOL selected = [[self.collectionView indexPathsForSelectedItems] containsObject:indexPath];
    cell.selected = selected;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.collectionView.indexPathsForVisibleItems containsObject:indexPath]) {
        NSBlockOperation *operation = self.operations[indexPath];
        [operation cancel];
        self.operations[indexPath] = nil;
    }
}

#pragma mark <UICollectionViewDelegateFlowLayout>

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    UIEdgeInsets cellInsets = [self cellInsets];
    UIEdgeInsets sectionInset = [self sectionInsets];
    return UIEdgeInsetsMake(sectionInset.top, sectionInset.left + cellInsets.left, sectionInset.bottom, sectionInset.right + cellInsets.right);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    UIEdgeInsets cellInsets = [self cellInsets];
    return cellInsets.top + cellInsets.bottom;
    ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    UIEdgeInsets cellInsets = [self cellInsets];
    return cellInsets.left + cellInsets.right;
    ;
}

#pragma mark <FSThumbnailCellDelegate>

- (void)cell:(FSThumbnailCell *)cell rotateClockwise:(BOOL)clockwise {
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    if (indexPath) {
        [self cancelDrawingThumbnailsAtIndexPaths:@[ indexPath ]];
        if ([self.pageManipulationDelegate rotatePagesAtIndexes:@[ @(indexPath.item) ] clockwise:clockwise]) {
            BOOL selected = [self.collectionView.indexPathsForSelectedItems containsObject:indexPath];
            [self.collectionView reloadItemsAtIndexPaths:@[ indexPath ]];
            if (selected) {
                [self.collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
            }
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailRotatePage") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)deleteCell:(FSThumbnailCell *)cell {
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    if (indexPath) {
        [self deletePagesAtIndexPaths:@[ indexPath ]];
    }
}

- (void)cell:(FSThumbnailCell *)cell insertBeforeOrAfter:(BOOL)beforeOrAfter {
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    if (indexPath) {
        self.insertIndex = (beforeOrAfter ? indexPath.item : indexPath.item + 1);
        [self showInsertMenu:beforeOrAfter ? cell.insertPrevBtn : cell.insertNextBtn insertBeforeOrAfter:beforeOrAfter];
    }
}

- (void)didShowEditButtonsInCell:(FSThumbnailCell *)cell {
    for (FSThumbnailCell *visibleCell in self.collectionView.visibleCells) {
        if (visibleCell != cell) {
            [visibleCell dismissLeftBtns];
            [visibleCell dismissRightBtns];
        }
    }
}

#pragma mark <FSReorderableCollectionViewReorderDelegate>

- (void)reorderableCollectionView:(FSReorderableCollectionView *)collectionView willMoveItemsAtIndexPaths:(NSArray<NSIndexPath *> *)sourceIndexPaths toIndex:(NSUInteger)destinationIndex {
    [self cancelDrawingThumbnails];
}

- (void)reorderableCollectionView:(FSReorderableCollectionView *)collectionView didMoveItemsAtIndexPaths:(NSArray<NSIndexPath *> *)sourceIndexPaths toIndex:(NSUInteger)destinationIndex {
    __block NSUInteger maxIndex = destinationIndex;
    __block NSUInteger minIndex = destinationIndex;
    [sourceIndexPaths enumerateObjectsUsingBlock:^(NSIndexPath *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        if (obj.item > maxIndex) {
            maxIndex = obj.item;
        }
        if (obj.item < minIndex) {
            minIndex = obj.item;
        }
    }];
    [self updatePageNumberLabelsInRange:NSMakeRange(minIndex, maxIndex - minIndex + 1)];
    [self onCellSelectedOrDeselected];
}

#pragma mark event handler

- (void)setIsEditing:(BOOL)isEditing {
    if (_isEditing != isEditing) {
        _isEditing = isEditing;
        //update tool bars
        [self resetTopBar];
            self.isBottomBarHidden = !isEditing;
        //update collection view
        [self.collectionView.visibleCells enumerateObjectsUsingBlock:^(__kindof FSThumbnailCell *_Nonnull cell, NSUInteger idx, BOOL *_Nonnull stop) {
            cell.isEditing = isEditing;
        }];
        [self deselectAll];
    }
}

- (void)selectOrDeselectAll {
    int pageCount = [self getPageCount];
    BOOL isSelectAll = (self.collectionView.indexPathsForSelectedItems.count == pageCount);

    if (!isSelectAll) {
        [self selectAll];
    } else {
        [self deselectAll];
    }
}

- (void)rotateSelected {
    NSArray<NSIndexPath *> *selectedIndexPaths = self.collectionView.indexPathsForSelectedItems;
    if (selectedIndexPaths.count > 0) {
        [self cancelDrawingThumbnailsAtIndexPaths:selectedIndexPaths];
        if ([self.pageManipulationDelegate rotatePagesAtIndexes:[selectedIndexPaths valueForKey:@"item"] clockwise:YES]) {
            [self.collectionView reloadItemsAtIndexPaths:selectedIndexPaths];
            [self.collectionView performBatchUpdates:^{
                [selectedIndexPaths enumerateObjectsUsingBlock:^(NSIndexPath *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                    [self.collectionView selectItemAtIndexPath:obj animated:NO scrollPosition:UICollectionViewScrollPositionNone];
                }];
            }
                                          completion:nil];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFailRotatePage") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)deleteSelected {
    [self deletePagesAtIndexPaths:self.collectionView.indexPathsForSelectedItems];
}

- (void)extractSelected {
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Select;
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:^{
            if (destinationFolder.count == 0)
                return;
            
            __block void (^inputFileName)(NSString *path) = ^(NSString *path) {
                BOOL isDir = NO;
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if (![fileManager fileExistsAtPath:path isDirectory:&isDir]) {
                    return;
                }
                if (!isDir) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                               style:UIAlertActionStyleCancel
                                                                             handler:^(UIAlertAction *action) {
                                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                                     inputFileName([path stringByDeletingLastPathComponent]);
                                                                                 });
                                                                             }];
                        [alertController addAction:cancelAction];
                        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                    });
                    return;
                }
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kInputNewFileName") preferredStyle:UIAlertControllerStyleAlert];
                [alertController addTextFieldWithConfigurationHandler:^(UITextField *_Nonnull textField) {
                    textField.text = @"";
                }];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   UITextField *textField = alertController.textFields[0];
                                                                   NSString *newName = textField.text;
                                                                   if (newName.length < 1) {
                                                                       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:FSLocalizedForKey(@"kInputNewFileName") preferredStyle:UIAlertControllerStyleAlert];
                                                                       UIAlertAction *okAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               inputFileName(path);
                                                                           });
                                                                       }];
                                                                       [alertController addAction:okAction];
                                                                       [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                   } else if ([fileManager fileExistsAtPath:[path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", newName]]]) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kFileAlreadyExists") preferredStyle:UIAlertControllerStyleAlert];
                                                                           UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                                                                                                  style:UIAlertActionStyleCancel
                                                                                                                                handler:^(UIAlertAction *action) {
                                                                                                                                }];
                                                                           UIAlertAction *OKAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                                                                                                  style:UIAlertActionStyleDefault
                                                                                                                                handler:^(UIAlertAction *action) {
                                                                                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                                                                                        [fileManager removeItemAtPath:[path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", newName]] error:nil];
                                                                                                                                        [self extractSelectedToPath:[path stringByAppendingString:[NSString stringWithFormat:@"/%@.pdf", newName]]];
                                                                                                                                        inputFileName = nil;
                                                                                                                                    });
                                                                                                                                }];
                                                                           [alertController addAction:cancelAction];
                                                                           [alertController addAction:OKAction];
                                                                           [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                                       });
                                                                   } else {
                                                                       [self extractSelectedToPath:[path stringByAppendingString:[NSString stringWithFormat:@"/%@.pdf", newName]]];
                                                                       inputFileName = nil;
                                                                   }
                                                               }];
                [alertController addAction:cancelAction];
                [alertController addAction:action];
                [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            };
            
            inputFileName(destinationFolder[0]);
        }];
        
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    [selectDestinationNavController fs_setCustomTransitionAnimator:self->_extensionsManager.presentedVCAnimator];
    UIViewController *rootViewController = self.pdfViewCtrl.fs_viewController;
    [rootViewController presentViewController:selectDestinationNavController animated:YES completion:nil];
}

- (void)extractSelectedToPath:(NSString *)destinationPath {
    NSArray<NSNumber *> *pages = [self.collectionView.indexPathsForSelectedItems valueForKey:@"item"];
    if (pages.count == 0) {
        return;
    }
    NSArray *sortPages = [pages sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 intValue] < [obj2 intValue] ? NSOrderedAscending : NSOrderedDescending;
    }];
    //create an empty doc
    FSPDFDoc *doc = [[FSPDFDoc alloc] init];
    FSRange *pageRanges = [[FSRange alloc] init];
    for (NSUInteger i = 0; i < pages.count; i++) {
        [pageRanges addSingle:[sortPages[i] intValue]];
    }
    //import by srcDoc
    FSProgressiveState state = FSProgressiveError;
    FSProgressive* progress = nil;
    @try {
        progress = [doc startImportPages:0 src_doc:self.document flags:0 layer_name:nil page_range:pageRanges pause:nil];
        if(!progress)
            state = FSProgressiveFinished;
        else
            state = FSProgressiveToBeContinued;

    } @catch (NSException *exception) {
        NSLog(@"FSPDFDoc::startImportPages EXCEPTION NAME:%@", exception.name);
        if ([exception.name isEqualToString:@"FSErrUnsupported"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kUnsupportedDocFormat") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        }
        return;
    }

    while (state == FSProgressiveToBeContinued) {
        state = [progress resume];
    }
    if (FSProgressiveFinished != state) {
        [self convenientAlert:FSLocalizedForKey(@"kFailExtract")];
    } else {
        NSDate *date = [NSDate date];
        FSMetadata *metadata = [[FSMetadata alloc] initWithDocument:doc];
        [metadata setCreationDateTime:[Utility convert2FSDateTime:date]];
        if (![doc saveAs:destinationPath save_flags:FSPDFDocSaveFlagNormal]) {
            [self convenientAlert:FSLocalizedForKey(@"kFailExtract")];
        } else {
            [self convenientAlert:FSLocalizedForKey(@"kSuccess")];
        }
    }
    [self onCellSelectedOrDeselected];
}

- (void)duplicatePages:(void (^)(void))completionBlock {
    NSArray<NSNumber *> *selectedPageIndexes = [self.collectionView.indexPathsForSelectedItems valueForKey:@"item"];
    if (selectedPageIndexes.count == 0) {
        return;
    }
    selectedPageIndexes = [selectedPageIndexes sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    NSUInteger maxSelectedPageIndex = selectedPageIndexes.lastObject.unsignedIntegerValue;
    [self.pageManipulationDelegate insertPagesFromDocument:self.document withSourceIndexes:selectedPageIndexes flags:FSPDFDocImportFlagNormal layerName:nil atIndex:maxSelectedPageIndex + 1 success:^(NSString * _Nonnull result) {
        NSInteger cellCount = [self getPageCount] - self.collectionView.indexSetForDraggingCells.count;
        if (cellCount == 1 || [self.collectionView numberOfItemsInSection:0] == cellCount) {
            [self.collectionView reloadData];
            completionBlock ? completionBlock() : nil;
        } else {
            [self.collectionView performBatchUpdates:^{
                [self.collectionView insertItemsAtIndexPaths:indexPathsFromRange(NSMakeRange(maxSelectedPageIndex + 1, selectedPageIndexes.count))];
            }
                                          completion:^(BOOL finished) {
                                              completionBlock ? completionBlock() : nil;
                                          }];
        }
        [self updatePageNumberLabelsInRange:NSMakeRange(maxSelectedPageIndex + 1, [self getPageCount] - maxSelectedPageIndex - 1)];
    } error:^(NSString * _Nonnull errorInfo) {
        if ([errorInfo isEqualToString:@"FSErrUnsupported"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kUnsupportedDocFormat") preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:okAction];
            
            [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        }else{
            [self convenientAlert:FSLocalizedForKey(@"kFailCopy")];
            completionBlock ? completionBlock() : nil;
        }
    }];
}

#pragma mark - InsertPageConfigVCtrlDelegate

- (void)InsertPageSucceed:(FSPDFDoc *)document count:(int)count{
    

    
    NSMutableArray<NSIndexPath *> *indexPaths = [[NSMutableArray alloc] init];
    int index = (int)self.insertIndex;
    for (int i = index; i < index + count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    [self.collectionView insertItemsAtIndexPaths:indexPaths];
    [self updatePageNumberLabelsInRange:NSMakeRange(index, [self getPageCount])];
    
//    [self.collectionView performBatchUpdates:^{
//        [self.collectionView insertItemsAtIndexPaths:indexPathsFromRange(NSMakeRange(index + 1, count))];
//    }
//                                  completion:^(BOOL finished) {
//                                  }];
//    [self updatePageNumberLabelsInRange:NSMakeRange(index + 1, [self getPageCount] - index - 1)];
}

#pragma mark insert pages and images

- (void)showInsertMenu:(UIButton *)button insertBeforeOrAfter:(BOOL)beforeOrAfter {
    FSFileAndImagePicker *picker = [[FSFileAndImagePicker alloc] init];
    picker.expectedFileTypes = @[ @"pdf", @"jbig2", @"jpx", @"tif", @"gif", @"png", @"jpg", @"bmp" ];
    picker.delegate = self;
    picker.extensionsManager = self.extensionsManager;
    picker.insertPageDelegate = self;
    UIViewController *rootViewController = self.pdfViewCtrl.fs_viewController;
    [picker presentInRootViewController:rootViewController fromView:button];
}

- (void)insertPages:(NSString *)path atIndex:(int)index {
    void (^insertPagesFromLoadedDocument)(FSPDFDoc *document) = ^(FSPDFDoc *document) {
        NSArray<NSNumber *> *sourcePagesIndexes = arrayFromRange(NSMakeRange(0, [document getPageCount]));
        [self.pageManipulationDelegate insertPagesFromDocument:document withSourceIndexes:sourcePagesIndexes flags:FSPDFDocImportFlagNormal layerName:nil atIndex:index success:^(NSString * result) {
            NSMutableArray<NSIndexPath *> *indexPaths = [[NSMutableArray alloc] init];
            for (int i = index; i < index + [document getPageCount]; i++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                [indexPaths addObject:indexPath];
            }
            [self.collectionView insertItemsAtIndexPaths:indexPaths];
            [self updatePageNumberLabelsInRange:NSMakeRange(index, [self getPageCount])];
        } error:^(NSString * _Nonnull errorInfo) {
            if ([errorInfo isEqualToString:@"FSErrUnsupported"]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kUnsupportedDocFormat") preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:okAction];
                
                [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            }else{
                [self convenientAlert:FSLocalizedForKey(@"kFailInsert")];
            }
        }];
    };

    FSPDFDoc *srcDoc = [[FSPDFDoc alloc] initWithPath:path];
    [Utility tryLoadDocument:srcDoc
        withPassword:@""
        success:^(NSString *password) {
            insertPagesFromLoadedDocument(srcDoc);
        }
        error:^(NSString *description) {
            [self convenientAlert:description];
        }
        abort:nil];

    // reset selectall
    [self onCellSelectedOrDeselected];
}

- (void)insertPageFromImage:(UIImage *)image atIndex:(int)index {
    if ([self.pageManipulationDelegate insertPageFromImage:image atIndex:index]) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        [self.collectionView insertItemsAtIndexPaths:@[ indexPath ]];
        [self updatePageNumberLabelsInRange:NSMakeRange(index, [self getPageCount] - index)];
    } else {
        [self convenientAlert:FSLocalizedForKey(@"kFailInsert")];
    }
    // reset selectall
    [self onCellSelectedOrDeselected];
}

- (void)convenientAlert:(NSString *)title {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alertController addAction:cancelAction];
    [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - inner methods

- (void)onCellSelectedOrDeselected {
    int selectedCount = (int) self.collectionView.indexPathsForSelectedItems.count;
    BOOL isAllPagesSelected = (selectedCount == [self getPageCount]);
    self.selectAllItem.selected = isAllPagesSelected;

    if (self.isEditing) {
        self.titleItem.text = [NSString stringWithFormat:@"%d", selectedCount];
    }

    BOOL isAnyCellSelected = selectedCount > 0;
    self.deleteItem.enable = isAnyCellSelected;
    self.duplicateItem.enable = isAnyCellSelected;
    self.rotateItem.enable = isAnyCellSelected;
    self.extractItem.enable = isAnyCellSelected && [Utility canExtractContentsInDocument:self.document];
}

- (void)selectAll {
    int pageCount = [self getPageCount];
    for (int i = 0; i < pageCount; i++) {
        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
    assert(pageCount == self.collectionView.indexPathsForSelectedItems.count);
    [self onCellSelectedOrDeselected];
}

- (void)deselectAll {
    for (NSIndexPath *selectedIndexPath in self.collectionView.indexPathsForSelectedItems) {
        [self.collectionView deselectItemAtIndexPath:selectedIndexPath animated:NO];
    }
    assert(0 == self.collectionView.indexPathsForSelectedItems.count);
    [self onCellSelectedOrDeselected];
}

- (void)deletePagesAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths {
    if (indexPaths.count == 0) {
        [self onCellSelectedOrDeselected];
        return;
    } else if (indexPaths.count == [self getPageCount]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kNotAllowDeleteAll") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        [self onCellSelectedOrDeselected];
        return;
    }
    indexPaths = [indexPaths sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    void (^deletePages)(void) = ^{
        [indexPaths enumerateObjectsUsingBlock:^(NSIndexPath *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            [self.operations[obj] cancel];
        }];
        [self cancelDrawingThumbnails];
        if (![self.pageManipulationDelegate deletePagesAtIndexes:[indexPaths valueForKey:@"item"]]) {
            [self convenientAlert:FSLocalizedForKey(@"kFailDelete")];
            [self onCellSelectedOrDeselected];
            return;
        }
        [self.collectionView deleteItemsAtIndexPaths:indexPaths];
        [self updatePageNumberLabelsInRange:NSMakeRange(indexPaths[0].item, [self getPageCount] - indexPaths[0].item)];

        [self onCellSelectedOrDeselected];
    };
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kSureToDeletePage") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *_Nonnull action) {
                                                             [self onCellSelectedOrDeselected];
                                                         }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *_Nonnull action) {
                                                             deletePages();
                                                         }];
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)updatePageNumberLabelsInRange:(NSRange)range {
    if (range.location == NSNotFound || range.length == 0) {
        return;
    }
    for (NSUInteger i = range.location; i < range.location + range.length; i++) {
        FSThumbnailCell *cell = (FSThumbnailCell *) [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        if ([cell isKindOfClass:[FSThumbnailCell class]]) {
            cell.labelNumber.text = [NSString stringWithFormat:@"%d", (int) i + 1];
        }
    }
}

- (void)cancelDrawingThumbnails {
    [self.operationQueue cancelAllOperations];
    [self.operations removeAllObjects];
}

- (void)cancelDrawingThumbnailsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths {
    [indexPaths enumerateObjectsUsingBlock:^(NSIndexPath *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        [self.operations[obj] cancel];
        self.operations[obj] = nil;
    }];
}

#pragma mark <FSFileAndImagePickerDelegate>

- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didPickFileAtPath:(NSString *)filePath {
    [self insertPages:filePath atIndex:(int)self.insertIndex];
}

- (void)fileAndImagePicker:(FSFileAndImagePicker *)fileAndImagePicker didPickImage:(UIImage *)image imagePath:(NSString *)imagePath {
    [self insertPageFromImage:image atIndex:(int)self.insertIndex];
}

- (void)fileAndImagePickerDidCancel:(FSFileAndImagePicker *)fileAndImagePicker {
}

#pragma mark <IRotationEventListener>

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark layout sizes

- (CGSize)calculateCellSize {
    CGSize cellSize;
#define OVERVIEW_IMAGE_WIDTH_IPHONE 142.0
#define OVERVIEW_IMAGE_HEIGHT_IPHONE 177.0
#define OVERVIEW_IMAGE_WIDTH 160.0
#define OVERVIEW_IMAGE_HEIGHT 200.0
    if (DEVICE_iPHONE) {
        cellSize = CGSizeMake(OVERVIEW_IMAGE_WIDTH_IPHONE, OVERVIEW_IMAGE_HEIGHT_IPHONE);
    } else {
        cellSize = CGSizeMake(OVERVIEW_IMAGE_WIDTH, OVERVIEW_IMAGE_HEIGHT);
    }
    
    return cellSize;
}

- (UIEdgeInsets)cellInsets {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    ScreenSizeMode sizeMode = [Utility getScreenSizeMode];
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        if (DEVICE_iPHONE) {
            if (sizeMode == ScreenSizeMode_47) {
                return UIEdgeInsetsMake(5, 12, 5, 12);
            } else if (sizeMode == ScreenSizeMode_55) {
                return UIEdgeInsetsMake(5, -2, 5, -2);
            } else {
                return UIEdgeInsetsMake(5, 6, 5, 6);
            }
        } else {
            return UIEdgeInsetsMake(10, 17, 10, 10);
        }
    } else {
        if (DEVICE_iPHONE) {
            if (sizeMode == ScreenSizeMode_40) {
                return UIEdgeInsetsMake(5, 0, 5, 0);
            } else if (sizeMode == ScreenSizeMode_47) {
                return UIEdgeInsetsMake(5, 9, 5, 8);
            } else if (sizeMode == ScreenSizeMode_55) {
                return UIEdgeInsetsMake(5, 2, 5, 1);
            } else {
                return UIEdgeInsetsMake(5, 7, 5, 7);
            }
        } else {
            return UIEdgeInsetsMake(10, 20, 10, 20);
        }
    }
}

- (UIEdgeInsets)sectionInsets {
    CGFloat topInset = DEVICE_iPHONE ? 10 : 20;

    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        if (DEVICE_iPHONE) {
            ScreenSizeMode sizeMode = [Utility getScreenSizeMode];
            if (sizeMode == ScreenSizeMode_47) {
                return UIEdgeInsetsMake(topInset, 20, 44, 16);
            } else if (sizeMode == ScreenSizeMode_55) {
                return UIEdgeInsetsMake(topInset, 0, 44, 0);
            } else {
                return UIEdgeInsetsMake(topInset, 6, 44, 5);
            }
        } else {
            return UIEdgeInsetsMake(topInset, 6, 44, 5);
        }
    } else {
        if (DEVICE_iPHONE) {
            ScreenSizeMode sizeMode = [Utility getScreenSizeMode];
            if (sizeMode == ScreenSizeMode_40) {
                return UIEdgeInsetsMake(topInset, 0, 44, 0);
            } else if (sizeMode == ScreenSizeMode_47) {
                return UIEdgeInsetsMake(topInset, 15, 44, 15);
            } else if (sizeMode == ScreenSizeMode_55) {
                return UIEdgeInsetsMake(topInset, 4, 44, 4);
            } else {
                return UIEdgeInsetsMake(topInset, 5, 44, 5);
            }
        } else {
            return UIEdgeInsetsMake(topInset, 10, 44, 5);
        }
    }
}

@end

static NSArray<NSNumber *> *arrayFromRange(NSRange range) {
    if (range.location == NSNotFound || range.length == 0) {
        return nil;
    }
    NSMutableArray<NSNumber *> *array = [NSMutableArray<NSNumber *> arrayWithCapacity:range.length];
    for (NSUInteger i = range.location; i < range.location + range.length; i++) {
        [array addObject:@(i)];
    }
    return array;
}

static NSArray<NSIndexPath *> *indexPathsFromRange(NSRange range) {
    if (range.location == NSNotFound || range.length == 0) {
        return nil;
    }
    NSMutableArray<NSIndexPath *> *indexPaths = [NSMutableArray<NSIndexPath *> arrayWithCapacity:range.length];
    for (NSUInteger i = range.location; i < range.location + range.length; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    return indexPaths;
}
