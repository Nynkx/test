/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PolygonModule.h"
#import "PolygonAnnotHandler.h"
#import "PolygonToolHandler.h"
#import "Utility.h"
#import <FoxitRDK/FSPDFViewControl.h>

@interface PolygonModule ()

@property (nonatomic, weak) TbBaseItem *propertyItem;
@property (nonatomic) BOOL isPolygon;

@end

@implementation PolygonModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (NSString *)getName {
    return @"Polygon";
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _isPolygon = YES;
        [self loadModule];
        PolygonAnnotHandler *annotHandler = [[PolygonAnnotHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerAnnotHandler:annotHandler];

        PolygonToolHandler *toolHandler = [[PolygonToolHandler alloc] initWithUIExtensionsManager:extensionsManager];
        [_extensionsManager registerToolHandler:toolHandler];
        [_extensionsManager registerAnnotPropertyListener:toolHandler];

        annotHandler.minVertexDistance = 5;
        toolHandler.minVertexDistance = 5;
    }
    return self;
}

- (void)loadModule {
    [_extensionsManager registerAnnotPropertyListener:self];

    _extensionsManager.annotationToolsBar.polygonClicked = ^() {
        self.isPolygon = YES;
        [self annotItemClicked];

    };
    _extensionsManager.annotationToolsBar.cloudClicked = ^() {
        self.isPolygon = NO;
        [self annotItemClicked];

    };
}

- (void)annotItemClicked {
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    PolygonToolHandler *toolHandler = (PolygonToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Polygon];
    toolHandler.isPolygon = self.isPolygon;
    [_extensionsManager setCurrentToolHandler:toolHandler];

    [_extensionsManager.toolSetBar removeAllItems];

    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        [self->_extensionsManager setCurrentToolHandler:nil];
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    self.propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:FSAnnotPolygon]];
    [_extensionsManager.toolSetBar addItem:self.propertyItem displayPosition:Position_CENTER];

    self.propertyItem.onTapClick = ^(TbBaseItem *item) {
        CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_extensionsManager.pdfViewCtrl];
        if (DEVICE_iPHONE) {
            [self->_extensionsManager showProperty:FSAnnotPolygon rect:rect inView:self->_extensionsManager.pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:FSAnnotPolygon rect:item.contentView.bounds inView:item.contentView];
        }
    };

    [Utility showAnnotationType:self.isPolygon ? FSLocalizedForKey(@"kPolygon") : FSLocalizedForKey(@"kCloud") type:FSAnnotPolygon pdfViewCtrl:_extensionsManager.pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.propertyItem.contentView.superview);
        make.centerX.mas_equalTo(self.propertyItem.contentView.superview.mas_centerX).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
    
    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(doneItem.contentView);
        make.centerX.equalTo(self.propertyItem.contentView.superview.mas_centerX).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_extensionsManager.pdfViewCtrl];
}

#pragma mark - IAnnotPropertyListener

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotPolygon) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

@end
