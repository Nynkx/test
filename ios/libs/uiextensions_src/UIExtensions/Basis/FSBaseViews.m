/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSBaseViews.h"
#import "PrivateDefine.h"

@implementation FSView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    self.backgroundColor = ThemeViewBackgroundColor;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}

@end

@implementation FSCellView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    self.backgroundColor = ThemeCellBackgroundColor;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}

@end

@implementation FSLineIconView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = ThemePopViewDividingLineColor;
}

@end

@implementation FSSwitch


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    self.onTintColor = SwitchOnColor;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}

@end

@implementation FSLabel

- (void)awakeFromNib{
    [super awakeFromNib];
    self.textColor = BlackThemeTextColor;
}

@end

@implementation FSButton

- (void)awakeFromNib{
    [super awakeFromNib];
}

@end

@implementation FSTableView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    self.backgroundView = [[UIView alloc] init];
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [self setTableFooterView:view];
    
    self.backgroundColor = ThemeViewBackgroundColor;
    self.separatorColor = DividingLineColor ;
    
    [self setSeparatorInset:UIEdgeInsetsZero];
    [self setLayoutMargins:UIEdgeInsetsZero];
}

@end

@interface FSTableViewCell ()

@property (nonatomic, weak) FSView *customSeparator;

@end
@implementation FSTableViewCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = ThemeCellBackgroundColor;
    self.textLabel.textColor = BlackThemeTextColor;
}

- (void)setUseCustomSeparator:(BOOL)useCustomSeparator{
    if (useCustomSeparator == _useCustomSeparator) return;
    _useCustomSeparator = useCustomSeparator;
    if (useCustomSeparator) {
        FSView *customSeparator = [[FSView alloc] init];
        customSeparator.backgroundColor = DividingLineColor;
        [self.contentView addSubview:customSeparator];
        [customSeparator mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(self.contentView);
            make.height.mas_equalTo(DIVIDE_VIEW_THICKNES);
        }];
        self.customSeparator = customSeparator;
    }else{
        [self.customSeparator removeFromSuperview];
    }
}

@end




