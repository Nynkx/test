/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "FSOptionsMoreButton.h"
#import "UIImage+Extensions.h"
#import "NSString+Extensions.h"
#import "FSWeakPointerArray.h"

@interface FSOptionsMoreButton ()
@property (nonatomic, strong) UIView *itemContainer;
@property (nonatomic, strong) UIView *markView;
@end

const CGFloat kItemWidth = 44.f;
const NSInteger kConstZero = 0;
const CGFloat kAnimationDuration = 0.3;
static FSWeakPointerArray *_moreButtonPool;
@implementation FSOptionsMoreButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initCommon];
    }
    return self;
}

- (instancetype)initWithOptionPosition:(FSOptionPosition)optionPosition{
    return [[FSOptionsMoreButton alloc] initWithOptionPosition:optionPosition optionItems:@[]];
}

- (instancetype)initWithOptionPosition:(FSOptionPosition)optionPosition optionItems:(NSArray<FSOptionsMoreItem *>*)optionItems{
    self = [[FSOptionsMoreButton alloc] init];
    if (self) {
        _optionPosition = optionPosition;
        _optionItems = optionItems;
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self initCommon];
}

- (void)initCommon{
    _optionPosition = FSOptionPositionRightToLeft;
    [self addTarget:self action:@selector(optionsMoreClicked:) forControlEvents:UIControlEventTouchUpInside];
    if (!_moreButtonPool) {
        _moreButtonPool = [FSWeakPointerArray array];
    }
    [_moreButtonPool addObject:self];
}

- (UIView *)markView{
    if (!_markView) {
        _markView = [UIView new];
        [self.fs_viewController.view addSubview:_markView];
        [_markView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.equalTo(self.fs_viewController.view);
        }];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFoldOptionsMore:)];
        [_markView addGestureRecognizer:tap];
    }
    return _markView;
}

- (void)setOptionItems:(NSArray<FSOptionsMoreItem *> *)optionItems{
    if (_itemContainer) {
        [_itemContainer removeFromSuperview];
        _itemContainer = nil;
    }
    _optionItems = optionItems;

}

- (void)setup{
    
    self.itemContainer = [UIView new];
    self.itemContainer.backgroundColor = OptionMoreColor;
    FSOptionsMoreItem *lastItem = nil;
    for (int i = 0; i < self.optionItems.count; i++) {
        FSOptionsMoreItem *item = self.optionItems[i];
        [self.itemContainer addSubview:item];
        [item mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (self.optionPosition == FSOptionPositionRightToLeft) {
                make.left.mas_equalTo(lastItem ? lastItem.mas_right : item.superview.mas_left).with.offset(ITEM_MARGIN_SMALL).priorityHigh();
                if (i == self.optionItems.count - 1) {
                    make.right.mas_equalTo(lastItem.superview).with.offset(-ITEM_MARGIN_SMALL).priorityHigh();
                }
            }else if (self.optionPosition == FSOptionPositionLeftToRight){
                make.right.mas_equalTo(lastItem ? lastItem.mas_left : item.superview.mas_right).with.offset(-ITEM_MARGIN_SMALL ).priorityHigh();
                if (i == self.optionItems.count - 1) {
                    make.left.mas_equalTo(lastItem.superview).with.offset(ITEM_MARGIN_SMALL).priorityHigh();
                }
            }
            make.centerY.mas_equalTo(item.superview).priorityHigh();
            make.top.bottom.mas_greaterThanOrEqualTo(self.itemContainer).priorityHigh();
        }];
        lastItem = item;
        [item addTarget:self action:@selector(touchedItem:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)optionsMoreClicked:(UIButton *)sender{
    if (!sender.superview) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Not Implemented" userInfo:nil];
    }
    sender.selected = !sender.selected;
    [self foldOptionsMore:!sender.selected];
}

- (void)foldOptionsMore:(BOOL)isFold{
    if (!self.superview) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Not Implemented" userInfo:nil];
    }
    if (!self.itemContainer.superview) {
        [self.markView addSubview:self.itemContainer];
        [self.itemContainer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self);
            if (self.optionPosition == FSOptionPositionRightToLeft) {
                make.right.mas_equalTo(self).offset(kConstZero);
            }else if (self.optionPosition == FSOptionPositionLeftToRight){
                make.left.mas_equalTo(self).offset(kConstZero);
            }
        }];
        [self.markView layoutIfNeeded];
        CGFloat offset = self.itemContainer.frame.size.width;
        [self.itemContainer mas_updateConstraints:^(MASConstraintMaker *make) {
            if (self.optionPosition == FSOptionPositionRightToLeft) {
                make.right.mas_equalTo(self).offset(offset);
            }else if (self.optionPosition == FSOptionPositionLeftToRight){
                make.left.mas_equalTo(self).offset(-offset);
            }
        }];
        [self.markView layoutIfNeeded];
    }
    CGFloat offset = self.itemContainer.frame.size.width;
    if (isFold) {
        [self.itemContainer mas_updateConstraints:^(MASConstraintMaker *make) {
            if (self.optionPosition == FSOptionPositionRightToLeft) {
                make.right.mas_equalTo(self).offset(offset);
            }else if (self.optionPosition == FSOptionPositionLeftToRight){
                make.left.mas_equalTo(self).offset(-offset);
            }
        }];
    }else{
        self.markView.hidden = isFold;
        [self.itemContainer mas_updateConstraints:^(MASConstraintMaker *make) {
            if (self.optionPosition == FSOptionPositionRightToLeft) {
                make.right.mas_equalTo(self).offset(kConstZero);
            }else if (self.optionPosition == FSOptionPositionLeftToRight){
                make.left.mas_equalTo(self).offset(kConstZero);
            }
        }];
    }
    
    [UIView animateWithDuration:kAnimationDuration
                     animations:^{
                         [self.markView layoutIfNeeded];
                     }  completion:^(BOOL finished) {
                         if (finished && isFold) {
                             self.markView.hidden = isFold;
                         }
                     } ];
}

- (void)touchedItem:(FSOptionsMoreItem *)item{
    if (item.touchedItem) item.touchedItem(item);
    self.selected = NO;
    [self foldOptionsMore:YES];
}

- (void)tapFoldOptionsMore:(UITapGestureRecognizer *)tap{
    self.selected = NO;
    [self foldOptionsMore:YES];
    
    [_moreButtonPool enumerateObjectsUsingBlock:^(FSOptionsMoreButton *_Nonnull moreBtn, NSUInteger idx, BOOL * _Nonnull stop) {
        if (moreBtn.markView.superview == tap.view.superview) {
            CGPoint point = [tap locationInView:moreBtn];
            if (point.x >= 0 && point.y >= 0 && point.x <= moreBtn.fs_width && point.y <= moreBtn.fs_height) {
                if (moreBtn.enabled) {
                    [moreBtn foldOptionsMore:NO];
                }
                *stop = YES;
            }
        }
    }];
}
@end

@implementation FSOptionsMoreItem

+ (FSOptionsMoreItem *)createItemWithImageAndTitle:(nullable NSString *)title
                                         imageNormal:(nullable UIImage *)imageNormal
                                       imageSelected:(nullable UIImage *)imageSelected{
    FSOptionsMoreItem *item = [FSOptionsMoreItem buttonWithType:UIButtonTypeCustom];
    
    CGSize titleSize = [title fs_adaptTextWithFontSize:9.0f maxSize:CGSizeMake(200, 100)];
    
    float width = imageNormal.size.width;
    float height = imageNormal.size.height;
    item.contentMode = UIViewContentModeScaleToFill;
    [item setImage:imageNormal forState:UIControlStateNormal];
    [item setImage:[imageNormal fs_applyingAlpha:0.5] forState:UIControlStateHighlighted];
    [item setImage:[imageNormal fs_applyingAlpha:0.5] forState:UIControlStateSelected];
    
    [item setTitle:title forState:UIControlStateNormal];
    [item setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [item setTitleColor:UIColorHex_DarkMode(0x5c5c5c, 0x5c5c5c) forState:UIControlStateHighlighted];
    [item setTitleColor:UIColorHex_DarkMode(0x5c5c5c, 0x5c5c5c) forState:UIControlStateSelected];
    item.titleLabel.font = [UIFont systemFontOfSize:9];
    
    item.titleEdgeInsets = UIEdgeInsetsMake(0, -width, -height * 1.5, 0);
    item.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width);
    item.frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width + 2 : width, titleSize.height + height);
    item.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    return item;
}
@end
