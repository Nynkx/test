/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger, FSOptionPosition) {
    FSOptionPositionRightToLeft = 0,
    FSOptionPositionLeftToRight = 1
};

@class FSOptionsMoreItem;
typedef void (^TouchedItem)(FSOptionsMoreItem * nullable);
@interface FSOptionsMoreItem : UIButton

@property (nonatomic, copy) TouchedItem touchedItem;

+ (FSOptionsMoreItem *)createItemWithImageAndTitle:(nullable NSString *)title
                                         imageNormal:(nullable UIImage *)imageNormal
                                       imageSelected:(nullable UIImage *)imageSelected;
@end

@interface FSOptionsMoreButton : UIButton

@property (nonatomic, assign) FSOptionPosition optionPosition;
@property (nonatomic, copy) NSArray <FSOptionsMoreItem *>*optionItems;

- (instancetype)initWithOptionPosition:(FSOptionPosition)optionPosition;

- (instancetype)initWithOptionPosition:(FSOptionPosition)optionPosition optionItems:(NSArray<FSOptionsMoreItem *>*)optionItems;

- (void)setup;
@end

NS_ASSUME_NONNULL_END
