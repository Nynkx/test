/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FSToolbar.h"
#import "PrivateDefine.h"

@interface FSItem ()
@property (nonatomic, assign) FSImageTextRelation relation;
@property (nonatomic, strong) NSMutableDictionary *stateConfig;
@property (nonatomic, assign) BOOL backgroundImage;
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nullable, nonatomic, readwrite, strong) UILabel     *titleLabel;
@property (nullable, nonatomic, readwrite, strong) UIImageView *imageView;
@end

@implementation FSItem

+ (FSItem *)createItemWithTitle:(nullable NSString *)title{
    return [self createItemWithTitle:title
                          background:nil];
}

+ (FSItem *)createItemWithImage:(nullable UIImage *)image{
    return [self createItemWithImageAndTitle:nil
                             imageNormal:image
                           imageSelected:image
                            imageDisable:image
                              background:nil
                       imageTextRelation:FSImageTextRelationTop];
}

+ (FSItem *)createItemWithTitle:(nullable NSString *)title
                         background:(nullable UIImage *)background{
    return [self createItemWithImageAndTitle:title
                             imageNormal:nil
                           imageSelected:nil
                            imageDisable:nil
                              background:background
                       imageTextRelation:FSImageTextRelationTop];
}

+ (FSItem *)createItemWithImage:(nullable UIImage *)imageNormal
                      imageSelected:(nullable UIImage *)imageSelected
                       imageDisable:(nullable UIImage *)imageDisabled{
        return [self createItemWithImage:imageNormal
                           imageSelected:imageSelected
                            imageDisable:imageDisabled
                              background:nil];
}

+ (FSItem *)createItemWithImage:(nullable UIImage *)imageNormal
                  imageSelected:(nullable UIImage *)imageSelected
                   imageDisable:(nullable UIImage *)imageDisabled
                     background:(nullable UIImage *)background{
    return [self createItemWithImageAndTitle:nil
                             imageNormal:imageNormal
                           imageSelected:imageSelected
                            imageDisable:imageDisabled
                              background:background
                       imageTextRelation:FSImageTextRelationTop];
}

+ (FSItem *)createItemWithImageAndTitle:(nullable NSString *)title
                                imageNormal:(nullable UIImage *)imageNormal
                              imageSelected:(nullable UIImage *)imageSelected
                               imageDisable:(nullable UIImage *)imageDisabled
                          imageTextRelation:(FSImageTextRelation)imageTextRelation{
    return [self createItemWithImageAndTitle:title
                                 imageNormal:imageNormal
                               imageSelected:imageSelected
                                imageDisable:imageDisabled
                                  background:nil
                           imageTextRelation:imageTextRelation];
}

+ (FSItem *)createItemWithImageAndTitle:(nullable NSString *)title
                                imageNormal:(nullable UIImage *)imageNormal
                              imageSelected:(nullable UIImage *)imageSelected
                               imageDisable:(nullable UIImage *)imageDisabled
                                 background:(nullable UIImage *)background
                          imageTextRelation:(FSImageTextRelation)imageTextRelation{
    FSItem *item = [[FSItem alloc] initWithRrelation:imageTextRelation];
    [item setTitle:title forState:UIControlStateNormal];
    [item setImage:imageNormal forState:UIControlStateNormal];
    [item setImage:[self imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateHighlighted];
    [item setImage:imageSelected ? imageSelected : imageNormal forState:UIControlStateSelected];
    if (imageDisabled) [item setImage:imageDisabled forState:UIControlStateDisabled];
    else [item setImage:[self imageByApplyingAlpha:imageNormal alpha:0.5] forState:UIControlStateDisabled];
    [item setBackgroundImage:background forState:UIControlStateNormal | UIControlStateHighlighted | UIControlStateDisabled | UIControlStateSelected];
    return item;
}

+ (UIImage *)imageByApplyingAlpha:(UIImage *)image alpha:(CGFloat)alpha {
    if (!image) return nil;
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    CGContextSetAlpha(ctx, alpha);
    CGContextDrawImage(ctx, area, image.CGImage);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.relation = FSImageTextRelationTop;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame relation:(FSImageTextRelation)relation{
    self = [super initWithFrame:frame];
    if (self) {
        self.relation = relation;
        [self setup];
    }
    return self;
}
- (instancetype)initWithRrelation:(FSImageTextRelation)relation{
    self = [super init];
    if (self) {
        self.relation = relation;
        [self setup];
    }
    return self;
}

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        [self addSubview:_imageView];
        if (_titleLabel) {
            switch (self.relation) {
                case FSImageTextRelationLeft:{
                    [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.center.left.right.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.left.mas_equalTo(_titleLabel.mas_right);
                        make.centerY.mas_equalTo(_titleLabel).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
                case FSImageTextRelationRight:{
                    [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.center.left.right.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.right.mas_equalTo(_titleLabel.mas_left);
                        make.centerY.mas_equalTo(_titleLabel).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
                case FSImageTextRelationBottom:{
                    [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.center.left.right.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.top.centerX.mas_equalTo(self);
                        make.bottom.mas_equalTo(_titleLabel.mas_top);
                        make.centerY.mas_equalTo(_titleLabel).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
                default:{
                    [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.center.left.right.top.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.bottom.centerX.mas_equalTo(self);
                        make.top.mas_equalTo(_titleLabel.mas_bottom);
                        make.centerX.mas_equalTo(_titleLabel).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
            }
        }else{
            [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.center.left.right.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
            }];
        }
    }
    return _imageView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [self addSubview:_titleLabel];
        if (_imageView) {
            switch (self.relation) {
                case FSImageTextRelationLeft:{
                    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.left.centerY.mas_equalTo(self);
                        make.right.mas_equalTo(_imageView.mas_left);
                        make.centerY.mas_equalTo(_imageView).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
                case FSImageTextRelationRight:{
                    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.right.centerY.mas_equalTo(self);
                        make.left.mas_equalTo(_imageView.mas_right);
                        make.centerY.mas_equalTo(_imageView).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
                case FSImageTextRelationBottom:{
                    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.bottom.centerX.mas_equalTo(self);
                        make.top.mas_equalTo(_imageView.mas_bottom);
                        make.centerX.mas_equalTo(_imageView).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                }
                    break;
                default:{
                    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
                        make.top.centerX.mas_equalTo(self);
                        make.bottom.mas_equalTo(_imageView.mas_top);
                        make.centerX.mas_equalTo(_imageView).priority(MASLayoutPriorityFittingSizeLevel);
                    }];
                    break;
                }
            }
        }else{
            [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.bottom.mas_equalTo(self).priority(MASLayoutPriorityFittingSizeLevel);
            }];
        }

    }
   return _titleLabel;
}

- (void)setup{
    
    self.stateConfig = @{}.mutableCopy;
    self.backgroundImage = NO;
}

- (void)setOnTapClick:(InteractionCallBack)onTapClick{
    if (!onTapClick) return;
    _onTapClick = [onTapClick copy];
    @weakify(self)
    [self setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        @strongify(self)
        !self.onTapClick ? : self.onTapClick(self);
    }];
}

- (void)setOnLongPress:(InteractionCallBack)onLongPress{
    if (!onLongPress) return;
    _onLongPress = [onLongPress copy];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressClicked:)];
    longPress.minimumPressDuration = 0.8; //press time
    [self addGestureRecognizer:longPress];
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state{
    if (![title isKindOfClass:[NSString class]]) return;
    [self setStateConfigValue:title forState:state];
}

- (void)setTitleColor:(nullable UIColor *)color forState:(UIControlState)state{
    if (![color isKindOfClass:[UIColor class]]) return;
    [self setStateConfigValue:color forState:state];
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state{
    if (![image isKindOfClass:[UIImage class]]) return;
    _backgroundImage = NO;
    [self setStateConfigValue:image forState:state];
}

- (void)setBackgroundImage:(nullable UIImage *)image forState:(UIControlState)state{
    if (![image isKindOfClass:[UIImage class]]) return;
    if (image) {
        _backgroundImageView = [[UIImageView alloc] init];
        [self insertSubview:_backgroundImageView atIndex:0];
        [_backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(self).priorityMedium();
        }];
    }
    _backgroundImage = YES;
    [self setStateConfigValue:image forState:state];
    _backgroundImage = NO;
}

- (void)setAttributedTitle:(nullable NSAttributedString *)title forState:(UIControlState)state{
    if (![title isKindOfClass:[NSAttributedString class]]) return;
    [self setStateConfigValue:title forState:state];
}

- (NSString *)currentTitle{
    return _titleLabel.text;
}

- (NSAttributedString *)currentAttributedTitle{
    return _titleLabel.attributedText;
}

- (UIColor *)currentTitleColor{
    return _titleLabel.textColor;
}

- (UIImage *)currentImage{
    return _imageView.image;
}

- (UIImage *)currentBackgroundImage{
    return _backgroundImageView.image;
}

- (void)onLongPressClicked:(UILongPressGestureRecognizer *)gest{
    if (gest.state == UIGestureRecognizerStateBegan) {
        !self.onLongPress ? : self.onLongPress(self);
    }
}

static NSString *const titleLabel_title = @"titleLabel_title";
static NSString *const titleLabel_title_color = @"titleLabel_title_color";
static NSString *const titleLabel_attributed_text = @"titleLabel_attributed_text";
static NSString *const imageView_image = @"imageView_image";
static NSString *const background_imageView_image = @"background_imageView_image";

- (NSString *)getKeyForValue:(id)value{
    if (!value) return nil;
    NSString *key = nil;
    if ([value isKindOfClass:[NSString class]]) key = titleLabel_title;
    else if ([value isKindOfClass:[UIColor class]]) key = titleLabel_title_color;
    else if ([value isKindOfClass:[NSAttributedString class]]) key = titleLabel_attributed_text;
    else if ([value isKindOfClass:[UIImage class]] && !_backgroundImage) key = imageView_image;
    else if ([value isKindOfClass:[UIImage class]] && _backgroundImage) key = background_imageView_image;
    return key;
}

- (id)getValueWithState:(UIControlState)state forKey:(NSString *)key{
    if (!key.length) return nil;
    NSDictionary *dic = [_stateConfig valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)state]];
    for (NSString *dicKey in [dic allKeys]) {
        if ([key isEqualToString:dicKey]) {
            return [dic valueForKey:key];
        }
    }
    return nil;
}

- (void)setStateConfigValue:(id)value forState:(UIControlState)state{
    if (!value) return;
    if (state == UIControlStateNormal || ((state & UIControlStateNormal) == UIControlStateNormal && ![self getValueWithState:UIControlStateNormal forKey:[self getKeyForValue:value]])) {
        [self setStateConfigValue:value forStateKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateNormal]];
    }
    if ((state & UIControlStateHighlighted) == UIControlStateHighlighted) {
        [self setStateConfigValue:value forStateKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateHighlighted]];
    }
    if ((state & UIControlStateDisabled) == UIControlStateDisabled){
        [self setStateConfigValue:value forStateKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateDisabled]];
    }
    if ((state & UIControlStateSelected) == UIControlStateSelected) {
        [self setStateConfigValue:value forStateKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateSelected]];
    }
    [self setConfigToItem];
}

- (void)setStateConfigValue:(id)value forStateKey:(NSString *)stateKey{
    NSString *key = [self getKeyForValue:value];
    if (key) {
        NSMutableDictionary *dic = [_stateConfig valueForKey:stateKey];
        if (!dic) dic = @{}.mutableCopy;
        [dic setValue:value forKey:key];
        [_stateConfig setValue:dic forKey:stateKey];
    }
}

- (void)setConfigToItem{
    NSDictionary *dic = [_stateConfig valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateNormal]];
    if (self.selected) {
        NSDictionary *tmpDic = [_stateConfig valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateSelected]];
        if (tmpDic) dic = tmpDic;
    }
    if (self.highlighted) {
        NSDictionary *tmpDic = [_stateConfig valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateHighlighted]];
        if (tmpDic) dic = tmpDic;
    }
    if (!self.enabled) {
        NSDictionary *tmpDic = [_stateConfig valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)UIControlStateDisabled]];
        if (tmpDic) dic = tmpDic;
    }
    if (dic) {
        NSString *title = [dic valueForKey:titleLabel_title];
        if (title) [self setLabelTitle:title];
        UIColor *titleColor = [dic valueForKey:titleLabel_title_color];
        if (titleColor) _titleLabel.textColor = titleColor;
        NSAttributedString *attributed_text = [dic valueForKey:titleLabel_attributed_text];
        if (attributed_text) [self setLabelAttributed:attributed_text];
        UIImage *image = [dic valueForKey:imageView_image];
        if (image) [self setImage:image isBackground:NO];
        image = [dic valueForKey:background_imageView_image];
        if (image) [self setImage:image isBackground:YES];
    }
}

- (void)setLabelTitle:(NSString *)title{
    self.titleLabel.text = title;
}

- (void)setLabelAttributed:(NSAttributedString *)title{
    self.titleLabel.attributedText = title;
}

- (void)setImage:(UIImage *)image isBackground:(BOOL)isBackground{
    if (isBackground) _backgroundImageView.image = image;
    else self.imageView.image = image;
    
}

- (void)setEnabled:(BOOL)enabled{
    [super setEnabled:enabled];
    [self setConfigToItem];
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    [self setConfigToItem];
}

- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    [self setConfigToItem];
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    [super touchesBegan:touches withEvent:event];
    [self setConfigToItem];
}

- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event{
    [super touchesMoved:touches withEvent:event];
    [self setConfigToItem];
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event{
    [super touchesEnded:touches withEvent:event];
    [self setConfigToItem];
}

- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event{
    [super touchesCancelled:touches withEvent:event];
    [self setConfigToItem];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGSize size = CGSizeZero;
    if (self.relation == FSImageTextRelationTop ||  self.relation == FSImageTextRelationBottom) {
        if (_titleLabel.intrinsicContentSize.width > _imageView.intrinsicContentSize.width) {
            size.width = _titleLabel.intrinsicContentSize.width;
        }else{
            size.width = _imageView.intrinsicContentSize.width;
        }
        size.height = _titleLabel.intrinsicContentSize.height +_imageView.intrinsicContentSize.height;
    }else if (self.relation == FSImageTextRelationLeft ||  self.relation == FSImageTextRelationRight) {
        if (_titleLabel.intrinsicContentSize.height > _imageView.intrinsicContentSize.height) {
            size.height = _titleLabel.intrinsicContentSize.height;
        }else{
            size.height = _imageView.intrinsicContentSize.height;
        }
        size.width = _titleLabel.intrinsicContentSize.width +_imageView.intrinsicContentSize.width;
    }
    if (!CGSizeEqualToSize(size, self.fs_size) && !CGSizeEqualToSize(size, CGSizeZero)) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(size);
        }];
    }
}

@end

@interface FSToolbarItems : NSObject

@property (nonatomic, copy) NSArray *items;
@property (nonatomic, weak) FSItem *firstItem;
@property (nonatomic, weak) FSItem *lastItem;
@property (nonatomic, strong) NSLayoutConstraint *lastCenterItemRightCconstraint;
- (FSItem *)getItemForIndex:(NSUInteger)index;
- (void)addItem:(FSItem *)item;
- (BOOL)removeItem:(FSItem *)item;
@end

@interface FSToolbar ()
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *stateBar;
@property (nonatomic, strong) UIView *contentBar;
@property (nonatomic, strong) UIView *divideView;
@property (nonatomic, assign) FSToolbarStyle style;
@property (nonatomic, strong) FSToolbarItems *leftItems;
@property (nonatomic, strong) FSToolbarItems *centerItems;
@property (nonatomic, strong) FSToolbarItems *rightItems;
@property (nonatomic, strong) UILayoutGuide *centerItemsLayoutGuide;
@property (nonatomic, strong) UILayoutGuide *centerItemsLeftLayoutGuide;
@property (nonatomic, strong) UILayoutGuide *centerItemsRightLayoutGuide;
@property (nonatomic, strong) UILayoutGuide *centerItemsLastLayoutGuide;
@property (nonatomic, assign) CGFloat interval;
@property (nonatomic, assign) CGFloat margin;
@property (nonatomic, assign) BOOL even;
@end

@implementation FSToolbar

- (instancetype)init{
    self = [super init];
    if (self) {
        self.style = FSToolbarStyleDefault;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.style = FSToolbarStyleDefault;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame style:(FSToolbarStyle)style{
    self = [super initWithFrame:frame];
    if (self) {
        self.style = style;
        [self setup];
    }
    return self;
}
- (instancetype)initWithStyle:(FSToolbarStyle)style{
    self = [super init];
    if (self) {
        self.style = style;
        [self setup];
    }
    return self;
}

- (void)setup{
    //fix ios11
    [self layoutIfNeeded];
    self.barTintColor = NormalBarBackgroundColor;
    self.interval = ITEM_MARGIN_LITTLE_NORMAL;
    self.margin = ITEM_MARGIN_NORMAL ;
    self.entiretyInterval = TOOLBAR_ITEM_INVALID_MARGIN_VALUE;
    self.even = NO;
    
    if (self.style != FSToolbarStyleDefault){
        [self setContentHuggingPriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisVertical];
        
        self.contentView = [[UIView alloc] init];
        self.contentView.backgroundColor = [UIColor fs_clearColor];
        
        self.contentBar = [[UIView alloc] init];
        self.contentBar.backgroundColor = [UIColor fs_clearColor];

        [super addSubview:self.contentView];
        [self.contentView addSubview:self.contentBar];
        
        if (self.style == FSToolbarStyleTop) {
            self.stateBar = [[UIView alloc] init];
            self.stateBar.backgroundColor = [UIColor fs_clearColor];
            [self.contentView addSubview:self.stateBar];
            
            CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
            [self.stateBar mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.mas_equalTo(self.contentView);
                if (@available(iOS 11.0, *)) {} else { make.height.mas_equalTo(MIN(statusBarSize.width, statusBarSize.height)); }
                make.bottom.mas_equalTo(self.contentBar.mas_top);
            }];
        }
        
        [self.contentBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.bottom.mas_equalTo(self.contentView.fs_mas_bottom);
            if (self.style == FSToolbarStyleTop){
                make.height.mas_equalTo(NAVIGATION_BAR_HEIGHT_NORMAL);
                if (@available(iOS 11.0, *)) {make.top.mas_equalTo(self.fs_mas_top);}
            }else if (self.style == FSToolbarStyleBottom) {
                make.top.mas_equalTo(self.contentView);
                make.height.mas_equalTo(TAB_BAR_HEIGHT_NORMAL);
            }
        }];
        
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.top.mas_equalTo(self);
            if (@available(iOS 11.0, *)){
                 make.left.mas_equalTo(self.mas_safeAreaLayoutGuideLeft);
                 make.right.mas_equalTo(self.mas_safeAreaLayoutGuideRight);
            }
            else make.left.right.mas_equalTo(self);
        }];
        self.hasDivide = YES;
        self.clipsToBounds = YES;
    }else{
        self.barTintColor = nil;
        self.backgroundColor = ThemeCellBackgroundColor;
    }
}

- (void)setHasDivide:(BOOL)hasDivide{
    if (_hasDivide == hasDivide) return;
    _hasDivide = hasDivide;
    if (hasDivide) {
        UIView *divideView = [[UIView alloc] init];
        divideView.layer.zPosition = 0.01;
        self.divideView = divideView;
        if (self.style != FSToolbarStyleDefault){
            divideView.backgroundColor = BarDividingLineColor;
            [self.contentView addSubview:divideView];
            
            [divideView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(divideView.superview);
                make.height.equalTo(@(DIVIDE_TOOL_BAR_THICKNES));
                if (self.style == FSToolbarStyleTop){ make.bottom.mas_equalTo(divideView.superview); }
                else if (self.style == FSToolbarStyleBottom) { make.top.mas_equalTo(divideView.superview); }
            }];
        }else{
            divideView.backgroundColor = DividingLineColor;
            [self addSubview:divideView];
            
            [divideView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(divideView.superview);
                make.height.equalTo(@(DIVIDE_TOOL_BAR_THICKNES));
                make.bottom.mas_equalTo(divideView.superview);
            }];
        }
    }else{
        [self.divideView removeFromSuperview];
    }
}

- (FSItem *)getItemForIndex:(NSUInteger)index position:(FSItemPosition)position{
    switch (position) {
        case FSItemPositionLeft:
            return [self.leftItems getItemForIndex:index];
        case FSItemPositionCenter:
            return [self.centerItems getItemForIndex:index];
        case FSItemPositionRight:
            return [self.rightItems getItemForIndex:index];
    }
    return nil;
}

- (void)addEvenItem:(FSItem *)item{
    self.even = YES;
    [self addItem:item margin:self.interval interval:self.interval position:FSItemPositionCenter];
    self.even = NO;
}

- (void)addEvenItem:(FSItem *)item margin:(CGFloat)margin{
    [self addItem:item margin:self.margin interval:self.interval position:FSItemPositionCenter];
}
- (void)addItem:(FSItem *)item position:(FSItemPosition)position{
    if (self.entiretyInterval < 0) return [self addItem:item margin:self.margin interval:self.interval position:position];
    [self addItem:item interval:self.entiretyInterval position:position];
}

- (void)addItem:(FSItem *)item interval:(CGFloat)interval position:(FSItemPosition)position{
    [self addItem:item margin:interval interval:interval position:position];
}

- (void)addItem:(FSItem *)item margin:(CGFloat)margin interval:(CGFloat)interval position:(FSItemPosition)position{
    if (!item) @throw [NSException exceptionWithName:NSGenericException reason:@"item is nil" userInfo:nil];
    [self addSubview:item];
    switch (position) {
        case FSItemPositionLeft:{
            FSItem *lastItem = [self.leftItems lastItem];
            if (!self.leftItems) self.leftItems = [[FSToolbarItems alloc] init];
            [item mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(lastItem ? lastItem.mas_right : item.superview).offset(lastItem ? interval  : margin);
                make.centerY.mas_equalTo(lastItem ? lastItem : item.superview);
                make.size.mas_equalTo(item.fs_size);
            }];
            [self.leftItems addItem:item];
        }
            break;
        case FSItemPositionCenter:{
            if (!self.centerItems) self.centerItems = [[FSToolbarItems alloc] init];
            FSItem *lastItem = [self.centerItems lastItem];
            
            UILayoutGuide *layout = self.centerItemsLayoutGuide;
            UILayoutGuide *leftLayout = self.centerItemsLeftLayoutGuide;
            UILayoutGuide *righttLayout = self.centerItemsRightLayoutGuide;
            if (!layout) {
                self.centerItemsLayoutGuide = [[UILayoutGuide alloc] init];
                [item.superview addLayoutGuide:self.centerItemsLayoutGuide];
                layout = self.centerItemsLayoutGuide;
                
                [layout.centerXAnchor constraintEqualToAnchor:item.superview.centerXAnchor].active = YES;
                [layout.centerYAnchor constraintEqualToAnchor:item.superview.centerYAnchor].active = YES;
                [layout.topAnchor constraintEqualToAnchor:item.superview.topAnchor].active = YES;
                [layout.bottomAnchor constraintEqualToAnchor:item.superview.bottomAnchor].active = YES;
                NSLayoutConstraint *constraint = [layout.leadingAnchor constraintLessThanOrEqualToAnchor:item.superview.leadingAnchor];
                constraint.priority = 750;
                constraint.active = YES;
                constraint = [layout.trailingAnchor constraintLessThanOrEqualToAnchor:item.superview.trailingAnchor];
                constraint.priority = 750;
                constraint.active = YES;
                
                self.centerItemsLeftLayoutGuide = [[UILayoutGuide alloc] init];
                [item.superview addLayoutGuide:self.centerItemsLeftLayoutGuide];
                leftLayout = self.centerItemsLeftLayoutGuide;
                
                [self anchor:leftLayout.leadingAnchor equalToAnchor:layout.leadingAnchor];
                [self anchor:leftLayout.topAnchor equalToAnchor:layout.topAnchor];
                [self anchor:leftLayout.bottomAnchor equalToAnchor:layout.bottomAnchor];
                
                self.centerItemsRightLayoutGuide = [[UILayoutGuide alloc] init];
                [item.superview addLayoutGuide:self.centerItemsRightLayoutGuide];
                righttLayout = self.centerItemsRightLayoutGuide;
                
                [self anchor:righttLayout.trailingAnchor equalToAnchor:layout.trailingAnchor];
                [self anchor:righttLayout.topAnchor equalToAnchor:layout.topAnchor];
                [self anchor:righttLayout.bottomAnchor equalToAnchor:layout.bottomAnchor];
                
                [self anchor:leftLayout.widthAnchor equalToAnchor:righttLayout.widthAnchor];
            }
            
            if (lastItem) {
                if (self.centerItems.lastCenterItemRightCconstraint) [lastItem.superview removeConstraint:self.centerItems.lastCenterItemRightCconstraint];
                
                UILayoutGuide *middleLayou =  [[UILayoutGuide alloc] init];
                [item.superview addLayoutGuide:middleLayou];
                [middleLayou.topAnchor constraintEqualToAnchor:layout.topAnchor].active = YES;
                [middleLayou.bottomAnchor constraintEqualToAnchor:layout.bottomAnchor].active = YES;
                [self anchor:middleLayou.leadingAnchor equalToAnchor:lastItem.trailingAnchor];
                [self anchor:middleLayou.trailingAnchor equalToAnchor:item.leadingAnchor];
                
                if (self.even) {
                    [middleLayou.widthAnchor constraintEqualToAnchor:leftLayout.widthAnchor].active = YES;
                }else{
                    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:leftLayout attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:margin];
                    constraint.priority = 750;
                    constraint.active = YES;
                    [NSLayoutConstraint constraintWithItem:middleLayou attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:interval].active = YES;
                }
                if (self.centerItemsLastLayoutGuide) {
                    if (self.even) {
                        [self.centerItemsLastLayoutGuide.widthAnchor constraintEqualToAnchor:middleLayou.widthAnchor].active = YES;
                    }
                }
                self.centerItemsLastLayoutGuide = middleLayou;
            }else{
                 [item.leadingAnchor constraintEqualToAnchor:leftLayout.trailingAnchor].active = YES;
            }
            [item mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(lastItem ? lastItem : item.superview);
                make.size.mas_equalTo(item.fs_size);
            }];
            self.centerItems.lastCenterItemRightCconstraint = [item.trailingAnchor constraintEqualToAnchor:righttLayout.leadingAnchor];
            [item.superview addConstraint:self.centerItems.lastCenterItemRightCconstraint];
            [self.centerItems addItem:item];
        }
            break;
        case FSItemPositionRight:{
            if (!self.rightItems) self.rightItems = [[FSToolbarItems alloc] init];
            FSItem *lastItem = [self.rightItems lastItem];
            [item mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(lastItem ? lastItem.mas_left : item.superview).offset(lastItem ? -interval : -margin);
                make.centerY.mas_equalTo(lastItem ? lastItem : item.superview);
                make.size.mas_equalTo(item.fs_size);
            }];
            [self.rightItems addItem:item];
        }
            break;
    }
}
- (void)removeItem:(FSItem *)item position:(FSItemPosition)position{
    switch (position) {
        case FSItemPositionLeft:
            [self.leftItems removeItem:item];
        case FSItemPositionCenter:
            [self.centerItems removeItem:item];
        case FSItemPositionRight:
            [self.rightItems removeItem:item];
    }
    [item removeFromSuperview];
}
- (void)removeItemByIndex:(NSUInteger)index position:(FSItemPosition)position{
    FSItem *item = nil;
    switch (position) {
        case FSItemPositionLeft:
            item = [self.leftItems getItemForIndex:index];
            break;
        case FSItemPositionCenter:
            item = [self.centerItems getItemForIndex:index];
            break;
        case FSItemPositionRight:
            item = [self.rightItems getItemForIndex:index];
            break;
    }
    [self removeItem:item position:position];
}
- (void)removeItem:(FSItem *)item{
    FSItemPosition position = -1;
    if ([self.leftItems.items containsObject:item]) {
        position = FSItemPositionLeft;
    }
    if ([self.centerItems.items containsObject:item]) {
        position = FSItemPositionCenter;
    }
    if ([self.rightItems.items containsObject:item]) {
        position = FSItemPositionRight;
    }
    if (position == -1) @throw [NSException exceptionWithName:NSGenericException reason:@"item did not add" userInfo:nil];
    [self removeItem:item position:position];
}
- (void)removeAllItems{
    [self removeItemsByPosition:FSItemPositionLeft];
    [self removeItemsByPosition:FSItemPositionCenter];
    [self removeItemsByPosition:FSItemPositionRight];
}
- (void)removeItemsByPosition:(FSItemPosition)position{
    switch (position) {
        case FSItemPositionLeft:
            if (self.leftItems) {
                for (FSItem *item in self.leftItems.items) {
                    [item removeFromSuperview];
                }
                self.leftItems = nil;
            }
            break;
        case FSItemPositionCenter:
            if (self.centerItems) {
                for (FSItem *item in self.centerItems.items) {
                    [item removeFromSuperview];
                }
                self.centerItems = nil;
            }
            break;
        case FSItemPositionRight:
            if (self.rightItems) {
                for (FSItem *item in self.rightItems.items) {
                    [item removeFromSuperview];
                }
                self.rightItems = nil;
            }
            break;
    }
}

- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection{
    [super traitCollectionDidChange:previousTraitCollection];
    if (self.style == FSToolbarStyleTop) {
        CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
        [self.stateBar mas_updateConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *)) {} else { make.height.mas_equalTo(MIN(statusBarSize.width, statusBarSize.height)); }
        }];
    }
}

- (__kindof UIView *)viewWithTag:(NSInteger)tag{
    if (self.style != FSToolbarStyleDefault){
        return [self.contentBar viewWithTag:tag];
    }
    return [super viewWithTag:tag];
}

- (void)addSubview:(UIView *)view{
    if (self.style != FSToolbarStyleDefault){
        [self.contentBar addSubview:view];
        return;
    }
    [super addSubview:view];
}

- (void)addLayoutGuide:(UILayoutGuide *)layoutGuide{
    if (self.style != FSToolbarStyleDefault){
        [self.contentBar addLayoutGuide:layoutGuide];
        return;
    }
    [super addLayoutGuide:layoutGuide];
}

- (NSLayoutConstraint *)anchor:(NSLayoutAnchor *)anchor equalToAnchor:(NSLayoutAnchor *)toAnchor{
    return [self anchor:anchor equalToAnchor:toAnchor priority:1000];
}

- (NSLayoutConstraint *)anchor:(NSLayoutAnchor *)anchor equalToAnchor:(NSLayoutAnchor *)toAnchor priority:(NSUInteger)priority{
    NSLayoutConstraint *constraint = [anchor constraintEqualToAnchor:toAnchor];
    constraint.priority = priority;
    constraint.active = YES;
    return constraint;
}

@end


@implementation FSToolbarItems

- (instancetype)init
{
    self = [super init];
    if (self) {
        _items = [NSArray array];
    }
    return self;
}

- (FSItem *)getItemForIndex:(NSUInteger)index{
    if (index > _items.count - 1) return nil;
    return _items[index];
}

- (void)addItem:(FSItem *)item{
    NSMutableArray *tmp = [NSMutableArray arrayWithArray:_items];
    [tmp addObject:item];
    _items = tmp.copy;
}

- (BOOL)removeItem:(FSItem *)item{
    NSMutableArray *tmp = [NSMutableArray arrayWithArray:_items];
    if ([tmp containsObject:item]) {
        [tmp removeObject:item];
        _items = tmp.copy;
        return YES;
    }
    return NO;
}

- (FSItem *)firstItem{
    return [_items firstObject];
}

- (FSItem *)lastItem{
    return [_items lastObject];
}

@end
