/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <UIKit/UIKit.h>
#import "FSToolbar.h"

NS_ASSUME_NONNULL_BEGIN

@interface FSView : UIView

@end

@interface FSCellView : UIView

@end

@interface FSLineIconView : UIView

@end

@interface FSSwitch : UISwitch

@end

@interface FSButton : UIButton

@end

@interface FSLabel : UILabel

@end

@interface FSTableView : UITableView

@end

@interface FSTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL useCustomSeparator;

@end

NS_ASSUME_NONNULL_END
