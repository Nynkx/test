/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FSImageTextRelation){
    FSImageTextRelationTop,    //title top
    FSImageTextRelationLeft,   //title left
    FSImageTextRelationRight,  //title right
    FSImageTextRelationBottom, //title bottom
};

@interface FSItem : UIControl

typedef void (^InteractionCallBack)(FSItem *item);

+ (FSItem *)createItemWithTitle:(nullable NSString *)title;

+ (FSItem *)createItemWithImage:(nullable UIImage *)image;

+ (FSItem *)createItemWithImage:(nullable UIImage *)imageNormal
                      imageSelected:(nullable UIImage *)imageSelected
                       imageDisable:(nullable UIImage *)imageDisabled;

+ (FSItem *)createItemWithImage:(nullable UIImage *)imageNormal
                  imageSelected:(nullable UIImage *)imageSelected
                   imageDisable:(nullable UIImage *)imageDisabled
                     background:(nullable UIImage *)background;

+ (FSItem *)createItemWithImageAndTitle:(nullable NSString *)title
                                imageNormal:(nullable UIImage *)imageNormal
                              imageSelected:(nullable UIImage *)imageSelected
                               imageDisable:(nullable UIImage *)imageDisabled
                          imageTextRelation:(FSImageTextRelation)imageTextRelation;

+ (FSItem *)createItemWithImageAndTitle:(nullable NSString *)title
                                imageNormal:(nullable UIImage *)imageNormal
                              imageSelected:(nullable UIImage *)imageSelected
                               imageDisable:(nullable UIImage *)imageDisabled
                                 background:(nullable UIImage *)background
                          imageTextRelation:(FSImageTextRelation)imageTextRelation;

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithFrame:(CGRect)frame relation:(FSImageTextRelation)relation;
- (instancetype)initWithRrelation:(FSImageTextRelation)relation;

@property (nullable, nonatomic, readonly, strong) NSString *currentTitle;   // normal/highlighted/selected/disabled. can return nil
@property (nonatomic, readonly, strong) UIColor  *currentTitleColor;        // normal/highlighted/selected/disabled. always returns non-nil. default is white(1,1)
@property (nullable, nonatomic, readonly, strong) UIImage  *currentImage;   // normal/highlighted/selected/disabled. can return nil
@property (nullable, nonatomic, readonly, strong) UIImage  *currentBackgroundImage;   // normal/highlighted/selected/disabled. can return nil
@property (nullable, nonatomic, readonly, strong) NSAttributedString *currentAttributedTitle;  // normal/highlighted/selected/disabled. can return nil

// return title and image views. will always create them if necessary. always returns nil for system buttons
@property (nullable, nonatomic, readonly, strong) UILabel     *titleLabel;
@property (nullable, nonatomic, readonly, strong) UIImageView *imageView;

@property (nonatomic, copy) InteractionCallBack onTapClick;
@property (nonatomic, copy) InteractionCallBack onLongPress;

- (void)setTitle:(nullable NSString *)title forState:(UIControlState)state;
- (void)setTitleColor:(nullable UIColor *)color forState:(UIControlState)state; // default is nil. use opaque white
- (void)setImage:(nullable UIImage *)image forState:(UIControlState)state; // default is nil. should be same size if different for different states
- (void)setBackgroundImage:(nullable UIImage *)image forState:(UIControlState)state; // default is nil
- (void)setAttributedTitle:(nullable NSAttributedString *)title forState:(UIControlState)state; // default is nil. title is assumed to

@end

typedef NS_ENUM(NSInteger, FSToolbarStyle) {
  FSToolbarStyleDefault = 0,
  FSToolbarStyleTop = 1,
  FSToolbarStyleBottom = 2
};

typedef NS_ENUM(NSInteger, FSItemPosition) {
    FSItemPositionLeft = 0,
    FSItemPositionCenter,
    FSItemPositionRight,
};

@interface FSToolbar : UIToolbar

/**If it is negative, the default is negative based on the default interval set internally */
@property (nonatomic, assign) CGFloat entiretyInterval;

@property (nonatomic, assign) BOOL hasDivide;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithFrame:(CGRect)frame style:(FSToolbarStyle)style;
- (instancetype)initWithStyle:(FSToolbarStyle)style;

- (FSItem *)getItemForIndex:(NSUInteger)index position:(FSItemPosition)position;
- (void)addEvenItem:(FSItem *)item;
- (void)addItem:(FSItem *)item position:(FSItemPosition)position;
- (void)addItem:(FSItem *)item interval:(CGFloat)interval position:(FSItemPosition)position;
- (void)removeItem:(FSItem *)item position:(FSItemPosition)position;
- (void)removeItemByIndex:(NSUInteger)index position:(FSItemPosition)position;
- (void)removeItem:(FSItem *)item;
- (void)removeAllItems;
- (void)removeItemsByPosition:(FSItemPosition)position;

@end

NS_ASSUME_NONNULL_END
