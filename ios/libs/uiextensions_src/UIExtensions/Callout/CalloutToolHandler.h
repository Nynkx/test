/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "Common/UIExtensionsManager+Private.h"
#import "UIExtensionsManager.h"

@interface CalloutToolHandler : NSObject <IToolHandler, IFSUndoEventListener> {
    UITextView *_textView;
    FSPointF* startOrgPoint;
    FSPointF* endOrgPoint;
    FSPointF* kneeOrgPoint;
    FSRectF* textOrgRect;

    BOOL _isSaved;
    BOOL _keyboardShown;
}

@property (nonatomic, assign) FSAnnotType type;
//@property (nonatomic, assign) CGPoint freeTextStartPoint;

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager;

@end
