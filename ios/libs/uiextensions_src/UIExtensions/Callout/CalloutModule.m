/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CalloutModule.h"
#import "FtAnnotHandler.h"
#import "SelectToolHandler.h"
#import "CalloutToolHandler.h"


@interface CalloutModule ()

@property (nonatomic, weak) UIExtensionsManager *extensionsManager;

@property (nonatomic, strong) PropertyBar *propertyMenu;
//@property (nonatomic, strong) FtAnnotHandler *annotHandler;
@property (nonatomic, weak) TbBaseItem *propertyItem;
@property (nonatomic, assign) BOOL propertyIsShow;
@property (nonatomic, assign) BOOL shouldShowProperty;
//@property (nonatomic, assign) BOOL isSizeChanging;

@end

@implementation CalloutModule

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        self.extensionsManager = extensionsManager;
        [_extensionsManager registerAnnotPropertyListener:self];
        [self loadModule];
    }
    return self;
}

- (NSString *)getName {
    return @"Callout";
}

- (void)loadModule {
    CalloutToolHandler *toolHandler = [[CalloutToolHandler alloc] initWithUIExtensionsManager:_extensionsManager];
    [_extensionsManager registerToolHandler:toolHandler];
    [_extensionsManager registerUndoEventListener:toolHandler];

    FtAnnotHandler *annotHandler = [[FtAnnotHandler alloc] initWithUIExtensionsManager:_extensionsManager];
    [_extensionsManager registerAnnotHandler:annotHandler];
    //    [_extensionsManager.propertyBar registerPropertyBarListener:annotHandler];

    _extensionsManager.annotationToolsBar.calloutClicked = ^(){
        [self annotItemClicked];
    };
}

-(void)annotItemClicked
{
    SelectToolHandler *select = (SelectToolHandler *) [_extensionsManager getToolHandlerByName:Tool_Select];
    [select clearSelection];
    [_extensionsManager changeState:STATE_ANNOTTOOL];
    id<IToolHandler> toolHandler = [_extensionsManager getToolHandlerByName:Tool_Callout];
    [_extensionsManager setCurrentToolHandler:toolHandler];
    
    [_extensionsManager.toolSetBar removeAllItems];
    TbBaseItem *doneItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_done")];
    doneItem.tag = 0;
    [_extensionsManager.toolSetBar addItem:doneItem displayPosition:Position_CENTER];
    doneItem.onTapClick = ^(TbBaseItem *item) {
        if (self->_extensionsManager.currentToolHandler) {
            [self->_extensionsManager setCurrentToolHandler:nil];
        }
        [self->_extensionsManager changeState:STATE_EDIT];
    };

    TbBaseItem *propertyItem = [TbBaseItem createItemWithImage:ImageNamed(@"annotation_toolitembg")];
    propertyItem.imageNormal = nil;
    self.propertyItem = propertyItem;
    
    propertyItem.tag = 1;
    [self.propertyItem setInsideCircleColor:[_extensionsManager getPropertyBarSettingColor:FSAnnotFreeText]];
    [_extensionsManager.toolSetBar addItem:propertyItem displayPosition:Position_CENTER];
    propertyItem.onTapClick = ^(TbBaseItem *item) {
        if (DEVICE_iPHONE) {
            CGRect rect = [item.contentView convertRect:item.contentView.bounds toView:self->_extensionsManager.pdfViewCtrl];
            [self->_extensionsManager showProperty:FSAnnotFreeText rect:rect inView:self->_extensionsManager.pdfViewCtrl];
        } else {
            [self->_extensionsManager showProperty:FSAnnotFreeText rect:item.contentView.bounds inView:item.contentView];
        }
    };
    
    TbBaseItem *continueItem = nil;
    if (_extensionsManager.continueAddAnnot) {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_continue")];
    } else {
        continueItem = [TbBaseItem createItemWithImage:ImageNamed(@"annot_single")];
    }
    continueItem.tag = 3;
    [_extensionsManager.toolSetBar addItem:continueItem displayPosition:Position_CENTER];
    continueItem.onTapClick = ^(TbBaseItem *item) {
        for (UIView *view in self->_extensionsManager.pdfViewCtrl.subviews) {
            if (view.tag == 2112) {
                return;
            }
        }
        self->_extensionsManager.continueAddAnnot = !self->_extensionsManager.continueAddAnnot;
        if (self->_extensionsManager.continueAddAnnot) {
            item.imageNormal = ImageNamed(@"annot_continue");
            item.imageSelected = ImageNamed(@"annot_continue");
        } else {
            item.imageNormal = ImageNamed(@"annot_single");
            item.imageSelected = ImageNamed(@"annot_single");
        }

        [Utility showAnnotationContinue:self->_extensionsManager.continueAddAnnot pdfViewCtrl:self->_extensionsManager.pdfViewCtrl siblingSubview:self->_extensionsManager.toolSetBar.contentView];
        [self performSelector:@selector(dismissAnnotationContinue) withObject:nil afterDelay:1];
    };

    [Utility showAnnotationType:FSLocalizedForKey(@"kCallout") type:FSAnnotFreeText pdfViewCtrl:_extensionsManager.pdfViewCtrl belowSubview:_extensionsManager.toolSetBar.contentView];

    [self.propertyItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.propertyItem.contentView.superview);
        make.size.mas_equalTo(self.propertyItem.contentView.fs_size);
    }];
    
    [continueItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.left.equalTo(self.propertyItem.contentView.mas_right).offset(ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(continueItem.contentView.fs_size);
    }];
    
    [doneItem.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.propertyItem.contentView);
        make.right.equalTo(self.propertyItem.contentView.mas_left).offset(-ITEM_MARGIN_MEDIUM);
        make.size.mas_equalTo(doneItem.contentView.fs_size);
    }];
}

- (void)onPropertyBarDismiss
{
    self.propertyIsShow = NO;
}

#pragma mark <IAnnotPropertyListener>

- (void)onAnnotColorChanged:(unsigned int)color annotType:(FSAnnotType)annotType {
    if (annotType == FSAnnotFreeText && [[_extensionsManager.currentToolHandler getName] isEqualToString:Tool_Callout]) {
        [self.propertyItem setInsideCircleColor:color];
    }
}

- (void)dismissAnnotationContinue {
    [Utility dismissAnnotationContinue:_extensionsManager.pdfViewCtrl];
}

@end
