/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "CalloutToolHandler.h"
#import "StringDrawUtil.h"

#import "FreetextUtil.h"

@interface CalloutToolHandler () <UITextViewDelegate>

@property (nonatomic, strong) FSFreeText* annot;
@property (nonatomic, assign) int pageIndex;
@end

@implementation CalloutToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
    CGFloat minAnnotPageInset;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _type = FSAnnotFreeText;
        minAnnotPageInset = 5;
    }
    return self;
}

-(NSString*)getName {
    return Tool_Callout;
}

-(BOOL)isEnabled {
    return YES;
}

-(void)onActivate {
}

-(void)onDeactivate {
    [self save];
//    [_textView resignFirstResponder];
}


// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    self.pageIndex = pageIndex;
    if (self.annot) {
        [self save];
        return YES;
    }
    BOOL isEditing = (_textView && !_isSaved);
    [self save];
    _isSaved = NO;
    
    if (_extensionsManager.currentToolHandler == self) {
        UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
        CGRect pageRect = pageView.bounds;
        CGPoint startPoint = [recognizer locationInView:pageView];
        CGPoint kneePoint;
        CGPoint endPoint;
        
        
        FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:self.pageIndex];
        self.annot = [[FSFreeText alloc] initWithAnnot:[page addAnnot:FSAnnotFreeText rect:[_pdfViewCtrl convertPageViewRectToPdfRect:CGRectMake(0, 0, 1, 1) pageIndex:pageIndex]]];
        //appearance
        FSDefaultAppearance *appearance = [self.annot getDefaultAppearance];
        appearance.flags = FSDefaultAppearanceFlagFont | FSDefaultAppearanceFlagTextColor | FSDefaultAppearanceFlagFontSize;
        NSString *fontName = [_extensionsManager getAnnotFontName:FSAnnotFreeText];
        int fontID = [Utility toStandardFontID:fontName];
        if (fontID == -1) {
            [appearance setFont:[[FSFont alloc] initWithName:fontName styles:0 charset:FSFontCharsetDefault weight:0]];
        } else {
            [appearance setFont:[[FSFont alloc] initWithFont_id:fontID]];
        }
        [appearance setText_size:[_extensionsManager getAnnotFontSize:FSAnnotFreeText]];
        unsigned int color = [_extensionsManager getPropertyBarSettingColor:FSAnnotFreeText];
        [appearance setText_color:color];
        [self.annot setBorderColor:_extensionsManager.config.defaultSettings.annotations.callout.color.rgbValue];
        [self.annot setDefaultAppearance:appearance];
        
        //font
        float fontSize = [appearance getText_size] ?: 10;
        fontSize = [Utility convertWidth:fontSize fromPageViewToPDF:_pdfViewCtrl pageIndex:self.pageIndex];
        FSFont *fsfont = [appearance getFont];
        if (fsfont && ![fsfont isEmpty]) {
            fontName = [fsfont getName];
        }
        if (fontName.length == 0) {
            fontName = @"Helvetica";
        }
        UIFont *font = [self getSysFont:fontName size:fontSize];
        CGSize textSize = [Utility getTestSize:font];
        
        //set kneePoint, endPoint, textView frame
        CGFloat textViewWidth = 100.0;
        CGRect textFrame;
        if (startPoint.y < pageRect.size.height / 2.0) {
            if (startPoint.x < pageRect.size.width / 2.0) {
                kneePoint.x = startPoint.x + 50.0;
                kneePoint.y = startPoint.y + 50.0;
                endPoint.x = kneePoint.x;
                endPoint.y = kneePoint.y + 20.0;
            } else {
                kneePoint.x = startPoint.x - 50.0;
                kneePoint.y = startPoint.y + 50.0;
                endPoint.x = kneePoint.x;
                endPoint.y = kneePoint.y + 20.0;
            }
            textFrame = CGRectMake(endPoint.x - textViewWidth / 2.0, endPoint.y, textViewWidth, textSize.height);
        } else {
            if (startPoint.x < pageRect.size.width / 2.0) {
                kneePoint.x = startPoint.x + 50.0;
                kneePoint.y = startPoint.y - 50.0;
                endPoint.x = kneePoint.x;
                endPoint.y = kneePoint.y - 20.0;
            } else {
                kneePoint.x = startPoint.x - 50.0;
                kneePoint.y = startPoint.y - 50.0;
                endPoint.x = kneePoint.x;
                endPoint.y = kneePoint.y - 20.0;
            }
            textFrame = CGRectMake(endPoint.x - textViewWidth / 2.0, endPoint.y - textSize.height, textViewWidth, textSize.height);
        }
        //set text view properties
        _textView = [[UITextView alloc] initWithFrame:textFrame];
        _textView.font = font;
        [FreetextUtil setTextViewProperties:_textView UIExtensionsManager:_extensionsManager isCallout:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasShown:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        _textView.delegate = self;
        [pageView addSubview:_textView];
        [_textView becomeFirstResponder];
        
        //set annot
        CGRect innerRect = textFrame;
        CGRect borderRect = [FreetextUtil calculateBorderRectWithInnerRect:innerRect startPoint:startPoint kneePoint:kneePoint];
        
        self.annot.fsrect = [_pdfViewCtrl convertPageViewRectToPdfRect:borderRect pageIndex:pageIndex];
        self.annot.NM = [Utility getUUID];
        self.annot.author = _extensionsManager.annotAuthor;
        int opacity = [_extensionsManager getAnnotOpacity:FSAnnotFreeText];
        self.annot.opacity = opacity / 100.0f;
//        NSString *content = _textView.text;
        self.annot.contents = nil;
        self.annot.createDate = [NSDate date];
        self.annot.modifiedDate = [NSDate date];
        self.annot.subject = @"Callout";
        self.annot.intent = @"FreeTextCallout";
        self.annot.flags = FSAnnotFlagPrint;

        [self.annot setInnerRect:[_pdfViewCtrl convertPageViewRectToPdfRect:innerRect pageIndex:pageIndex]];
        FSPointFArray *points = [[FSPointFArray alloc] init];
        [points add:[_pdfViewCtrl convertPageViewPtToPdfPt:startPoint pageIndex:pageIndex]];
        [points add:[_pdfViewCtrl convertPageViewPtToPdfPt:kneePoint pageIndex:pageIndex]];
        [points add:[_pdfViewCtrl convertPageViewPtToPdfPt:endPoint pageIndex:pageIndex]];
        [self.annot setCalloutLinePoints:points];
        [self.annot setCalloutLineEndingStyle:FSMarkupEndingStyleOpenArrow];
        
        int rotation = ([page getRotation] +[_pdfViewCtrl getViewRotation]) % 4;
        rotation = rotation == 0 ? rotation : 4 - rotation;
        self.annot.rotation = rotation ;
        
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        
        if (self.annot) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:self.annot];
            [annotHandler addAnnot:self.annot addUndo:YES];
        }
        return YES;
    }
    if (isEditing) {
        return YES;
    }
    return NO;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (_extensionsManager.currentToolHandler == self) {
        if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            if (_textView) {
                UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
                CGPoint point = [gestureRecognizer locationInView:pageView];
                if (_textView == [pageView hitTest:point withEvent:nil]) {
                    return NO;
                }
            }
            return YES;  //Tap gesture to add free text by simple click
        }
    }
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

//- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {

//    if (!_textView) {
//        CGRect selfRect = self.rect;
//        CGContextSetRGBFillColor(context, 0, 0, 1, 0.3);
//        CGContextFillRect(context, selfRect);
//    }
//}

- (UIFont *)getSysFont:(NSString *)name size:(float)size {
    UIFont *font = [UIFont fontWithName:[Utility convert2SysFontString:name] size:size];
    if (!font) {
        font = [UIFont systemFontOfSize:size];
    }
    return font;
}

#pragma mark IDvTouchEventListener
- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView {
    [self save];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    UIView *pageView = [_pdfViewCtrl getPageView:self.annot.pageIndex];
    if (pageView) {
        [FreetextUtil adjustTextViewFrame:textView inPageView:pageView forMinPageInset:minAnnotPageInset];
    }
    
    CGRect refreshRect = [FreetextUtil resetCalloutAppearanceWhenTextviewChanged:_pdfViewCtrl annot:self.annot textFrame:textView.frame];
    textView.frame = [_pdfViewCtrl convertPdfRectToPageViewRect:[self.annot getInnerRect] pageIndex:self.annot.pageIndex];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_pdfViewCtrl refresh:refreshRect pageIndex:self.annot.pageIndex];
    });
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [self save];
}

#pragma mark - keyboard

- (void)keyboardWasShown:(NSNotification*)notification
{
    if (_keyboardShown)
        return;
    _keyboardShown = YES;
    [FreetextUtil afterKeyboardShown:notification textView:_textView UIExtensionsManager:_extensionsManager pageIndex:self.pageIndex];
}

- (void)keyboardWasHidden:(NSNotification*)notification
{
    _keyboardShown = NO;
    [FreetextUtil adjustBottomOffset:_pdfViewCtrl pageIndex:self.pageIndex];
}

- (void)onPageChangedFrom:(int)oldIndex to:(int)newIndex
{
    [self save];
}

- (void)save
{

    if (!self.annot) {
        return;
    }
    if (_textView && !_isSaved) {
        _isSaved = YES;
        FSAnnotAttributes *attributes = [FSAnnotAttributes attributesWithAnnot:self.annot];

        self.annot.contents = _textView.text;
        [_pdfViewCtrl lockRefresh];
        [self.annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];

        FSAnnotAttributes *newAttributes = [FSAnnotAttributes attributesWithAnnot:self.annot];
        id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:self.annot];
        [_extensionsManager addUndoItem:[UndoItem itemForUndoModifyAnnotWithOldAttributes:attributes newAttributes:newAttributes pdfViewCtrl:_pdfViewCtrl page:self.annot.getPage annotHandler:annotHandler]];

        CGRect borderRect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.annot.fsrect pageIndex:self.annot.pageIndex];
        [_pdfViewCtrl refresh:borderRect pageIndex:self.annot.pageIndex];
        
        if (!_extensionsManager.continueAddAnnot) {
            [_extensionsManager setCurrentToolHandler:nil];
            [_extensionsManager changeState:STATE_EDIT];
        }
        
        [_textView resignFirstResponder];
        [_textView removeFromSuperview];
        _textView = nil;
        self.annot = nil;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event onView:(UIView*)view
{
    if (_textView != nil) {
        CGPoint pt = [_textView convertPoint:point fromView:view];
        if (CGRectContainsPoint(_textView.bounds, pt)) {
            return _textView;
        }
    }
    return nil;
}

//-(CGPoint)rotateVec:(float)px py:(float)py ang:(float)ang isChlen:(BOOL)isChlen newLine:(float)newLen
//{
//    CGPoint point = CGPointMake(0, 0);
//    float vx = px * cosf(ang) - py * sinf(ang);
//    float vy = px * sinf(ang) + py * cosf(ang);
//    if (isChlen) {
//        float d = sqrtf(vx * vx + vy * vy);
//        vx = vx / d * newLen;
//        vy = vy / d * newLen;
//        point.x = vx;
//        point.y = vy;
//    }
//    return point;
//}

#pragma mark <IFSUndoEventListener>

- (void)onWillRedo {
    if (self.annot) {
        [self save];
    }
}

- (void)onWillUndo {
    if (self.annot) {
        [self save];
    }
}

@end
