/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PanelController.h"

typedef void (^GetBookmarksFinishHandler)(NSMutableArray *bookmark);

@interface OutlineButton : UIButton

@property (nonatomic, strong) NSIndexPath *indexPath;

@end

typedef void (^bookmarkGotoPageHandler)(int page);

/** @brief The view controller for bookmarks. */
@interface OutlineViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *arrayBookmarks;
@property (copy, nonatomic) bookmarkGotoPageHandler bookmarkGotoPageHandler;
@property (assign, nonatomic) BOOL hasParentBookmark;

- (id)initWithStyle:(UITableViewStyle)style pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl panelController:(FSPanelController *)panelController;
- (NSMutableArray *)getBookmark:(FSBookmark *)parentBookmark;
- (void)getBookmarks:(FSBookmark *)bookmark finishHandler:(GetBookmarksFinishHandler)finishHandler;
- (void)loadData:(FSBookmark *)parentBookmark;
- (void)clearData;

@end
