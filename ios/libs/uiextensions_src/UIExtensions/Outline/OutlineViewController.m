/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "OutlineViewController.h"
#import "PanelController.h"

@implementation OutlineButton

@end

@interface OutlineViewController () {
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    __weak FSPanelController *_panelController;
}

- (void)refreshInterface;

@end

@implementation OutlineViewController

@synthesize bookmarkGotoPageHandler = _bookmarkGotoPageHandler;
@synthesize hasParentBookmark;

- (id)initWithStyle:(UITableViewStyle)style pdfViewCtrl:(FSPDFViewCtrl *)pdfViewCtrl panelController:(FSPanelController *)panelController {
    self = [super initWithStyle:style];
    if (self) {
        _pdfViewCtrl = pdfViewCtrl;
        _panelController = panelController;
        _arrayBookmarks = [[NSMutableArray alloc] init];
        _bookmarkGotoPageHandler = nil;
        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
        }
        [self loadData:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshInterface];
    self.view.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.backgroundColor = ThemeViewBackgroundColor;
    self.tableView.separatorColor = DividingLineColor ;
}

- (void)viewDidLayoutSubviews {
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }

    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.hasParentBookmark ? (_arrayBookmarks.count + 1) : _arrayBookmarks.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.hasParentBookmark && indexPath.row == 0) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        TabViewSectionTextColor;
        cell.backgroundColor = ThemeViewBackgroundColor;
        UIImageView *backImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"panel_outline_comeback" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        [cell.contentView addSubview:backImageView];
        UILabel *backLabel = [[UILabel alloc] init];
        backLabel.textColor = GrayThemeTextColor;
        backLabel.text = @"...";
        [cell.contentView addSubview:backLabel];

        [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.contentView.mas_centerY);
            make.left.mas_equalTo(15);
            make.size.mas_equalTo(CGSizeMake(20, 20));
        }];
        [backLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.contentView.mas_centerY);
            make.left.equalTo(backImageView.mas_right).offset(3);
            make.size.mas_equalTo(CGSizeMake(20, 20));
        }];
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    }

    static NSString *cellIdentifier = @"outlineCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = ThemeViewBackgroundColor;
        OutlineButton *detailButton = [OutlineButton buttonWithType:UIButtonTypeCustom];
        detailButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
        [detailButton addTarget:self action:@selector(detailOutline:) forControlEvents:UIControlEventTouchUpInside];
        detailButton.frame = CGRectMake(cell.frame.size.width - 48, 0, 48, 40);
        detailButton.center = CGPointMake(detailButton.center.x, cell.frame.size.height / 2);
        detailButton.tag = 30;
        [cell addSubview:detailButton];
    }
    OutlineButton *button = (OutlineButton *) [cell viewWithTag:30];
    button.indexPath = indexPath;
    int bookmarkIndex = (int) indexPath.row;
    if (self.hasParentBookmark) {
        bookmarkIndex--;
    }
    FSBookmark *bookmarkItem = [_arrayBookmarks objectAtIndex:bookmarkIndex];
    cell.textLabel.text = [bookmarkItem getTitle];
    cell.textLabel.textColor = UIColor_DarkMode(cell.textLabel.textColor, ThemeTextColor_Dark);
    [cell.textLabel setFont:[UIFont systemFontOfSize:17]];
    BOOL hasChild = [[bookmarkItem getFirstChild] isEmpty] ? NO : YES;
    button.hidden = !hasChild;
    cell.accessoryType = hasChild ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
            TabViewSectionTextColor;
    cell.backgroundColor = ThemeViewBackgroundColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.hasParentBookmark && indexPath.row == 0) //go to parent outline
    {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        _panelController.isHidden = YES;
        FSBookmark *bookmarkItem = [_arrayBookmarks objectAtIndex:self.hasParentBookmark ? (indexPath.row - 1) : indexPath.row];
        FSDestination *dest = [bookmarkItem getDestination];
        if (!dest || [dest isEmpty])
            return;
        if (dest.getZoomMode == FSDestinationZoomFitPage) {
           [_pdfViewCtrl gotoPage:[dest getPageIndex:_pdfViewCtrl.currentDoc] animated:NO];
        }else{
            FSPointF *point = [[FSPointF alloc] initWithX:[dest getLeft] Y:[dest getTop]];
            [_pdfViewCtrl gotoPage:[dest getPageIndex:_pdfViewCtrl.currentDoc] withDocPoint:point animated:NO];
        }
    }
}

- (void)detailOutline:(id)sender {
    OutlineButton *button = (OutlineButton *) sender;
    OutlineViewController *subOutlineViewCtrl = [[OutlineViewController alloc] initWithStyle:UITableViewStylePlain pdfViewCtrl:_pdfViewCtrl panelController:_panelController];
    subOutlineViewCtrl.hasParentBookmark = YES;
    FSBookmark *bookmarkItem = [_arrayBookmarks objectAtIndex:self.hasParentBookmark ? (button.indexPath.row - 1) : button.indexPath.row];
    [subOutlineViewCtrl loadData:bookmarkItem];
    subOutlineViewCtrl.bookmarkGotoPageHandler = self.bookmarkGotoPageHandler;
    [self.navigationController pushViewController:subOutlineViewCtrl animated:YES];
}

#pragma mark - methods
/** @brief Receive all the children of specified bookmark. */
- (NSMutableArray *)getBookmark:(FSBookmark *)parentBookmark {
    if (!parentBookmark || [parentBookmark isEmpty]) {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    FSBookmark *bmChild = [parentBookmark getFirstChild];
    if (bmChild && ![bmChild isEmpty]) {
        [array addObject:bmChild];
        while (true) {
            if ([bmChild isLastChild]) {
                break;
            }
            bmChild = [bmChild getNextSibling];
            if (!bmChild || [bmChild isEmpty]) {
                break;
            }
            [array addObject:bmChild];
        }
    }
    return array;
}

- (void)getBookmarks:(FSBookmark *)bookmark finishHandler:(GetBookmarksFinishHandler)finishHandler {
    NSMutableArray *bookmarks = nil;
    if (!bookmark || [bookmark isEmpty]) {
        bookmark = [[_pdfViewCtrl getDoc] getRootBookmark];
    }
    @try {
        bookmarks = [self getBookmark:bookmark];
    } @catch (NSException *_) {
        bookmarks = nil;
    }
    if (finishHandler) {
        finishHandler(bookmarks);
    }
}

- (void)loadData:(FSBookmark *)parentBookmark {
    [self getBookmarks:parentBookmark
        finishHandler:^(NSMutableArray *bookmark) {
            self->_arrayBookmarks = bookmark;
            [self.tableView reloadData];
        }];
}

- (void)clearData {
    [_arrayBookmarks removeAllObjects];
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self.tableView reloadData];
}

#pragma mark - Private methods

- (void)refreshInterface {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [self.tableView setTableFooterView:view];
}

@end
