/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "TextboxToolHandler.h"
#import "StringDrawUtil.h"

#import "FreetextUtil.h"

@interface TextboxToolHandler () <UITextViewDelegate>

@property (nonatomic, assign) int pageIndex;
@property (nonatomic, assign) BOOL pageIsAlreadyExist;
@property (nonatomic, strong) FSPointF *startPoint;
@property (nonatomic, strong) FSPointF *endPoint;
@property (nonatomic, assign) CGRect rect;
@property (nonatomic, strong) FSRectF *dibRect;

@property (nonatomic, assign) BOOL isPanCreate;
@end

@implementation TextboxToolHandler {
    __weak UIExtensionsManager *_extensionsManager;
    __weak FSPDFViewCtrl *_pdfViewCtrl;
    TaskServer *_taskServer;
    CGFloat minAnnotPageInset;
}

- (instancetype)initWithUIExtensionsManager:(UIExtensionsManager *)extensionsManager {
    self = [super init];
    if (self) {
        _extensionsManager = extensionsManager;
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _taskServer = _extensionsManager.taskServer;
        _pageIsAlreadyExist = NO;
        _type = FSAnnotFreeText;
        minAnnotPageInset = 5;
    }
    return self;
}

- (NSString *)getName {
    return Tool_Textbox;
}

- (BOOL)isEnabled {
    return YES;
}

- (void)onActivate {
}

- (void)onDeactivate {
    [self save];
}

//set textView font and properties
- (UIFont*)setTextViewFont:(int)pageIndex
{
    float fontSize = [_extensionsManager getAnnotFontSize:FSAnnotFreeText];
    fontSize = [Utility convertWidth:fontSize fromPageViewToPDF:_pdfViewCtrl pageIndex:pageIndex];
    NSString *fontName = [_extensionsManager getAnnotFontName:FSAnnotFreeText];
    UIFont *font = [self getSysFont:fontName size:fontSize];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:fontSize];
    }
    return font;
}

// PageView Gesture+Touch
- (BOOL)onPageViewLongPress:(int)pageIndex recognizer:(UILongPressGestureRecognizer *)recognizer {
    return NO;
}

- (BOOL)onPageViewTap:(int)pageIndex recognizer:(UITapGestureRecognizer *)recognizer {
    if (!self.pageIsAlreadyExist) {
        self.pageIndex = pageIndex;
        self.pageIsAlreadyExist = YES;
    }
    [self save];
    _isSaved = NO;

    if (_extensionsManager.currentToolHandler == self) {
        CGPoint point = CGPointZero;
        UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
        if (recognizer) {
            point = [recognizer locationInView:pageView];
        } else {
            point = _freeTextStartPoint;
        }
        
        UIFont* font = [self setTextViewFont:pageIndex];
        CGSize testSize = [Utility getTestSize:font];

        CGFloat pageViewWidth = [_pdfViewCtrl getPageView:pageIndex].frame.size.width;
        if(DEVICE_iPAD)
            _textView = [[UITextView alloc] initWithFrame:CGRectMake(point.x, point.y, pageViewWidth - point.x >= 300 ? 300 : pageViewWidth - point.x, testSize.height)];
        else
           _textView = [[UITextView alloc] initWithFrame:CGRectMake(point.x, point.y, pageViewWidth - point.x >= 100 ? 100 : pageViewWidth - point.x, testSize.height)];
        
        _textView.font = font;
        
        [self adjustTextViewFrame:_textView inPageView:pageView forMinPageInset:minAnnotPageInset];
    
        //set text view properties
        [FreetextUtil setTextViewProperties:_textView UIExtensionsManager:_extensionsManager isCallout:NO];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasShown:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
        [pageView addSubview:_textView];
        _textView.delegate = self;
        [_textView becomeFirstResponder];
    }
    return YES;
}

- (BOOL)onPageViewPan:(int)pageIndex recognizer:(UIPanGestureRecognizer *)recognizer {
    id<IAnnotHandler> annotHandler = nil;
    FSAnnot *annot = _extensionsManager.currentAnnot;
    if (annot != nil && ![annot isEmpty]) {
        annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            if ([annotHandler onPageViewShouldBegin:pageIndex recognizer:recognizer annot:annot]) {
                [annotHandler onPageViewPan:pageIndex recognizer:(UIPanGestureRecognizer*)recognizer annot:annot];
                return YES;
            } else {
                _extensionsManager.currentAnnot = nil;
            }
        } else {
            [annotHandler onPageViewPan:pageIndex recognizer:(UIPanGestureRecognizer*)recognizer annot:annot];
            return YES;
        }
    }
    UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
    CGPoint point = [recognizer locationInView:pageView];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [self save];
        self.pageIndex = pageIndex;
        self.startPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (pageIndex != self.pageIndex) {
            return NO;
        }
        
        self.endPoint = [_pdfViewCtrl convertPageViewPtToPdfPt:point pageIndex:pageIndex];
        self.dibRect = [Utility convertToFSRect:self.startPoint p2:self.endPoint];
        CGRect rect = [_pdfViewCtrl convertPdfRectToPageViewRect:self.dibRect pageIndex:pageIndex];
        rect = CGRectIntersection(rect, pageView.bounds);
        [_pdfViewCtrl refresh:CGRectUnion(rect, self.rect) pageIndex:pageIndex needRender:NO];
        self.rect = rect;
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        if (pageIndex != self.pageIndex) {
            pageIndex = self.pageIndex;
            pageView = [_pdfViewCtrl getPageView:pageIndex];
            point = [recognizer locationInView:pageView];
        }

        if (!self.pageIsAlreadyExist) {
            self.pageIndex = pageIndex;
            self.pageIsAlreadyExist = YES;
        }
        
        CGRect rect = self.rect;
        [self save];
        _isSaved = NO;
        self.rect = rect;
        
        if (_extensionsManager.currentToolHandler == self) {
            _textView = [[UITextView alloc] initWithFrame:CGRectMake(self.rect.origin.x, self.rect.origin.y, self.rect.size.width, self.rect.size.height)];
            [_pdfViewCtrl refresh:_textView.frame pageIndex:pageIndex needRender:NO];
            
            UIFont *annotFont = [self setTextViewFont:pageIndex];
            _textView.font = annotFont;
            //set text view properties
            [FreetextUtil setTextViewProperties:_textView UIExtensionsManager:_extensionsManager isCallout:NO];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(keyboardWasShown:)
                                                         name:UIKeyboardDidShowNotification
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(keyboardWasHidden:)
                                                         name:UIKeyboardWillHideNotification
                                                       object:nil];
            [pageView addSubview:_textView];
            _textView.delegate = self;
            [_textView becomeFirstResponder];
            self.isPanCreate = YES;
            return YES;
        }
        return NO;
    }
    return NO;
}

- (BOOL)onPageViewShouldBegin:(int)pageIndex recognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (_extensionsManager.currentToolHandler == self) {
        if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            if (_textView) {
                UIView *pageView = [_pdfViewCtrl getPageView:pageIndex];
                CGPoint point = [gestureRecognizer locationInView:pageView];
                if (_textView == [pageView hitTest:point withEvent:nil]) {
                    return NO;
                }
            }

            return YES; //Tap gesture to add free text by simple click
        }
    }
    return NO;
}

- (BOOL)onPageViewTouchesBegan:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesMoved:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesEnded:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)onPageViewTouchesCancelled:(int)pageIndex touches:(NSSet *)touches withEvent:(UIEvent *)event {
    return NO;
}

- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (_extensionsManager.currentToolHandler != self) {
        return;
    }
    if (!_textView) {
        CGRect selfRect = self.rect;
        CGContextSetRGBFillColor(context, 0, 0, 1, 0.3);
        CGContextFillRect(context, selfRect);
    }
}

- (UIFont *)getSysFont:(NSString *)name size:(float)size {
    UIFont *font = [UIFont fontWithName:[Utility convert2SysFontString:name] size:size];
    if (!font) {
        font = [UIFont systemFontOfSize:size];
    }
    return font;
}

#pragma mark IDvTouchEventListener
- (void)onScrollViewWillBeginZooming:(UIScrollView *)scrollView {
    [self save];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    UIView *pageView = [_pdfViewCtrl getPageView:self.pageIndex];
    if (pageView) {
        CGRect frame = textView.frame;
        CGSize constraintSize = CGSizeMake(frame.size.width, MAXFLOAT);
        CGSize size = [textView sizeThatFits:constraintSize];
        if (self.isPanCreate) {
            if (size.height < frame.size.height) {
                size.height = frame.size.height;
            }
        }

        textView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, size.height);
        if (textView.frame.size.height >= (CGRectGetHeight(pageView.frame) - 20)) {
            [textView endEditing:YES];
        }
        if (textView.frame.size.width >= (CGRectGetWidth(pageView.frame) - 20)) {
            [textView endEditing:YES];
        }
        [self adjustTextViewFrame:textView inPageView:pageView forMinPageInset:minAnnotPageInset];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self save];
}

- (void)adjustTextViewFrame:(UITextView *)textView inPageView:(UIView *)pageView forMinPageInset:(CGFloat)inset {
    CGRect bounds = CGRectInset(pageView.bounds, inset, inset);
    if (!CGRectIntersectsRect(textView.frame, bounds)) {
        return;
    }
    if (CGRectGetMinX(textView.frame) < CGRectGetMinX(bounds)) {
        CGPoint center = textView.center;
        center.x += CGRectGetMinX(bounds) - CGRectGetMinX(textView.frame);
        textView.center = center;
    }
    if (CGRectGetMaxX(textView.frame) > CGRectGetMaxX(bounds)) {
        CGPoint center = textView.center;
        center.x -= CGRectGetMaxX(textView.frame) - CGRectGetMaxX(bounds);
        textView.center = center;
    }
    if (CGRectGetMinY(textView.frame) < CGRectGetMinY(bounds)) {
        CGPoint center = textView.center;
        center.y += CGRectGetMinY(bounds) - CGRectGetMinY(textView.frame);
        textView.center = center;
    }
    if (CGRectGetMaxY(textView.frame) > CGRectGetMaxY(bounds)) {
        CGPoint center = textView.center;
        center.y -= CGRectGetMaxY(textView.frame) - CGRectGetMaxY(bounds);
        textView.center = center;
    }
}

#pragma mark - keyboard

- (void)keyboardWasShown:(NSNotification *)notification {
    if (_keyboardShown) {
        return;
    }
    _keyboardShown = YES;
    
    [FreetextUtil afterKeyboardShown:notification textView:_textView UIExtensionsManager:_extensionsManager pageIndex:self.pageIndex];
}

- (void)keyboardWasHidden:(NSNotification *)notification {
    _keyboardShown = NO;
    
    [FreetextUtil adjustBottomOffset:_pdfViewCtrl pageIndex:self.pageIndex];
}

- (void)onPageChangedFrom:(int)oldIndex to:(int)newIndex {
    [self save];
}

- (void)save {
    if (_textView && !_isSaved) {
        _isSaved = YES;

        CGRect textFrame = _textView.frame;
        NSString *content = _textView.text;
        
        FSRectF *rect = [_pdfViewCtrl convertPageViewRectToPdfRect:textFrame pageIndex:self.pageIndex];
        FSPDFPage *page = [_pdfViewCtrl.currentDoc getPage:self.pageIndex];
        if (!page || [page isEmpty]) {
            return;
        }
        FSFreeText *annot = [[FSFreeText alloc] initWithAnnot:[page addAnnot:FSAnnotFreeText rect:rect]];
        annot.NM = [Utility getUUID];
        annot.author = _extensionsManager.annotAuthor;
        
        [annot setDefaultAppearance:({
            FSDefaultAppearance *appearance = [annot getDefaultAppearance];
            appearance.flags = FSDefaultAppearanceFlagFont | FSDefaultAppearanceFlagTextColor | FSDefaultAppearanceFlagFontSize;
            NSString *fontName = [_extensionsManager getAnnotFontName:FSAnnotFreeText];
            int fontID = [Utility toStandardFontID:fontName];
            if (fontID == -1) {
                [appearance setFont:[[FSFont alloc] initWithName:fontName styles:0 charset:FSFontCharsetDefault weight:0]];
            } else {
                [appearance setFont:[[FSFont alloc] initWithFont_id:fontID]];
            }
            [appearance setText_size:[_extensionsManager getAnnotFontSize:FSAnnotFreeText]];
            unsigned int color = [_extensionsManager getPropertyBarSettingColor:FSAnnotFreeText];
            [appearance setText_color:color];
            appearance;
        })];
        int opacity = [_extensionsManager getAnnotOpacity:FSAnnotFreeText];
        annot.opacity = opacity / 100.0f;
        annot.contents = content;
        annot.createDate = [NSDate date];
        annot.modifiedDate = [NSDate date];
        annot.subject = @"Textbox";
        annot.flags = FSAnnotFlagPrint;
        [annot setBorderColor:_extensionsManager.config.defaultSettings.annotations.textbox.color.rgbValue];
        
        int rotation = ([page getRotation] +[_pdfViewCtrl getViewRotation]) % 4;
        rotation = rotation == 0 ? rotation : 4 - rotation;
        annot.rotation = rotation ;
        
        [_pdfViewCtrl lockRefresh];
        [annot resetAppearanceStream];
        [_pdfViewCtrl unlockRefresh];
        // move annot if exceed page edge, as the annot size may be changed after reset ap (especially for Chinese char, the line interspace is changed)
        {
            FSRectF *rect = annot.fsrect;
            if (rect.bottom < 0) {
                rect.top -= rect.bottom;
                rect.bottom = 0;
                annot.fsrect = rect;
                [_pdfViewCtrl lockRefresh];
                [annot resetAppearanceStream];
                [_pdfViewCtrl unlockRefresh];
            }
        }
        if (annot && ![annot isEmpty]) {
            id<IAnnotHandler> annotHandler = [_extensionsManager getAnnotHandlerByAnnot:annot];
            [annotHandler addAnnot:annot addUndo:YES];
        }

        [_textView resignFirstResponder];
        [_textView removeFromSuperview];
        _textView = nil;
        self.rect = CGRectZero;
        self.isPanCreate = NO;
        self.pageIsAlreadyExist = NO;

        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

// useless ?
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event onView:(UIView *)view {
    if (_textView != nil) {
        CGPoint pt = [_textView convertPoint:point fromView:view];
        if (CGRectContainsPoint(_textView.bounds, pt)) {
            return _textView;
        }
    }
    return nil;
}

@end
