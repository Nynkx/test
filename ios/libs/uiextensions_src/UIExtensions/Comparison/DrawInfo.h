/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <Foundation/Foundation.h>
#import <FoxitRDK/FSPDFViewControl.h>

const static int RESULTTYPE_DELETE = 1;
const static int RESULTTYPE_INSERT = 2;
const static int RESULTTYPE_REPLACEMENT = 3;

@interface DrawInfo : NSObject

@property (nonatomic,assign)int pageIndex;
@property (nonatomic,assign)int resultType;
@property (nonatomic,strong)NSString *filterType;
@property (nonatomic,strong)FSRectF *rectF;

@end
