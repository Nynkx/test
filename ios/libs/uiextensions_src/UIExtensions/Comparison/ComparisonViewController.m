/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "ComparisonViewController.h"
#import "PropertyBar.h"
#import "FileSelectDestinationViewController.h"
#import "MBProgressHUD.h"
#import "FSNavigationController.h"


#define CompareModeOverlay 10000000
#define CompareModeAbreast 10000001
#define CompareModeBtnWidth 150
#define CompareColorOld 20000000
#define CompareColorNew 20000001
#define CompareResultInfoBase 30000000
#define CompareResultInfoCompared 30000001

@interface ComparisonViewController ()

@property (nonatomic, strong) UIBarButtonItem *buttonCancel;
@property (nonatomic, strong) UIBarButtonItem *buttonDone;
@property (nonatomic,strong) UIButton *selectedBtn;
@property (nonatomic, strong) PropertyBar *propertyBar;
@property (nonatomic, strong) NSMutableDictionary *selectedColorDict;
@property (nonatomic, assign) int selectedColorBtnTag;

@property (nonatomic, strong) NSMutableDictionary *tempFilesDict;
@property (nonatomic, strong) UIView *containterView;

@property (nonatomic, strong) NSString *oldFilePwd;
@property (nonatomic, strong) NSString *cnewFilePwd;

@end

@implementation ComparisonViewController

#pragma mark - Initialization
- (instancetype)init {
    if (self = [super init]) {
        self.selectedFileArr = [NSMutableArray array];
        self.selectedFilePwdDict = [NSMutableDictionary dictionary];
        
        self.propertyBar = [[PropertyBar alloc] initWithPDFViewController:nil extensionsManager:nil];
        self.selectedColorDict = @{ @(CompareColorOld):@(0xB9534E),@(CompareColorNew):@(0x82C84F)}.mutableCopy;
        
        self.tempFilesDict = @{}.mutableCopy;
        self.oldFilePwd = nil;
        self.cnewFilePwd = nil;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSArray *result = [self.selectedFileArr sortedArrayUsingComparator:^NSComparisonResult(NSString *p1, NSString *p2){
        return [[self getModifyTime:p1] compare:[self getModifyTime:p2]];
    }];
    self.selectedFileArr = [[NSMutableArray alloc] initWithArray:result];
    
    self.view.backgroundColor = ThemeViewBackgroundColor;
    
    
    UIScrollView *verticalScrollView = [[UIScrollView alloc] init];
    [self.view addSubview:verticalScrollView];
    [verticalScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.mas_equalTo(self.view);
    }];

    self.containterView = [[UIView alloc] init];
    [verticalScrollView addSubview:self.containterView];
    
    [self.containterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(verticalScrollView);
        make.width.equalTo(verticalScrollView);
    }];
    
    [self initNavigationBar];
    [self initContentView];
    
}

-(NSDate *)getModifyTime:(NSString *)filePath {
    NSError *error = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&error];
    NSDate *fileModDate = [fileAttributes objectForKey:NSFileModificationDate];
    return fileModDate;
}

#pragma mark - Configure NavigationBar
- (void)initNavigationBar {
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLeft.frame = CGRectMake(0.0, 0.0, 65.0, 32);
    buttonLeft.titleLabel.font = [UIFont systemFontOfSize:DEVICE_iPHONE ? 15.0f : 18.0f];
    [buttonLeft setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [buttonLeft setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    [buttonLeft addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.buttonCancel = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    [self.navigationItem setLeftBarButtonItem:self.buttonCancel];
    [self.buttonCancel setEnabled:YES];

    self.buttonDone = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self.buttonDone setTintColor:WhiteThemeNavBarTextColor];
    [self.buttonDone setEnabled:YES];
    
    [self.navigationItem setRightBarButtonItem:self.buttonDone];
    self.navigationController.navigationBar.barTintColor = ThemeNavBarColor;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : WhiteThemeNavBarTextColor};
    
    [self.buttonDone setTitle:FSLocalizedForKey(@"kOK")];
    
    self.title = FSLocalizedForKey(@"kComparisonVCTitle");
}

#pragma mark - Configure content view
- (void)initContentView {
    
    // compare mode
    UILabel *comapreItemTitle = [[UILabel alloc] init];
    comapreItemTitle.numberOfLines = 0;
    comapreItemTitle.text = FSLocalizedForKey(@"kCompareItemTitle");
    comapreItemTitle.font = [UIFont systemFontOfSize:14.0f];
    comapreItemTitle.textColor = BlackThemeTextColor;
    [self.containterView addSubview:comapreItemTitle];
    
    CGSize titleSize = [Utility getTextSize:FSLocalizedForKey(@"kCompareItemTitle") fontSize:20.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
    
    [comapreItemTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.mas_equalTo(self.containterView.mas_safeAreaLayoutGuideTop).offset(10);
        } else {
            make.top.mas_equalTo(self.containterView.mas_top).offset(10);
        }
        make.left.mas_equalTo(self.containterView).offset(20);
        
        make.width.mas_equalTo(titleSize.width);
        make.height.mas_equalTo(titleSize.height);
    }];
    
    UIImage *btnNormalImg = [UIImage imageNamed:@"comparisonNormal" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIImage *btnSelectImg = [UIImage imageNamed:@"comparisonSelected" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    BOOL needOverlay = NO;
    if (needOverlay) {
        UIImage *overlayButtonImg = [UIImage imageNamed:@"comparisonOverlay" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];

        UIButton *overlayButton = [self createButtonWithTitle:FSLocalizedForKey(@"kCompareButtonOverlay") image:overlayButtonImg normalImg:btnNormalImg selectedImg:btnSelectImg];
        overlayButton.tag = CompareModeOverlay;
        [overlayButton addTarget:self action:@selector(onTapCompareModebtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.containterView addSubview:overlayButton];
        
        [overlayButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(comapreItemTitle.mas_bottom).offset(15);
            make.left.mas_equalTo(comapreItemTitle).offset( (SCREENWIDTH - CompareModeBtnWidth * 2 - 40) / 3 );
            make.width.mas_equalTo(CompareModeBtnWidth);
            make.height.mas_equalTo(CompareModeBtnWidth);
        }];
    }
    
    UIImage *abreastButtonImg = [UIImage imageNamed:@"comparisonAbreast" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];

    UIButton *abreastButton = [self createButtonWithTitle:FSLocalizedForKey(@"kCompareButtonAbreast") image:abreastButtonImg normalImg:btnNormalImg selectedImg:btnSelectImg];
    abreastButton.tag = CompareModeAbreast;
    [abreastButton addTarget:self action:@selector(onTapCompareModebtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.containterView addSubview:abreastButton];
    
    [abreastButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(comapreItemTitle.mas_bottom).offset(15);
        make.left.mas_equalTo(self.containterView.mas_centerX).offset(-CompareModeBtnWidth / 2);
//        make.left.mas_equalTo(overlayButton.mas_right).offset((SCREENWIDTH - CompareModeBtnWidth * 2 - 40) / 3);
        
        make.width.mas_equalTo(CompareModeBtnWidth);
        make.height.mas_equalTo(CompareModeBtnWidth);
    }];

    self.selectedBtn = abreastButton;
    self.selectedBtn.selected = YES;
    
    NSString *oldFileName = [[self.selectedFileArr[0] lastPathComponent] stringByDeletingPathExtension];
    UIView *lastview = [self createFileItemInfo:FSLocalizedForKey(@"kCompareFileInfoOld") filename:oldFileName defaultColor:(UInt32)self.selectedColorDict[@(CompareColorOld)] lastView:abreastButton btnTag:CompareColorOld];
    
    NSString *newFileName = [[self.selectedFileArr[1] lastPathComponent] stringByDeletingPathExtension];
    UIView *lastedview = [self createFileItemInfo:FSLocalizedForKey(@"kCompareFileInfoNew") filename:newFileName defaultColor:(UInt32)self.selectedColorDict[@(CompareColorNew)] lastView:lastview btnTag:CompareColorNew];

    [self.containterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lastedview.mas_bottom).offset(20);
    }];
}

-(UIView *)createFileItemInfo:(NSString *)title filename:(NSString *)fileName defaultColor:(UInt32)defaultColor lastView:(UIView *)lastview btnTag:(int)btnTag {
    UILabel *fileItemTitle = [[UILabel alloc] init];
    fileItemTitle.numberOfLines = 0;
    fileItemTitle.text = FSLocalizedForKey(title);
    fileItemTitle.font = [UIFont systemFontOfSize:14.0f];
    fileItemTitle.textColor = BlackThemeTextColor;
    [self.containterView addSubview:fileItemTitle];
    
    CGSize titleSize = [Utility getTextSize:FSLocalizedForKey(title) fontSize:14.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
    
    [fileItemTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lastview.mas_bottom).offset(30);
        make.left.mas_equalTo(self.view).offset(20);
        
        make.width.mas_equalTo(titleSize.width);
        make.height.mas_equalTo(titleSize.height);
    }];
    
    CGSize filenameSize = [Utility getTextSize:FSLocalizedForKey(fileName) fontSize:14.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
    UIButton *filenameBtn = [[UIButton alloc] init];
    UIFont *textFont = [UIFont systemFontOfSize:14.f];
    filenameBtn.titleLabel.font = textFont;
    filenameBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    filenameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    filenameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [filenameBtn setTitle:fileName forState:UIControlStateNormal];
    [filenameBtn setTitleColor:UIColorHex(0x179cd8) forState:UIControlStateNormal];
    [filenameBtn setTitleColor:UIColorHex(0x179cd8) forState:UIControlStateHighlighted];
    [filenameBtn setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
    [filenameBtn addTarget:self action:@selector(onTapCompareFileNameBtn:) forControlEvents:UIControlEventTouchUpInside];
    filenameBtn.tag = btnTag;
    [self.containterView addSubview:filenameBtn];
    
    [filenameBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(20);
        make.top.mas_equalTo(fileItemTitle.mas_bottom).offset(20);
        
        make.width.mas_equalTo(filenameSize.width + 10);
        make.height.mas_equalTo(filenameSize.height);
    }];
    
    if (self.selectedBtn.tag == CompareModeOverlay) {
        UIButton *colorbtn = [[UIButton alloc] init];
//        UIColor *btnbackColor = [UIColor colorWithRGB:defaultColor];

        [colorbtn setBackgroundColor:[UIColor colorWithRGB:defaultColor]];
        [colorbtn addTarget:self action:@selector(onTapCompareColorBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:colorbtn];
        colorbtn.tag = btnTag;
        
        [colorbtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.view.mas_right).offset(-20);
            make.top.mas_equalTo(filenameBtn.mas_top).offset(-10);
            
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(40);
        }];
        
        UILabel *colorTitle = [[UILabel alloc] init];
        colorTitle.numberOfLines = 0;
        colorTitle.text = FSLocalizedForKey(@"kCompareFileSelectedColor");
        colorTitle.font = [UIFont systemFontOfSize:14.0f];
        colorTitle.textColor = BlackThemeTextColor;
        [self.view addSubview:colorTitle];
        
        CGSize colorTitleSize = [Utility getTextSize:FSLocalizedForKey(@"kCompareFileSelectedColor") fontSize:14.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
        
        [colorTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(filenameBtn.mas_top);
            make.right.mas_equalTo(colorbtn.mas_left).offset(-20);
            
            make.width.mas_equalTo(colorTitleSize.width);
            make.height.mas_equalTo(colorTitleSize.height);
        }];
    }
    
    return filenameBtn;
}

#pragma mark create button
- (UIButton *)createButtonWithTitle:(NSString *)title image:(UIImage *)image normalImg:(UIImage *)btnNormalImg selectedImg:(UIImage *)btnSelectImg{
    UIFont *textFont = [UIFont systemFontOfSize:14.f];
    CGSize titleSize = [Utility getTextSize:title fontSize:textFont.pointSize maxSize:CGSizeMake(400, 100)];
    float width = image.size.width ;
    float height = image.size.height ;
    CGRect frame = CGRectMake(0, 0, titleSize.width > width ? titleSize.width : width, height + titleSize.height);
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [button setEnlargedEdge:3];
    
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = textFont;
    [button setTitleColor:BlackThemeTextColor forState:UIControlStateNormal];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [button setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
    
    [button setImage:image forState:UIControlStateNormal];
//    UIImage *translucentImage = [Utility imageByApplyingAlpha:image alpha:0.5];
//    [button setImage:translucentImage forState:UIControlStateHighlighted];
//    [button setImage:translucentImage forState:UIControlStateDisabled];
    [button setBackgroundImage:btnNormalImg forState:UIControlStateNormal];
    [button setBackgroundImage:btnSelectImg forState:UIControlStateHighlighted];
    [button setBackgroundImage:btnSelectImg forState:UIControlStateSelected];
    button.adjustsImageWhenHighlighted = NO;
    
    float leftpadding = 10;
    float toppadding = 10;
    
    if (image.size.width > CompareModeBtnWidth) {
        [button setImageEdgeInsets:UIEdgeInsetsMake(toppadding, leftpadding, titleSize.height + toppadding, leftpadding)];
    }else{
        [button setImageEdgeInsets:UIEdgeInsetsMake(-button.titleLabel.intrinsicContentSize.height, 0, 0, -button.titleLabel.intrinsicContentSize.width)];
    }
    
    [button setTitleEdgeInsets:UIEdgeInsetsMake(button.currentImage.size.height + toppadding, -button.currentImage.size.width, 0, 0)];

    return button;
}
#pragma mark - clear tmp file
- (BOOL)clearCacheWithPrefix:(NSString *)prefix {
    NSString *cacheFilePath = NSTemporaryDirectory();
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *subChildPath = [fileManager subpathsOfDirectoryAtPath:cacheFilePath error:nil];
    NSError *error = nil;
    for (NSString *subPath in subChildPath) {
        NSString *path = [cacheFilePath stringByAppendingFormat:@"%@", subPath];
        NSString *filename = [path lastPathComponent];
        if ([filename hasPrefix:prefix]) {
            [fileManager removeItemAtPath:path error:&error];
            if (error) {
                NSLog(@"delete file faild");
            }
        }
    }
    return YES;
}

#pragma mark - Action methods
- (BOOL)removeEncrypt:(FSPDFDoc *)pdfDoc filePath:(NSString *)filePath{
    BOOL isOK = NO;
    NSString *decryptedPDFPath = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    @autoreleasepool {
        isOK = [pdfDoc removeSecurity];
        if (isOK) {
            NSString *fileName = [filePath lastPathComponent];
            decryptedPDFPath = [NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"_decryped_%@", fileName]];
            if ([fileManager fileExistsAtPath:decryptedPDFPath isDirectory:nil]) {
                [fileManager removeItemAtPath:decryptedPDFPath error:nil];
            }
            isOK = [pdfDoc saveAs:decryptedPDFPath save_flags:FSPDFDocSaveFlagNormal];
        }
        if (isOK) {
            isOK = [fileManager removeItemAtPath:filePath error:nil];
            
            if (isOK) {
                isOK = [fileManager moveItemAtPath:decryptedPDFPath toPath:filePath error:nil];
            }
        }
    }
    
    return isOK;
}

- (void)doneAction {
    if (![FSLibrary hasModuleLicenseRight:FSModuleNameComparison]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:@"You are not authorized to use this add-on module， please contact us for upgrading your license. " preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    [self.buttonCancel setEnabled:NO];
    [self.buttonDone setEnabled:NO];
    
    NSString *oldFilepath = [self.selectedFileArr objectAtIndex:0];
    NSString *newFilepath = [self.selectedFileArr objectAtIndex:1];
    if ([oldFilepath isEqualToString:newFilepath]) {
        [self.view fs_showHUDMessage:FSLocalizedForKey(@"kCompareNotSomeFile") afterHideDelay:1.5];
        [self.buttonCancel setEnabled:YES];
        [self.buttonDone setEnabled:YES];
        return ;
    }
    
    __block NSMutableDictionary *comapreResultInfos = @{}.mutableCopy;
    comapreResultInfos[@"compareMode"] = @(self.selectedBtn.tag);
    comapreResultInfos[@"compareColor"] = self.selectedColorDict;
    comapreResultInfos[@"compareFiles"] = self.selectedFileArr;
    
    //remove files
    [self clearCacheWithPrefix:@"new"];
    [self clearCacheWithPrefix:@"old"];
    self.oldFilePwd = nil;
    self.cnewFilePwd = nil;
    
    // load file with password
    FSPDFDoc *oldDoc = [[FSPDFDoc alloc] initWithPath:oldFilepath];
    self.oldFilePwd = [self.selectedFilePwdDict objectForKey:oldFilepath];
    [oldDoc load:self.oldFilePwd];
//    int oldPageCount = [oldDoc getPageCount];
    
    FSPDFDoc *newDoc = [[FSPDFDoc alloc] initWithPath:newFilepath];
    self.cnewFilePwd = [self.selectedFilePwdDict objectForKey:newFilepath];
    [newDoc load:self.cnewFilePwd];
//    int newPageCount = [newDoc getPageCount];
    
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.detailsLabelText =  FSLocalizedForKey(@"kCompareing");
    
    // check compare module key and generate ComparedDoc
    @try {
        FSComparison *comparison = [[FSComparison alloc] initWithBase_doc:oldDoc compared_doc:newDoc];
        if ([comparison isEmpty]) return;
    } @catch (NSException *exception) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([exception.name isEqualToString:@"FSErrInvalidLicense"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:@"You are not authorized to use this add-on module， please contact us for upgrading your license. " preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
        }
        [self.buttonCancel setEnabled:YES];
        [self.buttonDone setEnabled:YES];
        return;
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        BOOL hasError = NO;
        
        FSPDFDoc *poldDoc = [[FSPDFDoc alloc] initWithPath:oldFilepath];
        [poldDoc load:self.oldFilePwd];
        
        FSPDFDoc *pnewDoc = [[FSPDFDoc alloc] initWithPath:newFilepath];
        [pnewDoc load:self.cnewFilePwd];
        
        @try {
            FSComparison *comparison = [[FSComparison alloc] initWithBase_doc:poldDoc compared_doc:pnewDoc];
            
            FSPDFDoc *doc = [comparison generateComparedDoc:FSComparisonCompareTypeAll];
            
            NSString *tempDirectory = NSTemporaryDirectory();
            NSString *compareResultpdf = [tempDirectory stringByAppendingString:@"compareResult.pdf"];
            
            [doc saveAs:compareResultpdf save_flags:FSPDFDocSaveFlagNormal];
            
            comapreResultInfos[@"compareResultFilePath"] = compareResultpdf;
        } @catch (NSException *exception) {
            hasError = YES;
        }
        
        if (hasError) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kUnknownError") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
            
            [self.buttonCancel setEnabled:YES];
            [self.buttonDone setEnabled:YES];
            
            return ;
        }
        
        [self.buttonDone setEnabled:YES];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [[self.navigationController.viewControllers objectAtIndex:0] dismissViewControllerAnimated:YES
                                                                                        completion:^{
                                                                                            if (self.doneHandler) {
                                                                                                self.doneHandler(self,comapreResultInfos);
                                                                                            }
                                                                                        }];
        
        
    });
}

-(NSString *)drawAnnotToComareResultPdf:(NSString *)compareResultPath resultDicts:(NSMutableDictionary *)resultDicts {
    FSPDFDoc *compareResultPdf = [[FSPDFDoc alloc] initWithPath:compareResultPath];
    
    FSErrorCode errorcode = [compareResultPdf load:self.oldFilePwd];
    if (errorcode != FSErrSuccess) {
        errorcode = [compareResultPdf load:self.cnewFilePwd];
    }
    
    int pageCount = [compareResultPdf getPageCount] / 2;
    
    for (int m = 0 ; m < pageCount; m++) {
        @autoreleasepool{
            
            int leftIndex = 2 * m;
            int rightIndex = 2 * m + 1;
            
            FSCompareResults *compareResult = [resultDicts objectForKey:@(m)];
            
            FSPDFPage *leftPage = [compareResultPdf getPage:leftIndex];
            [leftPage flatten:NO options:FSPDFPageFlattenAll];
            unsigned long baseResultInfoSize = [compareResult.base_doc_results getSize];
            
            for (int i = 0; i < baseResultInfoSize; i++)
            {
                FSCompareResultInfo *item = [compareResult.base_doc_results getAt:i];
                FSCompareResultInfoCompareResultType type = item.type;
                
                if ([[self getDeleteCompareResultTypeArr] containsObject:@(type)])
                {
                    NSString* res_string = [NSString stringWithFormat:@"\"%@\"", item.diff_contents];
                    [self createTextMarkup:leftPage rects:item.rect_array color:0xE23224 type:FSAnnotStrikeOut compareContents:res_string compareType:[NSString stringWithFormat:@"Compare : %@",FSLocalizedForKey(@"kComapreContainerLegendDelete")]];
                }
                else if ([[self getInsertCompareResultTypeArr] containsObject:@(type)] )
                {
                    NSString* res_string = [NSString stringWithFormat:@"\"%@\"", item.diff_contents];
                    [self createNote:leftPage rects:item.rect_array color:0x4F8DEE type:CompareResultInfoBase compareContents:res_string compareType:[NSString stringWithFormat:@"Compare : %@",FSLocalizedForKey(@"kComapreContainerLegendInsert")]];
                }
                else if ([[self getReplaceCompareResultTypeArr] containsObject:@(type)] )
                {
                    NSString* res_string = [NSString stringWithFormat:@"[%@]: \"%@\"\r\n[%@]: \"%@\"",FSLocalizedForKey(@"kCompareFileInfoOld"),item.diff_contents,FSLocalizedForKey(@"kCompareFileInfoNew"),[compareResult.compared_doc_results getAt:i].diff_contents];
                    [self createTextMarkup:leftPage rects:item.rect_array color:0xEE8F00 type:FSAnnotSquiggly compareContents:res_string compareType:[NSString stringWithFormat:@"Compare : %@",FSLocalizedForKey(@"kComapreContainerLegendReplace")]];
                }
            }
            
            FSPDFPage *rightPage = [compareResultPdf getPage:rightIndex];
            [rightPage flatten:NO options:FSPDFPageFlattenAll];
            unsigned long comparedResultInfoSize = [compareResult.compared_doc_results getSize];
            
            for (int i = 0; i < comparedResultInfoSize; i++)
            {
                FSCompareResultInfo *item = [compareResult.compared_doc_results getAt:i];
                FSCompareResultInfoCompareResultType type = item.type;
                
                if ([[self getDeleteCompareResultTypeArr] containsObject:@(type)])
                {
                    NSString* res_string = [NSString stringWithFormat:@"\"%@\"", item.diff_contents];
                    [self createNote:rightPage rects:item.rect_array color:0xE23224 type:CompareResultInfoCompared compareContents:res_string compareType:[NSString stringWithFormat:@"Compare : %@",FSLocalizedForKey(@"kComapreContainerLegendDelete")]];
                }
                else if ([[self getInsertCompareResultTypeArr] containsObject:@(type)] )
                {
                    NSString* res_string = [NSString stringWithFormat:@"\"%@\"", item.diff_contents];
                    [self createTextMarkup:rightPage rects:item.rect_array color:0x4F8DEE type:FSAnnotUnderline compareContents:res_string compareType:[NSString stringWithFormat:@"Compare : %@",FSLocalizedForKey(@"kComapreContainerLegendInsert")]];
                }
                else if ([[self getReplaceCompareResultTypeArr] containsObject:@(type)] )
                {
                    NSString* res_string = [NSString stringWithFormat:@"[%@]: \"%@\"\r\n[%@]: \"%@\"",FSLocalizedForKey(@"kCompareFileInfoOld"),[compareResult.base_doc_results getAt:i].diff_contents,FSLocalizedForKey(@"kCompareFileInfoNew"),item.diff_contents];
                    [self createTextMarkup:rightPage rects:item.rect_array color:0xEE8F00 type:FSAnnotSquiggly compareContents:res_string compareType:[NSString stringWithFormat:@"Compare : %@",FSLocalizedForKey(@"kComapreContainerLegendReplace")]];
                }
            }
            
        }
    }
    
    NSString *tempDirectory = NSTemporaryDirectory();
    NSString *tmpcompareResultpdf = [tempDirectory stringByAppendingString:@"compareResult_compared.pdf"];
    
    [compareResultPdf saveAs:tmpcompareResultpdf save_flags:FSPDFDocSaveFlagIncremental];
    
    return tmpcompareResultpdf;
}

-(void)fillTwoDocument:(FSPDFDoc *)oldDoc newDocument:(FSPDFDoc *)newDoc completion:(void (^ __nullable)(void))completion{
    int oldPageCount = [oldDoc getPageCount];
    int newPageCount = [newDoc getPageCount];
    
    NSString *tempDirectory = NSTemporaryDirectory();
    NSString *oldTmpPath = [tempDirectory stringByAppendingString:@"old.pdf"];
    NSString *newTmpPath = [tempDirectory stringByAppendingString:@"new.pdf"];
    if (oldPageCount > newPageCount) {
        @try {
            for (int i = newPageCount  ; i < oldPageCount; i ++) {
                @autoreleasepool{
                    FSPDFPage *page = nil;
                    page = [newDoc insertPageSize:i size:FSPDFPageSizeA3];
                    
                    [page setRotation:FSRotation0];
                    
                    if (!page.isParsed) {
                        FSProgressive *progressive = [page startParse:FSPDFPageParsePageNormal pause:nil is_reparse:NO];
                        FSProgressiveState proState = FSProgressiveToBeContinued;
                        while (proState == FSProgressiveToBeContinued) {
                            proState = [progressive resume];
                        }
                        
                        if (proState != FSProgressiveFinished){
                            return;
                        }
                    }
                }
                
            }
            
        } @catch (NSException *exception) {
            return;
        }
    }
    [newDoc saveAs:newTmpPath save_flags:FSPDFDocSaveFlagNormal];
    
    if (oldPageCount < newPageCount) {
        @try {
            for (int i = oldPageCount ; i < newPageCount; i ++) {
                @autoreleasepool{
                    FSPDFPage *page = nil;
                    page = [oldDoc insertPageSize:i size:FSPDFPageSizeA3];
                    
                    [page setRotation:FSRotation0];
                    
                    if (!page.isParsed) {
                        FSProgressive *progressive = [page startParse:FSPDFPageParsePageNormal pause:nil is_reparse:NO];
                        FSProgressiveState proState = FSProgressiveToBeContinued;
                        while (proState == FSProgressiveToBeContinued) {
                            proState = [progressive resume];
                        }
                        
                        if (proState != FSProgressiveFinished){
                            return;
                        }
                    }
                }
                
            }
            
        } @catch (NSException *exception) {
            return;
        }
    }
    [oldDoc saveAs:oldTmpPath save_flags:FSPDFDocSaveFlagNormal];
    
    completion();
}

- (FSRectF *)normalizePDFRect:(FSRectF *)rectF {
    if (rectF.left > rectF.right) {
        float tmp = rectF.left;
        rectF.left = rectF.right;
        rectF.right = tmp;
    }
    
    if (rectF.top < rectF.bottom) {
        float tmp = rectF.top;
        rectF.top = rectF.bottom;
        rectF.bottom = tmp;
    }
    
    if (rectF.left == rectF.right) rectF.right += 1;
    if (rectF.top == rectF.bottom) rectF.top += 1;
    return rectF;
}

-(void)createNote:(FSPDFPage *)page rects:(FSRectFArray *)rects color:(UInt32)color type:(int)type compareContents:(NSString *)compareContents compareType:(NSString *)compareType {
    FSRectF *rcNote = [[FSRectF alloc] init];
    int rectSize = [rects getSize];
    if (rectSize > 0) {
        FSRectF *item = [rects getAt:0];
        rcNote.left = item.left;
        rcNote.top = item.top ;
        rcNote.right = item.right ;
        rcNote.bottom = item.bottom ;
        
        rcNote = [self normalizePDFRect:rcNote];
    }
    
    FSNote *note = [[FSNote alloc] initWithAnnot:[page addAnnot:FSAnnotNote rect:rcNote]];
    [note setContent:compareContents];
    [note setBorderColor:color];
    [note setSubject:@"Note"];
    [note setTitle:compareType];
    note.flags = FSAnnotFlagPrint | FSAnnotFlagNoZoom | FSAnnotFlagNoRotate;
    
    NSDate *now = [NSDate date];
    FSDateTime *time = [Utility convert2FSDateTime:now];
    [note setCreationDateTime:time];
    [note setModifiedDateTime:time];
    [note setUniqueID:[Utility getUUID]];

    if (type == CompareResultInfoBase) {
        [note setIconName:@"Insert"];
    }else{
        [note setIconName:@"Cross"];
    }
    
    [note resetAppearanceStream];
}

-(void)createTextMarkup:(FSPDFPage *)page rects:(FSRectFArray *)rects color:(UInt32)color type:(FSAnnotType)type compareContents:(NSString *)compareContents compareType:(NSString *)compareType{
    FSRectF *annotRect = [[FSRectF alloc] init];
    FSTextMarkup *textMarkup = [[FSTextMarkup alloc] initWithAnnot:[page addAnnot:type rect:annotRect]];
    [textMarkup setContent:compareContents];
    
    FSQuadPointsArray *quadPointsArr = [FSQuadPointsArray new];
    int rectSize = [rects getSize];
    for (int i=0; i < rectSize; i++) {

        FSRectF *item = [rects getAt:i];
        FSQuadPoints *quadPoints = [FSQuadPoints new];

        FSPointF* point = [FSPointF new];
        [point set:item.left y:item.top];
        quadPoints.first = point;
        [point set:item.right y:item.top];
        quadPoints.second = point;
        [point set:item.left y:item.bottom];
        quadPoints.third = point;
        [point set:item.right y:item.bottom];
        quadPoints.fourth = point;

        [quadPointsArr add:quadPoints];
    }
    [textMarkup setQuadPoints:quadPointsArr];
    
    [textMarkup setBorderColor:color];
    
    [textMarkup setTitle:compareType];
    
    NSDate *now = [NSDate date];
    FSDateTime *time = [Utility convert2FSDateTime:now];
    [textMarkup setCreationDateTime:time];
    [textMarkup setModifiedDateTime:time];
    [textMarkup setUniqueID:[Utility getUUID]];
    textMarkup.flags = FSAnnotFlagPrint;
    
    if (type == FSAnnotSquiggly) {
        textMarkup.subject = @"Squiggly";
    } else if (type == FSAnnotStrikeOut) {
        textMarkup.subject = @"Strikeout";
    } else if (type == FSAnnotUnderline) {
        textMarkup.subject = @"Underline";
    }
    
    [textMarkup resetAppearanceStream];
}

- (void)cancelAction {
    [self.selectedFileArr removeAllObjects];
    self.selectedFileArr = nil;
    
    if (self.cancelHandler) {
        self.cancelHandler(self);
    }
    [[self.navigationController.viewControllers objectAtIndex:0] dismissViewControllerAnimated:YES
                                                                                    completion:^{
                                                                                        
                                                                                    }];
}

- (void)onTapCompareModebtn:(UIButton *)btn {
    if (btn != self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
}

-(void)onTapCompareFileNameBtn:(UIButton *)btn {
    FileSelectDestinationViewController *selectDestination = [[FileSelectDestinationViewController alloc] init];
    selectDestination.isRootFileDirectory = YES;
    selectDestination.fileOperatingMode = FileListMode_Import;
    selectDestination.expectFileType = @[@"*"];
    [selectDestination loadFilesWithPath:DOCUMENT_PATH];
    selectDestination.isNeedGetPassword = YES;

    selectDestination.operatingHandler = ^(FileSelectDestinationViewController *controller, NSArray *destinationFolder) {
        [controller dismissViewControllerAnimated:YES completion:^{
            
            int replaceIndex = btn.tag == CompareColorOld ? 0 : 1;
            [self.selectedFilePwdDict removeObjectForKey:self.selectedFileArr[replaceIndex]];
            
            [self.selectedFilePwdDict setValue:controller.filePasswordDict[destinationFolder[0]] forKey:destinationFolder[0]];
            

            NSString *fileName = [[destinationFolder[0] lastPathComponent] stringByDeletingPathExtension];
            CGSize titleSize = [Utility getTextSize:fileName fontSize:14.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
            [btn mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(titleSize.width + 10);
            }];
            [btn setTitle:fileName forState:UIControlStateNormal];
            [self.selectedFileArr replaceObjectAtIndex:replaceIndex withObject:destinationFolder[0]];
        }];
    };
    selectDestination.cancelHandler = ^(FileSelectDestinationViewController *controller) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    FSNavigationController *selectDestinationNavController = [[FSNavigationController alloc] initWithRootViewController:selectDestination];
    if (DEVICE_iPAD) {
        selectDestinationNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        selectDestinationNavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    }
    [[Utility getTopMostViewController] presentViewController:selectDestinationNavController animated:YES completion:nil];
}

- (void)onTapCompareColorBtn:(UIButton *)btn {
    self.selectedColorBtnTag = (int)btn.tag;
    NSArray *colors = @[@0xff6633,@0xff00ff,@0xffcc00,@0x00ffff,@0x00ff00,@0xffff00,@0xff0000,@0x993399,@0xcc99ff,@0xff99cc,@0x0000ff,@0x66cc33,@0x000000,@0x333333,@0x666666,@0x999999,@0xcccccc,@0xffffff];
    [self.propertyBar setColors:colors];
    [self.propertyBar resetBySupportedItems:PROPERTY_COLOR frame:CGRectMake(0, SCREENHEIGHT, SCREENWIDTH, 500)];

    [self.propertyBar setProperty:PROPERTY_COLOR intValue:(int)self.selectedColorDict[@(self.selectedColorBtnTag)]];
    [self.propertyBar addListener:(id)self];
    
    if (DEVICE_iPHONE) {
        CGRect rect = [btn convertRect:btn.bounds toView:self.view];
        [self.propertyBar  showPropertyBar:rect inView:self.view viewsCanMove:nil];
    } else {
        [self.propertyBar  showPropertyBar:btn.bounds inView:btn viewsCanMove:nil];
    }
}

#pragma mark -IPropertyValueChangedListener
- (void)onProperty:(long)property changedFrom:(NSValue *)oldValue to:(NSValue *)newValue {
    if (property == PROPERTY_COLOR) {
        int color = ((NSNumber *) newValue).intValue;
        self.selectedColorDict[@(self.selectedColorBtnTag)] = @(color);
        
        UIButton *colorbtn = (UIButton *)[self.view viewWithTag:self.selectedColorBtnTag];
        [colorbtn setBackgroundColor:[UIColor colorWithRGB:color]];
    }
}

#pragma mark - compareType array
-(NSArray *)getDeleteCompareResultTypeArr {
    NSArray *deleteArr = @[
                           @(FSCompareResultInfoCompareResultTypeDeleteText),
                           @(FSCompareResultInfoCompareResultTypeDeleteImage),
                           @(FSCompareResultInfoCompareResultTypeDeletePath),
                           @(FSCompareResultInfoCompareResultTypeDeleteShading),
                           @(FSCompareResultInfoCompareResultTypeDeleteAnnot)
                           ];
    return deleteArr;
}

-(NSArray *)getInsertCompareResultTypeArr {
    NSArray *insertArr = @[
                           @(FSCompareResultInfoCompareResultTypeInsertText),
                           @(FSCompareResultInfoCompareResultTypeInsertImage),
                           @(FSCompareResultInfoCompareResultTypeInsertPath),
                           @(FSCompareResultInfoCompareResultTypeInsertShading),
                           @(FSCompareResultInfoCompareResultTypeInsertAnnot)
                           ];
    return insertArr;
}

-(NSArray *)getReplaceCompareResultTypeArr {
    NSArray *replaceArr = @[
                            @(FSCompareResultInfoCompareResultTypeReplaceText),
                            @(FSCompareResultInfoCompareResultTypeReplaceImage),
                            @(FSCompareResultInfoCompareResultTypeReplacePath),
                            @(FSCompareResultInfoCompareResultTypeReplaceShading),
                            @(FSCompareResultInfoCompareResultTypeReplaceAnnot)
                            ];
    return replaceArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
