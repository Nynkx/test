/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import "ComparisonContainerViewController.h"
#import "PageNumberView.h"
#import "CompareResultTipsAlert.h"
#import "DrawInfo.h"

#define LegendViewWidth 150
#define FLOAT_ZERO_CMP          1E-3

@interface ComparisonContainerViewController ()<UINavigationControllerDelegate,IDrawEventListener, IGestureEventListener,IDocEventListener>
@property (nonatomic, strong) UIBarButtonItem *buttonDone;
@property (nonatomic, assign) BOOL isScreenLocked;
@property (nonatomic, strong) UIView *legendView;
@property (nonatomic, strong) GotoPageToolBar *gotoPageToolBar;
@property (nonatomic, strong) FSAnnot *annot;
@property (nonatomic, assign) int pageindex;

@property (nonatomic, strong) FSRectF *leftRect;
@property (nonatomic, strong) FSRectF *rightRect;
@property (nonatomic, strong) NSMutableDictionary *compareDiffDict;
@end

@implementation ComparisonContainerViewController

#pragma mark - Initialization
- (instancetype)initWithDictonary:(NSMutableDictionary *)comparisonOptionResult{
    if (self = [super init]) {
        self.isScreenLocked = NO;
        self.comparisonOptionResult = [[NSMutableDictionary alloc] initWithDictionary:comparisonOptionResult];
        self.pageindex = -1;
        self.annot = nil;
        self.leftRect = [[FSRectF alloc] init];
        self.rightRect = [[FSRectF alloc] init];
        self.compareDiffDict = @{}.mutableCopy;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    FSPDFViewCtrl *pdfviewCtrl = [[FSPDFViewCtrl alloc] initWithFrame:self.view.frame];
    _pdfviewControl = pdfviewCtrl;
    [_pdfviewControl setRMSAppClientId:@"972b6681-fa03-4b6b-817b-c8c10d38bd20" redirectURI:@"com.foxitsoftware.com.mobilepdf-for-ios://authorize"];
    [self.view addSubview:_pdfviewControl];
    
    [_pdfviewControl registerDocEventListener:self];
    
    [_pdfviewControl mas_remakeConstraints:^(MASConstraintMaker *make) {
//        if (@available(iOS 11.0, *)) {
//            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
//        } else {
//            make.top.mas_equalTo(self.view.mas_top);
//        }
        make.top.mas_equalTo(self.view);
        make.left.right.bottom.mas_equalTo(self.view);
    }];

    self.view.backgroundColor = ThemeViewBackgroundColor;
    
    [_pdfviewControl setPageLayoutMode:PDF_LAYOUT_MODE_TWO];
    [_pdfviewControl setPageSpacing:5 direction:FS_PAGESPACING_HORIZONTAL];

    [_pdfviewControl openDoc:self.comparisonOptionResult[@"compareResultFilePath"]
                   password:nil
                 completion:^(FSErrorCode error) {
                     
                 }];
    [_pdfviewControl registerGestureEventListener:self];
    [_pdfviewControl registerDrawEventListener:self];

    _gotoPageToolBar = [[GotoPageToolBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44) pdfviewControl:_pdfviewControl];
    [_pdfviewControl addSubview:_gotoPageToolBar];
    
    [_gotoPageToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        } else {
            if (DEVICE_iPAD) {
                make.top.mas_equalTo(self.view.mas_top).offset(20);
            }else{
                make.top.mas_equalTo(self.view.mas_top);
            }
            
        }
        
        make.height.mas_equalTo(44);
    }];
    
    PageNumberView *pageNumberView = [[PageNumberView alloc] initWithFrame:CGRectMake(0, 0, 100, 40) pdfviewControl:_pdfviewControl];
    [_pdfviewControl addSubview:pageNumberView];
    [pageNumberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-30);
        make.width.mas_equalTo(pageNumberView.frame.size.width);
        make.height.mas_equalTo(pageNumberView.frame.size.height);
    }];
    
    __weak typeof(self) weakself = self;
    pageNumberView.clickedHandler = ^(PageNumberView *view) {
        __strong typeof(self) strongself = weakself;
        [strongself.navigationController setNavigationBarHidden:YES animated:YES];
        [strongself.legendView setHidden:YES];
        [strongself->_gotoPageToolBar setHidden:NO];
        [strongself->_gotoPageToolBar enterGotoMode];
    };
    
    self.legendView = [[UIView alloc] init];
    [self.view addSubview:self.legendView];
    self.legendView.backgroundColor = [UIColor colorWithRGB:0x494A4B alpha:0.8];
//    [self.legendView mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (@available(iOS 11.0, *)) {
//            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(10);
//        } else {
//            make.top.mas_equalTo(self.navigationController.navigationBar.frame.size.height + 10);
//        }
//        make.right.mas_equalTo(self.view.mas_right).offset(-10);
//        make.width.mas_equalTo(LegendViewWidth);
//        make.height.mas_equalTo(LegendViewWidth);
//
//    }];

    self.legendView.layer.cornerRadius = 5;
    self.legendView.layer.masksToBounds = YES;
    
    UILabel *legendTitleLabel = [[UILabel alloc] init];
    [self.legendView addSubview:legendTitleLabel];
    CGSize legendTitleSize = [Utility getTextSize:FSLocalizedForKey(@"kComapreContainerLegendTitle") fontSize:12.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
    [legendTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(legendTitleLabel.superview).offset(10);
        make.left.mas_equalTo(legendTitleLabel.superview).offset(10);
        make.width.mas_equalTo(legendTitleSize.width);
        make.height.mas_equalTo(legendTitleSize.height);
    }];
    UIFont *font = [UIFont systemFontOfSize:12.0f];
    legendTitleLabel.font = font;
    legendTitleLabel.textColor = WhiteThemeTextColor;
    legendTitleLabel.text = FSLocalizedForKey(@"kComapreContainerLegendTitle");
    
    [self addLegendContentViewto:self.legendView];
}

-(NSDictionary *)getLegendContentDictory {
    NSDictionary *legendContent = @{
                                    @"kComapreContainerLegendReplace":@"comparisoncontainer_legend_replace",
                                    @"kComapreContainerLegendInsert":@"comparisoncontainer_legend_insert",
                                    @"kComapreContainerLegendDelete":@"comparisoncontainer_legend_delete",
                                    };
    return legendContent;
}

-(void)addLegendContentViewto:(UIView *)legendView {
    NSDictionary *legendContentDict = [self getLegendContentDictory];
    NSArray *titleArr = [legendContentDict allKeys];
    
    UIView *lastView = nil;
    for (int i = 0; i < titleArr.count; i++) {
        NSString *title = [titleArr objectAtIndex:i];
        NSString *imageName = [legendContentDict objectForKey:title];
        
        UIImageView *titleImgView = [[UIImageView alloc] init];
        [legendView addSubview:titleImgView];
        UIImage *titleImage = [UIImage imageNamed:imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        titleImgView.image = titleImage;
        [titleImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (lastView == nil) {
                make.top.mas_equalTo(40);
            }else{
                make.top.mas_equalTo(lastView.mas_bottom).offset(10);
            }
            make.left.mas_equalTo(titleImgView.superview.mas_left).offset(10);
            make.width.mas_equalTo(titleImage.size.width);
            make.height.mas_equalTo(titleImage.size.height);
        }];
        
        lastView = titleImgView;
        
        UILabel *legendContentTitle = [[UILabel alloc] init];
        CGSize legendContentTitleSize = [Utility getTextSize:FSLocalizedForKey(title) fontSize:12.0f maxSize:CGSizeMake(SCREENWIDTH - 60, 100)];
        [legendView addSubview:legendContentTitle];
        UIFont *font = [UIFont systemFontOfSize:12.0f];
        legendContentTitle.font = font;
        legendContentTitle.textColor = WhiteThemeTextColor;
        [legendContentTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(titleImgView);
            make.left.mas_equalTo(legendView.mas_left).offset(LegendViewWidth/2);
            make.width.mas_equalTo(legendContentTitleSize.width);
            make.height.mas_equalTo(legendContentTitleSize.height);
        }];
        legendContentTitle.text = FSLocalizedForKey(title);
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.delegate = self;
    
    if([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = UIInterfaceOrientationLandscapeLeft;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
    
    [self initNavigationBar];
}

#pragma mark - Configure NavigationBar
- (void)initNavigationBar {
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLeft.frame = CGRectMake(0.0, 0.0, 65.0, 32);
    buttonLeft = [Utility createButtonWithImage:[UIImage imageNamed:@"common_back_black" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
    [buttonLeft addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem addLeftBarButtonItem:buttonLeft ? [[UIBarButtonItem alloc] initWithCustomView:buttonLeft] : nil];
    
    self.buttonDone = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self.buttonDone setTintColor:BlackThemeTextColor];
    [self.buttonDone setEnabled:YES];
    
    [self.navigationItem setRightBarButtonItem:self.buttonDone];
    self.navigationController.navigationBar.barTintColor = UIColor_Owner_DarkMode(self, [UIColor whiteColor], ThemeBarColor_Dark);
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    [self.buttonDone setTitle:FSLocalizedForKey(@"kComapreContainerHideLegend")];
    
    self.title = FSLocalizedForKey(@"kComparisonVCTitle");
    UIColor *color = BlackThemeTextColor;
    NSDictionary *dict = [NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
//    [self.navigationController.navigationBar.layer setBorderWidth:2.0];
//    [self.navigationController.navigationBar.layer setBorderColor:[[UIColor colorWithRGB:0xe2e2e2] CGColor]];

    [self.legendView mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(10);
        } else {
            make.top.mas_equalTo(self.navigationController.navigationBar.frame.size.height + 20 + 5);
        }
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.width.mas_equalTo(LegendViewWidth);
        make.height.mas_equalTo(LegendViewWidth);

    }];
}

- (void)cancelAction {
    self.buttonDone = nil;
    self.gotoPageToolBar = nil;
    self.legendView = nil;
    [self.comparisonOptionResult removeAllObjects];
    self.comparisonOptionResult = nil;
    self.annot = nil;
    self.leftRect = nil;
    self.rightRect = nil;
    
    [self.pdfviewControl closeDoc:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doneAction {
    BOOL isHidden = !self.legendView.hidden;
    [self.legendView setHidden:isHidden];
    if (isHidden == YES) {
        [self.buttonDone setTitle:FSLocalizedForKey(@"kComapreContainerShowLegend")];
    }else {
        [self.buttonDone setTitle:FSLocalizedForKey(@"kComapreContainerHideLegend")];
    }
}

- (FSAnnot *)getAnnotAtPoint:(CGPoint)pvPoint pageIndex:(int)pageIndex {
    FSPDFPage *page = [_pdfviewControl.currentDoc getPage:pageIndex];
    FSMatrix2D *matrix = [_pdfviewControl getDisplayMatrix:pageIndex];
    FSPointF *devicePoint = [[FSPointF alloc] init];
    [devicePoint set:pvPoint.x y:pvPoint.y];
    FSAnnot *annot = [page getAnnotAtDevicePoint:devicePoint tolerance:5 matrix:matrix];
    
    if (!annot || [annot isEmpty]) {
        return nil;
    }
    return annot;
}

#pragma mark - GestureEvent delegate
- (BOOL)onTap:(UITapGestureRecognizer *)gestureRecognizer {
    if (_gotoPageToolBar.hidden == NO) {
        [_gotoPageToolBar setHidden:YES];
        [_gotoPageToolBar quitGotoMode];
    }
    
    CGPoint point = [gestureRecognizer locationInView:[_pdfviewControl getDisplayView]];
    int pageIndex = [_pdfviewControl getPageIndex:point];
    if (pageIndex < 0)
        return NO;
    
    self.pageindex = pageIndex;
    
    UIView *pageView = [_pdfviewControl getPageView:pageIndex];
    CGPoint cgPoint = [gestureRecognizer locationInView:pageView];
    CGRect rect1 = [pageView frame];
    CGSize size = rect1.size;
    if (cgPoint.x > size.width || cgPoint.y > size.height || cgPoint.x < 0 || cgPoint.y < 0)
        return NO;
    
    point = [gestureRecognizer locationInView:[_pdfviewControl getPageView:pageIndex]];
    
    FSAnnot *annot = [self getAnnotAtPoint:point pageIndex:pageIndex];
    self.annot = annot;
    
    if (annot && (annot.type == FSAnnotNote || annot.type == FSAnnotSquiggly || annot.type == FSAnnotStrikeOut || annot.type == FSAnnotUnderline)) {
        @try {
            NSMutableDictionary *mMapDiff = self.compareDiffDict;
            for (id key in mMapDiff) {
                NSMutableDictionary *value = [mMapDiff objectForKey:key];
                DrawInfo *oldInfo = [value objectForKey:@"oldInfo"];
                DrawInfo *newInfo = [value objectForKey:@"newInfo"];
                
                if ( (fabsf(oldInfo.rectF.left - self.annot.fsrect.left) < FLOAT_ZERO_CMP
                      && fabsf(oldInfo.rectF.right - self.annot.fsrect.right) < FLOAT_ZERO_CMP
                      && fabsf(oldInfo.rectF.top - self.annot.fsrect.top) < FLOAT_ZERO_CMP
                      && fabsf(oldInfo.rectF.bottom - self.annot.fsrect.bottom) < FLOAT_ZERO_CMP)
                    
                    ||(fabsf(newInfo.rectF.left - self.annot.fsrect.left) < FLOAT_ZERO_CMP
                       && fabsf(newInfo.rectF.right - self.annot.fsrect.right) < FLOAT_ZERO_CMP
                       && fabsf(newInfo.rectF.top - self.annot.fsrect.top) < FLOAT_ZERO_CMP
                       && fabsf(newInfo.rectF.bottom - self.annot.fsrect.bottom) < FLOAT_ZERO_CMP)
                    ) {
                    self.leftRect = oldInfo.rectF;
                    self.rightRect = newInfo.rectF;
                }
            }
           
        } @catch (NSException *e) {
        }
        
        [_pdfviewControl refresh:CGRectZero pageIndex:pageIndex needRender:NO];
        
        
        // show tips ui
        CompareResultTipsAlert *alertView = [[CompareResultTipsAlert alloc] init];
        
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300 , 150)];
        customView.backgroundColor = ThemeCellBackgroundColor;
        
        UILabel *titleLabel = [[UILabel alloc] init];
        
        FSMarkup *annotmark = [[FSMarkup alloc] initWithAnnot:annot];
        NSString *title = annotmark.title ? annotmark.title : @"";
//        title = title.length < @"Compare : ".length ? title : [title substringFromIndex:@"Compare : ".length];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        titleLabel.font = [UIFont systemFontOfSize:14.0f];
        titleLabel.textColor = BlackThemeTextColor;
        titleLabel.backgroundColor = UIColor_DarkMode([UIColor colorWithRGB:0xf2f2f2], ThemeViewBackgroundColor);

        [customView addSubview: titleLabel];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(titleLabel.superview);
            make.height.mas_equalTo(50);
        }];
        
        UIView *separatorLine = [[UIView alloc] init];
        separatorLine.backgroundColor = [UIColor colorWithRGB:0xe1e1e1];
        [customView addSubview:separatorLine];
        [separatorLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabel.mas_bottom).offset(-1);
            make.left.right.mas_equalTo(separatorLine.superview);
            make.height.mas_equalTo([Utility realPX:1.0f]);
        }];
        
        UIScrollView *verticalScrollView = [[UIScrollView alloc] init];
        verticalScrollView.backgroundColor = ThemeCellBackgroundColor;
        [customView addSubview:verticalScrollView];
        [verticalScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(customView);
            make.top.mas_equalTo(titleLabel.mas_bottom).offset(10);
            make.width.mas_equalTo(customView.mas_width);
            make.height.mas_equalTo(80);
        }];
        UIView *verticalContainerView = [[UIView alloc] init];
        [verticalScrollView addSubview:verticalContainerView];
        [verticalContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.and.right.equalTo(verticalScrollView);
            make.width.equalTo(verticalScrollView);
        }];
        
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.text = annot.content;
        
        contentLabel.font = [UIFont systemFontOfSize:14.0f];
        contentLabel.textColor = BlackThemeTextColor;
        contentLabel.tintColor = [UIColor colorWithRGB:0x5e5e5e];
        contentLabel.numberOfLines = 0;
        contentLabel.preferredMaxLayoutWidth = (customView.frame.size.width - 10.0 * 2);
        [contentLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [verticalContainerView addSubview: contentLabel];
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10.0);
            make.right.mas_equalTo(-10.0);
            make.top.mas_equalTo(0);
        }];
        
        [verticalContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(contentLabel.mas_bottom);
        }];
        
        customView.layer.cornerRadius = 5;
        customView.layer.masksToBounds = YES;
        
        [alertView setButtonTitles:NULL];
        [alertView setContainerView:customView];
        [alertView show];
        return YES;
    }
    
    if (_gotoPageToolBar.hidden == YES) {
        BOOL isHidden = !self.navigationController.navigationBar.hidden;
        [self.navigationController setNavigationBarHidden:isHidden animated:YES];
    }
    
    CGFloat width = pageView.bounds.size.width;
    if (width * 0.2 < point.x && point.x < width * 0.8) {
        return YES;
    }else{
        return NO;
    }
}

-(NSMutableDictionary *)fillDocDiffMap:(FSPDFDoc *)doc {
    NSMutableDictionary *fillDocDiffMap = @{}.mutableCopy;
    @try {
        FSPDFDictionary *root = [doc getCatalog];
        if (root != nil) {
            BOOL bExistPieceInfo = [root hasKey:@"PieceInfo"];
            if (!bExistPieceInfo) return nil;
            
            FSPDFDictionary *pieceInfo = [[root getElement:@"PieceInfo"] getDict];
            if (pieceInfo == nil ) return nil;
            
            FSPDFDictionary *comparePDF = [[pieceInfo getElement:@"ComparePDF"] getDict];
            if (comparePDF == nil) return nil;
            
            FSPDFDictionary *priDict = [[comparePDF getElement:@"Private"] getDict];
            if (priDict == nil) return nil;
            
            FSPDFDictionary *differences = [[priDict getElement:@"Differences"] getDict];
            if (priDict == nil) return nil;
            
            FSPDFArray *numArray = [[differences getElement:@"Nums"] getArray];
            if (numArray == nil) return nil;
            
            int count = [numArray getElementCount];
            
            for (int i = 0; i < count; i = i + 2) {
                FSPDFObject *object = [numArray getElement:i];
                if (object == nil) continue;
                
                int index = [object getInteger];
                DrawInfo *oldInfo = [DrawInfo new];
                DrawInfo *newInfo = [DrawInfo new];
                
                oldInfo.rectF = [FSRectF new];
                newInfo.rectF = [FSRectF new];
                
                FSPDFObject *object1 = [[numArray getElement:(i + 1)] getDict];
                FSPDFArray *cArray = [[(FSPDFDictionary *) object1 getElement:@"C"] getArray];
                if (cArray == nil) continue;
                
                FSPDFArray *cArrayOld = [[cArray getElement:0] getArray];
                if (cArrayOld == nil) continue;
                int cArrayOldCount = [cArrayOld getElementCount];
                for (int oldIndex = 0; oldIndex < cArrayOldCount; oldIndex ++) {
                    FSPDFObject *oldObject = [cArrayOld getElement:oldIndex];
                    if (oldObject == nil) continue;
                    
                    float _index = [oldObject getFloat];
                    if (oldIndex == 0) {
                        oldInfo.rectF.left = _index;
                    } else if (oldIndex == 1) {
                        oldInfo.rectF.top = _index;
                    } else if (oldIndex == 2) {
                        oldInfo.rectF.right = _index;
                    } else if (oldIndex == 3) {
                        oldInfo.rectF.bottom = _index;
                    }
                }
                oldInfo.rectF = [Utility normalizeFSRect:oldInfo.rectF];
                
                FSPDFArray *cArrayNew = [[cArray getElement:1] getArray];
                if (cArrayNew == nil) continue;
                
                int cArrayNewCount = [cArrayNew getElementCount];
                for (int newIndex = 0; newIndex < cArrayNewCount; newIndex ++) {
                    FSPDFObject *newObject = nil;
                    @try {
                        newObject = [cArrayNew getElement:newIndex];
                    }@catch (NSException *exception) {
                        continue;
                    }
                    
                    if (newObject == nil) continue;
                    
                    float _index = [newObject getFloat];
                    if (newIndex == 0) {
                        newInfo.rectF.left = _index;
                    } else if (newIndex == 1) {
                        newInfo.rectF.top = _index;
                    } else if (newIndex == 2) {
                        newInfo.rectF.right = _index;
                    } else if (newIndex == 3) {
                        newInfo.rectF.bottom = _index;
                    }
                }
                newInfo.rectF = [Utility normalizeFSRect:newInfo.rectF];
                
                NSString *outD = [[(FSPDFDictionary *) object1 getElement:@"D"] getWideString];
                int resultType = 0;
                if ([outD isEqualToString:@"D"]) {
                    resultType = RESULTTYPE_DELETE;
                } else if ([outD isEqualToString:@"I"]) {
                    resultType = RESULTTYPE_INSERT;
                } else if ([outD isEqualToString:@"R"]) {
                    resultType = RESULTTYPE_REPLACEMENT;
                }

                oldInfo.resultType = resultType;
                newInfo.resultType = resultType;
                
                NSString *outT = [[(FSPDFDictionary *) object1 getElement:@"T"] getWideString];
                oldInfo.filterType = outT;
                newInfo.filterType = outT;
                
                FSPDFArray *pageArray = [[(FSPDFDictionary *) object1 getElement:@"Pg"] getArray];
                if (pageArray == nil) continue;
                
                int pageCount = [pageArray getElementCount];
                
                if (pageCount != 2) continue;
                
                FSPDFObject *oldIndexObject = [pageArray getElement:0];
                if (oldIndexObject == nil) continue;
                
                oldInfo.pageIndex = [oldIndexObject getInteger];
                
                FSPDFObject *newIndexObject = [pageArray getElement:1];
                if (newIndexObject == nil) continue;
                newInfo.pageIndex = [newIndexObject getInteger];
                
                NSMutableDictionary *compareinfo = @{}.mutableCopy;
                [compareinfo setObject:oldInfo forKey:@"oldInfo"];
                [compareinfo setObject:newInfo forKey:@"newInfo"];
                [fillDocDiffMap setObject:compareinfo forKey:@(index)];
            }
        }
        
        return fillDocDiffMap;
    } @catch (NSException *exception) {
    }
}

#pragma mark IDrawEventListener
- (void)onDraw:(int)pageIndex inContext:(CGContextRef)context {
    if (self.annot && self.pageindex == self.annot.pageIndex) {
        int annotPageIndex = self.annot.pageIndex;
        int leftPageIndex = annotPageIndex %2 == 0 ?  annotPageIndex : annotPageIndex - 1;
        int rightPageIndex = annotPageIndex %2 == 0 ?  annotPageIndex + 1 : annotPageIndex;
        NSArray *needRenderPageIndexs = @[ @(leftPageIndex),@(rightPageIndex)];
        
        if (![needRenderPageIndexs containsObject:@(pageIndex)]) {
            return ;
        }
        
        if (pageIndex % 2 == 0) {
            CGRect rect = [_pdfviewControl convertPdfRectToPageViewRect:self.leftRect pageIndex:pageIndex];
            rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(-5, -5, -5, -5));
            CGContextSetLineWidth(context, 2.0f);
            UIColor *color = [UIColor colorWithRGB:self.annot.color];
            CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],1.0f);
            CGContextStrokeRect(context, rect);
        }else {
            CGRect rect = [_pdfviewControl convertPdfRectToPageViewRect:self.rightRect pageIndex:pageIndex];
            rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(-5, -5, -5, -5));
            CGContextSetLineWidth(context, 2.0f);
            UIColor *color = [UIColor colorWithRGB:self.annot.color];
            CGContextSetRGBStrokeColor(context,[color red], [color green], [color blue],1.0f);
            CGContextStrokeRect(context, rect);
        }

    }
    
}

#pragma mark - onDocOpened
-(void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    self.compareDiffDict = [self fillDocDiffMap:document];
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    BOOL isHome = [viewController isKindOfClass:[self class]];
    [self.navigationController setNavigationBarHidden:!isHome animated:YES];
}

- (void)dealloc {
    self.navigationController.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
