/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/


#import <UIKit/UIKit.h>

@interface CompareResultTipsAlert : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, retain) UIView *dialogView;    // Dialog's container view
@property (nonatomic, retain) UIView *containerView; // Container within the dialog (place your ui elements here)

@property (nonatomic, retain) NSArray *buttonTitles;

@property (copy) void (^onButtonTouchUpInside)(CompareResultTipsAlert *alertView, int buttonIndex) ;

- (id)init;

- (void)show;
- (void)close;
- (void)changeCustomViewSize;

- (void)setOnButtonTouchUpInside:(void (^)(CompareResultTipsAlert *alertView, int buttonIndex))onButtonTouchUpInside;

- (void)deviceOrientationDidChange: (NSNotification *)notification;
- (void)dealloc;
@end
