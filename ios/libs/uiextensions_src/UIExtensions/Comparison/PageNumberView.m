/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PageNumberView.h"
#import <QuartzCore/QuartzCore.h>

@interface PageNumberView () {
}

@property (strong, nonatomic) UIView *pageNumView;
@property (strong, nonatomic) UILabel *totalNumLabel;
@property (strong, nonatomic) UIImageView *prevImage;
@property (strong, nonatomic) UIImageView *nextImage;
@property (assign, nonatomic) BOOL gotoToolbarShouldShow;
@property (assign, nonatomic) BOOL isClickGoBtn;

@property (nonatomic, assign) BOOL isFullScreen;

@end

@implementation PageNumberView

- (instancetype)initWithFrame:(CGRect)frame pdfviewControl:(FSPDFViewCtrl *)pdfviewControl {
    if (self = [super initWithFrame:frame]) {
        _pdfViewCtrl = pdfviewControl;
        self.frame = frame;
        
        [self initSubViews];
        self.gotoToolbarShouldShow = NO;
        [self loadModule];
        _isClickGoBtn = NO;
    }
    return self;
}

- (NSString *)getName {
    return @"PageNumber";
}

- (void)initSubViews {
    self.totalNumLabel = [[UILabel alloc] init];
    self.totalNumLabel.userInteractionEnabled = YES;
    [self.totalNumLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showGotoPageToolbar:)]];

    self.prevImage = [[UIImageView alloc] init];
    self.prevImage.image = [UIImage imageNamed:@"goto_page_jump_prev" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.prevImage.userInteractionEnabled = YES;
    [self.prevImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGotoPrevView:)]];

    self.nextImage = [[UIImageView alloc] init];
    self.nextImage.image = [UIImage imageNamed:@"goto_page_jump_next" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.nextImage.userInteractionEnabled = YES;
    [self.nextImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGotoNextView:)]];

    self.pageNumView = [[UIView alloc] init];
    [self.pageNumView addSubview:self.totalNumLabel];
    [self.pageNumView addSubview:self.prevImage];
    [self.pageNumView addSubview:self.nextImage];

    [self addSubview:self.pageNumView];
    self.pageNumView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

- (void)addPageNumberView {
    self.prevImage.hidden = YES;
    self.nextImage.hidden = YES;

    //    CGRect frame = _pdfViewCtrl.bounds;
    CGRect frame = [UIScreen mainScreen].bounds;
    frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.width);
    if (DEVICE_iPHONE) {
//        self.pageNumView.frame = CGRectMake((IS_IPHONE_OVER_TEN && isLandscape) ? 49 : 15, IS_IPHONE_OVER_TEN ? frame.size.height - 93 - 34 : frame.size.height - 93, 50, 34);
        self.pageNumView.layer.cornerRadius = 17.0;
    } else {
//        self.pageNumView.frame = CGRectMake((IS_IPHONE_OVER_TEN && isLandscape) ? 54 : 20, frame.size.height - 94, 50, 30);
        self.pageNumView.layer.cornerRadius = 15.0;
    }

    self.pageNumView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.425];
//    [_pdfViewCtrl insertSubview:_pageNumView aboveSubview:[_pdfViewCtrl getDisplayView]];
}

- (void)removePageNumberView {
//    UIView *pageNumView = self.pageNumView;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [pageNumView removeFromSuperview];
//    });
}

- (NSString *)getDisplayPageLabel:(int)pageIndex needTotal:(BOOL)needTotal {
    // copied from single container scroll view
    NSString *ret;
    int pageCount = [_pdfViewCtrl getPageCount];

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_LEFT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_RIGHT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_MIDDLE) {
        if(pageIndex == 0 || pageIndex == 1) {
            if (pageCount == 2) {
                if(_pdfViewCtrl.IsChangeLayoutMode ){
                    ret = [NSString stringWithFormat:@"%d",pageIndex ];
                }else{
                    ret = [NSString stringWithFormat:@"%d",pageIndex + 1];
                }
                _pdfViewCtrl.IsChangeLayoutMode = NO;
            }else{
                ret = [NSString stringWithFormat:@"%d",1];
            }
        } else{
             if (pageCount == 2) {
                ret = [NSString stringWithFormat:@"%d", 2];
             }
             else if(pageCount % 2 == 1){
                pageIndex--;
                ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
            }else{
                if(pageIndex == pageCount-1){
                    ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
                }else{
                    pageIndex--;
                    ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
                }
            }
        }

        if (needTotal) {
            ret = [NSString stringWithFormat:@"%@/%d", ret, pageCount];
        }
        return ret;
    }

    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO &&
        !(pageCount % 2 == 1 && (pageIndex == pageCount - 1))) {
        if (pageIndex % 2 == 1) {
            pageIndex--;
        }
        ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
    } else {
        ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
    }

    if (needTotal) {
        ret = [NSString stringWithFormat:@"%@/%d", ret, pageCount];
    }
    return ret;
}

- (void)showGotoPageToolbar:(UITapGestureRecognizer *)recognizer {
    if (self.clickedHandler) {
        self.clickedHandler(self);
    }
//    [_extensionsManager changeState:STATE_PAGENAVIGATE];
}

- (void)handleGotoPrevView:(UITapGestureRecognizer *)recognizer {
//    [_extensionsManager setCurrentAnnot:nil];

    if ([_pdfViewCtrl hasPrevView]) {
        [_pdfViewCtrl gotoPrevView:YES];
    }
}

- (void)handleGotoNextView:(UITapGestureRecognizer *)recognizer {
//    [_extensionsManager setCurrentAnnot:nil];

    if ([_pdfViewCtrl hasNextView]) {
        [_pdfViewCtrl gotoNextView:YES];
    }
}

- (void)updatePageNumLabel {
    _totalNumLabel.text = [self getDisplayPageLabel:[_pdfViewCtrl getCurrentPage] needTotal:YES];
    _totalNumLabel.font = [UIFont systemFontOfSize:15];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize sizeName = [self.totalNumLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 0.0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.totalNumLabel.font, NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;
    
    if (DEVICE_iPHONE) {
        self.totalNumLabel.frame = CGRectMake(0, 0, sizeName.width, sizeName.height);
    } else {
        self.totalNumLabel.frame = CGRectMake(0, 0, sizeName.width, sizeName.height);
    }
}

- (void)setPageCountLabel {
    int state = 0; //[_extensionsManager getState];
    if (state == STATE_ANNOTTOOL || state == STATE_EDIT) {
        self.pageNumView.hidden = YES;
    } else {
        self.pageNumView.hidden = NO;
    }

    [self updatePageNumLabel];

    CGRect viewFrame = self.pageNumView.frame;
    if ([_pdfViewCtrl hasPrevView] && ![_pdfViewCtrl hasNextView]) {
        viewFrame.size.width = 46 + self.totalNumLabel.frame.size.width;
        self.pageNumView.frame = viewFrame;
        CGRect frame = self.totalNumLabel.frame;
        frame.origin.x = 36;
        self.totalNumLabel.frame = frame;
        self.prevImage.frame = CGRectMake(10, 9, 16, 16);
        self.prevImage.hidden = NO;
        self.nextImage.hidden = YES;
    } else if (![_pdfViewCtrl hasPrevView] && [_pdfViewCtrl hasNextView]) {
        viewFrame.size.width = 46 + self.totalNumLabel.frame.size.width;
        self.pageNumView.frame = viewFrame;
        CGRect frame = self.totalNumLabel.frame;
        frame.origin.x = 10;
        self.totalNumLabel.frame = frame;
        self.nextImage.frame = CGRectMake(20 + self.totalNumLabel.frame.size.width, 9, 16, 16);
        self.nextImage.hidden = NO;
        self.prevImage.hidden = YES;
    } else if ([_pdfViewCtrl hasPrevView] && [_pdfViewCtrl hasNextView]) {
        viewFrame.size.width = 72 + self.totalNumLabel.frame.size.width;
        self.pageNumView.frame = viewFrame;
        self.totalNumLabel.center = CGPointMake(viewFrame.size.width / 2, viewFrame.size.height / 2);
        self.prevImage.frame = CGRectMake(10, 9, 16, 16);
        self.nextImage.frame = CGRectMake(viewFrame.size.width - 26, 9, 16, 16);
        self.nextImage.hidden = NO;
        self.prevImage.hidden = NO;
    } else {
        viewFrame.size.width = 20 + self.totalNumLabel.frame.size.width;
        self.pageNumView.frame = viewFrame;
        self.totalNumLabel.center = CGPointMake(viewFrame.size.width / 2, viewFrame.size.height / 2);
        self.prevImage.hidden = YES;
        self.nextImage.hidden = YES;
    }
    self.totalNumLabel.textColor = WhiteThemeTextColor;
}

- (void)loadModule {
    [_pdfViewCtrl registerScrollViewEventListener:self];
    [_pdfViewCtrl registerGestureEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_pdfViewCtrl registerDocEventListener:self];
}

#pragma pageEventListener methods

- (void)onPageChanged:(int)oldIndex currentIndex:(int)currentIndex {
    [self setPageCountLabel];
}

- (void)onPageJumped {
    [self setPageCountLabel];
}

- (void)onPagesRemoved:(NSArray<NSNumber *> *)indexes {
    // If the document has only one page, there should not be a previous page and next page
    if ([_pdfViewCtrl getPageCount] == 1) {
        [_pdfViewCtrl clearPrevNextStack];
    }
    [self setPageCountLabel];
}

- (void)onPagesInsertedAtRange:(NSRange)range {
    // If insert a page, the data in the stack becomes invalid and needs to be empty
    [_pdfViewCtrl clearPrevNextStack];
    [self setPageCountLabel];
}

- (void)onPagesMoved:(NSArray<NSNumber *> *)indexes dstIndex:(int)dstIndex {
    // If insert a page, the data in the stack becomes invalid and needs to be empty
    [_pdfViewCtrl clearPrevNextStack];
    [self setPageCountLabel];
}

#pragma docEventlistener methods

- (void)onDocWillOpen {
}

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if(error != FSErrSuccess) return;
    [self addPageNumberView];
    [self setPageCountLabel];
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    [self removePageNumberView];
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
}

- (void)onDocWillSave:(FSPDFDoc *)document {
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (!_pdfViewCtrl.currentDoc) {
        return ;
    }
//    if (!self.gotoPageToolbar.hidden) {
//        self.gotoToolbarShouldShow = YES;
//    }
    self.pageNumView.hidden = YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (!_pdfViewCtrl.currentDoc) {
        return ;
    }
    //    CGRect frame = _pdfViewCtrl.bounds;
    CGRect frame = [UIScreen mainScreen].bounds;
    bool isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);

    if (DEVICE_iPHONE) {
        self.pageNumView.frame = CGRectMake((IS_IPHONE_OVER_TEN && isLandscape) ? 49 : 15, IS_IPHONE_OVER_TEN ? frame.size.height - 93 - 34 : frame.size.height - 93, 50, 34);
    } else {
        self.pageNumView.frame = CGRectMake((IS_IPHONE_OVER_TEN && isLandscape) ? 54 : 20, frame.size.height - 94, 50, 30);
    }
    
    [self setPageCountLabel];
    int state = 0;//[_extensionsManager getState];
    
    if (state == STATE_ANNOTTOOL || state == STATE_EDIT || state == STATE_FILLSIGN ){ //|| _extensionsManager.isFullScreen) {
        self.pageNumView.hidden = YES;
    } else {
        self.pageNumView.hidden = NO;
    }
    
    if (self.gotoToolbarShouldShow) {
        [self showGotoPageToolbar:nil];
        self.gotoToolbarShouldShow = NO;
    }
}

#pragma - IFullScreenListener

- (void)onFullScreen:(BOOL)isFullScreen {
    if (isFullScreen) {
        self.pageNumView.hidden = YES;
    } else {
        [self setPageCountLabel];
    }
}

#pragma - (void) onStateChanged : (int) state;

- (void)onStateChanged:(int)state {
//    if ([_extensionsManager getState] != STATE_PAGENAVIGATE)
//        _oldReadFrameState = [_extensionsManager getState];
//
//    if (state == STATE_PAGENAVIGATE) {
//        if ([_extensionsManager getState] == STATE_SEARCH) [_extensionsManager showSearchBar:NO];
//        [_extensionsManager setCurrentAnnot:nil];
//
//        [self.pageNumBar becomeFirstResponder];
//        self.gotoPageToolbar.hidden = NO;
//    }

    if (state == STATE_ANNOTTOOL || state == STATE_EDIT || state == STATE_FILLSIGN) {
        self.pageNumView.hidden = YES;
    } else {
        if (state == STATE_NORMAL || state == STATE_REFLOW) {
            [self setPageCountLabel];
        }
        self.pageNumView.hidden = NO;
    }
}

@end


@interface GotoPageToolBar ()<IScrollViewEventListener, IGestureEventListener, IPageEventListener, IDocEventListener>

@property (strong, nonatomic) UITextField *pageNumBar;
@property (strong, nonatomic) UIButton *goBtn;

@end

@implementation GotoPageToolBar

- (instancetype)initWithFrame:(CGRect)frame pdfviewControl:(FSPDFViewCtrl *)pdfviewControl {
    if (self = [super initWithFrame:frame]) {
        _pdfViewCtrl = pdfviewControl;
        self.frame = frame;
        
        [self initSubViews];
        [self loadModule];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    NSArray *subViewArray = [self subviews];
    
    for (id view in subViewArray) {
        if ([view isKindOfClass:(NSClassFromString(@"_UIToolbarContentView"))]) {
            UIView *testView = view;
            testView.userInteractionEnabled = NO;
        }
    }
    
}

- (NSString *)getName {
    return @"PageToolBar";
}

- (void)initSubViews {
    self.backgroundColor = GeneralColor;
    self.hidden = YES;
//    self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    
    self.goBtn = [[UIButton alloc] init];
    self.goBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    self.goBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.goBtn setTitle:FSLocalizedForKey(@"kGo") forState:UIControlStateNormal];
    [self.goBtn setTitleColor:[UIColor colorWithRGB:0x179cd8] forState:UIControlStateNormal];
    [self.goBtn setTitleColor:GeneralColor forState:UIControlStateHighlighted | UIControlStateDisabled | UIControlStateSelected | UIControlStateApplication | UIControlStateReserved];
    self.goBtn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.goBtn addTarget:self action:@selector(goAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.pageNumBar = [[UITextField alloc] init];
    self.pageNumBar.keyboardType = UIKeyboardTypeNumberPad;
    self.pageNumBar.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    self.pageNumBar.borderStyle = UITextBorderStyleRoundedRect;
    self.pageNumBar.layer.borderColor = [UIColor colorWithRGB:0x179cdb].CGColor;
    self.pageNumBar.layer.borderWidth = 1.0f;
    self.pageNumBar.layer.cornerRadius = 4.0f;
    self.pageNumBar.delegate = self;
    
    UIView *divideView = [[UIView alloc] init];
    divideView.backgroundColor = DividingLineColor;
    divideView.frame = CGRectMake(0, 44 - [Utility realPX:1.0f], CGRectGetWidth(_pdfViewCtrl.bounds), [Utility realPX:1.0f]);
    divideView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self addSubview:divideView];
    
    [self addSubview:self.goBtn];
    [self addSubview:self.pageNumBar];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize sizeName = [self.goBtn.titleLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 0.0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.goBtn.titleLabel.font, NSParagraphStyleAttributeName : paragraphStyle} context:nil].size;

    float goTopf = (44 - sizeName.height) / 2;

    [self.goBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-10);
        make.top.mas_equalTo(self.mas_top).offset(goTopf);
        make.height.mas_equalTo(sizeName.height);
        make.width.mas_equalTo(sizeName.width + 10);
    }];

    [self.pageNumBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(10);
        make.top.mas_equalTo(self.mas_top).offset(5);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(self.goBtn.mas_left).offset(-10);
    }];
}

- (NSString *)getDisplayPageLabel:(int)pageIndex needTotal:(BOOL)needTotal {
    // copied from single container scroll view
    NSString *ret;
    int pageCount = [_pdfViewCtrl getPageCount];
    
    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_LEFT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_RIGHT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_MIDDLE) {
        if(pageIndex == 0 || pageIndex == 1) {
            if (pageCount == 2) {
                if(_pdfViewCtrl.IsChangeLayoutMode ){
                    ret = [NSString stringWithFormat:@"%d",pageIndex ];
                }else{
                    ret = [NSString stringWithFormat:@"%d",pageIndex + 1];
                }
                _pdfViewCtrl.IsChangeLayoutMode = NO;
            }else{
                ret = [NSString stringWithFormat:@"%d",1];
            }
        } else{
            if (pageCount == 2) {
                ret = [NSString stringWithFormat:@"%d", 2];
            }
            else if(pageCount % 2 == 1){
                pageIndex--;
                ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
            }else{
                if(pageIndex == pageCount-1){
                    ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
                }else{
                    pageIndex--;
                    ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
                }
            }
        }
        
        if (needTotal) {
            ret = [NSString stringWithFormat:@"%@/%d", ret, pageCount];
        }
        return ret;
    }
    
    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO &&
        !(pageCount % 2 == 1 && (pageIndex == pageCount - 1))) {
        if (pageIndex % 2 == 1) {
            pageIndex--;
        }
        ret = [NSString stringWithFormat:@"%d,%d", pageIndex + 1, pageIndex + 2];
    } else {
        ret = [NSString stringWithFormat:@"%d", pageIndex + 1];
    }
    
    if (needTotal) {
        ret = [NSString stringWithFormat:@"%@/%d", ret, pageCount];
    }
    return ret;
}

- (void)gotoPageWithPageIndex:(int)pageIndex {
    if (pageIndex <= 0 || pageIndex > [_pdfViewCtrl getPageCount]) {
        self.pageNumBar.text = @"";
        self.pageNumBar.placeholder = [self getDisplayPageLabel:[_pdfViewCtrl getCurrentPage] needTotal:YES];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:[NSString stringWithFormat:@"%@ %d - %d", FSLocalizedForKey(@"kWrongPageNumber"), 1, [_pdfViewCtrl getPageCount]] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [self setHidden:NO];
                                                       }];
        [alertController addAction:action];
        [self.pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    } else {
        [self gotoPage:pageIndex - 1 animated:NO];
    }
}

- (BOOL)gotoPage:(int)index animated:(BOOL)animated {
    int pageCount = [_pdfViewCtrl getPageCount];
    if ([_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_LEFT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_RIGHT || [_pdfViewCtrl getPageLayoutMode] == PDF_LAYOUT_MODE_TWO_MIDDLE) {
        pageCount = pageCount +1;
    }
    
    NSAssert1(index >= -1 && index < pageCount, @"Attempt to go to page index out of range: %d", index);
    
    if (index >= 0 && index < pageCount) {
        if (YES) {
            [self.pageNumBar resignFirstResponder];
            self.hidden = YES;
            [_pdfViewCtrl gotoPage:index animated:animated];
        }
        return YES;
    }
    return NO;
}

- (void)goAction {
    NSString *stringPage = [_pageNumBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    int pageIndex = 0;
    if (stringPage != nil && stringPage.length != 0) {
        pageIndex = stringPage.intValue;
    }
    [self.pageNumBar resignFirstResponder];
    [self gotoPageWithPageIndex:pageIndex];
    [self quitGotoMode];
}

- (void)enterGotoMode {
    [self.pageNumBar becomeFirstResponder];

    self.pageNumBar.placeholder = [self getDisplayPageLabel:[_pdfViewCtrl getCurrentPage] needTotal:YES];
}

- (void)quitGotoMode {
    [self.pageNumBar resignFirstResponder];
    self.pageNumBar.text = nil;
}

- (void)handleGotoPrevView:(UITapGestureRecognizer *)recognizer {
    if ([_pdfViewCtrl hasPrevView]) {
        [_pdfViewCtrl gotoPrevView:YES];
    }
}

- (void)handleGotoNextView:(UITapGestureRecognizer *)recognizer {
    if ([_pdfViewCtrl hasNextView]) {
        [_pdfViewCtrl gotoNextView:YES];
    }
}

- (void)loadModule {
    [_pdfViewCtrl registerScrollViewEventListener:self];
    [_pdfViewCtrl registerGestureEventListener:self];
    [_pdfViewCtrl registerPageEventListener:self];
    [_pdfViewCtrl registerDocEventListener:self];
    
    //    [_extensionsManager registerFullScreenListener:self];
    //    [_extensionsManager registerStateChangeListener:self];
    //    [_extensionsManager registerRotateChangedListener:self];
}

#pragma docEventlistener methods

- (void)onDocWillClose:(FSPDFDoc *)document {
    [self.pageNumBar resignFirstResponder];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    [self showGotoPageToolbar:nil];
}

#pragma mark IGestureEventListener

- (BOOL)onTap:(UITapGestureRecognizer *)recognizer {
    //    if ([_extensionsManager getState] != STATE_PAGENAVIGATE)
    //        return NO;
    [self quitGotoMode];
    return NO;
}

@end
