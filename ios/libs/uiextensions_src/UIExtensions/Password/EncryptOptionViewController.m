/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "EncryptOptionViewController.h"


@interface EncryptOptionViewController () <UIPopoverPresentationControllerDelegate>
@property (nonatomic, strong) UIButton *buttonDone;

- (void)buttonCancelClicked:(id)sender;
- (void)buttonDoneClicked:(id)sender;
- (void)initNavigationBar;
- (void)refreshInterface;

@end

@implementation EncryptOptionViewController

@synthesize cellOpenDoc;
@synthesize switchOpenDoc;
@synthesize cellPrintDoc;
@synthesize switchPrintDoc;
@synthesize cellCopyAccessibility;
@synthesize switchCopyAccessibility;
@synthesize cellOpenDocPassword;
@synthesize textboxOpenDocPassword;
@synthesize cellOtherPassword;
@synthesize textboxOtherPassword;
@synthesize cellEncryptRMS;
@synthesize buttonEncryptRMS;
@synthesize optionHandler = _optionHandler;
@synthesize rmsHandler = _rmsHandler;

#pragma mark - life cycle

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StateOfDoneButton) name:UITextFieldTextDidChangeNotification object:nil];
    self.tableView.backgroundColor = UIColor_DarkMode( self.tableView.backgroundColor, PageBackgroundColor_Dark);
    if (DEVICE_iPHONE) {
        [self.tableView setBackgroundView:nil];
    }
    UIButton *buttonCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonCancel.frame = CGRectMake(0.0, 0.0, 55.0, 32);
    [buttonCancel setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    buttonCancel.titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    [buttonCancel setTitleColor:[UIColor colorWithRed:1.f / 255.f green:144.f / 255.f blue:210.f / 255.f alpha:1.f] forState:UIControlStateNormal];
    [buttonCancel setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [buttonCancel addTarget:self action:@selector(buttonCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:buttonCancel]];

    self.buttonDone = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonDone.frame = CGRectMake(0.0, 0.0, 55.0, 32);
    [_buttonDone setTitle:FSLocalizedForKey(@"kDone") forState:UIControlStateNormal];
    _buttonDone.titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];

    self.buttonDone.userInteractionEnabled = NO;
    [_buttonDone setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
    self.buttonDone.alpha = 0.4;

    [_buttonDone setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [_buttonDone addTarget:self action:@selector(buttonDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem addRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_buttonDone]];

    [self refreshInterface];
}

/*
- (void)viewDidUnload {
    [self setCellOpenDoc:nil];
    [self setSwitchOpenDoc:nil];
    [self setCellPrintDoc:nil];
    [self setSwitchPrintDoc:nil];
    [self setCellCopyAccessibility:nil];
    [self setSwitchCopyAccessibility:nil];
    [self setCellOpenDocPassword:nil];
    [self setTextboxOpenDocPassword:nil];
    [self setCellOtherPassword:nil];
    [self setTextboxOtherPassword:nil];
    [self setCellEncryptRMS:nil];
    [self setButtonEncryptRMS:nil];
    [self setCellAnnotDoc:nil];
    [self setSwitchAnnotDoc:nil];
    [self setCellAssembleDoc:nil];
    [self setSwitchAssembleDoc:nil];
    [self setCellFillForm:nil];
    [self setSwitchFillForm:nil];
    [self setCellAddLimitation:nil];
    [self setSwitchAddLimitation:nil];
    [self setCellEditDocument:nil];
    [self setSwitchEditDocument:nil];
    [self setCellExtractContent:nil];
    [self setSwitchExtractContent:nil];
    [super viewDidUnload];
}
*/

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}
*/

#pragma mark - Table view data source
//TODO
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //disable RMS encrypt
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return FSLocalizedForKey(@"kEncryptionSectionTitle");
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return FSLocalizedForKey(@"kEncryptionSectionFooter");
    }
    return nil;
}

//TODO:
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        int rowCount = 2; //info cells
        if (switchOpenDoc.on) {
            rowCount++; //show open doc password
        }
        if (_switchAddLimitation.on) {
            rowCount += 8;
        }
        return rowCount;
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

//TODO:
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSInteger row = indexPath.row;
        if (row == 0) {
            return cellOpenDoc;
        }
        if (switchOpenDoc.on) {
            row--;
        }
        if (row == 0) {
            return cellOpenDocPassword;
        }
        if (row == 1) {
            return _cellAddLimitation;
        }
        if (row == 2) {
            return cellPrintDoc;
        }
        if (row == 3) {
            return _cellFillForm;
        }
        if (row == 4) {
            return _cellAnnotDoc;
        }
        if (row == 5) {
            return _cellAssembleDoc;
        }
        if (row == 6) {
            return _cellEditDocument;
        }
        if (row == 7) {
            return cellCopyAccessibility;
        }
        if (row == 8) {
            return _cellExtractContent;
        }
        if (_switchAddLimitation.on && row == 9) {
            return cellOtherPassword;
        }
    } else {
        return cellEncryptRMS;
    }

    return nil;
}

#pragma mark - UITextField delegate handler

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)StateOfDoneButton {
    if (self.textboxOtherPassword.text.length == 0 && self.textboxOpenDocPassword.text.length == 0) {
        self.buttonDone.userInteractionEnabled = NO;
        [_buttonDone setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
    } else {
        [self.buttonDone setTitleColor:[UIColor colorWithRed:1.f / 255.f green:144.f / 255.f blue:210.f / 255.f alpha:1.f] forState:UIControlStateNormal];
        self.buttonDone.userInteractionEnabled = YES;
        self.buttonDone.alpha = 1;
    }
}

#pragma mark - UINavigationController delegate handler

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    navigationController.navigationBar.tag = 1;
    navigationController.navigationBar.barTintColor = UIColor_DarkMode(GeneralColor, ThemeBarColor_Dark);
    [self initNavigationBar];
}

#pragma mark - event handler

- (IBAction)switchOpenDocValueChanged:(id)sender {
    if (!((UISwitch *) sender).on) {
        self.textboxOpenDocPassword.text = nil;
    }
    [self StateOfDoneButton];
    [self.tableView reloadData];
}

- (IBAction)switchPrintDocValueChanged:(id)sender {
    [self.tableView reloadData];
}

- (IBAction)switchCopyAccessibilityValueChanged:(id)sender {
    if (!((UISwitch *) sender).on) {
        self.switchExtractContent.on = NO;
    }
    [self.tableView reloadData];
}

- (IBAction)switchAnnotDocValueChanged:(id)sender {
    if (((UISwitch *) sender).on) {
        self.switchFillForm.on = YES;
    }
    [self.tableView reloadData];
}

- (IBAction)switchAssembleDocValueChanged:(id)sender {
    if (!((UISwitch *) sender).on) {
        self.switchEditDocument.on = NO;
    }
    [self.tableView reloadData];
}

- (IBAction)encryptUsingRMS:(id)sender {
    if (self.rmsHandler != nil) {
        self.rmsHandler(self);
    }

    [self close];
}

- (IBAction)switchFillFormValueChanged:(id)sender {
    if (!((UISwitch *) sender).on) {
        self.switchAnnotDoc.on = NO;
        self.switchEditDocument.on = NO;
    }
    [self.tableView reloadData];
}

- (IBAction)switchEditDocumentValueChanged:(id)sender {
    if (((UISwitch *) sender).on) {
        self.switchAssembleDoc.on = YES;
        self.switchFillForm.on = YES;
    }
    [self.tableView reloadData];
}

- (IBAction)switchExtractContentValueChanged:(id)sender {
    if (((UISwitch *) sender).on) {
        self.switchCopyAccessibility.on = YES;
    }
    [self.tableView reloadData];
}

- (IBAction)switchAddLimitationValueChanged:(id)sender {
    if (!((UISwitch *) sender).on) {
        self.textboxOtherPassword.text = nil;
    }
    [self StateOfDoneButton];
    [self.tableView reloadData];
}

- (void)buttonCancelClicked:(id)sender {
    if (self.optionHandler != nil) {
        self.optionHandler(self, YES, nil, nil, NO, NO, NO, NO, NO, NO, NO, NO);
    }
    [self close];
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)buttonDoneClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self->switchOpenDoc.on && self->textboxOpenDocPassword.text.length == 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kEncryptMissOpenDocPass") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction *_Nonnull action) {
                                                               [self popoverDidCancel];
                                                           }];
            [alertController addAction:action];
            [self presentAlertController:alertController];
            return;
        }
        if ((self->switchPrintDoc.on || self->switchCopyAccessibility.on || self->_switchAnnotDoc.on || self->_switchAssembleDoc.on || self->_switchExtractContent.on || self->_switchFillForm.on || self->_switchEditDocument.on) && self->textboxOtherPassword.text.length == 0 && self->textboxOpenDocPassword.text.length == 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kEncryptMissOtherPass") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction *_Nonnull action) {
                                                               [self popoverDidCancel];
                                                           }];
            [alertController addAction:action];
            [self presentAlertController:alertController];
            return;
        }
        if ([self->textboxOpenDocPassword.text isEqualToString:self->textboxOtherPassword.text]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kPasswordNotSame") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK")
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction *_Nonnull action) {
                                                               [self popoverDidCancel];
                                                           }];
            [alertController addAction:action];
            [self presentAlertController:alertController];
            return;
        }
        if (self.optionHandler != nil) {
            NSString *openDocPass = (self->textboxOpenDocPassword.text.length == 0) ? nil : self->textboxOpenDocPassword.text;
            NSString *otherPass = (self->textboxOtherPassword.text.length == 0) ? nil : self->textboxOtherPassword.text;
            if (openDocPass == nil && otherPass == nil) //if none password is enter, tell user to give up
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kEncryptionDoneNoPass") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                                       style:UIAlertActionStyleCancel
                                                                     handler:^(UIAlertAction *action) {
                                                                         self.optionHandler(self, YES, nil, nil, NO, NO, NO, NO, NO, NO, NO, NO);
                                                                         [self close];
                                                                     }];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes") style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:cancelAction];
                [alertController addAction:action];
                [self presentAlertController:alertController];
                return;
            }
            self.optionHandler(self, NO, openDocPass, otherPass, self->switchPrintDoc.on, NO, self->_switchFillForm.on, self->_switchAnnotDoc.on, self->_switchAssembleDoc.on, self->_switchEditDocument.on, self->switchCopyAccessibility.on, self->_switchExtractContent.on);
        }
    }];
}

#pragma mark pop over

- (void)presentAlertController:(UIAlertController *)alertController {
    [[Utility getTopMostViewController] presentViewController:alertController animated:YES completion:nil];
    self.currentVC = alertController;
}

- (void)popoverDidCancel {
    self.currentVC = nil;
}

#pragma mark - Private methods

- (void)initNavigationBar {
    if (!self.navigationItem.titleView) {
        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 170.0f, 44.0f)];
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.frame = CGRectMake(00.0f, 0.0f, 170.0, 44.0f);
        titleLabel.text = FSLocalizedForKey(@"kEncryptionTitle");
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.autoresizesSubviews = YES;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = BlackThemeTextColor;
        titleLabel.font = [UIFont systemFontOfSize:18.0f];
        titleLabel.tag = 2;
        [titleView addSubview:titleLabel];
        self.navigationItem.titleView = titleView;
    }
}

//TODO
- (void)refreshInterface {
    id labelTitle = [cellOpenDoc.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kCellEncryptOpenDocumentTitle");
    }
    labelTitle = [cellPrintDoc.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kCellEncryptPrintDocumentTitle");
    }
    labelTitle = [cellCopyAccessibility.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kCellEncryptCopyContentTitle");
    }
    labelTitle = [_cellAnnotDoc.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kRMSANNOTATE");
    }
    labelTitle = [_cellAssembleDoc.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kRMSASSEMBLE");
    }
    labelTitle = [cellOpenDocPassword.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kCellEncryptPasswordTitle");
    }
    labelTitle = [cellOtherPassword.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kCellEncryptPasswordTitle");
    }
    labelTitle = [_cellAddLimitation.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kPwdDocumentLimitation");
    }
    labelTitle = [_cellFillForm.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kPwdFillForm");
    }
    labelTitle = [_cellEditDocument.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kPwdEditDocument");
    }
    labelTitle = [_cellExtractContent.contentView viewWithTag:100];
    if ([labelTitle isKindOfClass:[UILabel class]]) {
        ((UILabel *) labelTitle).text = FSLocalizedForKey(@"kPwdExtractContent");
    }
    textboxOpenDocPassword.placeholder = FSLocalizedForKey(@"kRequiredPlaceHolder");
    textboxOtherPassword.placeholder = FSLocalizedForKey(@"kRequiredPlaceHolder");
    [buttonEncryptRMS setTitle:FSLocalizedForKey(@"kRMSEncrypt") forState:UIControlStateNormal];
}

@end
