/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

#import "PasswordModule.h"

#import "EncryptOptionViewController.h"
#import "MoreView.h"
#import "SettingBar+private.h"

@interface PasswordSecurityInfo : NSObject
@property (nonatomic, copy) NSString *openPassword;
@property (nonatomic, copy) NSString *ownerPassword;
@property (nonatomic, assign) BOOL allowPrint;
@property (nonatomic, assign) BOOL allowPrintHigh;
@property (nonatomic, assign) BOOL allowFillForm;
@property (nonatomic, assign) BOOL allowAddAnnot;
@property (nonatomic, assign) BOOL allowAssemble;
@property (nonatomic, assign) BOOL allowModify;
@property (nonatomic, assign) BOOL allowCopyForAccess;
@property (nonatomic, assign) BOOL allowCopy;

- (id)initWithOpenPassword:(NSString *)openPassword ownerPassword:(NSString *)ownerPassword print:(BOOL)print printHigh:(BOOL)printHigh fillForm:(BOOL)fillForm addAnnot:(BOOL)addAnnot assemble:(BOOL)assemble modify:(BOOL)modify copyForAccess:(BOOL)copyForAccess copy:(BOOL)copy;
@end

@implementation PasswordSecurityInfo

- (id)initWithOpenPassword:(NSString *)openPassword ownerPassword:(NSString *)ownerPassword print:(BOOL)print printHigh:(BOOL)printHigh fillForm:(BOOL)fillForm addAnnot:(BOOL)addAnnot assemble:(BOOL)assemble modify:(BOOL)modify copyForAccess:(BOOL)copyForAccesst copy:(BOOL)copy {
    if (self = [super init]) {
        self.openPassword = openPassword;
        self.ownerPassword = ownerPassword;
        self.allowPrint = print;
        self.allowPrintHigh = printHigh;
        self.allowFillForm = fillForm;
        self.allowAddAnnot = addAnnot;
        self.allowAssemble = assemble;
        self.allowModify = modify;
        self.allowCopy = copy;
        self.allowCopyForAccess = copyForAccesst;
    }
    return self;
}

@end

typedef void (^PasswordCallBack)(BOOL isInputed, NSString *password);

@interface PasswordModule () <MoreMenuItemAction, IDocEventListener>
@property (nonatomic, strong) PasswordSecurityInfo *securityInfo;
@property (nonatomic, copy) PasswordCallBack passwordCallback;
@property (nonatomic, strong) TSAlertView *currentAlertView;
@property (nonatomic, assign) FSPDFDocPasswordType passwordType;
@property (nonatomic, strong) NSObject *currentVC;
@end

@implementation PasswordModule {
    FSPDFViewCtrl *__weak _pdfViewCtrl;
    UIExtensionsManager *__weak _extensionsManager;
}

- (instancetype)initWithExtensionsManager:(UIExtensionsManager *)extensionsManager {
    if (self = [super init]) {
        _pdfViewCtrl = extensionsManager.pdfViewCtrl;
        _extensionsManager = extensionsManager;

        self.passwordCallback = nil;
        self.inputPassword = nil;
        self.passwordType = FSPDFDocPwdInvalid;

        if (_extensionsManager.config.loadEncryption) {
            MoreMenuGroup *group = [_extensionsManager.more getGroup:TAG_GROUP_PROTECT];
            if (!group) {
                group = [[MoreMenuGroup alloc] init];
                group.tag = TAG_GROUP_PROTECT;

                group.title = FSLocalizedForKey(@"kSecurity");
                [_extensionsManager.more addGroup:group];
            }
            MoreMenuItem *pwdItem = [[MoreMenuItem alloc] init];
            pwdItem.tag = TAG_ITEM_PASSWORD;
            pwdItem.text = FSLocalizedForKey(@"kEncryption");
            pwdItem.callBack = self;
            [_extensionsManager.more addMenuItem:TAG_GROUP_PROTECT withItem:pwdItem];
        }

        [_pdfViewCtrl registerDocEventListener:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getInputPassword:) name:@"NSNotificationNameInputPassword" object:nil];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getInputPassword:(NSNotification *)not{
    self.inputPassword = not.object;
}

#pragma mark - TSAlertViewDelegate

- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.currentAlertView = nil;

    BOOL isCancelled = (buttonIndex == 0);
    self.inputPassword = isCancelled ? nil : alertView.inputTextField.text;

    if (self.passwordCallback) {
        self.passwordCallback(!isCancelled, self.inputPassword);
    }
}

// prompt an alert view to input user password
- (void)promptForPasswordWithTitle:(NSString *)title callback:(PasswordCallBack)callback {
    self.passwordCallback = callback;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.currentAlertView = [self createAlertViewToInputPassword];
        self.currentAlertView.tag = 2; // user password
        self.currentAlertView.title = title;
        [self.currentAlertView show];
    });
}

- (void)tryLoadPDFDocument:(FSPDFDoc *)document guessPassword:(NSString *)guessPassword success:(void (^)(NSString *password))success error:(void (^)(NSString *description))error abort:(void (^)(void))abort {
    self.pdfDoc = document;
    __weak typeof(self) weakSelf = self;
    FSErrorCode status = [document load:guessPassword];
    if (status == FSErrSuccess) {
        if (success) {
            success(guessPassword);
        }
    } else if (status == FSErrPassword) {
        NSString *title = guessPassword ? FSLocalizedForKey(@"kDocPasswordError") : FSLocalizedForKey(@"kDocNeedPassword");
        [self promptForPasswordWithTitle:title
                                callback:^(BOOL isInputed, NSString *newPassword) {
                                    if (isInputed) {
                                        [weakSelf tryLoadPDFDocument:document guessPassword:newPassword success:success error:error abort:abort];
                                    } else if (abort) {
                                        abort();
                                    }
                                }];
    } else {
        if (error) {
            error([Utility getErrorCodeDescription:status]);
        }
    }
}

- (TSAlertView *)createAlertViewToInputPassword {
    TSAlertView *alertView = [[TSAlertView alloc] init];
    [alertView addButtonWithTitle:FSLocalizedForKey(@"kCancel")];
    [alertView addButtonWithTitle:FSLocalizedForKey(@"kOK")];
    UIButton *sureBtn = alertView.buttons.lastObject;
    sureBtn.enabled = NO;
    alertView.style = TSAlertViewStyleInputText;
    alertView.buttonLayout = TSAlertViewButtonLayoutNormal;
    alertView.usesMessageTextView = NO;
    alertView.inputTextField.secureTextEntry = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inputTextFieldChange:) name:UITextFieldTextDidChangeNotification object:alertView.inputTextField];
    alertView.delegate = self;
    [sureBtn setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
    return alertView;
}

- (void)inputTextFieldChange:(NSNotification *)aNotification {
    if ([self.currentAlertView.inputTextField isEqual:aNotification.object]) {
        UIButton *sureBtn = self.currentAlertView.buttons.lastObject;
        if (((UITextField *) aNotification.object).text.length != 0) {
            sureBtn.enabled = YES;
            [sureBtn setTitleColor:[UIColor colorWithRed:0 / 255.0 green:122.0 / 255.0 blue:255.0 / 255.0 alpha:1] forState:UIControlStateNormal];
        } else {
            sureBtn.enabled = NO;
            [sureBtn setTitleColor:GrayThemeTextColor forState:UIControlStateNormal];
        }
    }
}

#pragma mark - MoreMenuItemAction

- (void)onClick:(MoreMenuItem *)item {
    if ([_pdfViewCtrl isDynamicXFA]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kXFAUnsupportedOP") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    if ([_pdfViewCtrl isRMSProtected]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kWarning") message:FSLocalizedForKey(@"kRMSAlreadyEncrypt") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    item.enable = NO;
    
    if (self.pdfDoc.isProtected) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kHadProtect") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        self.currentVC = alertController;

        item.enable = YES;
        return;
    }
    
    if ([item.text isEqualToString:FSLocalizedForKey(@"kEncryption")]) {
        [self handleEncryptDocument];
    } else {
        [self handleRemoveEncryption];
    }
    item.enable = YES;
}

- (BOOL)isOwner {
    if (self.passwordType == FSPDFDocPwdInvalid) {
        [self updatePasswordType];
    }
    if (self.passwordType == FSPDFDocPwdNoPassword ||
        self.passwordType == FSPDFDocPwdOwner) {
        return YES;
    }
    return NO;
}

-(void)exitCropMode{
    [_pdfViewCtrl setCropMode:PDF_CROP_MODE_NONE];
}

- (BOOL)encryptDocument:(FSPDFDoc *)doc {
    NSString *originalPDFPath = _pdfViewCtrl.filePath;
    NSString *encryptedPDFPath = nil;

    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isOK = NO;

    @autoreleasepool {
        FSStdSecurityHandler *stdSecurityHandler = [[FSStdSecurityHandler alloc] init];
        FSStdEncryptData *data = [[FSStdEncryptData alloc] init];
        data.cipher = FSSecurityHandlerCipherAES;
        data.key_length = 16;
        data.is_encrypt_metadata = YES;
        data.user_permissions = [self permissionsFromInfo];
        isOK = [stdSecurityHandler initialize:data user_password:self.securityInfo.openPassword owner_password:self.securityInfo.ownerPassword];
        if (isOK) {
            isOK = [doc setSecurityHandler:stdSecurityHandler];
        }

        if (isOK) {
            NSString *fileName = [originalPDFPath lastPathComponent] ?: @".pdf";
            encryptedPDFPath = [NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"_encryped_%@", fileName]];

            if ([fileManager fileExistsAtPath:encryptedPDFPath]) {
                [fileManager removeItemAtPath:encryptedPDFPath error:nil];
            }
            isOK = [doc saveAs:encryptedPDFPath save_flags:FSPDFDocSaveFlagNormal];
        }
        if (isOK) {
            isOK = [fileManager removeItemAtPath:originalPDFPath error:nil];
        }
        if (isOK) {
            isOK = [fileManager moveItemAtPath:encryptedPDFPath toPath:originalPDFPath error:nil];
        }
        if (isOK) {
            [self exitCropMode];
            NSString *password = self.securityInfo.ownerPassword ?: self.securityInfo.openPassword;
            [_pdfViewCtrl openDoc:originalPDFPath password:password completion:nil];
        }
    }
    return isOK;
}

- (BOOL)removeEncrypt:(FSPDFDoc *)pdfDoc {
    BOOL isOK = NO;
    NSString *decryptedPDFPath = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];

    @autoreleasepool {
        isOK = [pdfDoc removeSecurity];
        if (isOK) {
            NSString *fileName = [_pdfViewCtrl.filePath lastPathComponent];
            decryptedPDFPath = [NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"_decryped_%@", fileName]];
            if ([fileManager fileExistsAtPath:decryptedPDFPath isDirectory:nil]) {
                [fileManager removeItemAtPath:decryptedPDFPath error:nil];
            }
            isOK = [pdfDoc saveAs:decryptedPDFPath save_flags:FSPDFDocSaveFlagNormal];
        }
        if (isOK) {
            isOK = [fileManager removeItemAtPath:_pdfViewCtrl.filePath error:nil];

            if (isOK) {
                isOK = [fileManager moveItemAtPath:decryptedPDFPath toPath:_pdfViewCtrl.filePath error:nil];
            }
        }
        if (isOK) {
            [self exitCropMode];
            NSString *filePath = _pdfViewCtrl.filePath;
            /* close Document first.*/
            @autoreleasepool {
                [_pdfViewCtrl closeDoc:nil];
                _pdfViewCtrl.currentDoc = nil;
            }
            [_pdfViewCtrl openDoc:filePath password:nil completion:nil];
        }
    }

    return isOK;
}

#pragma mark - IDocEventListener

- (void)onDocOpened:(FSPDFDoc *)document error:(int)error {
    if (error != FSErrSuccess)
        return;
    self.pdfDoc = document;

    [self updatePasswordType];

    NSString *text = FSLocalizedForKey(@"kEncryption");
    if (self.passwordType == FSPDFDocPwdUser ||
        self.passwordType == FSPDFDocPwdOwner) {
        text = FSLocalizedForKey(@"kDecryption");
    }
    [self updateItemText:text];
}

- (void)onDocWillClose:(FSPDFDoc *)document {
    if (self.currentVC) {
        if ([self.currentVC isKindOfClass:[UIAlertController class]]) {
            [(UIAlertController *) self.currentVC dismissViewControllerAnimated:NO completion:nil];
        } else if ([self.currentVC isKindOfClass:[EncryptOptionViewController class]]) {
            if ([((EncryptOptionViewController *) self.currentVC).currentVC isKindOfClass:[UIAlertController class]]) {
                UIAlertController *alert = (UIAlertController *) ((EncryptOptionViewController *) self.currentVC).currentVC;
                [alert dismissViewControllerAnimated:NO completion:nil];
            }
            ((EncryptOptionViewController *) self.currentVC).currentVC = nil;
            [(EncryptOptionViewController *) self.currentVC dismissViewControllerAnimated:NO completion:nil];
        }
        self.currentVC = nil;
    }
}

- (void)onDocClosed:(FSPDFDoc *)document error:(int)error {
    self.pdfDoc = nil;
    self.passwordType = FSPDFDocPwdInvalid;
}

#pragma mark - encrypt document

- (void)handleEncryptDocument {
    EncryptOptionViewController *encryptOptionCtrl = [[EncryptOptionViewController alloc] initWithNibName:@"EncryptOptionViewController" bundle:[NSBundle bundleForClass:[self class]]];
    self.currentVC = encryptOptionCtrl;

    __weak typeof(self) weakSelf = self;
    NSString *path = _pdfViewCtrl.filePath;

    encryptOptionCtrl.optionHandler = ^(EncryptOptionViewController *ctrl, BOOL isCancel, NSString *openPassword, NSString *ownerPassword, BOOL print, BOOL printHigh, BOOL fillForm, BOOL addAnnot, BOOL assemble, BOOL modify, BOOL copyForAccess, BOOL copy) {
        if (isCancel)
            return;
        if (openPassword == nil && ownerPassword == nil) {
            return;
        }

        self.securityInfo = [[PasswordSecurityInfo alloc] initWithOpenPassword:openPassword ownerPassword:ownerPassword print:print printHigh:printHigh fillForm:fillForm addAnnot:addAnnot assemble:assemble modify:modify copyForAccess:copyForAccess copy:copy];

        if (self->_extensionsManager.isDocModified) { // whether to save before encryption?
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kShouldSaveEncryptionDocument") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction *action) {
                                                                     FSPDFDoc *pdfDoc = [[FSPDFDoc alloc] initWithPath:path];
                                                                     FSErrorCode error = [pdfDoc load:weakSelf.inputPassword];
                                                                     if (error == FSErrSuccess) {
                                                                         weakSelf.pdfDoc = pdfDoc;
                                                                     } else {
                                                                         //todo
                                                                     }
                                                                     [weakSelf encryptDocument:weakSelf.pdfDoc];

                                                                 }];
            UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [weakSelf encryptDocument:weakSelf.pdfDoc];

                                                           }];
            [alertController addAction:cancelAction];
            [alertController addAction:action];
            [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
            self.currentVC = alertController;
        } else {
            [self encryptDocument:openPassword ownerPassword:ownerPassword print:print printHigh:printHigh fillForm:fillForm addAnnot:addAnnot modify:modify assemble:assemble copyForAccess:copyForAccess copy:copy];
        }
    };

    // show permission setting option
    dispatch_async(dispatch_get_main_queue(), ^{
        FSNavigationController *encryptOptionNavCtrl = [[FSNavigationController alloc] initWithRootViewController:encryptOptionCtrl];
        encryptOptionNavCtrl.delegate = encryptOptionCtrl;
        if (DEVICE_iPHONE) {
            encryptOptionNavCtrl.modalPresentationStyle = UIModalPresentationCustom;
        } else {
            encryptOptionNavCtrl.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        UIViewController *rootViewController = self->_pdfViewCtrl.fs_viewController;
        [rootViewController presentViewController:encryptOptionNavCtrl animated:YES completion:nil];
    });
}

- (void)encryptDocument:(NSString *)openPassword ownerPassword:(NSString *)ownerPassword print:(BOOL)print printHigh:(BOOL)printHigh fillForm:(BOOL)fillForm addAnnot:(BOOL)addAnnot modify:(BOOL)modify assemble:(BOOL)assemble copyForAccess:(BOOL)copyForAccess copy:(BOOL)copy {
    _extensionsManager.currentAnnot = nil;
    _extensionsManager.currentToolHandler = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL isOK = [self encryptDocument:self.pdfDoc];
            if (!isOK) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kTryAgain") message:FSLocalizedForKey(@"kEncryptAddFail") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo") style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   [self encryptDocument:openPassword ownerPassword:ownerPassword print:print printHigh:printHigh fillForm:fillForm addAnnot:addAnnot modify:modify assemble:assemble copyForAccess:copyForAccess copy:copy];
                                                               }];
                [alertController addAction:cancelAction];
                [alertController addAction:action];
                [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                self.currentVC = alertController;
            } else {
                [self updatePasswordType];
                [self updateItemText:FSLocalizedForKey(@"kDecryption")];

                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kEncryptAddSuccess") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:action];
                [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                self.currentVC = alertController;

                // if this file is encrypted by open doc password,
                // clear thumbnail cache. other cache is remained as user can still open it and see.
                if (openPassword != nil) {
                }
            }
        });
    });
}

- (void)handleRemoveEncryption {
    if (![self isEncrypted]) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    typedef void (^SuccessCallback)(void);
    __block void (^promptForOwnerPassword)(NSString *title, SuccessCallback success) = ^(NSString *title, SuccessCallback success) {
        [weakSelf promptForPasswordWithTitle:title
                                    callback:^(BOOL isInputed, NSString *password) {
                                        if (isInputed) {
                                            BOOL isOwner = ([weakSelf.pdfDoc getPasswordType] == FSPDFDocPwdNoPassword) || ([weakSelf.pdfDoc checkPassword:password] == FSPDFDocPwdOwner);
                                            if (!isOwner) {
                                                promptForOwnerPassword(FSLocalizedForKey(@"kDocPasswordError"), success);
                                            } else if (success) {
                                                promptForOwnerPassword = nil; // fix strong-reference-cycle
                                                success();
                                            }
                                        } else {
                                            promptForOwnerPassword = nil; // fix strong-reference-cycle
                                        }
                                    }];
    };
    void (^tryRemoveEncryption)(void) = ^{
        if ([weakSelf isOwner]) {
            [weakSelf removeEncryption];
        } else {
            promptForOwnerPassword(FSLocalizedForKey(@"kDocNeedPassword"), ^{
                [weakSelf removeEncryption];
            });
        }
    };
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kEncryptRemovePass") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             self.currentVC = nil;
                                                         }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       if (self->_extensionsManager.isDocModified) { // whether to save before remove encryption?
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kConfirm") message:FSLocalizedForKey(@"kShouldSaveEecryptionDocument") preferredStyle:UIAlertControllerStyleAlert];
                                                           UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                                                                                  style:UIAlertActionStyleCancel
                                                                                                                handler:^(UIAlertAction *action) {
                                                                                                                    FSPDFDoc *pdfDoc = [[FSPDFDoc alloc] initWithPath:self->_pdfViewCtrl.filePath];
                                                                                                                    FSErrorCode error = [pdfDoc load:self.inputPassword];
                                                                                                                    if (error == FSErrSuccess) {
                                                                                                                        weakSelf.pdfDoc = pdfDoc;
                                                                                                                    } else {
                                                                                                                        //todo
                                                                                                                    }
                                                                                                                    self.currentVC = nil;
                                                                                                                    tryRemoveEncryption();
                                                                                                                    
                                                                                                                }];
                                                           UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                                                                            style:UIAlertActionStyleDefault
                                                                                                          handler:^(UIAlertAction *action) {
                                                                                                              self.currentVC = nil;
                                                                                                              tryRemoveEncryption();
                                                                                                          }];
                                                           [alertController addAction:cancelAction];
                                                           [alertController addAction:action];
                                                           [self->_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
                                                           self.currentVC = alertController;
                                                       }else{
                                                           tryRemoveEncryption();
                                                           self.currentVC = nil;
                                                       }
                                                   }];
    [alertController addAction:cancelAction];
    [alertController addAction:action];
    [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
    self.currentVC = alertController;
}

- (void)removeEncryption {
    _extensionsManager.currentAnnot = nil;
    _extensionsManager.currentToolHandler = nil;

    BOOL isOK = [self removeEncrypt:self.pdfDoc];
    if (!isOK) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kTryAgain") message:FSLocalizedForKey(@"kEncryptAddFail") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kNo")
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 self.currentVC = nil;
                                                             }];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kYes")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [self removeEncryption];
                                                           self.currentVC = nil;
                                                       }];
        [alertController addAction:cancelAction];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        self.currentVC = alertController;
    } else {
        [self updateItemText:FSLocalizedForKey(@"kEncryption")];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kSuccess") message:FSLocalizedForKey(@"kEncryptRemoveSuccess") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kOK") style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [_pdfViewCtrl.fs_viewController presentViewController:alertController animated:YES completion:nil];
        self.currentVC = alertController;
    }
}

#pragma mark - private method

- (BOOL)isEncrypted {
    return [self.pdfDoc getEncryptionType] == FSPDFDocEncryptPassword;
}

- (void)updatePasswordType {
    self.passwordType = [self.pdfDoc getPasswordType];
}

- (void)updateItemText:(NSString *)text {
    MoreMenuGroup *securityGroup = [_extensionsManager.more getGroup:TAG_GROUP_PROTECT];
    for (MoreMenuItem *item in [securityGroup getItems]) {
        if (item.tag == TAG_ITEM_PASSWORD) {
            item.text = text;
            break;
        }
    }
    [_extensionsManager.more reloadData];
}

- (int)permissionsFromInfo {
    unsigned int permission = 0x0;
    if (self.securityInfo.allowPrint)
        permission |= FSPDFDocPermPrint;
    if (self.securityInfo.allowPrintHigh)
        permission |= FSPDFDocPermPrint | FSPDFDocPermPrintHigh;
    if (self.securityInfo.allowFillForm)
        permission |= FSPDFDocPermFillForm;
    if (self.securityInfo.allowAddAnnot)
        permission |= FSPDFDocPermAnnotForm | FSPDFDocPermFillForm;
    if (self.securityInfo.allowAssemble)
        permission |= FSPDFDocPermAssemble;
    if (self.securityInfo.allowModify)
        permission |= FSPDFDocPermModify | FSPDFDocPermAssemble | FSPDFDocPermFillForm;
    if (self.securityInfo.allowCopyForAccess)
        permission |= FSPDFDocPermExtractAccess;
    if (self.securityInfo.allowCopy)
        permission |= FSPDFDocPermExtract | FSPDFDocPermExtractAccess;
    return permission;
}

@end

