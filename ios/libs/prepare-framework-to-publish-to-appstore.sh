#!/bin/sh

# Copyright (C) 2003-2021, Foxit Software Inc..
# All Rights Reserved.
#
# http://www.foxitsoftware.com
#
# The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
# distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
# is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
# Review legal.txt for additional license and legal information.
#

# Foxit RDK binaries are combined of arm64 x86_64 architectures. However, when RDK binaries are embedded to app and uploaded to app store,
# app may be refused because of "Invalid Binary Architecture" error, which means that x86_64 architectures are not allowed to be embedded to upload.
# So the following shell scripts are used to strip the arm architectures, and output the libraries to path "./device/FoxitRDK.framework" , "./device/uiextensionsDynamic.framework" and "./device/FoxitPDFScanUI.framework", RDK developers should use these output frameworks instead when publishing to Apple App Store.

#Run this shell script as "sh ./prepare-framework-to-publish-to-appstore.sh".

if [ ! -d "./device/FoxitRDK.framework" ]; then
mkdir -p ./device/FoxitRDK.framework
fi
cp -frp "./FoxitRDK.framework/" "./device/FoxitRDK.framework"
echo "Extracting arm64 architectures..."
lipo -extract "arm64" "FoxitRDK.framework/FoxitRDK" -output "device/FoxitRDK.framework/FoxitRDK"
echo "Output the finial library to ./device/FoxitRDK.framework."

if [ ! -d "./device/uiextensionsDynamic.framework" ]; then
mkdir -p ./device/uiextensionsDynamic.framework
fi
cp -frp "./uiextensionsDynamic.framework/" "./device/uiextensionsDynamic.framework"
echo "Extracting arm64 architectures..."
lipo -extract "arm64" "uiextensionsDynamic.framework/uiextensionsDynamic" -output "device/uiextensionsDynamic.framework/uiextensionsDynamic"
echo "Output the finial library to ./device/uiextensionsDynamic.framework."

if [ ! -d "./device/FoxitPDFScanUI.framework" ]; then
mkdir -p ./device/FoxitPDFScanUI.framework
fi
cp -frp "./FoxitPDFScanUI.framework/" "./device/FoxitPDFScanUI.framework"
echo "Extracting arm64 architectures..."
lipo -extract "arm64" "FoxitPDFScanUI.framework/FoxitPDFScanUI" -output "device/FoxitPDFScanUI.framework/FoxitPDFScanUI"
echo "Output the finial library to ./device/FoxitPDFScanUI.framework."
echo "Done."

