/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/
#import <Foundation/Foundation.h>
#import <FoxitRDK/FSPDFObjC.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ScanPDFSaveAsCallBack) (NSError * _Nullable error, NSString * _Nullable savePath);
@interface PDFScanManager : NSObject

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

/** @brief Callback after the file is saved.*/
@property (class, nonatomic, copy) ScanPDFSaveAsCallBack saveAsCallBack;

/** @brief Initialize the FoxitMobileRDKScanning module with additional parameters.
*
* @details Successful initialization of the SDK requires a valid serial number.
*
* @param[in]    serial1    First part of the serial number.
* @param[in]    serial2    Second part of the serial number.
* @return       FSErrSuccess when the serial number is valid and initialization is successful.
*               FSErrInvalidLicense when the serial number is invalidand initialization fails.
*               FSErrRightsExpired when serial number is valid but expired.
*/
+ (FSErrorCode)initializeScanner:(unsigned long)serial1 serial2:(unsigned long)serial2;

/** @brief Initialize the FoxitMobileRDKScanning Compression module with additional parameters.
*
* @details Successful initialization of the SDK requires a valid serial number.
*
* @param[in]    serial1    First part of the serial number.
* @param[in]    serial2    Second part of the serial number.
* @return       FSErrSuccess when the serial number is valid and initialization is successful.
*               FSErrInvalidLicense when the serial number is invalidand initialization fails.
*               FSErrRightsExpired when serial number is valid but expired.
*/
+ (FSErrorCode)initializeCompression:(unsigned long)serial1 serial2:(unsigned long)serial2;

/** @brief      Get the PDFScan controller.
*
* @details      Scan module start interface.
* @return       UIViewController    A controller object.
*/
+ (__kindof UIViewController *)getPDFScanView;
@end

NS_ASSUME_NONNULL_END
