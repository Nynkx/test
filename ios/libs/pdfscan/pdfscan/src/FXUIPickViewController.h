/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

@class FXUIPickViewController;

@protocol IFXUIPickViewControllerDelegate <NSObject>

- (void)FXUIPickViewController:(FXUIPickViewController *)pickViewController Image:(UIImage *)image counts:(int)counts currentIndex:(int)currentIndex;

@end

@interface FXUIPickViewController : UIViewController
@property (nonatomic, weak) id<IFXUIPickViewControllerDelegate> delegate;
@property (nonatomic, assign) BOOL isInDocument;
@property (nonatomic, assign) BOOL allowSingleSelect;
@end
