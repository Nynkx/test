/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FXUIPickViewController.h"
#import "FXImagePickThumbnailCell.h"
#import <Photos/Photos.h>

@interface FXUIPickViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) FSToolbar *topBar;
@property (nonatomic, strong) UICollectionView *contentView;
@property (nonatomic, strong) PHFetchResult *assetsFetchResults;
@property (nonatomic, strong) NSMutableArray *selectImageIndexs;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UIButton *doneBtn;
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation FXUIPickViewController

- (instancetype)init
{
    if (self = [super init]) {
        _isInDocument = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = ThemeNavBarColor;
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.translucent = YES;
    
    _selectImageIndexs = [NSMutableArray array];
    
    self.view.backgroundColor = ThemeCellBackgroundColor;
    //[self initSubView];
    
    _assetsFetchResults = [self loadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    [self initSubView];
}

- (void)initSubView
{
    _topBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
    [self.view addSubview:_topBar];
    
    [_topBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
    }];
    
    _backBtn = [[UIButton alloc] init];
    _backBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    _backBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_backBtn setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    CGSize sizeCancel = [Utility getTextSize:FSLocalizedForKey(@"kCancel") fontSize:15 maxSize:CGSizeMake(150, 44)];
    [_backBtn setTitleColor:BarLikeBlackTextColor forState:UIControlStateNormal];
    [_backBtn setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [_backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_topBar addSubview:_backBtn];
    
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(sizeCancel.width);
        make.top.bottom.mas_equalTo(_backBtn.superview);
        make.left.mas_equalTo(_backBtn.superview).offset(ITEM_MARGIN_NORMAL);
        make.centerY.mas_equalTo(_backBtn.superview);
    }];
    
    _doneBtn = [[UIButton alloc] init];
    _doneBtn.enabled = NO;
    _doneBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    _doneBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    CGSize sizeOK = [Utility getTextSize:FSLocalizedForKey(@"kDone") fontSize:15 maxSize:CGSizeMake(150, 44)];
    [_doneBtn setTitle:FSLocalizedForKey(@"kDone") forState:UIControlStateNormal];
    [_doneBtn setTitleColor:BarLikeBlackTextColor forState:UIControlStateNormal];
    [_doneBtn setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [_doneBtn setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
    [_doneBtn addTarget:self action:@selector(doneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_topBar addSubview:_doneBtn];
    
    [_doneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(sizeOK.width);
        make.top.bottom.mas_equalTo(_doneBtn.superview);
        make.right.mas_equalTo(_doneBtn.superview).offset(-ITEM_MARGIN_NORMAL);
        make.centerY.mas_equalTo(_doneBtn.superview);
    }];
    
    UILabel *title = [[UILabel alloc] init];
    _titleLabel = title;
    title.text = FSLocalizedForKey(@"kConvertPhotos");
    title.font = [UIFont systemFontOfSize:18];
    title.textColor = BarLikeBlackTextColor;
    title.textAlignment = NSTextAlignmentCenter;
    [_topBar addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(title.superview);
    }];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(DEVICE_iPHONE ? 78 : 132, DEVICE_iPHONE ? 78 : 132);
    layout.minimumInteritemSpacing = DEVICE_iPHONE ? 2 : 20;
    layout.minimumLineSpacing = DEVICE_iPHONE ? 2 : 20;
    layout.sectionInset = UIEdgeInsetsMake(DEVICE_iPHONE ? 0 : 20, DEVICE_iPHONE ? 0 : 14, 0, DEVICE_iPHONE ? 0 : 14);
    _contentView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    _contentView.delegate = self;
    _contentView.dataSource = self;
    [_contentView registerClass:[FXImagePickThumbnailCell class] forCellWithReuseIdentifier:@"cellid"];
    _contentView.backgroundColor = ThemeCellBackgroundColor;
    [self.view addSubview:_contentView];

    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topBar.mas_bottom);
        make.left.right.bottom.mas_equalTo(_contentView.superview);
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _assetsFetchResults.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FXImagePickThumbnailCell * cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellid" forIndexPath:indexPath];
    PHImageManager *imageManager = [PHImageManager defaultManager];
    PHAsset *asset = _assetsFetchResults[indexPath.item];
    [imageManager requestImageForAsset:asset targetSize:CGSizeMake(DEVICE_iPHONE ? 78 : 132, DEVICE_iPHONE ? 78 : 132) contentMode:PHImageContentModeAspectFill options:[[PHImageRequestOptions alloc] init] resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        cell.thumbImage = result;
        if ([self->_selectImageIndexs containsObject:indexPath])
        {
            cell.isHighlight = YES;
            cell.isDisable = NO;
        }else{
            cell.isHighlight = NO;
            if (self->_selectImageIndexs.count == 9) {
                cell.isDisable = YES;
            }else{
                cell.isDisable = NO;
            }
        }
    }];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FXImagePickThumbnailCell *cell = (FXImagePickThumbnailCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (self.allowSingleSelect) {
        [_selectImageIndexs removeAllObjects];
        [_selectImageIndexs addObject:indexPath];
        [self changedSelectImageIndexs];
        [collectionView reloadData];
        return;
    }
    if ([_selectImageIndexs containsObject:indexPath]) {
        [_selectImageIndexs removeObject:indexPath];
        [self changedSelectImageIndexs];
        if (_selectImageIndexs.count == 8) {
            [collectionView reloadData];
        }else{
           cell.isHighlight = NO;
        }
    }else{
        if (_selectImageIndexs.count >= 9) {
            AlertViewCtrlShow(FSLocalizedForKey(@"kWarning"), FSLocalizedForKey(@"kImageLimit"));
            return;
        }
        [_selectImageIndexs addObject:indexPath];
        [self changedSelectImageIndexs];
        if (_selectImageIndexs.count == 9) {
            [collectionView reloadData];
        }else{
            cell.isHighlight = YES;
        }
    }
}

- (PHFetchResult *)loadData
{
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    options.includeAssetSourceTypes = PHAssetSourceTypeUserLibrary | PHAssetSourceTypeiTunesSynced;
    PHFetchResult *assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
    return assetsFetchResults;
}

- (void)backBtnClick:(UIButton *)btn
{
    if ([self.delegate respondsToSelector:@selector(FXUIPickViewController:Image:counts:currentIndex:)]) {
        [self.delegate FXUIPickViewController:self Image:nil counts:0 currentIndex:0];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneBtnClick:(UIButton *)btn
{
    for (int i = 0; i < _selectImageIndexs.count; i++) {
        PHImageManager *imageManager = [PHImageManager defaultManager];
        PHAsset *asset = _assetsFetchResults[[(NSIndexPath *)_selectImageIndexs[i] item]];
        [imageManager requestImageDataForAsset:asset options:[[PHImageRequestOptions alloc] init] resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            UIImage *image = [UIImage imageWithData:imageData];
            
            if (i == self->_selectImageIndexs.count -1) {
                [self dismissViewControllerAnimated:YES completion:^{
                    if ([self.delegate respondsToSelector:@selector(FXUIPickViewController:Image:counts:currentIndex:)]) {
                        [self.delegate FXUIPickViewController:self Image:image counts:(int)self->_selectImageIndexs.count currentIndex:i];
                    }
                }];
            }else{
                if ([self.delegate respondsToSelector:@selector(FXUIPickViewController:Image:counts:currentIndex:)]) {
                    [self.delegate FXUIPickViewController:self Image:image counts:(int)self->_selectImageIndexs.count currentIndex:i];
                }
            }
        }];
    }
}

- (void)changedSelectImageIndexs
{
    if (_selectImageIndexs.count >= 1) {
        _doneBtn.enabled = YES;
    }else{
        _doneBtn.enabled = NO;
    }
}
@end
