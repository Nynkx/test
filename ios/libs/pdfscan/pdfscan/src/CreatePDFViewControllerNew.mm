/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "CreatePDFViewControllerNew.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "LTCanmeraViewControllerNew.h"
#import "LTDocumentEditViewControllerNew.h"
#import "PDFScanManager+private.h"
#import "UITableView+EmptyData.h"

@interface LTTransListCell : UITableViewCell
@property (nonatomic, strong) UIImageView *preImageView;
@property (nonatomic, strong) UILabel *documentTitleLabel;
@property (nonatomic, strong) UILabel *documentTimeLabel;

@property (nonatomic, strong) UIButton *moreBtn;
@property (nonatomic, strong) UIView *moreContentView;
@property (nonatomic, strong) TbBaseItem *deleteItem;

@property (nonatomic, assign) BOOL showMoreContentView;
@property (nonatomic, copy)   void(^moreShowClick)(LTTransListCell *cell);
@property (nonatomic, copy)   void(^deleteItemClick)(LTTransListCell *cell);
@end

@implementation LTTransListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _preImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_preImageView];
        [_preImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_preImageView.superview.mas_left).offset(10);
            make.centerY.mas_equalTo(_preImageView.superview.mas_centerY);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(30);
        }];
        
        _documentTitleLabel = [[UILabel alloc] init];
        _documentTitleLabel.textColor = BarLikeBlackTextColor;
        _documentTitleLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_documentTitleLabel];
        [_documentTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_preImageView.mas_right).offset(10);
            make.right.mas_equalTo(_documentTitleLabel.superview.mas_right).offset(-60);
            make.bottom.mas_equalTo(_documentTitleLabel.superview.mas_bottom).multipliedBy(0.5).offset(-2);
        }];
        
        _documentTimeLabel = [[UILabel alloc] init];
        _documentTimeLabel.textColor = SubjectLabTextColor;
        _documentTimeLabel.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_documentTimeLabel];
        [_documentTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_preImageView.mas_right).offset(10);
            make.right.mas_equalTo(_documentTimeLabel.superview.mas_right).offset(-60);
            make.top.mas_equalTo(_documentTimeLabel.superview.mas_bottom).multipliedBy(0.5).offset(2);
        }];
        
        _moreBtn = [[UIButton alloc] init];
        _moreBtn.accessibilityLabel = FSLocalizedForKey(@"kDocAct");
        [_moreBtn setImage:ImageNamed(@"document_cellmore_more") forState:UIControlStateNormal];
        [_moreBtn addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_moreBtn];
        [_moreBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_moreBtn.superview.mas_right);
            make.centerY.mas_equalTo(_moreBtn.superview.mas_centerY);
            make.width.mas_equalTo(68);
            make.height.mas_equalTo(68);
        }];
        _moreContentView = [[UIView alloc] init];
        _moreContentView.backgroundColor = OptionMoreColor;
        _moreContentView.hidden = YES;
        [self.contentView addSubview:_moreContentView];

        _deleteItem = [TbBaseItem createItemWithImageAndTitle:FSLocalizedForKey(@"kDelete") imageNormal:ImageNamed(@"thumb_delete_black") imageSelected:ImageNamed(@"thumb_delete_black") imageDisable:ImageNamed(@"thumb_delete_black") background:nil imageTextRelation:RELATION_BOTTOM];
        _deleteItem.textColor = BarLikeBlackTextColor;
        _deleteItem.textFont = [UIFont systemFontOfSize:12.0f];
        @weakify(self)
        _deleteItem.onTapClick = ^(TbBaseItem *item){
            @strongify(self)
            self.deleteItemClick(self);
        };
        
        
        [self.moreContentView addSubview:_deleteItem.contentView];
        CGSize size = _deleteItem.contentView.frame.size;
        [_deleteItem.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_deleteItem.contentView.superview.mas_right).offset(-10);
            make.centerY.mas_equalTo(_deleteItem.contentView.superview.mas_centerY).offset(-5);
            make.width.mas_equalTo(size.width);
            make.height.mas_equalTo(size.height);
        }];
        
        [self.moreContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(size.width + 20);
            make.right.mas_equalTo(_moreContentView.superview.mas_right).offset(size.width + 20);
            make.top.mas_equalTo(_moreContentView.superview.mas_top);
            make.bottom.mas_equalTo(_moreContentView.superview.mas_bottom);
        }];
        
        UIView *divideView = [[UIView alloc] init];
        divideView.backgroundColor = DividingLineColor;
        [self addSubview:divideView];
        [divideView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(divideView.superview.mas_left).offset(0);
            make.height.mas_equalTo([Utility realPX:1.0]);
            make.bottom.mas_equalTo(divideView.superview.mas_bottom);
            make.right.mas_equalTo(divideView.superview.mas_right).offset(0);
        }];
    }
    return self;
}

- (void)moreBtnClick:(UIButton *)btn
{
    self.moreShowClick(self);
}

- (void)setShowMoreContentView:(BOOL)showMoreContentView
{
    CGSize size = _deleteItem.contentView.frame.size;
    if (showMoreContentView) {
        self.moreBtn.hidden = YES;
        [UIView animateWithDuration:0.3 animations:^{
            self.moreContentView.hidden = NO;
            [self.moreContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(size.width + 20);
                make.right.mas_equalTo(self->_moreContentView.superview.mas_right);
                make.top.mas_equalTo(self->_moreContentView.superview.mas_top);
                make.bottom.mas_equalTo(self->_moreContentView.superview.mas_bottom);
            }];
            [self.moreContentView.superview layoutIfNeeded];
        }];
    }else
    {
        self.moreBtn.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.moreContentView.hidden = YES;
            [self.moreContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self->_moreContentView.superview.mas_right).offset(size.width + 20);
                make.width.mas_equalTo(size.width + 20);
                make.top.mas_equalTo(self->_moreContentView.superview.mas_top);
                make.bottom.mas_equalTo(self->_moreContentView.superview.mas_bottom);
            }];
        }];
        [self.moreContentView.superview layoutIfNeeded];
    }
}

@end

static NSString *const ltTransListCellID = @"ltTransListCellID";

@interface CreatePDFViewControllerNew ()<UITableViewDelegate,UITableViewDataSource,LTDocumentSessionDelegate>
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic, strong) UITableView *transList;
@property (nonatomic, copy) NSArray *transListDataSource;
@property (nonatomic, weak) LTDocumentManager *documentMgr;
@property (nonatomic, strong) NSIndexPath *oldIndexPath;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) UILongPressGestureRecognizer *longGesture;
@property (nonatomic, strong) UIView *recentView;
@property (nonatomic, strong) UIView *recentLabel;
@property (nonatomic, strong) UIView *topView;
@end

@implementation CreatePDFViewControllerNew

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSubview];
    
    LTDocumentManager *documentManager = [[LTDocumentManager alloc] init];
    self.documentMgr = documentManager;
    [PDFScanManager shareManager].documentManager = documentManager;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createTransListDataSource{
    if ([PDFScanManager shareManager].isTrial) {
        NSMutableArray *tmp = [NSMutableArray array];
        for (LTDocument *document in self.documentMgr.documents) {
            if (document.thumbnail && document.documentType == LTDocumentTypePDF) [tmp addObject:document];
            self.transListDataSource = [tmp copy];
        }
    }else{
        self.transListDataSource = [self.documentMgr.documents copy];
    }
    
}

- (void)btnShowCameraViewTapped:(UIButton *)sender
{
    [self HiddenMore];
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied)
    {
        UIAlertController *alert = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kNotAllowTakePhoto") message:FSLocalizedForKey(@"kCAMERADenied") actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kGoSetting") ] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
            if (index == 1){
                NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
        }];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }else if (authStatus == AVAuthorizationStatusNotDetermined){
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (granted) {
                    LTCanmeraViewControllerNew* cameraView = [[LTCanmeraViewControllerNew alloc] init];
                    cameraView.documentSession = [LTDocumentSession createNewDocument];
                    cameraView.documentSession.rename = nil;
                    //    cameraView.isOpenFromEditor = NO;
                    cameraView.modalPresentationStyle = UIModalPresentationFullScreen;
                    [self presentViewController:cameraView animated:YES completion:nil];
                }else{
                    UIAlertController *alert = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kNotAllowTakePhoto") message:FSLocalizedForKey(@"kCAMERADenied") actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kGoSetting") ] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                        if (index == 1){
                            NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                            if([[UIApplication sharedApplication] canOpenURL:url]) {
                                [[UIApplication sharedApplication] openURL:url];
                            }
                        }
                    }];
                    [self presentViewController:alert animated:YES completion:nil];
                    return;
                }
            });
        }];
    }else{
        LTCanmeraViewControllerNew* cameraView = [[LTCanmeraViewControllerNew alloc] init];
        cameraView.documentSession = [LTDocumentSession createNewDocument];
        cameraView.documentSession.rename = nil;
        cameraView.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:cameraView animated:YES completion:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    LTTransListCell *oldEditCell = [self.transList cellForRowAtIndexPath:_oldIndexPath];
    if (oldEditCell  && [oldEditCell.reuseIdentifier isEqualToString:ltTransListCellID]) {
        oldEditCell.showMoreContentView = NO;
    }
    [self removeGesture];
}

- (void)reloadData{
    [self createTransListDataSource];
    [_transList reloadData];
}

- (void)onSafeAreaChanged:(UIEdgeInsets)safeInsets
{
    ;
    if (IS_IPHONE_OVER_TEN) {
        [_recentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_recentLabel.superview.mas_left).offset(10 + safeInsets.left);
            make.centerY.mas_equalTo(_recentLabel.superview.mas_centerY);
        }];
        
        [_transList mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_recentView.mas_bottom);
            make.left.mas_equalTo(_transList.superview.mas_left).offset(safeInsets.left);
            make.right.mas_equalTo(_transList.superview.mas_right).offset(- safeInsets.right);
            make.bottom.mas_equalTo(_transList.superview.mas_bottom);
        }];
    }
}

- (void)onActivated
{
}

- (void)goBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)initSubview
{
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = ThemeBarColor;
    [self.view addSubview:topView];
    _topView = topView;
    [topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topView.superview.mas_left);
        make.top.mas_equalTo(topView.superview.mas_top);
        make.right.mas_equalTo(topView.superview.mas_right);
        make.height.mas_equalTo(topView.superview.mas_height).multipliedBy(DEVICE_iPHONE ? 0.4 : 0.3);
    }];
    
    UIButton *goBack = [UIButton new];
    [topView addSubview:goBack];
    [goBack setImage:ImageNamed(@"common_back_white") forState:UIControlStateNormal];
    [goBack addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [goBack mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(goBack.superview).offset(ITEM_MARGIN_NORMAL);
        make.top.mas_equalTo(goBack.superview.fs_mas_top).offset(ITEM_MARGIN_NORMAL);
    }];
    
    UILabel *title = [UILabel new];
    title.text = FSLocalizedForKey(@"kScan");
    title.textAlignment = NSTextAlignmentCenter;
    title.font = [UIFont systemFontOfSize:17.f];
    title.textColor = WhiteThemeTextColor;
    [topView addSubview:title];
    
    [title mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(goBack);
        make.centerX.mas_equalTo(title.superview);
    }];
    
    UIButton *addBtn = [[UIButton alloc] init];
    _addBtn = addBtn;
    _addBtn.accessibilityLabel = FSLocalizedForKey(@"kScan");
    [addBtn setImage:ImageNamed(@"add") forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(btnShowCameraViewTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:addBtn];
    [addBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(70);
        make.centerX.mas_equalTo(topView.mas_centerX);
        make.centerY.mas_equalTo(topView.mas_centerY).offset(12);
    }];
    
    UILabel *addLabel = [[UILabel alloc] init];
    addLabel.text = FSLocalizedForKey(@"kStartTap");
    addLabel.textAlignment = NSTextAlignmentCenter;
    addLabel.font = [UIFont systemFontOfSize:12];
    addLabel.textColor = WhiteThemeTextColor;
    [topView addSubview:addLabel];
    [addLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(topView.mas_centerX);
        make.top.mas_equalTo(addBtn.mas_bottom).offset(2);
    }];
    
    UIView *recentTileView = [[UIView alloc] init];
    _recentView = recentTileView;
    recentTileView.backgroundColor = NormalBarBackgroundColor;
    [self.view addSubview:recentTileView];
    [recentTileView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView.mas_bottom);
        make.left.mas_equalTo(topView.superview.mas_left);
        make.right.mas_equalTo(topView.superview.mas_right);
        make.height.mas_equalTo(26);
    }];
    
    UILabel *recentChangeLabel = [[UILabel alloc] init];
    _recentLabel = recentChangeLabel;
    recentChangeLabel.text = FSLocalizedForKey(@"kRecentTips");
    recentChangeLabel.font = [UIFont systemFontOfSize:12];
    recentChangeLabel.textColor = BarLikeBlackTextColor;
    [recentTileView addSubview:recentChangeLabel];
    [recentChangeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(recentChangeLabel.superview.fs_mas_left).offset(8);
        make.centerY.mas_equalTo(recentChangeLabel.superview.mas_centerY);
    }];
    
    UITableView *transList = [[UITableView alloc] init];
    transList.tableFooterView = [[UIView alloc] init];
    [transList registerClass:[LTTransListCell class] forCellReuseIdentifier:ltTransListCellID];
    _transList = transList;
    transList.separatorStyle = UITableViewCellSeparatorStyleNone;
    transList.rowHeight = 68;
    transList.delegate = self;
    transList.dataSource = self;
    [self.view addSubview:transList];
    [transList mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(recentTileView.mas_bottom);
        make.left.mas_equalTo(transList.superview.mas_left);
        make.right.mas_equalTo(transList.superview.mas_right);
        make.bottom.mas_equalTo(transList.superview.mas_bottom);
    }];
}

#pragma mark - transList delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [tableView fs_tableViewDisplayWitMsg:FSLocalizedForKey(@"kNoFilesYet") ifNecessaryForRowCount:[self.transListDataSource count]];
    return self.transListDataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.transListDataSource.count < 1) {
        return tableView.frame.size.height;
    }
    return 68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LTDocument *document = [self.transListDataSource objectAtIndex:self.transListDataSource.count - indexPath.row - 1];
    LTTransListCell *cell = [tableView dequeueReusableCellWithIdentifier:ltTransListCellID forIndexPath:indexPath];
    cell.preImageView.image = [document thumbnail];
    cell.documentTitleLabel.text = document.documentName;
    cell.documentTimeLabel.text = [self dateToString:document.timeOfCreation];
    cell.moreShowClick = ^(LTTransListCell *editCell){
        [self removeGesture];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGuest:)];
        self.tapGesture = tapGesture;
        UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGesture:)];
        self.longGesture = longGesture;
        [self.view addGestureRecognizer:longGesture];
        [self.view addGestureRecognizer:tapGesture];
        LTTransListCell *oldEditCell = [tableView cellForRowAtIndexPath:self->_oldIndexPath];
        if (oldEditCell  && [oldEditCell.reuseIdentifier isEqualToString:ltTransListCellID]) {
            oldEditCell.showMoreContentView = NO;
        }
        editCell.showMoreContentView = YES;
        self->_oldIndexPath = [tableView indexPathForCell:editCell];
    };
    cell.deleteItemClick = ^(LTTransListCell *editCell){
        editCell.showMoreContentView = NO;
        [self->_documentMgr removeDocument:[self->_documentMgr documentByName:editCell.documentTitleLabel.text]];
        [self removeGesture];
        [self reloadData];
    };

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.transListDataSource.count < 1) {
        return;
    }
    LTTransListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    @try {
        LTDocument *document = [_documentMgr documentByName:cell.documentTitleLabel.text];
         if ([[NSFileManager defaultManager] fileExistsAtPath:document.urlForDocument.path]) {
             LTDocumentSession *documentSession = [LTDocumentSession createSessionForEdit];
             documentSession.rename = nil;
             documentSession.delegate = self;
             [documentSession loadDocument:document];
         }else{
             [Utility alert_OK_WithTitle:@"kError" message:@"kUnfoundOrCannotOpen" actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                 [self.documentMgr removeDocument:document];
                 [self reloadData];
             }];
         }
    } @catch (NSException *exception) {
       
    }
}

- (void)HiddenMore
{
    [self tapGuest:nil];
}

- (void)tapGuest:(UIGestureRecognizer *)recognizer
{
    LTTransListCell *oldEditCell = [self.transList cellForRowAtIndexPath:_oldIndexPath];
    if (oldEditCell && [oldEditCell.reuseIdentifier isEqualToString:ltTransListCellID]) {
        oldEditCell.showMoreContentView = NO;
    }
    [self removeGesture];
}

- (void)longGesture:(UILongPressGestureRecognizer *)recognizer
{
    LTTransListCell *oldEditCell = [self.transList cellForRowAtIndexPath:_oldIndexPath];
    if (oldEditCell && [oldEditCell.reuseIdentifier isEqualToString:ltTransListCellID]) {
        oldEditCell.showMoreContentView = NO;
    }
    [self removeGesture];
}

- (void)removeGesture
{
    if (self.tapGesture) {
        [self.view removeGestureRecognizer:self.tapGesture];
    }
    if (self.longGesture) {
        [self.view removeGestureRecognizer:self.longGesture];
    }
}

- (void) documentSession:(LTDocumentSession*) session didStartLoadingDocument:(LTDocument*) document
{
    [self.view fs_showHUDLoading:nil];
}

-(void) documentSession:(LTDocumentSession*) session didLoadPage:(int) page ofTotal:(int) total
{
    
}

- (void) documentSession:(LTDocumentSession*) session didFinishLoadingDocument:(LTDocument*) document
{
    [self.view fs_hideHUDLoading];
    LTDocumentEditViewControllerNew* editorView = [[LTDocumentEditViewControllerNew alloc] init];
    editorView.documentSession = session;
    editorView.isDocumentEditModel = YES;
    editorView.documentSession.delegate = editorView;
    UINavigationController *editornav = [[UINavigationController alloc] initWithRootViewController:editorView];
    editornav.navigationBarHidden = YES;
    editornav.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:editornav animated:YES completion:nil];
}

- (NSString *)dateToString:(NSTimeInterval)times
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    formatter.locale = [NSLocale currentLocale];
    return [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:times]];
}

- (BOOL)fileWithNameDoesExist:(NSString*) filename
{
    return [self.documentMgr documentByName:filename] != nil;
}

@end
