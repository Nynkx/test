/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "FXImagePickThumbnailCell.h"

@interface FXImagePickThumbnailCell ()
@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UIImageView *selectImageView;
@end

@implementation FXImagePickThumbnailCell

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, DEVICE_iPHONE ? 78 : 132, DEVICE_iPHONE ? 78 : 132);
        self.backgroundColor = ThemeCellBackgroundColor;
        _thumbnailImageView = [[UIImageView alloc] init];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
        _thumbnailImageView.clipsToBounds = YES;
        _thumbnailImageView.frame = CGRectMake(0, 0,DEVICE_iPHONE ? 78 : 132, DEVICE_iPHONE ? 78 : 132);
        [self addSubview:_thumbnailImageView];
        
        _selectImageView = [[UIImageView alloc] init];
        _selectImageView.frame = CGRectMake(5, 5, 26, 26);
        _selectImageView.image = ImageNamed(@"common_redio_selected");
        _selectImageView.hidden = YES;
        [self addSubview:_selectImageView];
        
        self.contentView.isAccessibilityElement = YES;
    }
    return self;
}

- (void)setThumbImage:(UIImage *)thumbImage
{
    _thumbImage = thumbImage;
    _thumbnailImageView.image = _thumbImage;
}

- (void)setIsHighlight:(BOOL)isHighlight
{
    _isHighlight = isHighlight;
    if (isHighlight) {
        _selectImageView.hidden = NO;
        _thumbnailImageView.alpha = 0.6;
    }else{
        _selectImageView.hidden = YES;
        _thumbnailImageView.alpha = 1.0;
    }
}

- (void)setIsDisable:(BOOL)isDisable
{
    _isDisable = isDisable;
    if (isDisable) {
        _thumbnailImageView.alpha = 0.4;
        //self.userInteractionEnabled = NO;
    }else{
        _thumbnailImageView.alpha = 1.0;
        //self.userInteractionEnabled = YES;
    }
}
@end
