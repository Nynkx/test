/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import "PDFScanManager.h"
#import "CreatePDFViewControllerNew.h"
#import "PDFScanManager+private.h"

@implementation PDFScanManager
- (instancetype)init{
    return [super init];
}

static PDFScanManager *_manager = nil;
+ (PDFScanManager *)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[self alloc] init];
    });
    return _manager;
}

static ScanPDFSaveAsCallBack _saveAsCallBack;
+ (void)setSaveAsCallBack:(ScanPDFSaveAsCallBack)saveAsCallBack{
    _saveAsCallBack = saveAsCallBack;
}

+ (ScanPDFSaveAsCallBack)saveAsCallBack{
    return _saveAsCallBack;
}

static FSErrorCode _errorCode = FSErrUnknown;
static BOOL _isTrial = NO;
+ (FSErrorCode)initializeScanner:(unsigned long)serial1 serial2:(unsigned long)serial2{
    _isTrial = NO;
    if (serial1 == 0 && serial2 == 0) _isTrial = YES;
    PDFScanManager *manager = [self shareManager];
    AppFrameworkConfiguration *appFrameworkConfiguration = [[AppFrameworkConfiguration alloc] init];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    if ( paths && ([paths count] > 0)) [appFrameworkConfiguration setDocumentsDirectory:[[paths objectAtIndex:0] stringByAppendingString:@"/InternalDocuments"]];
    manager.documentPath = appFrameworkConfiguration.documentsDirectory.copy;
    
    LTInitStatus status = AppFrameworkSdk_InitEx(serial1, serial2, appFrameworkConfiguration);
    
    switch (status)
    {
        case LTInitStatusSuccess:
            _errorCode =  FSErrSuccess;
            break;
        case LTInitStatusExpiredLicense:
            _errorCode = FSErrRightsExpired;
             break;
        case LTInitStatusInvalidLicense:
            _errorCode = FSErrInvalidLicense;
            break;
    }
    manager.documentManager = [[LTDocumentManager alloc] init];
    return _errorCode;
}

+ (FSErrorCode)initializeCompression:(unsigned long)serial1 serial2:(unsigned long)serial2{
    
    FSErrorCode errorCode = FSErrUnknown;
    
    LTInitStatus status = MobileCompressionSdk_Init(serial1, serial2);
    switch (status)
    {
        case LTInitStatusSuccess:
            errorCode = FSErrSuccess;
             break;
        case LTInitStatusExpiredLicense:
            errorCode = FSErrRightsExpired;
             break;
        case LTInitStatusInvalidLicense:
            errorCode = FSErrInvalidLicense;
             break;
    }
    return errorCode;
}

+ (UIViewController *)getPDFScanView{
    if (_errorCode != FSErrSuccess && !_isTrial) {
        AlertViewCtrlShow(@"Check Serial", @"Invalid license");
        return nil;
    }
    CreatePDFViewControllerNew *pdfViewController = [[CreatePDFViewControllerNew alloc] init];
    FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:pdfViewController];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [pdfViewController.navigationController setNavigationBarHidden:YES];
    return nav;
}

- (BOOL)isTrial{
    return _isTrial;
}

- (NSString *)documentPath{
    if (_isTrial) {
        return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    }
    return _documentPath;
}

@end
