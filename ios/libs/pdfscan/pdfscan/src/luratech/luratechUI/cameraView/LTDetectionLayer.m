//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import "LTDetectionLayer.h"
#import "PointUtil.h"

@interface LTDetectionLayer()
@property AVCaptureVideoPreviewLayer* previewLayer;
@end


@implementation LTDetectionLayer

- (id) initWithPreviewLayer:(AVCaptureVideoPreviewLayer*) previewLayer
{
	self = [super initWithRect:previewLayer.frame];
	if (self != nil)
	{
		self.previewLayer = previewLayer;
	}
	
	return self;
}

- (void) updateResultDisplay:(LTDetectionResult*) result
{
	if (result.valid)
	{
		self.opacity = 1.0f;
		
		NSArray* mapped = [self mapResultsToView:result];
		[self update:mapped];
	}
	else
	{
		self.opacity = 0.0f;
	}
}

- (NSArray*) mapResultsToView:(LTDetectionResult*)detectionResult
{
	NSMutableArray* list = [NSMutableArray arrayWithCapacity:4];
	
	for (NSValue* value in detectionResult.points)
	{
		CGPoint p = point(value);
		p.x /= detectionResult.imageWidth;
		p.y /= detectionResult.imageHeight;
		
		p = [_previewLayer pointForCaptureDevicePointOfInterest:p];
		
		[list addObject:[NSValue valueWithBytes:&p objCType:@encode(CGPoint)]];
	}
	
	return [NSArray arrayWithArray:list];
}

@end
