//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LTDetectionResult;

@interface LTStatusLabelController : NSObject

/*! \brief Initialize with the given label. */
- (id) initWithLabel:(UILabel*)label;

/*! \brief Update the labels display text and visibility state according to flags. */
- (void) updateMessage:(LTDetectionResult*) result;

@end
