//
//  LTCanmeraViewControllerNew.m
//  FoxitApp
//
//  Created by Apple on 16/11/30.
//
//

#import "LTCanmeraViewControllerNew.h"
#import "LTDetectionLayer.h"
#import "LTStatusLabelController.h"
#import "LTDocumentEditViewControllerNew.h"
#import "LTPageFormatHelper.h"
#import "LTDefaultFilenameProvider.h"
#import "LTPageFormat.h"
#import "FXUIPickViewController.h"
#import "PDFScanManager+private.h"
#import "Preference.h"

@interface LTCanmeraViewControllerNew ()<LTImageProcessViewControllerDelegate,IFXUIPickViewControllerDelegate,LTDocumentSessionDelegate,LTImageProcessorDelegate>{
    BOOL isWaiting;
}

@property (nonatomic, strong) LTCamera* camera;
@property (nonatomic, strong) LTCaptureService* captureService;
@property (nonatomic, strong) LTPageFormat* pageFormat;

@property (nonatomic, strong) UIButton *albumBtn;
@property (nonatomic, strong) UIButton *shootBtn;
@property (nonatomic, strong) UIButton *completeBtn;
@property (nonatomic, strong) UIButton *flashlightBtn;
@property (nonatomic, strong) UIButton *singleFrametBtn;
@property (nonatomic, strong) UIButton *detectionBtn;
@property (nonatomic, strong) UIButton *formatSizeBtn;
@property (nonatomic, strong) UIView *videoPreviewView;
@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, strong) UILabel *countLabel;

//@property (strong, nonatomic) NSMutableArray * arrImageCollection;
@property (nonatomic, strong) LTImageProcessor *imageProcess;
@property (nonatomic, assign) BOOL cameraTriggered;
@property (nonatomic, strong) LTStatusLabelController* statusLabelController;
@property (nonatomic, strong) UILabel *detectionLabel;
@property (nonatomic) int pageInProcessor;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic) int imageNilCounts;
//@property (nonatomic) UIDeviceOrientation srcOrientation;
@end

@implementation LTCanmeraViewControllerNew

- (instancetype)init
{
    if (self = [super init]) {
        _isCallCameraForImages = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSubViews];
    
    _photos = [NSMutableArray array];
    _camera = [[LTCamera alloc] initWithDevicePosition:AVCaptureDevicePositionBack];
    _camera.lightEnabled = NO;
    
    isWaiting = NO;
    if (_camera)
    {
        _captureService = [[LTCaptureService alloc] initWithCamera:_camera];
        _captureService.delegate = self;
        //_captureService.useAutoTrigger = NO;
    }
    _statusLabelController = [[LTStatusLabelController alloc] initWithLabel:_detectionLabel];
    _pageFormat = [LTPageFormatHelper getStoredFormat];
    
    [self setupPreviewLayer];
    [self setupDetectionLayer];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)initSubViews
{
    UIView *bottomBar = [[UIView alloc] init];
    
    [self.view addSubview:bottomBar];
    bottomBar.backgroundColor = [UIColor blackColor];
    [bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bottomBar.superview.mas_left);
        make.right.mas_equalTo(bottomBar.superview.mas_right);
        make.bottom.mas_equalTo(bottomBar.superview.mas_bottom);
        make.height.mas_equalTo(DEVICE_iPHONE ? 88 : 118);
    }];
    
    self.shootBtn = [[UIButton alloc] init];
    self.shootBtn.accessibilityLabel = FSLocalizedForKey(@"kScan");
    [self.shootBtn setBackgroundImage:ImageNamed(@"camera_take_photo") forState:UIControlStateNormal];
    [bottomBar addSubview:self.shootBtn];
    [self.shootBtn addTarget:self action:@selector(shootTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.shootBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.shootBtn.superview.mas_centerX);
        make.centerY.mas_equalTo(self.shootBtn.superview.mas_centerY);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(58);
    }];
    
    UIImage *image = ImageNamed(@"images");
    
    self.albumBtn = [[UIButton alloc] init];
    self.albumBtn.accessibilityLabel = FSLocalizedForKey(@"kAddPhotos");
    [self.albumBtn setBackgroundImage:ImageNamed(@"images") forState:UIControlStateNormal];
    [bottomBar addSubview:self.albumBtn];
    [self.albumBtn addTarget:self action:@selector(openAlbumLibraryTap) forControlEvents:UIControlEventTouchUpInside];
    [self.albumBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.albumBtn.superview).multipliedBy(0.5).offset(-58/4);
        make.centerY.mas_equalTo(self.albumBtn.superview.mas_centerY);
        make.size.mas_equalTo(image.size);
    }];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    self.countLabel.backgroundColor = UIColorHex(0xFC6621);
    self.countLabel.layer.cornerRadius = 7;
    self.countLabel.layer.masksToBounds = YES;
    self.countLabel.font = [UIFont systemFontOfSize:14];
    self.countLabel.adjustsFontSizeToFitWidth = YES;
    self.countLabel.textColor = [UIColor whiteColor];
    [self.shootBtn addSubview:self.countLabel];
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.shootBtn.mas_right);
        make.top.mas_equalTo(self.shootBtn.mas_top);
        make.width.mas_equalTo(29);
    }];
    
    self.completeBtn = [[UIButton alloc] init];
    [self.completeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.completeBtn setTitle:FSLocalizedForKey(@"kDone") forState:UIControlStateNormal];
    self.completeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.completeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [bottomBar addSubview:self.completeBtn];
    [self.completeBtn addTarget:self action:@selector(capturedImagecountBtnTap) forControlEvents:UIControlEventTouchUpInside];
    CGSize size = [Utility getTextSize:FSLocalizedForKey(@"kDone") fontSize:15 maxSize:CGSizeMake(150, 36)];
    
    [self.completeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.completeBtn.superview).multipliedBy(1.5).offset(58/4);
        make.centerY.mas_equalTo(self.completeBtn.superview.mas_centerY);
        make.width.mas_equalTo(size.width);
        make.height.mas_equalTo(36);
    }];
    
    UIView *videoPreview = [[UIView alloc] init];
    videoPreview.backgroundColor = UIColorHex(0x000000);
    self.videoPreviewView = videoPreview;
    [self.view addSubview:videoPreview];
    [videoPreview mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(videoPreview.superview.mas_left);
        make.right.mas_equalTo(videoPreview.superview.mas_right);
        make.top.mas_equalTo(videoPreview.superview.mas_top);
        make.bottom.mas_equalTo(bottomBar.mas_top);
    }];
    
    UIView *bottomToolBar = [[UIView alloc] init];
    bottomToolBar.backgroundColor = UIColorHex(0xFFFFFF1C);
    [self.view addSubview:bottomToolBar];
    [bottomToolBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bottomToolBar.superview.mas_left);
        make.right.mas_equalTo(bottomToolBar.superview.mas_right);
        make.bottom.mas_equalTo(bottomBar.mas_top);
        make.height.mas_equalTo(DEVICE_iPHONE ? 29 : 39);
    }];
    
    UIView *placeholdView = [[UIView alloc] init];
    [bottomToolBar addSubview:placeholdView];
    [placeholdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(placeholdView.superview.mas_top);
        make.bottom.mas_equalTo(placeholdView.superview.mas_bottom);
        make.centerX.mas_equalTo(placeholdView.superview.mas_centerX);
        make.width.mas_equalTo(1);
    }];
    
    self.singleFrametBtn = [[UIButton alloc] init];
    self.singleFrametBtn.accessibilityLabel = FSLocalizedForKey(@"kScaneSingleTip");
    self.singleFrametBtn.selected = [Preference getIntValue:@"PhotoToPDF" type:@"singleFrame" defaultValue:1];
    [self.singleFrametBtn setImage:ImageNamed(@"cameracontinuous shootingno") forState:UIControlStateNormal];
    [self.singleFrametBtn setImage:ImageNamed(@"cameracontinuous shooting") forState:UIControlStateSelected];
    [bottomToolBar addSubview:self.singleFrametBtn];
    [self.singleFrametBtn addTarget:self action:@selector(singleFrametBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.singleFrametBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(placeholdView.mas_left).offset(-8);
        make.width.height.mas_equalTo(DEVICE_iPHONE ? 18 : 27);
        make.centerY.mas_equalTo(self.singleFrametBtn.superview.mas_centerY);
    }];
    
    self.flashlightBtn = [[UIButton alloc] init];
    self.flashlightBtn.accessibilityLabel = FSLocalizedForKey(@"kScaneLightTip");
    [self.flashlightBtn setImage:ImageNamed(@"cameraflashno") forState:UIControlStateNormal];
    [self.flashlightBtn setImage:ImageNamed(@"cameraflash")  forState:UIControlStateSelected];
    [bottomToolBar addSubview:self.flashlightBtn];
    [self.flashlightBtn addTarget:self action:@selector(flashlightBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.flashlightBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.singleFrametBtn.mas_left).offset(-16);
        make.width.height.mas_equalTo(DEVICE_iPHONE ? 18 : 27);
        make.centerY.mas_equalTo(self.flashlightBtn.superview.mas_centerY);
    }];
    
    self.detectionBtn = [[UIButton alloc] init];
    self.detectionBtn.accessibilityLabel = FSLocalizedForKey(@"kScaneDetechTip");
    self.detectionBtn.selected = [Preference getIntValue:@"PhotoToPDF" type:@"detectAuto" defaultValue:0];
    [self.detectionBtn setImage:ImageNamed(@"cameraautomaticdetectionno") forState:UIControlStateNormal];
    [self.detectionBtn setImage:ImageNamed(@"cameraautomaticdetection") forState:UIControlStateSelected];
    [bottomToolBar addSubview:self.detectionBtn];
    [self.detectionBtn addTarget:self action:@selector(detectionBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.detectionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(placeholdView.mas_right).offset(8);
        make.width.height.mas_equalTo(DEVICE_iPHONE ? 18 : 27);
        make.centerY.mas_equalTo(self.detectionBtn.superview.mas_centerY);
    }];
    
    self.formatSizeBtn = [[UIButton alloc] init];
    self.formatSizeBtn.accessibilityLabel = FSLocalizedForKey(@"kScaneFrameSizeTip");
    self.formatSizeBtn.enabled = self.detectionBtn.selected;
    [self.formatSizeBtn setImage:ImageNamed(@"pageFormatSize") forState:UIControlStateNormal];
    [bottomToolBar addSubview:self.formatSizeBtn];
    [self.formatSizeBtn addTarget:self action:@selector(formatSizeBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.formatSizeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.detectionBtn.mas_right).offset(16);
        make.width.height.mas_equalTo(DEVICE_iPHONE ? 18 : 27);
        make.centerY.mas_equalTo(self.formatSizeBtn.superview.mas_centerY);
    }];
    
    UILabel *detectionLabel = [[UILabel alloc] init];
    detectionLabel.hidden = YES;
    _detectionLabel = detectionLabel;
    [self.view addSubview:detectionLabel];
    [detectionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(detectionLabel.superview.mas_centerX);
        make.centerY.mas_equalTo(detectionLabel.superview.mas_centerY);
    }];
    
    UIButton *backBtn = [[UIButton alloc] init];
    backBtn.accessibilityLabel = FSLocalizedForKey(@"kClose");
    [backBtn setImage:ImageNamed(@"guanbi") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(closeCanmeraView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [backBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backBtn.superview.mas_left).offset(4);
        make.top.mas_equalTo(backBtn.superview.mas_top).offset(4);
        make.width.height.mas_equalTo(40);
    }];
    
    UIView *maskView = [[UIView alloc] init];
    _maskView = maskView;
    maskView.backgroundColor = [UIColor grayColor];
    maskView.hidden = YES;
    [self.view insertSubview:maskView belowSubview:backBtn];
    [maskView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    UILabel *tips = [[UILabel alloc] init];
    tips.backgroundColor = [UIColor blackColor];
    tips.font = [UIFont systemFontOfSize:18];
    tips.numberOfLines = 0;
    tips.textAlignment = NSTextAlignmentCenter;
    tips.text = FSLocalizedForKey(@"kNeedFullScreen");
    tips.textColor = [UIColor whiteColor];
    [maskView addSubview:tips];
    [tips mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(tips.superview.mas_centerY).offset(-104);
        make.left.mas_equalTo(tips.superview.mas_left).offset(30);
        make.right.mas_equalTo(tips.superview.mas_right).offset(-30);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //[self orientationToPortrait:UIInterfaceOrientationPortrait];
    
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.barTintColor = ThemeNavBarColor;
    
    //_pageFormat = [LTPageFormatHelper getStoredFormat];
    //[self setPageFormatButtonTitle:_pageFormat];
    
    if ([UIScreen mainScreen].bounds.size.width != SCREENWIDTH) {
        _maskView.hidden = NO;
    }else{
        _maskView.hidden = YES;
    }
    
    [_detectionLayer updateResultDisplay:nil];
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"run on simulator can not call canmera.");
#else
    [self startCameraSession];
#endif
    _cameraTriggered = NO;
    //_userTriggeredCamera = NO;
    [_statusLabelController updateMessage:nil];
    [self configureAndSetLayoutOfImageCount];
    
    // Add app goes to foreground/background delegates
    //[self onDeviceOrientationChange:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appHasGoneInBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appHasGoneInForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [_detectionLayer updateResultDisplay:nil];
    [_statusLabelController updateMessage:nil];
    [self stopCameraSession];
    
    // Remove app goes to foreground/background delegates
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.documentSession) {
        self.documentSession.delegate = self;
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        for (UIView *subview in self.view.subviews) {
            if (subview.tag == 2118) {
                [subview removeFromSuperview];
            }
        }
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        if ([UIScreen mainScreen].bounds.size.width != SCREENWIDTH) {
            self->_maskView.hidden = NO;
        }else{
            self->_maskView.hidden = YES;
        }
    }];
}

+ (int)fromUIInterfaceOrientation:(UIInterfaceOrientation) orientation
{
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
            return AVCaptureVideoOrientationPortrait;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            return AVCaptureVideoOrientationPortraitUpsideDown;
            
        case UIInterfaceOrientationLandscapeLeft:
            return AVCaptureVideoOrientationLandscapeLeft;
            
        case UIInterfaceOrientationLandscapeRight:
            return AVCaptureVideoOrientationLandscapeRight;
            
        default:
            return 0;
    }
}

- (void) updateVideoOrientation
{
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    int videoOrientation = [LTCanmeraViewControllerNew fromUIInterfaceOrientation:interfaceOrientation];
    if ( videoOrientation != 0 ) {
        self.previewLayer.connection.videoOrientation = (AVCaptureVideoOrientation)videoOrientation;
    }
}
- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateVideoOrientation];
    [self.previewLayer setFrame:self.videoPreviewView.bounds];
    self.detectionLayer.rect = self.videoPreviewView.bounds;
}

- (void)appHasGoneInBackground: (UIApplication *)application {

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:_flashlightBtn.selected forKey:@"camera.lightEnabled"];
    _camera.lightEnabled = NO;
}

- (void)appHasGoneInForeground: (UIApplication *)application {

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    bool lightEnabled = (bool)[defaults boolForKey:@"camera.lightEnabled"];
    _flashlightBtn.selected = lightEnabled;
    _camera.lightEnabled = lightEnabled;
}

- (void) setupPreviewLayer
{
    _previewLayer = [_camera createPreviewLayer];
    
    CALayer *viewLayer = [self.videoPreviewView layer];
    [viewLayer setMasksToBounds:YES];
    
    [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspect];
    [viewLayer insertSublayer:_previewLayer below:[viewLayer sublayers][0]];
}

- (void) setupDetectionLayer
{
    self.detectionLayer = [[LTDetectionLayer alloc] initWithPreviewLayer:self.previewLayer];
    
    CALayer *viewLayer = [self.videoPreviewView layer];
    [viewLayer addSublayer:self.detectionLayer];
}

- (void) startCameraSession
{
    [_captureService start];
    [_camera start];
    _captureService.useAutoTrigger = self.detectionBtn.selected;
    _detectionLabel.hidden = !self.detectionBtn.selected;
}

- (void) stopCameraSession
{
    _camera.lightEnabled = NO;
    [_captureService stop];
    [_camera stop];
    _flashlightBtn.selected = NO;
}

#pragma mark - btn action
- (void)closeCanmeraView
{
    if (self.documentSession != nil && !self.isOpenFromEditor) {
        if ((self.documentSession.isNewDocument && self.documentSession.pageCount > 0)) {
            if (self.documentSession.rename != nil) {
                [self.documentSession saveAsPDF:self.documentSession.rename];
            }else if (self.documentSession.documentName != nil){
                [self.documentSession saveAsPDF:self.documentSession.documentName];
            }else{
                NSString *documentName = [@"Scan " stringByAppendingString:[LTDefaultFilenameProvider timeBasedFilename]];
                documentName = [LTDefaultFilenameProvider filterFileNameSpecialCharactor:documentName];
                [self.documentSession saveAsPDF:documentName];
            }
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)openAlbumLibraryTap
{
    _imageNilCounts = 0;
    PHAuthorizationStatus statusA = [PHPhotoLibrary authorizationStatus];
    if (statusA == PHAuthorizationStatusRestricted || statusA == PHAuthorizationStatusDenied) {
        AlertViewCtrlShow(@"kWarning", @"KALBUM");
        return;
    }else if (statusA == PHAuthorizationStatusNotDetermined){
        [PHPhotoLibrary  requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied) {
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_photos removeAllObjects];
                FXUIPickViewController *pickVc = [[FXUIPickViewController alloc] init];
                pickVc.delegate = self;
                
                FSNavigationController *imagePickerNav = [[FSNavigationController alloc] initWithRootViewController:pickVc];
                self->_cameraTriggered = YES;
                imagePickerNav.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:imagePickerNav animated:YES completion:^{
                    
                }];
            });
        }];
    }else{
        [_photos removeAllObjects];
        FXUIPickViewController *pickVc = [[FXUIPickViewController alloc] init];
        pickVc.delegate = self;
        
        FSNavigationController *imagePickerNav = [[FSNavigationController alloc] initWithRootViewController:pickVc];
        _cameraTriggered = YES;
        imagePickerNav.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:imagePickerNav animated:YES completion:nil];
    }
}

- (void)shootTap:(UIButton *)btn
{
    [self takePhoto];
}

- (void)takePhoto
{
    if (!_cameraTriggered) {
        _cameraTriggered = YES;
        [_camera takePicture];
    }
}

- (void)capturedImagecountBtnTap
{
    if (_isCallCameraForImages && _callBackImages) {
        NSMutableArray *imagesArray = [NSMutableArray array];
        for (int i = 0; i < _documentSession.pageCount; i++) {
            UIImage *image = [_documentSession imageForPage:i];
            if (image) {
                [imagesArray addObject:image];
            }
        }
        _callBackImages(imagesArray.copy);
        return;
    }
    
    if (_isOpenFromEditor) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
            LTDocumentEditViewControllerNew* editorView = [[LTDocumentEditViewControllerNew alloc] init];
            editorView.documentSession = self.documentSession;
            editorView.documentSession.delegate = editorView;
            editorView.normalModelSavedDocument = ^(){
                [self dismissViewControllerAnimated:NO completion:nil];
            };
            editorView.discardCompelet = ^(){
                self->_documentSession = [LTDocumentSession createNewDocument];
                self->_documentSession.rename = nil;
            };
            editorView.addPageDocumentReload = ^(LTDocumentSession *session){
                self->_documentSession = session;
                self->_documentSession.delegate = self;
            };
            FSNavigationController *editornav = [[FSNavigationController alloc] initWithRootViewController:editorView];
            editornav.navigationBarHidden = YES;
        editornav.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:editornav animated:YES completion:nil];
    }
}

- (void)flashlightBtnTap:(UIButton *)btn
{
    if (!_camera.hasTorch) {
        AlertViewCtrlShow(@"kWarning", @"kNoTorch");
        btn.selected = NO;
        return;
    }
    btn.selected = !btn.selected;
    _camera.lightEnabled = !_camera.lightEnabled;
    
    if (_camera.lightEnabled) {
        [self.view fs_showHUDMessage:FSLocalizedForKey(@"kLightOpen") afterHideDelay:1.0];
    }else{
        [self.view fs_showHUDMessage:FSLocalizedForKey(@"kLightClose") afterHideDelay:1.0];
    }
}

- (void)singleFrametBtnTap:(UIButton *)btn
{
    btn.selected = !btn.selected;
    [Preference setIntValue:@"PhotoToPDF" type:@"singleFrame" value:(int)btn.selected];
    if (btn.selected) {
        [self.view fs_showHUDMessage:FSLocalizedForKey(@"kContinueMode") afterHideDelay:1.0];
    }else{
        [self.view fs_showHUDMessage:FSLocalizedForKey(@"kSigleMode") afterHideDelay:1.0];
    }
}

- (void)detectionBtnTap:(UIButton *)btn
{
    btn.selected = !btn.selected;
    [Preference setIntValue:@"PhotoToPDF" type:@"detectAuto" value:(int)btn.selected];
    _captureService.useAutoTrigger = btn.selected;
    _detectionLabel.hidden = !btn.selected;
    _formatSizeBtn.enabled = btn.selected;
    if (!btn.selected) {
        [_detectionLayer updateResultDisplay:nil];
    }

    if (btn.selected) {
         [self.view fs_showHUDMessage:FSLocalizedForKey(@"kAutoDetectOpen") afterHideDelay:1.0];
    }else{
         [self.view fs_showHUDMessage:FSLocalizedForKey(@"kAutoDetectClose") afterHideDelay:1.0];
    }
}

- (void)formatSizeBtnTap:(UIButton *)btn
{
    LTPageFormatViewControllerNew* formatView = [[LTPageFormatViewControllerNew alloc] init];
    
    formatView.delegate = self;
    
    FSNavigationController *formatPageNav = [[FSNavigationController alloc] initWithRootViewController:formatView];
    formatPageNav.modalPresentationStyle = DEVICE_iPHONE ? UIModalPresentationFullScreen : UIModalPresentationFormSheet;
    formatPageNav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    _cameraTriggered = YES;
    [self presentViewController:formatPageNav animated:YES completion:nil];
}

- (void)FXUIPickViewController:(FXUIPickViewController *)pickViewController Image:(UIImage *)image counts:(int)counts currentIndex:(int)currentIndex
{
    if (counts > 0) {
        if (image == nil) {
            _imageNilCounts += 1;
            if (currentIndex == counts - 1) {
                [self addImageToDocumentSession];
            }
        }else{
            if (currentIndex != counts - 1) {
                [self.photos addObject:image];
            }else{
                [self.photos addObject:image];
                [self addImageToDocumentSession];
            }
        }
        if (counts == _imageNilCounts && (currentIndex == counts - 1)) {
            dispatch_async(dispatch_get_main_queue(), ^{
                AlertViewCtrlShow(@"kWarning", @"kOK");
            });
            _cameraTriggered = NO;
            return;
        }
    }else{
        _cameraTriggered = NO;
    }
}

- (void)addImageToDocumentSession
{
    [self.view fs_showHUDLoading:nil process:^{
        for (int index = 0; index < self->_photos.count; index++) {
            UIImage *image = self->_photos[index];
            self->_imageProcess = [[LTImageProcessor alloc] initWithImage:image];
            const unsigned int LowResThreshold = 600 * 1000 * 1000;    // 600 MB
            bool useLowRes = [NSProcessInfo processInfo].physicalMemory < LowResThreshold;
            [self->_imageProcess setComputeLowResPreview: useLowRes];
            [self->_imageProcess setPageFormatWidth:self->_pageFormat.width height:self->_pageFormat.height receipt:self->_pageFormat.receipt];
            [self->_imageProcess setPageFormatThreshold:10.0f];
            [self->_imageProcess setColorMode:LTColorModeRGB];
            self->_imageProcess.delegate = self;
            [self->_imageProcess computeResult];
            self->isWaiting = YES;
            while (self->isWaiting) {
                [NSThread sleepForTimeInterval:0.1];
            }
        }
        self->_cameraTriggered = NO;
    }];
}

- (void) pageFomatViewController:(LTPageFormatViewControllerNew *)controller didFinishWithFormat:(LTPageFormat *)pageFormat
{
    [controller dismissViewControllerAnimated:YES completion:^{
        self->_pageFormat = pageFormat;
        [LTPageFormatHelper storeFormat:pageFormat];
        self->_cameraTriggered = NO;
    }];
}

#pragma mark - LTCaptureService delegate

- (void) captureService:(LTCaptureService *)service didCaptureImage:(UIImage *)image withDetection:(LTDetectionResult*) detection
{
    _imageProcess = [[LTImageProcessor alloc] initWithImage:image];
    const unsigned int LowResThreshold = 600 * 1000 * 1000;	// 600 MB
    bool useLowRes = [NSProcessInfo processInfo].physicalMemory < LowResThreshold;
    [_imageProcess setComputeLowResPreview: useLowRes];
    LTPageFormat *format =_pageFormat;
    if (!detection.valid) {
        NSArray *points = [NSMutableArray arrayWithObjects:
                            [NSValue valueWithCGPoint:CGPointMake(0, 0)],
                            [NSValue valueWithCGPoint:CGPointMake(image.size.width, 0)],
                            [NSValue valueWithCGPoint:CGPointMake(image.size.width, image.size.height)],
                            [NSValue valueWithCGPoint:CGPointMake(0, image.size.height)],
                            nil];
         detection = [[LTDetectionResult alloc]
                                    initWithPoints:points
                                    imageWidth:image.size.width
                                    imageHeight:image.size.height];
    }
     [_imageProcess setDetectionResult:detection];

    if (!self.detectionBtn.selected || (self.detectionBtn.selected && !detection.valid)) {
        int horDpi = [_imageProcess horizontalDpi];
        int verDpi = [_imageProcess verticalDpi];
        CGSize size = image.size;
        format = [LTPageFormatHelper evaluateFormat:size.width height:size.height horDpi:horDpi verDpi:verDpi];
    }
    [_imageProcess setPageFormatWidth:format.width height:format.height receipt:format.receipt];
   
    [_imageProcess setPageFormatThreshold:10.0f];
    [_imageProcess setColorMode:LTColorModeRGB];
     _imageProcess.delegate = self;
    
    [_imageProcess computeResult];
    [self.view fs_showHUDLoading:nil];
    [_detectionLayer updateResultDisplay:nil];
}

- (void) captureService:(LTCaptureService *)service detected:(LTDetectionResult *)result
{
    if (!_cameraTriggered && service.useAutoTrigger){
        [_statusLabelController updateMessage:result];
        [_detectionLayer updateResultDisplay:result];
    }
}

- (void) captureServiceDidAutoTrigger:(LTCaptureService *)service
{
    _cameraTriggered = YES;
}

- (void) captureServiceAutoTriggerCanceled:(LTCaptureService *)service
{
    _cameraTriggered = NO;
}

- (void) captureService:(LTCaptureService*) service proposesGUIUpdate:(LTDetectionResult*) result
{
    
    if (!_cameraTriggered && service.useAutoTrigger) {
        [_detectionLayer updateResultDisplay:result];
    }
}

- (void) imageProcessorStartedComputing:(LTImageProcessor*) processor
{
    //NSLog(@"start computing");
}

- (void) imageProcessor:(LTImageProcessor*) processor didFinishComputingPreviewImage:(UIImage*) image
{
    [self.view fs_hideHUDLoading];
}

- (void) imageProcessor:(LTImageProcessor*) processor didFinishComputingResultImage:(UIImage*) image
{
    if (_cameraTriggered) {
        [self.view fs_hideHUDLoading];
    }
    if (_documentSession != nil)
    {
        [_documentSession addPage:image];
    }
    else
    {
        _documentSession = [LTDocumentSession createNewDocument];
        _documentSession.delegate = self;
        _documentSession.rename = nil;
        [_documentSession addPage:image];
    }
    [self configureAndSetLayoutOfImageCount];
    //_cameraTriggered = NO;
    if (!self.singleFrametBtn.selected && !isWaiting) {
        LTImageProcessViewControllerNew *imageProcessViewControllerNew = [[LTImageProcessViewControllerNew alloc] initWithImage:[_documentSession imageForPage:(_documentSession.pageCount - 1)] detectionResult:nil];
        imageProcessViewControllerNew.delegate = self;
        
        [self presentViewController:imageProcessViewControllerNew animated:YES completion:nil];
        _pageInProcessor = _documentSession.pageCount - 1;
    }
    
    if (!isWaiting && self.singleFrametBtn.selected) {
        [_captureService start];
        if (self.flashlightBtn.selected) {
            _camera.lightEnabled = self.flashlightBtn.selected;
        }
    }
    _cameraTriggered = NO;
    isWaiting = NO;
}

- (void) processImageView:(LTImageProcessViewControllerNew*) ctrl
      didFinishWithResult:(UIImage*) image
{
    if (image) {
        [_documentSession replacePage:_pageInProcessor withImage:image];
    }
    [self capturedImagecountBtnTap];
}

- (void)configureAndSetLayoutOfImageCount
{
    NSString *countText;
    if (self.documentSession) {
        if (self.documentSession.pageCount > 0) {
            countText = [NSString stringWithFormat:@"%d",self.documentSession.pageCount];
        }else{
            countText = @"0";
        }
    }else{
        countText = @"0";
    }
    self.countLabel.text = countText;
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.shootBtn.mas_right);
        make.top.mas_equalTo(self.shootBtn.mas_top);
        make.width.mas_equalTo(29);
    }];
}

- (void) documentSession:(LTDocumentSession*) session startedSavingDocumentToFile:(NSString*) documentName
{
    [self.view fs_showHUDLoading:nil];
}

- (void) documentSession:(LTDocumentSession*) session didFinishSavingDocument:(LTDocument*) document
{
    [self.view fs_hideHUDLoading];
    [[PDFScanManager shareManager].documentManager addDocument:document];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
