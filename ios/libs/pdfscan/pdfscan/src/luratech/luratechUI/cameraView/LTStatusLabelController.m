//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import "LTStatusLabelController.h"

/*! Defines the number of frames in which the flags must differ before an update occurs.
	The delay is required to prevent steady changes of the displayed text.
 */
static const int UpdateDelay = 2;

static const int MessageNone			= 0;
static const int MessageNoDetection		= 1;
static const int MessageMoveCloser		= 2;
static const int MessageGlare			= 3;
static const int MessageTooDark			= 4;
static const int MessageHoldStill		= 5;

@interface LTStatusLabelController ()

@property (nonatomic) int activeMessage;
@property (nonatomic) int changeCandidate;
@property (nonatomic) int numChangeCandidateOcccurences;

@property UILabel* label;
@end


@implementation LTStatusLabelController

- (id) initWithLabel:(UILabel *)label
{
	self = [super init];
	if (self)
	{
		_label = label;
        _label.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
		_label.hidden = YES;
		
		self.activeMessage = 0;
		self.changeCandidate = 0;
		self.numChangeCandidateOcccurences = 0;
	}
	
	return self;
}

- (void) updateMessage:(LTDetectionResult*) result
{
	int candidateMsg = [self messageForResult:result];
	if (candidateMsg != _activeMessage && [self shouldUpdate:result with:candidateMsg])
	{
		[self updateMessage];
	}
}

- (BOOL) shouldUpdate:(LTDetectionResult*) result with:(int)candidateMsg
{
	[self updateCandidate:candidateMsg];

	if (_numChangeCandidateOcccurences > UpdateDelay || [self activeMessageIsImmediateDiscard])
	{
		_activeMessage = candidateMsg;
		_numChangeCandidateOcccurences = 0;
		_changeCandidate = MessageNone;
		return YES;
	}
	else
	{
		return NO;
	}
}

/*! Checks whether there is a new update candidate or not and increases the delay counter. */
- (void) updateCandidate:(int) candidateMsg
{
	if (candidateMsg == _changeCandidate)
	{
		_numChangeCandidateOcccurences++;
	}
	else
	{
		_numChangeCandidateOcccurences = 1;
		_changeCandidate = candidateMsg;
	}
}

- (BOOL) activeMessageIsImmediateDiscard
{
	return _activeMessage == MessageTooDark
		|| _activeMessage == MessageGlare;
}

- (void) updateMessage
{
	NSString* msg = nil;
	
	switch (_activeMessage)
	{
		case MessageNoDetection:
			msg = FSLocalizedForKey(@"kNoDetect");
			break;
		case MessageMoveCloser:
			msg = FSLocalizedForKey(@"kMoveClear");
			break;
		case MessageGlare:
			msg = FSLocalizedForKey(@"kGlareDetect");
			break;
		case MessageTooDark:
			msg = FSLocalizedForKey(@"kDark");
			break;
		case MessageHoldStill:
			msg = FSLocalizedForKey(@"kHoldStill");
			break;
		case MessageNone:
		default:
			msg = nil;
	}
	
	_label.text = msg;
	_label.hidden = (msg == nil);
}

- (int) messageForResult:(LTDetectionResult*) result
{
	if (result == nil)
		return MessageNone;
	else if (!result.valid)
		return MessageNoDetection;
	else if(result.tooDark)
		return MessageTooDark;
	else if (!result.largeEnough)
		return MessageMoveCloser;
	else if(result.hasGlare)
		return MessageGlare;
	else
		return MessageHoldStill;
}

@end
