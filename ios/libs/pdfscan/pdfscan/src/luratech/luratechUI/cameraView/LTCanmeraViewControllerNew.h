//
//  LTCanmeraViewControllerNew.h
//  FoxitApp
//
//  Created by Apple on 16/11/30.
//
//

#import <UIKit/UIKit.h>
#import "LTPageFormatViewControllerNew.h"
#import "LTDocumentSession+rename.h"

@class LTDetectionLayer;
@class LTDetectionFailReason;
@class LTCameraViewController;

@protocol LTCameraViewDelegate<NSObject>
- (void) cameraViewDoneWithSaveAsPDF:(LTCameraViewController*) view;
@end

@interface LTCanmeraViewControllerNew : UIViewController<LTCaptureServiceDelegate,UIActionSheetDelegate,LTPageFormatViewControllerDelegate>

@property (nonatomic, strong) LTDocumentSession* documentSession;
@property (weak) NSObject<LTCameraViewDelegate>* delegate;

@property (nonatomic, assign) BOOL isOpenFromEditor;
@property AVCaptureVideoPreviewLayer* previewLayer;
@property LTDetectionLayer* detectionLayer;
@property (nonatomic, assign) BOOL isCallCameraForImages;
@property (nonatomic, copy)  void(^callBackImages)(NSArray *imagesArray);

@end
