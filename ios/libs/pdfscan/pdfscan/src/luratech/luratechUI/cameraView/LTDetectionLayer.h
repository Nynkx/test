//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import "LTDetectionIndicationLayer.h"

@class LTDetectionResult;
@class AVCaptureVideoPreviewLayer;


@interface LTDetectionLayer : LTDetectionIndicationLayer

- (id) initWithPreviewLayer:(AVCaptureVideoPreviewLayer*) previewLayer;
- (void) updateResultDisplay:(LTDetectionResult*) result;

@end
