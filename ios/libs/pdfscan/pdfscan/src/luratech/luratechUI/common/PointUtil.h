//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#pragma once

#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>

/*! \brief Compute the squared distance between two points. */
static inline float squaredDistance(CGPoint p, CGPoint q)
{
	float dx = p.x - q.x;
	float dy = p.y - q.y;
	return (dx*dx) + (dy*dy);
}

/*! \brief Get a point from a NSValue. */
static inline CGPoint point(NSValue* value)
{
	CGPoint p;
	[value getValue:&p];
	return p;
}

/*! \brief Get the difference vector between two points */
static inline CGPoint deltaVector(CGPoint p1, CGPoint p2)
{
	return CGPointMake(p2.x - p1.x, p2.y - p1.y);
}

/*! \brief Get the squared length of a vector. */
static inline float length2(CGPoint v)
{
	return (v.x * v.x) + (v.y * v.y);
}

/*! \brief Get the length of a vector. */
static inline float length(CGPoint v)
{
	return sqrt(length2(v));
}

/*! \brief Get the mid points for a line connecting two points. */
static inline CGPoint midPoint(CGPoint p1, CGPoint p2)
{
	CGPoint delta = deltaVector(p1, p2);
	delta.x *= (CGFloat)0.5;
	delta.y *= (CGFloat)0.5;
	
	return CGPointMake(p1.x + delta.x, p1.y + delta.y);
}

static inline CGPoint normalize(CGPoint p)
{
	CGFloat l = length(p);
	return CGPointMake(p.x / l, p.y / l);
}

static inline CGPoint computeNormal(CGPoint line)
{
	return normalize(CGPointMake(-line.y, line.x));
}

static inline CGPoint absolute(CGPoint line)
{
	return CGPointMake(line.x >= 0 ? line.x : -line.x,
					   line.y >= 0 ? line.y : -line.y);
}