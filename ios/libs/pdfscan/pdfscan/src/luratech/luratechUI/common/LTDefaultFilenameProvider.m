//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import "LTDefaultFilenameProvider.h"

@implementation LTDefaultFilenameProvider

+ (NSString*) timeBasedFilename
{
	NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
	formatter.dateFormat = @"yyyy-MM-dd HH-mm-ss";
	formatter.locale = [NSLocale currentLocale];
	
	return [formatter stringFromDate:[NSDate date]];
}

+ (NSString *)filterFileNameSpecialCharactor:(NSString *)fileName
{
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"\\/:*?\\\\<>|"];
    NSString *newFilenameString = [fileName stringByTrimmingCharactersInSet:set];
    return newFilenameString;
}

@end
