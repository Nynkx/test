//
//  Foxit Europe AppFrameworkSDK Demo
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//
//	You may use the provided source code for the duration of your evaluation period.
//	Redistribution in binary form, with or without modification, is permitted upon
//  condition that the distributing person/company/institution has a valid license
//  for the Foxit Europe AppFrameworkSDK. Redistribution in source form is not allowed.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface LTDetectionIndicationLayer : CALayer

@property CGRect rect;

@property (nonatomic) UIColor* fillColor;
@property (nonatomic) UIColor* lineColor;

- (id) initWithRect:(CGRect) rect;
- (void) update:(NSArray*) points;

@end
