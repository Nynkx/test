//
//  Foxit Europe AppFrameworkSDK Demo
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//
//	You may use the provided source code for the duration of your evaluation period.
//	Redistribution in binary form, with or without modification, is permitted upon
//  condition that the distributing person/company/institution has a valid license
//  for the Foxit Europe AppFrameworkSDK. Redistribution in source form is not allowed.
//

#import "LTDetectionIndicationLayer.h"
#import "PointUtil.h"

@interface LTDetectionIndicationLayer()
@property CAShapeLayer* markerLayer;
@property CAShapeLayer* shadowLayer;
@end


@implementation LTDetectionIndicationLayer

- (id) initWithRect:(CGRect)rect
{
	self = [super init];
	if (self)
	{
		_fillColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		_lineColor = [UIColor colorWithRed:0 green:122.0f/255 blue:1 alpha:1.0f];
		
		_rect = rect;
		[self initLayers];
	}
	
	return self;
}

- (void) setFillColor:(UIColor *)fillColor
{
	_fillColor = fillColor;
	[self initLayers];
}

- (void) setLineColor:(UIColor *)lineColor
{
	_lineColor = lineColor;
	[self initLayers];
}

- (void) initLayers
{
	[self createShadowLayer:_fillColor stroke:_fillColor];
	[self createMarkerLayer:_fillColor stroke:_lineColor];
}

- (void) createShadowLayer:(UIColor*)fill stroke:(UIColor*)stroke
{
	if (_shadowLayer == nil)
	{
		_shadowLayer = [CAShapeLayer layer];
		[self addSublayer:_shadowLayer];
	}
	
	_shadowLayer.fillColor = fill.CGColor;
}

- (void) createMarkerLayer:(UIColor *)fill stroke:(UIColor *)stroke
{
	if (_markerLayer == nil)
	{
		self.markerLayer = [CAShapeLayer layer];
		self.markerLayer.lineWidth = 2;
		self.markerLayer.lineCap = kCALineCapRound;
		self.markerLayer.fillColor = nil;
		[self addSublayer:self.markerLayer];
	}
	
	self.markerLayer.strokeColor = stroke.CGColor;
}

- (void) update:(NSArray*) points
{
	[self updateMarker:points];
	[self updateShadow:points];
}

- (void) updateMarker:(NSArray*) points
{
	CGMutablePathRef path = CGPathCreateMutable();
	CGPoint p;
	
	p = point([points lastObject]);
	CGPathMoveToPoint(path, nil, p.x, p.y);
	
	for (NSValue* value in points)
	{
		p = point(value);
		CGPathAddLineToPoint(path, nil, p.x, p.y);
	}
	
	self.markerLayer.path = path;
    
    CGPathRelease(path);
}

- (void) updateShadow:(NSArray*) points
{
	if (_shadowLayer == nil)
		return;
    
	const CGPoint corners[] = {
		{ 0, 0 },
		{ _rect.size.width, 0 },
		{ _rect.size.width, _rect.size.height },
		{ 0, _rect.size.height }
	};
	
	CGMutablePathRef path = CGPathCreateMutable();
	
	for (int i = 0; i < points.count; ++i)
	{
		const int j = (i+1) % 4;
		
		CGPoint p1 = point(points[i]);
		CGPoint p2 = point(points[j]);
		CGPoint p3 = corners[(j+1) % 4];
		CGPoint p4 = corners[(i+1) % 4];
		
		CGPathMoveToPoint(path, nil, p1.x, p1.y);
		CGPathAddLineToPoint(path, nil, p2.x, p2.y);
		CGPathAddLineToPoint(path, nil, p3.x, p3.y);
		CGPathAddLineToPoint(path, nil, p4.x, p4.y);
		CGPathAddLineToPoint(path, nil, p1.x, p1.y);
	}
	
	_shadowLayer.path = path;

    CGPathRelease(path);
}

@end
