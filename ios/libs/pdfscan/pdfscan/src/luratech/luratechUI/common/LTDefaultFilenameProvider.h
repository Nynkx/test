//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import <Foundation/Foundation.h>

@interface LTDefaultFilenameProvider : NSObject

/*! \brief Returns a filename based on the current time.
 
	The used format is YYYY-MM-DD hh:mm:ss
 */
+ (NSString*) timeBasedFilename;
+ (NSString *)filterFileNameSpecialCharactor:(NSString *)fileName;

@end
