//
//  LTDocumentEditViewControllerNew.h
//  FoxitApp
//
//  Created by Apple on 16/12/1.
//
//

#import <UIKit/UIKit.h>
#import "LTCanmeraViewControllerNew.h"
#import "RACollectionViewReorderableTripletLayout.h"
#import "LTImageProcessViewControllerNew.h"

@interface LTDocumentEditViewControllerNew : UIViewController<UIGestureRecognizerDelegate,RACollectionViewDelegateReorderableTripletLayout, RACollectionViewReorderableTripletLayoutDataSource,LTDocumentSessionDelegate,LTImageProcessViewControllerDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) LTDocumentSession* documentSession;
@property (assign, nonatomic) BOOL isDocumentEditModel;
//@property (strong, nonatomic) NSMutableArray* arrImageCollection;
@property (nonatomic, copy) void(^normalModelSavedDocument)(void);
@property (nonatomic, copy) void(^discardCompelet)(void);
@property (nonatomic, copy) void(^addPageDocumentReload)(LTDocumentSession *session);

@end
