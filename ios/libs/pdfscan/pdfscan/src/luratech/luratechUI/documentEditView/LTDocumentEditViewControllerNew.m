//
//  LTDocumentEditViewControllerNew.m
//  FoxitApp
//
//  Created by Apple on 16/12/1.
//
//

#import "pageCollectionViewCell.h"
#import "LTCollectionViewScaleHelper.h"

#import "LTPageFormatHelper.h"
#import "LTDefaultFilenameProvider.h"

#import "LTDocumentEditViewControllerNew.h"
//#import "LTEditDocumentName.h"
//#import <ENSDK/ENSDK.h>
#import "SaveAsViewController.h"
#import "PDFScanManager+private.h"
#import "UITableView+EmptyData.h"

@interface LTDocumentEditViewControllerNew ()<UIGestureRecognizerDelegate ,UIPopoverControllerDelegate>

@property (weak, nonatomic) UICollectionViewCell* addPageCell;
//@property (nonatomic, strong) LTImageProcessor* imageProcessor;

@property (nonatomic) LTCollectionViewScaleHelper* scaleHelper;
@property (nonatomic, copy) NSString *documentName;
@property (nonatomic) int pageInProcessor;
@property (nonatomic, assign) BOOL isEditing;

@property (nonatomic, strong) FSToolbar *topBar;
@property (nonatomic, strong) FSToolbar *bottomBar;

@property (nonatomic, strong) TbBaseItem *addPageItem;
@property (nonatomic, strong) TbBaseItem *saveItem;
@property (nonatomic, strong) TbBaseItem *deleteItem;

@property (nonatomic, strong) UIButton *discardBtn;
@property (nonatomic, strong) UIButton *editBtn;
@property (nonatomic, strong) UIButton *doneBtn;
@property (nonatomic, strong) UIButton *selectAllBtn;
@property (nonatomic, strong) UIButton *fileNameBtn;

@property (nonatomic, strong) NSMutableArray *selectedArray;
//@property (nonatomic, assign) BOOL isDiscard;
@property (nonatomic, assign) BOOL isSave;
@property (nonatomic, assign) BOOL isSavePicture;
@property (nonatomic, assign) BOOL isReloadDocument;
@property (nonatomic, strong) NSString *savedPath;

@property (nonatomic, strong) UIView *fileNameLine;
@property (nonatomic, strong) NSString *oldFileName;
@end

@implementation LTDocumentEditViewControllerNew

- (void)viewDidLoad {
    [super viewDidLoad];
    _isReloadDocument = NO;
    _isEditing = NO;
    if (self.documentSession.isNewDocument && self.documentSession.rename == nil) {
        _documentName = [@"Scan " stringByAppendingString:[LTDefaultFilenameProvider timeBasedFilename]];
        _documentName = [LTDefaultFilenameProvider filterFileNameSpecialCharactor:_documentName];
    }else{
        if (self.documentSession.rename != nil) {
            _documentName = self.documentSession.rename;
        }else{
            _documentName = self.documentSession.documentName;
        }
    }
    [self initSubViews];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
    if (self.documentSession.pageCount < 1) {
        _saveItem.enable = NO;
    }else{
        _saveItem.enable = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.documentSession) {
        self.documentSession.delegate = self;
    }
}

- (void)initSubViews
{
    FSToolbar *topbar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleTop];
    topbar.barTintColor = ThemeBarColor;
    _topBar = topbar;
    [self.view addSubview:topbar];
    [topbar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(topbar.superview);
    }];
    
    [self initTopbar:topbar];
    
    FSToolbar *bottomBar = [[FSToolbar alloc] initWithStyle:FSToolbarStyleBottom];
    _bottomBar = bottomBar;
    [self.view addSubview:bottomBar];
    [bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(bottomBar.superview);
    }];
    [self initBottombar:bottomBar];
    
    RACollectionViewReorderableTripletLayout *raLayout = [[RACollectionViewReorderableTripletLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:raLayout];
    self.collectionView.backgroundColor = ThemeCellBackgroundColor;
    raLayout.itemSize = CGSizeMake(DEVICE_iPHONE ? 78 : 152 , DEVICE_iPHONE ? 98 : 172);
    raLayout.minimumLineSpacing = 0;
    raLayout.minimumInteritemSpacing = 2;
    raLayout.delegate = self;
    raLayout.datasource = self;
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topbar.superview.mas_left);
        make.right.mas_equalTo(topbar.superview.mas_right);
        make.bottom.mas_equalTo(bottomBar.mas_top);
        make.top.mas_equalTo(topbar.mas_bottom);
    }];
    
    [self.collectionView registerClass:[pageCollectionViewCell class] forCellWithReuseIdentifier:@"pageCellId"];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)initTopbar:(UIView *)topBar
{
    UIButton *discardBtn = [[UIButton alloc] init];
    _discardBtn = discardBtn;
    [discardBtn setImage:ImageNamed(@"common_back_white") forState:UIControlStateNormal];
    [discardBtn addTarget:self action:@selector(discardBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [topBar addSubview:discardBtn];
    [discardBtn setAccessibilityLabel:FSLocalizedForKey(@"kBack")];
    
    [discardBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(discardBtn.superview.mas_left).offset(4);
        make.centerY.mas_equalTo(discardBtn.superview);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *selectAllBtn = [[UIButton alloc] init];
    _selectAllBtn = selectAllBtn;
    _selectAllBtn.accessibilityLabel = FSLocalizedForKey(@"kSelectAll");
    selectAllBtn.hidden = YES;
    [selectAllBtn setImage:ImageNamed(@"selectall") forState:UIControlStateNormal];
    [selectAllBtn setImage:[Utility unableImage:ImageNamed(@"selectall")] forState:UIControlStateDisabled];
    [selectAllBtn setImage:ImageNamed(@"selectall_select") forState:UIControlStateSelected];
    [selectAllBtn addTarget:self action:@selector(selectAllBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [topBar addSubview:selectAllBtn];
    [selectAllBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectAllBtn.superview).offset(4);
        make.centerY.mas_equalTo(selectAllBtn.superview);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    UIButton *editBtn = [[UIButton alloc] init];
    editBtn.accessibilityLabel = FSLocalizedForKey(@"kEdit");
    [editBtn setTitle:FSLocalizedForKey(@"kEdit") forState:UIControlStateNormal];
    [editBtn setTitleColor:WhiteThemeTextColor forState:UIControlStateNormal];
    [editBtn setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    [editBtn setTitleColor:GrayThemeTextColor forState:UIControlStateDisabled];
    editBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    editBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    [editBtn addTarget:self action:@selector(editBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    _editBtn = editBtn;
    
    [topBar addSubview:editBtn];
    [editBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(editBtn.superview).offset(-14);
        make.centerY.mas_equalTo(discardBtn);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *doneBtn = [[UIButton alloc] init];
    doneBtn.hidden = YES;
    _doneBtn = doneBtn;
    [doneBtn setTitle:FSLocalizedForKey(@"kDone") forState:UIControlStateNormal];
    [doneBtn setTitleColor:WhiteThemeTextColor forState:UIControlStateNormal];
    [doneBtn setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    doneBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    doneBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    [doneBtn addTarget:self action:@selector(doneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [topBar addSubview:doneBtn];
    [doneBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(doneBtn.superview).offset(-14);
        make.centerY.mas_equalTo(doneBtn.superview);
        make.height.mas_equalTo(40);
    }];

    UIButton *fileNameBtn = [[UIButton alloc] init];
    _fileNameBtn = fileNameBtn;
    [fileNameBtn setTitle:_documentName forState:UIControlStateNormal];
    [fileNameBtn setTitleColor:WhiteThemeTextColor forState:UIControlStateNormal];
    [fileNameBtn setTitleColor:GrayThemeTextColor forState:UIControlStateHighlighted];
    fileNameBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    fileNameBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [fileNameBtn addTarget:self action:@selector(fileNameBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [topBar addSubview:fileNameBtn];
    [fileNameBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(fileNameBtn.superview);
        make.height.mas_equalTo(44);
        make.left.mas_greaterThanOrEqualTo(discardBtn.mas_right).offset(10);
        make.right.mas_lessThanOrEqualTo(editBtn.mas_left).offset(-10);
    }];
    
    UIView *fileNameLine = [[UIView alloc] init];
    _fileNameLine = fileNameLine;
    fileNameLine.backgroundColor = WhiteThemeTextColor;
    
    CGRect stringRect = [fileNameBtn.titleLabel.text boundingRectWithSize:CGSizeMake(200, 44) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fileNameBtn.titleLabel.font} context:NULL];
    [fileNameBtn addSubview:fileNameLine];
    [fileNameLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(fileNameLine.superview.mas_centerX);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(fileNameLine.superview.mas_bottom).offset(-((44 - stringRect.size.height) * 0.5 - 1));
        make.width.mas_equalTo(fileNameBtn.mas_width);
    }];
}

- (void)initBottombar:(UIView *)bottomBar
{
    //kDelete
    TbBaseItem *addPageItem = [TbBaseItem createItemWithImage:ImageNamed(@"organizeAdd") imageSelected:ImageNamed(@"organizeAdd") imageDisable:ImageNamed(@"organizeAdd")];
    _addPageItem = addPageItem;
    [bottomBar addSubview:addPageItem.contentView];
    CGFloat width = addPageItem.contentView.frame.size.width;
    CGFloat height = addPageItem.contentView.frame.size.height;
    [addPageItem.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(addPageItem.contentView.superview.mas_centerX).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? -16 : -36);
        make.centerY.mas_equalTo(addPageItem.contentView.superview.mas_centerY);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    addPageItem.onTapClick = ^(TbBaseItem *item){
        if (self->_isDocumentEditModel) {
            NSString *mediaType = AVMediaTypeVideo;
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
            if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied)
            {
                
              UIAlertController *alert = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kNotAllowTakePhoto") message:FSLocalizedForKey(@"kCAMERADenied") actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kGoSetting") ] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                    if (index == 1){
                        NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        if([[UIApplication sharedApplication] canOpenURL:url]) {
                            [[UIApplication sharedApplication] openURL:url];
                        }
                    }
                }];
                [self presentViewController:alert animated:YES completion:nil];
                 return;
            }else if (authStatus == AVAuthorizationStatusNotDetermined){
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (granted) {
                            LTCanmeraViewControllerNew* cameraView = [[LTCanmeraViewControllerNew alloc] init];
                            cameraView.documentSession = self->_documentSession;
                            cameraView.isOpenFromEditor = YES;
                            [self presentViewController:cameraView animated:YES completion:nil];
                        }else{
                            UIAlertController *alert = [UIAlertController fs_alertWithTitle:FSLocalizedForKey(@"kNotAllowTakePhoto") message:FSLocalizedForKey(@"kCAMERADenied") actions:@[ FSLocalizedForKey(@"kCancel"), FSLocalizedForKey(@"kGoSetting")] actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                                   if (index == 1){
                                        NSURL *url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                        if([[UIApplication sharedApplication] canOpenURL:url]) {
                                            [[UIApplication sharedApplication] openURL:url];
                                        }
                                    }
                              }];
                              [self presentViewController:alert animated:YES completion:nil];
                            return;
                        }
                    });
                }];
            }else{
                LTCanmeraViewControllerNew* cameraView = [[LTCanmeraViewControllerNew alloc] init];
                cameraView.documentSession = self->_documentSession;
                cameraView.isOpenFromEditor = YES;
                cameraView.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:cameraView animated:YES completion:nil];
            }
        }else{
            if (self->_isReloadDocument && self.addPageDocumentReload) {
                self.addPageDocumentReload(self->_documentSession);
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    };
    
    TbBaseItem *deleteItem = [TbBaseItem createItemWithImage:ImageNamed(@"thumb_delete_black") imageSelected:ImageNamed(@"thumb_delete_black") imageDisable:ImageNamed(@"thumb_delete_black")];
    _deleteItem = deleteItem;
    deleteItem.contentView.hidden = YES;
    [bottomBar addSubview:deleteItem.contentView];
    width = deleteItem.contentView.frame.size.width;
    height = deleteItem.contentView.frame.size.height;
    [deleteItem.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(deleteItem.contentView.superview.mas_centerX).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? -16 : -36);
        make.centerY.mas_equalTo(deleteItem.contentView.superview.mas_centerY);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    deleteItem.onTapClick = ^(TbBaseItem *item){
        for (int i =0; i < self.documentSession.pageCount; i++) {
            if ([self->_selectedArray[i] boolValue]) {
                [self.documentSession removePage:i];
                [self->_selectedArray removeObjectAtIndex:i];
                i--;
            }
        }
        self->_deleteItem.enable = NO;
        self->_selectAllBtn.selected = NO;
        if (self.documentSession) {
            if (self.documentSession.pageCount > 0) {
                self->_selectAllBtn.enabled = YES;
            }else{
                self->_selectAllBtn.enabled = NO;
            }
        }else{
            self->_selectAllBtn.enabled = NO;
        }
        [self.collectionView reloadData];
        if (self.documentSession.pageCount < 1) {
            self->_saveItem.enable = NO;
        }else{
            self->_saveItem.enable = YES;
        }
    };

    TbBaseItem *saveItem = [TbBaseItem createItemWithImage:ImageNamed(@"more_saveas_icon") imageSelected:ImageNamed(@"more_saveas_icon") imageDisable:ImageNamed(@"more_saveas_icon")];
    _saveItem = saveItem;
    [bottomBar addSubview:saveItem.contentView];
    width = saveItem.contentView.frame.size.width;
    height = saveItem.contentView.frame.size.height;
    [saveItem.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(saveItem.contentView.superview.mas_centerX).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? 16 : 36);
        make.centerY.mas_equalTo(saveItem.contentView.superview.mas_centerY);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    @weakify(self);
    saveItem.onTapClick = ^(TbBaseItem *item){
        @strongify(self);
        [self saveAs];
    };
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    [collectionView fs_collectionDisplayWitMsg:FSLocalizedForKey(@"kAddNewScanImageTip") ifNecessaryForRowCount:self.documentSession.pageCount];
    self.selectAllBtn.enabled = self.documentSession.pageCount;
    self.editBtn.enabled = self.documentSession.pageCount;
    if (self.documentSession.pageCount) {
        return self.documentSession.pageCount;
    }
    [self.doneBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    return 0;//_photosArray.count;
}

- (CGFloat)reorderingItemAlpha:(UICollectionView *)collectionview
{
    return .3f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView sizeForLargeItemsInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(12, 24, 12, 24);
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.collectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath didMoveToIndexPath:(NSIndexPath *)toIndexPath
{
    [_documentSession movePage:(int)fromIndexPath.item toIndex:(int)toIndexPath.item];
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath
{
    if (_isEditing) {
        return NO;
    }
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isEditing) {
        return NO;
    }
    return YES;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"pageCellId";
    pageCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    //cell.backgroundColor = [UIColor yellowColor];
    cell.thumbImage = [_documentSession thumbnailForPage:(int)indexPath.item];//_arrImageCollection[indexPath.item];
    cell.pageIndex = indexPath.item + 1;
    cell.isSelected = NO;
    if (_isEditing) {
        cell.isEditing = YES;
        cell.isSelected = [[_selectedArray objectAtIndex:indexPath.row] boolValue];
    }else{
        cell.isEditing = NO;
    }
    [cell.contentView setAccessibilityLabel:[NSString stringWithFormat:@"%ld",cell.pageIndex]];
    [cell.contentView setAccessibilityTraits:UIAccessibilityTraitButton];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isEditing) {
        pageCollectionViewCell *cell = (pageCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.isSelected = !cell.isSelected;
        _selectedArray[indexPath.item] = [NSNumber numberWithBool:cell.isSelected];
        BOOL isAllSelected = YES;
        for (NSNumber *value in _selectedArray) {
            if (![value boolValue]) {
                isAllSelected = NO;
                break;
            }
        }
        _selectAllBtn.selected = isAllSelected;
        
        BOOL hasSelected = NO;
        for (NSNumber *value in _selectedArray) {
            if ([value boolValue]) {
                hasSelected = YES;
                break;
            }
        }
        _deleteItem.enable = hasSelected;
    }else{
        UIImage *editImage = [_documentSession imageForPage:(int)indexPath.item];
        LTImageProcessViewControllerNew *imageProcessViewControllerNew = [[LTImageProcessViewControllerNew alloc] initWithImage:editImage detectionResult:nil];
        imageProcessViewControllerNew.delegate = self;
        _pageInProcessor = (int)indexPath.item;
        [self.navigationController pushViewController:imageProcessViewControllerNew animated:YES];
    }
}

- (void)processImageView:(LTImageProcessViewControllerNew *)ctrl didFinishWithResult:(UIImage *)image
{
    if (image) {
        [self.documentSession replacePage:_pageInProcessor withImage:image];
        [self.collectionView reloadData];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}

#pragma mark ------topbar btn click event------
- (void)discardBtnClick:(UIButton *)btn
{
    if (self.documentSession.pageCount == 0) {
        if ([self fileWithNameDoesExist:self.documentSession.documentName]) {
            [[PDFScanManager shareManager].documentManager removeDocument:[[PDFScanManager shareManager].documentManager documentByName:_documentName]];
        }
        if (self.discardCompelet) {
            self.discardCompelet();
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    if (self.documentSession.isNewDocument || self.documentSession.hasChanges || self.documentSession.rename != nil) {
        _isSavePicture = YES;
        _oldFileName = self.documentSession.documentName;
        [self.documentSession saveAsPDF:_documentName];
    }else{
        if (self.discardCompelet) {
            self.discardCompelet();
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)fileNameBtnClick:(UIButton *)btn
{
    if (_documentSession.pageCount > 0) {
        [self renameScaneFile:btn.titleLabel.text];
    }
}

- (void)renameScaneFile:(NSString *)fileName
{
    NSString *msg = FSLocalizedForKey(@"kInputNewFileName");//[NSString stringWithFormat:, fileName];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:FSLocalizedForKey(@"kRenameFileName") message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *renameaction = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kRename") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *newfileName = alertController.textFields.firstObject.text;
        NSString *newFile = [newfileName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (newFile == nil || newFile.length == 0)
        {
            AlertViewCtrlShow(@"kWarning", @"kInputNewFileName");
            return;
        }
        
        if ([newFile rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"\\/:*?\\\\<>|"]].location != NSNotFound)
        {
            [Utility alert_OK_WithTitle:@"kWarning" message:@"kIllegalNameWarning" actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                [self renameScaneFile:newFile];
            }];
            return;
        }
        
        if ([self fileWithNameDoesExist:newFile]) {
            [Utility alert_OK_WithTitle:@"kWarning" message:@"kFileFolderExist" actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                [self renameScaneFile:newFile];
            }];
        }else{
            self->_documentName = newFile;
            [self.fileNameBtn setTitle:newFile forState:UIControlStateNormal];
            self.documentSession.rename = newFile;
            CGRect stringRect = [self.fileNameBtn.titleLabel.text boundingRectWithSize:CGSizeMake(200, 44) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self->_fileNameBtn.titleLabel.font} context:NULL];
            [self->_fileNameLine mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self->_fileNameLine.superview.mas_centerX);
                make.height.mas_equalTo(1);
                make.bottom.mas_equalTo(self->_fileNameLine.superview.mas_bottom).offset(-((44 - stringRect.size.height) * 0.5 - 1));
                make.width.mas_equalTo(stringRect.size.width + 5);
            }];
            
            self->_oldFileName = self.documentSession.documentName;
        }
    }];
    renameaction.enabled = NO;
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:FSLocalizedForKey(@"kCancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = [fileName stringByDeletingPathExtension];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [textField setBlockForControlEvents:UIControlEventEditingChanged block:^(UITextField * _Nonnull sender) {
            renameaction.enabled = ![sender.text isEqualToString:self.fileNameBtn.titleLabel.text];
        }];
    }];
    [alertController addAction:cancel];
    [alertController addAction:renameaction];
    [self presentViewController:alertController animated:YES completion:nil];
    //    }
}

- (void)editBtnClick:(UIButton *)btn
{
    self.isEditing = YES;
    btn.hidden = YES;
    _doneBtn.hidden = NO;
    
    if (self.documentSession) {
        if (self.documentSession.pageCount > 0) {
            _selectAllBtn.enabled = YES;
        }else{
            _selectAllBtn.enabled = NO;
        }
    }else{
        _selectAllBtn.enabled = NO;
    }
}

- (void)doneBtnClick:(UIButton *)btn
{
    self.isEditing = NO;
    btn.hidden = YES;
    _editBtn.hidden = NO;
}

- (void)selectAllBtnClick:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected) {
        for (int i = 0; i < self.documentSession.pageCount; i++) {
            NSNumber *value = [NSNumber numberWithBool:YES];
            _selectedArray[i] = value;
        }
        _deleteItem.enable = YES;
    }else{
        for (int i = 0; i < self.documentSession.pageCount; i++) {
            NSNumber *value = [NSNumber numberWithBool:NO];
            _selectedArray[i] = value;
        }
        _deleteItem.enable = NO;
    }
    [self.collectionView reloadData];
}

- (void)setIsEditing:(BOOL)isEditing
{
    _isEditing = isEditing;
    if (isEditing) {
        _selectedArray = [NSMutableArray arrayWithCapacity:self.documentSession.pageCount];
        for (int i = 0; i < self.documentSession.pageCount; i++) {
            NSNumber *value = [NSNumber numberWithBool:NO];
            [_selectedArray addObject:value];
        }
        _deleteItem.enable = NO;
        _selectAllBtn.hidden = NO;
        _discardBtn.hidden = YES;
        _deleteItem.contentView.hidden = NO;
        _addPageItem.contentView.hidden = YES;
        _saveItem.contentView.hidden = YES;
    }else{
        [_selectedArray removeAllObjects];
        _selectedArray = nil;
        _selectAllBtn.hidden = YES;
        _discardBtn.hidden = NO;
        _deleteItem.contentView.hidden = YES;
        _addPageItem.contentView.hidden = NO;
        _saveItem.contentView.hidden = NO;
    }
    _selectAllBtn.selected = NO;
    [self.collectionView reloadData];
}

- (void)saveAs{
    _isSave = NO;
    NSString *fileName = [_documentName stringByAppendingString:@".pdf"];
    
    NSString *filePath = [[PDFScanManager shareManager].documentPath stringByAppendingPathComponent:fileName];
    
    SaveAsViewController *saveAsVC = [[SaveAsViewController alloc] init];
    FSNavigationController *nav = [[FSNavigationController alloc] initWithRootViewController:saveAsVC];
    saveAsVC.isScanSave = YES;
    saveAsVC.saveAsCallBack = ^(NSError * _Nonnull error, NSString * _Nonnull savePath) {
        self.savedPath = savePath;
        NSString *fileNameWithoutType = [[savePath lastPathComponent] stringByDeletingPathExtension];
        [self.documentSession saveAsPDF:fileNameWithoutType];
        LTDocumentManager *documentManager = [PDFScanManager shareManager].documentManager;
        for (LTDocument *document in documentManager.documents) {
            if ([document.documentName isEqualToString:self.documentName]) {
                [documentManager removeDocument:document];
                break;
            }
        }
    };
    [saveAsVC setPdfFilePath:filePath];

    if (DEVICE_iPHONE)
    {
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    else
    {
        nav.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [self presentViewController:nav animated:YES completion:nil];
}

- (void) documentSession:(LTDocumentSession*) session startedSavingDocumentToFile:(NSString*) documentName
{
    [self.view fs_showHUDLoading:nil];
}

- (void) documentSession:(LTDocumentSession*) session didFinishSavingDocument:(LTDocument*) document
{
    [self.view fs_hideHUDLoading];
    LTDocumentManager *documentManager = [[PDFScanManager shareManager] documentManager];
    [documentManager addDocument:document];

    if (_oldFileName.length > 0 && ![_oldFileName isEqualToString:document.documentName]) {
        if ([self fileWithNameDoesExist:_oldFileName]) {
            [documentManager removeDocument:[documentManager documentByName:_oldFileName]];
        }
        _oldFileName = nil;
    }

    if (self.savedPath.length) {
        NSString *realSaveAsPath = document.urlForDocument.path;
        NSError *error = nil;
        if (![self.savedPath isEqualToString:realSaveAsPath]) {
            [[NSFileManager defaultManager] copyItemAtPath:realSaveAsPath toPath:self.savedPath error:&error];
        }
        if(!error){
            [Utility alert_OK_WithTitle:@"kSuccess" message:@"kSaveSuccess" actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                !PDFScanManager.saveAsCallBack ? : PDFScanManager.saveAsCallBack(error , self.savedPath);
            }];
        }else{
            [Utility alert_OK_WithTitle:@"kFailed" message:@"kFailed" actionBack:^(NSUInteger index, UIAlertAction * _Nonnull action) {
                !PDFScanManager.saveAsCallBack ? : PDFScanManager.saveAsCallBack(error , self.savedPath);
            }];
        }
    }else{
        if (_isSavePicture) {
            _isSavePicture = NO;
            if (_isDocumentEditModel) {
                [self dismissViewControllerAnimated:NO completion:nil];
            }else{
                [self dismissViewControllerAnimated:NO completion:^{
                    if (self.normalModelSavedDocument) {
                        self.normalModelSavedDocument();
                    }
                }];
            }
        }
    }
}

- (BOOL)fileWithNameDoesExist:(NSString*) filename
{
    return [[PDFScanManager shareManager].documentManager documentByName:filename] != nil;
}

@end
