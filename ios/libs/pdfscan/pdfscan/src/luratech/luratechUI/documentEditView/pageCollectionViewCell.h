//
//  pageCollectionViewCell.h
//  FoxitApp
//
//  Created by Apple on 16/12/1.
//
//

#import <UIKit/UIKit.h>

@interface pageCollectionViewCell : UICollectionViewCell
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, strong) UIImage *thumbImage;
@property (nonatomic, assign) NSInteger pageIndex;
@end
