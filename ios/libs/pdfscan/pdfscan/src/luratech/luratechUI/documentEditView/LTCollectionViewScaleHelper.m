//
//  LuraTech AppFrameworkSDK Demo
//
//  Copyright (c) 2015 LuraTech Imaging GmbH.
//
//	You may use the provided source code for the duration of your evaluation period.
//
//	Redistribution in binary form, with or without modification, is permitted upon condition that
//	the distributor has a valid license for the LuraTech AppFrameworkSDK.
//
//	Redistribution in source form is not permitted.
//

#import "LTCollectionViewScaleHelper.h"

@interface LTCollectionViewScaleHelper ()

@property (weak, nonatomic) UICollectionView* collectionView;

@property (nonatomic) int itemsPerLine;
@property (nonatomic) int scaleStep;
@property (nonatomic) int lastScale;
@property (nonatomic) CGSize itemSize;

@end


@implementation LTCollectionViewScaleHelper

- (id) initWithCollectionView:(UICollectionView*) view
{
	self = [super init];
	if (self)
	{
		_collectionView = view;
		
		_scaleStep = 37;		// defines the scale "speed".
		
		_itemsPerLine = [self getDefaultNumberOfHorizontalItems];
		_itemSize = [self currentItemSize];

		UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc]
												  initWithTarget:self
												  action:@selector(pinchGestureAction:)];
		
		[_collectionView addGestureRecognizer:pinchGesture];
	}
	return self;
}

- (void) updateLayout
{
	_itemSize = [self currentItemSize];
}

- (void) pinchGestureAction:(UIPinchGestureRecognizer *) pinch
{
	// Computation of scale is performed in percent
	
	if (pinch.state == UIGestureRecognizerStateBegan)
		_lastScale = 100;
	
	const int currentScale = pinch.scale * 100;
	const int lastScaleLevel = _lastScale / _scaleStep;
	const int currentScaleFactor = currentScale / _scaleStep;
	
	if (lastScaleLevel < currentScaleFactor
		&& _itemsPerLine > 2)
	{
		_itemsPerLine--;
		[self updateItemSizeAndReloadView];
	}
	else if (lastScaleLevel > currentScaleFactor
			 && _itemsPerLine < 8)
	{
		_itemsPerLine++;
		[self updateItemSizeAndReloadView];
	}
	
	_lastScale = currentScale;
}

- (void) updateItemSizeAndReloadView
{
	_itemSize = [self currentItemSize];
	[_collectionView reloadData];
}

- (int) getDefaultNumberOfHorizontalItems
{
	return 1;
}

- (CGSize) currentItemSize
{
	float width = _collectionView.frame.size.width;
	
	const float margin = width * 0.025;
	width = (width - margin) / _itemsPerLine;
	return CGSizeMake(width, _collectionView.frame.size.height-20);
}

@end
