//
//  pageCollectionViewCell.m
//  FoxitApp
//
//  Created by Apple on 16/12/1.
//
//

#import "pageCollectionViewCell.h"

@interface pageCollectionViewCell ()
@property (nonatomic, strong) UIImageView *pageImageView;
@property (nonatomic, strong) UIImageView *pageSelectIcon;
@property (nonatomic, strong) UILabel *indexLabel;
@end

@implementation pageCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
//        UIView *viewcon = [[UIView alloc] init];
//        self.contentView.isAccessibilityElement = NO;
//        [self.contentView addSubview:viewcon];
//        [viewcon mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.mas_equalTo(viewcon.superview);
//        }];
        
        UILabel *indexLabel = [[UILabel alloc] init];
        _indexLabel = indexLabel;
        indexLabel.textAlignment = NSTextAlignmentCenter;
        indexLabel.textColor = BarLikeBlackTextColor;
        indexLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:indexLabel];
        [indexLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(indexLabel.superview.mas_left);
            make.right.mas_equalTo(indexLabel.superview.mas_right);
            make.bottom.mas_equalTo(indexLabel.superview.mas_bottom);
            make.height.mas_equalTo(20);
        }];
        
        _pageImageView = [[UIImageView alloc] init];
        _pageImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_pageImageView];
        [_pageImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_pageImageView.superview.mas_left);
            make.right.mas_equalTo(_pageImageView.superview.mas_right);
            make.top.mas_equalTo(_pageImageView.superview.mas_top);
            make.bottom.mas_equalTo(indexLabel.mas_top);
        }];
        
        _pageSelectIcon = [[UIImageView alloc] init];
        _pageSelectIcon.image = ImageNamed(@"common_redio_selected");
        _pageSelectIcon.hidden = YES;
        [self.contentView addSubview:_pageSelectIcon];
        [_pageSelectIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_pageSelectIcon.superview.mas_left);
            make.top.mas_equalTo(_pageSelectIcon.superview.mas_top);
            make.height.mas_equalTo(26);
            make.width.mas_equalTo(26);
        }];
        
        _pageIndex = 0;
        _isSelected = NO;
        _isEditing = NO;
        self.contentView.isAccessibilityElement = YES;
    }
    return self;
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    if (_isSelected) {
        _pageSelectIcon.image = ImageNamed(@"common_redio_selected");
    }else{
        _pageSelectIcon.image = ImageNamed(@"common_redio_blank");
    }
}

- (void)setIsEditing:(BOOL)isEditing
{
    _isEditing = isEditing;
    if (_isEditing) {
        _pageSelectIcon.hidden = NO;
    }else{
        _pageSelectIcon.hidden = YES;
    }
}

- (void)setThumbImage:(UIImage *)thumbImage
{
    _pageImageView.image = thumbImage;
}

- (void)setPageIndex:(NSInteger)pageIndex
{
    _indexLabel.text = [NSString stringWithFormat:@"%ld",(long)pageIndex];
    _pageIndex = pageIndex;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.alpha = 0.5;
    }
    else {
        self.alpha = 1.f;
    }
}

@end
