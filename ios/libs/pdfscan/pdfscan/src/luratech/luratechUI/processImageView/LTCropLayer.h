#import <QuartzCore/QuartzCore.h>
#import "LTDetectionIndicationLayer.h"

@interface LTCropLayer : LTDetectionIndicationLayer

- (id) initWithFrame:(CGRect) rect;

- (void) update:(NSArray*) points;

@end
