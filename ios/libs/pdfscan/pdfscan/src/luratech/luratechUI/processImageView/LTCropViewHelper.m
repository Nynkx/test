//
//  LTCropViewHelper.m
//  PDF Scanner
//
//  Created by Patrick Busse on 29.12.14.
//  Copyright (c) 2014 LuraTech Imaging GmbH. All rights reserved.
//

#import "LTCropViewHelper.h"
#import "UIImageView+GeometryConversion.h"

@interface LTCropViewHelper ()
@property (weak) UIImageView* cropView;
@end


@implementation LTCropViewHelper

- (id) initViewCropView:(UIImageView*) cropView
{
	self = [super init];
	if (self)
	{
		_cropView = cropView;
	}
	return self;
}

- (NSMutableArray*) transformPointsToViewSpace:(NSArray*) points
{
	NSMutableArray* transformed = [NSMutableArray arrayWithCapacity:points.count];
	
	const int width = _cropView.image.size.width;
	const int height = _cropView.image.size.height;
	
	for (NSValue* value in points)
	{
		CGPoint p = point(value);
		p.x *= width;
		p.y *= height;
		p = [_cropView convertPointFromImage:p];
		
		[transformed addObject:[NSValue valueWithCGPoint:p]];
	}
	
	return transformed;
}

- (NSMutableArray*) transformPointsToImageSpace:(NSArray*) points
{
	NSMutableArray* transformed = [NSMutableArray arrayWithCapacity:points.count];
	
	const int width = _cropView.image.size.width;
	const int height = _cropView.image.size.height;
	
	for (int i = 0; i < 4; ++i)
	{
		CGPoint p = point(points[i]);
		
		p = [_cropView convertPointFromView:p];
		p.x /= width;
		p.y /= height;
		
		[transformed addObject:[NSValue valueWithCGPoint:p]];
	}
	
	return transformed;
}

- (NSMutableArray*) createPointsAtImageCorners
{
	return [NSMutableArray arrayWithObjects:
			[NSValue valueWithCGPoint:CGPointMake(0, 0)],
			[NSValue valueWithCGPoint:CGPointMake(1, 0)],
			[NSValue valueWithCGPoint:CGPointMake(1, 1)],
			[NSValue valueWithCGPoint:CGPointMake(0, 1)],
			nil];
}

+ (BOOL) rectConvexWhenReplacing:(int) i withPoint:(CGPoint) point inArray:(NSArray*) array
{
	NSMutableArray* a = [NSMutableArray arrayWithArray:array];
	[a replaceObjectAtIndex:i withObject:[NSValue valueWithCGPoint:point]];
	return [LTCropViewHelper isConvex:a];
}

- (BOOL) pointsCloseToBorder:(NSArray *)points
{
	for (NSValue* value in points)
	{
		CGPoint p = point(value);
		if (p.x < 0.1 || p.y < 0.05 || p.x > 0.9 || p.y < 0.1)
			return YES;
	}
	
	return NO;
}

/*! \brief Check whether the points in the array form a convex square. */
+ (BOOL) isConvex:(NSArray*) array
{
	unsigned long n = [array count];
	bool sign = false;
	
	for(int i = 0; i < n; i++)
	{
		CGPoint p0 = point(array[i]);
		CGPoint p1 = point(array[(i+1) % n]);
		CGPoint p2 = point(array[(i+2) % n]);
		
		CGPoint d1 = CGPointMake(p2.x - p1.x, p2.y - p1.y);
		CGPoint d2 = CGPointMake(p0.x - p1.x, p0.y - p1.y);
		
		float zcrossproduct = d1.x * d2.y - d1.y * d2.x;
		
		if (i == 0)
			sign = zcrossproduct > 0;
		else if (sign != (zcrossproduct > 0))
			return false;
	}
	
	return true;
}

+ (NSArray*) computeCenterPoints:(NSArray *)points minimumDistance:(CGFloat)distance
{
	const CGFloat distance2 = distance * distance;
	
	NSMutableArray* midPoints = [NSMutableArray arrayWithCapacity:4];
	
	for (int side = 0; side < 4; ++side)
	{
		CGPoint p1 = point(points[side]);
		CGPoint p2 = point(points[(side + 1) % 4]);
		CGPoint mid = midPoint(p1, p2);
		
		CGPoint p = (squaredDistance(p1, mid) > distance2) ? mid : CGPointMake(-1, -1);
		[midPoints addObject:[NSValue valueWithCGPoint:p]];
	}
	
	return midPoints;
}

+ (NSValue*)intersectionOfLineFrom:(CGPoint)p1 to:(CGPoint)p2 withLineFrom:(CGPoint)p3 to:(CGPoint)p4
{
	CGFloat d = (p1.x-p2.x) * (p3.y - p4.y) - ((p1.y-p2.y) * (p3.x - p4.x));
	if (d == 0.0)
		return nil;
	
	CGFloat ix = ((p1.x * p2.y - p1.y* p2.x)
				  * (p3.x - p4.x) - (p1.x- p2.x) * (p3.x* p4.y - p3.y* p4.x)) / d;
	
	CGFloat iy = ((p1.x * p2.y - p1.y * p2.x)
				  * (p3.y- p4.y) - (p1.y - p2.y) * (p3.x * p4.y - p3.y * p4.x)) / d;
	
	return [NSValue valueWithCGPoint:CGPointMake(ix, iy)];
}

@end
