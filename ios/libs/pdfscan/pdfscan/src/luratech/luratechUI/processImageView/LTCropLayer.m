#import "LTCropLayer.h"
#import "UIImageView+GeometryConversion.h"

#import "PointUtil.h"


@interface LTCropLayer()
{
	float m_circleRadius;
}
@property CAShapeLayer* shape;
@end


@implementation LTCropLayer

- (id) initWithFrame:(CGRect) rect
{
	self = [super initWithRect:rect];
	if (self != nil)
	{
		m_circleRadius = 16;

		_shape = [CAShapeLayer layer];
		_shape.strokeColor = [UIColor colorWithRed:0 green:122.0f/255 blue:1 alpha:1.0f].CGColor;
		_shape.fillColor = ThemeCellBackgroundColor.CGColor;

		_shape.lineWidth = 1;
		
		[self addSublayer:_shape];
	}
	
	return self;
}

- (void) update:(NSArray*) points
{
	NSArray* cornerPoints = [NSArray arrayWithObjects:points[0], points[1], points[2], points[3], nil];
	NSArray* handlePoints = [NSArray arrayWithObjects:points[4], points[5], points[6], points[7], nil];
	
	[super update:cornerPoints];
	[self drawPoints:cornerPoints andHandles: handlePoints];
}

- (void) drawPoints:(NSArray*) points andHandles:(NSArray*) handles
{
	CGMutablePathRef path = CGPathCreateMutable();
	[self addCornerPoints:points toPath:path];
	[self addHandles:handles forPoints:points toPath:path];
	_shape.path = path;
	CGPathRelease(path);
}

- (BOOL) validPoint:(CGPoint) p
{
	return p.x >= 0 && p.y >= 0;
}

- (void) addCornerPoints:(NSArray*) points toPath:(CGMutablePathRef) path
{
	CGPoint rectOriginOffset = CGPointMake(m_circleRadius * 0.5f, m_circleRadius * 0.5f);
	for (int i = 0; i < [points count]; ++i)
	{
		CGPoint p = point(points[i]);
		if ([self validPoint:p])
		{
			CGPoint origin = CGPointMake(p.x - rectOriginOffset.x, p.y - rectOriginOffset.y);
		
			CGRect rect = CGRectMake(origin.x, origin.y, m_circleRadius, m_circleRadius);
			CGPathAddEllipseInRect(path, nil, rect);
		}
	}
}

- (void) addHandles:(NSArray*) handles forPoints:(NSArray*) points toPath:(CGMutablePathRef) path
{
	const CGFloat rectWidth = 32;
	const CGFloat rectHeight = 8;
	
	for (int i = 0; i < [handles count]; ++i)
	{
		CGPoint p = point(handles[i]);
		if ([self validPoint:p])
		{
			// Construct rect at the coordinate origin (0,0); then apply affine transformation to position.
			CGRect r = CGRectMake(- rectWidth / 2,- rectHeight / 2, rectWidth, rectHeight);
			CGFloat angle = [self angleForHandle:i usingPoints:points];
		
			CGAffineTransform trans = CGAffineTransformMakeTranslation(p.x, p.y);
			CGAffineTransform M = CGAffineTransformRotate(trans, angle);
	
			CGPathAddRect(path, &M, r);
		}
	}
}

- (CGFloat) angleForHandle:(int)i usingPoints:(NSArray*) points
{
	const int i0 = i;
	const int i1 = (i + 1) % 4;
	
	CGPoint p0 = point(points[i0]);
	
	CGPoint p1 = point(points[i1]);
	p1.x -= p0.x;
	p1.y -= p0.y;
	
	CGPoint line = deltaVector(CGPointMake(0, 0), p1);
	return atan2(line.y, line.x);
}

@end
