//
//  MagnifierView.m
//  SimplerMaskTest
//

#import "LTMagnifierView.h"
#import <QuartzCore/QuartzCore.h>
#define WIDTH       80
#define HEIGHT      80
#define RADIUS      40

@implementation LTMagnifierView
@synthesize LTviewToMagnify, LTtouchPoint;

- (id)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)]) {
		// make the circle-shape outline with a nice border.
		self.layer.borderColor = [[UIColor colorWithRed:0 green:122.0f/255 blue:1 alpha:1.0f] CGColor];
		self.layer.borderWidth = 1;
		self.layer.cornerRadius = RADIUS;
		self.layer.masksToBounds = YES;
		//self.frameRect = CGRectMake(0, 0, 10, 10);
		
		[self createCrosshair];
	}
	return self;
}

- (void) createCrosshair
{
	CAShapeLayer* cross = [[CAShapeLayer alloc] init];
	cross.strokeColor = [[UIColor colorWithRed:0 green:122.0f/255 blue:1 alpha:1.0f] CGColor];
	cross.fillColor = [[UIColor clearColor] CGColor];
	[self.layer addSublayer:cross];
	
	CGMutablePathRef path = CGPathCreateMutable();
	CGRect layerFrame = self.layer.frame;
	int w = layerFrame.size.width;
	int h = layerFrame.size.height;
	
	CGPathMoveToPoint(path, nil, w / 2, 0);
	CGPathAddLineToPoint(path, nil, w / 2, h);
	
	CGPathMoveToPoint(path, nil, 0, h / 2);
	CGPathAddLineToPoint(path, nil, w, h / 2);
	
	CGPathAddEllipseInRect(path, nil, CGRectInset(layerFrame, w / 3, h / 3));
	
	cross.path = path;
	
	CGPathRelease(path);
}

- (void)setTouchPoint:(CGPoint)pt {
	LTtouchPoint = pt;
	CGFloat middleX = self.LTframeRect.size.width / 2;
	//show on left top side
	if (pt.x > middleX) self.center = CGPointMake(RADIUS, RADIUS);
	//show on right top side
	else self.center = CGPointMake(self.LTframeRect.size.width - RADIUS, RADIUS);
}

- (void) setFrameRect:(CGRect)frameRect
{
	// frameRect and viewToMagnify.frame must be equal
	_LTframeRect = frameRect;
	self.LTviewToMagnify.frame = frameRect;
}

- (void)drawRect:(CGRect)rect {
	// here we're just doing some transforms on the view we're magnifying,
	// and rendering that view directly into this view,
	// rather than the previous method of copying an image.
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextClearRect(context, [[UIScreen mainScreen] bounds]);
	CGContextTranslateCTM(context, 1*(self.frame.size.width*0.5), 1*(self.frame.size.height*0.5));
	CGContextScaleCTM(context, 1.5, 1.5);
	CGContextTranslateCTM(context,-1*(LTtouchPoint.x),-1*(LTtouchPoint.y));
	[self.LTviewToMagnify.layer renderInContext:context];
}

@end
