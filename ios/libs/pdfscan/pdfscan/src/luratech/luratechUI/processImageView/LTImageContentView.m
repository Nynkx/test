//
//  LTImageContentView.m
//  FoxitApp
//
//  Created by Apple on 16/12/2.
//
//

#import "LTImageContentView.h"

@interface LTImageContentView ()

@end

@implementation LTImageContentView

- (instancetype)init
{
    if (self = [super init]) {
        
        return self;
    }
    return self;
}

- (void)addImageView
{
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.editImageView = imageView;
    [self addSubview:imageView];
    [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20);
        make.height.mas_equalTo([NSObject fs_getForegroundActiveWindow].bounds.size.height - 74);
    }];
}

- (void)refreshImage
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    if (IS_IPHONE_OVER_TEN) {
        if (@available(iOS 11.0, *)) {
            insets = [NSObject fs_getForegroundActiveWindow].safeAreaInsets;
        } 
    }
    [self.editImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20 - insets.left - insets.right);
        make.height.mas_equalTo([NSObject fs_getForegroundActiveWindow].bounds.size.height - 74 - insets.bottom - insets.top);
    }];
    //[self layoutSubviews];
    self.editImageView.image = _editImage;
}

- (void)setEditImage:(UIImage *)editImage
{
    _editImage = editImage;
    self.editImageView.image = editImage;
}

- (void)safeAreaInsetsDidChange
{
    //if (IS_IPHONE_OVER_TEN) {
        UIEdgeInsets insets = [NSObject fs_getForegroundActiveWindow].safeAreaInsets;
        [_editImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
            make.width.mas_equalTo([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20 - insets.left - insets.right);
            make.height.mas_equalTo([NSObject fs_getForegroundActiveWindow].bounds.size.height - 74 );
        }];
    //}
}

//- (void)updateCurrentConstraints
//{
//    
//}


@end
