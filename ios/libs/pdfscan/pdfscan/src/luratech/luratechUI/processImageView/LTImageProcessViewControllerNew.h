//
//  LTImageProcessViewControllerNew.h
//  FoxitApp
//
//  Created by Apple on 16/12/2.
//
//

#import <UIKit/UIKit.h>
#import "LTImageProcessViewControllerNew.h"

@class LTImageProcessViewControllerNew;
@protocol LTImageProcessViewControllerDelegate<NSObject>

- (void) processImageView:(LTImageProcessViewControllerNew*) ctrl
      didFinishWithResult:(UIImage*) image;

@end


@interface LTImageProcessViewControllerNew : UIViewController
//@property (nonatomic, strong) UIImage *editImage;
@property (nonatomic, weak) id<LTImageProcessViewControllerDelegate> delegate;

- (id) initWithImage:(UIImage*) image detectionResult:(LTDetectionResult*) result;
@end
