//
//  LTModeList.m
//  FoxitApp
//
//  Created by Apple on 16/12/6.
//
//

@interface LTModeListHearderView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *flexibleBtn;
@property (nonatomic, copy)   void(^flexibleBtnClick)();
@end

@implementation LTModeListHearderView

- (instancetype)init
{
    if (self = [super init]) {
        UILabel *title = [[UILabel alloc] init];
        title.font = [UIFont systemFontOfSize:15];
        title.textColor = [UIColor whiteColor];
        self.titleLabel = title;
        [self addSubview:title];
        [title mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(title.superview.mas_centerX).multipliedBy(DEVICE_iPHONE? 0.001 : 0.66).offset(DEVICE_iPHONE ? 13 : 0);
            make.centerY.mas_equalTo(title.superview.mas_centerY);
        }];
        
        UIButton *flexibleBtn = [[UIButton alloc] init];
        [flexibleBtn setImage:ImageNamed(@"imageProcessListClose") forState:UIControlStateNormal];
        [flexibleBtn setImage:ImageNamed(@"imageProcessListOpen") forState:UIControlStateSelected];
        [flexibleBtn addTarget:self action:@selector(flexibleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.flexibleBtn = flexibleBtn;
        [self addSubview:flexibleBtn];
        [flexibleBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(flexibleBtn.superview.mas_centerX).multipliedBy(DEVICE_iPHONE? 2 : 1.33).offset(DEVICE_iPHONE ? -13 : 0);
            make.centerY.mas_equalTo(flexibleBtn.superview.mas_centerY);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(40);
        }];
        
        UIView *divideView = [[UIView alloc] init];
        divideView.backgroundColor = DividingLineColor;
        divideView.alpha = 0.6;
        [self addSubview:divideView];
        [divideView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(divideView.superview.mas_centerX);
            make.height.mas_equalTo([Utility realPX:1.0]);
            make.bottom.mas_equalTo(divideView.superview.mas_bottom);
            make.width.mas_equalTo(divideView.superview.mas_width).multipliedBy(DEVICE_iPHONE ? 0.99 : 0.33);
        }];
        return self;
    }
    return nil;
}

- (void)flexibleBtnClick:(UIButton *)btn
{
    if (self.flexibleBtnClick) {
        self.flexibleBtnClick();
    }
}

- (void)safeAreaInsetsDidChange
{
    UIEdgeInsets edget = UIEdgeInsetsZero;
    if (IS_IPHONE_OVER_TEN) {
        edget = self.safeAreaInsets;
        
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleLabel.superview.mas_centerX).multipliedBy(DEVICE_iPHONE? 0.001 : 0.66).offset(DEVICE_iPHONE ? 13 + edget.left : 0);
            make.centerY.mas_equalTo(self.titleLabel.superview.mas_centerY);
        }];
        
        [self.flexibleBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.flexibleBtn.superview.mas_centerX).multipliedBy(DEVICE_iPHONE? 2 : 1.33).offset(DEVICE_iPHONE ? -13 - edget.right: 0);
            make.centerY.mas_equalTo(self.flexibleBtn.superview.mas_centerY);
            make.width.mas_equalTo(24);
            make.height.mas_equalTo(24);
        }];
    }
}

@end

typedef NS_ENUM(NSInteger, LTModeListCellType) {
    LTModeListCellTypeTitle = 0,
    LTModeListCellTypeSlider,
};

@class LTModeListCell;
@protocol LTModeListCellSilderValueChange <NSObject>
- (void)LTModeListCellSilderValueChange:(LTModeListCell *)cell sliderView:(UISlider *)slider;
@end

@interface LTModeListCell : UITableViewCell
@property (nonatomic, assign) LTModeListCellType type;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UISlider *sliderView;
@property (nonatomic, strong) UIImageView *selectIcon;
@property (nonatomic, weak) id<LTModeListCellSilderValueChange> delegate;
- (instancetype)initWithType:(LTModeListCellType)type;
- (void)setTitleLableText:(NSString *)text;
- (void)setSliderVlaue:(float)vlaue;
@end

@implementation LTModeListCell

- (instancetype)initWithType:(LTModeListCellType)type
{
    if (self = [super init]) {
        self.backgroundColor = [UIColor blackColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.alpha = 0.6;
        _type = type;
        if (type == LTModeListCellTypeTitle) {
            UILabel *titleLabel = [[UILabel alloc] init];
            _titleLabel = titleLabel;
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.font = [UIFont systemFontOfSize:14];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [self.contentView addSubview:titleLabel];
            [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(titleLabel.superview.mas_centerX).multipliedBy(DEVICE_iPHONE ? 0.001 : 0.66).offset(DEVICE_iPHONE ? 23 : 10);
                make.centerY.mas_equalTo(titleLabel.superview.mas_centerY);
            }];
            
            UIImageView *selectIcon = [[UIImageView alloc] init];
            _selectIcon = selectIcon;
            selectIcon.image = ImageNamed(@"select_gray");
            [self.contentView  addSubview:selectIcon];
            selectIcon.hidden = YES;
            [selectIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(selectIcon.superview.mas_centerX).multipliedBy(DEVICE_iPHONE ? 2 : 1.33).offset(DEVICE_iPHONE ? -13 : 0);
                make.centerY.mas_equalTo(selectIcon.superview.mas_centerY);
                make.width.height.mas_equalTo(26);
            }];
            
        }else if (type == LTModeListCellTypeSlider) {
            UISlider *slier = [[UISlider alloc] init];
            slier.minimumTrackTintColor = [UIColor colorWithRed:17/255.0 green:156/255.0 blue:216/255.0 alpha:1.0];
            slier.maximumTrackTintColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
            
            slier.minimumValue = -4;
            slier.maximumValue = 5;
            _sliderView = slier;
            [_sliderView setThumbImage:ImageNamed(@"sliderThumb") forState:UIControlStateNormal];
            [_sliderView addTarget:self action:@selector(sliderChange:) forControlEvents:UIControlEventValueChanged];
            [self.contentView addSubview:slier];
            [slier mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(slier.superview.mas_centerY);
                make.centerX.mas_equalTo(slier.superview.mas_centerX);
                make.width.mas_equalTo(slier.superview.mas_width).multipliedBy(DEVICE_iPHONE ? 0.8 : 0.32);
                make.height.mas_equalTo(44);
            }];
        }
    }
    return self;
}

- (void)sliderChange:(id) sender {
    if ([sender isKindOfClass:[UISlider class]]) {
        UISlider * slider = sender;
        if ([self.delegate respondsToSelector:@selector(LTModeListCellSilderValueChange:sliderView:)]) {
            [self.delegate LTModeListCellSilderValueChange:self sliderView:slider];
        }
    }
}

- (void)setTitleLableText:(NSString *)text
{
    if (_type == LTModeListCellTypeTitle) {
        _titleLabel.text = text;
    }
}

- (void)setSliderVlaue:(float)vlaue
{
    if (_type == LTModeListCellTypeSlider) {
        _sliderView.value = vlaue;
    }
}

@end

#import "LTModeList.h"

@interface LTModeList ()<UITableViewDelegate,UITableViewDataSource,LTModeListCellSilderValueChange>
@property (nonatomic, strong) NSMutableArray *listFlexibleStatus;
@end

@implementation LTModeList

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    if (self = [super initWithFrame:frame style:style]) {
        self.backgroundColor = [UIColor blackColor];
        self.alpha = 0.6;
        self.delegate = self;
        self.dataSource = self;
        self.sectionHeaderHeight = 60;
        self.rowHeight = 44;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listFlexibleStatus = [NSMutableArray arrayWithObjects:@0,@0,@0,nil];
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    float height = tableView.frame.size.height;
    if (section == 1) {
        LTModeListHearderView *colorHearder = [[LTModeListHearderView alloc] init];
        colorHearder.flexibleBtnClick = ^(){
            if ([self->_listFlexibleStatus[section - 1] integerValue]) {
                self->_listFlexibleStatus[section - 1] = @0;
                [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    if (!DEVICE_iPHONE || !UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                        make.height.mas_equalTo(height - 176);
                    }
                }];
            }else{
                self->_listFlexibleStatus[section - 1] = @1;
                [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    if (!DEVICE_iPHONE || !UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                        make.height.mas_equalTo(height + 176);
                    }
                }];
            }
            [tableView reloadData];
        };
        colorHearder.titleLabel.text = FSLocalizedForKey(@"kColor");
        
        if ([_listFlexibleStatus[section - 1] integerValue]) {
            colorHearder.flexibleBtn.selected = YES;
        }else{
            colorHearder.flexibleBtn.selected = NO;
        }
        return colorHearder;
    }else if (section == 2){
        LTModeListHearderView *brightnessHearder = [[LTModeListHearderView alloc] init];
        brightnessHearder.flexibleBtnClick = ^(){
            if ([self->_listFlexibleStatus[section - 1] integerValue]) {
                self->_listFlexibleStatus[section - 1] = @0;
                [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height - 44);
                }];
            }else{
                self->_listFlexibleStatus[section - 1] = @1;
                [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height + 44);
                }];
            }
            [tableView reloadData];
        };
        brightnessHearder.titleLabel.text = FSLocalizedForKey(@"kBrightness");
        
        if ([_listFlexibleStatus[section - 1] integerValue]) {
            brightnessHearder.flexibleBtn.selected = YES;
        }else{
            brightnessHearder.flexibleBtn.selected = NO;
        }
        return brightnessHearder;
    }else if (section == 3){
        LTModeListHearderView *contrastHearder = [[LTModeListHearderView alloc] init];
        contrastHearder.flexibleBtnClick = ^(){
            if ([self->_listFlexibleStatus[section - 1] integerValue]) {
                self->_listFlexibleStatus[section - 1] = @0;
                [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height - 44);
                }];
            }else{
                self->_listFlexibleStatus[section - 1] = @1;
                [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height + 44);
                }];
            }
            [tableView reloadData];
        };
        contrastHearder.titleLabel.text = FSLocalizedForKey(@"kContrast");
        
        if ([_listFlexibleStatus[section - 1] integerValue]) {
            contrastHearder.flexibleBtn.selected = YES;
        }else{
            contrastHearder.flexibleBtn.selected = NO;
        }
        return contrastHearder;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        if ([_listFlexibleStatus[section - 1] integerValue]) {
            return 4;
        }
        return 0;
    }else if (section == 2){
        if ([_listFlexibleStatus[section - 1] integerValue]) {
            return 1;
        }
        return 0;
    }else if (section == 3){
        if ([_listFlexibleStatus[section - 1] integerValue]) {
            return 1;
        }
        return 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LTColorMode colorMode = _imageProcessor.colorMode;
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            LTModeListCell *modeListCell = [[LTModeListCell alloc] initWithType:LTModeListCellTypeTitle];
            [modeListCell setTitleLableText:FSLocalizedForKey(@"kColorModelNone")];
            if (colorMode == LTColorModeRGB) {
                modeListCell.selectIcon.hidden = NO;
            }else{
                modeListCell.selectIcon.hidden = YES;
            }
            return modeListCell;
        }else if (indexPath.row == 1){
            LTModeListCell *modeListCell = [[LTModeListCell alloc] initWithType:LTModeListCellTypeTitle];
            [modeListCell setTitleLableText:FSLocalizedForKey(@"kColor")];
            if (colorMode == LTColorModeRGBWhiteBalance) {
                modeListCell.selectIcon.hidden = NO;
            }else{
                modeListCell.selectIcon.hidden = YES;
            }
            return modeListCell;
        }else if (indexPath.row == 2){
            LTModeListCell *modeListCell = [[LTModeListCell alloc] initWithType:LTModeListCellTypeTitle];
            [modeListCell setTitleLableText:FSLocalizedForKey(@"kColorModelBlackWhite")];
            if (colorMode == LTColorModeBitonal) {
                modeListCell.selectIcon.hidden = NO;
            }else{
                modeListCell.selectIcon.hidden = YES;
            }
            return modeListCell;
        }else if (indexPath.row == 3){
            LTModeListCell *modeListCell = [[LTModeListCell alloc] initWithType:LTModeListCellTypeTitle];
            [modeListCell setTitleLableText:FSLocalizedForKey(@"kColorModeGray")];
            if (colorMode == LTColorModeGrayscale) {
                modeListCell.selectIcon.hidden = NO;
            }else{
                modeListCell.selectIcon.hidden = YES;
            }
            return modeListCell;
        }
    }else if (indexPath.section == 2)
    {
        if (indexPath.row == 0) {
            LTModeListCell *modeListCell = [[LTModeListCell alloc] initWithType:LTModeListCellTypeSlider];
            modeListCell.delegate = self;
            [modeListCell setSliderVlaue:_imageProcessor.brightnessLevel];
            return modeListCell;
        }
    }else if (indexPath.section == 3)
    {
        if (indexPath.row == 0) {
            LTModeListCell *modeListCell = [[LTModeListCell alloc] initWithType:LTModeListCellTypeSlider];
            modeListCell.delegate = self;
            [modeListCell setSliderVlaue:_imageProcessor.contrastLevel];
            return modeListCell;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [_imageProcessor setColorMode:LTColorModeRGB];
        }else if (indexPath.row == 1){
            [_imageProcessor setColorMode:LTColorModeRGBWhiteBalance];
        }else if (indexPath.row == 2){
            [_imageProcessor setColorMode:LTColorModeBitonal];
        }else if (indexPath.row == 3){
            [_imageProcessor setColorMode:LTColorModeGrayscale];
        }
        [self.superview fs_showHUDLoading:nil];
        [_imageProcessor computePreview];
        [self reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return 60;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    float height = self.frame.size.height;
    if ([_listFlexibleStatus[0] integerValue]) {
        if (DEVICE_iPHONE) {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && (fromInterfaceOrientation == UIInterfaceOrientationPortrait || fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)) {
                [self mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height - 176);
                }];
            }else if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) && (fromInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || fromInterfaceOrientation == UIInterfaceOrientationLandscapeRight)){
                [self mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height + 176);
                }];
            }
        }
    }
}

- (void)LTModeListCellSilderValueChange:(LTModeListCell *)cell sliderView:(UISlider *)slider
{
    NSIndexPath *indexPath = [self indexPathForCell:cell];
    if (indexPath.section == 2) {
        [_imageProcessor setBrightnessLevel:slider.value];
    }else if (indexPath.section == 3){
        [_imageProcessor setContrastLevel:slider.value];
    }
    [_imageProcessor computePreview];
}
@end
