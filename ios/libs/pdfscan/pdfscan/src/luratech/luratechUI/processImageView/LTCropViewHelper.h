//
//  LTCropViewHelper.h
//  PDF Scanner
//
//  Created by Patrick Busse on 29.12.14.
//  Copyright (c) 2014 LuraTech Imaging GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PointUtil.h"

@interface LTCropViewHelper : NSObject

- (id) initViewCropView:(UIImageView*) cropView;

- (NSMutableArray*) transformPointsToViewSpace:(NSArray*) points;
- (NSMutableArray*) transformPointsToImageSpace:(NSArray*) points;
- (NSMutableArray*) createPointsAtImageCorners;

+ (BOOL) rectConvexWhenReplacing:(int) i withPoint:(CGPoint) point inArray:(NSArray*) array;
- (BOOL) pointsCloseToBorder:(NSArray*) points;

/*! \brief Check whether the points in the array form a convex square. */
+ (BOOL) isConvex:(NSArray*) array;

/*! \brief Compute the center points for points forming a rectangle. 
	
	The returned points fullfil the constraint the they are at least distance away from the line
	endpoints. For sides not fullfilling the constraint nil is inserted.
 */
+ (NSArray*) computeCenterPoints:(NSArray*) points minimumDistance:(CGFloat) distance;

+ (NSValue*)intersectionOfLineFrom:(CGPoint)p1 to:(CGPoint)p2 withLineFrom:(CGPoint)p3 to:(CGPoint)p4;

@end
