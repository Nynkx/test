//
//  MagnifierView.h
//  SimplerMaskTest
//

#import <UIKit/UIKit.h>

@interface LTMagnifierView : UIView {
	UIView *LTviewToMagnify;
	CGPoint LTtouchPoint;
}

@property (nonatomic, retain) UIView *LTviewToMagnify;
@property (nonatomic) CGPoint LTtouchPoint;
@property (nonatomic) CGRect LTframeRect;

@end
