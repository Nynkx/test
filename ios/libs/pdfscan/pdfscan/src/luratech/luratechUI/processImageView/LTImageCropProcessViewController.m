//
//  LTImageCropProcessViewController.m
//  FoxitApp
//
//  Created by Apple on 16/12/14.
//
//

#import "LTImageCropProcessViewController.h"
#import "LTMagnifierView.h"
#import "LTCropLayer.h"
#import "LTCropViewHelper.h"
#import "UIImageView+GeometryConversion.h"

/*! Check whether a line direction was flipped. */
static inline bool normalsFlipped(CGPoint oldCorner, CGPoint newCorner, CGPoint reference)
{
    CGPoint oldLineVector = normalize(deltaVector(oldCorner, reference));
    CGPoint newLineVector = normalize(deltaVector(newCorner, reference));
    
    bool flipped = fabs(oldLineVector.x - newLineVector.x) > 0.1f
    || fabs(oldLineVector.y - newLineVector.y) > 0.1f;
    
    return flipped;
}

@interface LTImageCropProcessViewController ()
{
    int _selectedPoint;
    bool _didMovePoint;
}
@property (nonatomic, strong) LTImageProcessor* imageProcessor;
@property (nonatomic, assign) BOOL *isCroping;

@property NSMutableArray* points;
@property UIImage* imageCache;
@property (nonatomic, strong) LTMagnifierView *loupe;
@property LTCropLayer* cropLayer;
@property (nonatomic) LTCropViewHelper* helper;
@property (nonatomic, strong) UIView *bottomBar;
@property (nonatomic, strong) UIImageView *cropView;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *sureBtn;
@property (nonatomic, strong) UIButton *resetBtn;
@end

@implementation LTImageCropProcessViewController

- (id) initWithImage:(UIImage*) image andPoints:(NSArray*) points
{
    if (self = [super init])
    {
        _didMovePoint = false;
        _selectedPoint = -1;
        _points = [NSMutableArray arrayWithArray:points];
        _imageCache = image;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initSubView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.cropView.frame = CGRectMake(0, 0, APPDELEGATE.window.bounds.size.width, APPDELEGATE.window.bounds.size.height - 49);
    [self.view layoutSubviews];
    _helper = [[LTCropViewHelper alloc] initViewCropView:_cropView];
    
    [self initView];
    [self initGestureRecognizer];
    [self createLoupe];
    //[self enableApplyButton:NO];
}

- (void)initSubView
{
    self.view.backgroundColor = UIColorHex(0X0C0C0C);
    _bottomBar = [[UIView alloc] init];
    _bottomBar.backgroundColor = UIColorHex(0x1F1F1F);
    [self.view addSubview:_bottomBar];
    [_bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_bottomBar.superview.mas_left);
        make.right.mas_equalTo(_bottomBar.superview.mas_right);
        make.bottom.mas_equalTo(_bottomBar.superview.mas_bottom);
        make.height.mas_equalTo(49);
    }];
    
    _cropView = [[UIImageView alloc] init];
    _cropView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:_cropView];
    [_cropView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_cropView.superview.mas_left).offset(10);
        make.right.mas_equalTo(_cropView.superview.mas_right).offset(-10);
        make.top.mas_equalTo(_cropView.superview.mas_top).offset(25);
        make.bottom.mas_equalTo(_bottomBar.mas_top).offset(-5);
    }];
    
    UIButton *cancelBtn = [[UIButton alloc] init];
    _cancelBtn = cancelBtn;
    _cancelBtn.accessibilityLabel = FSLocalizedForKey(@"kClose");
    [cancelBtn setImage:ImageNamed(@"guanbi") forState:UIControlStateNormal];
    [cancelBtn setImage:[Utility unableImage:ImageNamed(@"guanbi")] forState:UIControlStateHighlighted];
    
    [cancelBtn addTarget:self action:@selector(cancelBtnclick:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBar addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cancelBtn.superview.mas_centerY);
        make.left.mas_equalTo(cancelBtn.superview.mas_left).offset(14);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *sureBtn = [[UIButton alloc] init];
    _sureBtn = sureBtn;
    _sureBtn.accessibilityLabel = FSLocalizedForKey(@"kDone");
    [sureBtn setImage:ImageNamed(@"imageProcessCropSave") forState:UIControlStateNormal];
    [sureBtn setImage:[Utility unableImage:ImageNamed(@"imageProcessCropSave")] forState:UIControlStateHighlighted];
    
    [sureBtn addTarget:self action:@selector(sureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBar addSubview:sureBtn];
    [sureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(sureBtn.superview.mas_centerY);
        make.right.mas_equalTo(sureBtn.superview.mas_right).offset(-14);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *resetBtn = [[UIButton alloc] init];
    _resetBtn = resetBtn;
    [resetBtn setTitle:FSLocalizedForKey(@"kReset") forState:UIControlStateNormal];
    resetBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    CGSize size = [Utility getTextSize:FSLocalizedForKey(@"kReset") fontSize:15 maxSize:CGSizeMake(150, 44)];
    [resetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resetBtn setTitleColor:NormalBarBackgroundColor forState:UIControlStateHighlighted];
    [resetBtn addTarget:self action:@selector(resetBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBar addSubview:resetBtn];
    [resetBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(resetBtn.superview.mas_centerY);
        make.centerX.mas_equalTo(resetBtn.superview.mas_centerX);
        make.width.mas_equalTo(size.width);
        make.height.mas_equalTo(49);
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_cropLayer == nil)
    {
        // Select all when no detection points set.
        if ([_points count] != 4)
            _points = [_helper createPointsAtImageCorners];
        
        // Create a small inset on the image view, so that points around the border can be grabbed.
        //if ([_helper pointsCloseToBorder:_points])
            //_cropView.frame = CGRectInset(_cropView.frame, 25, 25);
        
        _loupe.LTframeRect = _cropView.frame;
        
        _cropLayer = [[LTCropLayer alloc] initWithFrame:_cropView.frame];
        [_cropView.layer addSublayer:_cropLayer];
        
        _points = [_helper transformPointsToViewSpace:_points];
        
        for (int i = 0; i < 4; ++i)
            [_points addObject:[NSValue valueWithCGPoint:CGPointMake(-1, -1)]];
        
        [self updateHandles];
        [self drawPoints];
    }

}

- (void)viewSafeAreaInsetsDidChange
{
    UIEdgeInsets edget = UIEdgeInsetsZero;
    //if (Device_iPhoneX) {
        edget = self.view.safeAreaInsets;
        [_cropView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cropView.superview.mas_left).offset(10 + edget.left);
            make.right.mas_equalTo(_cropView.superview.mas_right).offset(-10 - edget.right);
            make.top.mas_equalTo(_cropView.superview.mas_top).offset(25 + edget.top);
            make.bottom.mas_equalTo(_bottomBar.mas_top).offset(-5);
        }];
        
        [_bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_bottomBar.superview.mas_left);
            make.right.mas_equalTo(_bottomBar.superview.mas_right);
            make.bottom.mas_equalTo(_bottomBar.superview.mas_bottom);
            make.height.mas_equalTo(49 + edget.bottom);
        }];
        
        [_cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_cancelBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.left.mas_equalTo(_cancelBtn.superview.mas_left).offset(14 + edget.left);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(40);
        }];
        
        [_sureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_sureBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.right.mas_equalTo(_sureBtn.superview.mas_right).offset(-14 - edget.right);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(40);
        }];
        
        CGSize size = [Utility getTextSize:FSLocalizedForKey(@"kReset") fontSize:15 maxSize:CGSizeMake(150, 44)];
        [_resetBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_resetBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.centerX.mas_equalTo(_resetBtn.superview.mas_centerX);
            make.width.mas_equalTo(size.width);
            make.height.mas_equalTo(49);
        }];
    //}
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _imageCache = nil;
    _cropView.image = nil;
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_cropLayer setFrame:self.cropView.bounds];
    self.loupe.LTframeRect = self.cropView.frame;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Initialization

- (void) initView
{
    if (_imageCache != nil)
    {
        _cropView.image = _imageCache;
        _imageCache = nil;
    }
}

- (void) createLoupe
{
    // Create the loupe and add it as subview once.
    // Steadily adding and removing causes layoutSubviews to be called constantly.
    // This restores the 'old' layout, discarding an inset for cases where there
    // is no valid detection.
    
    //show real image as layer
    UIImageView *shownImage = [[UIImageView alloc] initWithFrame:self.cropView.frame];
    [shownImage setContentMode:self.cropView.contentMode];
    [shownImage setImage:self.cropView.image];
    
    
    self.loupe = [[LTMagnifierView alloc] init];
    self.loupe.LTframeRect = self.cropView.frame;
    self.loupe.LTviewToMagnify = shownImage;
    
    [self.cropView addSubview:self.loupe];
    self.loupe.layer.zPosition = MAXFLOAT;
    
    _loupe.hidden = YES;
}

- (void) initGestureRecognizer
{
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(panGesture:)];
    pan.maximumNumberOfTouches = 1;
    
    // Add to parent view instead of crop view, so we can pan over the image borders.
    [self.view addGestureRecognizer:pan];
}


- (void)cancelBtnclick:(UIButton *)btn
{
    if ([self.delegate respondsToSelector:@selector(cancelCrop)]) {
        [_delegate cancelCrop];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)sureBtnClick:(UIButton *)btn
{
    NSArray* transformed = [_helper transformPointsToImageSpace:_points];
    if ([self.delegate respondsToSelector:@selector(cropViewChangedDetection:imageWidth:imageHeight:)]) {
        [_delegate cropViewChangedDetection:transformed imageWidth:1 imageHeight:1];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    //TODO: imageprocess
}

- (void)resetBtnClick:(UIButton *)btn
{
    NSLog(@"reset crop");
    [_points removeAllObjects];
    [_cropLayer removeFromSuperlayer];
    _cropLayer = nil;
    [self viewDidAppear:YES];
}

#pragma mark - UI Event handling

//- (void) pressedApply:(id) sender
//{
//    //	if (_didMovePoint)
//    //	{
//    NSArray* transformed = [_helper transformPointsToImageSpace:_points];
//    //[_delegate cropViewChangedDetection:transformed imageWidth:1 imageHeight:1];
//    //	}
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

//called automaticly, without adiing as gestureRecognizer
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_loupe.hidden && touches.count == 1)
    {
        NSArray *touchArray = [touches allObjects];
        UITouch *currentTouch = touchArray[0];
        CGPoint touchedPoint = [currentTouch locationInView:self.cropView];
        
        //check if "crop point" is not selected, then do noting
        int pIndex = [self indexOfPointAt:touchedPoint];
        if ( pIndex != -1)
            [self showLoupeAtPoint:point(_points[pIndex])];
    }
}

//remove loupe when other gesture ends
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    _loupe.hidden = YES;
}

- (void) panGesture:(UIPanGestureRecognizer *)gesture
{
    CGPoint position = [gesture locationInView:_cropView];
    
    if (gesture.state == UIGestureRecognizerStateBegan)
        _selectedPoint = [self indexOfPointAt:position];
    else if (gesture.state == UIGestureRecognizerStateEnded) {
        _loupe.hidden = YES;
        _selectedPoint = -1;
    }
    
    [self movePoint:position];
}


#pragma mark - Point mapping

- (void) movePoint:(CGPoint) p
{
    if ([self isInsideView:p] && _selectedPoint != -1)
    {
        if ([self cornerPointSelected])
            [self moveCornerPointTo:p];
        else if ([self handlePointSelected])
            [self moveHandlePointTo:p];
        
        [self updateHandles];
        [self drawPoints];
        [self showLoupeAtPoint:point(_points[_selectedPoint])];
        
        _didMovePoint = YES;
        //[self enableApplyButton:YES];
    }
}

- (BOOL) cornerPointSelected
{
    return _selectedPoint >= 0 && _selectedPoint < 4;
}

- (BOOL) handlePointSelected
{
    return _selectedPoint >=4 && _selectedPoint < 8;
}

- (NSArray*) corners
{
    return [NSArray arrayWithObjects:_points[0], _points[1], _points[2], _points[3], nil];
}

- (void) moveCornerPointTo:(CGPoint) p
{
    
    if ([LTCropViewHelper rectConvexWhenReplacing:_selectedPoint withPoint:p inArray:[self corners]])
    {
        [_points replaceObjectAtIndex:_selectedPoint
                           withObject:[NSValue valueWithCGPoint:p]];
    }
}


- (void) moveHandlePointTo:(CGPoint) p
{
    // Compute indices used and get points
    assert(_selectedPoint >= 4 && _selectedPoint < 8);
    const int corner1Index = _selectedPoint - 4;
    const int corner2Index = (_selectedPoint - 3) % 4;
    const int corner0Index = (corner1Index + 3) % 4;
    const int corner4Index = (corner2Index + 1) % 4;
    
    CGPoint corner0 = point(_points[corner0Index]);
    CGPoint corner1 = point(_points[corner1Index]);
    CGPoint corner2 = point(_points[corner2Index]);
    CGPoint corner3 = point(_points[corner4Index]);
    
    // Compute new position of the handle
    CGPoint line = deltaVector(corner1, corner2);
    
    CGPoint normal = absolute(computeNormal(line));
    
    CGPoint oldHandlePoint = point(_points[_selectedPoint]);
    
    CGPoint d = deltaVector(oldHandlePoint, p);
    
    CGPoint pointOffset = CGPointMake(normal.x * d.x, normal.y * d.y);
    
    CGPoint newHandlePoint = CGPointMake(oldHandlePoint.x + pointOffset.x,
                                         oldHandlePoint.y + pointOffset.y);
    
    CGPoint movedLineEnd = CGPointMake(newHandlePoint.x + line.x, newHandlePoint.y + line.y);
    
    // Compute new points for corner1 and 2
    NSValue* newCorner1 = [LTCropViewHelper intersectionOfLineFrom:newHandlePoint to:movedLineEnd
                                                      withLineFrom:corner0 to:corner1];
    
    NSValue* newCorner2 = [LTCropViewHelper intersectionOfLineFrom:newHandlePoint to:movedLineEnd
                                                      withLineFrom:corner3 to:corner2];
    
    // Validate points and update when valid
    if (newCorner1 != nil && newCorner2 != nil)
    {
        const CGFloat minLength = 50 * 50;
        
        bool newPointsInView = [self isInsideView:point(newCorner1)] && [self isInsideView:point(newCorner2)];
        bool flipped = normalsFlipped(corner1, point(newCorner1), corner0)
        || normalsFlipped(corner2, point(newCorner2), corner3);
        
        if (squaredDistance(point(newCorner1), corner0) > minLength
            && squaredDistance(point(newCorner2), corner3) > minLength
            && !flipped
            && newPointsInView
            )
        {
            [_points setObject:newCorner1 atIndexedSubscript:corner1Index];
            [_points setObject:newCorner2 atIndexedSubscript:corner2Index];
        }
    }
}

- (void) updateHandles
{
    NSArray* handles = [LTCropViewHelper computeCenterPoints:_points minimumDistance:50];
    [_points replaceObjectsInRange:NSRangeFromString(@"{4, 4}") withObjectsFromArray:handles];
}

- (void) drawPoints
{
    [_cropLayer update:_points];
}

- (void) showLoupeAtPoint:(CGPoint) point
{
    self.loupe.hidden = NO;
    self.loupe.LTtouchPoint = point;
    [self.loupe setNeedsDisplay];
}

- (BOOL) isInsideView:(CGPoint) p
{
    p = [_cropView convertPointFromView:p];
    
    CGSize size = _cropView.image.size;
    
    return (int)p.x >= 0 && (int)p.y >= 0 && (int)p.x <= (int)size.width && (int)p.y <= (int)size.height;
}

- (int) indexOfPointAt:(CGPoint) p
{
    const float threshDistance2 = 30 * 30;
    CGPoint q;
    
    for (int i = 0; i < [_points count]; ++i)
    {
        q = point(_points[i]);
        if (squaredDistance(p, q) < threshDistance2)
            return i;
    }
    return -1;
}

//- (void) enableApplyButton:(BOOL) enable
//{
//    self.navigationItem.rightBarButtonItem.enabled = enable;
//}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        self->_loupe.hidden = YES;
        [self->_cropLayer removeFromSuperlayer];
        self->_cropLayer = nil;
//        if (_points.count == 8) {
//            for (int i = 0; i < 4; ++i)
//                [_points removeLastObject];
//        }
//        _points = [_helper transformPointsToImageSpace:_points];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        [self.view layoutSubviews];
        self->_helper = [[LTCropViewHelper alloc] initViewCropView:self->_cropView];
        
        [self initGestureRecognizer];
        [self createLoupe];
        
        if (self->_cropLayer == nil)
        {
            // Select all when no detection points set.
            if ([self->_points count] != 4)
                self->_points = [self->_helper createPointsAtImageCorners];
            
            // Create a small inset on the image view, so that points around the border can be grabbed.
            //if ([_helper pointsCloseToBorder:_points])
            //_cropView.frame = CGRectInset(_cropView.frame, 25, 25);
            
            self->_loupe.LTframeRect = self->_cropView.frame;
            
            self->_cropLayer = [[LTCropLayer alloc] initWithFrame:self->_cropView.frame];
            [self->_cropView.layer addSublayer:self->_cropLayer];
            
            self->_points = [self->_helper transformPointsToViewSpace:self->_points];
            
            for (int i = 0; i < 4; ++i)
                [self->_points addObject:[NSValue valueWithCGPoint:CGPointMake(-1, -1)]];
            
            [self updateHandles];
            [self drawPoints];
        }
    }];
}

@end
