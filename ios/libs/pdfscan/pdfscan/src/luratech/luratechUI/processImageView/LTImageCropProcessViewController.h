//
//  LTImageCropProcessViewController.h
//  FoxitApp
//
//  Created by Apple on 16/12/14.
//
//

#import <UIKit/UIKit.h>

@protocol LTImageCropProcessViewDelegate <NSObject>
- (void)cancelCrop;
- (void)cropViewChangedDetection:(NSArray*) points imageWidth:(int)width imageHeight:(int) height;
@end


@interface LTImageCropProcessViewController : UIViewController
@property (nonatomic, weak) id<LTImageCropProcessViewDelegate> delegate;
- (id) initWithImage:(UIImage*) image andPoints:(NSArray*) points;
@end
