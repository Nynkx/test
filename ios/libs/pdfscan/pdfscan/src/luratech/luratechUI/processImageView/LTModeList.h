//
//  LTModeList.h
//  FoxitApp
//
//  Created by Apple on 16/12/6.
//
//

#import <UIKit/UIKit.h>

@interface LTModeList : UITableView
@property (nonatomic, strong) LTImageProcessor* imageProcessor;

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
@end
