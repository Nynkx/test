//
//  LTImageProcessViewControllerNew.m
//  FoxitApp
//
//  Created by Apple on 16/12/2.
//
//

#import "LTImageProcessViewControllerNew.h"
#import "LTImageContentView.h"
#import "LTModeList.h"
#import "LTPageFormat.h"
#import "LTPageFormatHelper.h"
#import "LTImageCropProcessViewController.h"

@interface LTImageProcessViewControllerNew ()<UIScrollViewDelegate,LTImageCropProcessViewDelegate,UIGestureRecognizerDelegate,LTImageProcessorDelegate>
{
    int _selectedPoint;
    bool _didMovePoint;
}
@property (nonatomic, strong) LTImageContentView *contentView;
@property (nonatomic, strong) LTModeList *modeList;
@property (nonatomic, strong) UIView *bottomBar;
@property (nonatomic, strong) LTImageProcessor* imageProcessor;
@property (nonatomic, strong) LTPageFormat* pageFormat;
@property (nonatomic, assign) BOOL isCroping;
@property (nonatomic, assign) BOOL isModeling;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) UIButton *modelBtn;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *rotationBtn;
@property (nonatomic, strong) UIButton *cropBtn;
@property (nonatomic, assign) BOOL isChanged;
@property NSMutableArray* points;
@end

@implementation LTImageProcessViewControllerNew

- (id) initWithImage:(UIImage*) image detectionResult:(LTDetectionResult*) result
{
    if (self = [super init])
    {
        _isCroping = NO;
        _isModeling = NO;
        _imageProcessor = [[LTImageProcessor alloc] initWithImage:image];
        _imageProcessor.colorMode = LTColorModeRGB;
        [_imageProcessor setDelegate:self];
        if (result == nil) {
            NSArray *points = [NSMutableArray arrayWithObjects:
                               [NSValue valueWithCGPoint:CGPointMake(0, 0)],
                               [NSValue valueWithCGPoint:CGPointMake(image.size.width, 0)],
                               [NSValue valueWithCGPoint:CGPointMake(image.size.width, image.size.height)],
                               [NSValue valueWithCGPoint:CGPointMake(0, image.size.height)],
                               nil];
            result = [[LTDetectionResult alloc]
                      initWithPoints:points
                      imageWidth:image.size.width
                      imageHeight:image.size.height];
        }
        [_imageProcessor setDetectionResult:result];
        
        int horDpi = [_imageProcessor horizontalDpi];
        int verDpi = [_imageProcessor verticalDpi];
        CGSize size = image.size;
        LTPageFormat *format = [LTPageFormatHelper evaluateFormat:size.width height:size.height horDpi:horDpi verDpi:verDpi];
        _pageFormat = format;
        
        [_imageProcessor setPageFormatWidth:_pageFormat.width height:_pageFormat.height];
        //[_imageProcessor rotateImage:LTRotationDirectionCW];
        const unsigned int LowResThreshold = 600 * 1000 * 1000;	// 600 MB
        bool useLowRes = [NSProcessInfo processInfo].physicalMemory < LowResThreshold;
        [_imageProcessor setComputeLowResPreview: useLowRes];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSubViews];
    _isChanged = NO;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)initSubViews
{
    UIView *bottomBar = [[UIView alloc] init];
    self.bottomBar = bottomBar;
    bottomBar.backgroundColor = UIColorHex(0x1C1C1C);
    [self.view addSubview:bottomBar];
    [bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bottomBar.superview.mas_left);
        make.right.mas_equalTo(bottomBar.superview.mas_right);
        make.bottom.mas_equalTo(bottomBar.superview.mas_bottom);
        make.height.mas_equalTo(49);
    }];
    [self initBottomBar:bottomBar];
    
    UIView *contentViewAutoLayout = [[UIView alloc] init];
    contentViewAutoLayout.backgroundColor = UIColorHex(0x3F3F3F);
    [self.view addSubview:contentViewAutoLayout];
    [contentViewAutoLayout mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(contentViewAutoLayout.superview.mas_left);
        make.right.mas_equalTo(contentViewAutoLayout.superview.mas_right);
        make.top.mas_equalTo(contentViewAutoLayout.superview.mas_top);
        make.bottom.mas_equalTo(bottomBar.mas_top);
    }];
    
    LTImageContentView *contentView = [[LTImageContentView alloc] init];
    self.contentView = contentView;
    contentView.delegate = self;
    contentView.backgroundColor = UIColorHex(0x3F3F3F);
    //contentView.contentSize = CGSizeMake([NSObject fs_getForegroundActiveWindow].bounds.size.width * 3, [NSObject fs_getForegroundActiveWindow].bounds.size.height - 49);
    contentView.contentSize = CGSizeMake([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20, [NSObject fs_getForegroundActiveWindow].bounds.size.height - 74);
    contentView.minimumZoomScale=1.0f;
    contentView.maximumZoomScale=3.0f;
    [contentViewAutoLayout addSubview:contentView];
    [contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(contentView.superview.mas_left).offset(10);
        make.right.mas_equalTo(contentView.superview.mas_right).offset(-10);
        make.top.mas_equalTo(contentView.superview.mas_top).offset(20);
        make.bottom.mas_equalTo(bottomBar.mas_top).offset(-5);
    }];
    
    [contentView addImageView];
    contentView.editImage = _imageProcessor.inputImage;
    //contentView.contentOffset = CGPointMake([NSObject fs_getForegroundActiveWindow].bounds.size.width, 0);
}

- (void)initBottomBar:(UIView *)bottomBar
{
    UIButton *cropBtn = [[UIButton alloc] init];
    _cropBtn = cropBtn;
    _cropBtn.accessibilityLabel = FSLocalizedForKey(@"kCropDone");
    [cropBtn setImage:ImageNamed(@"imageProcessCrop") forState:UIControlStateNormal];
    [cropBtn setImage:[Utility unableImage:ImageNamed(@"imageProcessCrop")] forState:UIControlStateHighlighted];
    [cropBtn addTarget:self action:@selector(cropBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:cropBtn];
    [cropBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(cropBtn.superview.mas_centerX);
        make.centerY.mas_equalTo(cropBtn.superview.mas_centerY);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(40);
    }];
    
    UIButton *cancelBtn = [[UIButton alloc] init];
    _cancelBtn = cancelBtn;
    [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitle:FSLocalizedForKey(@"kCancel") forState:UIControlStateNormal];
    CGSize size = [Utility getTextSize:FSLocalizedForKey(@"kCancel") fontSize:15 maxSize:CGSizeMake(150, 44)];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBtn setTitleColor:NormalBarBackgroundColor forState:UIControlStateHighlighted];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [bottomBar addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cancelBtn.superview.mas_left).offset(14);
        make.centerY.mas_equalTo(cancelBtn.superview.mas_centerY);
        make.height.mas_equalTo(49);
        make.width.mas_equalTo(size.width);
    }];
    
    UIButton *saveBtn = [[UIButton alloc] init];
    _saveBtn = saveBtn;
    //saveBtn.enabled = NO;
    saveBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [saveBtn setTitle:FSLocalizedForKey(@"kSave") forState:UIControlStateNormal];
    
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveBtn setTitleColor:NormalBarBackgroundColor forState:UIControlStateHighlighted];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    size = [Utility getTextSize:FSLocalizedForKey(@"kSave") fontSize:15 maxSize:CGSizeMake(150, 44)];
    [bottomBar addSubview:saveBtn];
    [saveBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(saveBtn.superview.mas_right).offset(-14);
        make.centerY.mas_equalTo(saveBtn.superview.mas_centerY);
        make.height.mas_equalTo(49);
        make.width.mas_equalTo(size.width);
    }];
    
    UIButton *rotateBtn = [[UIButton alloc] init];
    rotateBtn.accessibilityLabel = FSLocalizedForKey(@"kRotate");
    _rotationBtn = rotateBtn;
    [rotateBtn setImage:ImageNamed(@"imageProcessRotate") forState:UIControlStateNormal];
    [rotateBtn setImage:[Utility unableImage:ImageNamed(@"imageProcessRotate")] forState:UIControlStateHighlighted];
    [rotateBtn addTarget:self action:@selector(rotateBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:rotateBtn];
    [rotateBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_cropBtn.mas_left).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? -16 : -36);
        make.centerY.mas_equalTo(rotateBtn.superview.mas_centerY);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(40);
    }];
    
    UIButton *modelBtn = [[UIButton alloc] init];
    modelBtn.accessibilityLabel = FSLocalizedForKey(@"kAdjustBtn");
    _modelBtn = modelBtn;
    [modelBtn setImage:ImageNamed(@"imageProcessModel") forState:UIControlStateNormal];
    [modelBtn setImage:ImageNamed(@"imageProcessModelTap") forState:UIControlStateSelected];
    [modelBtn addTarget:self action:@selector(modelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:modelBtn];
    [modelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_cropBtn.mas_right).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? 16 : 36);
        make.centerY.mas_equalTo(modelBtn.superview.mas_centerY);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(40);
    }];
}

- (void)viewSafeAreaInsetsDidChange
{
    UIEdgeInsets edget = UIEdgeInsetsZero;
    //if (IS_IPHONE_OVER_TEN) {
        edget = self.view.safeAreaInsets;
        [self.bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bottomBar.superview.mas_left);
            make.right.mas_equalTo(self.bottomBar.superview.mas_right);
            make.bottom.mas_equalTo(self.bottomBar.superview.mas_bottom);
            make.height.mas_equalTo(49 + edget.bottom);
        }];
        
        [_cropBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_cropBtn.superview.mas_centerX);
            make.centerY.mas_equalTo(_cropBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
        
        CGSize size = [Utility getTextSize:FSLocalizedForKey(@"kCancel") fontSize:16 maxSize:CGSizeMake(150, 44)];
        [_cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cancelBtn.superview.mas_left).offset(14 + edget.left);
            make.centerY.mas_equalTo(_cancelBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(49);
            make.width.mas_equalTo(size.width);
        }];
        
        size = [Utility getTextSize:FSLocalizedForKey(@"kSave") fontSize:16 maxSize:CGSizeMake(150, 44)];
        [_saveBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_saveBtn.superview.mas_right).offset(-14 - edget.right);
            make.centerY.mas_equalTo(_saveBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(49);
            make.width.mas_equalTo(size.width);
        }];
        
        [_rotationBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_cropBtn.mas_left).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? -16 : -36);
            make.centerY.mas_equalTo(_rotationBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
        
        [_modelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cropBtn.mas_right).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? 16 : 36);
            make.centerY.mas_equalTo(_modelBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
        
        [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_contentView.superview.mas_left).offset(10 + edget.right);
            make.right.mas_equalTo(_contentView.superview.mas_right).offset(-10 - edget.right);
            make.top.mas_equalTo(_contentView.superview.mas_top).offset(20 + edget.top);
            make.bottom.mas_equalTo(_bottomBar.mas_top).offset(-5);
        }];
        
        self.contentView.contentSize = CGSizeMake([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20 - edget.left - edget.right, [NSObject fs_getForegroundActiveWindow].bounds.size.height - 74 - edget.top - edget.bottom);
        self.contentView.contentOffset = CGPointZero;
        [self.contentView refreshImage];
    //}
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.contentView.editImageView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.contentView.contentSize = CGSizeMake([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20, [NSObject fs_getForegroundActiveWindow].bounds.size.height - 74);
    self.contentView.contentOffset = CGPointZero;
    [self.contentView refreshImage];
}

- (void)viewDidAppear:(BOOL)animated
{
    //self.contentView.contentOffset = CGPointMake([NSObject fs_getForegroundActiveWindow].bounds.size.width, 0);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //[self.frame onRotateChangedBefore:toInterfaceOrientation duration:duration];
    NSLog(@"------will rotation-------");
    [self.modeList willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[self.frame onRotateChangedAfter:fromInterfaceOrientation];
    NSLog(@"------did rotation-------");
    [self.modeList didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self willRotateToInterfaceOrientation:nil duration:0];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        [self didRotateFromInterfaceOrientation:nil];
        if (IS_IPHONE_OVER_TEN) {
            return;
        }
        UIEdgeInsets edget = UIEdgeInsetsZero;
        if (@available(iOS 11.0, *)) {
            edget = self.view.safeAreaInsets;
        }
        [self.bottomBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bottomBar.superview.mas_left);
            make.right.mas_equalTo(self.bottomBar.superview.mas_right);
            make.bottom.mas_equalTo(self.bottomBar.superview.mas_bottom);
            make.height.mas_equalTo(49 + edget.bottom);
        }];
        
        [self->_cropBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self->_cropBtn.superview.mas_centerX);
            make.centerY.mas_equalTo(self->_cropBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
        
        CGSize size = [Utility getTextSize:FSLocalizedForKey(@"kCancel") fontSize:16 maxSize:CGSizeMake(150, 44)];
        [self->_cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self->_cancelBtn.superview.mas_left).offset(14);
            make.centerY.mas_equalTo(self->_cancelBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(49);
            make.width.mas_equalTo(size.width);
        }];
        
        size = [Utility getTextSize:FSLocalizedForKey(@"kSave") fontSize:16 maxSize:CGSizeMake(150, 44)];
        [self->_saveBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self->_saveBtn.superview.mas_right).offset(-14);
            make.centerY.mas_equalTo(self->_saveBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(49);
            make.width.mas_equalTo(size.width);
        }];
        
        [self->_rotationBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self->_cropBtn.mas_left).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? -16 : -36);
            make.centerY.mas_equalTo(self->_rotationBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
        
        [self->_modelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self->_cropBtn.mas_right).offset((DEVICE_iPHONE || SIZECLASS == UIUserInterfaceSizeClassCompact) ? 16 : 36);
            make.centerY.mas_equalTo(self->_modelBtn.superview.mas_centerY).offset(-(edget.bottom * 0.5));
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
        
        [self->_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self->_contentView.superview.mas_left).offset(10);
            make.right.mas_equalTo(self->_contentView.superview.mas_right).offset(-10);
            make.top.mas_equalTo(self->_contentView.superview.mas_top).offset(20);
            make.bottom.mas_equalTo(self->_bottomBar.mas_top).offset(-5);
        }];
        
        self.contentView.contentSize = CGSizeMake([NSObject fs_getForegroundActiveWindow].bounds.size.width - 20, [NSObject fs_getForegroundActiveWindow].bounds.size.height - 74);
        self.contentView.contentOffset = CGPointZero;
        [self.contentView refreshImage];
    }];
}



#pragma mark ------bottom btn click event-------

- (void)cancelBtnClick:(UIButton *)btn
{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)rotateBtnClick:(UIButton *)btn
{
    [_imageProcessor rotateImage:LTRotationDirectionCCW];
    [self.view fs_showHUDLoading:nil];
    [_imageProcessor computePreview];
}

- (void)cropBtnClick:(UIButton *)btn
{
    if (!_isCroping) {
        if (_isModeling) {
            self.modelBtn.selected = NO;
            [self.modeList removeFromSuperview];
            _isModeling = NO;
        }
        _isCroping = YES;
        LTDetectionResult* result = _imageProcessor.detectionResult;
        NSMutableArray* points = [NSMutableArray arrayWithCapacity:4];
        for (NSValue* v in result.points)
        {
            CGPoint p;
            [v getValue:&p];
            
            p.x /= result.imageWidth;
            p.y /= result.imageHeight;
            p.x = MIN(MAX(0, p.x), 1);
            p.y = MIN(MAX(0, p.y), 1);
            [points addObject:[NSValue valueWithCGPoint:p]];
        }
        
        LTImageCropProcessViewController *cropVc = [[LTImageCropProcessViewController alloc] initWithImage:_imageProcessor.inputImage andPoints:points];
        cropVc.delegate = self;
        cropVc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:cropVc animated:YES completion:nil];
    }
}

- (void)modelBtnClick:(UIButton *)btn
{
    if (!_isModeling) {
        btn.selected = YES;
        _isModeling = YES;
        _tapGesture = [[UITapGestureRecognizer alloc] init];
        [self.view addGestureRecognizer:_tapGesture];
        _tapGesture.delegate = self;
        LTModeList *modeList = [[LTModeList alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        modeList.imageProcessor = _imageProcessor;
        self.modeList = modeList;
        [self.view addSubview:modeList];
        [modeList mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(modeList.superview.mas_left);
            make.right.mas_equalTo(modeList.superview.mas_right);
            make.bottom.mas_equalTo(self.bottomBar.mas_top);
            make.height.mas_equalTo(240);
        }];
    }else{
        btn.selected = NO;
        [self.modeList removeFromSuperview];
        _isModeling = NO;
    }
}

- (void)saveBtnClick:(UIButton *)btn
{
    if (!_isChanged) {
        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate processImageView:self didFinishWithResult:nil];
        }else{
            [self dismissViewControllerAnimated:YES completion:^{
                [self.delegate processImageView:self didFinishWithResult:nil];
            }];
        }
        return;
    }
     [self.view fs_showHUDLoading:nil];
    [_imageProcessor computeResult];
}

- (void)cancelCrop
{
    if (_isCroping) {
        _isCroping = NO;
    }
}

- (void) cropViewChangedDetection:(NSArray*) points imageWidth:(int)width imageHeight:(int) height
{
    if (_isCroping) {
        _isCroping = NO;
        LTDetectionResult* result = [[LTDetectionResult alloc]
                                     initWithPoints:points
                                     imageWidth:width
                                     imageHeight:height];
        
        [_imageProcessor setDetectionResult:result];
         [self.view fs_showHUDLoading:nil];
        [_imageProcessor computePreview];
    }
}

/*- (void)tapGuesture:(UITapGestureRecognizer *)tapGesture
{
    CGPoint point = [tapGesture locationInView:self.view];
    if (_isModeling) {
        if (!CGRectContainsPoint(self.modeList.frame, point)) {
            [self.modeList removeFromSuperview];
            _isModeling = NO;
        }
    }
}*/

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    CGPoint point = [touch locationInView:self.view];
    CGPoint point2 = [touch locationInView:self.bottomBar];
    if (_isModeling) {
        if (!CGRectContainsPoint(self.modeList.frame, point) && !CGRectContainsPoint(self.modelBtn.frame, point2)) {
            self.modelBtn.selected = NO;
            [self.modeList removeFromSuperview];
            _isModeling = NO;
        }
    }
    return NO;
}

#pragma mark -imageProcess Delegate

- (void) imageProcessor:(LTImageProcessor*) processor didFinishComputingPreviewImage:(UIImage*) image
{
    [self.view fs_hideHUDLoading];
    _isChanged = YES;
    self.contentView.editImage = image;
    [self.contentView refreshImage];
    if (_isCroping) {
        _isCroping = NO;
    }
}

- (void) imageProcessor:(LTImageProcessor*) processor didFinishComputingResultImage:(UIImage*) image
{
    [self.view fs_hideHUDLoading];
    if ([self.delegate respondsToSelector:@selector(processImageView:didFinishWithResult:)]) {
        NSData *imageDate = UIImagePNGRepresentation(image);
        image = nil;
        //NSData *newDate = [NSData dataWithData:imageDate];
       
        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate processImageView:self didFinishWithResult:[UIImage imageWithData:imageDate]];
        }else{
            [self dismissViewControllerAnimated:YES completion:^{
                 [self.delegate processImageView:self didFinishWithResult:[UIImage imageWithData:imageDate]];
            }];
        }
    }
}
@end
