//
//  LTImageContentView.h
//  FoxitApp
//
//  Created by Apple on 16/12/2.
//
//

#import <UIKit/UIKit.h>

@interface LTImageContentView : UIScrollView
@property (nonatomic,strong) UIImageView *editImageView;
@property (nonatomic, strong) UIImage *editImage;

- (void)addImageView;
- (void)refreshImage;

@end
