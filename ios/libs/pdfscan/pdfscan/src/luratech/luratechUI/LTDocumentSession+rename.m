//
//  LTDocumentSession+rename.m
//  FoxitApp
//
//  Created by Apple on 17/3/27.
//
//

#import "LTDocumentSession+rename.h"
#import <objc/runtime.h>

@implementation LTDocumentSession (rename)

- (NSString *)rename {
    return objc_getAssociatedObject(self, @selector(rename));
}

- (void)setRename:(NSString *)value {
    objc_setAssociatedObject(self, @selector(rename), value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
