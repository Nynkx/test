//
//  Foxit Europe AppFrameworkSDK Demo
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//
//	You may use the provided source code for the duration of your evaluation period.
//	Redistribution in binary form, with or without modification, is permitted upon
//  condition that the distributing person/company/institution has a valid license
//  for the Foxit Europe AppFrameworkSDK. Redistribution in source form is not allowed.
//

#import "LTPageFormat.h"


@implementation LTPageFormat

- (id) initWithFormatName:(NSString *)name width:(int)width height:(int)height
{
	self = [super init];
	if (self)
	{
		_name = name;
		_width = width;
		_height = height;
        _receipt = false;
	}
	return self;
}

- (id) initWithFormatName:(NSString *)name width:(int)width height:(int)height receipt:(Boolean)receipt
{
    self = [super init];
    if (self)
    {
        _name = name;
        _width = width;
        _height = height;
        _receipt = receipt;
    }
    return self;
}

+ (LTPageFormat*) pageFormatWith:(NSString *)name width:(int)width height:(int)height
{
	return [[LTPageFormat alloc] initWithFormatName:name
                                              width:width
                                             height:height
                                            receipt:false];
}

+ (LTPageFormat*) pageFormatWith:(NSString *)name width:(int)width height:(int)height receipt:(Boolean)receipt
{
    return [[LTPageFormat alloc] initWithFormatName:name
                                              width:width
                                             height:height
                                            receipt:receipt];
}

- (float) aspect
{
	if (_height == 0 || _receipt)
		return 0.0f;
	else if (_width > _height)
		return _width / (float)_height;
	else
		return _height / (float)_width;
}

- (NSString*) shortcutName
{
	return [_shortcutName length] > 0 ? _shortcutName : _name;
}

@end
