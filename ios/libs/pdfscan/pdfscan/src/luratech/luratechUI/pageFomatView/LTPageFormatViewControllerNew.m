//
//  LTPageFormatViewControllerNew.m
//  FoxitApp
//
//  Created by Apple on 16/12/12.
//
//

#import <UIKit/UIKit.h>
//#import "UIViewController+Extensions.h"

typedef NS_ENUM(NSInteger, LTPageFormatCellStyle) {
    LTPageFormatCellStyleNormal = 0,
    LTPageFormatCellStyleOriginal,
    LTPageFormatCellStyleCustom
};

@interface LTPageFormatCell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic, strong) UILabel *pageFormatName;
@property (nonatomic, strong) UILabel *widthValueLabel;
@property (nonatomic, strong) UILabel *heighValueLabel;
@property (nonatomic, strong) UITextField *customWidthValue;
@property (nonatomic, strong) UITextField *customHeighValue;
@property (nonatomic, strong) UIImageView *selectImageView;

- (instancetype)initWithCellStyle:(LTPageFormatCellStyle)cellStyle;
@end


@implementation LTPageFormatCell

- (instancetype)initWithCellStyle:(LTPageFormatCellStyle)cellStyle
{
    if (self = [super init]) {
        if (cellStyle == LTPageFormatCellStyleNormal) {
            _pageFormatName = [[UILabel alloc] init];
            _pageFormatName.textAlignment = NSTextAlignmentLeft;
            [self.contentView addSubview:_pageFormatName];
            [_pageFormatName mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_pageFormatName.superview.mas_left).offset(20);
                make.top.mas_equalTo(_pageFormatName.superview.mas_top).offset(5);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *widthLabel = [[UILabel alloc] init];
            widthLabel.textAlignment = NSTextAlignmentLeft;
            widthLabel.font = [UIFont systemFontOfSize:14];
            widthLabel.text = FSLocalizedForKey(@"kWidth");
            [self.contentView addSubview:widthLabel];
            [widthLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(widthLabel.superview.mas_left).offset(20);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(48);
                make.height.mas_equalTo(21);
            }];
            
            _widthValueLabel = [[UILabel alloc] init];
            _widthValueLabel.textAlignment = NSTextAlignmentRight;
            _widthValueLabel.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:_widthValueLabel];
            [_widthValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(widthLabel.mas_right).offset(10);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *unitLabel = [[UILabel alloc] init];
            unitLabel.textAlignment = NSTextAlignmentLeft;
            unitLabel.text = @"mm";
            unitLabel.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:unitLabel];
            [unitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_widthValueLabel.mas_right).offset(2);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *heighLabel = [[UILabel alloc] init];
            heighLabel.textAlignment = NSTextAlignmentLeft;
            heighLabel.font = [UIFont systemFontOfSize:14];
            heighLabel.text = FSLocalizedForKey(@"kHeight");
            [self.contentView addSubview:heighLabel];
            [heighLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(unitLabel.mas_right).offset(10);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(48);
                make.height.mas_equalTo(21);
            }];
            
            _heighValueLabel = [[UILabel alloc] init];
            _heighValueLabel.textAlignment = NSTextAlignmentRight;
            _heighValueLabel.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:_heighValueLabel];
            [_heighValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(heighLabel.mas_right).offset(10);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *unitLabel2 = [[UILabel alloc] init];
            unitLabel2.textAlignment = NSTextAlignmentLeft;
            unitLabel2.text = @"mm";
            unitLabel2.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:unitLabel2];
            [unitLabel2 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_heighValueLabel.mas_right).offset(2);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(21);
            }];
        }else if (cellStyle == LTPageFormatCellStyleOriginal){
            _pageFormatName = [[UILabel alloc] init];
            _pageFormatName.textAlignment = NSTextAlignmentLeft;
            [self.contentView addSubview:_pageFormatName];
            [_pageFormatName mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_pageFormatName.superview.mas_left).offset(20);
                make.centerY.mas_equalTo(_pageFormatName.superview.mas_centerY);
            }];
        }else if (cellStyle == LTPageFormatCellStyleCustom){
            _pageFormatName = [[UILabel alloc] init];
            _pageFormatName.textAlignment = NSTextAlignmentLeft;
            [self.contentView addSubview:_pageFormatName];
            [_pageFormatName mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_pageFormatName.superview.mas_left).offset(20);
                make.top.mas_equalTo(_pageFormatName.superview.mas_top).offset(5);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *widthLabel = [[UILabel alloc] init];
            widthLabel.textAlignment = NSTextAlignmentLeft;
            widthLabel.font = [UIFont systemFontOfSize:14];
            widthLabel.text = FSLocalizedForKey(@"kWidth");
            [self.contentView addSubview:widthLabel];
            [widthLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(widthLabel.superview.mas_left).offset(20);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(48);
                make.height.mas_equalTo(21);
            }];
            
            _customWidthValue = [[UITextField alloc] init];
            _customWidthValue.keyboardType = UIKeyboardTypeNumberPad;
            _customWidthValue.textAlignment = NSTextAlignmentRight;
            _customWidthValue.layer.borderColor = [UIColor grayColor].CGColor;
            _customWidthValue.layer.borderWidth = 1;
            _customWidthValue.layer.cornerRadius = 3;
            _customWidthValue.font = [UIFont systemFontOfSize:14];
            _customWidthValue.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 0)];
            _customWidthValue.rightViewMode = UITextFieldViewModeAlways;
            _customWidthValue.delegate = self;
            [self.contentView addSubview:_customWidthValue];
            [_customWidthValue mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(widthLabel.mas_right).offset(20);
                make.centerY.mas_equalTo(widthLabel.mas_centerY);
                make.width.mas_equalTo(80);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *unitLabel = [[UILabel alloc] init];
            //unitLabel.textAlignment = NSTextAlignmentLeft;
            unitLabel.text = @"mm";
            unitLabel.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:unitLabel];
            [unitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_customWidthValue.mas_right).offset(2);
                make.top.mas_equalTo(_pageFormatName.mas_bottom).offset(5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *heighLabel = [[UILabel alloc] init];
            heighLabel.textAlignment = NSTextAlignmentLeft;
            heighLabel.font = [UIFont systemFontOfSize:14];
            heighLabel.text = FSLocalizedForKey(@"kHeight");
            [self.contentView addSubview:heighLabel];
            [heighLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(heighLabel.superview.mas_left).offset(20);
                make.top.mas_equalTo(_customWidthValue.mas_bottom).offset(5);
                make.width.mas_equalTo(48);
                make.height.mas_equalTo(21);
            }];
            
            _customHeighValue = [[UITextField alloc] init];
            _customHeighValue.keyboardType = UIKeyboardTypeNumberPad;
            _customHeighValue.textAlignment = NSTextAlignmentRight;
            _customHeighValue.layer.borderColor = [UIColor grayColor].CGColor;
            _customHeighValue.layer.borderWidth = 1;
            _customHeighValue.layer.cornerRadius = 3;
            _customHeighValue.font = [UIFont systemFontOfSize:14];
            _customHeighValue.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 0)];
            _customHeighValue.rightViewMode = UITextFieldViewModeAlways;
            _customHeighValue.delegate = self;
            [self.contentView addSubview:_customHeighValue];
            [_customHeighValue mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(heighLabel.mas_right).offset(20);
                make.centerY.mas_equalTo(heighLabel.mas_centerY);
                make.width.mas_equalTo(80);
                make.height.mas_equalTo(21);
            }];
            
            UILabel *unitLabel2 = [[UILabel alloc] init];
            //unitLabel2.textAlignment = NSTextAlignmentLeft;
            unitLabel2.text = @"mm";
            unitLabel2.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:unitLabel2];
            [unitLabel2 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_customHeighValue.mas_right).offset(2);
                make.top.mas_equalTo(_customWidthValue.mas_bottom).offset(5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(21);
            }];
        }
        
        _selectImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_selectImageView];
        [_selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_selectImageView.superview.mas_right).offset(-4);
            make.centerY.mas_equalTo(_selectImageView.superview.mas_centerY);
            make.width.mas_equalTo(38);
            make.height.mas_equalTo(38);
        }];
    }
    return self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL isNumber = [self validateNumber:string];
    if (isNumber) {
        NSString *number = [textField.text stringByAppendingString:string];
        if ([number intValue] > 5587 || [number intValue] < 1) {
            AlertViewCtrlShow(FSLocalizedForKey(@"kWarning"), FSLocalizedForKey(@"kOutPageSizeTip"));
            return NO;
        }
    }
    return isNumber;
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

@end


#import "LTPageFormatViewControllerNew.h"
#import "LTPageFormatHelper.h"
#import "LTPageFormat.h"

@interface LTPageFormatViewControllerNew ()
@property (nonatomic, strong) NSArray *headerTitles;
@property (nonatomic, strong) NSDictionary *sectionContent;
@property (nonatomic, strong) UITableView *pageFormatListView;
@property (nonatomic, strong) UIButton *leftButton;
//@property (nonatomic, strong) UIView *topToolBar;
@end

@implementation LTPageFormatViewControllerNew

- (instancetype)init
{
    if (self = [super init]) {
        self.selectedFormat = [LTPageFormatHelper getStoredFormat];
        
        self.headerTitles = [LTPageFormatHelper getFormatNames];
        self.sectionContent = [LTPageFormatHelper getFormats];
    }
    return self; 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *leftButton = [[UIButton alloc] init];
    _leftButton = leftButton;
    [leftButton addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:ImageNamed(@"common_back_white") forState:UIControlStateNormal];
    [leftButton setImage:[Utility unableImage:ImageNamed(@"common_back_white")] forState:UIControlStateHighlighted];
    [leftButton setAccessibilityLabel:FSLocalizedForKey(@"kBack")];
    [self.navigationItem setHidesBackButton:YES];
    [leftButton sizeToFit];
    [self.navigationController.navigationBar addSubview:leftButton];
    leftButton.translatesAutoresizingMaskIntoConstraints = false;
    CGSize size = leftButton.bounds.size;
    if (@available(iOS 11.0, *)) {
        [leftButton.leftAnchor constraintEqualToAnchor:leftButton.superview.safeAreaLayoutGuide.leftAnchor constant:10].active = true;
    } else {
        [leftButton.leftAnchor constraintEqualToAnchor:leftButton.superview.leftAnchor constant:10].active = true;
    }
    [leftButton.centerYAnchor constraintEqualToAnchor:leftButton.superview.centerYAnchor].active = true;
    [leftButton.widthAnchor constraintEqualToConstant:size.width].active = true;
    [leftButton.heightAnchor constraintEqualToConstant:size.height].active = true;
    
    self.navigationItem.title = FSLocalizedForKey(@"kPageSize");

    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = ThemeNavBarColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:WhiteThemeNavBarTextColor,NSFontAttributeName:[UIFont systemFontOfSize:18]};
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.pageFormatListView = [[UITableView alloc] init];
    [self.view addSubview:_pageFormatListView];
    [self.pageFormatListView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    self.pageFormatListView.dataSource = self;
    self.pageFormatListView.delegate = self;
    self.pageFormatListView.cellLayoutMarginsFollowReadableWidth = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    }else if (indexPath.row == 1){
        return 88;
    }
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionKey = [self.headerTitles objectAtIndex:section];
    return [(NSArray *)[self.sectionContent objectForKey:sectionKey] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //find right pageFormat
    NSString *sectionKey = [self.headerTitles objectAtIndex:indexPath.section];
    NSArray *sectionValues = [self.sectionContent objectForKey:sectionKey];
    LTPageFormat *currentPageFormat = [sectionValues objectAtIndex:indexPath.row];
    LTPageFormatCell *cell;
    if (indexPath.row == 0) {
        cell = [[LTPageFormatCell alloc] initWithCellStyle:LTPageFormatCellStyleOriginal];
        cell.pageFormatName.text = FSLocalizedForKey(@"kOriginal");
        [cell.pageFormatName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0f]];
    }else if (indexPath.row == 1){
        cell = [[LTPageFormatCell alloc] initWithCellStyle:LTPageFormatCellStyleCustom];
        cell.pageFormatName.text = FSLocalizedForKey(@"kCustom");
        [cell.pageFormatName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0f]];
        [cell.customWidthValue setText:[NSString stringWithFormat:@"%d",currentPageFormat.width]];
        [cell.customHeighValue setText:[NSString stringWithFormat:@"%d",currentPageFormat.height]];
    }else{
        cell = [[LTPageFormatCell alloc] initWithCellStyle:LTPageFormatCellStyleNormal];
        cell.pageFormatName.text = currentPageFormat.name;
        [cell.pageFormatName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0f]];
        [cell.widthValueLabel setText:[NSString stringWithFormat:@"%d",currentPageFormat.width]];
        [cell.heighValueLabel setText:[NSString stringWithFormat:@"%d",currentPageFormat.height]];
    }
    
    if ([currentPageFormat.name isEqualToString:self.selectedFormat.name])
    {
        [cell.selectImageView setImage:ImageNamed(@"small_select_icon")];
    }
    else if ([currentPageFormat.name isEqualToString:FSLocalizedForKey(@"kOriginal")] && [self.selectedFormat.name isEqualToString:@"Original"])
    {
        [cell.selectImageView setImage:ImageNamed(@"small_select_icon")];
    }
    else if ([currentPageFormat.name isEqualToString:FSLocalizedForKey(@"kCustom")] && [self.selectedFormat.name isEqualToString:@"Custom"])
    {
        [cell.selectImageView setImage:ImageNamed(@"small_select_icon")];
    }
    else [cell.selectImageView setImage:nil];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionKey = [self.headerTitles objectAtIndex:indexPath.section];
    NSArray *section = [self.sectionContent objectForKey:sectionKey];
    self.selectedFormat = [section objectAtIndex:indexPath.row];
    
    LTPageFormatCell *customCell = (LTPageFormatCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (![_selectedFormat.name isEqualToString:@"Custom"]) {
        LTPageFormat *custom = [LTPageFormatHelper getCustomPageFormat];
        if (custom.width != [customCell.customWidthValue.text intValue] || (custom.height != [customCell.customHeighValue.text intValue] && ([customCell.customWidthValue.text intValue] > 1 && [customCell.customHeighValue.text intValue] > 1))) {
            custom.width = [customCell.customWidthValue.text intValue];
            custom.height = [customCell.customHeighValue.text intValue];
            [LTPageFormatHelper storeCustomFormat:custom];
        }
    }else{
        _selectedFormat.width = [customCell.customWidthValue.text intValue];
        _selectedFormat.height = [customCell.customHeighValue.text intValue];
    }
    
    [self notifyDelegate];
}

- (void)notifyDelegate
{
    if ([_delegate respondsToSelector:@selector(pageFomatViewController:didFinishWithFormat:)])
        [_delegate pageFomatViewController:self didFinishWithFormat:_selectedFormat];
}

#pragma mark - backBtn click
- (void)backBtnClick:(UIButton *)btn
{
    LTPageFormatCell *customCell = (LTPageFormatCell *)[self.pageFormatListView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (![_selectedFormat.name isEqualToString:@"Custom"]){
        LTPageFormat *custom = [LTPageFormatHelper getCustomPageFormat];
        if (custom.width != [customCell.customWidthValue.text intValue] || (custom.height != [customCell.customHeighValue.text intValue] && ([customCell.customWidthValue.text intValue] > 1 && [customCell.customHeighValue.text intValue] > 1))) {
            custom.width = [customCell.customWidthValue.text intValue];
            custom.height = [customCell.customHeighValue.text intValue];
            [LTPageFormatHelper storeCustomFormat:custom];
        }
    }else{
        _selectedFormat.width = [customCell.customWidthValue.text intValue];
        _selectedFormat.height = [customCell.customHeighValue.text intValue];
    }
    
    if ([_delegate respondsToSelector:@selector(pageFomatViewController:didFinishWithFormat:)])
        [_delegate pageFomatViewController:self didFinishWithFormat:_selectedFormat];
}

@end
