//
//  Foxit Europe AppFrameworkSDK Demo
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//
//	You may use the provided source code for the duration of your evaluation period.
//	Redistribution in binary form, with or without modification, is permitted upon
//  condition that the distributing person/company/institution has a valid license
//  for the Foxit Europe AppFrameworkSDK. Redistribution in source form is not allowed.
//

#import <Foundation/Foundation.h>

@class LTPageFormat;

/*! \brief Provides a static interface defining all required constants and available formats. */
@interface LTPageFormatHelper : NSObject

/*! returns dictionary of sections, german and international formates*/
+ (NSDictionary *)getFormats;

 /*! returns array of section names, these are keys of getFormats dictionary*/
+ (NSArray *)getFormatNames;

/*! Identifier used for automatic formating. */
+ (NSString*) originalFormatName;

/*! Get the identifer used for custom format. */
+ (NSString*) customFormatName;

+ (LTPageFormat*) getCustomPageFormat;
+ (void) storeCustomFormat:(LTPageFormat*) format;

+ (LTPageFormat*) getOriginalPageFormat;

/*! writes given pageFormat into user defaults */
+ (void) storeFormat:(LTPageFormat *)format;

/*! reads stored pageFormat out of user defaults, or originalFormat is nothing was stored */
+ (LTPageFormat *)getStoredFormat;

/*! evaluate width and height in mm and return the according format */
+ (LTPageFormat*) evaluateFormat:(int)width height:(int)height horDpi:(int)horDpi verDpi:(int)verDpi;

/*! get the format for specific range */
+ (LTPageFormat*) formatForRange:(int)range width:(int)width height:(int)height;

@end
