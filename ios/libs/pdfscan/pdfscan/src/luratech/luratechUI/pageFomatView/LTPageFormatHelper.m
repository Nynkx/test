//
//  Foxit Europe AppFrameworkSDK Demo
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//
//	You may use the provided source code for the duration of your evaluation period.
//	Redistribution in binary form, with or without modification, is permitted upon
//  condition that the distributing person/company/institution has a valid license
//  for the Foxit Europe AppFrameworkSDK. Redistribution in source form is not allowed.
//

#import "LTPageFormatHelper.h"
#import "LTPageFormat.h"

@implementation LTPageFormatHelper

+ (NSDictionary *)getFormats
{
	NSString* businessCard = FSLocalizedForKey(@"Business card");
	NSString* envelope = FSLocalizedForKey(@"Envelope");
	
    NSArray *formats = @[[LTPageFormat pageFormatWith:[LTPageFormatHelper originalFormatName] width:0 height:0],
                         [LTPageFormatHelper getCustomPageFormat],
                         [LTPageFormat pageFormatWith:@"A3" width:297 height:420],
						 [LTPageFormat pageFormatWith:@"A4" width:210 height:297],
						 [LTPageFormat pageFormatWith:@"A5" width:148 height:210],
						 [LTPageFormat pageFormatWith:envelope width:220 height:110],
						 [LTPageFormat pageFormatWith:businessCard width:90 height:55],
						 [LTPageFormat pageFormatWith:@"Executive" width:184 height:267],
                         [LTPageFormat pageFormatWith:@"Legal" width:216 height:356],
                         [LTPageFormat pageFormatWith:@"Letter" width:216 height:279],
                         [LTPageFormat pageFormatWith:@"Ledger" width:279 height:432]];
	
    return @{ FSLocalizedForKey(@"NameSection") : formats};
}

+ (NSArray *)getFormatNames
{
    return @[FSLocalizedForKey(@"NameSection")];
}

+ (LTPageFormat*) getCustomPageFormat
{
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	int customWidth = (int)[defaults integerForKey:@"pageFormat.custom.width"];
    int customHeight = (int)[defaults integerForKey:@"pageFormat.custom.height"];
	Boolean customReceipt = (Boolean)[defaults boolForKey:@"pageFormat.custom.receipt"];
	
	customWidth = MAX(1, customWidth);
	customHeight = MAX(1, customHeight);
	
	LTPageFormat* custom = [LTPageFormat pageFormatWith:[LTPageFormatHelper customFormatName]
												  width:customWidth
												 height:customHeight
                                                receipt:customReceipt];
	
	custom.shortcutName = FSLocalizedForKey(@"Custom_short");
	return custom;
}

+ (void) storeCustomFormat:(LTPageFormat *)format
{
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	[defaults setInteger:format.width forKey:@"pageFormat.custom.width"];
	[defaults setInteger:format.height forKey:@"pageFormat.custom.height"];
    [defaults setBool:format.receipt forKey:@"pageFormat.custom.receipt"];
}

+ (LTPageFormat*) getOriginalPageFormat
{
	return [LTPageFormat pageFormatWith:[LTPageFormatHelper originalFormatName] width:0 height:0];
}

#pragma mark - storage

+ (void) storeFormat:(LTPageFormat *)format
{
    if ([format.name isEqualToString:[LTPageFormatHelper customFormatName]]) {
        [LTPageFormatHelper storeCustomFormat:format];
    }
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:format.name forKey:@"pageFormat.name"];
    [defaults setInteger:format.width forKey:@"pageFormat.width"];
    [defaults setInteger:format.height forKey:@"pageFormat.height"];
    [defaults setBool:format.receipt forKey:@"pageFormat.receipt"];
	[defaults setObject:format.shortcutName forKey:@"pageFormat.shortcut"];
}

+ (LTPageFormat *)getStoredFormat
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* storedFormatName = [defaults stringForKey:@"pageFormat.name"];
	NSString* shortcutName = [defaults stringForKey:@"pageFormat.shortcut"];
	int storedFormatWidth = (int)[defaults integerForKey:@"pageFormat.width"];
    int storedFormatHeight = (int)[defaults integerForKey:@"pageFormat.height"];
    Boolean storedFormatReceipt = (Boolean)[defaults boolForKey:@"pageFormat.receipt"];
    
	
    if (storedFormatName == nil)
	{
        return [LTPageFormat pageFormatWith:[LTPageFormatHelper originalFormatName] width:0 height:0 receipt:false];
	}
	else
	{
		LTPageFormat* format = [LTPageFormat pageFormatWith:storedFormatName
													  width:storedFormatWidth
													 height:storedFormatHeight
                                                    receipt:storedFormatReceipt];
		
		if (shortcutName != nil)
			format.shortcutName = shortcutName;
		
		return format;
	}
}

+ (NSString*) customFormatName
{
	return @"Custom";
}

+ (NSString*) originalFormatName
{
	return @"Original";
}

+ (LTPageFormat*) evaluateFormat:(int)width height:(int)height horDpi:(int)horDpi verDpi:(int)verDpi
{
    if  (horDpi == 300 && verDpi == 300) {
        return [self getOriginalPageFormat];
    }
    else
    {
        const int range = 2;
        int widthInMM = width * 25.4 / horDpi;
        int heightInMM = height * 25.4 /verDpi;
        LTPageFormat *format = [self formatForRange:range width:widthInMM height:heightInMM];
        if (format == nil)
        {
            format = [self formatForRange:range width:heightInMM height:widthInMM];
            if (format == nil)
            {
                int customWidth = MAX(1, widthInMM);
                int customHeight = MAX(1, heightInMM);
                LTPageFormat* custom = [LTPageFormat pageFormatWith:[LTPageFormatHelper customFormatName] width:customWidth height:customHeight];
                return custom;
            }
            return format;
        }
        return format;
    }
}

+ (LTPageFormat*) formatForRange:(int)range width:(int)width height:(int)height
{
    int wmin = width - range, wmax = width + range;
    int hmin = height - range, hmax = height + range;
    NSArray *formats = [[self getFormats] allValues][0];
    NSString *predicateFormat = @"width>=%i AND width<=%i AND height>=%i AND height<=%i";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat, wmin, wmax, hmin, hmax];
    LTPageFormat *format = [formats filteredArrayUsingPredicate:predicate].firstObject;
    return format;
}

@end
