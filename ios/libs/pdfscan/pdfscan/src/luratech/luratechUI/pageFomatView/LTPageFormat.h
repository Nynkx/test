//
//  Foxit Europe AppFrameworkSDK Demo
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//
//	You may use the provided source code for the duration of your evaluation period.
//	Redistribution in binary form, with or without modification, is permitted upon
//  condition that the distributing person/company/institution has a valid license
//  for the Foxit Europe AppFrameworkSDK. Redistribution in source form is not allowed.
//

#import <Foundation/Foundation.h>

@interface LTPageFormat : NSObject

+ (LTPageFormat*) pageFormatWith:(NSString*) name width:(int) width height:(int) height;
+ (LTPageFormat*) pageFormatWith:(NSString*) name width:(int) width height:(int) height receipt:(Boolean) receipt;
- (id) initWithFormatName:(NSString*) name width:(int) width height:(int) height;

@property (nonatomic) int width;
@property (nonatomic) int height;
@property (nonatomic) NSString* name;

/*! Defines an abbreviation for long names. If not set it returns the same as name. */
@property (nonatomic) NSString* shortcutName;

/*! \brief Get the aspect ratio of the page format.
 
	// TODO: Check whether really a requirement, or if it can be dropped.
	For valid formats, e.g. width and height > 0, the returned aspect ratio is always >= 1. 
 */
@property (nonatomic, readonly) float aspect;

/*! \brief Is the page format a long and narrow cash register receipt?
    If so, then the length is mostly ignored (only the width is used).
    Likewise, the aspect ratio is handled as if unknown (as with "Original" size).
 */
@property (nonatomic) Boolean receipt;

@end
