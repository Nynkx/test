//
//  LTPageFormatViewControllerNew.h
//  FoxitApp
//
//  Created by Apple on 16/12/12.
//
//

#import <UIKit/UIKit.h>

@class LTPageFormat;
@class LTPageFormatViewControllerNew;

@protocol LTPageFormatViewControllerDelegate<NSObject>
-(void) pageFomatViewController:(LTPageFormatViewControllerNew*) controller
            didFinishWithFormat:(LTPageFormat*) pageFormat;
@end

@interface LTPageFormatViewControllerNew : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) LTPageFormat* selectedFormat;

@property (weak, nonatomic) id<LTPageFormatViewControllerDelegate> delegate;
@end
