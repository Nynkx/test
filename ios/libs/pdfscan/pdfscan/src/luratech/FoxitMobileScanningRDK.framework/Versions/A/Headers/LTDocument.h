//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTDocument.h
	\brief Declares the LTDocument class and its delegate.
 */

#pragma once

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LTDocument;

/*! \brief Represents the type of an LTDocument. */
typedef enum
{
	LTDocumentTypePDF,				/*!< \brief The document is a PDF. */
	LTDocumentTypeImage,			/*!< \brief The document is an jpeg image. */
	LTDocumentTypeImageSeries,		/*!< \brief The document is a series of jpeg images. */
	LTDocumentTypeUnknown = -1		/*!< \brief The document type is not known. Used to indicate errors. */
}
LTDocumentType;

///*! \brief Protocol for classes responding to LTDocument events. 
// 
// */
//@protocol LTDocumentDelegate<NSObject>
//
///*! Called when page \c pageIndex of \c document was successfully loaded.
// 
//	Implementing this method is required.
// 
//	\param document The document calling the delegate.
//	\param image The image of the page.
//	\param pageIndex The index of the page in the document.
// */
//- (void) document:(LTDocument*) document didGenerateImage:(UIImage*)image forPage:(int) pageIndex;
//@end



/*! \brief LTDocument represents a document stored in the applications document directory.
 
	Documents are identified by their LTDocument::documentName property.
 
	A document can either be a PDF, an image or an image series. Multiple documents with the same name
	but different types are not allowed.
 
	The thumbnails of documents are stored in the applications cache directory.
	Usually they only have to be generated once, when the document is created.
	However, when iOS purges the contents of the cache directory, they need to be recomputed.
	Generation of document thumbnails is automatically managed by instances of the LTDocumentManager class.
 
	LTDocument requires a valid MobileCompressionSDK license.
 */
@interface LTDocument : NSObject

///*! \brief The delegate called by the document. */
//@property (weak, nonatomic) id<LTDocumentDelegate> delegate;

/*! \brief The document's name. */
@property (readonly, nonatomic) NSString* documentName;

/*! \brief Time when the document was created in seconds since January 01. 1970 (Unix timestamp). */
@property (readonly, nonatomic) double timeOfCreation;

/*! \brief Size of the document in bytes. */
@property (readonly, nonatomic) int fileSize;

/*! \brief Number of pages in the document. */
@property (readonly, nonatomic) int pageCount;

/*! \brief The type of the document. */
@property (readonly, nonatomic) LTDocumentType documentType;

/*! \brief Files making up the document.
 
	PDFs and images contain of only on file. Image series may have multiple entries.
 */
@property (readonly, nonatomic) NSArray* filenames;

/*! \brief The document's mime-type. */
@property (readonly, nonatomic) NSString* mimeType;

/*! \brief The document's thumbnail.
 
	This property is nil when the document's thumbnail has not been generated yet.
	If a thumbnail is missing LTDocumentDelegate::documentFinishedGeneratingThumbnails: is called when it becomes ready.
 
	The generation of document thumbnails is not initiated by the document itself, but by the LTDocumentManager class.
 */
@property (readonly, nonatomic) UIImage* thumbnail;


///*! \brief Request the given page as image.
// 
//	The function calls the delegate's LTDocumentDelegate::document:didGenerateImage:forPage: method when the image is ready.
//	For images the delegate will be called almost immediately. 
//	However, for PDF it may take while to decompress the page.
//	
//	The document does not keep a reference to the image after calling the delegate.
//
//	\param pageIndex Index of the page.
// */
//- (void) getImageForPage:(int) pageIndex;

/*! \brief Get the documents filename as URL.
 
	For PDFs and images this is an URL to the files directly.
	For image series the URL is referencing the directory containing the images.
 */
- (NSURL*) urlForDocument;

/*! Compare whether two documents are the same.
	For comparison the documents filename is considerted.
 */
- (BOOL) isEqual:(LTDocument*) other;

@end
