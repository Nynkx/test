//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTAppFrameworkSdk.h
	\brief Main include file for the Foxit Europe Mobile Scanning RDK (formerly known as App Framework SDK).
 
	Imports all framework headers and declares a method for framework initialization.
	Initializing the framework is required before any framework class can be instantiated.
 */
#pragma once

#import <Foundation/Foundation.h>

/*! \brief Returns values for ::AppFrameworkSdk_Init. 
 
	The values defined in this enumeration are used as return value for ::AppFrameworkSdk_Init 
	and ::MobileCompressionSdk_Init.
	They are used to indicate the result of RDK initialization.
 */
typedef enum
{
	LTInitStatusSuccess,			/*!< \brief Initialization successful. */
	LTInitStatusInvalidLicense,		/*!< \brief Initialization failed due to an invalid license. */
	LTInitStatusExpiredLicense		/*!< \brief Initialization failed due to an expired license. */
}
LTInitStatus;

/*! \brief Container containing additional configuration parameters for the AppFramework.
 */
@interface AppFrameworkConfiguration : NSObject
/*!
    Contains a path where all documents should be stored and loaded.
    It is not necessary that the directory is already existing.
*/
@property (retain) NSString *documentsDirectory;
@end

/*! \brief Initialize the Mobile Scanning RDK.
 
	This function must be called before any Mobile Scanning RDK object can be instantiated.

	Successful initialization of the RDK requires a valid serial number,
	or a demo serial number (in which case you will get watermarks).
 
	\param serial1 First part of the serial number.
	\param serial2 Second part of the serial number.
	\return \arg \c LTInitStatusSuccess when serial is valid and initialization succeeded.
			\arg \c LTInitStatusInvalidLicense when serial is invalid.
			\arg \c LTInitStatusExporedLicense when serial number valid but expired.
 */
LTInitStatus AppFrameworkSdk_Init(unsigned long serial1, unsigned long serial2);

/*! \brief Initialize the Mobile Scanning RDK with additional parameters.
 
	This function must be called before any Mobile Scanning RDK object can be instantiated.
 
	Successful initialization of the RDK requires a valid serial number,
	or a demo serial number (in which case you will get watermarks).
 
	\param serial1 First part of the serial number.
	\param serial2 Second part of the serial number.
    \param configuration contains additional configuration parameters for the SDK.
           If configuration is nil, the configuration is default and identical to AppFrameworkSdk_Init()
 
	\return \arg \c LTInitStatusSuccess when serial is valid and initialization succeeded.
 \arg \c LTInitStatusInvalidLicense when serial is invalid.
 \arg \c LTInitStatusExporedLicense when serial number valid but expired.
 
    /code
        Sample how to set the documents directory to some private directory.
 
        AppFrameworkConfiguration* appFrameworkConfiguration = [AppFrameworkConfiguration alloc];
 
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        if ( paths && ([paths count] > 0))
            [appFrameworkConfiguration setDocumentsDirectory:[[paths objectAtIndex:0] stringByAppendingString:@"/InternalDocuments"]];
 
 */
LTInitStatus AppFrameworkSdk_InitEx(unsigned long serial1, unsigned long serial2, AppFrameworkConfiguration* configuration);

/*! \brief Initialize the Mobile Scanning RDK.
 
	To enable PDF functionality you need to initialize the MobileCompressionSDK as well.
	Classes requiring the MobileCompressionSDK to be initialized are:
		\arg LTDocument
		\arg LTDocumentManager
		\arg LTDocumentSession
 
	\param serial1 First part of the serial number.
	\param serial2 Second part of the serial number.
	\return \arg \c LTInitStatusSuccess when serial is valid and initialization succeeded.
			\arg \c LTInitStatusInvalidLicense when serial is invalid.
			\arg \c LTInitStatusExporedLicense when serial number valid but expired.
 */
LTInitStatus MobileCompressionSdk_Init(unsigned long serial1, unsigned long serial2);

// Import all framework components.
#import <FoxitMobileScanningRDK/LTDocumentSession.h>
#import <FoxitMobileScanningRDK/LTCamera.h>
#import <FoxitMobileScanningRDK/LTCaptureService.h>
#import <FoxitMobileScanningRDK/LTDetectionResult.h>
#import <FoxitMobileScanningRDK/LTDocument.h>
#import <FoxitMobileScanningRDK/LTDocumentManager.h>
#import <FoxitMobileScanningRDK/LTImageProcessor.h>

/*!
	\mainpage Foxit Europe Mobile Scanning RDK iOS
	\section sec1 General
 
	The Foxit Europe Mobile Scanning RDK provides building blocks to write your own PDF Scan application for iOS,
	that turns your or your user's iPhones and iPads into a mobile scanner for all kinds of documents.
	
	The Mobile Scanning RDK uses Foxit Europe’s award-winning MRC compression technology to create highly compressed
	documents.
	Foxit Europe's layer-based mixed raster content technology (MRC) can compress scanned documents to a ratio
	of 1:100 or better. Outstanding image quality and text legibility are preserved, while storage costs and
	bandwidth requirements are drastically reduced.
 
	The Mobile Scanning RDK provides support for the following tasks:
		\arg Control camera focus and take pictures automatically when the camera is focused on a document.
		\arg Detection of page edges provides for automatic cropping and scaling of pages.
		\arg Brightness, contrast and color balance are adjusted automatically for optimal readability.
		\arg Create PDF files fully compliant with PDF/A-2u (ISO 19005-2), the file format recommended for long-term archiving.
		\arg Store all images taken as PDF files and optionally also as series of JPEG images.
		\arg Pages can be rotated and re-ordered before converting them to a PDF file.
		\arg Edit PDF files, e.g. move, add, remove pages, after they have been created. (Currently only supported for PDF files created with the SDK.)
	
	\section sec2 Demo application
 
	The accompanying source code of the demo application shows how the Mobile Scanning RDK can be used to
	create scan apps.
	
	Before you can run the demo application you need to enter your license information into the demo's source code.
	To do this you open \c LTAppDelegate.m and look for the method initializeSdk.
	Enter the serials you received together with the RDK and remove the \c error preprocessor directive.
	With the supplied demo serial numbers you will get watermarks in your documents.
 
	\section sec3 Use in your own app
 
	To add the Mobile Scanning RDK to your own application you need to add the \c AppFrameworkSDK.framework
	found in the distribution to your project and to your project's build target using 'Link Binary With Libraries' from the
	'Build Phases' tab.
 
	Additionally you need to add the following iOS frameworks (Xcode 6.1):
		\arg \c AssetsLibrary.framework
		\arg \c CoreMotion.framework
		\arg \c UIKit.framework
 
	You also need to add "-ObjC" to "Other Linker Flags" in your projects build settings.
	
	In case there are link errors related to the C++ standard library you need to change the extension of one of your source files from \c .\c m to \c .\c mm.
	This has the effect that Xcode now links against the C++ standard library and the link errors should disappear.
 
	Before you can create any RDK objects you must initialize the Mobile Scanning RDK using ::AppFrameworkSdk_Init with your serial number as parameter.
	If you also require PDF functionality, you need to initialize the MobileCompressionSDK using ::MobileCompressionSdk_Init with your MobileCompressionSdk serial number.
	Without specific initialization of the MobileCompressionSDK the following modules are not available:
		\arg LTDocument
		\arg LTDocumentManager
		\arg LTDocumentSession
 
	\section sec4 Support
 
	If you have any problems or questions using the Mobile Scanning RDK you can contact our support.
		\arg e-mail: support-eu@foxitsoftware.com
		\arg www: http://www.foxitsoftware.com
 
 */
