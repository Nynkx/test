//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTDocumentManager.h
	\brief Declares the LTDocumentManager class and its delegate.
 */

#pragma once

#import <Foundation/Foundation.h>

@class LTDocument;
@class LTDocumentManager;

/*! \brief Delegate prototol for LTDocumentManager.
	
	Allows application to react on changes to the documents managed by the application.
 
	The implementation of all methods in this delegate is optional
 */
@protocol LTDocumentManagerDelegate <NSObject>

/*! \brief Called when a new LTDocument was added to document manager.
 
	Implementing this method is optional.
 
	\param manager Document manager calling the delegate.
	\param document The document that was added.
 */
@optional
- (void) documentManager:(LTDocumentManager*) manager didAddDocument:(LTDocument*) document;

/*! \brief Called when a document was removed from the document manager.
	
	Implementing this method is optional.
 
	\param manager Document manager calling the delegate.
	\param document The document that was removed.
 */
@optional
- (void) documentManager:(LTDocumentManager*) manager didRemoveDocument:(LTDocument*) document;

/*! \brief Called when a document was renamed.
 
	Implementing this method is optional.
 
	\param manager Document manager calling the delegate.
	\param document The document that was updated.
 */
@optional
- (void) documentManager:(LTDocumentManager*) manager didUpdateDocument:(LTDocument*) document;

/*! \brief Called when the meta data of a document was updated or generated.
	
	This methods will usually be called when the page count of a PDF has been read from file or
	the thumbnail for a document has been generated.
 
	Implementing this method is optional.
 
	\param manager Document manager calling the delegate.
	\param document The document whose meta data was upated.
 */
@optional
- (void) documentManager:(LTDocumentManager*) manager updatedMetaDataForDocument:(LTDocument*) document;

@end

/*! \brief Manages all documents stored in the applications document directory.
 
	A document is identified by its LTDocument::documentName property, which is equal to its filename without any extension.
	There can only be one document with a given name at a time, regardless of the document type.
	
	The document manager automatically handles generation of document thumbnails.
 
 	LTDocumentManager requires a valid MobileCompressionSDK license.
 */
@interface LTDocumentManager : NSObject

/*! \brief Delegate responding to LTDocumentManager events. */
@property (weak) id<LTDocumentManagerDelegate> delegate;

/*! \brief Get all documents currently stored in the applications document directory. */
@property (readonly) NSArray* documents;

/*! \brief Initialize a LTDocumentManager object.
 
	The method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 */
- (id) init;

/*! \brief Add a document to the document manager.

	If a document with the same name and different type does already exist, it is deleted first.
    In all cases, the newer document supersedes the previous one.
	Calls LTDocumentManagerDelegate::documentManager::didAddDocument: when done.
 
	\param document The document to add to the manager.
 */
- (void) addDocument:(LTDocument*) document;

/*! \brief Delete a document from the apps filesystem. 
 
	Calls LTDocumentManagerDelegate::documentManager:didRemoveDocument: when its done deleting the document.
 
	\param document The document to delete.
 */
- (void) removeDocument:(LTDocument*) document;

/*! \brief Change the name of a document. 
	
	Calls LTDocumentManagerDelegate::documentManager:didUpdateDocument: when its done renaming.
 
	\param document The document to rename.
	\param newDocumentName The name for the new document.
 */
- (void) renameDocument:(LTDocument*) document to:(NSString*) newDocumentName;

/*! \brief Find a document by its name.
 
	Returns a LTDocument object for a document with the given name.
	
	The objects returned by this method for the same \c documentName are not equal when their pointers are
	compared.
	To check whether two LTDocument object are equal use LTDocument::isEqual.
 
	\param documentName Name of the document to be returned.
	\return LTDocument for a document named \c documentName or \c nil if no document with that name does exist.
 */
- (LTDocument*) documentByName:(NSString*) documentName;

@end
