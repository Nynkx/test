//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTDetectionResult.h
	\brief Declares the LTDetectionResult class.
 */

#pragma once

#import <Foundation/Foundation.h>

/*! \brief The result of a document detection in an image.
 
	LTDetectionResult stores the result of a document detection performend on an image, e.g.
	a frame of the camera.
 
	The detected document is specified by the #points property, which may be empty when no document was detected.
	For valid detection (#valid) it contains four points.
	The points have the following order:
		\arg top-left,
		\arg top-right,
		\arg bottom-right,
		\arg bottom-left.
 
	To allow mapping of a detection result to different image sizes the points are always relative to the 
	#imageWidth and #imageHeight stored in the corresponding properties.

	In addition to the detected an object of LTDetectionResult also stores whether
		\arg the image contains glare (#hasGlare),
		\arg is too dark (#tooDark),
		\arg the detected document is large enough (#largeEnough).
 */
@interface LTDetectionResult : NSObject

/*! \brief Create a new detection result.
 
	The method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 
	\param points The corners of the document in the image.
	\param width Width of the image the points were detected in.
	\param height Height of the image the points were detected in.
 */
- (id) initWithPoints:(NSArray*) points imageWidth:(int)width imageHeight:(int) height;

/*! \brief Flag indicating whether detection was successful. 
 
	If the flag is set to \c YES #points contains 4 \NSValue objects, otherwise it is empty.
 */
@property (readonly, nonatomic) BOOL valid;

/*! \brief Flag indicating whether the scene is too dark. */
@property (readwrite, nonatomic) BOOL tooDark;

/*! \brief Flag indicating whether the document detected on the result contains glare. */
@property (readonly, nonatomic) BOOL hasGlare;

/*! \brief Flag indicating whether the detected object is big enough. */
@property (readonly, nonatomic) BOOL largeEnough;

/*! \brief Contains four corners of a detected document.
 
	When detection was successful the array contains four NSValues storing CGPoint objects, otherwise
	the array is empty.
	Points are ordered: top-left, top-right, bottom-right, bottom-left.
 
	To get one of the points as CGPoint use: 
	\code
	CGPoint p;
	NSValue* value = points[i];
	[value getValue:&p];
	\endcode
	
 */
@property (readonly, nonatomic) NSArray* points;

/*! \brief Width of the image the detection results was produced from. */
@property (readonly, nonatomic) int imageWidth;

/*! \brief Height of the image the detection results was produced from. */
@property (readonly, nonatomic) int imageHeight;

@end
