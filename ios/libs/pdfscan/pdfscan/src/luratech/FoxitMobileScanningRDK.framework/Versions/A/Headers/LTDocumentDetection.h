//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>

@interface LTDocumentDetection : NSObject

- (LTDocumentDetection*) init;

/*! \brief Detect the document in the image.

    The LTDetectionResult may not be valid when image was taken manually or when the capture service
    failed to detect the document in the final image.
 
    \param image Quality value in range [MinimumQuality, MaximumQuality].
 */
- (LTDetectionResult*) detect:(UIImage*) image;

@end
