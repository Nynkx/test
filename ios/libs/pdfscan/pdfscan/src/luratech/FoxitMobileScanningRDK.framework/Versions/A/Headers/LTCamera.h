//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTCamera.h
	\brief Declares LTCamera for accessing a device's camera and a corresponding delegate.
 */

#pragma once

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class LTCamera;

/*! \brief Delegate protocol for LTCamera.

	The protocol declares a method for notifying the delegate when an image was taken and
	passes it to the delegate.
	Another method notifies the delegate when capturing an image was canceled.
 */
@protocol LTCameraDelegate <NSObject>

/*! \brief Called by LTCamera when an image was captured.
 
	\param camera Camera object which took the image.
	\param image The image taken by the camera.
 */
- (void) camera:(LTCamera*)camera didCaptureImage:(UIImage*) image;

/*! \brief Called by LTCamera after the process of taking an image was canceled.
 
	Implementing this method is optional.
 
	\param camera Camera object which was canceled.
 */
@optional
- (void) cameraCanceledTakingImage:(LTCamera*) camera;

@end

/*! \brief Provides access to a device's camera.
 
	The LTCamera object is used as input device for LTCaptureService.
 
	It provides access to the camera's preview video-feed and an interface for taking images.
 
 */
@interface LTCamera : NSObject

/*! \brief Delegate implementing the LTCameraDelegate protocol.
 
	The delegate is notified whenever an image was taken or taking an image was canceled.
 */
@property (weak) id<LTCameraDelegate> delegate;

/*! \brief Does the device has a torch-light? */
@property (nonatomic, readonly) BOOL hasTorch;

/*! \brief Does the device support focus modes? */
@property (nonatomic, readonly) BOOL hasFocus;

/*! \brief Does the device support exposure settings. */
@property (nonatomic, readonly) BOOL hasExposure;

/*! \brief Enable or disable the light or check the current light state. */
@property (nonatomic) BOOL lightEnabled;

/*! \brief Encapsulated AVCapture device. */
@property (readonly) AVCaptureDevice* captureDevice;

/*! \brief Enable or disable a final autofocus before taking the picture.
 
	By default this is enabled.
 */
@property (nonatomic) BOOL autofocusBeforeTakingPicure;

/*! \brief Create a new LTCamera object using the provided AVCaptureDevice.
	
	The method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 
	\param device The device which is controlled by the LTCamera object.
 */
- (id) initWithCaptureDevice:(AVCaptureDevice*) device;

/*! \brief Create a new LTCamera object using the device at the given position.
 
	The current device must have a device for the provided position.
	The value for the 'standard' camera is \c AVCaptureDevicePositionBack.
 
	The method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
	
	\param position Position of the device that should be used by the LTCamera object.
 */
- (id) initWithDevicePosition:(AVCaptureDevicePosition) position;

/*! \brief Auto-focus at a certain point of interest.
 x and y range from 0.0 to 1.0.
 * Default: 0.5f, 0.5f (center of the screen)
 */
- (BOOL) setAutoFocusAtPoint:(CGPoint)point;

/*! \brief Instruct the camera to take an image.
	
	The image is not taking immediately, except for devices not supporting focus-modes.
	Before the image is actually taken the camera tries to re-focus on the current scene.
	After focussing is finished an image will be taken.
 
	The taken image will be passed to the delegates LTCameraDelegate::camera:didCaptureImage: method.
	The object does not hold any reference to the taken images afterwards.
 */
- (void) takePicture;

/*! \brief Instruct the camera to cancel taking an image.
 
	This methods has no effect when takePicture was not called or when taking an image is
	already done.
	It does only prevent an image to be taken when it is called while the camera is still
	focussing on the scene.
 
	When cancelling was successful the delegates LTCameraDelegate::cameraCanceledTakingImage
	will be called.
*/
- (void) cancelTakePicture;

/*! \brief Start the camera session.
 
	The camera will not update it's preview layer until it was started.
 */
- (void) start;

/*! \brief Stop the camera session.
 
	Stops updating the preview layer. 
 */
- (void) stop;

/*! \brief Creates a view that displays the video output. 
 
	The returned view an be embedded into an applications UI to show user's the current video-feed
	of the camera.
	The preview layer will not update until a call to LTCamera::start and stops until after a call to LTCamera::stop.
 */
- (AVCaptureVideoPreviewLayer*) createPreviewLayer;

@end
