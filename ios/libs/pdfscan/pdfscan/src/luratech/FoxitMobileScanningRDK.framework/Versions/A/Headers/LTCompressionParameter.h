//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>

/*! \brief PDF/A conformance levels. */
typedef enum
{
    LTNone,			/*!< \brief No PDF conformance. */
    LTPDFA_1A,
    LTPDFA_1B,
    LTPDFA_2A,
    LTPDFA_2U,
    LTPDFA_2B,
    LTPDFA_3A,
    LTPDFA_3U,
    LTPDFA_3B
}
LTPDFAVersion;

/*! \brief Bitonal coders for MRC compression. */
typedef enum
{
    LTFaxG4,      /*< \brief FaxG4 Coder. */
    LTjbig2       /*< \brief JBig2 coder. Higher quality, but slower. */
}
LTBitonalCoder;

/*! \brief Coders for image data.
 
 Used to set foreground and background coders for MRC compression.
 */
typedef enum
{
    LTjpeg,           /*!< JPEG coder. */
    LTjpeg2000        /*!< JPEG 2000 coder. Higher quality, but slower. */
}
LTImageCoder;

typedef LTImageCoder LTForegroundCoder;
typedef LTImageCoder LTBackgroundCoder;

@interface LTCompressionParameter : NSObject

/*! \brief Default constructor setting default values. */
- (id) init;

/*! \brief Get the bitonal coder used for compression. */
- (LTBitonalCoder) bitonalCoder;

/*! \brief Get the coder for the image's foreground. */
- (LTForegroundCoder) foregroundCoder;

/*! \brief Get the coder for the image's background. */
- (LTBackgroundCoder) backgroundCoder;

/*! \brief Get the foreground quality level. */
- (int) foregroundQuality;

/*! \brief Get the background quality level. */
- (int) backgroundQuality;

/*! \brief Get the segmentation foreground quality. */
- (int) segmentationForegroundQuality;

/*! \brief Get the segmentation background quality. */
- (int) segmentationBackgroundQuality;

/*! \brief Get the PDF-A conformance level. */
- (LTPDFAVersion) version;

/*! \brief Check whether PDF layer switches should be visible in a PDF viewer. */
- (BOOL) layered;

/*! \brief Check whether lossless JBig2 is used. */
- (BOOL) losslessJBIG2;

/*! \brief Get the text sensitivity for the MRC compression. */
- (int) textSensitivity;

/*! \brief Export mask layer as TIFF? */
- (BOOL) extraMaskOutput;
/*! \brief Filename to be used for mask layer */
- (NSString*) extraMaskPath;
/*! \brief Switch to export mask layer as TIFF. */
- (void) setExtraMaskOutput:(bool) val;
/*! \brief Set filename to be used for mask layer. */
- (void) setExtraMaskPath:(NSString*) filename;


/*! \brief Set the foreground quality level.
 \param val Quality value in range [MinimumQuality, MaximumQuality].
 */
- (void) setForegroundQuality:(int) val;

/*! \brief Set the background quality level.
 \param val Quality value in range [MinimumQuality, MaximumQuality].
 */
- (void) setBackgroundQuality:(int) val;

/*! \brief Set the segmentation foreground quality level.
 \param val Quality value in range [MinimumQuality, MaximumQuality].
 */
- (void) setSegmentationForegroundQuality:(int) val;

/*! \brief Set the segmentation background quality level.
 \param val Quality value in range [MinimumQuality, MaximumQuality].
 */
- (void) setSegmentationBackgroundQuality:(int) val;

/*! \brief Set whether layer switches should be displayed in a viewer. */
- (void) setLayered:(bool) layered;

/*! \brief Set the PDF-A conformance level to be used. */
- (void) setPdfAConformance: (LTPDFAVersion) conformance;

/*! \brief Set whether JBig2 should be lossless or lossy. */
- (void) setLosslessJBIG2:(bool) bLossless;

@end
