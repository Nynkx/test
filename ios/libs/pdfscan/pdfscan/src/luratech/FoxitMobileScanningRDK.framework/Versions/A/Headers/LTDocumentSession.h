//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTDocumentSession.h
	\brief Declares the LTDocumentSession class and its delegate.
 */

#pragma once

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "LTCompressionParameter.h"

@class LTDocumentSession;
@class LTDocument;

/*! \brief Delegate for the LTDocumentSession class.
	
	The delegate declares an interface that allows to react on load and save actions and to display their
	current progress to the user.
	
	Additionally delegate methods are called when the current document that is edited is changed, e.g. a page
	is added.
 
 	LTDocumentSession requires a valid MobileCompressionSDK license.
 */
@protocol LTDocumentSessionDelegate <NSObject>
@optional

/*! \brief Called when LTDocumentSession starts writing a document to a file. 
 
 	Implementing this method is optional.

	\param session The LTDocumentSession calling the method.
	\param documentName Name of the document which is saved.
 */
- (void) documentSession:(LTDocumentSession*) session startedSavingDocumentToFile:(NSString*) documentName;

/*! \brief Indicate the current progress when saving a page.
 
 	Implementing this method is optional.
	
	\param session The LTDocumentSession calling the method.
	\param done Current progress.
	\param total Total number of pages to save.
 */
- (void) documentSession:(LTDocumentSession*) session didFinishSavingPage:(int)done of:(int)total;

/*! \brief Called when a document was successfully saved.
 
 	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param document LTDocument created by saving the document.
 */
- (void) documentSession:(LTDocumentSession*) session didFinishSavingDocument:(LTDocument*) document;

/*! \brief Called when a page was added to the document.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param newPageIndex The index of the new page.
 */
- (void) documentSession:(LTDocumentSession*) session didAddPage:(int) newPageIndex;

/*! \brief Called when a page was removed from the document.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param removedPageIndex Index of the page that was removed.
 */
- (void) documentSession:(LTDocumentSession*) session didRemovePage:(int) removedPageIndex;

/*! \brief Called when the position of a page in a document has changed.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param oldIndex oldIndex of the moved page.
	\param newIndex newIndex of the moved page.
 */
- (void) documentSession:(LTDocumentSession*) session didMovePage:(int) oldIndex toPage:(int) newIndex;

/*! \brief Called when the image for a page was replaced.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param pageIndex The index of the page that was replaced.
 */
- (void) documentSession:(LTDocumentSession*) session didReplacePage:(int) pageIndex;

/*! \brief Called when a document session starts loading a document.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param document The document that is loaded.
 */
- (void) documentSession:(LTDocumentSession*) session didStartLoadingDocument:(LTDocument*) document;

/*!	\brief Called to indicate update progress.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param page The page that was loaded.
	\param total Total number of pages that will be loaded.
 */
-(void) documentSession:(LTDocumentSession*) session didLoadPage:(int) page ofTotal:(int) total;

/*!	\brief Called when loading a document is done.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param document The document that was loaded.
 */
- (void) documentSession:(LTDocumentSession*) session didFinishLoadingDocument:(LTDocument*) document;

/*! \brief Called when loading a document was canceled.
 
	Implementing this method is optional.
 
	\param session The LTDocumentSession calling the method.
	\param document The document whose loading process was canceled.
 */
- (void) documentSession:(LTDocumentSession*) session didCancelLoadingDocument:(LTDocument*) document;

@end

/*! \brief LTDocumentSession handles the creation and manipulation of documents.
 
	A LTDocumentSession object represents a document in memory that is currently being edited or created.
	It offers an interface for adding, moving, removing and replacing page in a document.
 
	Pages are added to the document as images (\c UIImage).
 
	A LTDocumentSession should not be initialized using an \c init method but with the two static initializers:
		\arg #createNewDocument
		\arg #createSessionForEdit
*/
@interface LTDocumentSession : NSObject

/*! \brief Create a LTDocumentSession for a new document.
 
	This method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 */
+ (LTDocumentSession*) createNewDocument;

/*! \brief Create a LTDocumentSession for editing an existing document.
 
	This method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 */
+ (LTDocumentSession*) createSessionForEdit;

/*! \brief Delegate responding to LTDocumentSession events. */
@property (weak, nonatomic) id<LTDocumentSessionDelegate> delegate;

/*! \brief Get the current page count. */
@property (readonly, nonatomic) int pageCount;

/*! \brief Check whether any changes were made to the document changes. */
@property (readonly, nonatomic) BOOL hasChanges;

/*! \brief Check whether the document is a new document. */
@property (readonly, nonatomic) BOOL isNewDocument;

/*! \brief The document's name. */
@property (readonly, nonatomic) NSString* documentName;

/*! \brief Add a page to the document. 
	
	Calls LTDocumentSessionDelegate::documentSession:didAddPage: when adding the page is done. Default compression parameters are used.
 
	\param image Image for the new page.
 */
- (void) addPage:(UIImage*) image;
/*! \brief Add a page to the document.
	
	Calls LTDocumentSessionDelegate::documentSession:didAddPage: when adding the page is done.
 
	\param image Image for the new page.
    \param compressionParameter compression parameter to compress the page.
 
 */
- (void) addPage:(UIImage*) image withCompressionParameter:(LTCompressionParameter*) compressionParameter;

/*! \brief Remove a page from document. 
 
	Calls LTDocumentSessionDelegate::documentSession:didRemovePage: when removing the page is done.
 
	\param pageIndex Index of the page to be removed.
 */
- (void) removePage:(int) pageIndex;

/*! \brief Move a page. 
	
	Calls LTDocumentSessionDelegate::documentSession:didMovePage:toPage when moving the page is done.
 
	\param from Old index of the page.
	\param to New index of the page.
*/
- (void) movePage:(int)from toIndex:(int) to;

/*! \brief Replace the image of a page.
 
	Calls LTDocumentSessionDelegate::documentSession:didReplacePage: when replacing the page image is done.
 
	\param pageIndex Index of the page.
	\param image New image for the page.
 */
- (void) replacePage:(int)pageIndex withImage:(UIImage*) image;

/*! \brief Save the document as PDF.
 
	Calls the following delegate methods:
		\arg LTDocumentSessionDelegate::documentSession:startedSavingDocumentToFile: when saving started,
		\arg LTDocumentSessionDelegate::documentSession:didFinishSavingPage:of: for every page that was saved and compressed and
		\arg LTDocumentSessionDelegate::documentSession:didFinishSavingDocument: when it is done saving the document.
 
	\param documentName Name of the document.
 */
- (void) saveAsPDF:(NSString*) documentName;

/*! \brief Save the document as image series.
 
	Calls the following delegate methods:
		\arg LTDocumentSessionDelegate::documentSession:startedSavingDocumentToFile: when saving started,
		\arg LTDocumentSessionDelegate::documentSession:didFinishSavingPage:of: for every page that was saved and compressed and
		\arg LTDocumentSessionDelegate::documentSession:didFinishSavingDocument: when it is done saving the document.
 
	\param documentName Name of the document.
 */
- (void) saveAsImages:(NSString*) documentName;

/*! \brief Load the page of the document into the session.
	
	All pages of the document will be added to the session.
	It is possible to load multiple documents into a session, however, not concurrently.
 
	Calls the following delegate methods:
		\arg LTDocumentSessionDelegate::documentSession:didStartSavingDocument: when loading started,
		\arg LTDocumentSessionDelegate::documentSession:didLoadPage:ofTotal: for every page that was loaded and
		\arg LTDocumentSessionDelegate::documentSession:didFinishLoadingDocument: when it is done loading the document.
 
	\param document The document to be loaded into the sesssion.
*/
- (void) loadDocument:(LTDocument*) document;

/*! \brief Cancel loading a document. 
 
	This method has no effect, when the document session is currently not loading a document.
 */
- (void) cancelLoading;

/*! \brief Get a thumbnail for a page.
	\param pageIndex Index of the page for which the thumbnail should be returned.
	\return The thumbnail of the page.
 */
- (UIImage*) thumbnailForPage:(int) pageIndex;

/*! \brief Get an image for a page.
	\param pageIndex Index of the page for which the image should be returned.
	\return The image of the page.
 */
- (UIImage*) imageForPage:(int) pageIndex;

@end
