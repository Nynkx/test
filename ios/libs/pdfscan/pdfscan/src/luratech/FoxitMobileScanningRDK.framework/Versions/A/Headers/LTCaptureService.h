//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*! 
	\file LTCaptureService.h
	\brief Declares the LTCaptureService class and its delegate.
*/

#pragma once

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "LTCamera.h"

@class LTCamera;
@class LTCaptureService;
@class LTDetectionResult;

/*! \brief Delegate for the capture service.
	
	The delegate declares an interface allowing the LTCaptureService to publish its detection results
	and allows the applications to indicate detection results in the UI.
 */
@protocol LTCaptureServiceDelegate <NSObject>

/*! \brief Called by capture service when an image was taken.
 
	The LTDetectionResult may not be valid when image was taken manually or when the capture service
	failed to detect the document in the final image.
 
	Implementing this method is required.
 
	\param service Capture service which took the image.
	\param image Image taken by the capture service.
	\param detection The detection made on the image.
 */
@required
- (void) captureService:(LTCaptureService*) service
		didCaptureImage:(UIImage*) image
		  withDetection:(LTDetectionResult*) detection;

/*! \brief Called by a capture whenever a frame was evaluated.
 
 	Implementing this method is optional.
 
	\param service Capture service calling the delegate.
	\param result Detection result in the current frame.
 */
@optional
- (void) captureService:(LTCaptureService*)service
			   detected:(LTDetectionResult*) result;

/*! \brief Called by the capture service to indicate the progress of the auto trigger process.
	
	The method can be used to update the GUI to visualize the progress of auto triggering.
 
 	Implementing this method is optional.
 
	\param service Capture service calling the delegate.
	\param currentFrameCount The number of valid frames.
	\param requiredFrames Number of frames required until the camera is automatically triggered.
 */
@optional
- (void) captureService:(LTCaptureService*) service
	  didEvaluateResult:(int) currentFrameCount
			 ofRequired:(int) requiredFrames;

/*! \brief Called by the capture service when the camera was triggered automatically.
 
 	Implementing this method is optional.
 	
	\param service Capture service calling the delegate.
 */
@optional
- (void) captureServiceDidAutoTrigger:(LTCaptureService*) service;

/*! \brief Called by a capture service when auto trigger did cancel.
 
	A reason to cancel the automatic triggering may be intense movement of the device.
 
 	Implementing this method is optional.
 	\param service Capture service calling the delegate.
 */
@optional
- (void) captureServiceAutoTriggerCanceled:(LTCaptureService*) service;

/*! \brief Called by capture service to propose a GUI update.
 
	The difference to LTCaptureServiceDelegate::captureService:detected: is that this method will not be called every frame.
	The capture service attempts to smooth the output result and prevent the visualization from 
	making abrupt changes.
 
	Implementing this method is optional.
 
 	\param service Capture service calling the delegate.
	\param result Result that should be display in the GUI.
 */
@optional
- (void) captureService:(LTCaptureService*) service
		proposesGUIUpdate:(LTDetectionResult*) result;
@end



/*! \brief Service for capturing images.
 
	The capture service is initialized with a LTCamera object, which is used as video/image input source.
	It takes control over the camera and sets itself as the camera's delegate.
	
	The capture service will automatically trigger the camera when:
		\arg a document is detected
		\arg the document occupies enough space in the image
		\arg no glare is detected on the document
		\arg scene is bright enough
		\arg the device is not moving
	The delegates LTCaptureServiceDelegate::captureServiceDidAutoTrigger: method is called to notify the delegate.
	
	The current progress of the auto-trigger process is published using the delegate's optional
	LTCaptureServiceDelegate::captureService:didEvaluateResult:ofRequired: method.
 
	Similar to accessing LTCamera directly, the auto trigger mechanism will first re-focus the camera
	before the image is taken.
 
	After an image was successfully taken the delegate's LTCaptureServiceDelegate::captureSerivce:didCaptureImage:withResult is called to
	provide the delegate the with the taken image and a document detection.
	The detection may not be valid, since the delegate method is also called when the camera was triggered
	by another object.
 
	In case during focussing the camera a movement of the device is detected, the auto-trigger process is
	canceled.
	The delegate is notified by calling LTCaptureServiceDelegate::captureServiceAutoTriggerCanceled:.
 */
@interface LTCaptureService : NSObject<LTCameraDelegate>

/*! \brief Delegate to handle capture service events. */
@property (weak, nonatomic) id<LTCaptureServiceDelegate> delegate;

/*! \brief The camera object used by the capture service. */
@property (readonly, nonatomic) LTCamera* camera;

/*! \brief Enable or disable automatic triggering. 

	By default autoTrigger is enabled.
 
	When you disable automatic triggering, you can still call LTCamera::takePicture to take images.
	The image including a detection will then be published using LTCaptureServiceDelegate::captureService::didCaptureImage:withDetection.
 */
@property (nonatomic) BOOL useAutoTrigger;

/*! \brief Initialize the capture service using the provided camera.
 
	The method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 
	\param camera Camera object which will be used by the capture service.
 */
- (id) initWithCamera:(LTCamera*) camera;

/*! \brief Start processing camera frames. 
 
	The methods calls LTCamera::start to start producing video output.
	
	For every frame the capture service processes LTCaptureServiceDelegate::captureService:detected: is called
	to provide the delegate with the detection result made in the current frame.
 */
- (void) start;

/*! \brief Stop processing camera frames. 
 
	The method does \b NOT call LTCamera::stop, thus after the capture service is stopped, the camera still outputs its
	preview video feed.
 */
- (void) stop;

@end
