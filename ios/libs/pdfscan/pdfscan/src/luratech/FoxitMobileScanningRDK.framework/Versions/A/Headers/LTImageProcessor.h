//
//  Foxit Europe AppFrameworkSDK
//
//  Copyright (c) 2017 Foxit Europe GmbH. All rights reserved.
//

/*!
	\file LTImageProcessor.h
	\brief Declares the LTImageProcessor class and its delegate.
 */

#pragma once

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LTDetectionResult;
@class LTImageProcessor;

/*! \brief Rotation direction for the image processor. */
typedef enum
{
	LTRotationDirectionCW,		/*!< \brief Rotate the image clock-wise. */
	LTRotationDirectionCCW,		/*!< \brief Rotate the image counter-clock-wise. */
	LTRotationDirectionNone		/*!< \brief No rotation. */
} LTRotationDirection;

/*! \brief Color mode for the image processor. */
typedef enum
{
	LTColorModeRGBWhiteBalance,		/*!< \brief Color image with white balance applied. */
	LTColorModeRGB,					/*!< \brief Color image. */
	LTColorModeGrayscale,			/*!< \brief Grayscale image. */
	LTColorModeBitonal				/*!< \brief Bitonal image using adaptive binarization. */
} LTColorMode;

/*! \brief Delegate responding to image processor events. */
@protocol LTImageProcessorDelegate<NSObject>

/*! \brief This function is called when the image processor is done computing a preview image. 
 
	\param processor Image processor calling the delegate
	\param image The computed preview image.
 */
- (void) imageProcessor:(LTImageProcessor*) processor didFinishComputingPreviewImage:(UIImage*) image;

/*! \brief This function is called when the image processor is done computing a result image. 
 
 	\param processor Image processor calling the delegate
	\param image The computed result image.
 */
- (void) imageProcessor:(LTImageProcessor*) processor didFinishComputingResultImage:(UIImage*) image;

/*! \brief Function called when the computation of a new image started. 
	
	Implementing this method is optional.
 
 	\param processor Image processor calling the delegate
 */
@optional
- (void) imageProcessorStartedComputing:(LTImageProcessor*) processor;

@end


/*! \brief Asynchronous image processing.
 
	For performance reasons image operations can be performed on a smaller preview version of the image original image.
	This can be controlled using the #computeLowResPreview property.
 
	The constrast and brightness levels must be in the range [-10 10], where 0 means no adjustment.
	Values smaller 0 result in darker images / less contrast, values larger 0 result in brighter images / increased contrast.
 
	Result and preview images are computed with calls to #computePreview and #computeResult.
	Results will then be send to the image procesor's delegate.
 */
@interface LTImageProcessor : NSObject

/*!	\brief Create a new image processor using the given image.
	
	The method returns nil when ::AppFrameworkSdk_Init was not called or initialization failed.
 
	\param image The image to process.
 */
- (LTImageProcessor*) initWithImage:(UIImage*)image;

/*! \brief Delegate responding to the image processor's events. */
@property (weak, nonatomic) id<LTImageProcessorDelegate> delegate;

/*! \brief Check whether the image processor is currently computing a preview or result image. */
@property (readonly, nonatomic) BOOL isComputing;

/*! \brief The input image. */
@property (readonly, nonatomic) UIImage* inputImage;

/*! \brief The detection result used to extract a region from the image. */
@property (nonatomic) LTDetectionResult* detectionResult;

/*! \brief The LTColorMode used for the preview and result. */
@property (nonatomic) LTColorMode colorMode;

/*! \brief Rotate the image in the given direction.
	\param direction Direction in which the image is rotated.
 */
- (void) rotateImage:(LTRotationDirection) direction;

/*! \brief Reset the image to its original orientation. */
- (void) resetRotation;

/*! \brief The contrast level for the image. */
@property (nonatomic) int contrastLevel;

/*! \brief The brighntess level for the image. */
@property (nonatomic) int brightnessLevel;

/*! \brief Compute a low-resolution preview image.
 
	The low resolution image will have a size suitable for displaying it on the current device.
 */
@property (nonatomic) BOOL computeLowResPreview;

/*! \brief Set the width and height of the result image. 
		
	Set the page format to which the image should be adjusted.
	The image processor transforms the image to fit the given aspect ratio.
 
	DPI values are computed and attached to the image so that the when result image is added to a document and
	saved as PDF, the page will have the specified dimensions.
 
	To disable page format adjustments set both values to 0.
 
	\param width Width in millimeter
	\param height Height in millimeter.
 */
- (void) setPageFormatWidth:(int) width height:(int) height;
/*! \brief Like (void) setPageFormatWidth:(int) width height:(int) height;
     but with additional receipt parameter.
     
     If an image is a receipt, the aspect ratio will not be forced, since
     the height is deliberatly left unspecified.
 */
- (void) setPageFormatWidth:(int) width height:(int) height receipt:(Boolean) receipt;

- (int) horizontalDpi;

- (int) verticalDpi;

/*! \brief Set the threshold for matching the image to the aspect ratio set by LTImageProcessor::setPageFormatWidth:height.
 
	The threshold is used to decide whether a given image should be transformed into the requested page format.
	To force the transformation use a rather high value, e.g. 10.
 
	The default value is \c 0.1.
 
	\param threshold Threshold value for matching the page format.
 */
- (void) setPageFormatThreshold:(float) threshold;

/*! \brief Start computing a preview image.
 
	Calls the delegate methods LTImageProcessorDelegate::imageProcessorStartedComputing and
	LTImageProcessorDelegate::imageProcessor:didFinishComputingPreviewImage:.
 */
- (void) computePreview;

/*! \brief Start computing a result image.
 
	Calls the delegate methods LTImageProcessorDelegate::imageProcessorStartedComputing and
	imageProcessor:didFinishComputingResultImage:.
 */
- (void) computeResult;

@end
