/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#import <UIKit/UIKit.h>

//! Project version number for pdfscan.
FOUNDATION_EXPORT double FoxitPDFScanUIVersionNumber;

//! Project version string for pdfscan.
FOUNDATION_EXPORT const unsigned char FoxitPDFScanUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FoxitPDFScanUI/PublicHeader.h>

#import <FoxitPDFScanUI/PDFScanManager.h>
