/**
* Copyright (C) 2003-2021, Foxit Software Inc..
* All Rights Reserved.
*
* http://www.foxitsoftware.com
*
* The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
* distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
* is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
* Review legal.txt for additional license and legal information.
*/

#define DEVICE_iPHONE ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#import <UIKit/UIKit.h>

@interface FXImagePickThumbnailCell : UICollectionViewCell
@property (nonatomic, strong) UIImage *thumbImage;
@property (nonatomic, assign) BOOL isHighlight;
@property (nonatomic, assign) BOOL isDisable;
@end
