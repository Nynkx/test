/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

/**
 * @file    Defines.h
 * @details    Macros of constants or helper functions. Shared among the public headers.
 */

#import <Foundation/Foundation.h>

#define FSLocalizedString(string) (string == nil ? nil : [[NSBundle bundleForClass:[self class]] localizedStringForKey:(string) value:@"" table:@"FoxitLocalizable"])

#define GetImage(name) [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]

#define isEmptyString(string) ([string isEqualToString:@""] || string == nil)

#define EDIT_ITEM_STROKEOUT 8
#define EDIT_ITEM_UNDERLINE 10
#define EDIT_ITEM_HIGHLIGHT 12
#define EDIT_ITEM_NOTE 14
#define EDIT_ITEM_RECTANGLE 2
#define EDIT_ITEM_FREETEXT 6
#define EDIT_ITEM_PENCIL 4

#define DOCUMENT_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

//Define thumbnail/overview image size
//old Width = 58.0
#define THUMBNAIL_IMAGE_WIDTH 58.0
#define THUMBNAIL_IMAGR_HEIGHT 58.0
#define THUMBNAIL_IMAGE_WIDTH_EX 84.0
#define THUMBNAIL_IMAGE_HEIGHT_EX 87.0
#define THUMBNAIL_IMAGE_WIDTH_LARGE_EX 128.0
#define THUMBNAIL_IMAGE_HEIGHT_LARGE_EX 134.0f
#define FILE_IMAGE_WIDTH 50.0
#define FILE_IMAGE_HEIGHT 54.0
#define OVERVIEW_IMAGE_WIDTH 160.0
#define OVERVIEW_IMAGE_HEIGHT 200.0
#define OVERVIEW_IMAGE_WIDTH_IPHONE 142.0
#define OVERVIEW_IMAGE_HEIGHT_IPHONE 177.0

#define kPreventRepeatClickTime(_seconds_) \
static BOOL shouldPrevent; \
if (shouldPrevent) return; \
shouldPrevent = YES; \
dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((_seconds_) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{ \
shouldPrevent = NO; \
}); \

#define FS_TOPBAR_ITEM_BOOKMARK_TAG 100     ///< tag of bookmark button item in top tool bar
#define FS_TOPBAR_ITEM_BACK_TAG 101         ///< tag of back button item in top tool bar
#define FS_TOPBAR_ITEM_MORE_TAG 102         ///< tag of more button item in top tool bar
#define FS_TOPBAR_ITEM_SEARCH_TAG 103       ///< tag of search button item in top tool bar
#define FS_BOTTOMBAR_ITEM_PANEL_TAG 200     ///< tag of panel button item in bottom tool bar
#define FS_BOTTOMBAR_ITEM_ANNOT_TAG 201     ///< tag of annotation button item in top bottom bar
#define FS_BOTTOMBAR_ITEM_SIGNATURE_TAG 202 ///< tag of signature button item in top bottom bar
#define FS_BOTTOMBAR_ITEM_READMODE_TAG 203  ///< tag of read mode button item in top bottom bar
#define FS_BOTTOMBAR_ITEM_FILLSIGN_TAG 204 ///< tag of fill sign button item in top bottom bar

/** @brief States of extensions manager. */
#define STATE_NORMAL 1        ///< tag of normal ui state
#define STATE_REFLOW 2        ///< tag of reflow ui state
#define STATE_SEARCH 3        ///< tag of search ui state
#define STATE_EDIT 4          ///< tag of edit ui state
#define STATE_SIGNATURE 5     ///< tag of signature ui state
#define STATE_ANNOTTOOL 6     ///< tag of annottool ui state
#define STATE_PANZOOM 7      ///< tag of panandzoom state
#define STATE_PAGENAVIGATE 8  ///< tag of pagenavigate ui state
#define STATE_CREATEFORM 9   ///< tag of create form state
#define STATE_SPEECH 10      ///< tag of panandzoom state
#define STATE_FILLSIGN 11    ///< tag of fill sign state

#define Tool_Select @"Select"         ///< name of select tool
#define Tool_Note @"Note"             ///< name of note tool
#define Tool_Freetext @"Freetext"     ///< name of free text tool
#define Tool_Textbox @"Textbox"       ///< name of text box tool
#define Tool_Callout @"Callout"       ///< name of callout tool
#define Tool_Pencil @"Pencil"         ///< name of pencil tool
#define Tool_Eraser @"Eraser"         ///< name of eraser tool
#define Tool_Stamp @"Stamp"           ///< name of stamp tool
#define Tool_Insert @"Insert"         ///< name of insert text tool
#define Tool_Replace @"Replace"       ///< name of replace text tool
#define Tool_Attachment @"Attachment" ///< name of attachment tool
#define Tool_Signature @"Signature"   ///< name of signature tool
#define Tool_Line @"Line"             ///< name of line tool
#define Tool_Arrow @"Arrow"           ///< name of arrow line tool
#define Tool_Markup @"Markup"         ///< name of markup tool, which includes highlight squiggly strikeout and underline tools
#define Tool_Highlight @"Highlight"   ///< name of highlight tool
#define Tool_Squiggly @"Squiggly"     ///< name of squiggly tool
#define Tool_Strikeout @"Strikeout"   ///< name of strikeout tool
#define Tool_Underline @"Underline"   ///< name of underline tool
#define Tool_Shape @"Shape"           ///< name of shape tool, which includes rectangle and oval tools
#define Tool_Rectangle @"Rectangle"   ///< name of rectangle tool
#define Tool_Oval @"Oval"             ///< name of oval tool
#define Tool_Distance @"Distance"     ///< name of distance tool
#define Tool_Image @"Image"           ///< name of image tool
#define Tool_Polygon @"Polygon"       ///< name of polygon tool
#define Tool_Cloud @"Cloud"           ///< name of cloud tool
#define Tool_PolyLine @"PolyLine"     ///< name of polyline tool
#define Tool_Audio @"Audio"           ///< name of audio tool
#define Tool_Video @"Video"           ///< name of video tool
#define Tool_Multiple_Selection @"MultipleSelection"           ///< name of MultipleSelection tool
#define Tool_Redaction @"Redaction"       ///< name of redact tool
#define Tool_Form @"Form"              ///< name of form tool
#define Tool_Fill_Sign @"FillSign"     ///< name of fill and sign tool

#define Module_Fill_Sign @"FillSign"     ///< name of fill and sign module

/** @brief Customized annotation type on application level. */
#define FSAnnotArrowLine 99    ///< tag of Arrow custom annot type
#define FSAnnotInsert 101      ///< tag of Insert custom annot type
#define FSAnnotTextbox 103     ///< tag of Textbox custom annot type
#define FSAnnotCloud 105       ///< tag of Cloud custom annot type
#define FSAnnotCallout 107     ///< tag of Callout custom annot type
#define FSAnnotDistance 109    ///< tag of Distance custom annot type
#define FSAnnotReplace  111    ///< tag of Replace custom annot type

#define IOS11_OR_LATER ([[UIDevice currentDevice] systemVersion].floatValue >= 11.0)

#if defined(TARGET_OS_IPHONE) && defined(TARGET_OS_UIKITFORMAC)
    #if !TARGET_OS_UIKITFORMAC && TARGET_OS_IPHONE
        #define _MAC_CATALYST_ 0
    #else
        #define _MAC_CATALYST_ 1
    #endif
#else
    #define _MAC_CATALYST_ 0
#endif
