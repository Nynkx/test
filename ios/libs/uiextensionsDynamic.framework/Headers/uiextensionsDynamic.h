/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

/**
 * @file    uiextensionsDynamic.h
 * @details    The public header files collection.
 */

#import <UIKit/UIKit.h>

//! Project version number for uiextensionsDynamic.
FOUNDATION_EXPORT double uiextensionsDynamicVersionNumber;

//! Project version string for uiextensionsDynamic.
FOUNDATION_EXPORT const unsigned char uiextensionsDynamicVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <uiextensionsDynamic/PublicHeader.h>

#import <uiextensionsDynamic/Defines.h>
#import <uiextensionsDynamic/FileListViewController.h>
#import <uiextensionsDynamic/UIExtensionsManager.h>
#import <uiextensionsDynamic/UIExtensionsConfig.h>
#import <uiextensionsDynamic/PanelController.h>
#import <uiextensionsDynamic/PanelHost.h>
#import <uiextensionsDynamic/SegmentView.h>
#import <uiextensionsDynamic/SettingBar.h>
#import <uiextensionsDynamic/MoreView.h>
