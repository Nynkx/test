/**
 * Copyright (C) 2003-2021, Foxit Software Inc..
 * All Rights Reserved.
 *
 * http://www.foxitsoftware.com
 *
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK for iOS to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */

/**
 * @file    MoreView.h
 * @details    The definition of the more view. Click the "more" button on the top tool bar to see the more view.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class MoreMenuItem;

#define TAG_GROUP_FILE 1
#define TAG_GROUP_PROTECT 4
#define TAG_GROUP_COMMENT 6
#define TAG_GROUP_FORM 7


#define TAG_ITEM_FILEINFO 10
#define TAG_ITEM_SAVE_AS 11
#define TAG_ITEM_REDUCEFILESIZE 12
#define TAG_ITEM_WIRELESSPRINT 14
#define TAG_ITEM_SCREENCAPTURE 16

#define TAG_ITEM_PASSWORD 17
#define TAG_ITEM_CERTIFICATE 18

#define TAG_ITEM_CREATEFORM 23
#define TAG_ITEM_RESETFORM 24
#define TAG_ITEM_IMPORTFORM 25
#define TAG_ITEM_EXPORTFORM 26

#define TAG_ITEM_IMPORTCOMMENT 12
#define TAG_ITEM_EXPORTCOMMENT 13

/** @brief Callback for click action. */
typedef void (^CancelCallback)(void);

/** brief Protocol for menu callback action. */
@protocol MoreMenuItemAction <NSObject>
/** brief Callback triggered when click on a memu item. */
- (void)onClick:(MoreMenuItem *)item;
@end


/** brief Definition for menu item.*/
@interface MoreMenuItem : NSObject
/** brief The menu tag.*/
@property (nonatomic, assign) NSUInteger tag;
/** brief The menu title text.*/
@property (nonatomic, strong) NSString *text;
/** brief The menu icon id. Unused.*/
@property (nonatomic, assign) NSInteger iconId;
/** brief The menu item is enabled or not.*/
@property (nonatomic, assign) BOOL enable;
/** brief The callback action associated with this menu item.*/
@property (nonatomic, weak) id<MoreMenuItemAction> callBack;
/** brief The custom view for item. Unused.*/
@property (nonatomic, strong) UIView *customView;
@end

/** @brief The menu group. */
@interface MoreMenuGroup : NSObject
/** @brief The tag for menu group.*/
@property (nonatomic, assign) NSUInteger tag;
/** @brief The title for menu group.*/
@property (nonatomic, strong) NSString *title;
/** @brief Get the menu items.*/
- (NSMutableArray *)getItems;
/** @brief Set the menu items.*/
- (void)setItems:(NSMutableArray *)arr;
@end

/** brief Menu view for "more menu". */
@interface MoreMenuView : NSObject
/** brief Callback called when cancel item clicked.*/
@property (nonatomic, copy) CancelCallback onCancelClicked;
/** brief Add a new menu group. */
- (void)addGroup:(MoreMenuGroup *)group;
/** brief Remove menu group by group tag.*/
- (void)removeGroup:(NSUInteger)tag;
/** brief Get menu group by group tag.*/
- (MoreMenuGroup *)getGroup:(NSUInteger)tag;
/** brief Add a new memu item on the specified group. */
- (void)addMenuItem:(NSUInteger)groupTag withItem:(MoreMenuItem *)item;
/** brief Remove a existing memu item from the specified group. */
- (void)removeMenuItem:(NSUInteger)groupTag WithItemTag:(NSUInteger)itemTag;
/** brief Get the content view for the menu view. */
- (UIView *)getContentView;
/** brief Set the title for the menu view. */
- (void)setMenuTitle:(NSString *)title;
/** brief Reload the data for this menu view, which is based on a table view. */
- (void)reloadData;
/** brief Set visible or invisible stat for specified menu group. */
- (void)setMoreViewItemHiddenWithGroup:(NSUInteger)groupTag hidden:(BOOL)isHidden;
/** brief Set visible or invisible stat for specified menu item on specified menu group. */
- (void)setMoreViewItemHiddenWithGroup:(NSUInteger)groupTag andItemTag:(NSUInteger)itemTag hidden:(BOOL)isHidden;
@end
